function [img background gain offset]= loadimage(varargin)
background=[];
gain=[];
offset=[];
filename = varargin{1};
normalize = varargin{2};
if numel(varargin)>2
    wl = varargin{3};
    [~, part2 part3] = fileparts(wl);
    %     wl = strsplit('.',wl);
    ext = part3;
    wl = part2;
else
    wl= '';
end

% load image from cache
img = imageCache(filename);



%%% check if background can be approximated by gain/offset
pos = regexp(filename, '(_p)(\d{3,4})', 'match');
if isempty(pos)
    pos={'0'};
    %         fprintf('Regular expression not found: Could not find background files\n')
    %         return
end
pos=str2double(pos{1}(3:end));
%     tp = regexp(filename, '(_t)(\d{4,5})', 'match');
%     tp = str2double(tp{1}(3:end));
%     wl = regexp(filename, '(_w)(\d{1,2})', 'match');
%     wl = str2double(wl{1}(3:end));
[exp expid] = regexp(filename, '\d{6}[A-Z]{2,}\d{0,2}', 'match');
if isempty(exp)
    exp={'0'};
end
exp=exp{1};


% load background
offset=29;
if strfind(filename,'_z')
    offset=34;
end
[mainpath,name] = fileparts(filename);
%     p =  strfind(filename,'_p');
if ~isempty(mainpath) && strcmp(mainpath(end),'\')
    mainpath(end)=[];
end
[~,posFolder]=fileparts(mainpath);
if normalize==1
    bgname = [mainpath '/../background/' posFolder '/' name '.png'];
    gainname = [mainpath '/../background/' posFolder  '/gain_' wl '.png'];
    offsetname = [mainpath '/../background/' posFolder '/offset_' wl '.png'];
    bgmatname = [mainpath '/../background/' sprintf('%s_p%04d/%s_p%04d_%s.bg',exp,pos,exp,pos,wl)];
    %old
    %     bgname = [mainpath '/../background/' filename(p(1)-10:end-3) 'png'];
    %     gainname = [mainpath '/../background/' filename(p(1)-10:p(1)+5) '/gain_' wl '.png'];
    %     offsetname = [mainpath '/../background/' filename(p(1)-10:p(1)+5) '/offset_' wl '.png'];
    %     bgmatname = [mainpath '/../background/' sprintf('%s_p%04d/%s_p%04d_%s.bg',exp,pos,exp,pos,wl)];
    
    if exist(bgmatname,'file')
%         fprintf('Loading new approximation: ')
        background=imageCache(bgname);
        [gain offset] = gainCache(bgmatname);
        img = normalizeImage(img,background,gain,offset,[]);
    elseif exist(bgname,'file')
        background=imageCache(bgname);
        
        %         % load gain/offset
        %         if ~exist(gainname,'file')
        %             gainname = [filename(1:p(1)-10) 'background' filename(p(1)-10:p(1)+5) '/gain.png'];
        %         end
        
        if exist(gainname,'file')
            gain = imageCache(gainname)*255;
            
            %             offset = imageCache(offsetname);
            offset=[];
            
            % normalize the newest method
            img = normalizeImage(img,background,gain,offset);
        else
            fprintf('Warning: Gain/offset not found %s using old normalization\n',gainname);
            % use the method without gain/offset
            img = normalizeImage(img,background);
        end
    else
        
        
        fprintf('Warning: Background not found %s no normalization will be applied.\n',bgname);
        
    end
elseif normalize == 3
    %%%% Do MSER brightfield normalization (Buggenthin et al., 2013)
    
    % load background image
    bg = imageCache([mainpath '/../background_projected/' sprintf('%s_p%04d/%s_p%04d_%s_projbg.png',exp,pos,exp,pos,wl)]);
    %     bgmatname = double(bgmatname)/(2^16-1);
    
    
    % process raw image
    %      img = img./bg;
    %
    %      img = img./mean(img(:));
end
img(isnan(img))=0;
img(isinf(img))=0;