function [Center]=myseg_drlse(OrigImage, Threshold, trackset, centerX, centerY,bg,imgraw)
%% load needed libraries and imagej, set path
%addpath(genpath('/home/ibis/felix.buggenthin/PhD/Hemato/src'));
%addpath(genpath('/home/ibis/felix.buggenthin/Frameworks/Matlab'));
%addpath('/home/ibis/felix.buggenthin/Frameworks/Matlab/Active_Contour/DRLSE_v0/');
% domovie = 0;
% nr = str2double(nrstr);
doplot = 1;
domovie=1;
% load([tracksetpath '/' moviename '/' num2str(nr) '.mat'])
% load(tracksetpath);
% trackset = results.quants{str2double(nrstr)};
% trackset = results{str2double(nrstr)}.detects(1);

% if isunix
%     nfspath = '/nfs/cmbdata/';
%     moviepath = [nfspath 'TTT/' moviename];
%     if exist(moviepath,'dir') ~= 7
%         moviepath = [nfspath 'schwarzfischer/' moviename];
%     end
% else
% %     moviepath = ['D:\Data\Hemato\Movies\' moviename];
%       moviepath = ['Z:\TTT\' moviename];
% end

%% set constants
% figure(1)
%mean and std for beads (needs to be looked at again)
params.bmean = 16.2772;
params.bstd = 4.8037;
params.lastCentroid = [50 50];
params.currentCell = -1;
params.prevCell = -1;
params.prevSize = -1;
% subsize = numel(trackset.filename);
% if isempty(trackset.filename)
%     error('tracking is empty!')
% end

% if domovie
%     h = figure;
%     set(h,'NextPlot','replacechildren')
%     rect = get(h,'Position');
%     rect(1:2) = [0 0];
%     F = getframe;
%     s = 1;
% end

% propsarray(subsize) = emptyStats();
% imageCache('init',1);
% huhu = [45,58,71,80,81];
% for j=3000:subsize;
%     j = huhu(b);
%     tic
%     fprintf('%d-%d\n',nr,j)
    % load image and position of cell, crop image to 50 by 50 pixels surrounding the tracked cell
%     bffile = trackset.filename{j};
%     if ~strcmp(bffile(1),'/')
%         bffile =['/' bffile];
%     end
%     if numel(bffile) == 0
%         stats = emptyStats();
%         warning('Filename does not exist... skipping');
%         continue
%     else
    
%     splits = strsplit('/',bffile);
%     position = splits{2};
%     filename = splits{3};
%     filename2 = strsplit('.',filename);
%     filename2 = filename2{1};
%     filename3 = strsplit('_',filename2);
%     wl = filename3{end};
    
%     disp(['Cell: ' num2str(trackset.cellNr(j))]);
%     if trackset.cellNr(j) ~= params.currentCell
%         params.currentCell = trackset.cellNr(j);
%         newcell = 1;
%     else
%         newcell = newcell +1;
%     end
   
    x = centerX;
    y = centerY;
   

%     
%     image = imread(strcat(moviepath,'/',position,'/',filename));
% 
%     image = adapthisteq(image);
%     image = wiener2(image);
%     
%     image =  loadimage([moviepath,'/',position,'/',filename],0,wl);
%     image = imgraw;
%     bg = imread([moviepath,'/',position,'/background/',filename(1:end-3) 'png']);
%     bg = double(bg)/(2^16-1);
%     image2 = image./bg;
%     image3 = im2uint8(adapthisteq(image2-min(image2(:))));
   
    image3=OrigImage;
    image4 = wiener2(image3,[5 5]);
    
%I_c is the cropped image
    I_c = cropImage(image4,x,y);
%     I_org = cropImage(image,x,y);
%     I_c = double(I_c(:,:,1));

    
    [xs,ys] = size(I_c);
    if isempty(I_c) || 800/(xs*ys) > 1 || 800/(xs*ys) <= 0
        warning('Empty cropped image, negative coordinates?')
        Center=0;
    end

%     msers = linearMser(imcomplement(image4),15,20,300,1);
    bwimage = zeros(size(image4));
    bwimage(20:30,20:30)=1;
%     for i=1:numel(msers)
%         bwimage(msers{i}) = 1;
%     end
        
    bw = cropImage(bwimage,x,y);
    bw = logical(imfill(bw,'holes'));
    bw = imclearborder(bw);
    bw_org = bw;
    usebw = 0;
    initialstats = regionprops(bw,'BoundingBox');
    
    masks = cell(numel(initialstats),2);
    c0 = 2;
    initialLSF = c0*ones(size(bw));
    offset = 0;
    for i=1:numel(initialstats)
        bb = floor(initialstats(i).BoundingBox);

        if numel(find(bb <=1)) ~= 0
            warning('bounding objects!!!')
        end
        
        if bb(1)-offset < 1
            begin1 = 1;
        else
            begin1 = bb(1)-offset;
        end
        if bb(1)+bb(3)+offset > size(bw,2)
            stop1 = size(bw,2);
        else
            stop1 = bb(1)+bb(3)+offset;
        end
        masks{i,1} = begin1:stop1;
        
        if bb(2)-offset < 1
            begin2 = 1;
        else
            begin2 = bb(2)-offset;
        end
        if bb(2)+bb(4)+offset > size(bw,1)
            stop2 = size(bw,2);
        else
            stop2 = bb(2)+bb(4)+offset;
        end
        masks{i,2} = begin2:stop2;
        initialLSF(masks{i,2},masks{i,1}) = -c0;
    end
    initialLSF = c0*ones(size(bw));
%     initialLSF(45:55,45:55)=-c0;
%     I_c(bw) = max(max(I_c));

    bw = applySnakeFluor(I_c,initialLSF,0);
    bw = ~bw;
    bw = imclearborder(bw);
    Center = bw;
%     L = applyMarkerWatershedding(I_org,bw);
    
    scndtry = 0;
    status = 0;
%     while scndtry <= 2
%         if status == 0
%         [stats,status] = measureStats(I_c,L,params,0);
%         elseif status == 2
%             [L,bw3,ol] = applyMarkerWatershedding(I_org,bw);
%             disp('do different watershedding...')
%             scndtry = scndtry +1;
%         elseif status == 3
%             stats = emptyStats();
%             scndtry = 3;
%         elseif status == 4
%             scndtry = 3;
%             bw(edge(bw)) = 0;
%             [stats,status] = measureStats(I_c,bw,params,1);
%             disp('use bw...')
%             usebw = 1;
%         elseif status == 1
%             scndtry = 3;
%         else
%             error ('no status')
%         end
        
%     if isempty(stats)
%         stats = emptyStats();
%     else

%          if doplot
%             subplot(2,3,1);
%             
%             % axis equal
%             imagesc(imoverlay(I_org,edge(bw_org))),colormap('gray')
%             axis off
%             axis equal
%             title('Raw image')
%             
%             subplot(2,3,2);
%             % figure(3);
%             % axis equal
%             imagesc(bw_org),colormap('gray')
%             axis off
%             axis equal
%             title('B/W image')
%             
%             subplot(2,3,3)
%             imagesc(initialLSF),colormap('gray');
%             axis off
%             axis equal
%             title('initial LSF');
%             
%             subplot(2,3,4);
%             % figure(4);
%             % axis equal
%             imagesc(bw);
%             axis off
%             axis equal
%             title('Level Set algorithm')
%             
%             
%             subplot(2,3,5)
%             imagesc(L)
%             axis off
%             axis equal
%             title('Watershedded image')
%             
%             subplot(2,3,6);
%             %             figure(1);
%             if ~usebw
%                 ol = L;
%                 ol(ol ~= 0) = 1;
%                 ol = ~ol;
%             else
%                 ol = edge(bw);
%             end
%             
%             imagesc(imoverlay(I_org,ol)),colormap('gray')
% %             hold on
% %             plot(stats.Centroid(1),stats.Centroid(2),'r+')
%             xlim([0 100])
%             ylim([0 100])
%             axis off
%             axis equal
%             title('Segmentation Overlay')
%             suptitle(sprintf('Movie: %s Position: %i Timepoint: %i',moviename,nr,j))
%             
%             %             if domovie
%             %                 F(s) = getframe(h,rect);
%             %                 s = s+1;
%             %             end
%             pause(0.01)
%             cla
%          end
        
        
%     end   
%     end
    
%     disp(stats.Centroid)
%     disp(stats.Area)
%     toc
% 
%     end
%     propsarray(j) = stats;
%     params.prevCell = params.currentCell;
%     if isnan(stats.Area)
%         params.lastCentroid = [50 50];
%         params.prevSize = nanmean([propsarray.Area]);
%     else
%         params.lastCentroid = stats.Centroid;
%         params.prevSize = stats.Area;
%     end
    
% end
% if domovie
%     framez = 6;
%     movie2avi(F,sprintf('~/%s_t%i_p%i_f%i_.avi',moviename,nr,j,framez),'fps',8)
% end
% if ~doplot
%  save([savepath 'propsarray_' sprintf('%.6i',nr) '.mat'],'propsarray','-v7.3');
% end
% exit
end

