%%%%%%%% CELL SEGMENTATION %%%%%%%%%%%
% needs: image, x, y and the tracksettings
% gives: detected center as binary image, all detected, the extracted
% subimg

function [Center, All, subimg]=cellSegmentation(imageDetect, x, y, trackset)

% extract subimage
[subimg centerX centerY] = extractSubImage(imageDetect, x, y, trackset.win);
subimg = imadjust(subimg);


% set Threshold
% check for thresholding mode
method=lower(strsplit_qtfy(' ', trackset.threshmethod));
if(strcmp(method(2),'global'))
    ThreshImg=imadjust(imageDetect);
elseif(strcmp(method(2), 'sub'))
    ThreshImg=subimg;
else
    error('Unknown Threshold Image Mode');
end

Threshold = determineThreshold(ThreshImg, trackset.threshmethod)*trackset.threshcorr;

% segmentation
[Center,All] = myseg(subimg, Threshold, trackset,centerX,centerY);
