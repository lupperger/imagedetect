function [found, time] = annotationchange(results,t,channel,cell,field)

if isfield(results{t}.nonFluor(channel),field) && ismember(1,results{t}.nonFluor(channel).(field)(results{t}.nonFluor(channel).cellNr == cell)) && ismember(0,results{t}.nonFluor(channel).(field)(results{t}.nonFluor(channel).cellNr == cell))
    %%% annotation in cell itself
    found = true;
    time = results{t}.nonFluor(channel).timepoint(results{t}.nonFluor(channel).(field) & results{t}.nonFluor(channel).cellNr == cell);
    time = time(1);
elseif isfield(results{t}.nonFluor(channel),field) && ismember(1,results{t}.nonFluor(channel).(field)(results{t}.nonFluor(channel).cellNr == cell)) && ismember(floor(cell/2),results{t}.nonFluor(channel).cellNr)
    
    motherdata = results{t}.nonFluor(channel).(field)(results{t}.nonFluor(channel).cellNr == floor(cell/2));
    if ~motherdata(end)
        %%% annotation change in first timepoint
        found = true;
        time = results{t}.nonFluor(channel).timepoint(results{t}.nonFluor(channel).(field) & results{t}.nonFluor(channel).cellNr == cell);
        time = time(1);
    else
        found = false;
        time = -1;
    end
else
    %%% no annotation change
    found = false;
    time = -1;
end
