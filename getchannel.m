% get quantification channel with (Detection WL, Quantification WL,
% treenum)
function qchan = getchannel(dwl,qwl,t)
global vtset

found= 0;
for q = 1:numel(vtset.results{t}.quants)
    if strcmp(vtset.results{t}.quants(q).settings.wl,qwl)
        if strcmp(vtset.results{t}.detects(vtset.results{t}.quants(q).settings.detectchannel).settings.wl,dwl)
            found=1;
            break
        end
    end
end
if found==0
    qchan = 0;
    
else 
    qchan = q;
end