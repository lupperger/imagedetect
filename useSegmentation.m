function [ wllist ] = useSegmentation( checked, chlist)
%USESEGMENTATION Summary of this function goes here
%   Detailed explanation goes here
global vtset multiFolder

if(checked)
    
 wb = waitbar(0, 'Scanning segmentation files...');
    % get file extension
    [~, ~,ext] = getAnImageFile(vtset.movieFolder,vtset.experiment);
    % check whether a segmentation folder exists 
    if (exist([vtset.movieFolder 'segmentation'],'dir') == 7)
        %generate all files
        filelist = rdir([vtset.movieFolder(1:end-1) '\segmentation\*\*.' ext]);
%         folderlist = strsplit_qtfy(';',genpath([vtset.movieFolder 'segmentation']));
%         matches = regexp(folderlist,'^.*segmentation*.$','match','ignorecase');
%         folderlist = [matches{:}];
        
    % search for subsubfolders including segmentation
    else
        %folderlist = [];
        %generate all subfolders
%         folderlist = [folderlist strsplit_qtfy(';',genpath([vtset.movieFolder]))];
%         matches = regexp(folderlist,'^.*segmentation*.$','match','ignorecase');
%         folderlist = [matches{:}];
        filelist = rdir([vtset.movieFolder(1:end-1) '\*\segmentation\*.' ext]);
        
        multiFolder = true;
        if isempty(filelist)
            warning('No segmentation folder found'); 
        end        
    end
        channels ={};
        %iterate over all folders and pictures 
        
            %iterate over all pictures
            for i = 1: numel(filelist)
               [~, name, ext] = fileparts(filelist(i).name);  
                % cut every picture at the wavelength identifier
                y=strsplit_qtfy('w',[name ext]);
                %mark endings with *
                channels{end +1} =  ['w' y{numel(y)} '*'];
                
                waitbar(i/numel(filelist));
            end
        
        % save only unique endings
        channels=unique(channels);
        % add endings to existing ones
        wllist = chlist;
        wllist = [ wllist; channels'];
        % replace channels
        %set(handles.extDetection,'String',wllist);
        close(wb);
    
else
    % remove all segmentation endings (marked with *)
    wllist = chlist;
     x=strfind(wllist, '*');
    y = find(not(cellfun('isempty', x)));
    wllist(y) = [];
    %set(handles.extDetection,'String',wllist);
    % make extDetection visible 
    %set(handles.extDetection, 'Value', 1);
end

end

