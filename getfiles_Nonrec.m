% searches all subfolders for all files with given extension

function res = getfiles_Nonrec(varargin)

folder = varargin{1};
ext = varargin{2};

res.files={};
res.folders={};
res=addfiles(res,folder,ext);

allpaths = genpath(folder);
if ispc
    allpaths=strsplit_qtfy(';',allpaths);
else
    allpaths=strsplit_qtfy(':',allpaths);
end
for p = 2:numel(allpaths)-1
    res = addfiles(res,allpaths{p},ext);

end


function res=addfiles(res,folder,ext)


files = dir([folder '/*.' ext]);

if numel(files)>0
    res.files = [res.files files.name];
    res.folders = [res.folders repmat({folder},1,numel(files))];
end
