% needs experiment folder
% scans for WL1-9
% creates struct containing wl1-9 with all timepoints and filenames

function ind=createIndex(folder)

subfolders=dir(folder);

for i=1:9
    ind.(['WL' num2str(i)]).filename={};
    ind.(['WL' num2str(i)]).timepoint=[];
end

h=waitbar(0,'Creating index...');
for f=3:numel(subfolders)
    waitbar((f-2)/(numel(subfolders)-2),h)
    if subfolders(f).isdir
        for i=1:9
            files=dir([folder subfolders(f).name '/*w' num2str(i) '*']);
            for file=1:numel(files)
                %sotre filename
                ind.(['WL' num2str(i)]).filename{end+1}=files(file).name;
                
                % store timepoint
                ileft = findstr('t0', files(file).name);
                iright = findstr(['_w' num2str(i)],files(file).name);
                ind.(['WL' num2str(i)]).timepoint(end+1) = str2double(files(file).name(ileft+1:iright-1));
                
                % store position
            end
        end
    end
end
ind.root=folder;
close(h)

        
