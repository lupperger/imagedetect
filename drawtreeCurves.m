%field1 = color
%field2 = width

function drawtreeCurves(param1, param2, cells,channel)

global vtset

data = vtset.results{vtset.treenum}.quants(channel);

treepattern=1;

% find min an max field1
maxuser1 = max(data.int(~isinf(data.int)));
minuser1 = max(eps, min(data.int));

% find min an max field2
maxuser2 = max(data.int(~isinf(data.int)));
minuser2 = max(eps,min(data.int));

% divisor to normalize width
divisor=20;


% iterate over all cells
% cells = unique(data.cellNr)';
if iscolumn(cells)
    cells = cells';
end
figure(vtset.htree);
set(gca,'color',[1 1 1])
hold on;
for cell=cells
    % get index
    index = find(data.cellNr==cell);
    
    if numel(index)>1
        % get path to root
        rp = rootpath(cell);
        % determine x coordinate
        x = 0;
        for i=2:numel(rp)
            horlen = 1/2^(i-1);
            % sort it for new tree pattern view
%             if treepattern
%                 horlen = -horlen;
%             end
            if mod(rp(i),2)==0
                % even number, left branch
                x = x - horlen;
            else
                % odd number, right branch
                x = x + horlen;
            end
        end
        
        % draw the line
        if treepattern
            time = [floor(log2(cell)):0.1:(floor(log2(cell))+1)];
        else
            time = data.(vtset.timemode)(index)/vtset.timemodifier;
        end
        % draw line with widht
        
        alltp= time;%data.(vtset.timemode)(index)/vtset.timemodifier;
        data1=data.int(index);
        data2=data.int(index);
        if treepattern
            temptime= data.(vtset.timemode)(index)/vtset.timemodifier;
            data1= interp1(temptime,data1,linspace(temptime(1),temptime(end),numel(time)));
            data2= interp1(temptime,data2,linspace(temptime(1),temptime(end),numel(time)));
        end
        
        for tp= 2:numel(alltp)
            % calc color from field1
            if strcmp(param1(1),'S')
                try
                mymap = get(vtset.hcurves(vtset.hcurvescells == cell),'Color');
                catch e
                    warndlg('Error: Could not get cell color.\nNo valid timecourse plot for tree view.','TreeView')
                    close(vtset.htree);
                    return;
                end
                if iscell(mymap)
                    mymap = mymap{1};
                end
                cl = 1;
            else 
                cl=max(1,ceil(eps+(data1(tp)-minuser1)/(maxuser1-minuser1)*64));
                mymap=jet;
            end
            
            
            %calc width from field2
            if strcmp(param2(1),'A')
                offset=eps+(data2(tp)-minuser2)/(maxuser2-minuser2);
            elseif treepattern
                offset=2/(2^floor(log2(cell)))/2*divisor;
            else
                offset = 0.2;
            end
            
            %         offset=max(eps,allint(tp)/divisor);
            
            rectangle('Position', [ alltp(tp-1), (x-offset/divisor), max(eps,(alltp(tp)-alltp(tp-1))),max(eps,offset/divisor*2)], 'FaceColor',mymap(cl,:),'LineStyle','none');
        end
        
        % draw line if annotation starts
        allanno=[];%data.wavelength_3(index);
        if ismember(1,allanno) && ismember(0,allanno)
            ap=find(allanno==1);
            ap=ap(1);
            aptp=alltp(ap);
            %         plot_arrow(x-0.05,aptp, x+0.05,aptp,'headheight',0.2,'headwidth',0.3);
            
            %         fig_pos=get(gca,'position');
            %         yp1=(fig_pos(2)+fig_pos(4))*0.98;
            %         yp2=0.98;
            %         xp1=fig_pos(1)+fig_pos(3)*0.5;
            %         annotation('arrow',[xp1 xp1],[yp1 yp2])
            line( [aptp aptp], [x-0.05 x+0.05],'Color', 'black', 'LineWidth', 4);
        end
        % draw line if annotation2 starts
        allanno=[];%data.wavelength_2(index);
        if ismember(1,allanno) && ismember(0,allanno)
            ap=find(allanno==1);
            ap=ap(1);
            aptp=alltp(ap);
            %         plot_arrow(x-0.05,aptp, x+0.05,aptp,'headheight',0.2,'headwidth',0.3);
            
            %         fig_pos=get(gca,'position');
            %         yp1=(fig_pos(2)+fig_pos(4))*0.98;
            %         yp2=0.98;
            %         xp1=fig_pos(1)+fig_pos(3)*0.5;
            %         annotation('arrow',[xp1 xp1],[yp1 yp2])
            line( [aptp aptp],[x-0.05 x+0.05], 'Color', 'red', 'LineWidth', 4);
        end
        
        % and the connection of the children, if there
        c1 = cell*2;
        c2 = cell*2+1;
        if (numel(find(data.cellNr==c1))>0&&numel(find(data.cellNr==c2))>0)
            x1 = x+(1/2^floor(log2(cell)+1));
            x2 = x-(1/2^floor(log2(cell)+1));
            line( [max(time) max(time)],[x1 x2]);
            %         text(x2-0.05,max(time), sprintf('%d',cell*2));
            %         text(x1,max(time), sprintf('%d',cell*2+1));
        end
        
        %%% write probability of decision tree
%         alldata= data.int(index);
%         allsizes = data.size(index);
%         alltimes = data.absoluteTime(index);
        
        infox = x;
        infoy = min(time);%;mean([min(time) max(time)]) - (max(time)-min(time))*0.2;
        % draw number
        
        
        if 0%numel(alldata)>3
            featurevec = getfeatures(alldata,allsizes,alltimes);
            
            [sfitnames nodes]= eval(T,featurevec);
            probs = T.classcount(nodes);
            probs = probs(:,1)./sum(probs,2);
            
            stpReason = unique(data.stopReason(data.cellNr == cell));
            text(infoy-2, infox,sprintf('%2.2f',probs));
        end
        if ~treepattern
            text(infoy-2, infox,sprintf('%d',cell));
        else
            text(infoy, infox,sprintf('%d',cell));
        end
        %     text(infox,infoy-2, sprintf('%d,%d',cell,stpReason));
        %     text(infox,infoy-2, sprintf('%0.3f',mean(data1)));
        
        %     if median(data1(end-3:end)) > median(data1(1:3))*1.99%abs(median(data1(1:3)) -median(data1(end-3:end))) > median(data1(1:3))*1.2% && var(data1)/mean(data1) >0.6
        %         fate = 't';
        %     else
        %         fate = 's';
        %     end
        %     if mean(data1)<4
        %         fate = 'l'; %%% l == 3
        %     end
        % %
        %     text(infox,infoy-2, sprintf('%d,%s',cell,fate));
        % draw information
        %     imagesc(data.(userfield)(index)', 'XData', [infox infox+0.07], 'YData', [infoy infoy+((max(time)-min(time))/1.8/60)]);
        set(gca, 'clim', [minuser1 maxuser1]);
        %        col = [ 1 0 0 ] * (data.(userfield)(index)-minuser)/(maxuser-minuser)  ;
        %    rectangle('Position', [infox, infoy, 0.07, (max(time)-min(time))/1.8/60], 'FaceColor',col);
        
        %
        
    end
end
hold off;
% set(gca,'XDir','reverse');
set(gca,'YTick',[])
grid on
colorbar;
if ~treepattern
    xlabel('Hours');
    xscale = get(get(vtset.hplot,'currentaxes'),'xlim');
    set(get(vtset.htree,'currentaxes'),'xlim',xscale);
else
    xlabel('Generation')
end



