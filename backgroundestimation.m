% iterate over image, detect background via skewness, get stützstellen,
% interpolate -> create synthetic illumination image

function [ZI counter]=backgroundestimation(varargin)

image=varargin{1};

% get params
if numel(varargin)>1
    settings=varargin{2};
    
    binsize=settings.winsize; %20-40
    kurto=settings.kurtosis; %3
    skew = settings.skewness; %0.3
    varmean = settings.dispersion; %2.5e-04
else
    binsize = 20;
    kurto = 3;
    skew = 0.3;
    varmean = 2.5e-04;
end

if numel(varargin)>2
    handles=varargin{3};
end



%init
show=0;
imgsize=size(image);
imgwidth=size(image,2);
imghight=size(image,1);

wh = waitbar(0,'Getting image features...');
try
% varmean =0.0009; %% nan gfp
% varmean = 0.0003; %% venus
if show
    figure;
end
scounter=0;
minpoints = 70000;
points=100000;
while points>minpoints
    counter=0;
    illu= [];
for i = 1:binsize/2:imgsize(1)-binsize
waitbar(i/imgsize(1),wh)
    for j=1:binsize/2:imgsize(2)-binsize
        
        sub = image(i:i+binsize, j:j+binsize);
        
        if  show 
            scounter=scounter+1;
            subplot(10,20,scounter);
%             imagesc(sub)
% patchhist({sub(:)},10)
%                         hist(sub(:),20);%set(gca,'xlim',[0 1])
            %             title(sprintf('%f',max(sub(:))));
%             title(sprintf('i%d,j%d',i,j));
%             title(sprintf('%2.4f',var(sub(:))/mean(sub(:)) ));
%             title('Cells')
            axis off
            colormap(gray(256))
            drawnow;
        end
        if kurtosis(sub(:))<kurto && skewness(sub(:))<skew && var(sub(:))/mean(sub(:)) < varmean %&& ...%mean(sub(:))*1.3 > max(sub(:))  &&...
                %max(sub(:))-min(sub(:)) <0.1% && mean(sub(:))*1.3 > max(sub(:)) 
            counter=counter+1;
            % found BG
            
            if show 
                scounter=scounter+1;
                subplot(10,20,scounter);
                imagesc(sub)
                %             hist(sub(:),20);set(gca,'xlim',[0 1])
                %             title(sprintf('%f',max(sub(:))));
%                 title(sprintf('BG i%d,j%d',i,j));
%                 title(sprintf('%2.4f',skewness(sub(:))));
%                 title('back')
                axis off
                drawnow;
            end
%             x(end+1) = i+binsize/2;
%             y(end+1) = j+binsize/2;
%             z(xcounter,ycounter)= mean(sub(:));
            illu(end+1,:)=[i+binsize/2,j+binsize/2, mean(sub(:))];
%             illu2(i+binsize/2,j+binsize/2)=mean(sub(:));
%         else 
%             fprintf('y %d,x %d\n',i,j);
        end
    end
end
points = counter;
fprintf('Found %d interpoints, optimizing...\n',points);
varmean = varmean*0.8;
end
% toBase(illu2)
fprintf('using %d interpolation points\n',counter)

% update background GUI
if numel(varargin)>2
    set(handles.textStatus,'String',sprintf('Calculating Background... using %d interpolation points ...',counter))
    drawnow;
end
waitbar(1,wh,'Interpolating...')
%%%% check if x and y are interchanged ... -.-
%%%% should be fine now! 29.04.11
y=illu(:,1);
x=illu(:,2);
z=illu(:,3);


%interpolate
[XI YI] = meshgrid(1:imgwidth,1:imghight);
F=TriScatteredInterp(x,y,z,'natural');
ZI=F(XI,YI);

%extrapolate
for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
    ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
end

for i=1:imgwidth
    ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
end

% repair strange extrapolations
ZI(ZI<min(image(:)))=min(image(:));
ZI(ZI>max(image(:)))=max(image(:));
close(wh);

catch e
    delete(wh)
    rethrow(e)
end

function dummy
%%
% img = image2;

tic
imgwidth=size(img,2);
imghight=size(img,1);

settings.winsize = 24;
settings.kurtosis = 3;
settings.skewness = 0.3;
settings.varmean = 13e-04;

[y x z] = backgroundestimation(img,settings);
toc
tic
[XI YI] = meshgrid(1:imgwidth,1:imghight);
% ZI=griddata(x',y',z',XI,YI,'v4', {'Qt','Qbb','Qc' ,'Qs'});
ZI=griddata(x',y',z',XI,YI,'v4');
toc

tic
for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
    ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
end

for i=1:imgwidth
    ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
end

ZI(ZI<min(img(:)))=min(img(:));
ZI(ZI>max(img(:)))=max(img(:));

toc

%% new version, try to speed up

imgwidth=size(img,2);
imghight=size(img,1);

settings.winsize = 28;
settings.kurtosis = 3;
settings.skewness = 0.3;
settings.varmean = 2.5e-04;

ZI = backgroundestimation(img,settings);

% tic
% [XI YI] = meshgrid(1:imgwidth,1:imghight);
% % [X Y] = meshgrid(x,y);
% F=TriScatteredInterp(x,y,z,'natural');
% ZI=F(XI,YI);
% % ZI = interp2(X,Y,z,XI,YI);
% toc
% 
% tic
% for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
%     ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
% end
% 
% for i=1:imgwidth
%     ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
% end
% 
% ZI(ZI<min(img(:)))=min(img(:));
% ZI(ZI>max(img(:)))=max(img(:));
% toc

%% interp2

[y x z] = backgroundestimation(img,40);
z(z==0)=[];
[X Y] = meshgrid(x,y);
[XI YI] = meshgrid(1:imgwidth,1:imghight);
%%
ZI = interp2(X,Y,z,XI,YI);


%%
% mesh(XI,YI,ZI/max(ZI(:)))
figure;
imagesc(ZI/max(ZI(:)))

%% test some graythreshing
gt=graythresh(sub)*1.2;
imbw=im2bw((sub),gt);
% imbw=imfill(imbw,'holes');
[dummy l]=bwlabel(imbw);
l





