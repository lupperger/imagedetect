function ret = isStructEqual(struct1, struct2)
%ISSTRUCTEQUAL Check if two structures are structurally equal, that is
%   they have strictly the same number of fields by the same name and of
%   the same class at each level. The actual values of the fields are not
%   checked and do not affect the return value of this function.
%
%   Return 1 if the two structures are structurally identical, 0 otherwise

%   $Revision: 1.1.6.1 $
%   $Date: 2009/04/27 19:47:21 $
%   Copyright 2008-2009 The MathWorks, Inc.

% prime the machinery
fNames1 = fieldnames(struct1);
fNames2 = fieldnames(struct2);

% each level of the structs must contains the same fields, no more no fewer
if ~isempty(setdiff(fNames1, fNames2)) || ~isempty(setdiff(fNames2, fNames1))
    ret = 0;
    return
end

% fields must match in name and in type (class)
for i = 1:length(fNames1)
    try
        var1 = getfield(struct1, fNames1{i}); %#ok<GFLD>
        var2 = getfield(struct2, fNames1{i}); %#ok<GFLD>
    catch %#ok<CTCH>
        ret = 0;
        return
    end
    
    class1 = class(var1);
    class2 = class(var2);
    if ~isequal(class1, class2)
        ret = 0;
        return
        
    elseif isstruct(var1)
        % traverse through substructs using recursion
        if ~isStructEqual(var1, var2)
            ret = 0;
            return
        end
    end
end

ret = 1;

