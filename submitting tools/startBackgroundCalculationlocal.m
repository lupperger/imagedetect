function varargout = startBackgroundCalculationlocal(varargin)
% STARTBACKGROUNDCALCULATIONLOCAL MATLAB code for startBackgroundCalculationlocal.fig
%      STARTBACKGROUNDCALCULATIONLOCAL, by itself, creates a new STARTBACKGROUNDCALCULATIONLOCAL or raises the existing
%      singleton*.
%
%      H = STARTBACKGROUNDCALCULATIONLOCAL returns the handle to a new STARTBACKGROUNDCALCULATIONLOCAL or the handle to
%      the existing singleton*.
%
%      STARTBACKGROUNDCALCULATIONLOCAL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STARTBACKGROUNDCALCULATIONLOCAL.M with the given input arguments.
%
%      STARTBACKGROUNDCALCULATIONLOCAL('Property','Value',...) creates a new STARTBACKGROUNDCALCULATIONLOCAL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before startBackgroundCalculationlocal_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to startBackgroundCalculationlocal_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help startBackgroundCalculationlocal

% Last Modified by GUIDE v2.5 03-Nov-2013 15:33:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @startBackgroundCalculationlocal_OpeningFcn, ...
                   'gui_OutputFcn',  @startBackgroundCalculationlocal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before startBackgroundCalculationlocal is made visible.
function startBackgroundCalculationlocal_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to startBackgroundCalculationlocal (see VARARGIN)

% Choose default command line output for startBackgroundCalculationlocal
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
ButtonSelect_Callback([],[],handles)
% UIWAIT makes startBackgroundCalculationlocal wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = startBackgroundCalculationlocal_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editExperiment_Callback(hObject, eventdata, handles)
% hObject    handle to editExperiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editExperiment as text
%        str2double(get(hObject,'String')) returns contents of editExperiment as a double


% --- Executes during object creation, after setting all properties.
function editExperiment_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editExperiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editWavelength_Callback(hObject, eventdata, handles)
% hObject    handle to editWavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editWavelength as text
%        str2double(get(hObject,'String')) returns contents of editWavelength as a double


% --- Executes during object creation, after setting all properties.
function editWavelength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editWavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editFileType_Callback(hObject, eventdata, handles)
% hObject    handle to editFileType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFileType as text
%        str2double(get(hObject,'String')) returns contents of editFileType as a double


% --- Executes during object creation, after setting all properties.
function editFileType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFileType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editEmailAdress_Callback(hObject, eventdata, handles)
% hObject    handle to editEmailAdress (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editEmailAdress as text
%        str2double(get(hObject,'String')) returns contents of editEmailAdress as a double


% --- Executes during object creation, after setting all properties.
function editEmailAdress_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editEmailAdress (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonSubmit.
function ButtonSubmit_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSubmit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bgcalc
path = bgcalc.path;
% get params and check them

wavelength = get(handles.listboxRight,'String');
if isempty(wavelength)
    errordlg('No wavelength to calculate');
    return
end

experiment = get(handles.editExperiment,'String');
experiment = strsplit('\',experiment);
experiment=experiment{end};


dogain = get(handles.checkboxCalculateGain,'Value');
wh = waitbar(0,'Starting calculation...');

% iterate over wavelengths
for wl = wavelength'
    w = strsplit('.',cell2mat(wl));
    % iterate over positions
    allpos = dir(path);
    for f = 3:numel(allpos)
        if allpos(f).isdir
            try
                fullpath =[path '/' allpos(f).name];
                waitbar(0,wh,sprintf('Position %s, wavelength %s',allpos(f).name,w{1}))
                calculateBackgroundComplete(path,fullpath,w{1},w{2},num2str(dogain))
            catch e
                errordlg(sprintf('Error calculating wavelength %s, extension %s in folder %s',w{1},w{2},fullpath));
                close(wh)
                rethrow(e)
%                 return
            end
        end
    end
    
end


close(wh);



% --- Executes on button press in ButtonExit.
function ButtonExit_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1)


% --- Executes on button press in checkboxCalculateGain.
function checkboxCalculateGain_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxCalculateGain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxCalculateGain


% --- Executes on selection change in listboxFound.
function listboxFound_Callback(hObject, eventdata, handles)
% hObject    handle to listboxFound (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listboxFound contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxFound


% --- Executes during object creation, after setting all properties.
function listboxFound_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxFound (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonToLeft.
function ButtonToLeft_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonToLeft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
listright = get(handles.listboxRight,'String');
idright = get(handles.listboxRight,'Value');

listleft = get(handles.listboxFound,'String');
idleft = get(handles.listboxFound,'Value');

if isempty(listright)
    return
end

% copy to left
listleft{end+1} = listright{idright};
listleft = sort(listleft);
listright(idright)=[];

% update list
set(handles.listboxRight,'String',listright)
set(handles.listboxFound,'String',listleft)
set(handles.listboxFound,'Value',1)
set(handles.listboxRight,'Value',min(numel(listright),get(handles.listboxRight,'Value')))

% --- Executes on button press in ButtonToRight.
function ButtonToRight_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonToRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
listright = get(handles.listboxRight,'String');
idright = get(handles.listboxRight,'Value');

listleft = get(handles.listboxFound,'String');
idleft = get(handles.listboxFound,'Value');

if isempty(listleft)
    return
end

% copy to right
listright{end+1} = listleft{idleft};
listright = sort(listright);
listleft(idleft)=[];

% update list
set(handles.listboxRight,'String',listright)
set(handles.listboxFound,'String',listleft)
set(handles.listboxRight,'Value',1)
set(handles.listboxFound,'Value',min(numel(listleft),get(handles.listboxFound,'Value')))

% --- Executes on selection change in listboxRight.
function listboxRight_Callback(hObject, eventdata, handles)
% hObject    handle to listboxRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listboxRight contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxRight


% --- Executes during object creation, after setting all properties.
function listboxRight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonSelect.
function ButtonSelect_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bgcalc
lastdir = get(handles.editExperiment,'String');

path=uigetdir(lastdir,'Select Experiment directory');
if path == 0
    return
end

if exist(path,'dir')
    set(handles.editExperiment,'String',path)
    getWavelengths(path,handles);
    bgcalc.path = path;
end


function getWavelengths(path,handles)

% read xml

d = dir([path '/*.xml']);
if isempty(d)
    errordlg('XML not found')
    return
end
xmlfilename = d(end).name;


xmlfile = fileread([path '/' xmlfilename]);
xmlstr =  xml_parseany(xmlfile); 
wllist={};
list = xmlstr.WavelengthData{1}.WavelengthInformation;
for k = 1:size(list,2)
    wllist{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name) '.' char(list{k}.WLInfo{1}.ATTRIBUTE.ImageType)];
end
% set left listbox

set(handles.listboxFound,'String',wllist)
set(handles.listboxFound,'Value',1)
% clear right listbox
set(handles.listboxRight,'String',{})
set(handles.listboxRight,'Value',1)
