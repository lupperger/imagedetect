#!/bin/bash 

user=$1

while [ 1 = 1 ]
do
  o=`qstat -u cmbscd | wc -c`
  if [ $o -eq 0 ]
  then
    break;
  fi
  sleep 300
done

# done, send message
echo "Queue done!"
echo "Please copy back your experiments"
echo "$2" | sendemail -s outmail.helmholtz-muenchen.de -f "$user@helmholtz-muenchen.de" -t "$user@helmholtz-muenchen.de" -u "Queue done `date`"


