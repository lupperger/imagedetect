function results=mapmissingperimeter(results)
% global vtset


wh = waitbar(0,'Calculating missing perimeter');


for q=1:numel(results.detects)
    results.detects(q).perimeter=[];
    for c = 1:numel(results.detects(q).cellNr)
        waitbar(c/numel(results.detects(q).cellNr),wh)
        try
        results.detects(q).perimeter(c) = sum(sum(bwperim(results.detects(q).cellmask{c})));
        catch
            results.detects(q).perimeter(c) = -1;
        end
    end
end

close(wh)