function changedetectionchannel(tree,channelIndex,cells,range,newchannel)
global vtset

% restrict cells/range
if strcmp(cells,'all')
    cells = unique(vtset.results{tree}.quants(channelIndex).cellNr);
end
if strcmp(range,'complete')
    range = unique(vtset.results{tree}.quants(channelIndex).timepoint);
end

indizes = ismember(vtset.results{tree}.quants(channelIndex).cellNr,cells) & ismember(vtset.results{tree}.quants(channelIndex).timepoint,range);


vtset.results{tree}.quants(channelIndex).detectchannel(indizes) = newchannel;

wh=waitbar(0,'Quantifying cells on new detection channel...');
quantifyCells(channelIndex,tree,cells,range);
delete(wh);