function [bw,finalLSF] = applySnakeFluor(Img,initialLSF,doplot)

Img = double(Img(:,:,1));
%% parameter setting
timestep=5;  % time step
mu=0.2/timestep;  % coefficient of the distance regularization term R(phi)
iter_inner=5;
iter_outer=30;
lambda=34.5; % coefficient of the weighted length term L(phi)
alfa=1;  % coefficient of the weighted area term A(phi)
epsilon=3; % papramater that specifies the width of the DiracDelta function

sigma=1;     % scale parameter in Gaussian kernel
G=fspecial('gaussian',15,sigma);
Img_smooth=conv2(Img,G,'same');  % smooth image by Gaussiin convolution
[Ix,Iy]=gradient(Img_smooth);
f=Ix.^2+Iy.^2;
g=1./(1+f);  % edge indicator function.

% initialize LSF as binary step function

% initialLSF(10:40, 10:40)=-c0;  

phi=initialLSF;

if doplot
%     figure(1);
%     mesh(-phi);   % for a better view, the LSF is displayed upside down
%     hold on;  contour(phi, [0,0], 'r','LineWidth',2);
%     title('Initial level set function');
%     view([-80 35]);

    figure(2);
    imagesc(Img,[0, 255]); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
    title('Initial zero level contour');
%     
%     figure(3);
%     imagesc(Img),colormap('gray')
%     hold on
%     v = 1:100;
%     quiver(v,v,Ix,Iy);
    
    
end

potential=2;  
if potential ==1
    potentialFunction = 'single-well';  % use single well potential p1(s)=0.5*(s-1)^2, which is good for region-based model 
elseif potential == 2
    potentialFunction = 'double-well';  % use double-well potential in Eq. (16), which is good for both edge and region based models
else
    potentialFunction = 'double-well';  % default choice of potential function
end

% start level set evolution

for n=1:iter_outer
    phi = drlse_edge(phi, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);
    if mod(n,2)==0 && doplot
        figure(2);
        imagesc(Img,[0, 255]); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
       pause(0.5)
    end
end
% pause(1);
% refine the zero level contour by further level set evolution with alfa=0
alfa=0;
iter_refine = 10;
phi = drlse_edge(phi, g, lambda, mu, alfa, epsilon, timestep, iter_inner, potentialFunction);

finalLSF=phi;
bw = im2bw(finalLSF,graythresh(finalLSF));
if doplot
    figure(2);
    imagesc(Img,[0, 255]); axis off; axis equal; colormap(gray); hold on;  contour(phi, [0,0], 'r');
    hold on;  contour(phi, [0,0], 'r');
    str=['Final zero level contour, ', num2str(iter_outer*iter_inner+iter_refine), ' iterations'];
    title(str);


%     pause(1);
%     figure;
%     mesh(-finalLSF); % for a better view, the LSF is displayed upside down
%     hold on;  contour(phi, [0,0], 'r','LineWidth',2);
%     str=['Final level set function, ', num2str(iter_outer*iter_inner+iter_refine), ' iterations'];
%     title(str);
%     axis on;
end


