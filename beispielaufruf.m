%% create trackset structure

trackset.win = 51;
trackset.threshcorr = 1;
trackset.smooth = 4;
trackset.max = 4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest = 1;
trackset.invert = 0;
trackset.threshmethod='Otsu global';
trackset.detectionChannel = 0;


%% load image

% img = imread('/home/ibis/schwarzfischer/data/100104PH4_p002/100104PH4_p002_t00132_w1.tif');

%convert to double:
img = double(img)/255;

% show  it
figure;
imagesc(img)


%% detect some cells:


x = 420;
y = 246;

% perform segmentation:
[Center, All, subimg]=cellSegmentation(img, x, y, trackset);

% show detected cell:
figure;
imagesc(Center)

%% show subimg with outlines:

% get outlines (logical matrix)
BWoutline = bwperim(Center);

% copy subimg
Segout = subimg;

% set outline pixel to a higher  value than maximum of image (via indexing)
Segout(BWoutline) = max(subimg(:))*1.2;

% show it
figure;
imagesc(Segout);
axis off
axis equal

% set to grayscale
colormap(gray)

%%%% calculate intensity
int = sum(subimg(Center))

