
% structs have to contain same fields
function res=mergeStructs(res, s2)

for f=fields(s2)'
    if strcmp(f,'treelist')
        res.treelist.treefile(end+1:end+numel(s2.(str2mat(f)).treefile)) = s2.(str2mat(f)).treefile;
        res.treelist.root(end+1:end+numel(s2.(str2mat(f)).root)) = s2.(str2mat(f)).root;
        
    elseif ~strcmp(f,'settings')
        res.(str2mat(f))(end+1:end+numel(s2.(str2mat(f)))) = s2.(str2mat(f));

    end
end