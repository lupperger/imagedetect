function filereader_csv(res,tree)

global vtset

%% first gather all detects
for m = 1:numel(vtset.results{tree}.detects)
    gatherimages_csv(res,tree,'D',m)
end

%% check if same wl as in detects --> copy, else gather quants
for m= 1:numel(vtset.results{tree}.quants)
        gatherimages_csv(res,tree,'Q',m)
end

%% same with nonFluor --> check if same wl as in detects or quants
for m= 1:numel(vtset.results{tree}.nonFluor)
        gatherimages_csv(res,tree,'n',m)

end
