function rescaletoProteinNumbers
global vtset

% ask for channel

channels = {};
for x = 1:numel(vtset.results{vtset.treenum}.quants)
    channels{end+1} = ['Q: ' vtset.results{vtset.treenum}.quants(x).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(x).settings.detectchannel).settings.wl];
end

[Selection,ok] = listdlg('ListString',channels);

if ~ok
    return
end

% ask for factor

answer = inputdlg('Enter protein scaling factor','Scaling Factor');
if isempty(answer)
    return
end

factor = str2double(answer);

vtset.results{vtset.treenum}.quants(Selection).int = vtset.results{vtset.treenum}.quants(Selection).int./factor;

vtset.functions.lstTrees_Callback();
