function varargout = CellCoordGUI(varargin)
% CELLCOORDGUI M-file for CellCoordGUI.fig
%      CELLCOORDGUI, by itself, creates a new CELLCOORDGUI or raises the existing
%      singleton*.
%
%      H = CELLCOORDGUI returns the handle to a new CELLCOORDGUI or the handle to
%      the existing singleton*.
%
%      CELLCOORDGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLCOORDGUI.M with the given input arguments.
%
%      CELLCOORDGUI('Property','Value',...) creates a new CELLCOORDGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellCoordGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellCoordGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellCoordGUI

% Last Modified by GUIDE v2.5 01-Feb-2012 09:43:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CellCoordGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CellCoordGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellCoordGUI is made visible.
function CellCoordGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellCoordGUI (see VARARGIN)

global cellcoord siq

%init

if numel(varargin)>1
    cellcoord.im=varargin{1};
    cellcoord.tracking_coords=varargin{2};
%     set(handles.Button_SelectROI,'
else
    cellcoord.im=[];
    cellcoord.tracking_coords=[];
end
if numel(varargin)>2
    cellcoord.clims1=varargin{3};
    cellcoord.clims2=varargin{4};
else
    
    cellcoord.clims1=0;
    cellcoord.clims2=1;
end
cellcoord.clims1 = max(cellcoord.clims1,-1);
cellcoord.clims2 = min(cellcoord.clims2,1);

cellcoord.forceAutoSeg=0;
cellcoord.draw=@draw;
cellcoord.redraw=@redraw;
cellcoord.figurehandle = handles.figure1;
try 
    cellcoord.impoly.delete 
catch e 
    %nevermind
end

% set mousebutton callbacks
set(handles.figure1,'windowbuttonmotionfcn',@wbmcb);
% set(gcf,'WindowButtonUpFcn',@wbucb);
set(handles.figure1,'WindowButtonDownFcn',@wbdcb);
set(handles.figure1,'KeyPressFcn',@mykeyFunction);

% Choose default command line output for CellCoordGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

if numel(varargin)>1
    draw
end

if isempty(siq.autodetect)
    if siq.automode && ~siq.manual
        cellcoord.forceAutoSeg=1;
    end
    if ~siq.manual
    ButtonAuto_Callback([],[],handles);
    end
end
% uiwait(handles.figure1);



% get mouse position
function wbmcb(src,evnt)
global cellcoord
handles = guidata(src);
act= get(handles.image1,'CurrentPoint');
cellcoord.cursor=act(1,1:2);

% draw crosshair
try 
    delete(cellcoord.linehandle(1))
    delete(cellcoord.linehandle(2))
catch e
    %never mind
end
if (sum(cellcoord.cursor(1)<size(cellcoord.im,2)  & cellcoord.cursor(2)<size(cellcoord.im,1) & cellcoord.cursor>0)==2)
    cellcoord.linehandle(1) = line([1 size(cellcoord.im,2)],[cellcoord.cursor(2) cellcoord.cursor(2)],'color','black','linestyle','--','parent',get(cellcoord.figurehandle,'CurrentAxes'));
    cellcoord.linehandle(2) = line([cellcoord.cursor(1) cellcoord.cursor(1)],[1 size(cellcoord.im,1)],'color','black','linestyle','--','parent',get(cellcoord.figurehandle,'CurrentAxes'));
end

% mouse button down
function wbdcb(src,evnt)
global cellcoord siq
handles = guidata(src);

% only if innerhalb vom image
if ~(sum(cellcoord.cursor(1)<size(cellcoord.im,2)  & cellcoord.cursor(2)<size(cellcoord.im,1) & cellcoord.cursor>0)==2)
    return
end

% store coordinates
cellcoord.tracking_coords(end+1,:)=cellcoord.cursor(1,1:2);
siq.tracking_coords(end+1,:)=cellcoord.cursor(1,1:2);
% track_real(index,:)=cpa(1,1:2);
siq.manual = 1;
fprintf('.');

% plot point and redraw
redraw

% keyboard input
function mykeyFunction(src, evnt)
handles = guidata(src);
k= evnt.Key; %k is the key that is pressed
 
if strcmp(k,'backspace') %if enter was pressed
    pause(0.01) %allows time to update
    undo_pushbutton_Callback([],[],handles)
end

% --- Outputs from this function are returned to the command line.
function varargout = CellCoordGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
global cellcoord

varargout{1} = cellcoord.tracking_coords;


% --- Executes on button press in Button_SelectROI.
function Button_SelectROI_Callback(hObject, eventdata, handles)
% hObject    handle to Button_SelectROI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global cellcoord siq

%delete mouse callback
set(handles.figure1,'windowbuttonmotionfcn','');
set(handles.figure1,'WindowButtonDownFcn','');

if isfield(cellcoord,'impoly') && numel(cellcoord.impoly)
    
    cellcoord.impoly.delete;
end

cellcoord.impoly = impoly;

coords=cellcoord.tracking_coords;
% select coords in ROI
if isfield(cellcoord,'impoly') && cellcoord.impoly.isvalid
    xiyi = cellcoord.impoly.getPosition;
    in = inpolygon(coords(:,1),coords(:,2),xiyi(:,1),xiyi(:,2));
    coords= coords(in,:);
end

set(handles.text_numberofcells,'String',sprintf('Number of Cells: %d',size(coords,1)))
siq.tracking_coords = coords;


% set mousebutton callbacks
set(handles.figure1,'windowbuttonmotionfcn',@wbmcb);
% set(gcf,'WindowButtonUpFcn',@wbucb);
set(handles.figure1,'WindowButtonDownFcn',@wbdcb);
set(handles.figure1,'KeyPressFcn',@mykeyFunction);


% --- Executes on button press in undo_pushbutton.
function undo_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to undo_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global cellcoord
% delete last track and re-plot 

if numel(cellcoord.tracking_coords)
    cellcoord.tracking_coords(end,:) = [];
end
redraw

function draw
global cellcoord
handles=guidata(cellcoord.figurehandle);
cellcoord.axeshandle=handles.image1;

axes(handles.image1)
colormap(gray)
cla
imagesc(cellcoord.im,[cellcoord.clims1 cellcoord.clims2]);
axis off
axis equal
hold on
redraw

function redraw
global cellcoord siq
handles=guidata(cellcoord.figurehandle);
% cellcoord.axeshandle=subplot(handles.('image1'));
% 
% axes(handles.image1)
% cla
% imagesc(cellcoord.im,[cellcoord.clims1 cellcoord.clims2]);
% axis off
% axis equal
% hold on
try
    % delete dots
    delete(cellcoord.hcurves)
catch e
    %nevermind
end

if isfield(siq,'autodetect') && numel(siq.autodetect)
%     hold off
    cla
    temp = (siq.normDivided-cellcoord.clims1)./(cellcoord.clims2 -cellcoord.clims1);
    temp(temp<0)=0;
    temp(temp>1)=1;
    temp2=temp;
    temp2(siq.autodetect)=0;
    test(:,:,1) = temp2;
    test(:,:,2) = temp2;
    temp(siq.autodetect)=1;
    test(:,:,3)= temp;
    imagesc(test,[cellcoord.clims1 cellcoord.clims2])
%     hold on
end
% else
    if numel(cellcoord.tracking_coords)
        cellcoord.hcurves=plot(cellcoord.axeshandle,cellcoord.tracking_coords(:,1),cellcoord.tracking_coords(:,2),'k.','LineWidth',2,'color','red');
    end
% end

set(handles.text_numberofcells,'String',sprintf('Number of Cells: %d',size(cellcoord.tracking_coords,1)))


% if isfield(cellcoord,'impoly') && numel(cellcoord.impoly)
%     impoly(gca,cellcoord.impoly.getPosition)
% end


% --- Executes on button press in set_pushbutton.
function set_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to set_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global adjC cellcoord

adjC.close=0;

adjustContrast(cellcoord.clims1,cellcoord.clims2,'cellcoord','cellcoord.draw()','cellcoord.clims1','cellcoord.clims2',cellcoord.im)
    


% --- Executes on button press in reset_pushbutton.
function reset_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to reset_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global cellcoord siq
if isfield(siq,'autodetect')
    siq.autodetect = [];
end
cellcoord.tracking_coords=[];
siq.tracking_coords=[];
draw


% --- Executes on button press in ok_pushbutton.
function ok_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to ok_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq cellcoord
if isfield(siq,'gimme')
    %send to quantify
    coords=cellcoord.tracking_coords;
    % select coords in ROI
    if isfield(cellcoord,'impoly') && cellcoord.impoly.isvalid
        xiyi = cellcoord.impoly.getPosition;
        in = inpolygon(coords(:,1),coords(:,2),xiyi(:,1),xiyi(:,2));
        coords= coords(in,:);
    end

    
    siq.tracking_coords = coords;
    figure(siq.figurehandle)
    siq.quantify()
else
    % close
    delete(handles.figure1)
end


% --- Executes on button press in ButtonAuto.
function ButtonAuto_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonAuto (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq cellcoord

if ~isfield(siq,'mseroptions')
    siq.mseroptions.maxVariation = 1;
    siq.mseroptions.minSize = 15;
    siq.mseroptions.maxSize = 300;
    siq.mseroptions.delta = 1;
    siq.mseroptions.invert=0;
end
if ~cellcoord.forceAutoSeg
    mseroptions = MserSettings(siq.mseroptions);
else
    mseroptions = siq.mseroptions;
    cellcoord.forceAutoSeg=0;
end
if ~numel(mseroptions)
    return
end

siq.mseroptions = mseroptions;

set(handles.text_numberofcells,'String',sprintf('Segmenting...'))
drawnow

[mserBW,mserProps] = mserSegmentation(siq.normDivided,mseroptions);

%%% only for FACS vs IMAGING
% mserBW = siq.seg;
% stats=regionprops( bwconncomp(mserBW,4), 'Centroid');
% coords = [stats(:).Centroid];
% 
% mserProps = [coords(1:2:end);coords(2:2:end)]';
%%% only for FACS vs IMAGING


% [mserBW,mserProps] = myseg_doublemser(siq.normDivided,mseroptions);

% [drlseBW,drlseProps] = drlseSegmentation(image,bw);

% [ws,bw_ws] = completeWatershedding(image,drlseBW);

siq.autodetect = mserBW>=1;
siq.manual = 0;
cellcoord.tracking_coords = mserProps;
siq.tracking_coords = cellcoord.tracking_coords;
draw
