function [gain offset]=gainCache(file,size)
global gaincache

if strcmp(file,'init')
    % initialize
    gaincache=[];
    gaincache.size = size;
    gaincache.files = {};
    gaincache.gain={};
    gaincache.offset={};
    gaincache.next=1;
else
    % search for it
    pos=find(strcmp(gaincache.files,file));
    if numel(pos)==0
        % new one, load it
        if exist(file,'file')
            fprintf('Loading gainCache %s ',file)
            load(file,'-mat')
            
            gaincache.gain{gaincache.next}=gain;
            gaincache.offset{gaincache.next}=offset;
        else
            msgbox(sprintf('File not found %s',file),'File not found','error');
            error('File not found %s',file);
        end
        gain=gaincache.gain{gaincache.next};
        offset=gaincache.offset{gaincache.next};
        gaincache.files{gaincache.next}=file;
%         fprintf('Cache - from disk: %s\n', file);
        % increase pointer
        gaincache.next=gaincache.next+1;
        if gaincache.next>gaincache.size
            gaincache.next=1;
        end
    else
        % existing one
        fprintf('Cache - from mem:  %s\n', file);
        gain=gaincache.gain{pos};
        offset=gaincache.offset{pos};
    end
end
