%% check two channels if they have same data points

ch1 = 1;
ch2 = 4;

tree= 1;
found2= zeros(numel(vtset.results{tree}.quants(ch2).cellNr),1);
found1= zeros(numel(vtset.results{tree}.quants(ch1).cellNr),1);
for id = 1:numel(vtset.results{tree}.quants(ch1).cellNr)
    c = vtset.results{tree}.quants(ch1).cellNr(id);
    tp = vtset.results{tree}.quants(ch1).timepoint(id);
    
    id2 = find(vtset.results{tree}.quants(ch2).cellNr == c & vtset.results{tree}.quants(ch2).timepoint == tp);
    
    if numel(id2)
        found1(id) = 1;
        found2(id2) = 1;
    end
end
    
%% delete all ids

vtset.results{tree}.quants(ch2).cellNr(~found2) = [];
vtset.results{tree}.quants(ch2).timepoint(~found2) = [];
vtset.results{tree}.quants(ch2).absoluteTime(~found2) = [];
vtset.results{tree}.quants(ch2).positionIndex(~found2) = [];
vtset.results{tree}.quants(ch2).filename(~found2) = [];
vtset.results{tree}.quants(ch2).int(~found2) = [];
vtset.results{tree}.quants(ch2).active(~found2) = [];
vtset.results{tree}.quants(ch2).detectchannel(~found2) = [];


%% delete everything which is not in NonFluor

tree=1;
for q = 1:numel(vtset.results{tree}.quants)
    delid = [];
    for id = 1:numel(vtset.results{tree}.quants(q).cellNr)
        c = vtset.results{tree}.quants(q).cellNr(id);
        tp = vtset.results{tree}.quants(q).timepoint(id);
        
        f = find(vtset.results{tree}.nonFluor.cellNr == c&vtset.results{tree}.nonFluor.timepoint == tp);
        
        if ~numel(f)
            delid(end+1) = id;
        end
    end
    if numel(delid)
        for field=fields(vtset.results{tree}.quants(q))'
            if ~strcmp(field,'settings')
                vtset.results{tree}.quants(q).(cell2mat(field))(delid)=[];
            end
        end
    end
end
        
    