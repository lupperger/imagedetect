function varargout = adjustContrast(varargin)
% ADJUSTCONTRAST MATLAB code for adjustContrast.fig
%      ADJUSTCONTRAST, by itself, creates a new ADJUSTCONTRAST or raises the existing
%      singleton*.
%
%      H = ADJUSTCONTRAST returns the handle to a new ADJUSTCONTRAST or the handle to
%      the existing singleton*.
%
%      ADJUSTCONTRAST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADJUSTCONTRAST.M with the given input arguments.
%
%      ADJUSTCONTRAST('Property','Value',...) creates a new ADJUSTCONTRAST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before adjustContrast_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to adjustContrast_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help adjustContrast

% Last Modified by GUIDE v2.5 11-Feb-2013 10:42:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @adjustContrast_OpeningFcn, ...
                   'gui_OutputFcn',  @adjustContrast_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before adjustContrast is made visible.
function adjustContrast_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to adjustContrast (see VARARGIN)
global adjC
% Choose default command line output for adjustContrast
handles.output = hObject;

if varargin{1} == varargin{2}
    msgbox(sprintf('Contrast minimum %0.2f and maximum %0.2f are equal',varargin{1},varargin{2}))
    close(handles.figure1)
    return
   
end

adjC.temp.min=varargin{1};
adjC.temp.max=varargin{2};

adjC.min=varargin{1};
adjC.max=varargin{2};

% get the things to update:
adjC.update.global = varargin{3};
adjC.update.execute = varargin{4};
adjC.update.min = varargin{5};
adjC.update.max = varargin{6};
if numel(varargin)>6
    adjC.img=varargin{7};
end
adjC.close = 0;

adjC.pressed=0;

redraw(handles);
set(handles.figure1,'WindowButtonDownFcn',@wbdcb);
set(handles.figure1,'WindowButtonUpFcn',@wbucb);
set(handles.figure1,'windowbuttonmotionfcn',@wbmcb1);


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes adjustContrast wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function wbmcb1(src,evnt)
global adjC
handles = guidata(src);
act= get(handles.axes1,'CurrentPoint');
adjC.cursor=act(1,1:2);
% only innerhalb pressed
if ~(sum(adjC.cursor(1)<1  & adjC.cursor(2)<1 & adjC.cursor>0)==2)
    adjC.pressed=0;
end

function wbucb(src,evnt)
global adjC
handles = guidata(src);
eval(sprintf('global %s',adjC.update.global));
adjC.pressed=0;
eval(adjC.update.execute);
uicontrol(handles.ButtonOk)


function wbdcb(src,evnt)
global adjC
adjC.pressed=1;
handles = guidata(src);
if (sum(adjC.cursor(1)<1  & adjC.cursor(2)<1 & adjC.cursor>min(get(handles.axes1,'xlim')))==2)
    mousebutton = get(gcbf, 'SelectionType');
    
    if strcmp(mousebutton(1),'n')
        % left click
        while adjC.pressed
            if adjC.cursor(1)<adjC.max
                adjC.min = adjC.cursor(1);
                redraw(handles)
                % update
            end
            pause(0.1)
        end
    elseif strcmp(mousebutton(1),'a')
        %right click
        while adjC.pressed
            if adjC.cursor(1)>adjC.min
                adjC.max = min(adjC.cursor(1),1);
                redraw(handles)
                
            end
            pause(0.1)
        end
    else
        %some strange weird matlab double click fuckup
    end
end

function redraw(handles)
global adjC 
eval(sprintf('global %s;',adjC.update.global));
cla
subplot(handles.('axes1'));
% hist(adjC.img(:),linspace(0,1,255))
line([adjC.min adjC.min],[0 max(get(gca,'ylim'))],'Linewidth',3,'color','black')
line([adjC.max adjC.max],[0 max(get(gca,'ylim'))],'Linewidth',3,'color','black')
line([adjC.min adjC.max],[0 max(get(gca,'ylim'))],'Linewidth',3,'color','black')

if adjC.temp.min<0
    set(gca,'xlim',[-1 1])
else
    
    set(gca,'xlim',[0 1])
end
set(gca,'yticklabel',[])

eval(sprintf('%s = %f;',adjC.update.min,adjC.min));
eval(sprintf('%s = %f;',adjC.update.max,adjC.max));



% --- Outputs from this function are returned to the command line.
function varargout = adjustContrast_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in ButtonOk.
function ButtonOk_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonOk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)

% --- Executes on button press in ButtonCancel.
function ButtonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global adjC
adjC.min=adjC.temp.min;
adjC.max=adjC.temp.max;
adjC.close = 1;

try
eval(sprintf('global %s;',adjC.update.global))
eval(sprintf('%s = %f;',adjC.update.min,adjC.min));
eval(sprintf('%s = %f;',adjC.update.max,adjC.max));
eval(adjC.update.execute);
catch e
    fprintf('AdjustContrast: Parent Figure seems to be already closed.\n')
end

close(handles.figure1)


% --- Executes on button press in ButtonAuto.
function ButtonAuto_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonAuto (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global adjC
eval(sprintf('global %s;',adjC.update.global))
eval(sprintf('%s = ''auto'';',adjC.update.min(1:end-3),'auto'));
eval(adjC.update.execute);
close(handles.figure1)
