function varargout = figureSetup(varargin)
% FIGURESETUP MATLAB code for figureSetup.fig
%      FIGURESETUP, by itself, creates a new FIGURESETUP or raises the existing
%      singleton*.
%
%      H = FIGURESETUP returns the handle to a new FIGURESETUP or the handle to
%      the existing singleton*.
%
%      FIGURESETUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FIGURESETUP.M with the given input arguments.
%
%      FIGURESETUP('Property','Value',...) creates a new FIGURESETUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before figureSetup_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to figureSetup_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help figureSetup

% Last Modified by GUIDE v2.5 08-Dec-2013 16:25:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @figureSetup_OpeningFcn, ...
                   'gui_OutputFcn',  @figureSetup_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before figureSetup is made visible.
function figureSetup_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to figureSetup (see VARARGIN)

% Choose default command line output for figureSetup
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

if numel(varargin)>0
    set(handles.imageName, 'String',  varargin{1}.name);
    filesettings = generateFilesettings(varargin{1}.name);
    set(handles.table, 'Data', filesettings.matrix);
    set(handles.setSeparator, 'String', filesettings.separator);
    set(handles.cbPosition, 'Value', (filesettings.matrix{1,1} > 0));
    set(handles.cbTimepoint, 'Value', (filesettings.matrix{2,1} > 0));
    set(handles.cbWavelength, 'Value', (filesettings.matrix{3,1} > 0));
    set(handles.cbAdditional, 'Value', (filesettings.matrix{4,1} > 0));
    setSampleIm(handles);
end

% UIWAIT makes figureSetup wait for user response (see UIRESUME)
 uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = figureSetup_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global returnsettings confirmStructure

if confirmStructure
    varargout{1} = returnsettings;
else
    varargout{1} = [];
end

% Get default command line output from handles structure
%varargout{1} = handles.output;



function imageName_Callback(hObject, eventdata, handles)
% hObject    handle to imageName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imageName as text
%        str2double(get(hObject,'String')) returns contents of imageName as a double


% --- Executes during object creation, after setting all properties.
function imageName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectImage.
function selectImage_Callback(hObject, eventdata, handles)
% hObject    handle to selectImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filesettings
     [FileName PathName] = uigetfile({'*.png;*.jpeg;*.jpg;*.tif','Image Files (*.png;*.jpeg;*.jpg;*.tif)'},'Select an image');
    set(handles.imageName, 'String', FileName);
   filesettings = generateFilesettings(FileName);
    set(handles.table, 'Data', filesettings.matrix);
    set(handles.setSeparator, 'String', filesettings.separator);
    set(handles.cbPosition, 'Value', (filesettings.matrix{1,1} > 0));
    set(handles.cbTimepoint, 'Value', (filesettings.matrix{2,1} > 0));
    set(handles.cbWavelength, 'Value', (filesettings.matrix{3,1} > 0));
    set(handles.cbAdditional, 'Value', (filesettings.matrix{4,1} > 0));
      
    %create sample image name
%     sampleIm = {'','','',''};
%     for i=1:4 
%         sampleIm{tab{i,1}} = [tab{i,3} [repmat('0',1,(tab{i,2}-1)) '1']];
%     end
%     sampleIm = strrep([name '_' strjoin(sampleIm,'_') '.' ext],'_.','_');
setSampleIm(handles);
function imageName2_Callback(hObject, eventdata, handles)
% hObject    handle to imageName2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imageName2 as text
%        str2double(get(hObject,'String')) returns contents of imageName2 as a double


% --- Executes during object creation, after setting all properties.
function imageName2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageName2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function setSeparator_Callback(hObject, eventdata, handles)
% hObject    handle to setSeparator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of setSeparator as text
%        str2double(get(hObject,'String')) returns contents of setSeparator as a double
setSampleIm(handles);
% --- Executes during object creation, after setting all properties.
function setSeparator_CreateFcn(hObject, eventdata, handles)
% hObject    handle to setSeparator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
%function fragmentImage(name)


% --- Executes when entered data in editable cell(s) in table.
function table_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to table (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
tab = get(hObject, 'data');
%get ext and name
% x = strsplit('.', get(handles.imageName, 'String'));
%     ext = x(end);
%     ext= ext{1};
%     x = strsplit('_', get(handles.imageName, 'String'));
%     name = x{1};
%     %create sample image name
%     sampleIm = {'','','',''};
%     for i=1:4 
%         sampleIm{tab{i,1}} = [tab{i,3} [repmat('0',1,(tab{i,2}-1)) '1']];
%     end
%     sampleIm = strrep([name '_' strjoin(sampleIm,'_') '.' ext],'_.','_');
setSampleIm(handles);


function [sampleIm, posFolder] = setSampleIm(handles)
    global filesettings
    x = strsplit_qtfy('.', get(handles.imageName, 'String'));
    ext = x(end);
    ext= ext{1};
    tab = get(handles.table, 'Data');
    sep = get(handles.setSeparator,'String');
    x = strsplit_qtfy(sep, get(handles.imageName, 'String'));
    name = x{1};
    boxes = {get(handles.cbPosition, 'Value'),get(handles.cbTimepoint, 'Value'),get(handles.cbWavelength, 'Value'),get(handles.cbAdditional, 'Value')};
    filesettings.matrix = tab;
    filesettings.separator = sep;
    sampleIm = generateFilename(name, boxes{1}, boxes{2}, boxes{3}, boxes{4}, ext, filesettings);
    if(get(handles.cbPosition,'Value') && tab{1,1} ~= -1)
         posFolder = [name sep [tab{1,3} [repmat('0',1,(tab{1,2}-1)) '1']]];
    else
        posFolder = [name sep 'p001'];
    end
set(handles.imageName2, 'String', sampleIm);
set(handles.posFold, 'String', posFolder);           
        
function posFold_Callback(hObject, eventdata, handles)

% hObject    handle to posFold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of posFold as text
%        str2double(get(hObject,'String')) returns contents of posFold as a double


% --- Executes during object creation, after setting all properties.
function posFold_CreateFcn(hObject, eventdata, handles)
% hObject    handle to posFold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2


% --- Executes on button press in cbPosition.
function cbPosition_Callback(hObject, eventdata, handles)
% hObject    handle to cbPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbPosition
if(~get(hObject,'Value'))
    tab = get(handles.table, 'Data');
    tab(1,:) = {-1,-1,'-1'};
    set(handles.table, 'Data', tab)
end
    setSampleIm(handles);

% --- Executes on button press in cbTimepoint.
function cbTimepoint_Callback(hObject, eventdata, handles)
% hObject    handle to cbTimepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbTimepoint
if(~get(hObject,'Value'))
    tab = get(handles.table, 'Data');
    tab(2,:) = {-1,-1,'-1'};
    set(handles.table, 'Data', tab)
end
    setSampleIm(handles);

% --- Executes on button press in cbWavelength.
function cbWavelength_Callback(hObject, eventdata, handles)
% hObject    handle to cbWavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbWavelength
if(~get(hObject,'Value'))
    tab = get(handles.table, 'Data');
    tab(3,:) = {-1,-1,'-1'};
    set(handles.table, 'Data', tab)
end
    setSampleIm(handles);

% --- Executes on button press in cbAdditional.
function cbAdditional_Callback(hObject, eventdata, handles)
% hObject    handle to cbAdditional (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cbAdditional
if(~get(hObject,'Value'))
    tab = get(handles.table, 'Data');
    tab(4,:) = {-1,-1,'-1'};
    set(handles.table, 'Data', tab)
end
    setSampleIm(handles);


% --- Executes on button press in togglebutton3.
function togglebutton3_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton3


% --- Executes on button press in ok.
function ok_Callback(hObject, eventdata, handles)
% hObject    handle to ok (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filesettings returnsettings confirmStructure

confirmStructure=1;
returnsettings = filesettings;
close(handles.figure1);



% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global confirmStructure

confirmStructure=0;
close(handles.figure1);