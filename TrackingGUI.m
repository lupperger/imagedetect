function varargout = TrackingGUI(varargin)
% TRACKINGGUI M-file for TrackingGUI.fig
%      TRACKINGGUI, by itself, creates a new TRACKINGGUI or raises the existing
%      singleton*.
%
%      H = TRACKINGGUI returns the handle to a new TRACKINGGUI or the handle to
%      the existing singleton*.
%
%      TRACKINGGUI('CALLBACK',hObject,eventData,handles,...) calls the
%      local
%      function named CALLBACK in TRACKINGGUI.M with the given input arguments.
%
%      TRACKINGGUI('Property','Value',...) creates a new TRACKINGGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TrackingGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TrackingGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TrackingGUI

% Last Modified by GUIDE v2.5 17-Mar-2014 16:10:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @TrackingGUI_OpeningFcn, ...
    'gui_OutputFcn',  @TrackingGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TrackingGUI is made visible.
function TrackingGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TrackingGUI (see VARARGIN)

% Choose default command line output for TrackingGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global vtset

% show splashscreen

showsplashscreen
tic;
vtset.version='0.9.9';
set(handles.versionNumber,'String',vtset.version);
set(handles.showOnlyCells,'KeyPressFcn',@myKeyFunction);

%%%% include some function
%# function adjustContrast
%# function imfilter
%# function addQuantificationChannel
%# function loadAdditionalAnnotation


try
    % initialize

    set(0,'DefaultFigureColor',[1 1 1])
    vtset.updateCallback = @update;
    vtset.trackCell=@btnShowCell_Callback;
    vtset.saveTrees=@saveTrees;
    vtset.plotCell=@plotCell;
    vtset.functions.initTreeList = @initializeList;
    vtset.functions.trackCell = @btnShowCell_Callback;
    vtset.functions.showNextTree = @showNextTree_Callback;
    vtset.functions.showPrevTree = @showPrevTree_Callback;
    vtset.functions.selectionMode = @selectionMode;
    vtset.functions.initFigure = @initFigure;
    vtset.functions.resetActiveChannel = @resetActiveChannel;
    vtset.functions.lstTrees_Callback = @lstTrees_Callback;
    
    vtset.setVisibilty = @setVisibilty;
    vtset.setVisibiltyAxes = @setVisibiltyAxes;
    
    vtset.setCellColor = @setCellColor;
    vtset.addAxes = @addAxes;
    vtset.combinechannels = @combineTimecourses;
    vtset.setYSettings = @setYSettings;
    vtset.applyAxisSettings = @applyAxisSettings;
    vtset.redrawtree = @showtree;
    vtset.tools.setCellActivity=@setCellActivity;
    vtset.tools.outlierPostProcessing=@outlierPostProcessing;
    vtset.tools.setVisibleChannel = @setVisibleChannel;
    vtset.tools.alignHaxes = @alignHaxes;
    vtset.tools.setCellMarker = @setCellMarker;
    vtset.tools.setPlotLineWidth = @setPlotLineWidth;
    vtset.tools.setPlotLineStyle = @setPlotLineStyle;
    vtset.tools.recalcCells = @recalcCells;
    vtset.tools.densityTreePlot = @densityTreePlot;
    vtset.tools.requantifyCells = @requantifyCells;
    vtset.tools.addDetectionChannel = @addDetectionChannel;
    vtset.hObject = handles.figure1;
    
    vtset.showaxesQuants.wl = cell(1,3);
    vtset.showaxesQuants.showoutline = ones(1,3);
    vtset.showaxesQuants.active = ones(1,3);
    
    vtset.datacursor.showtip = 1;
    vtset.plotmode='int';
    vtset.smoothgraph=0;
%     vtset.doublethes=0;
    vtset.timemode='absoluteTime';
    vtset.timemodifier=60*60;
    vtset.treenum=1;
    vtset.showonlycells='all';
    vtset.showtree=1;
    vtset.combinedOutput=1;
    vtset.showhalo=1;
    vtset.showcellnumber=1;
    vtset.hplot=-1;
    vtset.colors.grid=1;
    vtset.colors.dynamicLineWidth=0;
    vtset.drawmode.inspected=0;
    vtset.dcselectedarray = [];
    vtset.selectionAxesAdded = 0;
    vtset.doubleclickTicID = tic;
    vtset.configfile = 'qtfy.cfg';
    
    vtset.contrast.wl = {};
    vtset.contrast.contrast = {};
    
    vtset.tttrootFolder='';
    tttrootFolder='';
    %%% read tttfilesfolder from qtfy.cfg
    if exist(vtset.configfile,'file')
        
        cfg = fileread(vtset.configfile);
        eval(cfg)
    else
        fprintf('config file not found\n');
    end
    if ~exist(tttrootFolder,'dir')
        tttrootFolder=selectNAS;
    end
    if tttrootFolder==0
        % cannot work without ttt/qtf files
        delete(vtset.hObject)
        return
    end
    vtset.AMTfilesFolder=tttrootFolder;
    vtset.tttfilesFolder=tttrootFolder;
    vtset.tttrootFolder=tttrootFolder;
    vtset.movieFolder='C:\..\';
    vtset.movierootFolder='C:\..\';
    
    

    % rescale for mac&linux
    if ~ispc
        pos = get(hObject,'position');
        set(hObject,'position',[pos(1:2) pos(3)+100 pos(4)]);
    end
    
    vtset.deltafilterthresh=5;
    vtset.selected=[];
    vtset.hcurves=[];
    vtset.outlierhandles=[];
    if isunix
        imageCache('init',200)
    else
        imageCache('init',50)
    end
    gainCache('init',20)
    
    %%% parallel computing
    %     [s r ] =system('grep family /proc/cpuinfo | wc -l');
    %     if str2double(r)>1
    %         matlabpool(2)
    %     end
    
    % position me
    scnsize = get(0,'ScreenSize');
    oldpos = get(handles.figure1,'Position');
    set(handles.figure1,'Position', [100 scnsize(4)-400 oldpos(3) oldpos(4)]);
    
    
    if numel(varargin)>0
        vtset.results = varargin{1};
        vtset.possibleUpdate = zeros(1,numel(vtset.results));
        
        initializeList;
    end
catch exception
    msgbox('An initialization error occured','Error','error');
    rethrow(exception)
end


% UIWAIT makes TrackingGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);
function tttrootFolder=selectNAS
global vtset
% select and write it afterwards
tttrootFolder = uigetdir(vtset.tttrootFolder,'Choose your ttt-root folder where you store your ttt and qtf files (e.g. X:\TTTfiles)');
if tttrootFolder==0
    % cannot work without ttt/qtf files
    return
end
if ~strcmpi(tttrootFolder,'X:\TTTfiles')
%     warndlg(sprintf('Your NAS drive looks different then it should be for a normal TTT user (e.g. X:\\TTTfiles).\nPlease doublecheck that your NAS drive is correct:\n%s',tttrootFolder));
end
% write to cfg file
writecfg(tttrootFolder)


function writecfg(tttrootFolder)
global vtset
fid = fopen(vtset.configfile,'w');
fprintf(fid,'%s\n','% this is where you store your ttt and qtfy files');
fprintf(fid,'tttrootFolder = ''%s/'';',tttrootFolder);
fclose(fid);

function showsplashscreen
% create a figure that is not visible yet, and has minimal titlebar properties
global vtset
fh = figure('Visible','off','MenuBar','none','NumberTitle','off');
vtset.splash.fh=fh;
% put an axes in it
ah = axes('Parent',fh,'Visible','off');

% put the image in it
[X,map] = imread('pic/qtfylogo.gif');
ih = image(X,'parent',ah);
colormap(map)
axis off

text(10,size(X,1)-20,'QTFy v 1.0')

% set the figure size to be just big enough for the image, and centered at
% the center of the screen
imxpos = get(ih,'XData');
imypos = get(ih,'YData');
set(ah,'Unit','Normalized','Position',[0,0,1,1]);
figpos = get(fh,'Position');
figpos(3:4) = [imxpos(2) imypos(2)]./2;
set(fh,'Position',figpos);
movegui(fh,'north')

% make the figure visible
set(fh,'Visible','on');

ht = timer('StartDelay',5,'ExecutionMode','SingleShot');
vtset.splash.ht = ht;
set(ht,'TimerFcn','global vtset;try;close(vtset.splash.fh);stop(vtset.splash.ht);delete(vtset.splash.ht);catch e;end;vtset=rmfield(vtset,''splash'');');
start(ht);

% --- Outputs from this function are returned to the command line.
function varargout = TrackingGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure


varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on selection change in lstTrees.
function lstTrees_Callback(hObject, eventdata, handles)
% hObject    handle to lstTrees (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns lstTrees contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lstTrees
global vtset vzt


if(exist('hObject'))
    vtset.lstTree = hObject
    vtset.tree_numbers = get(hObject, 'Value');
end

handles = guidata(vtset.hObject);


%prevent crash on doubleclick
if(toc(vtset.doubleclickTicID)<.5)
    return;
end
vtset.doubleclickTicID = tic;
% treelist=get(handles.lstTrees,'String');
%
% upto = findstr('|',(treelist{get(handles.lstTrees,'Value')}))-2;
% if numel(upto)==0
%     upto = numel((treelist{get(handles.lstTrees,'Value')}));
% end
% vtset.treenum = strmatch(treelist{get(handles.lstTrees,'Value')}(1:upto),vtset.results.treefiles);
vtset.treenum = get(handles.lstTrees,'Value');
vtset.selected = [];
% vtset.showquantification = 1:numel(vtset.results{vtset.treenum}.quants);
% cells=get(handles.showOnlyCells,'String');
% 
% if strcmpi(cells,'all')
%     cells = unique([vtset.results{vtset.treenum}.quants(1).cellNr]);
% else
%     f = strfind(cells,'>g');
%     if f
%         gen = str2double(cells(f+2))+1;
%         cells(f:f+2) =[];
%         cells = [cells ' ' num2str(2^gen:max(vtset.results{vtset.treenum}.nonFluor.cellNr))];
%     end
%     cells=strrep(cells, '-',':');
%     cells=strrep(cells, ',',' ');
%     cells=['[' cells ']' ];
%     cells=eval(cells);
%     cells=cells(ismember(cells, unique([vtset.results{vtset.treenum}.quants(1).cellNr])));
% end
% vtset.showonlycells = cells;

% init channels for menu
% also init contrast if not already there
% also init all possible quants for CellInspector
vtset.allpossiblequants={};
vtset.channels={};
for i = 1:numel(vtset.results{vtset.treenum}.quants)
    vtset.channels{i} = ['Q ' vtset.results{vtset.treenum}.quants(i).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(i).settings.detectchannel).settings.wl ' ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(i).settings.detectchannel).settings.trackset.threshmethod];
    if ~ismember(vtset.results{vtset.treenum}.quants(i).settings.wl,vtset.contrast.wl)
        vtset.contrast.contrast{end+1} = 'auto';
        vtset.contrast.wl{end+1} = vtset.results{vtset.treenum}.quants(i).settings.wl;
    end
    vtset.allpossiblequants{end+1} = vtset.results{vtset.treenum}.quants(i).settings.wl;
end

for i = 1:numel(vtset.results{vtset.treenum}.detects)
    if ~ismember(vtset.results{vtset.treenum}.detects(i).settings.wl,vtset.contrast.wl)
        vtset.contrast.contrast{end+1} = 'auto';
        vtset.contrast.wl{end+1} = vtset.results{vtset.treenum}.detects(i).settings.wl;
    end
end

if ~ismember(vtset.results{vtset.treenum}.nonFluor.settings.wl,vtset.contrast.wl)
    vtset.contrast.contrast{end+1} = 'auto';
    vtset.contrast.wl{end+1} = vtset.results{vtset.treenum}.nonFluor.settings.wl;
end

initializeList();
initFigure();
redraw();
showtree();

if isfield(vtset,'htree2') && ishandle(vtset.htree2)
    vzt.redraw();
end




% --- Executes during object creation, after setting all properties.
function lstTrees_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lstTrees (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function setVisibleChannel
global vtset
channels = {};
for x = 1:numel(vtset.results{vtset.treenum}.quants)
    channels{end+1} = ['Q: ' vtset.results{vtset.treenum}.quants(x).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(x).settings.detectchannel).settings.wl];
end
channel = listdlg('Promptstring','Please select wavelength:', 'Name','Show Tree',...
    'Liststring',channels,'ListSize',[200 200],'SelectionMode','multiple');
if ~numel(channel)
    return
end

for t = 1:numel(vtset.results)
    if numel(vtset.results{t}.quants) == numel(vtset.results{vtset.treenum}.quants)
        vtset.results{t}.settings.visiblePlots=zeros(1,numel(vtset.results{t}.quants));
        vtset.results{t}.settings.visiblePlots(channel) = 1;
    else
        fprintf('Cannot assign visible channel settings to tree %s.\n',vtset.results{t}.settings.treefile);
    end
end

function alignHaxes
global vtset

maxleft=0;
width=1;
for ch = vtset.haxes(vtset.haxesvisible==1)
    oldpos = get(ch,'position');
    if oldpos(1)>maxleft
        maxleft = oldpos(1);
        width = oldpos(3);
    end
end

for chan = find(vtset.haxesvisible==1)
    oldpos = get(vtset.haxes(chan),'position');
    set(vtset.haxes(chan),'position',[maxleft oldpos(2) width oldpos(4)])
%     set(vtset.haxes(chan),'activePositionProperty','outerposition')
end

% create/read the tree and description list
function initializeList
global vtset
handles = guidata(vtset.hObject);
try
    counter=0;
    trees=cell(numel(vtset.results),1);
    % create the listbox entries
    possibleUpdate={'','*'};
    for i=1:numel(vtset.results)
        counter=counter+1;
        trees{counter} = [vtset.results{i}.settings.treefile ' | ' vtset.results{i}.description possibleUpdate{vtset.possibleUpdate(i)+1}];
    end
    
    set(handles.lstTrees,'String',trees);
    if isfield(vtset,'treenum') && numel(vtset.treenum)
        set(handles.lstTrees,'Value',vtset.treenum);
    else
        set(handles.lstTrees,'Value',1);
    end
    cells=get(handles.showOnlyCells,'String');
    
    if strcmpi(cells,'all')
        cells = unique([vtset.results{vtset.treenum}.nonFluor.cellNr]);
    else
        f = strfind(cells,'>g');
        if f
            gen = str2double(cells(f+2))+1;
            cells(f:f+2) =[];
            cells = [cells ' ' num2str(2^gen:max(vtset.results{vtset.treenum}.nonFluor.cellNr))];
        end
        cells=strrep(cells, '-',':');
        cells=strrep(cells, ',',' ');
        cells=['[' cells ']' ];
        cells=eval(cells);
        cells=cells(ismember(cells, unique([vtset.results{vtset.treenum}.nonFluor.cellNr])));
    end
    vtset.showonlycells = cells;
catch exception
    msgbox('Invalid Tree List','Error','error')
    rethrow(exception)
end

function myKeyFunctionKomma(src, evnt)

% handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

if strcmp(k,',') %if enter was pressed
    pause(0.01) %allows time to update
    TrackACell;
end

function myKeyFunction(src, evnt)
global vtset
handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

if strcmp(k,'return') %if enter was pressed
    pause(0.01) %allows time to update
    cells=get(handles.showOnlyCells,'String');
    if strcmpi(cells,'all')
        cells = unique([vtset.results{vtset.treenum}.nonFluor.cellNr]);
    else
        f = strfind(cells,'>g');
        if f
            gen = str2double(cells(f+2))+1;
            cells(f:f+2) =[];
            cells = [cells ' ' num2str(2^gen:max(vtset.results{vtset.treenum}.nonFluor.cellNr))];
        end
        cells=strrep(cells, '-',':');
        cells=strrep(cells, ',',' ');
        cells=['[' cells ']' ];
        try
        cells=eval(cells);
        catch e
            errordlg('Could not validate input')
        end
        cells=cells(ismember(cells, unique([vtset.results{vtset.treenum}.nonFluor.cellNr])));
    end
    vtset.showonlycells=cells;
    setVisibilty(0);
end

function mydeltafilterKeyFunction(src, evnt)
global vtset
handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

% h= (findobj('Tag','deltafiltercounter'))
% set(h,'String','x')

if strcmp(k,'return') %if enter was pressed
    pause(0.01) %allows time to update
    vtset.deltafilterthresh=str2double(get(handles.deltafilter,'String'));
    redraw;
end


% --- Executes on button press in btnShowTree.
function btnShowTree_Callback(hObject, eventdata, handles)
% hObject    handle to btnShowTree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset


    
    if numel(vtset.results{vtset.treenum}.quants)>1
        channels = {};
        for x = 1:numel(vtset.results{vtset.treenum}.quants)
            channels{end+1} = ['Q: ' vtset.results{vtset.treenum}.quants(x).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(x).settings.detectchannel).settings.wl];
        end
        channel = listdlg('Promptstring','Please select wavelength:', 'Name','Show Tree',...
            'Liststring',channels,'ListSize',[200 200],'SelectionMode','multiple');
        if ~numel(channel)
            return
        end
%         param1 = channels{channel};
    else
        %         param1 = vtset.results{vtset.treenum}.quants.settings.wl;
        channel = 1;
    end
    
%     param2 = questdlg('Which parameter should be on x-axis','Show Tree', ...
%         'Absolute','No thickness','Absolute');
%     if numel(param2)>0
        
        % create figure, set callback
%         if ~isfield(vtset,'htree2') || numel(vtset.htree2)==0
%             vtset.htree2=figure;
%             set(get(vtset.htree2,'currentaxes'),'Position',[.1 .1 .85 .85])
%             scnsize = get(0,'ScreenSize');
%             oldpos = get(vtset.htree2,'Position');
%             set(vtset.htree2,'Position', [100 scnsize(4)-900 oldpos(3) oldpos(4)]);
%             set(vtset.htree2,'Name', 'Tree');
%         else
%             figure(vtset.htree2);
%         end
%         clf;
        
        
        %%%% get cells
        cells = vtset.showonlycells;
%         if strcmpi(cells,'all')
%             cells = unique(vtset.results{vtset.treenum}.quants(1).cellNr)';
%         else
%             f = strfind(cells,'>g');
%             if f
%                 gen = str2double(cells(f+2))+1;
%                 cells(f:f+2) =[];
%                 cells = [cells ' ' num2str(2^gen:max(vtset.results{vtset.treenum}.nonFluor.cellNr))];
%             end
%             cells=strrep(cells, '-',':');
%             cells=strrep(cells, ',',' ');
%             cells=['[' cells ']' ];
%             cells=eval(cells);
%             cells=cells(ismember(cells, unique(vtset.results{vtset.treenum}.quants(1).cellNr)));
%         end
        visualizeTree(cells,channel)
%         drawtreeCurves(param1,param2, cells,channel)
        
%     end
% end

% function redraw_cell(cell)
% global vtset
% h = vtset.hcurves(vtset.hcurvescells == cell & vtset.hcurvestree==1)
% 
% function chkeckboxMenu(w1)
% 'w1'

function showNextTree_Callback(hObject, eventdata, handles)
global vtset
handles = guidata(vtset.hObject);
if vtset.treenum == length(vtset.results)
    vtset.treenum = 1;
else
    vtset.treenum = vtset.treenum+1;
end
vtset.functions.initTreeList();
lstTrees_Callback([], [], handles)

    
function showPrevTree_Callback(hObject, eventdata, handles)
global vtset
handles = guidata(vtset.hObject);

if vtset.treenum == 1
    vtset.treenum = length(vtset.results);
else
    vtset.treenum = vtset.treenum-1;
end
vtset.functions.initTreeList();
lstTrees_Callback([], [], handles)

function  resetActiveChannel(hObject, eventdata, handles)
global vtset

if(isfield (vtset.results{vtset.treenum}.settings, 'activeChannel'))
    disp('reset');
    for i = 1:numel(vtset.results{vtset.treenum}.settings.activeChannel)
        quant = vtset.results{vtset.treenum}.settings.activeChannel{i}.quantid   
        vtset.results{vtset.treenum}.quants(quant).cellNr = 0;
        vtset.results{vtset.treenum}.quants(quant).timepoint = 0;
        vtset.results{vtset.treenum}.quants(quant).absoluteTime = 0;
        vtset.results{vtset.treenum}.quants(quant).positionIndex = 0;
        vtset.results{vtset.treenum}.quants(quant).filename = 0;
        vtset.results{vtset.treenum}.quants(quant).int = 0;
        vtset.results{vtset.treenum}.quants(quant).active = 0;
        vtset.results{vtset.treenum}.quants(quant).detectchannel = 0;
        vtset.results{vtset.treenum}.quants(quant).detected = 0;
        vtset.results{vtset.treenum}.quants(quant).inspected = 0;
    end
end
vtset.tools.alignHaxes();
redraw;

function selectionMode(hObject, eventdata, handles)
global vtset;

allready_exist = false;

    for i = 1:numel(vtset.results{vtset.treenum}.quants)
       wl = vtset.results{vtset.treenum}.quants(i).settings.wl
       
      if isfield (vtset.results{vtset.treenum}.settings, 'activeChannel')
          allready_exist = false
            for j = 1:numel(vtset.results{vtset.treenum}.settings.activeChannel)
                    if strcmp(vtset.results{vtset.treenum}.settings.activeChannel{j}.wl, wl)
                     allready_exist = true
                    end
            end
            
            if(~allready_exist)               
                vtset.results{vtset.treenum}.settings.activeChannel{end+1}.wl = wl;
                addAxes(wl)
                vtset.results{vtset.treenum}.settings.activeChannel{end}.quantid = numel(vtset.results{vtset.treenum}.quants)
            end
       else
           vtset.results{vtset.treenum}.settings.activeChannel{1}.wl = wl;
           addAxes(wl)
           vtset.results{vtset.treenum}.settings.activeChannel{1}.quantid = numel(vtset.results{vtset.treenum}.quants)
       end
       
    end

    
for i = 1:numel(vtset.dcselectedarray)
    cell(i) = vtset.dcselectedarray(i).cell;
    channel(i) = vtset.dcselectedarray(i).channel;
    timepoint(i) = vtset.dcselectedarray(i).timepoint;
end

[timepoint, order] = sort(timepoint)
channel = channel(order)
cell = cell(order)



lasttime = 0;
starttime = 1;
tmp_quant = numel(vtset.results{vtset.treenum}.quants);

%{
%reset active channel
for i = 1:numel(vtset.results{vtset.treenum}.settings.activeChannel)
    quant = vtset.results{vtset.treenum}.settings.activeChannel{i}.quantid   
    vtset.results{vtset.treenum}.quants(quant).cellNr = 0;
    vtset.results{vtset.treenum}.quants(quant).timepoint = 0;
    vtset.results{vtset.treenum}.quants(quant).absoluteTime = 0;
    vtset.results{vtset.treenum}.quants(quant).positionIndex = 0;
    vtset.results{vtset.treenum}.quants(quant).filename = 0;
    vtset.results{vtset.treenum}.quants(quant).int = 0;
    vtset.results{vtset.treenum}.quants(quant).active = 0;
    vtset.results{vtset.treenum}.quants(quant).detectchannel = 0;
    vtset.results{vtset.treenum}.quants(quant).detected = 0;
    vtset.results{vtset.treenum}.quants(quant).inspected = 0;
end
%}

cellsingle = ones(1,numel(timepoint));
for i = 1:numel(timepoint)
    for j = (i+1):(numel(timepoint))
        if(cell(i) == cell(j))
            cellsingle(i) = 0;
            cellsingle(j) = 0;
        end
    end    
end


for i = 1:numel(timepoint)      
    tmp_wl = vtset.results{vtset.treenum}.quants(channel(i)).settings.wl;    
    for j = 1:numel(vtset.results{vtset.treenum}.settings.activeChannel)
        if strcmp(vtset.results{vtset.treenum}.settings.activeChannel{j}.wl, tmp_wl)
           tmp_quant = vtset.results{vtset.treenum}.settings.activeChannel{j}.quantid;
        end
    end
        
    [alldata, alltps, order] = calculateTimeCourse(cell(i), channel(i), vtset.plotmode,vtset.smoothgraph);
    
    tp = vtset.results{vtset.treenum}.quants(channel(i)).timepoint(vtset.results{vtset.treenum}.quants(channel(i)).cellNr==cell(i));
    if(cellsingle(i) == 1)
        index = find(tp);
        globalindex = find(ismember(vtset.results{vtset.treenum}.quants(channel(i)).timepoint, tp) & vtset.results{vtset.treenum}.quants(channel(i)).cellNr==cell(i));
    else
        index = find(tp > lasttime & tp <= timepoint(i))             
        globalindex = find(ismember(vtset.results{vtset.treenum}.quants(channel(i)).timepoint, tp) & vtset.results{vtset.treenum}.quants(channel(i)).cellNr==cell(i) & vtset.results{vtset.treenum}.quants(channel(i)).timepoint > lasttime & vtset.results{vtset.treenum}.quants(channel(i)).timepoint <= timepoint(i));
    end
    data = alldata(index)
    tps = alltps(index) * vtset.timemodifier  
    
    
    lasttime = timepoint(i)        
        
    vtset.results{vtset.treenum}.quants(tmp_quant).absoluteTime = [vtset.results{vtset.treenum}.quants(tmp_quant).absoluteTime tps];
    vtset.results{vtset.treenum}.quants(tmp_quant).int = [vtset.results{vtset.treenum}.quants(tmp_quant).int data];
    
    cellarray = [];
    for j = 1:numel(data)
        cellarray = [cellarray cell(i)]
    end     
    vtset.results{vtset.treenum}.quants(tmp_quant).cellNr = [vtset.results{vtset.treenum}.quants(tmp_quant).cellNr cellarray];
    
   
    vtset.results{vtset.treenum}.quants(tmp_quant).timepoint = [vtset.results{vtset.treenum}.quants(tmp_quant).timepoint vtset.results{vtset.treenum}.quants(channel(i)).timepoint(globalindex)];
    vtset.results{vtset.treenum}.quants(tmp_quant).positionIndex = [vtset.results{vtset.treenum}.quants(tmp_quant).positionIndex vtset.results{vtset.treenum}.quants(channel(i)).positionIndex(globalindex)];
    vtset.results{vtset.treenum}.quants(tmp_quant).active = [vtset.results{vtset.treenum}.quants(tmp_quant).active vtset.results{vtset.treenum}.quants(channel(i)).active(globalindex)];   
    vtset.results{vtset.treenum}.quants(tmp_quant).detectchannel = [vtset.results{vtset.treenum}.quants(tmp_quant).detectchannel vtset.results{vtset.treenum}.quants(channel(i)).detectchannel(globalindex)];
    vtset.results{vtset.treenum}.quants(tmp_quant).inspected = [vtset.results{vtset.treenum}.quants(tmp_quant).inspected vtset.results{vtset.treenum}.quants(channel(i)).inspected(globalindex)];
    vtset.results{vtset.treenum}.quants(tmp_quant).filename = [vtset.results{vtset.treenum}.quants(tmp_quant).filename vtset.results{vtset.treenum}.quants(channel(i)).filename(globalindex)];   
end

%{
figure, a1 = axes
hold(a1)
plot(a1,  vtset.results{vtset.treenum}.quants(end).absoluteTime,  vtset.results{vtset.treenum}.quants(end).int)
%}
vtset.tools.alignHaxes();
redraw;
set(vtset.haxes(end-1), 'YLimmode', 'Auto');
set(vtset.haxes(end-2), 'YLimmode', 'Auto');


function addAxes(wl)
global vtset
vtset.results{vtset.treenum}.quants(end+1).settings = vtset.results{vtset.treenum}.quants(numel(vtset.results{vtset.treenum}.quants)).settings;
vtset.results{vtset.treenum}.quants(end).settings.wl = wl;
vtset.results{vtset.treenum}.quants(end).cellNr = 0;
vtset.results{vtset.treenum}.quants(end).timepoint = 0;
vtset.results{vtset.treenum}.quants(end).absoluteTime = 0;
vtset.results{vtset.treenum}.quants(end).positionIndex = 0;
vtset.results{vtset.treenum}.quants(end).filename = 0;
vtset.results{vtset.treenum}.quants(end).int = 0;
vtset.results{vtset.treenum}.quants(end).active = 0;
vtset.results{vtset.treenum}.quants(end).detectchannel = 0;
vtset.results{vtset.treenum}.quants(end).detected = 0;
vtset.results{vtset.treenum}.quants(end).inspected = 0;

vtset.showquantification(end+1) = vtset.showquantification(end)+1;
vtset.results{vtset.treenum}.settings(end).channelsettings(end+1) = vtset.results{vtset.treenum}.settings(end).channelsettings(end);

vtset.channels={};
 for i = 1:numel(vtset.results{vtset.treenum}.quants)
     vtset.channels{i} = ['Q ' vtset.results{vtset.treenum}.quants(i).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(i).settings.detectchannel).settings.wl ' ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(i).settings.detectchannel).settings.trackset.threshmethod];
 end
 
vtset.tools.alignHaxes();
vtset.selectionAxesAdded = 1;
initFigure;
redraw;
showtree;




function initFigure
global vtset

% init figure
if ~ishandle(vtset.hplot)
    vtset.hplot=figure;
    scnsize = get(0,'ScreenSize');
    oldpos = get(vtset.hplot,'Position');
    set(vtset.hplot,'Position', [100 scnsize(4)-900 650 oldpos(4)]);
    set(vtset.hplot,'Name', sprintf('Single-cell time courses: %s',vtset.results{vtset.treenum}.settings.treefile));
    set(vtset.hplot,'color',[1 1 1]);
    set(vtset.hplot,'renderer','painters')
    set(vtset.hplot,'resizeFcn',@resizeFigure)
    clf(vtset.hplot)
    
    %toolbox extention START
    [X map] = imread('pic/crosshair.png');
    Y = imresize(X, [20 20]);
    tbh = findall(vtset.hplot,'Type','uitoolbar');
    tth = uitoggletool(tbh,'CData',Y,...
        'Separator','on',...
        'TooltipString','Track Cell',...
        'HandleVisibility','on');
    set(tth, 'ClickedCallback', 'global vtset; vtset.functions.trackCell(1, 1 , 1)');
    
    [X map] = imread('pic/arrow-up.jpg');
    Y = imresize(X, [20 20]);
    tbh = findall(vtset.hplot,'Type','uitoolbar');
    tth = uitoggletool(tbh,'CData',Y,...
        'Separator','off',...
        'TooltipString','Show previous Tree',...
        'HandleVisibility','on');
    set(tth, 'ClickedCallback', 'global vtset; vtset.functions.showPrevTree(1, 1 , 1)');

    [X map] = imread('pic/arrow-down.jpg');
    Y = imresize(X, [20 20]);
    tbh = findall(vtset.hplot,'Type','uitoolbar');
    tth = uitoggletool(tbh,'CData',Y,...
        'Separator','off',...
        'TooltipString','Show next Tree',...
        'HandleVisibility','on');
    set(tth, 'ClickedCallback', 'global vtset; vtset.functions.showNextTree(1, 1 , 1)');
    
%     [X map] = imread('pic/hand.jpg');
%     Y = imresize(X, [20 20]);
%     tbh = findall(vtset.hplot,'Type','uitoolbar');
%     tth = uitoggletool(tbh,'CData',Y,...
%         'Separator','off',...
%         'TooltipString','Send selected to new channel',...
%         'HandleVisibility','on');
%     set(tth, 'ClickedCallback', 'global vtset; vtset.functions.selectionMode(1,1,1)');
%     
%     [X map] = imread('pic/clear.png');
%     Y = imresize(X, [20 20]);
%     tbh = findall(vtset.hplot,'Type','uitoolbar');
%     tth = uitoggletool(tbh,'CData',Y,...
%         'Separator','off',...
%         'TooltipString','Reset new channel',...
%         'HandleVisibility','on');
%     set(tth, 'ClickedCallback', 'global vtset; vtset.functions.resetActiveChannel(1,1,1)');
    %toolbox extention extention END
    
else
    figure(vtset.hplot);    
    set(vtset.hplot,'Name', sprintf('Single-cell time courses: %s',vtset.results{vtset.treenum}.settings.treefile));

%     set(vtset.hplot,'renderer','painters')
    clf(vtset.hplot)
end


% and axes for quantifications
vtset.showquantification = 1:numel(vtset.results{vtset.treenum}.quants);

vtset.haxes=[];
vtset.haxesvisible=[];
for i = 1:numel(vtset.showquantification)
    vtset.haxes(end+1)=axes('parent',vtset.hplot,'outerposition',[0 1-i/(numel(vtset.showquantification)+vtset.showtree*vtset.combinedOutput) 1 1/(numel(vtset.showquantification)+vtset.showtree*vtset.combinedOutput)]);
    vtset.haxesvisible(end+1)=1;   
    set(vtset.haxes(end),'LooseInset', [0,0,0,0]);
    % set all max and min axis
    %     set(vtset.haxes(end),'xlim',[min(vtset.results{vtset.treenum}.quants(i).absoluteTime)/vtset.timemodifier-eps max(vtset.results{vtset.treenum}.quants(i).absoluteTime)/vtset.timemodifier])
    if isempty(vtset.results{vtset.treenum}.quants(i).absoluteTime)
        set(vtset.haxes(end),'xlim',[0 1])
    else
        set(vtset.haxes(end),'xlim',[min(vtset.results{vtset.treenum}.quants(i).absoluteTime)/vtset.timemodifier-.1 1+max(vtset.results{vtset.treenum}.quants(i).absoluteTime)/vtset.timemodifier])
    end
    
    if isfield(vtset.results{vtset.treenum}.settings,'ylims') && size(vtset.results{vtset.treenum}.settings.ylims,1)>=i
        if vtset.results{vtset.treenum}.settings.ylims(i,1) == vtset.results{vtset.treenum}.settings.ylims(i,2)
            vtset.results{vtset.treenum}.settings.ylims(i,1)=0;
        end
        set(vtset.haxes(end),'ylim',vtset.results{vtset.treenum}.settings.ylims(i,:))
    else
        if isempty(vtset.results{vtset.treenum}.quants(i).int)
            set(vtset.haxes(end),'ylim',[0 1])
            vtset.results{vtset.treenum}.settings.ylims(i,:) = [0 1];
        else
            set(vtset.haxes(end),'ylim',[min([0, torow(vtset.results{vtset.treenum}.quants(i).int)])-eps max(vtset.results{vtset.treenum}.quants(i).int)+eps])
            vtset.results{vtset.treenum}.settings.ylims(i,:) = [min(vtset.results{vtset.treenum}.quants(i).int)-eps max(vtset.results{vtset.treenum}.quants(i).int)];
        end
    end
    set(vtset.haxes(end),'DrawMode','fast') 
end
% and axes for tree view
%%% TODO will this be called when ~vtset.showtree
i=i+1;
vtset.haxes(end+1)=axes('parent',vtset.hplot,'outerposition',[0 1-i/(numel(vtset.showquantification)+vtset.showtree*vtset.combinedOutput) 1 1/(numel(vtset.showquantification)+vtset.showtree*vtset.combinedOutput)]);
vtset.haxesvisible(end+1)=1;
set(vtset.haxes(end),'LooseInset', [0,0,0,0]);
set(vtset.haxes(end),'xlim',[min(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier-eps 1+max(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier])


% rescale xlim and link them
linkaxes(vtset.haxes,'x')

% set(vtset.haxes,'xLimMode','auto')

% set all max and min axis
if isfield(vtset.results{vtset.treenum}.settings,'xlim')
    if strcmp(vtset.results{vtset.treenum}.settings.xlim,'auto')
        set(vtset.haxes(end),'xlim',[min(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier-eps 1+max(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier])
%         set(vtset.haxes(end),'xlimmode','auto')
    else
        set(vtset.haxes(end),'xlim',vtset.results{vtset.treenum}.settings.xlim)
    end
else
    set(vtset.haxes(end),'xlim',[min(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier-eps 1+max(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier])
    vtset.results{vtset.treenum}.settings.xlim = [min(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier-eps 1+max(vtset.results{vtset.treenum}.nonFluor(1).absoluteTime)/vtset.timemodifier];
end
if isfield(vtset.results{vtset.treenum}.settings, 'ylim')
    if strcmp(vtset.results{vtset.treenum}.settings.ylim,'auto')
        try
            set(vtset.haxes,'ylimmode','auto');
            vtset.tools.alignHaxes();
            vtset.results{vtset.treenum}.settings.ylim='auto';
        catch e;
        end;
    else
        set(vtset.haxes(end),'ylim',[-1 1])
        set(vtset.haxes(end),'DrawMode','fast')
    end
else
    set(vtset.haxes(end),'ylim',[-1 1])
    set(vtset.haxes(end),'DrawMode','fast')
end

% align all axes
% for chan = 2:numel(vtset.haxes)
%     oldpos = get(vtset.haxes(chan),'position');
%     coursepos = get(vtset.haxes(1),'position');
%     set(vtset.haxes(chan),'position',[coursepos(1) oldpos(2) coursepos(3) oldpos(4)])
% end

% init all cell curves (colors etc)
numcells = numel(unique(torow(vtset.results{vtset.treenum}.nonFluor.cellNr)));
vtset.hcurves = [];
vtset.hcurvescells = [];
vtset.hcurveschannel = [];
% vtset.hcurvestree = [];
vtset.hcurvesaxes=[];
vtset.htreeaxes = [];
vtset.htreelines = [];
vtset.htreecells= [];
vtset.htreeannotation = [];

vtset.colors.color = cell(numcells,1);
vtset.colors.cell = zeros(numcells,1);
vtset.colors.linestyle = cell(numcells,1);
vtset.colors.linewidth = zeros(numcells,1);
vtset.colors.markerstyle = cell(numcells,1);
vtset.colors.treelinewidth = zeros(numcells,1);
vtset.colors.visible = ones(numcells,1);
vtset.colors.ylim = get(vtset.haxes,'ylim');
vtset.colors.xmin = get(vtset.haxes,'xlim');
% vtset.colors.dynamicLineWidth=0;

l = lines;
counter = 0;
for c = unique(torow(vtset.results{vtset.treenum}.nonFluor.cellNr))
    counter=counter+1;
    clr =  l(mod(c,64)+1,:);
    vtset.colors.color{counter} = clr;
    vtset.colors.cell(counter) = c;
    vtset.colors.linestyle{counter} = '-';
    vtset.colors.linewidth(counter) = 1;
    vtset.colors.markerstyle{counter} = '.';
    vtset.colors.treelinewidth(counter) = 1;
    vtset.colors.visible(counter) = 1;
end

% function resize for visible plots?


% create menu (show quantification) or if figure exists edit menu
%%% add menu
%%% has to be init in treelist callback
f=uimenu(vtset.hplot,'Label','Show quantification');
vtset.hcheckboxMenu=[];
for i = 1:numel(vtset.channels)
    vtset.hcheckboxMenu(i) = uimenu(f,'label',vtset.channels{i},'callback',['global vtset;vtset.setVisibiltyAxes(' num2str(i) ');vtset.tools.alignHaxes();'],'checked','on');
end
vtset.hcheckboxMenu(i+1) = uimenu(f,'label','Show tree','callback',['global vtset;vtset.setVisibiltyAxes(' num2str(i+1) ');vtset.tools.alignHaxes();'],'checked','on');
vtset.hcheckboxMenu(i+2) = uimenu(f,'label','Combined output','separator','on','checked','on');
set(vtset.hcheckboxMenu(i+2), 'callback','global vtset;vtset.combinedOutput = ~vtset.combinedOutput;l={''off'',''on''};set(vtset.hcheckboxMenu(end),''checked'',l{vtset.combinedOutput+1});vtset.functions.initFigure();vtset.updateCallback();vtset.redrawtree();');


%%% TOOL menu
f = uimenu(vtset.hplot,'label','Tools');
% uimenu(f,'label','Reload tree data','callback',['global vtset;xmlfilename = dir([vtset.movierootFolder ''/'' vtset.results{vtset.treenum}.settings.exp ''/*xml'']);'...
%     'xmlfilename = xmlfilename(end).name;'...
%     'muh = strsplit([''20'' vtset.results{vtset.treenum}.settings.exp(1:2)],vtset.results{vtset.treenum}.settings.treeroot);'...
%     'res = tttParser([vtset.tttrootFolder [''20'' vtset.results{vtset.treenum}.settings.exp(1:2)] muh{2} ''/'' vtset.results{vtset.treenum}.settings.treefile],[vtset.movierootFolder ''/'' vtset.results{vtset.treenum}.settings.exp ''/'' xmlfilename]);'...
%     'gatherimages(res,1,''n'',1);'...
%     'vtset.updateCallback();']);
uimenu(f,'label','Detect Outliers','callback','optimizeSegmentationGUI')
uimenu(f,'label','Rescale to protein numbers','callback','rescaletoProteinNumbers')
uimenu(f,'label','Combine channels','callback','global vtset;vtset.combinechannels();')
uimenu(f,'label','Add detection channel','callback','global vtset;vtset.tools.addDetectionChannel();')
uimenu(f,'label','Add quantification channel','callback','addQuantificationChannel();')
uimenu(f,'label','Activate timepoints','callback','global vtset;vtset.tools.setCellActivity();')
% uimenu(f,'label','Outlier post processing','callback','global vtset;vtset.tools.outlierPostProcessing();')
% uimenu(f,'label','Reload TTT data and recalc cells','callback','global vtset;vtset.tools.recalcCells();')
% % uimenu(f,'label','Apply current display settings to all open trees','callback','global vtset;vtset.tools.setVisibleChannel();')
uimenu(f,'label','Density plot of trees','callback','global vtset;vtset.tools.densityTreePlot();')
uimenu(f,'label','(Re)load additional TTT annotation','callback','loadAdditionalAnnotation')
uimenu(f,'label','Requantify cells','callback','vtset.tools.requantifyCells()')
uimenu(f,'label','Copy figure to clipboard','callback',['global vtset;'...
    'try;'...
    'toplot = vtset.hplot;'...
    'if ~vtset.combinedOutput;'...
    'b = questdlg(''Copy time course or tree view?'',''Copy to clipboard'',''Time course'',''Tree view'', ''Time course'');'...
    'if strcmp(b(2),''r'');'...
    'toplot=vtset.htree;'...
    'end;'...
    'end;'...
    'print(toplot, ''-dmeta'');'...
    'catch e;'...
    'fprintf(''Copy to Clipboard not possible.\n'');'...
    'end;']);



%%%% VIEW menu
f = uimenu(vtset.hplot,'label','View');
uimenu(f,'label','Apply current axis settings to all trees','callback',['global vtset;vtset.applyAxisSettings();'],'checked','off');
uimenu(f,'label','Set x axis limits','separator','on','callback',['global vtset; '...
                'oldx = get(vtset.haxes(1),''xlim''); '...
                't=inputdlg(''Enter new x axis limits:'',''X axis'',1,{num2str(oldx)});'...
                'try set(vtset.haxes,''xlim'',eval([''['' t{1} '']'']));'...
                'vtset.results{vtset.treenum}.settings.xlim=eval([''['' t{1} '']'']);'...
                'catch e;end;'...
                '']);
uimenu(f,'label','Autoset x axis limits','callback',['global vtset; '...
                'try set(vtset.haxes,''xlimmode'',''auto'');vtset.results{vtset.treenum}.settings.xlim=''auto'';catch e;end;'...
                '']);
uimenu(f,'label','Set x axis label','Callback','global vtset;vtset.setYSettings(''XLabel'')');
uimenu(f,'label','Set y axis limits','separator','on','Callback','global vtset;vtset.setYSettings(''YAxis'');vtset.results{vtset.treenum}.settings.ylim=[0 2];');
uimenu(f,'label','Autoset y axis limits','callback',['global vtset; '...
                'try set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();vtset.results{vtset.treenum}.settings.ylim=''auto'';catch e;end;'...
                '']);
uimenu(f,'label','Set y axis labels','Callback','global vtset;vtset.setYSettings(''YLabel'')');
uimenu(f,'label','Show tooltip','separator','on','callback',['global vtset;vtset.datacursor.showtip = ~vtset.datacursor.showtip; updateDataCursors(vtset.datacursor.Mgr)']);
uimenu(f,'label','Show grid','separator','on','callback',['global vtset;if ~vtset.colors.grid; '...
                'for q=1:numel(vtset.haxes);'...
                'grid(vtset.haxes(q),''on''); '...
                'end;'...
                'vtset.colors.grid=1; '...
                'else;'...
                'for q=1:numel(vtset.haxes);'...
                'grid(vtset.haxes(q),''off''); '...
                'end;'...
                'vtset.colors.grid=0; '...
                'end;']);
uimenu(f,'label','Set title','separator','on','Callback','global vtset;vtset.setYSettings(''Title'')');
uimenu(f,'label','Tree: Dynamic line width','Separator','on','Callback',['global vtset;'...
    'vtset.colors.dynamicLineWidth=~vtset.colors.dynamicLineWidth;'...
    'vtset.redrawtree();']);
f2 = uimenu(f,'label','Tree line width');
uimenu(f2,'label','+1','Callback','global vtset;vtset.colors.treelinewidth = vtset.colors.treelinewidth+1;vtset.redrawtree();');
uimenu(f2,'label','-1','Callback','global vtset;if vtset.colors.treelinewidth > 1; vtset.colors.treelinewidth = vtset.colors.treelinewidth-1;vtset.redrawtree(); end;');
uimenu(f2,'label','1','separator','on','Callback','global vtset;vtset.colors.treelinewidth = ones(numel(vtset.colors.treelinewidth),1);vtset.redrawtree();');
uimenu(f2,'label','2','Callback','global vtset;vtset.colors.treelinewidth = ones(numel(vtset.colors.treelinewidth),1)+1;vtset.redrawtree();');
uimenu(f2,'label','3','Callback','global vtset;vtset.colors.treelinewidth = ones(numel(vtset.colors.treelinewidth),1)+2;vtset.redrawtree();');
uimenu(f2,'label','4','Callback','global vtset;vtset.colors.treelinewidth = ones(numel(vtset.colors.treelinewidth),1)+3;vtset.redrawtree();');
uimenu(f2,'label','5','Callback','global vtset;vtset.colors.treelinewidth = ones(numel(vtset.colors.treelinewidth),1)+4;vtset.redrawtree();');
uimenu(f2,'label','Custom...','Separator','on','Callback','global vtset;x=inputdlg(''Enter line with'',''Tree line width'',1);if isempty(x);return;end;vtset.colors.treelinewidth = ones(numel(vtset.colors.treelinewidth),1)+str2double(x);vtset.redrawtree();');
uimenu(f,'label','Tree: Show cell number','Separator','off','Callback',['global vtset;'...
    'vtset.showcellnumber=~vtset.showcellnumber;'...
    'vtset.redrawtree();']);
f3 = uimenu(f,'label','Time course line width','Separator','on');
uimenu(f3,'label','+1','Callback','global vtset; vtset.tools.setPlotLineWidth(vtset.colors.linewidth(1)+1,1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);');
uimenu(f3,'label','-1','Callback','global vtset;if vtset.colors.linewidth(1) > 1;vtset.tools.setPlotLineWidth(vtset.colors.linewidth(1)-1,1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);end;');
uimenu(f3,'label','1','separator','on','Callback','global vtset;vtset.tools.setPlotLineWidth(1,1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);');
uimenu(f3,'label','2','Callback','global vtset;vtset.tools.setPlotLineWidth(2,1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);');
uimenu(f3,'label','3','Callback','global vtset;vtset.tools.setPlotLineWidth(3,1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);');
uimenu(f3,'label','4','Callback','global vtset;vtset.tools.setPlotLineWidth(4,1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);');
uimenu(f3,'label','5','Callback','global vtset;vtset.tools.setPlotLineWidth(5,1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);');
uimenu(f3,'label','Custom...','Separator','on','Callback','global vtset;x=inputdlg(''Enter line with'',''Plot line width'',1);if isempty(x);return;end;vtset.tools.setPlotLineWidth(str2double(x),1:max(vtset.results{vtset.treenum}.nonFluor.cellNr),0);');




uimenu(f,'label','Show inspected only','Separator','on','Callback','global vtset;vtset.drawmode.inspected = 1;vtset.updateCallback();');
uimenu(f,'label','Show non-inspected only','Callback','global vtset;vtset.drawmode.inspected = 2;vtset.updateCallback();');
uimenu(f,'label','Show all','Callback','global vtset;vtset.drawmode.inspected = 0;vtset.updateCallback();');



%%%% PlotMode menu
f = uimenu(vtset.hplot,'label','Plot mode');
vtset.hplotmode(1)=uimenu(f,'label','Absolute','separator','off','Checked','on');
set(vtset.hplotmode(1),'Callback',['global vtset;vtset.plotmode=''int'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(1),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(2)=uimenu(f,'label','Intensity by area','separator','off');
set(vtset.hplotmode(2),'Callback',['global vtset;vtset.plotmode=''mean'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(2),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(3)=uimenu(f,'label','Net production rate','separator','off');
set(vtset.hplotmode(3),'Callback',['global vtset;vtset.plotmode=''prodrate'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(3),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(4)=uimenu(f,'label','Concentration','separator','off');
set(vtset.hplotmode(4),'Callback',['global vtset;vtset.plotmode=''concentration'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(4),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(5)=uimenu(f,'label','Cell area','separator','off');
set(vtset.hplotmode(5),'Callback',['global vtset;vtset.plotmode=''size'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(5),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(6)=uimenu(f,'label','Cumulative','separator','off');
set(vtset.hplotmode(6),'Callback',['global vtset;vtset.plotmode=''cumulative'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(6),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(7)=uimenu(f,'label','Cumulative - ignoring decreasing intensities','separator','off');
set(vtset.hplotmode(7),'Callback',['global vtset;vtset.plotmode=''cumulativeNN'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(7),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(8)=uimenu(f,'label','Linear regression','separator','off');
set(vtset.hplotmode(8),'Callback',['global vtset;vtset.plotmode=''linearregression'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(8),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);
vtset.hplotmode(9)=uimenu(f,'label','Doulbing hypothesis','separator','off');
set(vtset.hplotmode(9),'Callback',['global vtset;vtset.plotmode=''doublethes'';set(vtset.hplotmode,''Checked'',''off'');set(vtset.hplotmode(9),''Checked'',''on'');vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();']);

vtset.hplotsmooth=uimenu(f,'label','Smooth lines','separator','on');
set(vtset.hplotsmooth,'Callback',['global vtset;vtset.smoothgraph=~vtset.smoothgraph;vtset.updateCallback();'...
    'set(vtset.haxes,''ylimmode'',''auto'');vtset.tools.alignHaxes();l={''off'',''on''};set(vtset.hplotsmooth,''Checked'',l{vtset.smoothgraph+1});']);

        

% set all visibility plots saved in settings
if isfield(vtset.results{vtset.treenum}.settings,'visiblePlots') && numel(vtset.results{vtset.treenum}.settings.visiblePlots)>1 
    for ch = find(~vtset.results{vtset.treenum}.settings.visiblePlots)
        setVisibiltyAxes(ch)
    end
else
    vtset.results{vtset.treenum}.settings.visiblePlots = ones(1,numel(vtset.haxes));
end

function setYSettings(option)
global vtset
if strcmp(option,'YAxis')
    prompt=vtset.channels;
%     for i = 1:numel(vtset.haxes)
%             prompt{end+1} = 'Enter the Y-Axis limits';
%     end
    
    name='Y-Axis limits';
    numlines=1;
    defaultanswer=get(vtset.haxes,'Ylim');
    def={};
    for i = 1:numel(vtset.channels)
        def{end+1} = sprintf('%2.1f %2.1f',defaultanswer{i});
    end
    answer=inputdlg(prompt,name,numlines,def);
    
    if numel(answer)>0
        for i = 1:numel(vtset.channels)
            set(vtset.haxes(i),'ylim',eval(['[' answer{i} ']']));
            vtset.results{vtset.treenum}.settings.ylims(i,:) = eval(['[' answer{i} ']']);
        end
        
    end
    
elseif strcmp(option,'XLabel')
    old = get(get(vtset.haxes(end),'xlabel'),'String');
    t=inputdlg('Enter new x label:','X Label',1,{old});
    if numel(t)
        set(get(vtset.haxes(end),'xlabel'),'String',t)
    end
    
elseif strcmp(option,'YLabel')
    prompt={};
    defaultanswer={};
    for i = 1:numel(vtset.haxes)
        prompt{end+1} = 'Enter the Y-Axis Label';
        defaultanswer{end+1} = get(get(vtset.haxes(i),'ylabel'),'String');
    end
    name='Y-Axis labels';
    numlines=1;
    answer=inputdlg(prompt,name,numlines,defaultanswer);
    if numel(answer)
            for i = 1:numel(vtset.haxes)
                 set(get(vtset.haxes(i),'ylabel'),'String',answer{i});
            end
    end
elseif strcmp(option,'Title')
    prompt={};
    defaultanswer={};
    for i = 1:numel(vtset.haxes)-1
        prompt{end+1} = 'Enter the Title';
        defaultanswer{end+1} = get(get(vtset.haxes(i),'title'),'string');
    end
    name='Titles';
    numlines=1;
    answer=inputdlg(prompt,name,numlines,defaultanswer);
    if numel(answer)
            for i = 1:numel(vtset.haxes)-1
                 set(get(vtset.haxes(i),'title'),'String',answer{i});
            end
    end
    
end
vtset.tools.alignHaxes()
% legend?

function applyAxisSettings()
    global vtset
    
    % set x-axis limits
    xlimit =  vtset.results{vtset.treenum}.settings.xlim;
    for i= 1:numel(vtset.results)
        vtset.results{i}.settings.xlim = xlimit;
    end
    % set y-axis limits
    ylimit =  vtset.results{vtset.treenum}.settings.ylims;
    for i= 1:numel(vtset.results)
        if i~=vtset.treenum
            channels = {};
            for j = 1:numel(vtset.results{i}.quants)
                channels{j} = ['Q ' vtset.results{i}.quants(j).settings.wl ' D ' vtset.results{i}.detects(vtset.results{i}.quants(j).settings.detectchannel).settings.wl ' ' vtset.results{i}.detects(vtset.results{i}.quants(j).settings.detectchannel).settings.trackset.threshmethod];
            end
            for j = 1: numel(channels)
                for k= 1: numel(vtset.channels)
                    if strcmp(channels{j}, vtset.channels{k})
                        if isfield(vtset.results{vtset.treenum}.settings,'ylim') && strcmp(class(vtset.results{vtset.treenum}.settings.ylim),'char')
                            vtset.results{i}.settings.ylim = vtset.results{vtset.treenum}.settings.ylim;
                        else
                            vtset.results{i}.settings.ylims(j,:) = ylimit(k,:);
                            vtset.results{i}.settings.ylim = ylimit(k,:);                            
                        end
                        break;
                    end
                end
            end
        end
    end
    
function setVisibiltyAxes(channel)
    global vtset
    if any(~ishandle(vtset.haxes))
        % reinit the whole figure, i guess someone closed the treeview and
        % tries to make it appear again
        initFigure();
        redraw();
        showtree();
    end


if ~vtset.combinedOutput && channel == numel(vtset.haxesvisible)
    % do not hide the tree if shown in different window
    return
end

if vtset.haxesvisible(channel)
    vtset.haxesvisible(channel)=0;
    vtset.results{vtset.treenum}.settings.visiblePlots(channel) = 0;
    set(vtset.hcheckboxMenu(channel),'checked','off')
else
    vtset.haxesvisible(channel)=1;
    vtset.results{vtset.treenum}.settings.visiblePlots(channel) = 1;
    set(vtset.hcheckboxMenu(channel),'checked','on')
end

set(vtset.haxes(vtset.haxesvisible==0),'position',[10 10 eps eps])

ids = find(vtset.haxesvisible);
if ~vtset.combinedOutput
    % do not consider tree when in different window
    ids(end)=[];
end
for i = 1:numel(ids)
    id = ids(i);
    set(vtset.haxes(id),'outerposition',[0 1-i/(numel(ids)) 1 1/(numel(ids))]);
end

vtset.showtree = vtset.haxesvisible(end);

for i = 1:numel(vtset.haxes)
    xlabel(vtset.haxes(i),'')
end
xlabel(vtset.haxes(ids(end)),'Time [h]')

% rescale tree view
% oldpos = get(vtset.haxes(end),'position');
% coursepos = get(vtset.haxes(find(vtset.haxesvisible,1,'first')),'position');
% set(vtset.haxes(end),'position',[coursepos(1) oldpos(2) coursepos(3) oldpos(4)])

vtset.tools.alignHaxes();



function resizeFigure(hObject,handles,something)
global vtset
if numel(vtset.haxes)>1
    oldpos = get(vtset.haxes(end),'position');
    coursepos = get(vtset.haxes(find(vtset.haxesvisible,1,'first')),'position');
    set(vtset.haxes(end),'position',[coursepos(1) oldpos(2) coursepos(3) oldpos(4)])
end


function redraw
global vtset opp

vtset.results{vtset.treenum}.settings.anno = strrep(vtset.results{vtset.treenum}.settings.anno,'wl02','wavelength_2');
vtset.results{vtset.treenum}.settings.anno2 = strrep(vtset.results{vtset.treenum}.settings.anno2,'wl02','wavelength_2');
vtset.results{vtset.treenum}.settings.anno = strrep(vtset.results{vtset.treenum}.settings.anno,'wl03','wavelength_3');
vtset.results{vtset.treenum}.settings.anno2 = strrep(vtset.results{vtset.treenum}.settings.anno2,'wl03','wavelength_3');


if ~isfield(vtset.results{vtset.treenum}.quants,'absoluteTime')
    vtset.timemode='timepoint';
end

% handle show quantification menu
% has to be init in treelist callback
if isfield(vtset,'checkboxMenu')
    % do the stuff
    vtset.checkboxMenu = str2double(vtset.checkboxMenu);
    
    if ismember(vtset.checkboxMenu, vtset.showquantification) 
        % remove it
        if numel(vtset.showquantification)==1
            fprintf('will not remove last quantification\n')
            vtset = rmfield(vtset,'checkboxMenu');
            return
        end
        vtset.showquantification(vtset.showquantification == vtset.checkboxMenu) = [];
        set(vtset.hcheckboxMenu(vtset.checkboxMenu),'checked','off')
    else
        % add it
        vtset.showquantification(end+1) = vtset.checkboxMenu;
        set(vtset.hcheckboxMenu(vtset.checkboxMenu),'checked','on')
    end
    
    %remove it
    vtset = rmfield(vtset,'checkboxMenu');
end


try
    curtree = vtset.results{vtset.treenum}.quants;
    
    % gather cells   
%     cells = vtset.showonlycells;
%     if strcmpi(cells,'all')
        cells = unique([curtree(:).cellNr]);
%     else
%         f = strfind(cells,'>g');
%         if f
%             gen = str2double(cells(f+2))+1;
%             cells(f:f+2) =[];
%             cells = [cells ' ' num2str(2^gen:max(vtset.results{vtset.treenum}.nonFluor.cellNr))];
%         end
%         cells=strrep(cells, '-',':');
%         cells=strrep(cells, ',',' ');
%         cells=['[' cells ']' ];
%         cells=eval(cells);
%         cells=cells(ismember(cells, unique([curtree(:).cellNr])));
%     end
%     
%     if isfield(vtset,'hide')
%         % get children
%         children= [str2double(vtset.hide) getchildren(str2double(vtset.hide),max(unique([curtree(:).cellNr])+1))];
%         % hide the branch
%         cells=setdiff(cells,children);
%         vtset.colors.visible(~ismember(cells,vtset.colors.cell))=0;
%         vtset.colors.visible(ismember(cells,vtset.colors.cell))=1;
%         vtset.showonlycells=num2str(torow(cells));
%         % delete hide 
%         vtset=rmfield(vtset,'hide');
%     end
%     if isfield(vtset,'show')
%         % get children
%         children= [str2double(vtset.show) getchildren(str2double(vtset.show),max(unique([curtree(:).cellNr])+1))];
%         % hide the branch
%         cells=unique([cells children]);
%         vtset.colors.visible(~ismember(cells,vtset.colors.cell))=0;
%         vtset.colors.visible(ismember(cells,vtset.colors.cell))=1;
%         vtset.showonlycells=num2str(torow(cells));
%         % delete hide 
%         vtset=rmfield(vtset,'show');
%     end
    
    if numel(cells)<1
        warndlg(sprintf('No cells in selected range.\nEnter ''all'' in ''Show only cell'' field and press return'));
        return
    end
    
 
    vtset.cells=cells;
    
    % create figure, set callback

    % plot the stuff

    
    for quantChannel = vtset.showquantification
        % skip if not selected in menu
        hold(vtset.haxes(quantChannel),'on')
        mylegend={};


%         hold on;

        % print the whole tree
        for i=1:numel(cells)
            cell=cells(i);
            plotCell(cell,quantChannel,vtset.haxes(quantChannel));       
        end
        
        
        % draw zero line for prodrate
%         if (strcmp(vtset.plotmode, 'prodrate') || vtset.doublethes)
%             h=line([0,max(get(gca,'xlim'))], [0,0], 'LineStyle',  '--', 'Color', 'black');
%             mylegend=[mylegend,'zero line'];
%         end
        
        
        %         set(get(vtset.hplot,'currentaxes'),'Position',[.1 .1 .85 .82])
        htitle = title(vtset.haxes(quantChannel),sprintf('%s %s',strrep(strrep(vtset.results{vtset.treenum}.settings.treefile,'_','\_'),'.ttt',''),vtset.channels{quantChannel}));
        set(htitle,'buttonDownFcn','muh')
%         set(htitle,'ButtonDownFcn',['global vtset;vtset.setVisibiltyAxes(' 'muh' ')'])

        set(vtset.haxes(quantChannel),'Xticklabel',[]);
        

        

%         legend(mylegend);
%         legend('off');

        
        
        
        %%% read params from colors struct and recolor the figure
        
            

            
            %%%% set figure settings
            %%% grid
            if vtset.colors.grid
                grid(vtset.haxes(quantChannel),'on')
            else
                grid(vtset.haxes(quantChannel),'off')
            end
            
            %%%legend
%             if strcmp(vtset.colors.legend,'<off>')
%                 legend('off')
%             elseif strcmp(vtset.colors.legend,'Left Inside')
%                 legend('Location','NorthWest')
%             elseif strcmp(vtset.colors.legend,'Right Inside')
%                 legend('Location','NorthEast')
%             elseif strcmp(vtset.colors.legend,'Left Outside')
%                 legend('Location','NorthWestOutside')
%             elseif strcmp(vtset.colors.legend,'Right Outside')
%                 legend('Location','NorthEastOutside')
%             end
            
            %%%bgcolor
            % TODO
            
            %%%% scale x/y axis
%             set(gca,'xlim',vtset.colors.xaxisrange)
%             set(gca,'ylim',vtset.colors.yaxisrange)
            
            
            %%%% set title/labels
%             xlabel(vtset.colors.xlabel)
%             ylabel(vtset.colors.ylabel)
%             title(vtset.colors.title)
        
    end

    
    % go into data cursor mode, set callback
%     figure(vtset.hplot);
    xlabel(vtset.haxes(end),'Time [h]')

    
    
    vtset.datacursor.Mgr  = datacursormode(vtset.hplot);
    %         cursorObj = datacursormode(xxxxx);
    %         set(cursorObj, 'enable','on', 'UpdateFcn',@dataTipsTxt);
    % set(xxxxx, 'Pointer', 'crosshair');
    % set(xxxxx, 'ButtonDownFcn',get(xxxxx,'WindowButtonDownFcn'));
    % %         set(xxxxx, 'WindowButtonMotionFcn',@windown);
    %             pause(0.2);
    %             modeMgr = get(xxxxx,'ModeManager');
    %             modeMgr.CurrentMode.WindowButtonDownFcn{1} = @windown;
    %             modeMgr.CurrentMode.ButtonDownFilter = @true;
    %              warning off MATLAB:uitools:uimode:callbackerror
    set(vtset.datacursor.Mgr ,'UpdateFcn',@customDC, 'Enable','on','DisplayStyle','datatip')   ;
    
    %make datatip window appear in uppper left corner
%     w = findobj('Tag','figpanel');
%     pos = get(vtset.hplot,'Position');
%     set(w,'Position',[0 pos(4)-60 120 60]);
    
    
    %datacursormode on
    
    
    %     deltafiltercounter
   
%       showtree(curtree,mylegend)
  
     

    % make the mouse over function. only data cursor mode over timecourse,
    % normal mouse over treeview
    % set mousebutton callbacks
    vtset.innerhalb=false;
    set(vtset.hplot,'windowbuttonmotionfcn',@wbmcb);
    % set(gcf,'WindowButtonUpFcn',@wbucb);
%     set(vtset.hplot,'WindowButtonDownFcn',@wbdcb);
    


% and align them correctly
vtset.tools.alignHaxes();


catch exception
    
    msgbox('A plotting error occured','Error','error')
    rethrow(exception)
end

% check for open outlier post processing
if isfield(opp,'figure1') && ishandle(opp.figure1)
    optimizeSegmentationGUI();
end

function h=plotCell(cell,quantChannel,haxes)
global vtset

[alldata, alltps, order] = calculateTimeCourse(cell, quantChannel, vtset.plotmode,vtset.smoothgraph);
%[alldata, alltps] = calculateTimeCourse(cell, quantChannel, 'linearregression',vtset.smoothgraph);

if vtset.drawmode.inspected == 1
    % show only inspected
    insp=vtset.results{vtset.treenum}.quants(quantChannel).inspected(vtset.results{vtset.treenum}.quants(quantChannel).cellNr==cell & vtset.results{vtset.treenum}.quants(quantChannel).active==1);
    insp = insp(order);
    alldata = alldata(insp==1);
    alltps = alltps(insp==1);
    insp = insp(insp==1);
elseif vtset.drawmode.inspected == 2
    % show only non inspected
    insp=vtset.results{vtset.treenum}.quants(quantChannel).inspected(vtset.results{vtset.treenum}.quants(quantChannel).cellNr==cell & vtset.results{vtset.treenum}.quants(quantChannel).active==1);
    insp = insp(order);
    alldata = alldata(insp==0);
    alltps = alltps(insp==0);
    insp = insp(insp==0);
else
    insp=vtset.results{vtset.treenum}.quants(quantChannel).inspected(vtset.results{vtset.treenum}.quants(quantChannel).cellNr==cell & vtset.results{vtset.treenum}.quants(quantChannel).active==1);
    insp = insp(order);
end

visi = {'off','on'};



ids = vtset.hcurvescells == cell  & vtset.hcurveschannel == quantChannel;
% update data
if sum(ids)>0
%     delete(vtset.hcurves(ids))
%     vtset.hcurves(ids)=[];
%     vtset.hcurvescells(ids)=[];
%     vtset.hcurveschannel(ids)=[];
%     vtset.hcurvestree(ids)=[];
%     vtset.hcurvesaxes(ids) = [];
    set(vtset.hcurves(ids),'xdata',alltps)
    set(vtset.hcurves(ids),'ydata',alldata)
    set(vtset.hcurves(ids),'visible',visi{vtset.colors.visible(vtset.colors.cell == cell)+1})
    set(vtset.hcurves(ids),'color',vtset.colors.color{vtset.colors.cell == cell})
    set(vtset.hcurves(ids),'linestyle',vtset.colors.linestyle{vtset.colors.cell == cell})
    set(vtset.hcurves(ids),'marker',vtset.colors.markerstyle{vtset.colors.cell == cell})
    set(vtset.hcurves(ids),'parent',haxes)
    return
end

% hold(haxes,'on')


lnst={vtset.colors.linestyle{vtset.colors.cell == cell},vtset.colors.linestyle{vtset.colors.cell == cell},'-','--','--'};
lnwd={vtset.colors.linewidth(vtset.colors.cell == cell),vtset.colors.linewidth(vtset.colors.cell == cell),2,2};
style={vtset.colors.markerstyle{vtset.colors.cell == cell},'o','o','*'};
% clr={[.3 .3 .3],[0 1 0 ],[1 0 0]};
% annolegend={'','anno1+','anno2+','anno 1+2'};

% create annotation vector
annovec=zeros(1,numel(alldata));
if isfield(vtset.results{vtset.treenum}.settings,'anno') && isfield(vtset.results{vtset.treenum}.quants(quantChannel),vtset.results{vtset.treenum}.settings.anno)
    annovec=torow(vtset.results{vtset.treenum}.quants(quantChannel).(vtset.results{vtset.treenum}.settings.anno)(vtset.results{vtset.treenum}.quants(quantChannel).cellNr==cell & vtset.results{vtset.treenum}.quants(quantChannel).active==1));
%     annovec = annovec+any(vtset.results{vtset.treenum}.nonFluor.(vtset.results{vtset.treenum}.settings.anno)(vtset.results{vtset.treenum}.nonFluor.cellNr == cell));
end
if isfield(vtset.results{vtset.treenum}.settings,'anno2') && isfield(vtset.results{vtset.treenum}.quants(quantChannel),vtset.results{vtset.treenum}.settings.anno2)
    annovec=annovec+2*torow(vtset.results{vtset.treenum}.quants(quantChannel).(vtset.results{vtset.treenum}.settings.anno2)(vtset.results{vtset.treenum}.quants(quantChannel).cellNr==cell & vtset.results{vtset.treenum}.quants(quantChannel).active==1));
%     annovec = annovec+2*any(vtset.results{vtset.treenum}.nonFluor.(vtset.results{vtset.treenum}.settings.anno2)(vtset.results{vtset.treenum}.nonFluor.cellNr == cell));
end
% try to show inspected timepoints
% annovec= annovec+insp;
%plot data
try
    for curanno=0:3
        range=find(annovec==curanno);
        if sum(range)>0
            h=plot(haxes,alltps(max(1,range(1)-1):range(end)),alldata(max(1,range(1)-1):range(end)),...
                style{curanno+1},... % MarkerType                
                'Color', vtset.colors.color{vtset.colors.cell == cell},... %'Color', clr{curanno+1},... 
                'LineStyle', lnst{curanno+1},... 
                'LineWidth',lnwd{curanno+1});
            vtset.hcurves(end+1)=h;
            vtset.hcurvescells(end+1)=cell;
            vtset.hcurveschannel(end+1)=quantChannel;
            %         vtset.hcurvestree(end+1)=0;
            vtset.hcurvesaxes(end+1) = haxes;
            %         mylegend=[mylegend,sprintf('Cell %d %s', cell,annolegend{curanno+1})];
        end
    end
catch e
    fprintf('Plotting error at cell %d, in plot channel %d\n',cell,quantChannel);
%     rethrow(e)
end



% allows to combine two (or more?) channels into one axes, with fixed
% colorcode and marker style
function combineTimecourses
global vtset

% ask which channels
channels = {};
for x = 1:numel(vtset.results{vtset.treenum}.quants)
    channels{end+1} = ['Q: ' vtset.results{vtset.treenum}.quants(x).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(x).settings.detectchannel).settings.wl];
end
list = listdlg('Promptstring','Please select channels to combine:', 'Name','Combine Timecourses',...
            'Liststring',channels,'ListSize',[200 200],'SelectionMode','multiple');
if numel(list)<2
    return
end
ch1 =list(1);
ch2 = list(2);

% hide one axes and rearrange
setVisibiltyAxes(ch2);

% calc linespec depending on number of cells in generation and write into
% vtset.colors
allgen = torow(floor(log2(vtset.colors.cell(vtset.colors.visible==1)))+1);
allcells= torow(vtset.colors.cell(vtset.colors.visible==1));
for gen = allgen
    % numel of gen
    ids = find(allgen == gen);
    numgen = numel(ids);
    
    % if only up to 16 cells make different styles
    if numgen<17
        style={'-','--',':','-.'};
        marker={'.','+','o','square'};
        for i = 1:numgen
            vtset.colors.linestyle{vtset.colors.cell == allcells(ids(i))} = '-';%style{i};
            vtset.colors.color{vtset.colors.cell == allcells(ids(i))} = [0 0 1];
            vtset.colors.markerstyle{vtset.colors.cell == allcells(ids(i))} = '.';%marker{i};
        end
        % else make a gradient
    else
        % iterate over all cells light to dark
        makeitbrighter=linspace(0.8,0,numgen);
        for i = 1:numgen
            vtset.colors.color{vtset.colors.cell == allcells(ids(i))} = [makeitbrighter(i) makeitbrighter(i) 1];
        end
    end
end
% plot all cells
for c = allcells
    plotCell(c,ch1,vtset.haxes(ch1))
end


% same with other channel
for gen = allgen
    % numel of gen
    ids = find(allgen == gen);
    numgen = numel(ids);
    if numgen<17
        style={'-','--',':','-.'};
        marker={'.','+','o','square'};
        for i = 1:numgen
%             vtset.colors.linestyle{vtset.colors.cell == allcells(ids(i))} = ':';%style{i};
            vtset.colors.color{vtset.colors.cell == allcells(ids(i))} = [1 0 0];
%             vtset.colors.markerstyle{vtset.colors.cell == allcells(ids(i))} = 'o';%marker{i};
        end
        
    else
        % iterate over all cells light to dark
        makeitbrighter=linspace(0.8,0,numgen);
        for i = 1:numgen
            vtset.colors.color{vtset.colors.cell == allcells(ids(i))} = [1 makeitbrighter(i) makeitbrighter(i)];
        end
    end
end
for c = allcells
    plotCell(c,ch2,vtset.haxes(ch1))
end

%make tree black
% for c= allcells
%     vtset.colors.color{vtset.colors.cell == c} = [ 0 0 0];
% end
% showtree

% and hide rest
setVisibilty(0)
vtset.tools.alignHaxes();



function wbmcb(src,evnt)
global vtset
% handles = guidata(src);
if ~vtset.combinedOutput
    % always draussen
%     if vtset.innerhalb
        vtset.innerhalb = false;
        datacursormode(vtset.hplot,'on')
        return
%     end
end
try
act= get(vtset.haxes(end),'CurrentPoint');
vtset.cursor=act(1,1:2);
xlim=get(vtset.haxes(end),'xlim');
ylim=get(vtset.haxes(end),'ylim');
% (ylim(1)<vtset.cursor(2)<ylim(2))
% (xlim(1)<vtset.cursor(1)<xlim(2))
% (ylim(1)<vtset.cursor(2)<ylim(2))
if xlim(1)<vtset.cursor(1) && vtset.cursor(1)<xlim(2) && ylim(1)<vtset.cursor(2) && vtset.cursor(2)<ylim(2)
%     'drin' inside tree axis
    if ~vtset.innerhalb
        vtset.innerhalb = true;
        datacursormode(vtset.hplot,'off')
    end
        
else 
%     'draussen'
    if vtset.innerhalb
        vtset.innerhalb = false;
        datacursormode(vtset.hplot,'on')
    end
end
catch e
    % could not find tree axis
end

function setPlotLineWidth(size,cell,branch)
global vtset;

if branch
    cell = [cell getchildren(cell,max(vtset.results{vtset.treenum}.nonFluor.cellNr)+1)];
end
% restrict to cells
cell = unique(vtset.results{vtset.treenum}.nonFluor.cellNr(ismember(vtset.results{vtset.treenum}.nonFluor.cellNr,cell)));
% save all colors
if (size == -1)
        vtset.colors.linewidth(1:numel(vtset.results{vtset.treenum}.nonFluor)) = 1;
else       
    for c = cell
        vtset.colors.linewidth(c) = size;   
    end
end

% set all timecourse linewidths
set(vtset.hcurves(ismember(vtset.hcurvescells,cell)),'linewidth',size);



function setPlotLineStyle(style,cell,branch)
global vtset;

if branch
    cell = [cell getchildren(cell,max(vtset.results{vtset.treenum}.nonFluor.cellNr)+1)];
end
% restrict to cells
cell = unique(vtset.results{vtset.treenum}.nonFluor.cellNr(ismember(vtset.results{vtset.treenum}.nonFluor.cellNr,cell)));
% save all colors
if (style == -1)
        vtset.colors.linestyle{1:numel(vtset.results{vtset.treenum}.nonFluor)} = '-';
else       
    for c = cell
        vtset.colors.linestyle{c} = style;   
    end
end

% set all timecourse linestyles
set(vtset.hcurves(ismember(vtset.hcurvescells,cell)),'linestyle',style);

function setCellMarker(marker,cell, branch)
global vtset
% save marker
if strcmp(marker,'Reset')
    marker = '.';
end
if branch
    cell = [cell getchildren(cell,max(vtset.results{vtset.treenum}.nonFluor.cellNr)+1)];
end
% restrict to cells
cell = unique(vtset.results{vtset.treenum}.nonFluor.cellNr(ismember(vtset.results{vtset.treenum}.nonFluor.cellNr,cell)));

%set all marker
for c = cell
vtset.colors.markerstyle{vtset.colors.cell == c } = marker;
end
% set all timecourse colors
set(vtset.hcurves(ismember(vtset.hcurvescells,cell)),'marker',marker);

% set all tree colors
set(vtset.htreelines(ismember(vtset.htreecells,cell)& ~vtset.htreeannotation),'marker', marker);

function setCellColor(color,cell,branch,handle)
global vtset

% set(str2double(handle),'checked','on')

if branch   
   cell = [cell getchildren(cell,max(vtset.results{vtset.treenum}.nonFluor.cellNr)+1)];
end
% restrict to cells
cell = unique(vtset.results{vtset.treenum}.nonFluor.cellNr(ismember(vtset.results{vtset.treenum}.nonFluor.cellNr,cell)));
% save all colors
l = lines;
for c = cell
    if strcmp(color,'Reset')
        vtset.colors.color{vtset.colors.cell == c } = l(mod(c,64)+1,:);
    else
        vtset.colors.color{vtset.colors.cell == c } = eval(color);
    end
end

% set all timecourse colors
if strcmp(color,'Reset')
    for c = cell
        set(vtset.hcurves(ismember(vtset.hcurvescells,c)),'color', l(mod(c,64)+1,:));
    end
else
    set(vtset.hcurves(ismember(vtset.hcurvescells,cell)),'color',eval(color));
end
% set all tree colors
if strcmp(color,'Reset')
    for c = cell
        set(vtset.htreelines(ismember(vtset.htreecells,c)& ~vtset.htreeannotation),'color', l(mod(c,64)+1,:));
    end
else
    set(vtset.htreelines(ismember(vtset.htreecells,cell)& ~vtset.htreeannotation),'color',eval(color));
end


function setVisibilty(tochange)

global vtset
if ~ishandle(vtset.hplot)
    redraw
end

% gather visible cells
cells = vtset.showonlycells;
% if strcmpi(cells,'all')
%     cells = unique([vtset.results{vtset.treenum}.quants(1).cellNr]);
% else
%     f = strfind(cells,'>g');
%     if f
%         gen = str2double(cells(f+2))+1;
%         cells(f:f+2) =[];
%         cells = [cells ' ' num2str(2^gen:max(vtset.results{vtset.treenum}.nonFluor.cellNr))];
%     end
%     cells=strrep(cells, '-',':');
%     cells=strrep(cells, ',',' ');
%     cells=['[' cells ']' ];
%     cells=eval(cells);
%     cells=cells(ismember(cells, unique([vtset.results{vtset.treenum}.quants(1).cellNr])));
% end

if isfield(vtset,'show')
    tochange = str2double(vtset.show);
    hide= 0;
    vtset=rmfield(vtset,'show');
elseif isfield(vtset,'hide')
    tochange = str2double(vtset.hide);
    hide = 1;
    vtset=rmfield(vtset,'hide');
elseif tochange == 0
    hide = -1;
else
    visi = get(vtset.hcurves(vtset.hcurvescells==tochange),'visible');
    if iscell(visi)
        visi=visi{1};
    end
    if strcmp(visi,'off')
        hide=0;
    else
        hide=1;
    end
end

if hide==1
    % get children
    children= [tochange getchildren(tochange,max(unique([vtset.results{vtset.treenum}.nonFluor.cellNr])+1))];
    % hide the branch
    cells=setdiff(cells,children);
%     vtset.colors.visible(~ismember(cells,vtset.colors.cell))=0;
%     vtset.colors.visible(ismember(cells,vtset.colors.cell))=1;
    vtset.showonlycells=(torow(cells));
    % delete hide
%     vtset=rmfield(vtset,'hide');
elseif hide == 0
    % get children
    children= [tochange getchildren(tochange,max(unique([vtset.results{vtset.treenum}.nonFluor.cellNr])+1))];
    % hide the branch
    cells=unique([cells children]);
%     vtset.colors.visible(~ismember(cells,vtset.colors.cell))=0;
%     vtset.colors.visible(ismember(cells,vtset.colors.cell))=1;
    vtset.showonlycells=(torow(cells));
    % delete hide
%     vtset=rmfield(vtset,'show');
end
% visi = {'off','on'};

% set visible of alltimecourses
set(vtset.hcurves(ismember( vtset.hcurvescells,cells)),'visible','on')
set(vtset.hcurves(~ismember(vtset.hcurvescells,cells)),'visible','off')

% set properties
vtset.colors.visible(ismember( vtset.colors.cell,cells)) = 1;
vtset.colors.visible(~ismember( vtset.colors.cell,cells)) = 0;

% set visible of tree view elements
set(vtset.htreelines(ismember(vtset.htreecells,cells)),'visible','on')
set(vtset.htreelines(~ismember(vtset.htreecells,cells)),'visible','off')

% set xlim
if strcmp(vtset.results{vtset.treenum}.settings.xlim,'auto')
    set(vtset.haxes,'xLimMode','auto')
end


%%% alwys haxes(end)
function showtree(curtree, mylegend)
global vtset
%%%%% show tree
vtset.htreelines = [];
vtset.htreecells= [];
vtset.htreeannotation = [];

if vtset.combinedOutput
    if isfield(vtset,'htree') && numel(vtset.htree)
        try
        delete(vtset.htree)
        catch
            %nevermind
        end
        vtset.htree=[];
    end
    cla(vtset.haxes(end));
%     vtset.haxes(end+1)=subplot(numel(vtset.showquantification)+1,1,numel(vtset.showquantification)+1);
else
    % create figure, set callback
    if ~isfield(vtset,'htree') || numel(vtset.htree)==0
        vtset.htree=figure;
        set(get(vtset.htree,'currentaxes'),'Position',[.1 .1 .85 .85])
        scnsize = get(0,'ScreenSize');
        oldpos = get(vtset.htree,'Position');
        set(vtset.htree,'Position', [100 scnsize(4)-900 oldpos(3) oldpos(4)]);
        set(vtset.htree,'Name', 'Tree');
    else
        figure(vtset.htree);
        
%         oldpos = get(vtset.hplot,'Position');
%         set(vtset.htree,'Position', [oldpos(1) oldpos(2)-(oldpos(4)+92) oldpos(3) oldpos(4)]);
    end
%     cla(get(vtset.htree,'currentAxes'));
    cla(vtset.haxes(end));
    set(vtset.haxes(end),'parent',vtset.htree,'outerposition',[0 0 1 1]);
    set(vtset.haxes(end),'LooseInset', [0,0,0,0]);

%     vtset.haxes(end+1)=subplot('position',[0.1 0.1 0.85 0.85]);
    xlabel(vtset.haxes(end),'Time [h]')
end


data = vtset.results{vtset.treenum}.nonFluor(1);
tic
for c=1:numel(vtset.cells)
    cell=vtset.cells(c);
    % get index
    index = find(data.cellNr==cell );
    if numel(index)>0

        % get path to root
        rp = rootpath(cell);
        % determine x coordinate
        x = 0;
        for i=2:numel(rp)
            horlen = 1/2^(i-1);
            if mod(rp(i),2)==0
                % even number, left branch
                x = x - horlen;
            else
                % odd number, right branch
                x = x + horlen;
            end
        end
        
        
        % draw the line
        time = data.(vtset.timemode)(index)/vtset.timemodifier;
%         mymap = get(vtset.hcurves(vtset.hcurvescells == cell & vtset.hcurveschannel == vtset.hcurveschannel(1)),'Color');
        mymap = vtset.colors.color(vtset.colors.cell == cell);
        if iscell(mymap)
            mymap = mymap{1};
        end
        cl = 1;
        
        if c~=1
            temptime = data.(vtset.timemode)(data.cellNr == floor(cell/2))/vtset.timemodifier;
            if numel(temptime)
                time(1) = max(temptime);
            end
        else
            time(1) = min(time);
        end
        
        linwi = vtset.colors.treelinewidth(vtset.colors.cell==cell);

        if vtset.colors.dynamicLineWidth
            linwi = 10;
            gen = floor(log2(cell))+1;
        else
            gen= 1;
        end
               
        
        try
            h=line([time(1) max(time)],[x x],'color',mymap(cl,:),'linewidth',ceil(linwi/gen),'marker',vtset.colors.markerstyle{vtset.colors.cell == cell},'linestyle',vtset.colors.linestyle{vtset.colors.cell == cell},'parent',vtset.haxes(end));
        catch e
            rethrow(e)
        end
        vtset.htreelines(end+1)=h;
        vtset.htreecells(end+1)=cell;
        vtset.htreeannotation(end+1) = 0;
%         vtset.hcurveschannel(end+1)=1;
%         vtset.hcurvestree(end+1)=1;
%         vtset.htreesaxes(end+1) = vtset.haxes(end);

        % draw annotation change
        if isfield(vtset.results{vtset.treenum}.settings,'anno') && isfield(data,vtset.results{vtset.treenum}.settings.anno)
            allanno=data.(vtset.results{vtset.treenum}.settings.anno)(index);
            if ismember(1,allanno) && ismember(0,allanno)
                ap=find(allanno==1);
                ap=ap(1);
                aptp=time(ap);
                h=line( [aptp aptp], [x-0.05 x+0.05],'Color', 'black', 'LineWidth', 4,'parent',vtset.haxes(end));
                vtset.htreelines(end+1)=h;
                vtset.htreecells(end+1)=cell;
                vtset.htreeannotation(end+1) = 1;
            end
        
        end
        if isfield(vtset.results{vtset.treenum}.settings,'anno2') && isfield(data,vtset.results{vtset.treenum}.settings.anno2)
            allanno=data.(vtset.results{vtset.treenum}.settings.anno2)(index);
            if ismember(1,allanno) && ismember(0,allanno)
                ap=find(allanno==1);
                ap=ap(1);
                aptp=time(ap);
                h=line( [aptp aptp], [x-0.05 x+0.05],'Color', 'red', 'LineWidth', 4,'parent',vtset.haxes(end));
                vtset.htreelines(end+1)=h;
                vtset.htreecells(end+1)=cell;
                vtset.htreeannotation(end+1) = 1;
            end
        end
        
%         % connection to mother
%         if mod(cell,2)
% %             horlen = 1/2^(i-1);
%             x2 = x-1/2^floor(log2(cell));
%             hline = line( [min(time) min(time)],[x x2],'color','black');
%         else
%             x2 = x+1/2^floor(log2(cell));
%             hline = line( [min(time) min(time)],[x x2],'color','black');
%         end
%         set(hline,'uicontextmenu',hcmenu)
        
        % and the connection of the children, if there
        c1 = cell*2;
        c2 = cell*2+1;
        if numel(find(data.cellNr==c1))>0
%             x1 = x+(1/2^floor(log2(cell)+1));
            x2 = x-(1/2^floor(log2(cell)+1));
            hline = line( [max(time) max(time)],[x x2],'color','black','parent',vtset.haxes(end));
            displayAll(c1,cell, hline);
%             vtset.htreelines(end+1) = hline;
%             vtset.htreecells(end+1) = cell;
%             vtset.htreeannotation(end+1) = 1;
%             % Define a context menu; it is not attached to anything
%             hcmenu = uicontextmenu('parent',get(vtset.haxes(end),'parent'));
%             % Define callbacks for context menu items that change linestyle
%             hcb1 = ['global vtset;vtset.setVisibilty(' num2str(c1) ')'];
%             hcb2 = ['global vtset;vtset.setVisibilty(' num2str(c1) ')'];
%             % Define the context menu items and install their callbacks
%             item1 = uimenu(hcmenu, 'Label', 'Hide branch', 'Callback', hcb1);
%             item2 = uimenu(hcmenu, 'Label', 'Show branch', 'Callback', hcb2);
%             % open cell inspector
%             cb = ['global vtset;if ~isfield(vtset,''dcselected'');msgbox(''no channel selected'');return;end;vtset.dcselected.cell = ' num2str(c1) ';'...
%                 'vtset.dcselected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).cellNr == ' num2str(c1) '));'...
%                 'vtset.trackCell([],[],[]);'];
%             item2B = uimenu(hcmenu, 'Label', 'Inspect this cell', 'Callback', cb);
% 
%             % define color change callbacks & menu items
%             item3 = uimenu(hcmenu, 'Label', 'Cell color');
%             item31 = uimenu(item3, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(c1) ',0);']);
%             item32 = uimenu(item3, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(c1) ',0);']);
%             item33 = uimenu(item3, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(c1) ',0);']);
%             item34 = uimenu(item3, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(c1) ',0);']);
%             item35 = uimenu(item3, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(c1) ',0);']);
%             item36 = uimenu(item3, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0.25 0.25 0.25]'',' num2str(c1) ',0);']);
%             item37 = uimenu(item3, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(c1) ',0);']);
%             item38 = uimenu(item3, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(c1) ',0);']);
%             item39 = uimenu(item3, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(c1) ',0);']);
%             
%             item4 = uimenu(hcmenu, 'Label', 'Branch color');
%             item41 = uimenu(item4, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(c1) ',1);']);
%             item42 = uimenu(item4, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(c1) ',1);']);
%             item43 = uimenu(item4, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(c1) ',1);']);
%             item44 = uimenu(item4, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(c1) ',1);']);
%             item45 = uimenu(item4, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(c1) ',1);']);
%             item46 = uimenu(item4, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0.25 0.25 0.25]'',' num2str(c1) ',1);']);
%             item47 = uimenu(item4, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(c1) ',1);']);
%             item48 = uimenu(item4, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(c1) ',1);']);
%             item49 = uimenu(item4, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(c1) ',1);']);
%             
%             item5 = uimenu(hcmenu, 'Label', 'Cell marker');
%             item51 = uimenu(item5, 'Label', 'dot','Callback',['global vtset;vtset.tools.setCellMarker(''.'',' num2str(c1) ',0);']);
%             item52 = uimenu(item5, 'Label', 'plus','Callback',['global vtset;vtset.tools.setCellMarker(''+'',' num2str(c1) ',0);']);
%             item53 = uimenu(item5, 'Label', 'square','Callback',['global vtset;vtset.tools.setCellMarker(''square'',' num2str(c1) ',0);']);
%             item54 = uimenu(item5, 'Label', 'circle','Callback',['global vtset;vtset.tools.setCellMarker(''o'',' num2str(c1) ',0);']);
%             item55 = uimenu(item5, 'Label', 'cross','Callback',['global vtset;vtset.tools.setCellMarker(''x'',' num2str(c1) ',0);']);
%             item56 = uimenu(item5, 'Label', 'None','Callback',['global vtset;vtset.tools.setCellMarker(''none'',' num2str(c2) ',0);']);
%             
%             item6 = uimenu(hcmenu, 'Label', 'Line width');
%             item61 = uimenu(item6, 'Label', '1', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(1'',' num2str(c1) ');']);
%             item62 = uimenu(item6, 'Label', '2', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(2'',' num2str(c1) ');']);
%             item63 = uimenu(item6, 'Label', '3', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(3'',' num2str(c1) ');']);
%             item64 = uimenu(item6, 'Label', '4', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(4'',' num2str(c1) ');']);
%             item65 = uimenu(item6, 'Label', '5', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(5'',' num2str(c1) ');']);
%             item66 = uimenu(item6, 'separator','on','Label', 'Custom', 'Callback', ['global vtset; x=inputdlg(''Enter line with'',''Timecourse line width'',1);if isempty(x);return;end;vtset.tools.setPlotLineWidth(str2double(x)'',' num2str(c1) ');']);
%             
%             item7 = uimenu(hcmenu, 'Label', 'Line style');
%             item71 = uimenu(item7, 'Label', 'Solid', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-'',' num2str(c1) ');']);
%             item72 = uimenu(item7, 'Label', 'Dashed', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''--'',' num2str(c1) ');']);
%             item73 = uimenu(item7, 'Label', 'Dotted', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle('':'',' num2str(c1) ');']);
%             item74 = uimenu(item7, 'Label', 'Dash-dot line', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-.'',' num2str(c1) ');']);
%             item75 = uimenu(item7, 'Label', 'NoLine', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''none'',' num2str(c1) ');']);
%             % Attach the context menu to each line
%             set(hline,'uicontextmenu',hcmenu)
%             if ismember(c1,vtset.cells)
%                 %muoseside=get(gcf,'SelectionType');
%                 set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb1))
%             else
%                 set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb2))
%             end

        end
        if numel(find(data.cellNr==c2))>0
            x1 = x+(1/2^floor(log2(cell)+1));
%             x2 = x-(1/2^floor(log2(cell)+1));
            hline = line( [max(time) max(time)],[x x1],'color','black','parent',vtset.haxes(end));
            displayAll(c2,cell,hline);
%             vtset.htreelines(end+1) = hline;
%             vtset.htreecells(end+1) = cell;
%             vtset.htreeannotation(end+1) = 1;
%             % Define a context menu; it is not attached to anything
%             hcmenu = uicontextmenu('parent',get(vtset.haxes(end),'parent'));
%             % Define callbacks for context menu items that change linestyle
%             hcb1 = ['global vtset;vtset.setVisibilty(' num2str(c2) ')'];
%             hcb2 = ['global vtset;vtset.setVisibilty(' num2str(c2) ')'];
%             % Define the context menu items and install their callbacks
%             item1 = uimenu(hcmenu, 'Label', 'Hide branch', 'Callback', hcb1);
%             item2 = uimenu(hcmenu, 'Label', 'Show branch', 'Callback', hcb2);
%             % open cell inspector
%             cb = ['global vtset;if ~isfield(vtset,''dcselected'');msgbox(''no channel selected'');return;end;vtset.dcselected.cell = ' num2str(c2) ';'...
%                 'vtset.dcselected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).cellNr == ' num2str(c2) '));'...
%                 'vtset.trackCell([],[],[]);'];
%             item2B = uimenu(hcmenu, 'Label', 'Inspect this cell', 'Callback', cb);
% 
%             % define color change callbacks & menu items
%             item3 = uimenu(hcmenu, 'Label', 'Cell color');
%             item31 = uimenu(item3, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(c2) ',0);']);
%             item32 = uimenu(item3, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(c2) ',0);']);
%             item33 = uimenu(item3, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(c2) ',0);']);
%             item34 = uimenu(item3, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(c2) ',0);']);
%             item35 = uimenu(item3, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(c2) ',0);']);
%             item36 = uimenu(item3, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0.25 0.25 0.25]'',' num2str(c2) ',0);']);
%             item37 = uimenu(item3, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(c2) ',0);']);
%             item38 = uimenu(item3, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(c2) ',0);']);
%             item39 = uimenu(item3, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(c2) ',0);']);
%             
%             item4 = uimenu(hcmenu, 'Label', 'Branch color');
%             item41 = uimenu(item4, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(c2) ',1);']);
%             item42 = uimenu(item4, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(c2) ',1);']);
%             item43 = uimenu(item4, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(c2) ',1);']);
%             item44 = uimenu(item4, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(c2) ',1);']);
%             item45 = uimenu(item4, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(c2) ',1);']);
%             item46 = uimenu(item4, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0.25 0.25 0.25]'',' num2str(c2) ',1);']);
%             item47 = uimenu(item4, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(c2) ',1);']);
%             item48 = uimenu(item4, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(c2) ',1);']);
%             item49 = uimenu(item4, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(c2) ',1);']);
%             
%             item5 = uimenu(hcmenu, 'Label', 'Cell marker');
%             item51 = uimenu(item5, 'Label', 'dot','Callback',['global vtset;vtset.tools.setCellMarker(''.'',' num2str(c2) ',0);']);
%             item52 = uimenu(item5, 'Label', 'plus','Callback',['global vtset;vtset.tools.setCellMarker(''+'',' num2str(c2) ',0);']);
%             item53 = uimenu(item5, 'Label', 'square','Callback',['global vtset;vtset.tools.setCellMarker(''square'',' num2str(c2) ',0);']);
%             item54 = uimenu(item5, 'Label', 'circle','Callback',['global vtset;vtset.tools.setCellMarker(''o'',' num2str(c2) ',0);']);
%             item55 = uimenu(item5, 'Label', 'cross','Callback',['global vtset;vtset.tools.setCellMarker(''x'',' num2str(c2) ',0);']);
%             item56 = uimenu(item5, 'Label', 'None','Callback',['global vtset;vtset.tools.setCellMarker(''none'',' num2str(c2) ',0);']);
%             
%             item6 = uimenu(hcmenu, 'Label', 'Line width');
%             item61 = uimenu(item6, 'Label', '1', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(1'',' num2str(c2) ');']);
%             item62 = uimenu(item6, 'Label', '2', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(2'',' num2str(c2) ');']);
%             item63 = uimenu(item6, 'Label', '3', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(3'',' num2str(c2) ');']);
%             item64 = uimenu(item6, 'Label', '4', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(4'',' num2str(c2) ');']);
%             item65 = uimenu(item6, 'Label', '5', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(5'',' num2str(c2) ');']);
%             item66 = uimenu(item6, 'separator','on','Label', 'Custom', 'Callback', ['global vtset; x=inputdlg(''Enter line with'',''Timecourse line width'',1);if isempty(x);return;end;vtset.tools.setPlotLineWidth(str2double(x)'',' num2str(c2) ');']);
%             
%             item7 = uimenu(hcmenu, 'Label', 'Line style');
%             item71 = uimenu(item7, 'Label', 'Solid', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-'',' num2str(c2) ');']);
%             item72 = uimenu(item7, 'Label', 'Dashed', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''--'',' num2str(c2) ');']);
%             item73 = uimenu(item7, 'Label', 'Dotted', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle('':'',' num2str(c2) ');']);
%             item74 = uimenu(item7, 'Label', 'Dash-dot line', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-.'',' num2str(c2) ');']);
%             item75 = uimenu(item7, 'Label', 'NoLine', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''none'',' num2str(c2) ');']);
%             % Attach the context menu to each line
%             set(hline,'uicontextmenu',hcmenu)
%             if ismember(c2,vtset.cells)
%                 set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb1))
%             else
%                 set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb2))
%             end

        end
        
%         if vtset.showcellnumber
%             hline=text('position',[min(time)-1, x],'string',sprintf('%d',cell),'Interpreter','none','parent',vtset.haxes(end), 'EraseMode', 'none');
%             % Define a context menu; it is not attached to anything
%             hcmenu = uicontextmenu;
%             % Define callbacks for context menu items that change linestyle
%             hcb1 = ['global vtset;vtset.hide=''' num2str(cell) ''';vtset.updateCallback()'];
%             hcb2 = ['global vtset;vtset.show=''' num2str(cell) ''';vtset.updateCallback()'];
%             % Define the context menu items and install their callbacks
%             item1 = uimenu(hcmenu, 'Label', 'Hide branch', 'Callback', hcb1);
%             item2 = uimenu(hcmenu, 'Label', 'Show branch', 'Callback', hcb2);
%             % Attach the context menu to each line
%             set(hline,'uicontextmenu',hcmenu)
%             if ismember(cell,vtset.cells)
%                 set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb1))
%             else
%                 set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb2))
%             end
%         end
    end
    
end

%%%% workaround (plotting speed of line & text): extract cell numbers and plot afterwards:
% if vtset.showcellnumber
    
    for c=1:numel(vtset.cells)
        cell=vtset.cells(c);
        % get index
        index = find(data.cellNr==cell); %%%% TODO: only active?
        if numel(index)>0
            
            % get path to root
            rp = rootpath(cell);
            % determine x coordinate
            x = 0;
            for i=2:numel(rp)
                horlen = 1/2^(i-1);
                if mod(rp(i),2)==0
                    % even number, left branch
                    x = x - horlen;
                else
                    % odd number, right branch
                    x = x + horlen;
                end
            end
            time = data.(vtset.timemode)(index)/vtset.timemodifier;
            
            if vtset.showcellnumber
                % draw the cell number
                
                hline=text('position',[min(time)-2, x],'string',sprintf('%d',cell),'Interpreter','none','parent',vtset.haxes(end),'fontsize',10);
                displayAll(cell,cell,hline);
%                 vtset.htreelines(end+1) = hline;
%                 vtset.htreecells(end+1) = cell;
%                 vtset.htreeannotation(end+1) = 1;
%                 % Define a context menu; it is not attached to anything
%                 hcmenu = uicontextmenu('parent',get(vtset.haxes(end),'parent'));
%                 % Define callbacks for context menu items that change linestyle
%                 hcb1 = ['global vtset;vtset.hide=''' num2str(cell) ''';vtset.setVisibilty();'];
%                 hcb2 = ['global vtset;vtset.show=''' num2str(cell) ''';vtset.setVisibilty();'];
%                 % Define the context menu items and install their callbacks
%                 %             set(item1,'Callback',hcb1);
%                 %             set(item2,'Callback',hcb2);
%                 item1 = uimenu(hcmenu, 'Label', 'Hide branch', 'Callback', hcb1);
%                 item2 = uimenu(hcmenu, 'Label', 'Show branch', 'Callback', hcb2);
%                 % open cell inspector
%                 cb = ['global vtset;if ~isfield(vtset,''dcselected'');msgbox(''no channel selected'');return;end;vtset.dcselected.cell = ' num2str(cell) ';'...
%                     'vtset.dcselected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).cellNr == ' num2str(cell) '));'...
%                     'vtset.trackCell([],[],[]);'];
%                 item2B = uimenu(hcmenu, 'Label', 'Inspect this cell', 'Callback', cb);
%                 % define color change callbacks & menu items
%                 item3 = uimenu(hcmenu, 'Label', 'Cell color');
%                 item31 = uimenu(item3, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(cell) ',0);']);
%                 item32 = uimenu(item3, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(cell) ',0);']);
%                 item33 = uimenu(item3, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(cell) ',0);']);
%                 item34 = uimenu(item3, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(cell) ',0);']);
%                 item35 = uimenu(item3, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(cell) ',0);']);
%                 item36 = uimenu(item3, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0.25 0.25 0.25]'',' num2str(cell) ',0);']);
%                 item37 = uimenu(item3, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(cell) ',0);']);
%                 item38 = uimenu(item3, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(cell) ',0);']);
%                 item39 = uimenu(item3, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(cell) ',0);']);
%                 
%                 item4 = uimenu(hcmenu, 'Label', 'Branch color');
%                 item41 = uimenu(item4, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(cell) ',1);']);
%                 item42 = uimenu(item4, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(cell) ',1);']);
%                 item43 = uimenu(item4, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(cell) ',1);']);
%                 item44 = uimenu(item4, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(cell) ',1);']);
%                 item45 = uimenu(item4, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(cell) ',1);']);
%                 item46 = uimenu(item4, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0.25 0.25 0.25]'',' num2str(cell) ',1);']);
%                 item47 = uimenu(item4, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(cell) ',1);']);
%                 item48 = uimenu(item4, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(cell) ',1);']);
%                 item49 = uimenu(item4, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(cell) ',1);']);
%                 
%                 item5 = uimenu(hcmenu, 'Label', 'Cell marker');
%                 item51 = uimenu(item5, 'Label', 'dot','Callback',['global vtset;vtset.tools.setCellMarker(''.'',' num2str(cell) ',0);']);
%                 item52 = uimenu(item5, 'Label', 'plus','Callback',['global vtset;vtset.tools.setCellMarker(''+'',' num2str(cell) ',0);']);
%                 item53 = uimenu(item5, 'Label', 'square','Callback',['global vtset;vtset.tools.setCellMarker(''square'',' num2str(cell) ',0);']);
%                 item54 = uimenu(item5, 'Label', 'circle','Callback',['global vtset;vtset.tools.setCellMarker(''o'',' num2str(cell) ',0);']);
%                 item55 = uimenu(item5, 'Label', 'cross','Callback',['global vtset;vtset.tools.setCellMarker(''x'',' num2str(cell) ',0);']);
%                 item56 = uimenu(item5, 'Label', 'None','Callback',['global vtset;vtset.tools.setCellMarker(''none'',' num2str(cell) ',0);']);
%                 
%                 item6 = uimenu(hcmenu, 'Label', 'Line width');
%                 item61 = uimenu(item6, 'Label', '1', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(1'',' num2str(cell) ');']);
%                 item62 = uimenu(item6, 'Label', '2', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(2'',' num2str(cell) ');']);
%                 item63 = uimenu(item6, 'Label', '3', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(3'',' num2str(cell) ');']);
%                 item64 = uimenu(item6, 'Label', '4', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(4'',' num2str(cell) ');']);
%                 item65 = uimenu(item6, 'Label', '5', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(5'',' num2str(cell) ');']);
%                 item66 = uimenu(item6,'separator','on', 'Label', 'Custom', 'Callback', ['global vtset; x=inputdlg(''Enter line with'',''Timecourse line width'',1);if isempty(x);return;end;vtset.tools.setPlotLineWidth(str2double(x)'',' num2str(cell) ');']);
%                 
%                 item7 = uimenu(hcmenu, 'Label', 'Line style');
%                 item71 = uimenu(item7, 'Label', 'Solid', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-'',' num2str(cell) ');']);
%                 item72 = uimenu(item7, 'Label', 'Dashed', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''--'',' num2str(cell) ');']);
%                 item73 = uimenu(item7, 'Label', 'Dotted', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle('':'',' num2str(cell) ');']);
%                 item74 = uimenu(item7, 'Label', 'Dash-dot line', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-.'',' num2str(cell) ');']);
%                 item75 = uimenu(item7, 'Label', 'NoLine', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''none'',' num2str(cell) ');']);
%                 % Attach the context menu to each line
%                 set(hline,'uicontextmenu',hcmenu)
%                 % also set to whole cell line in treeview
%                 set(vtset.htreelines(vtset.htreecells == cell & vtset.htreeannotation==0),'uicontextmenu',hcmenu)
%                 if ismember(cell,vtset.cells)
%                     set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb1))
%                     % also set to whole cell line in treeview
%                     set(vtset.htreelines(vtset.htreecells == cell & vtset.htreeannotation==0),'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb1))
%                 else
%                     set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb2))
%                     set(vtset.htreelines(vtset.htreecells == cell & vtset.htreeannotation==0),'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb2))
%                 end

            end
            % stop reason
            sr = vtset.results{vtset.treenum}.nonFluor.stopReason(vtset.results{vtset.treenum}.nonFluor.cellNr == cell);
            sr = sr(1);
            if sr == 2
                % death
                hline=text('position',[max(time)+1, x],'string',sprintf('x'),'Interpreter','none','parent',vtset.haxes(end),'fontsize',8);
                vtset.htreelines(end+1) = hline;
                vtset.htreecells(end+1) = cell;
                vtset.htreeannotation(end+1) = 1;
            elseif sr == 3
                % lost
                hline=text('position',[max(time)+1, x],'string',sprintf('?'),'Interpreter','none','parent',vtset.haxes(end),'fontsize',8);
                vtset.htreelines(end+1) = hline;
                vtset.htreecells(end+1) = cell;
                vtset.htreeannotation(end+1) = 1;
            end
        end
    end
% end


% xscale = get(vtset.haxes(1),'xlim');
% set(vtset.haxes(end),'xlim',xscale);
set(vtset.haxes(end),'YTick',[])
% oldpos = get(vtset.haxes(end),'position');
% coursepos = get(vtset.haxes(end-1),'position');
% set(vtset.haxes(end),'position',[coursepos(1) oldpos(2) coursepos(3) oldpos(4)])
xlabel(vtset.haxes(end),'Time [h]')
if vtset.colors.grid
    grid(vtset.haxes(end),'on')
else
    grid(vtset.haxes(end),'off')
end
% legend(mylegend);
% legend('off');
vtset.tools.alignHaxes();
setVisibilty(0);
toc



% update function for data cursor
function output_txt = customDC(obj,event_obj)
global vtset

if (numel(getCursorInfo(vtset.datacursor.Mgr)) == 1)
    vtset.dcselectedarray = [];
end
% find the cell that was clicked

cell = vtset.hcurvescells((vtset.hcurves==event_obj.Target));
channel = vtset.hcurveschannel((vtset.hcurves==event_obj.Target));
abstime = event_obj.Position(1);

%change marker style
h=vtset.datacursor.Mgr.CurrentDataCursor;
set(h,'Marker','o');
set(h,'MarkerEdgeColor',[1 .5 0])
set(h,'markerSize',14);
set(h,'MarkerFaceColor','none')

if isempty(cell)
    output_txt = 'Please select a cell';
    return
end

vtset.dcselected.cell = cell;%allCells(cell);

% get real timepoint out of absolute time !!! warning rounding error may
% occur

timepoint=vtset.results{vtset.treenum}.quants(channel).timepoint(vtset.results{vtset.treenum}.quants(channel).cellNr==cell ...
    & vtset.results{vtset.treenum}.quants(channel).(vtset.timemode)/vtset.timemodifier==abstime);
vtset.dcselected.timepoint = timepoint;
vtset.dcselected.channel = channel; %refers to the selected quantification channel

    
%update dc selected array
currentselected.cell = cell;
currentselected.timepoint = timepoint;
currentselected.channel = channel;
contains = false;
for i=1:numel(vtset.dcselectedarray)
    if(vtset.dcselectedarray(i).cell == cell && vtset.dcselectedarray(i).channel == channel && vtset.dcselectedarray(i).timepoint == timepoint)
        contains = true;
    end
end
if (~contains)
    vtset.dcselectedarray = [vtset.dcselectedarray currentselected];
end



%recalc time to hh:mm
hours=floor(abstime);
minutes=mod(abstime,1);
minutes=minutes*60;

% set label

output_txt = sprintf('cell %d, timepoint %d\ntime %d h %.1f m\nValue: %f', cell, ...
    timepoint,hours,minutes, event_obj.Position(2));

% hide it possilby
try
    if ~vtset.datacursor.showtip
        set(vtset.datacursor.Mgr.CurrentDataCursor.textBoxHandle,'Visible','off')
    end
catch e
end

% highlight selected cell in timecourses
try
    set(vtset.hcurves,'linewidth',vtset.colors.linewidth(1))
    set(vtset.hcurves(vtset.hcurvescells == cell),'linewidth',vtset.colors.linewidth(1)+2)
%     uistack(vtset.hcurves(vtset.hcurvescells == cell),'top');
    uistack(vtset.hcurves(vtset.hcurvescells == cell & vtset.hcurveschannel == vtset.dcselected.channel),'top');
catch e
    % tree view closed
end
% highlight selected cell in treeview
try
    set(vtset.htreelines,'linewidth',vtset.colors.treelinewidth(1))
    set(vtset.htreelines(vtset.htreecells == cell),'linewidth',vtset.colors.treelinewidth(1)+2)
catch e
    % tree view closed
end
uistack(h,'top')
% try
%{
if vtset.selectionMode.status
    update_selection_action(cell,channel,abstime)
end
%}
% catch e
%     rethrow(e)
% end




% --- Executes on button press in btnShowCell.
function btnShowCell_Callback(hObject, eventdata, handles)
% hObject    handle to btnShowCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vtset
if numel(vtset.dcselected) == 0 || isfield(vtset.dcselected,'cell') && numel(vtset.dcselected.cell)==0
    msgbox('No cell selected.');
else
    % delete open ManualTracking
    %     vtset.closeManualTracking();
    %     verifyTree(0);
    %     vtset.updateCallback();
    vtset.selected.cell=vtset.dcselected.cell;
    vtset.selected.timepoint=vtset.dcselected.timepoint;
    vtset.selected.channel=vtset.dcselected.channel;
    TrackACell
    %{
    if(~isfield(vtset,'windowposition'))
         vtset.windowposition = get(gcf,'position');
    else
        set(gcf,'position',vtset.windowposition);
        vtset.windowposition = get(gcf,'position');
    end
    %}
end


% called if the user did something to a cell
function update

% initFigure()
    redraw();
%     showtree();


% --- Executes on button press in btnCellImg.
function btnCellImg_Callback(hObject, eventdata, handles)
% hObject    handle to btnCellImg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vtset

if numel(vtset.selected) == 0|| isfield(vtset.selected,'cell') && numel(vtset.selected.cell)==0
    msgbox('No cell selected.');
else
    button = questdlg('Which image set do you want to see?','Cell movie', ...
        'Detection','Quantification','Non-fluor','Detection');
    if numel(button)>0
        %         redraw;
        if button(1) == 'D'
            corrImage = [];
            if vtset.results{vtset.treenum}.settings.corrDetect
                corrImage = vtset.results{vtset.treenum}.settings.corrImage;
            end
            CellMovie(vtset.results.detects{vtset.treenum} , vtset.selected.cell, vtset.results.trackset.win, corrImage, vtset.results{vtset.treenum}.quants);
        elseif button(1) == 'Q'
            corrImage = [];
            if vtset.results{vtset.treenum}.settings.corrQuant
                corrImage = vtset.results{vtset.treenum}.settings.corrImage;
            end
            CellMovie(vtset.results{vtset.treenum}.quants , vtset.selected.cell, vtset.results.trackset.win, corrImage, vtset.results{vtset.treenum}.quants);
        elseif button(1) == 'N'
            CellMovie(vtset.results.nonFluor{vtset.treenum} , vtset.selected.cell, vtset.results.trackset.win, [], vtset.results{vtset.treenum}.quants);
        end
    end
end

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- RECALC CELL
function btnCellAll_Callback(hObject, eventdata, handles)
% hObject    handle to btnCellAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

if numel(vtset.selected) == 0|| isfield(vtset.selected,'cell') && numel(vtset.selected.cell)==0
    msgbox('No cell selected.');
else
    curtree = vtset.results{vtset.treenum}.quants(vtset.selected.channel);
    tps= curtree.timepoint(curtree.cellNr==vtset.selected.cell);

    
    res=questdlg(sprintf('Recalculate cell %d in %d images?',vtset.selected.cell,numel(tps)), 'Recalc', 'Yes', 'No', 'Yes');
    
    % recalc with new trackset of selected cell
    if strcmp(res,'Yes')
        
        hwait=waitbar(0,sprintf('recalculating %d timepoints:',numel(tps)));
%         for i=1:numel(tps)
            
            % get actual detection channel
            channelIndex = vtset.results{vtset.treenum}.quants(vtset.selected.channel).detectchannel(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr == vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint == vtset.selected.timepoint);
            % new detection
            % set actual trackset temporary
            vtset.results{vtset.treenum}.detects(channelIndex).settings.trackset = vtset.temp.trackset;
            detectCells(channelIndex,vtset.treenum,vtset.selected.cell,tps)
            
            % quantify relevant quantification
            waitbar(0,hwait,'Quantifying ...')
            for q = 1:numel(vtset.results{vtset.treenum}.quants)
                if ismember(channelIndex,vtset.results{vtset.treenum}.quants(q).detectchannel(vtset.results{vtset.treenum}.quants(q).cellNr == vtset.selected.cell))
                    quantifyCells(q,vtset.treenum,vtset.selected.cell,tps)
                end
            end 

%             waitbar(i/numel(tps));
%         end
        % end
        vtset.unsaved(vtset.treenum)=1;
        vtset.updateCallback();
        close(hwait)
        TrackACell
        
    end
end

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global vtset
%%% CHECK FOR UNSAVED TREES

if isfield(vtset,'unsaved') && sum(vtset.unsaved)>0
    
    % ask
    button = questdlg('There are unsaved trees. Save results before exit?','Save?', ...
        'Yes','No','Cancel','Yes');
    
    
    if numel(button)>0
        if button(1) == 'Y'
            
            saveTrees(find(vtset.unsaved),0);
            if sum(vtset.unsaved)==0
                clearlocks;
                clear global vtset
                delete(hObject);
            end
            %         exit
        elseif button(1) == 'N'
            clearlocks;
            clear global vtset
            delete(hObject);
            %         exit
        end
    end
else
    clearlocks;
    clear global vtset
    delete(hObject);
    %         exit
end

% [s r ] =system('grep family /proc/cpuinfo | wc -l');
% if str2double(r)>1
%     matlabpool close
% end



function clearlocks
global vtset

if isfield(vtset,'results')
    for tree=1:numel(vtset.results)
        if strfind(vtset.results{tree}.settings.treefile,'simple')
            filename = [vtset.AMTfilesFolder vtset.results{tree}.settings.treefile(1:end-10) 'amt_tree_locked'];
        else
            filename = [vtset.AMTfilesFolder vtset.results{tree}.settings.treefile(1:end-3) 'amt_tree_locked'];
        end
        
        try
            delete(filename);
        catch
            warndlg(sprintf('Cannot remove lock from\n%s\nLock not found.',filename));
        end
    end
end



% --- Executes on button press in int.
function int_Callback(hObject, eventdata, handles)
% hObject    handle to int (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of int
set(hObject,'Value',1)
refreshplot('int');


function refreshplot(mode)
global vtset;
switch mode   % Get Tag of selected object
    case 'int'
        %execute this code when int_radiobutton is selected
        vtset.plotmode='int';
        redraw;
        
    case 'mean'
        %execute this code when mean_radiobutton is selected
        vtset.plotmode='mean';
        redraw;
        
    case 'prodrate'
        %execute this code when prodrate_radiobutton is selected
        vtset.plotmode='prodrate';
        redraw;
        
    case 'concentration'
        vtset.plotmode='concentration';
        redraw;
    case 'size'
        vtset.plotmode='size';
        redraw;
        
    case 'cumulative'
        vtset.plotmode='cumulative';
        redraw;
    case 'cumulativeNN'
        vtset.plotmode='cumulativeNN';
        redraw;
    case 'linearregression'
        vtset.plotmode='linearregression';
        redraw
    otherwise
        % Code for when there is no match.
        error('fucked up')
end
set(vtset.haxes,'ylimmode','auto');
vtset.tools.alignHaxes();

% --- Executes on button press in meannew.
function meannew_Callback(hObject, eventdata, handles)
% hObject    handle to meannew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of meannew
set(hObject,'Value',1)
refreshplot('mean');



% --- Executes on button press in newprod.
function newprod_Callback(hObject, eventdata, handles)
% hObject    handle to newprod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of newprod
set(hObject,'Value',1)
refreshplot('prodrate');


% --- Executes on button press in smooth.
function smooth_Callback(hObject, eventdata, handles)
% hObject    handle to smooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of smooth
global vtset

if vtset.smoothgraph==0
    vtset.smoothgraph=1;
else
    vtset.smoothgraph=0;
end

redraw;


% --- Executes on button press in doublethes.
% function doublethes_Callback(hObject, eventdata, handles)
% % hObject    handle to doublethes (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hint: get(hObject,'Value') returns toggle state of doublethes
% 
% global vtset
% 
% if vtset.doublethes==0
%     vtset.doublethes=1;
% else
%     vtset.doublethes=0;
% end
% 
% redraw;


% --- Executes on button press in zline.
function zline_Callback(hObject, eventdata, handles)
% hObject    handle to zline (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of zline


% --- Executes on button press in deletetree.
function deletetree_Callback(hObject, eventdata, handles)
% hObject    handle to deletetree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

curtree = get(handles.lstTrees,'Value');

button = questdlg(['Do you want to delete tree' vtset.results.treefiles{curtree}],'Delete Tree');

% delete tree
if numel(button)>0
    if button(1) == 'Y'
        temptreefile = vtset.results.treefiles{curtree}(1:end-11);
        vtset.results.treefiles(curtree)=[];
        vtset.results.quants(curtree)=[];
        vtset.results.detects(curtree)=[];
        vtset.results.nonFluor(curtree)=[];
        vtset.results.description(curtree)=[];
        set(handles.lstTrees,'Value',max(1,get(handles.lstTrees,'Value')-1));
        vtset.treenum=get(handles.lstTrees,'Value');
        initializeList;
        redraw;
    end
end

%clear lock

try
    delete([vtset.AMTfilesFolder '/' temptreefile '.amt_tree_locked']);
    
catch e
    warndlg(sprintf('Cannot remove lock from\n%s\nLock not found.',[vtset.AMTfilesFolder '/' temptreefile '.amt_tree_locked']));
    rethrow(e)
end


% --- Executes on button press in setdescription.
function setdescription_Callback(hObject, eventdata, handles)
% hObject    handle to setdescription (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vtset

curtree = vtset.treenum;

%init description
description='';
if isfield(vtset.results{curtree}, 'description') && ~isempty(vtset.results{curtree}.description)
    description=vtset.results{curtree}.description;
end
% get new description
varname = inputdlg('Set tree description:','Set',1,{description});

%set new description and update tree list
if numel(varname) > 0 && numel(varname{1})>0
    vtset.results{curtree}.description = varname{1};
end
initializeList;
vtset.unsaved(curtree)=1;

%%%% TODO still needed??!?
% --- Executes on button press in btnTimeCourse.
function btnTimeCourse_Callback(hObject, eventdata, handles)
% hObject    handle to btnTimeCourse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% treelist=get(handles.lstTrees,'String');
lstTrees_Callback([],[],handles)

%%%% deletes one timepoint in one tree
% --- Executes on button press in deletecell.
function deletecell_Callback(hObject, eventdata, handles)
% hObject    handle to deletecell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

try
    curtree=vtset.treenum;
    cell=vtset.selected.cell;
    tp=vtset.selected.timepoint;
    
    
    %%% TODO extend to delete whole ranges
    
    button = questdlg(sprintf('Do you want to delete Cell %d Timepoint %d',cell,tp),'Delete Tree');
    if numel(button)>0
        if button(1) == 'Y'
            
            %%% trackpoint will be  deleted in actual quant channel
            % non fluor and detect can be used for other channels
            
            deleteid = find(vtset.results{curtree}.quants(chan).cellNr==cell & vtset.results{curtree}.quants(chan).timepoint==tp);
            if numel(deleteid)==1
                for field=fields(vtset.results{curtree}.quants)'
                    vtset.results{curtree}.quants(chan).(cell2mat(field))(deleteid)=[];
                end
                
            else
                msgbox(sprintf('Could not delete cell %d, timepoint %d in tree %s', cell, tp, vtset.results.treefiles{curtree}),'Error','error');
            end
            
        end
    end
    
    vtset.unsaved(vtset.treenum)=1;
    
catch e
    
    errordlg(sprintf('Error deleting cell %d, timepoint %d in tree\n%s\nField %s out of bound',cell,tp,vtset.results{vtset.treenum}.settings.treefile,...
        cell2mat(field)),'Delete error');
end


% --- Executes on button press in concentration.
function concentration_Callback(hObject, eventdata, handles)
% hObject    handle to concentration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of concentration
set(hObject,'Value',1)
refreshplot('concentration');



%%% NEW button
% --------------------------------------------------------------------
function createNew_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to createNew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

CreateNewTreeSet(handles,[]);



%%% called from NEW button
function CreateNewTreeSet(handles,settings)

global vtset

%load general tree settings
%%%%% wrong name!!! %%%%%%%
if numel(settings)<1
    [settings treelist]=CreateNewTrackset;
else
    [settings treelist]=CreateNewTrackset(settings);
end

% cancel abfangen
if numel(settings)==0
    return;
end

% restrict treelist
treelist.root=treelist.root(settings.selectedtrees);
treelist.treefile=treelist.treefile(settings.selectedtrees);
settings.selectedtrees=1:numel(treelist.treefile);

if isfield(vtset,'results')
    % remove locks
    clearlocks;
    
    % del struct
    vtset=rmfield(vtset,'results');
end

% get movie start time
% d1 = dir([settings.imgroot '/*p*1']);
% d2 = dir([settings.imgroot '/' d1(1).name '/*xml']);
% xmlfilename=[settings.imgroot '/' d1(1).name '/' d2(1).name];
% xmlfile = fileread(xmlfilename);
% try
%     coords=strfind(xmlfile,'V42');
%     abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
% catch e
%     coords=strfind(xmlfile,'V41');
%     abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
% end
% 
% settings.moviestart = abstime;
try
settings.moviestart = getmoviestart(settings.imgroot);
catch e
    settings.moviestart = 0;
end
%start calculation
executeQuantification(settings,treelist)
% vtset.results=mytracking(trackset, settings, treelist);

%%% create lock files

% check for exp folder
try
    if ~exist(vtset.AMTfilesFolder,'dir')
        errordlg('QTFy files Folder %s, does not exist!','Folder not found',vtset.AMTfilesFolder);
        %         mkdir([vtset.AMTfilesFolder vtset.results{vtset.treenum}.settings.exp]);
    end
catch e
    errordlg(sprintf('Could not create QTFy file folder\n%s',vtset.AMTfilesFolder));
end
% lock it
for tree = 1: numel(vtset.results)
    filename=[vtset.AMTfilesFolder vtset.results{tree}.settings.treefile(1:end-4) '.amt_tree_locked'];
    try
        fnr=fopen(filename,'a');
        fclose(fnr);
    catch r
        errordlg(sprintf('Could not create lock file\n%s',filename));
        %         rethrow(r)
    end
end

%show trees
vtset.unsaved=ones(numel(vtset.results),1);
vtset.possibleUpdate=zeros(numel(vtset.results),1);
vtset.treenum = 1;
initializeList





% SAVE button
% --------------------------------------------------------------------
function saveTrees_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to saveTrees (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

if isfield(vtset,'unsaved') && numel(find(vtset.unsaved))>1
    button=questdlg(sprintf('There is more than one unsaved tree. Save all trees?'),'Save?','Save all','No, just this one','Save all');
    if strcmp(button(1),'S')
        saveTrees(find(vtset.unsaved),0);
    else
        saveTrees(vtset.treenum,0);
    end
else
    saveTrees(vtset.treenum,0);
end



function saveTrees(saveList,force)

global vtset
if ~isfield(vtset,'results')
    warndlg('There are no trees to store');
    return;
end


wh=waitbar(0,'Saving tree...');
%%% store in same folder

if exist(vtset.AMTfilesFolder,'dir')
    for treeNum=torow(saveList)
        
        amt_tree=strrep(vtset.results{treeNum}.settings.treefile,'ttt','');
        status=find(saveList==treeNum)/numel(saveList);
        waitbar(status,wh,sprintf('Saving tree %s',amt_tree));
        if exist([vtset.AMTfilesFolder amt_tree 'amt_tree'],'file')
            % tree already quantified, ask
            if ~force
                button=questdlg(sprintf('Tree %s has already been quantified. Overwrite?',amt_tree),'Save?','Yes','No','Save All','Yes');
            else
                button(1)='Y';
            end
            if strcmp(button(1),'Y') || strcmp(button(1),'S')
                %%% save tree
                saveTree([vtset.AMTfilesFolder amt_tree 'amt_tree'],treeNum);
                waitbar(status,wh,'done.');
                pause(0.1)
                if strcmp(button(1),'S')
                    force=1;
                end
                
            else
                waitbar(1,wh,'canceled.');
                pause(0.5)
                
            end
            
        else
            % create new file
            
            %check if exp folder exists
            if ~exist([vtset.AMTfilesFolder],'dir')
                %%%% this should never happen anymore!!!
                fprintf('Creating experiment directory %s \n',vtset.AMTfilesFolder);
                try
                    errordlg('QTFy files Folder %s, does not exist!','Folder not found',vtset.AMTfilesFolder);
                    %                     mkdir([vtset.AMTfilesFolder]);
                    
                catch e
                    errordlg(sprintf('Error creating new experiment directory\n%s',[vtset.AMTfilesFolder vtset.results{vtset.treenum}.settings.exp]));
                    close(wh);
                    rethrow(e)
                end
                
            end
            
            %%% save tree
            saveTree([vtset.AMTfilesFolder amt_tree 'amt_tree'],treeNum);
            waitbar(status,wh,'done.');
            pause(0.5)
            
        end
    end
    
else
    msgbox(sprintf('QTFy file folder %s not found!',vtset.AMTfilesFolder),'QTFy file folder error','error');
    
end
close(wh);


function saveTree(filename,treeNum)
global vtset

results = vtset.results{treeNum};

% results.quants=vtset.results.quants{treeNum};
% results.detects=vtset.results.detects{treeNum};
% results.nonFluor=vtset.results.nonFluor{treeNum};
% results.description=vtset.results.description{treeNum};
% results{vtset.treenum}.settings=vtset.results{vtset.treenum}.settings;
% results.trackset=vtset.results.trackset;
% results.treelist.treefile=vtset.results.treelist.treefile{treeNum};
% results.treelist.root=vtset.results.treelist.root{treeNum};
% results.treefiles=vtset.results.treefiles{treeNum};
results.version=vtset.version;
try
    save(filename,'results')
    fprintf('File %s stored.\n',filename);
catch e
    errordlg(sprintf('Error saving experiment\n%s',filename));
    rethrow(e);
end

vtset.unsaved(treeNum)=0;


% LOAD button
% --------------------------------------------------------------------
function loadTrees_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to loadTrees (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vtset

% if ~exist(vtset.movierootFolder,'dir')
%     warndlg(sprintf('No experiment found in %s. Disk properly mounted?',vtset.movierootFolder),'File not found');
%     return;
% end

d= uigetdir(vtset.movieFolder,'Please select movie folder');
if d == 0
    return
else
    vtset.movieFolder = [d '/'];
    vtset.movierootFolder = [d '/'];
end
% explist={d([d.isdir]).name};
% 
% if isfield(vtset,'results')
%     exp = vtset.results{1}.settings.exp;
%     init = find(strncmp(exp,explist,8));
% else
%     init = 1;
% end

% exp = regexp(d, '\d{6,}[A-Z]{2,3}\d{1,}', 'match');
[~, exp]=fileparts(d);
if isempty(exp)
    msgbox('Could not validate expfolder')
    return
end
 
% [exp ok]=listdlg('PromptString','Select experiment:',...
%     'SelectionMode','single',...
%     'ListSize',[200 200],...
%     'ListString',explist(1:end),'initialvalue',init);

% exp = inputdlg('Please enter the experiment number','Experiment Number',1,{'090901PH2'});

if numel(exp)>0
    % get exp number from explist
    % +2 since '.' and '..' listet in dir
    %%%% changed 8.12.2010

%     exp=explist{exp};
    vtset.AMTfilesFolder = [vtset.tttrootFolder '20' exp(1:2) '/' exp '/'];
    vtset.tttfilesFolder = [vtset.tttrootFolder '20' exp(1:2) '/' exp '/'];
    vtset.experiment = exp;
 
    % show list of quantified trees
    if 1%&&exist(vtset.AMTfilesFolder,'dir')
        
        
        %         vtset.results.quants={};
        %         vtset.results.detects={};
        %         vtset.results.nonFluor={};
        %         vtset.results.description={};
        %         vtset.results{vtset.treenum}.settings={};
        %         vtset.results.trackset={};
        %         vtset.results.treefiles={};
        %         vtset.results.treelist={};
        %         vtset.results.treelist.root={};
        
        %         amtfiles=dir([vtset.AMTfilesFolder exp '/*.amt_tree']);
        % get all amt files in all subfolders
        
        wb=waitbar(0,'Loading QTFy-file list...');
        filestrct = getfiles_Nonrec(vtset.AMTfilesFolder,'amt_tree');
%         filestrct = getfiles_rec(vtset.AMTfilesFolder,'amt_tree');
        amtlist=filestrct.files;
        waitbar(1,wb);
        close(wb);
        
        if numel(amtlist)==0
            errordlg(sprintf('No tree found for experiment %s.\nPlease select NAS folder correctely (QTFy / Preferences / Select NAS).',exp));
            uiwait
            vtset.AMTfilesFolder = vtset.tttrootFolder;
            vtset.tttfilesFolder = vtset.tttrootFolder;
            
            return
        end;
           
        
        [selectedtrees ok]=listdlg('PromptString','Select QTFy files:',...
            'SelectionMode','multiple',...
            'ListSize',[400 400],...
            'ListString',amtlist);
        if ~ok
            return;
        end
        
        
        lh=waitbar(0,'Loading trees...');           
        
        if isfield(vtset,'results') && ~strcmp(vtset.results{vtset.treenum}.settings.exp,exp)
            % remove locks
            clearlocks;
            
            % del struct
            vtset=rmfield(vtset,'results');
            vtset.treenum = 1;
            
            % reinit contrast
            vtset.contrast.contrast = {};
            vtset.contrast.wl = {};
        end
        
        force = 0 ;
        %%%% LOADING
        for f = selectedtrees % 1:numel(amtfiles)
            % check for lock
            b='Y';
            
            if exist([filestrct.folders{f} '/' filestrct.files{f} '_locked'],'file') && ~force
                b=questdlg(sprintf('File \n%s\nis locked. Load anyway?',[vtset.AMTfilesFolder exp '/' filestrct.files{f}]));
                if strcmpi(b(1),'Y')
                    force=1;
                end
            end
            if strcmpi(b(1),'Y')
                % do it
                try
                load([filestrct.folders{f} '/' filestrct.files{f} ],'-mat');
                waitbar((f)/numel(filestrct.files),lh);
                unsaved=0;
                
                if isfield(results,'version')
                    %%% TODO check if this works with new structure
                    version = strsplit_qtfy('.',results.version);
                    if str2double(version{2})>=9 && str2double(version{3})>=9
                        %%% newest structure
                        results.settings.imgroot=vtset.movierootFolder ;
                        
                    else
                        
                        results.settings.treefile =results.treefiles;
                        results.settings.treeroot = results.treelist.root;
                        results.detects.settings.wl=results.settings.extDetect;
                        results.detects.settings.trackset = results.trackset;
                        results.quants.settings.wl=results.settings.extQuant;
                        results.quants.settings.detectchannel = 1;
                        results.quants.settings.normalize = results.settings.corrQuant;
                        results.detects.settings.normalize = results.settings.corrDetect;
                        results.quants.detectchannel = ones(numel(results.quants.timepoint),1);
                        results.detects.trackset = cell2mat(results.quants.trackset);
                        results.detects.cellmask = results.quants.cellmask;
                        results.detects.size = results.quants.size;
                        results.detects.X=results.detects.X-1;
                        results.detects.Y=results.detects.Y-1;
                        results.quants.X=results.quants.X-1;
                        results.quants.Y=results.quants.Y-1;
                        results.nonFluor.settings.wl = results.settings.extNonFluor;
                        results.settings.imgroot=vtset.movierootFolder ;
                    end
                    
                                            

                end
                
                % check some missing old structure stuff
                if ~isfield(results.settings,'filesettings')
                    [imFile, ~, ext] = getAnImageFile(vtset.movieFolder,vtset.experiment);
                    filesettings = generateFilesettings(imFile);
                    results.settings.filesettings = filesettings;
                end
                if ~isfield(results.settings,'moviestart')
                    if isfield(vtset,'results') && numel(vtset.results)>0
                        results.settings.moviestart = vtset.results{1}.settings.moviestart;
                    else
                        results.settings.moviestart = getmoviestart(vtset.movieFolder );
                    end
                end
                
                %%%% TODO : rwrite to check every channel!!
                
                if ~isfield(results.quants,'active')
                    for d = 1:numel(results.quants)
                        results.quants(d).active=ones(numel(results.quants(d).int),1);
                    end
                    unsaved=1;

                end
                if ~isfield(results.detects,'active') || ~numel(results.detects.active)
                    for d = 1:numel(results.detects)
                        results.detects(d).active=ones(1,numel(results.detects(d).size));
                    end
                    unsaved=1;

                end
                
                if ~isfield(results.settings,'lastTTTchange')
                    % get initial timestamp of this tree
                    results.settings.lastTTTchange = 0;%getTTTtimestamp(results.settings.treefile);
                    unsaved=1;
                end
                
                if ~isfield(results.detects,'perimeter')
                    results = mapmissingperimeter(results);
                    unsaved=1;
                end
                
%                 if ~isfield(results.detects,'eccentricity')
%                     results = mapmissingeccentricity(results);
%                     unsaved=1;
%                 end
                
                if ~isfield(results.nonFluor,'stopReason') || numel(results.nonFluor.stopReason)<1 ||~isfield(results.nonFluor,'Y') || numel(results.nonFluor.Y)<1 || numel(results.nonFluor.stopReason) ~= numel(results.nonFluor.X)
                    try
                    results = mapmissingstopreason(results);
                    catch e
                        fprintf('could not map stop reason\n')
                        results.nonFluor.stopReason = zeros(1,numel(results.nonFluor.cellNr));
                    end
                        
                    unsaved=1;
                end
                
                %%% check additional annotations
                if isfield(results.nonFluor,results.settings.anno) && numel(results.nonFluor.(results.settings.anno)) ~= numel(results.nonFluor.X)
                    try
                        results = mapmissingannotationLocal(results,results.settings.anno);
                    catch e
                        fprintf('could not map annotation %s reason\n',results.settings.anno)
                        results.nonFluor.(results.settings.anno) = zeros(1,numel(results.nonFluor.cellNr));
                    end
                    
                    unsaved=1;
                end
                if isfield(results.nonFluor,results.settings.anno2) && numel(results.nonFluor.(results.settings.anno2)) ~= numel(results.nonFluor.X)
                    try
                        results = mapmissingannotationLocal(results,results.settings.anno2);
                    catch e
                        fprintf('could not map annotation %s reason\n',results.settings.anno2)
                        results.nonFluor.(results.settings.anno2) = zeros(1,numel(results.nonFluor.cellNr));
                    end
                    
                    unsaved=1;
                end

                if ~isfield(results.detects,'inspected')
                    for d = 1:numel(results.detects)
                        results.detects(d).inspected = zeros(numel(results.detects(d).cellNr),1);
                    end
                    for d = 1:numel(results.quants)
                        results.quants(d).inspected = zeros(numel(results.quants(d).cellNr),1);
                    end
                    unsaved=1;
                end
                
                % new trackset struct
                for d = 1:numel(results.detects)
                    if ~isfield(results.detects(d).trackset,'ellipse')
                        results.detects(d).trackset(1).ellipse =[];
                        unsaved=1;
                    end
                    if ~isfield(results.detects(d).settings.trackset,'ellipse')
                        results.detects(d).settings.trackset.ellipse =[];
                        unsaved=1;
                    end
                    if ~isfield(results.detects(d).trackset,'freehand')
                        results.detects(d).trackset(1).freehand =[];
                        unsaved=1;
                    end
                    if ~isfield(results.detects(d).settings.trackset,'freehand')
                        results.detects(d).settings.trackset.freehand =[];
                        unsaved=1;
                    end
                    % make sure ordering is correct
                    results.detects(d).settings.trackset = orderfields(results.detects(d).settings.trackset);
                    results.detects(d).trackset = orderfields(results.detects(d).trackset);
                end

                
                %check if new tracking happened
                lastTTTchange = getTTTtimestamp(results.settings.treefile);
                possibleUpdate=0;
                if lastTTTchange>0
                    if results.settings.lastTTTchange < lastTTTchange
                        possibleUpdate=1;
                    end

                end

                
                catch e
                    rethrow(e)
                    msgbox(sprintf('Error loading tree %s. Skipping...\n',[filestrct.folders{f} '/' filestrct.files{f} ]))
                    fprintf('Error loading tree %s. Skipping...\n',[filestrct.folders{f} '/' filestrct.files{f} ])
                    continue
                end
                
                %%%% update vtset struct
                if isfield(vtset,'results')
                    vtset.results{end+1} = results;
                    vtset.unsaved(numel(vtset.results))=unsaved;
                    vtset.possibleUpdate(numel(vtset.results)) = possibleUpdate;
                else
                    vtset.results{1} = results;
                    vtset.unsaved= unsaved;
                    vtset.possibleUpdate = possibleUpdate;
                end
                
                
                
                
                
                
                % lock file
                try
                    %                     fnr=fopen([cell2mat(filestrct.folders(f)) cell2mat(filestrct.files(f)) '_locked'],'a');
                    %                     fclose(fnr);
                catch e
                    errordlg(sprintf('Could not create lock file\n%s',[cell2mat(filestrct.folders(f)) cell2mat(filestrct.files(f)) '_locked']));
                    rethrow(e)
                end
                
                
            end
        end
        
        %%% initialize the treelist
        waitbar(1,lh,'Initializing...')
        pause(0.01)
        
        initializeList;
        
        close(lh);
        
        % ask if you want to recalc new tracked data
        if sum(vtset.possibleUpdate)>0
            % always check for deleted, changed tracks and delete them
            % also ask if they want to quantify mew stuff
            updateQTFfiles(find(vtset.possibleUpdate),0)

           
        end
        
    else
        msgbox(sprintf('There are no quantified tree of experiment %s',exp),'No trees found','warn')
    end
    
end



% --- Executes on button press in settings_button.
function settings_button_Callback(hObject, eventdata, handles)
% hObject    handle to settings_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vtset

settings=CreateNewTrackset(vtset.results{vtset.treenum}.settings);

if numel(settings)>0
%     vtset.results{vtset.treenum}.settings=settings;
    vtset.results{vtset.treenum}.settings.anno=settings.anno;
    vtset.results{vtset.treenum}.settings.anno2=settings.anno2;
    vtset.results{vtset.treenum}.settings.imgroot=settings.imgroot;
%     vtset.results{vtset.treenum}.settings.treeroot=settings.treeroot;
    for chan = 1:numel(vtset.results{vtset.treenum}.detects)
        vtset.results{vtset.treenum}.detects(chan).settings.normalize = settings.corrDetect;
    end
    for chan = 1:numel(vtset.results{vtset.treenum}.quants)
        vtset.results{vtset.treenum}.quants(chan).settings.normalize = settings.corrQuant;
    end
end


% --- Executes on button press in recalcTree.
function recalcTree_Callback(hObject, eventdata, handles)
% hObject    handle to recalcTree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset


%%% show list of all open trees
treelist = {};
somethingnew={'','*'};
for t = 1:numel(vtset.results)
    treelist{end+1}= [vtset.results{t}.settings.treefile somethingnew{vtset.possibleUpdate(t)+1}];
end

selected=listdlg('liststring',treelist,'name','Select tree to calculate','initialvalue',unique([vtset.treenum find(vtset.possibleUpdate)]),'ListSize',[200 200]);

if ~numel(selected)
    return
end

updateQTFfiles(selected,1);
return

%%% ask if recalc whole tree, or only new tracked cells?
% answer=questdlg('Recalc only new cells or whole tree?','Recalc','Only new cells','Whole tree','Cancel','Only new cells');
% 
% if strcmp(answer(1),'C')
%     return
% end
% %%% new tracked cells:
% if strcmp(answer(1),'O')
%     % iterate over selected trees
%     for tree = selected
%     % load ttt file
% 
% %     xmlfilename = dir([vtset.movierootFolder '/' vtset.results{tree}.settings.exp '/*xml']);
% %     xmlfilename = xmlfilename(1).name;
%     xmlfilename = [vtset.movierootFolder vtset.results{tree}.settings.exp '/' vtset.results{tree}.settings.exp '_TATexp.xml'];
%     position = strsplit('-',vtset.results{tree}.settings.treefile);
%     tttname=[vtset.tttrootFolder '20' vtset.results{tree}.settings.exp(1:2) '/' vtset.results{tree}.settings.exp '/' position{1} '/' vtset.results{tree}.settings.treefile];
%     tttname= strrep(tttname,'_simple','');
%     res = tttParser(tttname,xmlfilename);
%     
%     if ~numel(res)
%         continue
%     end
%     
%     % map them and see whats new
%     [c possibleids] = setdiff([res.timepoint res.cellNr],[tocolumn(vtset.results{tree}.nonFluor.timepoint) tocolumn(vtset.results{tree}.nonFluor.cellNr)],'rows');
%     
%     if ~numel(possibleids)
%         fprintf('No new tracked cells found for tree%s\n',vtset.results{tree}.settings.treefile)
%         continue
%     end
%     
%     for f = fieldnames(res)'
%         res.(cell2mat(f)) = res.(cell2mat(f))(possibleids);
%     end
%     
%     % load img xml file 
%     
%     %check if new exp
%     newexperiment = 0;
%     tocheck = strsplit('/',vtset.results{1}.nonFluor.filename{1});
%     if numel(tocheck{1}) == 15
%         newexperiment = 1;
%     end
%     fprintf('Getting new %d timepoints',numel(res.cellNr))
%     
%     newDindizes=[];
%     newQindizes=[];
%     newNFindizes=[];
%     
%     % check for missing stopreason
%     if ~isfield(vtset.results{tree}.nonFluor, 'stopReason') || ~numel(vtset.results{tree}.nonFluor.stopReason)
%         mapmissingstopreason(tree)
%     end
%     wh = waitbar(0,'Gathering new timepoints');
% 
%     imgroot = vtset.results{tree}.settings.imgroot;
% 
%     for posi = torow(unique(res.positionIndex));
%         if exist(sprintf('%s/%s_p%04d/',imgroot,vtset.results{tree}.settings.exp, posi),'dir')
%             newexperiment=1;
%             filename = sprintf('%s/%s_p%04d/%s_p%04d.log',imgroot,vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi);
%         else
%             filename = sprintf('%s/%s_p%03d/%s_p%03d.log',imgroot,vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi);
%             newexperiment = 0;
%         end
%         
%         if exist(filename,'file')
%             % read logfile
%             log = positionLogFileReader(filename);
%             idx=find(res.positionIndex == posi)';
%             
%             
%             % all quants
%             for d = 1:numel(vtset.results{tree}.quants)
%                 wl = vtset.results{tree}.quants(d).settings.wl;
%                 wl = strsplit('.',wl);
%                 wl = str2double(wl{1}(2:end));
%                 
%                 for id = idx
%                     % calc  everything
%                     abstime = log.absoluteTime(log.timepoint == res.timepoint(id) & log.wavelength == wl)-vtset.results{tree}.settings.moviestart;
%                     if isempty(abstime)
%                         continue
%                     end
%                     if newexperiment
%                         if log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl) ~= -1
%                             zindex = sprintf('z%03d_',log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl));
%                         else
%                             zindex='';
%                         end
%                         % example: 110624AF6_p0024_t00002_w1.tif
%                         % 111103PH5_p0148_t00002_z001_w1.png
%                         
%                         filename = sprintf('%s_p%04d/%s_p%04d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,vtset.results{tree}.quants(d).settings.wl);
%                     else
%                         filename = sprintf('%s_p%03d/%s_p%03d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,vtset.results{tree}.quants(d).settings.wl);
%                     end
%                     
%                     
%                     % store everything
%                     vtset.results{tree}.quants(d).absoluteTime(end+1) = abstime;
%                     vtset.results{tree}.quants(d).active(end+1) = 0;
%                     vtset.results{tree}.quants(d).cellNr(end+1) = res.cellNr(id);
%                     vtset.results{tree}.quants(d).detectchannel(end+1) = vtset.results{tree}.quants(d).settings.detectchannel;
%                     vtset.results{tree}.quants(d).filename{end+1} = filename;
%                     vtset.results{tree}.quants(d).int(end+1) = -1;
%                     vtset.results{tree}.quants(d).timepoint(end+1) = res.timepoint(id);
%                     vtset.results{tree}.quants(d).positionIndex(end+1) = posi;
%                     vtset.results{tree}.quants(d).inspected(end+1) = 0;
%                     newQindizes(d,end+1) = numel(vtset.results{tree}.quants(d).positionIndex);
%                 end
%             end
%             
%             % for all detects
%             for d = 1:numel(vtset.results{tree}.detects)
%                 
%                 wl = vtset.results{tree}.detects(d).settings.wl;
%                 wl = strsplit('.',wl);
%                 wl = str2double(wl{1}(2:end));
%                 
%                 for id = idx
%                     % calc  everything
%                     abstime = log.absoluteTime(log.timepoint == res.timepoint(id) & log.wavelength == wl)-vtset.results{tree}.settings.moviestart;
%                     if isempty(abstime)
%                         continue
%                     end
%                     if newexperiment
%                         if log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl) ~= -1
%                             zindex = sprintf('z%03d_',log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl));
%                         else
%                             zindex='';
%                         end
%                         % example: 110624AF6_p0024_t00002_w1.tif
%                         % 111103PH5_p0148_t00002_z001_w1.png
%                         
%                         filename = sprintf('%s_p%04d/%s_p%04d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,vtset.results{tree}.detects(d).settings.wl);
%                     else
%                         filename = sprintf('%s_p%03d/%s_p%03d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,vtset.results{tree}.detects(d).settings.wl);
%                     end
%                     
%                     
%                     % store everything
%                     vtset.results{tree}.detects(d).absoluteTime(end+1) = abstime;
%                     vtset.results{tree}.detects(d).active(end+1) = 1;
%                     vtset.results{tree}.detects(d).cellmask{end+1} = [];
%                     vtset.results{tree}.detects(d).cellNr(end+1) = res.cellNr(id);
%                     vtset.results{tree}.detects(d).filename{end+1} = filename;
%                     vtset.results{tree}.detects(d).positionIndex(end+1) = posi;
%                     vtset.results{tree}.detects(d).size(end+1) = 0;
%                     vtset.results{tree}.detects(d).timepoint(end+1) = res.timepoint(id);
%                     vtset.results{tree}.detects(d).inspected(end+1) = 0;
%                     if isstructEqual(vtset.results{tree}.detects(d).trackset(end),vtset.results{tree}.detects(d).settings.trackset)
%                         vtset.results{tree}.detects(d).trackset(end+1) = vtset.results{tree}.detects(d).settings.trackset;
%                     else
%                         endid = numel(vtset.results{tree}.detects(d).trackset);
%                         for field2 = fieldnames(vtset.results{tree}.detects(d).settings.trackset)'
%                             vtset.results{tree}.detects(d).trackset(endid+1).(cell2mat(field2)) = vtset.results{tree}.detects(d).settings.trackset.(cell2mat(field2));
%                             
%                         end
%                     end
%                     vtset.results{tree}.detects(d).X(end+1) = res.X(id);
%                     vtset.results{tree}.detects(d).Y(end+1) = res.Y(id);
%                     newDindizes(d,end+1) = numel(vtset.results{tree}.detects(d).X);
%                 end
%             end
%             
%             % and all nonfluor
%             for d = 1:numel(vtset.results{tree}.nonFluor)
%                 wl = vtset.results{tree}.nonFluor(d).settings.wl;
%                 wl = strsplit('.',wl);
%                 wl = str2double(wl{1}(2:end));
%                 
%                 for id = idx
%                     % calc  everything
%                     abstime = log.absoluteTime(log.timepoint == res.timepoint(id) & log.wavelength == wl)-vtset.results{tree}.settings.moviestart;
%                     if isempty(abstime)
%                         continue
%                     end
%                     if newexperiment
%                         if log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl) ~= -1
%                             zindex = sprintf('z%03d_',log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl));
%                         else
%                             zindex='';
%                         end
%                         % example: 110624AF6_p0024_t00002_w1.tif
%                         % 111103PH5_p0148_t00002_z001_w1.png
%                         
%                         filename = sprintf('%s_p%04d/%s_p%04d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,vtset.results{tree}.nonFluor(d).settings.wl);
%                     else
%                         filename = sprintf('%s_p%03d/%s_p%03d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,vtset.results{tree}.nonFluor(d).settings.wl);
%                     end
%                     
%                     % store it
%                     vtset.results{tree}.nonFluor.absoluteTime(end+1) = abstime;                    
%                     vtset.results{tree}.nonFluor.cellNr(end+1) = res.cellNr(id);
%                     vtset.results{tree}.nonFluor.filename{end+1} = filename;
%                     vtset.results{tree}.nonFluor.positionIndex(end+1) = posi;
%                     vtset.results{tree}.nonFluor.timepoint(end+1) = res.timepoint(id);
%                     vtset.results{tree}.nonFluor.stopReason(end+1) = res.stopReason(id);
%                     if isfield(vtset.results{tree}.nonFluor,vtset.results{1}.settings.anno)
%                         vtset.results{tree}.nonFluor.(vtset.results{1}.settings.anno)(end+1) = res.(vtset.results{1}.settings.anno)(id);
%                     end
%                     if isfield(vtset.results{tree}.nonFluor,vtset.results{1}.settings.anno2)
%                         vtset.results{tree}.nonFluor.(vtset.results{1}.settings.anno2)(end+1) = res.(vtset.results{1}.settings.anno2)(id);
%                     end
%                     
%                     
%                     newNFindizes(end+1) = numel(vtset.results{tree}.nonFluor.stopReason);
%                 end
%             end
%         end
%     end    
% 
%     fprintf('done.\')
%     % execute detect
%     for i = 1:numel(vtset.results{tree}.detects)
%         waitbar(i/numel(vtset.results{tree}.detects),wh,sprintf('Detecting channel %d of %d',i,numel(vtset.results{tree}.detects)))
%         detectCells(i,tree,'Only New Cells',newDindizes(i,0~=newDindizes(i,:)))
%     end
%     
%     % execute quantify
%     for i = 1:numel(vtset.results{tree}.quants)
%         waitbar(i/numel(vtset.results{tree}.quants),wh,sprintf('Quantifying channel %d of %d',i,numel(vtset.results{tree}.quants)))
%         quantifyCells(i,tree,'Only New Cells',newQindizes(i,0~=newQindizes(i,:)))
%     end
%     % update view
%     close(wh)
%     vtset.unsaved(tree) = 1;
%     redraw
%     end
%     
% else
%     %%% whole tree
% % lets see if this is needed
% % msgbox('Is this a neccessary feature?')
% 
% end




% function [abstime filename]=getabstime(newexperiment,exp,posi,tp,ext,tree)
% global vtset
% if newexperiment
%     filename = sprintf('%s/%s/%s_p%04d/%s_p%04d_t%05d_%s',  vtset.movieFolder,exp, exp,posi,exp,posi,tp,ext);
% else
%     filename = sprintf('%s/%s/%s_p%03d/%s_p%03d_t%05d_%s',  vtset.movieFolder, exp,exp,posi,exp,posi,tp,ext);
% end
% 
% if ~(exist(filename,'file'))
%     abstime=[];
%     return
% end
%    
% 
% xmlfilename=[filename '_meta.xml'];
% xmlfile = fileread(xmlfilename);
% 
% try
%     coords=strfind(xmlfile,'V42');
%     abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
% catch e
%     coords=strfind(xmlfile,'V41');
%     try
%         abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
%     catch e
%         'xml image file error'
%         abstime = 0;
%     end
% end
% 
% if exist(filename,'file')
%     if newexperiment
%         filename = sprintf('/%s_p%04d/%s_p%04d_t%05d_%s',   exp,posi,exp,posi,tp,ext);
%     else
%         filename = sprintf('/%s_p%03d/%s_p%03d_t%05d_%s',  exp,posi,exp,posi,tp,ext);
%     end
% end
% 
% abstime=abstime-vtset.results{tree}.settings.moviestart;

function showOnlyCells_Callback(hObject, eventdata, handles)
% hObject    handle to showOnlyCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of showOnlyCells as text
%        str2double(get(hObject,'String')) returns contents of showOnlyCells as a double


% --- Executes during object creation, after setting all properties.
function showOnlyCells_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showOnlyCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function deltafilter_Callback(hObject, eventdata, handles)
% hObject    handle to deltafilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of deltafilter as text
%        str2double(get(hObject,'String')) returns contents of deltafilter as a double


% --- Executes during object creation, after setting all properties.
function deltafilter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to deltafilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% get a feeling for how large deviations are from estimation and store in
% vtset
function calculateDeviations(channel)
global vtset
wh = waitbar(0,sprintf('calculating outliers for channel %d',channel));
allrm=[];
for tree = 1:numel(vtset.results)
    waitbar(tree/numel(vtset.results),wh);
    for c = unique(vtset.results{tree}.nonFluor.cellNr)
        alldata = vtset.results{tree}.quants(channel).int(vtset.results{tree}.quants(channel).cellNr ==c);
        alltps  = vtset.results{tree}.quants(channel).timepoint(vtset.results{tree}.quants(channel).cellNr ==c);
        
        [alltps, order] = sort(alltps);
        alldata = alldata(order);
        
        
        % leave one out cross validation
        rm=distance2Smoothed(alldata);
        allrm=[allrm torow(rm)];
    end
end
vtset.outlier.allrm{channel}=allrm;
delete(wh);


% --- Executes on slider movement.
function slider_outlier_Callback(hObject, eventdata, handles)
% hObject    handle to slider_outlier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global vtset
if isfield(vtset,'dcselected')
    channel = vtset.dcselected.channel;
else
    channel = 1;
end
% cells = vtset.showonlycells;

% get a feeling for how large deviations are from estimation and store in
% vtset
if isfield(vtset,'outlier') && isfield(vtset.outlier,'allrm') && numel(vtset.outlier.allrm) >= channel && numel(vtset.outlier.allrm{channel})
    
else
    vtset.outlier.allrm{channel}={};
    calculateDeviations(channel)
end

allrm = vtset.outlier.allrm{channel};


set(handles.outlierThresholdValue,'String',num2str(get(handles.slider_outlier,'Value')));
if numel(vtset.hplot)>0
    
%     axes(vtset.haxes(chan));
    
    try
%         for i=1:numel(vtset.outlierhandles)
            delete(vtset.outlierhandles);
%         end
    catch
        % already deleted
    end
    vtset.outlierhandles=[];
    vtset.outliers=[];
    
    % gather cells
    if ~isfield(vtset.dcselected,'channel')
        msgbox('You have to select a channel first')
        return
    end
        
    chan = vtset.dcselected.channel;
    cells = vtset.showonlycells;
%     if strcmpi(cells,'all')
%         cells = unique(vtset.results{vtset.treenum}.quants(chan).cellNr);
%     else
%         cells=strrep(cells, '-',':');
%         cells=strrep(cells, ',',' ');
%         cells=['[' cells ']' ];
%         cells=eval(cells);
%         cells=cells(ismember(cells, unique(vtset.results{vtset.treenum}.quants(chan).cellNr)));
%     end
    
    hold(vtset.haxes(chan),'on');
    
    for c = torow(cells)
        data= tocolumn(vtset.results{vtset.treenum}.quants(chan).int(vtset.results{vtset.treenum}.quants(chan).cellNr==c & vtset.results{vtset.treenum}.quants(chan).active==1));
        tps= tocolumn(vtset.results{vtset.treenum}.quants(chan).timepoint(vtset.results{vtset.treenum}.quants(chan).cellNr==c & vtset.results{vtset.treenum}.quants(chan).active==1));
        abst= vtset.results{vtset.treenum}.quants(chan).absoluteTime(vtset.results{vtset.treenum}.quants(chan).cellNr==c & vtset.results{vtset.treenum}.quants(chan).active==1)/vtset.timemodifier;
        % outlier  = deviate thresh * std from smoothed data
%         thresh=get(handles.slider_outlier,'Value')*std(smooth(data)-data);
%         outliers= (abs(smooth(data)-data))>thresh;
        
        % outlier in respect to whole distribution
        rm=distance2Smoothed(data);
        outliers = rm>median(allrm(allrm<100))+get(handles.slider_outlier,'Value')*std(allrm(allrm<100));
        if sum(outliers)>0
            
            vtset.outlierhandles(end+1)=plot(abst(outliers),data(outliers),'o','linewidth',5','parent',vtset.haxes(chan),'Color','black');
            vtset.outliers(end+1:end+sum(outliers),:,:,:) = [zeros(sum(outliers),1)+c, tps(outliers)];
            
        end
        
    end
    hold(vtset.haxes(chan),'off');
end



% --- Executes during object creation, after setting all properties.
function slider_outlier_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_outlier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function outlierThresholdValue_Callback(hObject, eventdata, handles)
% hObject    handle to outlierThresholdValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of outlierThresholdValue as text
%        str2double(get(hObject,'String')) returns contents of outlierThresholdValue as a double


% --- Executes during object creation, after setting all properties.
function outlierThresholdValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outlierThresholdValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonDeleteOutliers.
function ButtonDeleteOutliers_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDeleteOutliers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vtset
%vtset.functions.slider_outlier_Callback;
%slider_outlier_Callback(hObject, eventdata, handles);

if numel(vtset.outliers)>0;
    force=0;
    erfolgid=[];
    for i = 1:size(vtset.outliers,1)
        cell = vtset.outliers(i,1);
        tp = vtset.outliers(i,2);
        chan = vtset.dcselected.channel;
        if ~force
            button = questdlg(sprintf('Do you want to mark Cell %d Timepoint %d as outlier',cell,tp),'Delete Tree','Yes','Mark All','Cancel','Cancel');
        end
        if numel(button)==0
            return
        end
        if strcmp(button(1),'C')
            return
        elseif strcmp(button(1),'M')
            force=1;
            button(1) = 'Y';
        end
        
        
        if strcmp(button(1),'Y')
            
            %%% trackpoint will be  marked in actual quant channel
            % non fluor and detect can be used for other channels
            
            deleteid = find(vtset.results{vtset.treenum}.quants(chan).cellNr==cell & vtset.results{vtset.treenum}.quants(chan).timepoint==tp);
            if numel(deleteid)==1
                vtset.results{vtset.treenum}.quants(chan).active(deleteid)= 0;
                erfolgid(end+1) = i;
            else
                msgbox(sprintf('Could not delete cell %d, timepoint %d in tree %s', cell, tp, vtset.results.treefiles{curtree}),'Error','error');
            end
            
        end
        
       
    end
    vtset.outliers(erfolgid,:)=[];
    try
        delete(vtset.outlierhandles);
    catch e
        % nevermind
    end
    vtset.outlierhandles=[];


    vtset.unsaved(vtset.treenum)=1;
    redraw;
%     TrackACell;
end


% --- Executes on button press in ButtonPlotStyle.
function ButtonPlotStyle_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonPlotStyle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

if isfield(vtset,'colors')
    setPlotStyle(vtset.colors)
else
    vtset.colors.title = sprintf('Tree: %s',vtset.results{vtset.treenum}.settings.treefile(1:end-11));
    vtset.colors.xlabel = '[h]';
    vtset.colors.ylabel = '';
    figure(vtset.hplot)
    vtset.colors.yaxisrange = get(vtset.haxes(1), 'ylim');
    vtset.colors.xaxisrange = get(vtset.haxes(1), 'xlim');
    vtset.colors.cell={};
    vtset.colors.color={};
    vtset.colors.linestyle={};
    vtset.colors.linewidth={};
    vtset.colors.markerstyle={};
    vtset.colors.grid=1;
    vtset.colors.legend='<off>';
    vtset.colors.treelinewidth = 1;
    vtset.colors.dynamicLineWidth = 1;
    setPlotStyle(vtset.colors)
end


% function settings=checkpaths(results)
% global vtset
% %%% check for win or linux paths and change them if neccessary
% exp = results{vtset.treenum}.settings.exp;
% if ispc
%     %%% WINDOWS uaaaah
%     vtset.AMTfilesFolder = [vtset.tttrootFolder '20' exp(1:2) '/' exp '/'];
%     vtset.tttfilesFolder = [vtset.tttrootFolder '20' exp(1:2) '/' exp '/'];
%     vtset.movieFolder = [vtset.movierootFolder exp '/'];
%     results{vtset.treenum}.settings.imgroot = [vtset.movierootFolder results{vtset.treenum}.settings.exp];
% else
%     %%% LINUX YEAH
%     results{vtset.treenum}.settings.imgroot = [vtset.movierootFolder results{vtset.treenum}.settings.exp];
%     
% end

% settings = results{vtset.treenum}.settings;


%%%% moved to own function
% function exportTree2CSV(treenum,FileName,PathName,force)
% global vtset
% 
% try
% 
% %%%% NEW:
% % create a subfile for each quantification wavelength
% % header should always be the same:
% % cellNr,timepoint,intensity,area,perimeter,intbyarea,concentration,producationrate,x,y,pos,absoluteTime,stopReason,active,inspected
% 
% 
% exportstruct = cell(1,numel(vtset.results{treenum}.quants));
% 
% for q = 1:numel(vtset.results{treenum}.quants)
%     %init
%     exportstruct{q}.cellNr = vtset.results{treenum}.quants(q).cellNr;
%     exportstruct{q}.timepoint = vtset.results{treenum}.quants(q).timepoint;
%     exportstruct{q}.intensity =vtset.results{treenum}.quants(q).int;
%     exportstruct{q}.area = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.perimeter = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.intbyarea = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.concentration = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.productionrate = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.x = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.y = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.pos = vtset.results{treenum}.quants(q).positionIndex;
%     exportstruct{q}.absoluteTime = vtset.results{treenum}.quants(q).absoluteTime;
%     exportstruct{q}.stopReason = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.active = vtset.results{treenum}.quants(q).active;
%     exportstruct{q}.inspected = vtset.results{treenum}.quants(q).inspected;
%     exportstruct{q}.doublinghypothesis = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.linregression = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.cumulative = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     exportstruct{q}.cumulativeNN = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
%     
%     % iterate over index and fill out x,y,area, stopreason
%     delidd=[];
%     for i = 1:numel(vtset.results{treenum}.quants(q).cellNr)
%         tp = vtset.results{treenum}.quants(q).timepoint(i);
%         c = vtset.results{treenum}.quants(q).cellNr(i);
%         dchan=vtset.results{treenum}.quants(q).detectchannel(i);
%         idd = vtset.results{treenum}.detects(dchan).timepoint == tp & vtset.results{treenum}.detects(dchan).cellNr ==c;
%         idn = vtset.results{treenum}.nonFluor.timepoint == tp & vtset.results{treenum}.nonFluor.cellNr ==c;
%         if sum(idd)==0
%             delidd(end+1)=i;
%             continue
%         end
%         exportstruct{q}.x(i) = vtset.results{treenum}.detects(dchan).X(idd);
%         exportstruct{q}.y(i) = vtset.results{treenum}.detects(dchan).Y(idd);
%         exportstruct{q}.area(i) = vtset.results{treenum}.detects(dchan).size(idd);
%         exportstruct{q}.perimeter(i) = vtset.results{treenum}.detects(dchan).perimeter(idd);
%         exportstruct{q}.stopReason(i) = vtset.results{treenum}.nonFluor.stopReason(idn);
%     end
%     
%     if numel(delidd)
%         % delete elements which do not have any detects struct
%         for f = fieldnames(exportstruct{q})'
%             exportstruct{q}.(cell2mat(f))(delidd)=[];
%         end
%     end
%     
%     if ~isempty(exportstruct{q}.area)
%         % calc remaining intbyarea,
%         exportstruct{q}.intbyarea = exportstruct{q}.intensity./exportstruct{q}.area;
%         % and prod, conc, cumsum, doublethes, linreg
%         for c = unique(exportstruct{q}.cellNr)
%             id = exportstruct{q}.cellNr ==c;
%             int = exportstruct{q}.intensity(id);
%             alltps = exportstruct{q}.timepoint(id);
%             
%             prod = diff(int);
%             prod(end+1) = 0;
%             loine=(linspace(1,2^(2/3),numel(int)));
%             conc= (torow(int)./((loine.^(3/2))));
%             
%             exportstruct{q}.productionrate(id) = prod;
%             exportstruct{q}.concentration(id) = conc;
%             
%             % doublethes
%             line = linspace(medianfirst(int,3),medianfirst(int,3)*2,numel(int));
%             alldata = int-line;
%             exportstruct{q}.doublinghypothesis(id) = alldata;
%             
%             % linreg
%             p = polyfit(alltps, int, 1);
%             alldata = polyval(p, alltps);
%             exportstruct{q}.linregression(id) = alldata;
%             
%             % cumsum
%             sumofmothers=0;
%             sumofmothersNN=0;
%             trace = rootpath(c);
%             if numel(trace)>1
%                 for t = trace(1:end-1)
%                     alldata2= exportstruct{q}.intensity(exportstruct{q}.cellNr==t & exportstruct{q}.active==1);
%                     lastmother= exportstruct{q}.intensity(exportstruct{q}.cellNr==floor(t/2) & exportstruct{q}.active==1);
%                     if t == 1
%                         lastmother=0;
%                     end
%                     dadiff2= diff([lastmother(end)/2 alldata2]);
%                     dafiff2NN = dadiff2;
%                     dafiff2NN(dafiff2NN<0)=0;
%                     dafiff2NN(1) = diff([lastmother(end)/2 alldata2(1)]);
%                     
%                     sumofmothers = sumofmothers+sum(dadiff2);
%                     sumofmothersNN = sumofmothersNN+sum(dafiff2NN);
%                 end
%             end
%             lastmother=exportstruct{q}.intensity(exportstruct{q}.cellNr==floor(c/2) & exportstruct{q}.active==1);
%             if c == 1
%                 lastmother=0;
%             end
%             dadiff=diff([lastmother(end)/2 int]);
%             dadiffNN = dadiff;
%                 dadiffNN(dadiffNN<0)=0;
%                 dadiffNN(1) = diff([lastmother(end)/2 int(1)]);
%             alldata = cumsum(dadiff)+sumofmothers;
%             alldataNN = cumsum(dadiffNN)+sumofmothersNN;
%             exportstruct{q}.cumulative(id) = alldata;
%             exportstruct{q}.cumulativeNN(id) = alldataNN;
%         end
%     end
% end
%     
% %%%%%%%% OLD STUFF %%%%%%%%%%%%%%%%%%%%%%
% %{
% clear exportstruct
% results =vtset.results;
% i = vtset.treenum;
% % find missing measurements in different channels, then set them to NaN
% x=[torow(results{i}.quants(1).timepoint) ; torow(results{i}.quants(1).cellNr)]';
% for chan = 2:numel(results{i}.quants)
%     x = union(x,[torow(results{i}.quants(chan).timepoint) ; torow(results{i}.quants(chan).cellNr)]','rows');
% end
% for dchan = 1:numel(results{i}.detects)
%     x = union(x,[torow(results{i}.detects(dchan).timepoint) ; torow(results{i}.detects(dchan).cellNr)]','rows');
% end
% 
% reihenfolge={};
% for chan = 1:numel(results{i}.quants)
%     % find missing value and add them in quants
%     y= [torow(results{i}.quants(chan).timepoint) ; torow(results{i}.quants(chan).cellNr)]';    
%     missing = setdiff(x,y,'rows');
%     
%     for mis = 1: size(missing,1)
%         results{i}.quants(chan).timepoint(end+1) =  missing(mis,1);
%         results{i}.quants(chan).int(end+1) =  -1;
%         results{i}.quants(chan).cellNr(end+1) =  missing(mis,2);
% %         results{i}.quants(chan).positionIndex(end+1) =  missing(mis,1);
%     end
%     y= [torow(results{i}.quants(chan).timepoint) ; torow(results{i}.quants(chan).cellNr)]';
%     
%     % get the order of this channel for the export
%     reihenfolge{chan}= [];
%     for n = 1:size(x,1)
%         %
%         reihenfolge{chan}(end+1) = find(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%     end
%     
% end
% % fill a new struct with all sizes, X, Y, perimeter
% detectexport.size=[];
% detectexport.perimeter=[];
% detectexport.X=[];
% detectexport.Y=[];
% detectexport.absoluteTime=[];
% detectexport.positionIndex=[];
% detectexport.inspected=zeros(1,size(x,1));
% 
% reihenfolgedetect={};
% for dchan= 1:numel(results{i}.detects)
%     y= [torow(results{i}.detects(dchan).timepoint) ; torow(results{i}.detects(dchan).cellNr)]';
%     
%     % fill the struct, but leave inspected as they are; otherwise overwrite
%     
%     
%     
% %     reihenfolgedetect{dchan}= [];
%     for n = find(detectexport.inspected==0)
%         if any(x(n,1) == y(:,1) & x(n,2) == y(:,2))
%         detectexport.size(n) = results{i}.detects(dchan).size(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%         detectexport.perimeter(n) = results{i}.detects(dchan).perimeter(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%         detectexport.X(n) = results{i}.detects(dchan).X(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%         detectexport.Y(n) = results{i}.detects(dchan).Y(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%         detectexport.absoluteTime(n) = results{i}.detects(dchan).absoluteTime(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%         detectexport.positionIndex(n) = results{i}.detects(dchan).positionIndex(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%         detectexport.inspected(n) = results{i}.detects(dchan).inspected(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%         end
%         
%         %
% %         reihenfolgedetect{dchan}(end+1) = find(x(n,1) == y(:,1) & x(n,2) == y(:,2));
%     end
% end
% %cellNr;timepoint;int1;int2;int3;int4;area;intbyarea1;intbyarea2;intbyarea3;intbyarea4;...
% %concentration1;concentration2;concentration3;concentration4;...
% %productionrate1;productionrate2;productionrate3;productionrate4;...
% %x;y;pos;absoluteTime;stopReason;wavelength_1;wavelength_2;wavelength_3;wavelength_4;wavelength_5;wavelength_6;wavelength_7;wavelength_8;wavelength_9
% 
% 
% chan = 1;
% 
% exportstruct.cellNr=results{i}.quants(chan).cellNr(reihenfolge{1});
% exportstruct.timepoint=results{i}.quants(chan).timepoint(reihenfolge{1});
% exportstruct.int1=results{i}.quants(chan).int(reihenfolge{1});
% if numel(results{i}.quants)>1
%     exportstruct.int2=results{i}.quants(2).int(reihenfolge{2});
% else
%     exportstruct.int2=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% if numel(results{i}.quants)>2
%     exportstruct.int3=results{i}.quants(3).int(reihenfolge{3});
% else
%     exportstruct.int3=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% if numel(results{i}.quants)>3
%     exportstruct.int4=results{i}.quants(4).int(reihenfolge{4});
% else
%     exportstruct.int4=zeros(1,numel(results{i}.quants(chan).int))-1;
% end
% 
% 
% exportstruct.area=detectexport.size;
% exportstruct.perimeter=detectexport.perimeter;
% 
% exportstruct.intbyarea1=torow(exportstruct.int1)./torow(exportstruct.area);
% exportstruct.intbyarea1(exportstruct.int1==-1) = -1;
% if numel(results{i}.quants)>1
%     
%     exportstruct.intbyarea2=exportstruct.int2./exportstruct.area;
%     exportstruct.intbyarea2(exportstruct.int2==-1) = -1;
% else
%     exportstruct.intbyarea2=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% if numel(results{i}.quants)>2
%     exportstruct.intbyarea3=exportstruct.int3./exportstruct.area;
%     exportstruct.intbyarea3(exportstruct.int3==-1) = -1;
% else
%     exportstruct.intbyarea3=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% if numel(results{i}.quants)>3
%     exportstruct.intbyarea4=exportstruct.int4./exportstruct.area;
%     exportstruct.intbyarea4(exportstruct.int4==-1) = -1;
% else
%     exportstruct.intbyarea4=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% 
% for chan = 1:numel(results{i}.quants)
%     
%     for c = torow(unique(exportstruct.cellNr))
%         alldata=exportstruct.(sprintf('int%d',chan))(exportstruct.cellNr == c);
%         ids = (exportstruct.cellNr == c);
% %         prod =  derivative_cwt(torow(alldata),'gaus1',10,1/1000)/1000;
%         prod = diff(alldata);
%         prod(end+1) = 0;
%         loine=(linspace(1,2^(2/3),numel(alldata)));
%         conc= (torow(alldata)./((loine.^(3/2))));
%         exportstruct.(sprintf('concentration%d',chan))(ids)=conc;
%         exportstruct.(sprintf('producationrate%d',chan))(ids)=prod;
%         exportstruct.(sprintf('producationrate%d',chan))(exportstruct.(sprintf('int%d',chan))==-1) = -1;
%         exportstruct.(sprintf('concentration%d',chan))(exportstruct.(sprintf('int%d',chan))==-1) = -1;
%     end
%     
% end
% 
% if ~isfield(exportstruct,'concentration2')
%     exportstruct.concentration2=zeros(numel(results{i}.quants(chan).int),1)-1;
%     exportstruct.producationrate2=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% 
% if ~isfield(exportstruct,'concentration3')
%     exportstruct.concentration3=zeros(numel(results{i}.quants(chan).int),1)-1;
%     exportstruct.producationrate3=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% 
% if ~isfield(exportstruct,'concentration4')
%     exportstruct.concentration4=zeros(numel(results{i}.quants(chan).int),1)-1;
%     exportstruct.producationrate4=zeros(numel(results{i}.quants(chan).int),1)-1;
% end
% chan = 1;
% exportstruct.x=detectexport.X;
% exportstruct.y=detectexport.Y;
% exportstruct.pos=detectexport.positionIndex;
% exportstruct.absoluteTime=detectexport.absoluteTime;
% 
% if ~isfield(vtset.results{i}.nonFluor,'stopReason') || ~numel(vtset.results{i}.nonFluor.stopReason)
%     mapmissingstopreason(i)
% end
% 
% y= [torow(results{i}.nonFluor(1).timepoint) ; torow(results{i}.nonFluor(1).cellNr)]';
%     
% reihenfolgenonFluor= [];
% for n = 1:size(x,1)
%     %
%     reihenfolgenonFluor(end+1) = find(x(n,1) == y(:,1) & x(n,2) == y(:,2));
% end
% 
% exportstruct.stopReason=vtset.results{i}.nonFluor.stopReason(reihenfolgenonFluor);
% 
% %     exportstruct.wavelength_1=results{i}.quants(chan).wavelength_1;
% %     exportstruct.wavelength_2=results{i}.quants(chan).wavelength_2;
% %     exportstruct.wavelength_3=results{i}.quants(chan).wavelength_3;
% %     exportstruct.wavelength_4=results{i}.quants(chan).wavelength_4;
% %     exportstruct.wavelength_5=results{i}.quants(chan).wavelength_5;
% %     exportstruct.wavelength_6=results{i}.quants(chan).wavelength_6;
% %     exportstruct.wavelength_7=results{i}.quants(chan).wavelength_7;
% %     exportstruct.wavelength_8=results{i}.quants(chan).wavelength_8;
% %     exportstruct.wavelength_9=results{i}.quants(chan).wavelength_9;
% 
% %}
% catch e 
%     msgbox(sprintf('Export of tree \n%s\nfailed. Structure is not consistent.',vtset.results{treenum}.settings.treefile),'Export','error')
% %     rethrow(e)
%     return
% end
% 
% % init channels names
% channels=cell(1,numel(vtset.results{treenum}.quants));
% for i = 1:numel(vtset.results{treenum}.quants)
%     wl1=vtset.results{treenum}.quants(i).settings.wl;
%     wl1=strsplit_qtfy('.',wl1);
%     wl1=wl1{1};
%     wl2=vtset.results{treenum}.detects(vtset.results{treenum}.quants(i).settings.detectchannel).settings.wl;
%     wl2=strsplit_qtfy('.',wl2);
%     wl2=wl2{1};
%     channels{i} = ['Q' wl1 'D' wl2 vtset.results{treenum}.detects(vtset.results{treenum}.quants(i).settings.detectchannel).settings.trackset.threshmethod];
% end
% 
% doexcel=1;
% 
% for q = 1:numel(exportstruct)
%     if isempty(exportstruct{q}.active)
%         continue
%     end
%     try
%         FileNameT = strsplit_qtfy('.',FileName);
%         FileNameT = [FileNameT{1} '_' strrep(channels{q},' ','') '.' FileNameT{2}];
%         fprintf('Exporting file to %s\n',[PathName FileNameT]);
%         ezwrite([PathName FileNameT],exportstruct{q},',')
%         fprintf('Exporting file to %s\n',[PathName FileNameT(1:end-11) 'xls']);
%         newexp = cell(numel(exportstruct{q}.active)+1,numel(fieldnames( exportstruct{q})));
%         % header
%         ccounter=0;
%         for f = fieldnames(exportstruct{q})'
%             ccounter=ccounter+1;
%             newexp{1,ccounter}=cell2mat(f);
%             newexp(2:end,ccounter) = num2cell(exportstruct{q}.(cell2mat(f)));
%         end
%         if ispc && doexcel
%             try
%                 xlswrite([strrep(strrep(PathName,'/','\'),'\\','\') FileNameT(1:end-11) 'xls'],newexp);
%                 
%             catch e
%                 if doexcel
%                     
%                     errordlg('Excel export not possible. Please check if Excel is installed properly.')
%                 end
%                 doexcel = 0;
%             end
%         end
%     catch e
%         msgbox(sprintf('Export of tree \n%s\ninto file\n%s\nfailed.',vtset.results{vtset.treenum}.settings.treefile,[PathName FileName]),'Export','error')
%         rethrow(e)
%     end
%     
% end
% if ~force
%     msgbox(sprintf('Export of tree \n%s\ninto file(s)\n%s\nsuccessfull',vtset.results{vtset.treenum}.settings.treefile,[PathName FileName]),'Export','none')
% end











% --- Executes on button press in ButtonExport.
function ButtonExport_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonExport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
suggest=[vtset.tttrootFolder '/'];
filename = strsplit_qtfy('.',  vtset.results{vtset.treenum}.settings.treefile);
suggest = [suggest filename{1} '.amt_simple'];
[FileName,PathName] = uiputfile('*.amt_simple','Enter filename to export csv-file (amt_simple)',suggest);
%prevent overwriting
if FileName == 0
    return;
end
wh = waitbar(0,'Exporting tree...');
try
exportTree2CSV(vtset.treenum,FileName,PathName,0)
catch e
end
close(wh);


% --- Executes on button press in cellareabutton.
function cellareabutton_Callback(hObject, eventdata, handles)
% hObject    handle to cellareabutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cellareabutton
set(hObject,'Value',1);
refreshplot('size');


% --- Executes on button press in ButtonExportAll.
function ButtonExportAll_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonExportAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

answer=questdlg('Export csv?');

if strcmp('Y',answer(1))
%     suggest=[vtset.results{vtset.treenum}.settings.treeroot '/..'];
%     PathName = uigetdir(suggest,'Select folder to export csv-files (amt_simple)');
    
%     if PathName==0
%         return;
%     end
    wh = waitbar(0,'Exporting csv...');
    for treenum=1:numel(vtset.results)
        waitbar(treenum/numel(vtset.results),wh)
        pos = strsplit_qtfy('/',strrep(vtset.results{treenum}.settings.treeroot,'\','/'));
        PathName = [vtset.tttfilesFolder '/' pos{end} '/'];
        if ~exist(PathName,'dir')
            fprintf('Position file folder (%s) not found, trying to export to tttrootfolder\n',PathName)
            PathName = vtset.tttfilesFolder;
        end
%         PathName = [pwd '/'];
%         PathName = strrep(PathName,'/nfs/ttt/','X:\');
        filename = strsplit_qtfy('.',  vtset.results{treenum}.settings.treefile);
        FileName = [filename{1} '.amt_simple'];
%         FileName = [vtset.results{treenum}.settings.treefile(1:end-4) '.csv'];
        exportTree2CSV(treenum,FileName,PathName,1)
    end
    close(wh)
end

answer=questdlg('Export timecourse plots?');

if strcmp('Y',answer(1))
    if ~ishandle(vtset.hplot)
        errordlg('Error exporting screenshots. Please make sure that the time course windows is open.')
        return
    end
    PathName = uigetdir('','Select folder to export jpg-files');
    if PathName==0
        return;
    end
    wh = waitbar(0,'Exporting jpg...');
    temp = vtset.treenum;
    xscale = get(vtset.haxes(1),'Xlim');
    yscale = get(vtset.haxes(1),'ylim');
    for treenum=1:numel(vtset.results)
        vtset.treenum = treenum;
        initFigure()
        redraw();
        showtree()
        drawnow;
        waitbar(treenum/numel(vtset.results),wh)
        filename = strsplit_qtfy('.',  vtset.results{vtset.treenum}.settings.treefile);
        FileName = filename{1};%[vtset.results{vtset.treenum}.settings.treefile(1:end-4) '.jpg'];
%         for m = 1:numel(vtset.haxes)
%             set(vtset.haxes(m),'Xlim',xscale)
%             set(vtset.haxes(m),'ylim',yscale)
%         end
        print(vtset.hplot,[PathName '/' FileName],'-djpeg')
        
    end
    vtset.treenum=temp;
    close(wh)
end


% --- Executes on button press in checkbox_ShowTree.
function checkbox_ShowTree_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_ShowTree (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
% Hint: get(hObject,'Value') returns toggle state of checkbox_ShowTree
vtset.combinedOutput = get(hObject,'Value');
lstTrees_Callback([],[],handles)




% --- Executes on button press in Button_CopyToClipboard.
function Button_CopyToClipboard_Callback(hObject, eventdata, handles)
% hObject    handle to Button_CopyToClipboard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
try
    toplot = vtset.hplot;
    if ~vtset.combinedOutput
        b = questdlg('Copy time course or tree view?','Copy to clipboard','Time course','Tree view', 'Time course');
        if strcmp(b(2),'r')
            toplot=vtset.htree;
        end
    end
    print(toplot, '-dmeta');
catch e
    fprintf('Copy to Clipboard not possible.\n');
end


% --- Executes on button press in Checkbox_ShowHalo.
function Checkbox_ShowHalo_Callback(hObject, eventdata, handles)
% hObject    handle to Checkbox_ShowHalo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Checkbox_ShowHalo
global vtset
vtset.showhalo = get(hObject,'Value');


% --- Executes on button press in checkboxShowCellNumber.
function checkboxShowCellNumber_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShowCellNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxShowCellNumber
global vtset
vtset.showcellnumber = get(hObject,'Value');
showtree();


function setCellActivity()
global vtset


% ask for cell(s)
tree = vtset.treenum;
cells  = inputdlg('Enter cells which timepoints should be set to active','Set measurement activity');

if isempty(cells)
    return
end
cells = cells{1};
cells=strrep(cells, '-',':');
cells=strrep(cells, ',',' ');
cells=['[' cells ']' ];
cells=eval(cells);

% set all quants active and remember them
quantids={};
counter = 0;
for channel = 1:numel(vtset.results{tree}.quants);
    
    allids = ismember(vtset.results{tree}.quants(channel).cellNr,cells) & vtset.results{tree}.quants(channel).active==0;
%     notactive = vtset.results{tree}.quants(channel).active(allids)==0;
    quantids{channel}=allids;
    counter= counter+sum(allids);
    vtset.results{tree}.quants(channel).active(allids)=1;
end
wh = waitbar(0,sprintf('Found %d missing quantifications, running segmentation...',counter));
pause(0.5);
% detect them
for channel = 1:numel(vtset.results{tree}.detects);
    waitbar(0,wh,sprintf('Running segmentation Channel %d of %d...',channel,numel(vtset.results{tree}.detects)));
    allids = ismember(vtset.results{tree}.detects(channel).cellNr,cells) & vtset.results{tree}.detects(channel).active == 0;
%     notactive = vtset.results{tree}.detects(channel).active(allids)==0;
    vtset.results{tree}.detects(channel).active(allids)=1;
    % detect them
    detectCells(channel,tree,cells,vtset.results{tree}.detects(channel).timepoint(allids))
end

waitbar(0,wh,sprintf('Found %d missing quantifications, running quantification...',counter));
pause(0.5);

% quantify
for channel = 1:numel(vtset.results{tree}.quants);
    waitbar(0,wh,sprintf('Running quantification Channel %d of %d...',channel,numel(vtset.results{tree}.quants)));
    quantifyCells(channel,tree,cells,vtset.results{tree}.quants(channel).timepoint(quantids{channel}))
end

delete(wh);
redraw();


function outlierPostProcessing()
global vtset


treelist = {};
for t = 1:numel(vtset.results)
    treelist{end+1}= vtset.results{t}.settings.treefile;
end

selected=listdlg('liststring',treelist,'name','Select tree to calculate','initialvalue',vtset.treenum,'ListSize',[200 200]);


% ask for channel and cells
if numel(vtset.results{vtset.treenum}.quants)>1
    channels = {};
    for x = 1:numel(vtset.results{vtset.treenum}.quants)
        channels{end+1} = ['Q: ' vtset.results{vtset.treenum}.quants(x).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(x).settings.detectchannel).settings.wl];
    end
    if isfield(vtset,'dcselected')
        ini = vtset.dcselected.channel;
    else
        ini = 1;
    end
    channel = listdlg('Promptstring','Please select wavelength:', 'Name','Show Tree',...
        'Liststring',channels,'ListSize',[200 200],'SelectionMode','single','InitialValue',ini);
    if ~numel(channel)
        return
    end
else
    channel = 1;
end
if isfield(vtset,'dcselected')
    ini = num2str(vtset.dcselected.cell);
else
    ini ='';
end
cells = inputdlg('Please enter cells which should be corrected','Enter cells',1,{ini});

if isempty(cells)
    return
end
cells = cells{1};
cells=strrep(cells, '-',':');
cells=strrep(cells, ',',' ');
cells=['[' cells ']' ];
cells=eval(cells);

% get all params
% upto = 10;

printdat=questdlg('Do you want to see optimization?');
if strcmp(printdat,'Yes')
    printdat=1;
else
    printdat=0;
end
% chan = 'w3';
%%% TODO ask for that
constraints.minArea=100;
constraints.maxArea=500;



% get a feeling for how large deviations are from estimation
allrm=[];
for tree = 1:numel(vtset.results)
    for c = unique(vtset.results{tree}.nonFluor.cellNr)

        alldata=calculateTimeCourse(c,channel,'int',0,tree,'timepoint',1);
        
        % leave one out cross validation
        rm =zeros(numel(alldata),1);
        estimated=zeros(numel(alldata),1);
        for p = 2:numel(alldata)-1
            data = alldata(find(alldata)~=p);
            %         tps = alltps(find(alldata)~=p);
            
            sm = moving_average(data,5);
            
            estimate = (sm(p)+sm(p-1))/2;
            actual = alldata(p);
            estimated(p) = estimate;
            rm(p)=(estimate-actual)^2;
        end
        allrm=[allrm torow(rm)];
    end
end

for tree=selected
    
    
    % show?
    if printdat
        h=figure;
        oldpos= get(h,'position');
        set(h,'position',[oldpos(1:2) 1200 400]);
        colormap(gray)
    else
        wh=waitbar(0,'optimizing...');
    end
    
    % do it
    for c= cells
        [alldata,alltps]=calculateTimeCourse(c,channel,'int',0,tree,'timepoint',1);

        
        if printdat
            oldalldata = alldata;
            oldalltps = alltps;
        end
        
        % leave one out cross validation
        rm =zeros(numel(alldata),1);
        estimated=zeros(numel(alldata),1);
        for p = 2:numel(alldata)-1
            data = alldata(find(alldata)~=p);
            tps = alltps(find(alldata)~=p);
            
            sm = moving_average(data,10);
            
            estimate = (sm(p)+sm(p-1))/2;
            actual = alldata(p);
            estimated(p) = estimate;
            rm(p)=(estimate-actual)^2;
        end
        
        
        
        
        opts.StopTemp=0.1;
        opts.Verbosity = 2;
        opts.StopVal = 1e-2;
        opts.InitTemp = 1;
        str = 0.2;
        opts.Generator = @(v)genthresh(v,str);
        opts.CoolSched = @(T) (.1*T);
        
        
        
        torepair = find(rm > median(allrm(allrm<100))+2*std(allrm(allrm<100)));
        % vtset.results{tree}.quants(channel).oldint = vtset.results{tree}.quants(channel).int
        
        for xyz = torow(torepair)%1:upto            
            tp = alltps(xyz);
            id = vtset.results{tree}.quants(channel).timepoint == tp & vtset.results{tree}.quants(channel).cellNr == c;
            %         Center = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellmask{vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp};
            trackset = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).trackset(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
            x = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).X(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
            y = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).Y(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
            filename = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).filename{vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp};
            img  = loadimage([vtset.results{tree}.settings.imgroot '/' filename],vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).settings.normalize,vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).settings.wl);
            detectchannel = vtset.results{tree}.quants(channel).detectchannel(id);
            %     trackset.win =50;
            
            quantfilename = vtset.results{tree}.quants(channel).filename{id};
            quantimage = loadimage([vtset.results{tree}.settings.imgroot '/' quantfilename], vtset.results{tree}.quants(channel).settings.normalize,vtset.results{tree}.quants(channel).settings.wl);
            quantsubimage = extractSubImage(quantimage, x, y, trackset.win);
            
            trackset.threshmethod='Otsu global';
            
            
            estimate = estimated(xyz);
            %         actual = alldata(order(i));
            
            if printdat
                
                subplot(2,3,1)
                [Center,subimg]=cellSegmentation(img, x, y, trackset,[],[]);
                out = bwperim(Center);
                sub = quantsubimage;
                sub(out) = max(sub(:))*1.2;
                imagesc(sub);
                axis off
                axis equal
                
                
                
                
                subplot(2,3,[4:6])
                cla
                alldata = vtset.results{tree}.quants(channel).int(vtset.results{tree}.quants(channel).cellNr == c);
                alltps = vtset.results{tree}.quants(channel).timepoint(vtset.results{tree}.quants(channel).cellNr == c);
                [alltps,orderx]=sort(alltps);
                alldata=alldata(orderx);
                plot(alltps,alldata,'r')
                hold on
                plot(oldalltps,oldalldata,':k')
                line([tp tp],[min(get(gca,'ylim')) max(get(gca,'ylim'))])
                
                
                subplot(2,3,2)
                axis off
                axis equal
                
            else
                waitbar(find(c==cells)/numel(cells),wh,sprintf('Tree %d of %d, Cell %d of %d',find(tree==selected),numel(selected),find(c==cells),numel(cells)))
            end
            trackset.threshcorr=1;
            % Center = zeros(51,51);
            % Center(5:end-5,5:end-5)=1;
            % now optimize with contraints: max, min area, but maximize intensity
            zuOptimieren = trackset.threshcorr;
            [bestparam bestcost] = fminsearch(@(zuOptimieren)optimizeSingleSegmentation(zuOptimieren,img,x,y,trackset,quantsubimage,constraints,estimate,printdat), zuOptimieren,opts);
            
            
            %     [bestparam bestcost, eflag] = fminsearch(@(zuOptimieren)optimizeSingleSegmentation(zuOptimieren,img,x,y,trackset,quantsubimage,constraints,estimate), zuOptimieren);
            
            trackset.threshcorr = bestparam;
            [seg,subimg]=cellSegmentation(img, x, y, trackset,[],[]);
            
            
            %     seg = region_seg(subimg, double(Center), 1000,1,true);
            % show final segmentation
            if printdat
                subplot(2,3,3)
                out = bwperim(seg);
                sub = quantsubimage;
                sub(out) = max(sub(:))*1.2;
                imagesc(sub);
                axis off
                axis equal
                drawnow
            end
            
            % store everything
            % finally store size & cellmask & perimeter
            idd =vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp;
            vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellmask{idd} = seg;
            vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).trackset(idd) = trackset;
            vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).size(idd) = sum(seg(:));
            vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).perimeter(idd) = sum(sum(bwperim(seg)));
            
            % and quant
            vtset.results{tree}.quants(channel).int(id) = sum(sum(quantsubimage(seg)));
            plotCell(c,channel,vtset.haxes(channel))
            
            
            % and all other channels!
            
            for qchan=1:numel(vtset.results{tree}.quants)
                temp = vtset.results{tree}.quants(qchan).detectchannel(vtset.results{tree}.quants(qchan).cellNr==c & vtset.results{tree}.quants(qchan).timepoint==tp);
                if qchan == channel
                    continue
                end
                
                if temp == detectchannel
                    % load corresponding img
                    idq = vtset.results{tree}.quants(qchan).cellNr==c & vtset.results{tree}.quants(qchan).timepoint==tp;
                    filename = vtset.results{tree}.quants(qchan).filename{idq};
                    qimg = loadimage([vtset.results{tree}.settings.imgroot '/' filename],vtset.results{tree}.quants(qchan).settings.normalize,vtset.results{tree}.quants(qchan).settings.wl);
                    
                    quantsubimage = extractSubImage(qimg, x, y, trackset.win);
                    vtset.results{tree}.quants(qchan).int(idq) = sum(sum(quantsubimage(seg)));
                    plotCell(c,qchan,vtset.haxes(qchan))
                end
            end
            
        end
        
        
    end
end
if ~printdat
    delete(wh)
end


% --- Executes on button press in radiobuttonCumulative.
function radiobuttonCumulative_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonCumulative (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonCumulative
set(hObject,'Value',1);
refreshplot('cumulative')


% --- Executes on button press in radiobuttonCumulativeNN.
function radiobuttonCumulativeNN_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonCumulativeNN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonCumulativeNN
set(hObject,'Value',1);
refreshplot('cumulativeNN')

% --- Executes on button press in radiobuttonLinearRegression.
function radiobuttonLinearRegression_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonLinearRegression (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonLinearRegression
set(hObject,'Value',1);
refreshplot('linearregression');

function recalcCells
% reload TTT data, delete content, update, recalc cell with all children
global vtset

msgbox('You can explicitly enter all cell numbers which should be recalculated, or you can just enter one cell, and select to recalculate all its daughter cells afterwards.')
uiwait
% get cell number(s)
cells = inputdlgGetCellNumbers();

if numel(cells) == 0
    return
end
% do you want all descendents?
a = questdlg(sprintf('Do you want to recalculate all the daughters of these cells: %s',num2str(cells)));
if strcmp(a(1),'C')
    return
end

wh = waitbar(0,'Getting new TTT data');

% reload TTT data
try
    xmlfilename = [vtset.movieFolder  '/' vtset.results{vtset.treenum}.settings.exp '_TATexp.xml'];
    position = strsplit_qtfy('-',vtset.results{vtset.treenum}.settings.treefile);
    tttname=[vtset.tttrootFolder '20' vtset.results{vtset.treenum}.settings.exp(1:2) '/' vtset.results{vtset.treenum}.settings.exp '/' position{1} '/' vtset.results{vtset.treenum}.settings.treefile];
    tttname= strrep(tttname,'_simple','');
    res = tttParser(tttname,xmlfilename);
catch e
    delete(wh)
    errordlg('Could not load new TTT data.')
    rethrow(e)
end
% do not check for changes (assumption: change exists)

% find all children
allcells=cells;

if strcmp(a(1),'Y')
    for c = cells
        allcells = [allcells getchildren(c,max(res.cellNr))];
    end
end

waitbar(0,wh,'Deleting old content')
% delete old stuff
try
    % first  quants
    for chan = 1:numel(vtset.results{vtset.treenum}.quants)
        ids = ismember(vtset.results{vtset.treenum}.quants(chan).cellNr,allcells);
        for field=fields(vtset.results{vtset.treenum}.quants)'
            if ~strcmp(field,'settings')
                vtset.results{vtset.treenum}.quants(chan).(cell2mat(field))(ids)=[];
            end
        end
    end
    % all detects
    for chan = 1:numel(vtset.results{vtset.treenum}.detects)
        ids = ismember(vtset.results{vtset.treenum}.detects(chan).cellNr,allcells);
        for field=fields(vtset.results{vtset.treenum}.detects)'
            if ~strcmp(field,'settings')
                vtset.results{vtset.treenum}.detects(chan).(cell2mat(field))(ids)=[];
            end
        end
    end
    % and nonFluor
    ids = ismember(vtset.results{vtset.treenum}.nonFluor.cellNr,allcells);
    for field=fields(vtset.results{vtset.treenum}.nonFluor)'
        if ~strcmp(field,'settings') && ~numel(vtset.results{vtset.treenum}.nonFluor.(cell2mat(field))) == 0
            vtset.results{vtset.treenum}.nonFluor.(cell2mat(field))(ids)=[];
        end
    end
catch e
    delete(wh)
    errordlg('Could not delete old files. Please close QTFy, do NOT save this tree, and report corrupt QTFy file.')
    rethrow(e)
end
% find relevant res elemtes
ids = ismember(res.cellNr,allcells);

% delete rest, and try to feed new struct with gatherimages
for field=fields(res)'
    res.(cell2mat(field))(~ids)=[];
end

waitbar(0,wh,'Checking new content')

% insert new
% first  quants
for chan = 1:numel(vtset.results{vtset.treenum}.quants)
    resquant = gatherAbsTimeAndFileName(res,vtset.results{vtset.treenum}.settings.imgroot,vtset.results{vtset.treenum}.settings.exp,vtset.results{vtset.treenum}.quants(chan).settings.wl,vtset.results{vtset.treenum}.settings.moviestart);
    ids = resquant.absoluteTime~=0;
    for field=fields(vtset.results{vtset.treenum}.quants)'
        if ~strcmp(field,'settings') && ~any(strcmp(field,{'int','active','inspected','detectchannel'}))
            vtset.results{vtset.treenum}.quants(chan).(cell2mat(field))(end+1:end+sum(ids))=resquant.(cell2mat(field))(ids);
        elseif any(strcmp(field,{'int','active','inspected'}))
            vtset.results{vtset.treenum}.quants(chan).(cell2mat(field))(end+1:end+sum(ids))=zeros(sum(ids),1)+strcmp(field,'active');
        elseif strcmp(field,'detectchannel')
            vtset.results{vtset.treenum}.quants(chan).(cell2mat(field))(end+1:end+sum(ids))=zeros(sum(ids),1)+vtset.results{vtset.treenum}.quants(chan).settings.detectchannel;            
        end
    end
end

% and detects
for chan = 1:numel(vtset.results{vtset.treenum}.detects)
    resquant = gatherAbsTimeAndFileName(res,vtset.results{vtset.treenum}.settings.imgroot,vtset.results{vtset.treenum}.settings.exp,vtset.results{vtset.treenum}.detects(chan).settings.wl,vtset.results{vtset.treenum}.settings.moviestart);
    ids = resquant.absoluteTime~=0;
    for field=fields(vtset.results{vtset.treenum}.detects)'
        if ~strcmp(field,'settings') && ~any(strcmp(field,{'cellmask','size','trackset','active','inspected','perimeter'}))
            vtset.results{vtset.treenum}.detects(chan).(cell2mat(field))(end+1:end+sum(ids))=resquant.(cell2mat(field))(ids);
        elseif any(strcmp(field,{'size','active','inspected','perimeter'}))
            vtset.results{vtset.treenum}.detects(chan).(cell2mat(field))(end+1:end+sum(ids))=zeros(sum(ids),1)+strcmp(field,'active');
        elseif strcmp(field,'trackset')
            vtset.results{vtset.treenum}.detects(chan).(cell2mat(field))(end+1:end+sum(ids))=repmat(vtset.results{1}.detects(1).settings.trackset,1,sum(ids));
        elseif strcmp(field,'cellmask')
            vtset.results{vtset.treenum}.detects(chan).(cell2mat(field))(end+1:end+sum(ids))=cell(sum(ids),1);
        end
    end
end

% nonFluor
resquant = gatherAbsTimeAndFileName(res,vtset.results{vtset.treenum}.settings.imgroot,vtset.results{vtset.treenum}.settings.exp,vtset.results{vtset.treenum}.nonFluor.settings.wl,vtset.results{vtset.treenum}.settings.moviestart);
ids = resquant.absoluteTime~=0;
for field=fields(vtset.results{vtset.treenum}.nonFluor)'
    if ~strcmp(field,'settings') &&  ~numel(vtset.results{vtset.treenum}.nonFluor.(cell2mat(field))) == 0
        vtset.results{vtset.treenum}.nonFluor.(cell2mat(field))(end+1:end+sum(ids))=resquant.(cell2mat(field))(ids);
    end
end

waitbar(0,wh,'Detecting new cells')
% recalc everything new
% new detects
for chan = 1:numel(vtset.results{vtset.treenum}.detects)
    detectCells(chan,vtset.treenum,allcells,'complete')
end

waitbar(0,wh,'Quantifying new cells')
% new quants
for chan = 1:numel(vtset.results{vtset.treenum}.quants)
    quantifyCells(chan,vtset.treenum,allcells,'complete')
end

delete(wh)

% refresh plot
vtset.updateCallback();

function tttfile = getFullTTT(treefile)
global vtset
pos = regexp(treefile, '(_p)(\d{3,4})', 'match');
if isempty(pos)
    tttfile = [];
    return
end
alldash = strfind(treefile,'_');
exp = treefile(1:alldash(1)-1);
pos=(pos{1}(3:end));
tttfile = [vtset.tttrootFolder '/20' exp(1:2) '/' exp '/' exp '_p' pos '/' treefile];


function lastTTTchange = getTTTtimestamp(treefile)

treefile = getFullTTT(treefile);
if isempty(treefile)
    fprintf('treefile not consistent: Cannot determine tttfile timestamp.\n')
    lastTTTchange = 0;
    return
end

if exist(treefile,'file')
    d = dir(treefile);
    lastTTTchange = d.datenum;
    
else
    fprintf('ttt-file not found: Cannot determine tttfile timestamp.\n')
    lastTTTchange = 0;
    return
end

function updateQTFfiles(treeids,force)
global vtset
fprintf('will update %d file(s)...\n',numel(treeids));
wh=waitbar(0,'Checking for new ttt data...');
skip = 0;
counter=0;
for tree = treeids
    % load new ttt file
    treefile = getFullTTT(vtset.results{tree}.settings.treefile);
    if strfind(treefile,'simple')
        fprintf('Could not read ttt simple file!!\n')
        continue
    end
    xmlfilename = [vtset.movieFolder '/' vtset.results{tree}.settings.exp '_TATexp.xml'];

    if~exist(xmlfilename,'file')
        errordlg(sprintf('XML file not found\n%s\nCannot update QTFy files.',xmlfilename))
        break
    end
    
    res = tttParser(treefile,xmlfilename);
    
    % check for differences
    if ~numel(res)
        continue
    end
    
    % first see if somethin was deleted
    counter=counter+1;
    waitbar(counter/numel(treeids),wh,'Deleting timepoints...')
    [c,ids]=setdiff([tocolumn(vtset.results{tree}.nonFluor.timepoint) tocolumn(vtset.results{tree}.nonFluor.cellNr) tocolumn(vtset.results{tree}.nonFluor.X) tocolumn(vtset.results{tree}.nonFluor.Y)],[res.timepoint res.cellNr res.X res.Y],'rows');
    %%%% TODO: check if all field have same amount of entries, otherwise
    %%%% delete field, just like annotations
    if numel(ids)
        % set to unsaved
        vtset.unsaved(tree)=1;
        
        fprintf('Deleting %d timepoints...\n',numel(ids));
        % del all quants
        for q = 1:numel(vtset.results{tree}.quants)
            delids = false(1,numel(vtset.results{tree}.quants(q).cellNr));
            for x = 1:numel(ids)
                delids = delids | vtset.results{tree}.quants(q).cellNr == c(x,2) & vtset.results{tree}.quants(q).timepoint == c(x,1);
            end 
            if any(delids)
                deleteTPs(tree,'quants',q,delids)
            end
        end
        % and all detects
        for q = 1:numel(vtset.results{tree}.detects)
            delids = false(1,numel(vtset.results{tree}.detects(q).cellNr));
            for x = 1:numel(ids)
                delids = delids | vtset.results{tree}.detects(q).cellNr == c(x,2) & vtset.results{tree}.detects(q).timepoint == c(x,1);
            end
            
            if any(delids)
                deleteTPs(tree,'detects',q,delids)
            end
        end
        % finally nonFluor
        for q = 1:numel(vtset.results{tree}.nonFluor)
            delids = false(1,numel(vtset.results{tree}.nonFluor(q).cellNr));
            for x = 1:numel(ids)
                delids = delids | vtset.results{tree}.nonFluor(q).cellNr == c(x,2) & vtset.results{tree}.nonFluor(q).timepoint == c(x,1);
            end
            
            if any(delids)
                deleteTPs(tree,'nonFluor',q,delids)
            end
        end
    end
    
    
    % now see whats new    
    waitbar(counter/numel(treeids),wh,'Checking for new ttt data...')

    [c possibleids] = setdiff([res.timepoint res.cellNr res.X res.Y],[tocolumn(vtset.results{tree}.nonFluor.timepoint) tocolumn(vtset.results{tree}.nonFluor.cellNr) tocolumn(vtset.results{tree}.nonFluor.X) tocolumn(vtset.results{tree}.nonFluor.Y)],'rows');
    
    if ~numel(possibleids)
        fprintf('No new tracked cells found for tree%s\n',vtset.results{tree}.settings.treefile)
        % and new timestamp
        vtset.results{tree}.settings.lastTTTchange =  getTTTtimestamp(vtset.results{tree}.settings.treefile);
        % no update anymore possible
        vtset.possibleUpdate(tree)=0;
        continue
    else
        if skip
            fprintf('Skipping new data for tree%s\n',vtset.results{tree}.settings.treefile)
            continue
        end
        %aks if you want to do it now or later
        if ~force
            a = questdlg(sprintf('There are new tracks for tree %s\nDo you want to quantify it now?',vtset.results{tree}.settings.treefile),'Found new data','Yes','Yes, do them all','No, skip all','Yes');
            if numel(a) == 3
            elseif strcmp(a(1),',')
                force=1;
            else
                skip=1;
            end
        end
    end
    
    if skip
        continue
    end
    
    % filter res for only new stuff
    for f = fieldnames(res)'
        res.(cell2mat(f)) = res.(cell2mat(f))(possibleids);
    end
    % update all structs
    filereader(res,tree);
    
    %%% detect!
    waitbar(0,wh,'Starting detection process...')
    for i = 1:numel(vtset.results{tree}.detects)
        waitbar(i/numel(vtset.results{tree}.detects),wh,sprintf('Detecting channel %d of %d',i,numel(vtset.results{tree}.detects)))
        detectCells(i,tree,'only new cells',find(vtset.results{tree}.detects(i).active==1 & vtset.results{tree}.detects(i).size ==0 ) )
    end
    
    %%% quantify
    waitbar(0,wh,'Quantifying all channels...')
    for i = 1:numel(vtset.results{tree}.quants)
        waitbar(i/numel(vtset.results{tree}.quants),wh,sprintf('Quantifying channel %d of %d',i,numel(vtset.results{tree}.quants)))
        quantifyCells(i,tree,'only new cells',find(vtset.results{tree}.quants(i).int == -1 ) )
    end
    
    % update plot axis
    
    vtset.results{tree}.setting.xlim='auto';
    
    
    % set to unsaved
    vtset.unsaved(tree)=1;
    % and possibleUpdate
    vtset.possibleUpdate(tree)=0;
    % and new timestamp
    vtset.results{tree}.settings.lastTTTchange =  getTTTtimestamp(vtset.results{tree}.settings.treefile);
    %%% check additional annotations
    if isfield(vtset.results{tree}.nonFluor,vtset.results{tree}.settings.anno) && numel(vtset.results{tree}.nonFluor.(vtset.results{tree}.settings.anno)) ~= numel(vtset.results{tree}.nonFluor.X)
        try
            mapmissingannotation(tree,vtset.results{tree}.settings.anno);
        catch e
            fprintf('could not map annotation %s reason\n',vtset.results{tree}.settings.anno)
            vtset.results{tree}.nonFluor.(vtset.results{tree}.settings.anno) = zeros(1,numel(vtset.results{tree}.nonFluor.cellNr));
        end
        
    end
    if isfield(vtset.results{tree}.nonFluor,vtset.results{tree}.settings.anno2) && numel(vtset.results{tree}.nonFluor.(vtset.results{tree}.settings.anno2)) ~= numel(vtset.results{tree}.nonFluor.X)
        try
            mapmissingannotationLocal(tree,vtset.results{tree}.settings.anno2);
        catch e
            fprintf('could not map annotation %s reason\n',vtset.results{tree}.settings.anno2)
            vtset.results{tree}.nonFluor.(vtset.results{tree}.settings.anno2) = zeros(1,numel(vtset.results{tree}.nonFluor.cellNr));
        end
        
    end
end

delete(wh);


initializeList();
if ishandle(vtset.hplot)
    handles = guidata(vtset.hObject);
    
    lstTrees_Callback([],[],handles)
end


% --------------------------------------------------------------------
function MenuqtfyMain_Callback(hObject, eventdata, handles)
% hObject    handle to MenuqtfyMain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function MenuHelp_Callback(hObject, eventdata, handles)
% hObject    handle to MenuHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function MenuAbout_Callback(hObject, eventdata, handles)
% hObject    handle to MenuAbout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
showsplashscreen();

% --------------------------------------------------------------------
function MenuNew_Callback(hObject, eventdata, handles)
% hObject    handle to MenuNew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
CreateNewTreeSet(handles,[]);

% --------------------------------------------------------------------
function MenuLoad_Callback(hObject, eventdata, handles)
% hObject    handle to MenuLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
loadTrees_ClickedCallback([], [], handles)

% --------------------------------------------------------------------
function MenuSave_Callback(hObject, eventdata, handles)
% hObject    handle to MenuSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
saveTrees_ClickedCallback([], [], handles)

% --------------------------------------------------------------------
function MenuPreferences_Callback(hObject, eventdata, handles)
% hObject    handle to MenuPreferences (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function ManuExit_Callback(hObject, eventdata, handles)
% hObject    handle to ManuExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
close(vtset.hObject)

% --------------------------------------------------------------------
function MenuSaveAll_Callback(hObject, eventdata, handles)
% hObject    handle to MenuSaveAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
saveTrees(1:numel(vtset.results),1)

% --------------------------------------------------------------------
function MenuSelectNAS_Callback(hObject, eventdata, handles)
% hObject    handle to MenuSelectNAS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
tttrootFolder=selectNAS;
if tttrootFolder ~=0
    vtset.tttfilesFolder= [tttrootFolder '/'];
    vtset.tttrootFolder = [tttrootFolder '/'];
    vtset.AMTfilesFolder = [tttrootFolder '/'];
end


% --------------------------------------------------------------------
function MenuClearMemory_Callback(hObject, eventdata, handles)
% hObject    handle to MenuClearMemory (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imageCache('init',50);


function requantifyCells()
% this function will be called by the tools menu and will requantify all
% cells of selected trees (e.g. if you have new backgrounds)
global vtset
% ask for tree list

treelist = {};
for t = 1:numel(vtset.results)
    treelist{end+1}= vtset.results{t}.settings.treefile;
end

selected=listdlg('liststring',treelist,'name','Select tree to calculate','initialvalue',vtset.treenum,'ListSize',[200 200]);



% do the calculations
wh =waitbar(0,'Requantifying cells...');
for t = selected
    for chan = 1:numel(vtset.results{t}.quants)
        try
        quantifyCells(chan,t,'all','complete')
        vtset.unsaved(t)=1;
        catch
            % do i have to catch anything here?
            
        end
    end
end
close(wh)

    function displayAll(cell, c,hline)
        global vtset
        vtset.htreelines(end+1) = hline;
        vtset.htreecells(end+1) = c;
        vtset.htreeannotation(end+1) = 1;
        % Define a context menu; it is not attached to anything
        hcmenu = uicontextmenu('parent',get(vtset.haxes(end),'parent'));
        % Define callbacks for context menu items that change linestyle
        hcb1 = ['global vtset;vtset.hide=''' num2str(cell) ''';vtset.setVisibilty();'];
        hcb2 = ['global vtset;vtset.show=''' num2str(cell) ''';vtset.setVisibilty();'];
        % Define the context menu items and install their callbacks
        %             set(item1,'Callback',hcb1);
        %             set(item2,'Callback',hcb2);
        item1 = uimenu(hcmenu, 'Label', 'Hide branch', 'Callback', hcb1);
        item2 = uimenu(hcmenu, 'Label', 'Show branch', 'Callback', hcb2);
        % open cell inspector
        cb = ['global vtset;if ~isfield(vtset,''dcselected'');msgbox(''no channel selected'');return;end;vtset.dcselected.cell = ' num2str(cell) ';'...
            'vtset.dcselected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.dcselected.channel).cellNr == ' num2str(cell) '));'...
            'vtset.trackCell([],[],[]);'];
        item2B = uimenu(hcmenu, 'Label', 'Inspect this cell', 'Callback', cb);
        % define color change callbacks & menu items
        item3 = uimenu(hcmenu, 'Label', 'Cell color');
        item31 = uimenu(item3, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(cell) ',0);']);
        item32 = uimenu(item3, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(cell) ',0);']);
        item33 = uimenu(item3, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(cell) ',0);']);
        item34 = uimenu(item3, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(cell) ',0);']);
        item35 = uimenu(item3, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(cell) ',0);']);
        item36 = uimenu(item3, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0 0 0]'',' num2str(cell) ',0);']);
        item37 = uimenu(item3, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(cell) ',0);']);
        item38 = uimenu(item3, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(cell) ',0);']);
        item39 = uimenu(item3, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(cell) ',0);']);
        
        item4 = uimenu(hcmenu, 'Label', 'Branch color');
        item41 = uimenu(item4, 'Label', 'Green','Callback',['global vtset;vtset.setCellColor(''[0 0.5 0]'',' num2str(cell) ',1);']);
        item42 = uimenu(item4, 'Label', 'Red','Callback',['global vtset;vtset.setCellColor(''[1 0 0]'',' num2str(cell) ',1);']);
        item43 = uimenu(item4, 'Label', 'Cyan','Callback',['global vtset;vtset.setCellColor(''[0 0.75 0.75]'',' num2str(cell) ',1);']);
        item44 = uimenu(item4, 'Label', 'Magenta','Callback',['global vtset;vtset.setCellColor(''[0.75 0 0.75]'',' num2str(cell) ',1);']);
        item45 = uimenu(item4, 'Label', 'Yellow','Callback',['global vtset;vtset.setCellColor(''[0.75 0.75 0]'',' num2str(cell) ',1);']);
        item46 = uimenu(item4, 'Label', 'Black','Callback',['global vtset;vtset.setCellColor(''[0 0 0]'',' num2str(cell) ',1);']);
        item47 = uimenu(item4, 'Label', 'Blue','Callback',['global vtset;vtset.setCellColor(''[0 0 1]'',' num2str(cell) ',1);']);
        item48 = uimenu(item4, 'Label', 'Gray','Callback',['global vtset;vtset.setCellColor(''[0.7 0.7 0.7]'',' num2str(cell) ',1);']);
        item49 = uimenu(item4, 'Label', 'Reset Color','Callback',['global vtset;vtset.setCellColor(''Reset'',' num2str(cell) ',1);']);
        
        item5 = uimenu(hcmenu, 'Label', 'Cell marker');
        item51 = uimenu(item5, 'Label', 'dot','Callback',['global vtset;vtset.tools.setCellMarker(''.'',' num2str(cell) ',0);']);
        item52 = uimenu(item5, 'Label', 'plus','Callback',['global vtset;vtset.tools.setCellMarker(''+'',' num2str(cell) ',0);']);
        item53 = uimenu(item5, 'Label', 'square','Callback',['global vtset;vtset.tools.setCellMarker(''square'',' num2str(cell) ',0);']);
        item54 = uimenu(item5, 'Label', 'circle','Callback',['global vtset;vtset.tools.setCellMarker(''o'',' num2str(cell) ',0);']);
        item55 = uimenu(item5, 'Label', 'cross','Callback',['global vtset;vtset.tools.setCellMarker(''x'',' num2str(cell) ',0);']);
        item56 = uimenu(item5, 'Label', 'None','Callback',['global vtset;vtset.tools.setCellMarker(''none'',' num2str(cell) ',0);']);
        
        item6 = uimenu(hcmenu, 'Label', 'Branch marker');
        item61 = uimenu(item6, 'Label', 'dot','Callback',['global vtset;vtset.tools.setCellMarker(''.'',' num2str(cell) ',1);']);
        item62 = uimenu(item6, 'Label', 'plus','Callback',['global vtset;vtset.tools.setCellMarker(''+'',' num2str(cell) ',1);']);
        item63 = uimenu(item6, 'Label', 'square','Callback',['global vtset;vtset.tools.setCellMarker(''square'',' num2str(cell) ',1);']);
        item64 = uimenu(item6, 'Label', 'circle','Callback',['global vtset;vtset.tools.setCellMarker(''o'',' num2str(cell) ',1);']);
        item65 = uimenu(item6, 'Label', 'cross','Callback',['global vtset;vtset.tools.setCellMarker(''x'',' num2str(cell) ',1);']);
        item66 = uimenu(item6, 'Label', 'None','Callback',['global vtset;vtset.tools.setCellMarker(''none'',' num2str(cell) ',1);']);
        
        item7 = uimenu(hcmenu, 'Label', 'Line width');
        item71 = uimenu(item7, 'Label', '1', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(1'',' num2str(cell) ',0);']);
        item72 = uimenu(item7, 'Label', '2', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(2'',' num2str(cell) ',0);']);
        item73 = uimenu(item7, 'Label', '3', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(3'',' num2str(cell) ',0);']);
        item74 = uimenu(item7, 'Label', '4', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(4'',' num2str(cell) ',0);']);
        item75 = uimenu(item7, 'Label', '5', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(5'',' num2str(cell) ',0);']);
        item76 = uimenu(item7,'separator','on', 'Label', 'Custom', 'Callback', ['global vtset; x=inputdlg(''Enter line with'',''Timecourse line width'',1);if isempty(x);return;end;vtset.tools.setPlotLineWidth(str2double(x)'',' num2str(cell) ',0);']);
        
        item8 = uimenu(hcmenu, 'Label', 'Branch width');
        item81 = uimenu(item8, 'Label', '1', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(1'',' num2str(cell) ',1);']);
        item82 = uimenu(item8, 'Label', '2', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(2'',' num2str(cell) ',1);']);
        item83 = uimenu(item8, 'Label', '3', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(3'',' num2str(cell) ',1);']);
        item84 = uimenu(item8, 'Label', '4', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(4'',' num2str(cell) ',1);']);
        item85 = uimenu(item8, 'Label', '5', 'Callback', ['global vtset; vtset.tools.setPlotLineWidth(5'',' num2str(cell) ',1);']);
        item86 = uimenu(item8,'separator','on', 'Label', 'Custom', 'Callback', ['global vtset; x=inputdlg(''Enter line with'',''Timecourse line width'',1);if isempty(x);return;end;vtset.tools.setPlotLineWidth(str2double(x)'',' num2str(cell) ',1);']);
        
        item9 = uimenu(hcmenu, 'Label', 'Line style');
        item91 = uimenu(item9, 'Label', 'Solid', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-'',' num2str(cell) ',0);']);
        item92 = uimenu(item9, 'Label', 'Dashed', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''--'',' num2str(cell) ',0);']);
        item93 = uimenu(item9, 'Label', 'Dotted', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle('':'',' num2str(cell) ',0);']);
        item94 = uimenu(item9, 'Label', 'Dash-dot line', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-.'',' num2str(cell) ',0);']);
        item95 = uimenu(item9, 'Label', 'NoLine', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''none'',' num2str(cell) ',0);']);
        
        item10 = uimenu(hcmenu, 'Label', 'Branch style');
        item101 = uimenu(item10, 'Label', 'Solid', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-'',' num2str(cell) ',1);']);
        item102 = uimenu(item10, 'Label', 'Dashed', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''--'',' num2str(cell) ',1);']);
        item103 = uimenu(item10, 'Label', 'Dotted', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle('':'',' num2str(cell) ',1);']);
        item104 = uimenu(item10, 'Label', 'Dash-dot line', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''-.'',' num2str(cell) ',1);']);
        item105 = uimenu(item10, 'Label', 'NoLine', 'Callback', ['global vtset; vtset.tools.setPlotLineStyle(''none'',' num2str(cell) ',1);']);
        % Attach the context menu to each line
        set(hline,'uicontextmenu',hcmenu)
        % also set to whole cell line in treeview
        set(vtset.htreelines(vtset.htreecells == cell & vtset.htreeannotation==0),'uicontextmenu',hcmenu)
        if ismember(cell,vtset.cells)
            set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb1))
            if c == cell
                % also set to whole cell line in treeview
                set(vtset.htreelines(vtset.htreecells == cell & vtset.htreeannotation==0),'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb1))
            end
        else
            set(hline,'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb2))
            if c == cell
                set(vtset.htreelines(vtset.htreecells == cell & vtset.htreeannotation==0),'ButtonDownFcn',sprintf('if strcmp(''normal'',(get(gcf,''SelectionType'')));%s\nend',hcb2))
            end
        end
        
        
        
        


% --- Executes on button press in ButtonDeleteOutliersGui.
function ButtonDeleteOutliersGui_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDeleteOutliersGui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
optimizeSegmentationGUI
