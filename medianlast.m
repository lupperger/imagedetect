% returns the median of num last elements
% if num > numel(vec) returns the median of numel(vec) last elements

function ret=medianlast(vec, num)

if numel(vec)<=num
    ret=medianfirst(vec,numel(vec));
else
    ret=median(vec(end-num+1:end));
end