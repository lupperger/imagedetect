function gatherimages(res,tree,ch,x)
%%
global vtset

imgroot = vtset.results{tree}.settings.imgroot;

if strcmp(ch(1),'D')
    channel = vtset.results{tree}.detects;
elseif strcmp(ch(1),'Q')
    channel = vtset.results{tree}.quants;
else
    channel = vtset.results{tree}.nonFluor;
end

for posi = unique(res.positionIndex)'
    fprintf('gathering position %d files %s...',posi,channel(x).settings.wl)
    
    % read position log file to get absolute timepoints otherwise use old
    % method
    if exist(sprintf('%s/%s_p%04d/',imgroot,vtset.results{tree}.settings.exp, posi),'dir')
        newexperiment=1;
        filename = sprintf('%s/%s_p%04d/%s_p%04d.log',imgroot,vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi);
    else
        filename = sprintf('%s/%s_p%03d/%s_p%03d.log',imgroot,vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi);
        newexperiment = 0;
    end
    log = positionLogFileReader(filename);
    if ~isempty(log)
        % read logfile
        
        
        wl = strsplit_qtfy('.',channel(x).settings.wl);
        ext = wl{2};
        wl = str2double(wl{1}(2:end));
%         posFolder = [vtset.results{tree}.settings.exp vtset.results{tree}.settings.filesettings.separator [vtset.results{tree}.settings.filesettings.matrix{1,3} [repmat('0',1,(vtset.results{tree}.settings.filesettings.matrix{1,2}-1)) num2str(posi)]]];
        posFolder = [vtset.results{tree}.settings.exp vtset.results{tree}.settings.filesettings.separator vtset.results{tree}.settings.filesettings.matrix{1,3} sprintf(['%0' num2str(vtset.results{tree}.settings.filesettings.matrix{1,2}) 'd'], posi)];

        

        
        idx=find(res.positionIndex == posi)';
        
        
        for id = idx
            % store everything
            abstime = log.absoluteTime(log.timepoint == res.timepoint(id) & log.wavelength == wl);
            if isempty(abstime)
                continue
            end
            channel(x).cellNr(end+1) = res.cellNr(id);
            channel(x).timepoint(end+1) = res.timepoint(id);            
            channel(x).absoluteTime(end+1) = abstime-vtset.results{tree}.settings.moviestart;
            channel(x).positionIndex(end+1) = posi;
            if strcmp(ch(1),'D')
                channel(x).X(end+1) = res.X(id);
                channel(x).Y(end+1) = res.Y(id);
                channel(x).active(end+1) = 1;
                channel(x).inspected(end+1) = 0;
                channel(x).perimeter(end+1) = 0;
                channel(x).size(end+1) = 0;
                channel(x).cellmask{end+1} = [];
                channel(x).trackset(end+1) = struct(channel(x).settings.trackset);
            elseif strcmp(ch(1),'Q')
                %%% TODO check if this always works with every channel
                %%% combination
                channel(x).detectchannel(end+1) = channel(x).settings.detectchannel;
                channel(x).active(end+1) = 0;
                channel(x).int(end+1) = -1;
                channel(x).inspected(end+1) = 0;
            else
                channel(x).stopReason(end+1) = res.stopReason(id);
                channel(x).X(end+1) = res.X(id);
                channel(x).Y(end+1) = res.Y(id);
            end
             imgName = generateFilename(vtset.results{tree}.settings.exp, posi, res.timepoint(id), wl, 1, ext, vtset.results{tree}.settings.filesettings);
             channel(x).filename{end+1} = [posFolder '/' imgName];
%            zindex= '';
%             if newexperiment
%                 if log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl) ~= -1
%                     zindex = sprintf('z%03d_',log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl));
%                 end
%                 % example: 110624AF6_p0024_t00002_w1.tif
%                 % 111103PH5_p0148_t00002_z001_w1.png
%                
%                 channel(x).filename{end+1} = sprintf('%s_p%04d/%s_p%04d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,channel(x).settings.wl);
%             else
%                 channel(x).filename{end+1} = sprintf('%s_p%03d/%s_p%03d_t%05d_%s%s',  vtset.results{tree}.settings.exp,posi,vtset.results{tree}.settings.exp,posi,res.timepoint(id),zindex,channel(x).settings.wl);
%             end
% %             fprintf('.')

        end
    else
        
        %gather all detects
        %     for x = 1:numel(channel)
        wl = strsplit_qtfy('.',channel(x).settings.wl);
        ext = wl{2};
        wl = str2double(wl{1}(2:end));
        posFolder = [vtset.results{tree}.settings.exp vtset.results{tree}.settings.filesettings.separator [vtset.results{tree}.settings.filesettings.matrix{1,3} [repmat('0',1,(vtset.results{tree}.settings.filesettings.matrix{1,2}-1)) posi]]];
        %ext = channel(x).settings.wl;
        foldername = sprintf('%s%s_p%03d/*%s',  imgroot, vtset.results{tree}.settings.exp,posi,ext);
%         newexperiment=0;
        d = dir(foldername);
        if numel(d)==0
            foldername = sprintf('%s%s_p%04d/*%s',  imgroot, vtset.results{tree}.settings.exp,posi,ext);
            d = dir(foldername);
%             newexperiment=1;
        end
        
        fprintf('found %d images...',numel(d))
        % iterate over them and feed results struct, until timepoint is
        % reached
        for file = 1:numel(d)
            % check timepoint
            links = strfind(d(file).name,'_t');
            rechts = strfind(d(file).name,'_w');
            r2= strfind(d(file).name,'_z');
            if numel(r2)
                rechts=r2;
            end
            tp = str2double(d(file).name(links+2:rechts-1));
            if tp>max(res.timepoint)
                break;
            end
            
            % read xml and store absolute time
            xmlfilename=[foldername(1:end-8) '/' d(file).name '_meta.xml'];
            %             sprintf('%s\n',xmlfilename);
            xmlfile = fileread(xmlfilename);
            % very naive
            try
                
                % tat id is 1025 for acquisition time
                coords=strfind(xmlfile,'>1025<');
                % get the xml id
                if numel(coords)>1
                    fprintf('\nWarning: Found more than one TAT identifier for acquisition time. Tying to use first one...\n')
                    coords = coords(1);
                end
                id = xmlfile(coords-3:coords);
                id = strrep(id,'I','');
                
                % get xml value
                coords=strfind(xmlfile,['V' id]);
                abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
            catch e
                
                fprintf('\nError: XML image file error: absolute time not found in %s\n',xmlfilename)
                abstime = vtset.results{tree}.settings.moviestart;
                
                
            end
            
            %get x,y,filename and cellnr for pos&tp&abstime
            idx=find(res.timepoint==tp & res.positionIndex == posi)';
            for id = idx
                % store everything
                channel(x).cellNr(end+1) = res.cellNr(id);
                channel(x).timepoint(end+1) = tp;
                channel(x).absoluteTime(end+1) = abstime-vtset.results{tree}.settings.moviestart;
                channel(x).positionIndex(end+1) = posi;
                if strcmp(ch(1),'D')
                    channel(x).X(end+1) = res.X(id);
                    channel(x).Y(end+1) = res.Y(id);
                    channel(x).active(end+1) = 1;
                    channel(x).inspected(end+1) = 0;
                    channel(x).perimeter(end+1) = 0;
                    channel(x).size(end+1) = 0;
                    channel(x).cellmask{end+1} = [];
                    channel(x).trackset(end+1) = struct(channel(x).settings.trackset);
                    
                elseif strcmp(ch(1),'Q')
                    %%% TODO check if this always works with every channel
                    %%% combination
                    channel(x).detectchannel(end+1) = channel(x).settings.detectchannel;
                    channel(x).active(end+1) = 0;
                    channel(x).inspected(end+1) = 0;
                    channel(x).int(end+1) = -1;
                else
                    channel(x).stopReason(end+1) = res.stopReason(id);
                    channel(x).X(end+1) = res.X(id);
                    channel(x).Y(end+1) = res.Y(id);
                end
                imgName = generateFilename(vtset.results{tree}.settings.exp, posi, tp, wl, 1, ext, vtset.results{tree}.settings.filesettings);
                channel(x).filename{end+1} = [posFolder '/' imgName];
%                 if newexperiment
%                     channel(x).filename{end+1} = sprintf('%s_p%04d/%s',  vtset.results{tree}.settings.exp,posi,d(file).name);
%                 else
%                     channel(x).filename{end+1} = sprintf('%s_p%03d/%s',  vtset.results{tree}.settings.exp,posi,d(file).name);
%                 end
                fprintf('.')
            end
        end
    end
    fprintf('done.\n')
    %     end
    
    if strcmp(ch(1),'D')
        vtset.results{tree}.detects = channel;
    elseif strcmp(ch(1),'Q')
        vtset.results{tree}.quants = channel;
    else
        vtset.results{tree}.nonFluor = channel;
    end
    
end

% function gatherimages(res,tree)
%
% global vtset
%
% imgroot = vtset.results{tree}.settings.imgroot;
%
% for posi = unique(res.positionIndex)'
%     fprintf('gathering position %d...',posi)
%     %gather all detects
%     for x = 1:numel(vtset.results{tree}.detects)
%         ext = vtset.results{tree}.detects(x).settings.wl;
%
%         foldername = sprintf('%s%s_p%03d/*%s',  imgroot, vtset.results{tree}.settings.exp,posi,ext);
%         newexperiment=0;
%         d = dir(foldername);
%         if numel(d)==0
%             foldername = sprintf('%s%s_p%04d/*%s',  imgroot, vtset.results{tree}.settings.exp,posi,ext);
%             d = dir(foldername);
%             newexperiment=1;
%         end
%
%         fprintf('found %d images...',numel(d))
%         % iterate over them and feed results struct, until timepoint is
%         % reached
%         for file = 1:numel(d)
%             % check timepoint
%             links = strfind(d(file).name,'_t');
%             rechts = strfind(d(file).name,'_w');
%             tp = str2double(d(file).name(links+2:rechts-1));
%             if tp>max(res.timepoint(res.positionIndex==posi))
%                 break;
%             end
%
%             % read xml and store absolute time
%             xmlfilename=[foldername(1:end-7) d(file).name '_meta.xml'];
%             xmlfile = fileread(xmlfilename);
%             % very naive
%
%             try
%                 coords=strfind(xmlfile,'V42');
%                 abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
%             catch e
%                 coords=strfind(xmlfile,'V41');
%                 abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
%             end
%
%             %get x,y,filename and cellnr for pos&tp&abstime
%             idx=find(res.timepoint==tp & res.positionIndex == posi)';
%             for id = idx
%                 % store everything
%                 vtset.results{tree}.detects(x).cellNr(end+1) = res.cellNr(id);
%                 vtset.results{tree}.detects(x).timepoint(end+1) = tp;
%                 vtset.results{tree}.detects(x).absoluteTime(end+1) = abstime;
%                 vtset.results{tree}.detects(x).positionIndex(end+1) = posi;
%                 vtset.results{tree}.detects(x).X(end+1) = res.X(id);
%                 vtset.results{tree}.detects(x).Y(end+1) = res.Y(id);
%                 if newexperiment
%                     vtset.results{tree}.detects(x).filename{end+1} = sprintf('%s_p%04d/%s',  vtset.results{tree}.settings.exp,posi,d(file).name);
%                 else
%                     vtset.results{tree}.detects(x).filename{end+1} = sprintf('%s_p%03d/%s',  vtset.results{tree}.settings.exp,posi,d(file).name);
%                 end
%                 fprintf('.')
%             end
%         end
%         fprintf('done.\n')
%
%
%
%     end
%     % gather all quants
%     %%% TODO check if same wl as one detect, then copy, else gather files
%     for x= 1:numel(vtset.results{tree}.quants)
%         vtset.results{tree}.quants(x).cellNr = vtset.results{tree}.detects(1).cellNr;
%         vtset.results{tree}.quants(x).timepoint = vtset.results{tree}.detects(1).timepoint;
%         vtset.results{tree}.quants(x).absoluteTime = vtset.results{tree}.detects(1).absoluteTime;
%         vtset.results{tree}.quants(x).positionIndex = vtset.results{tree}.detects(1).positionIndex;
%         vtset.results{tree}.quants(x).filename = vtset.results{tree}.detects(1).filename;
%         vtset.results{tree}.quants(x).detectchannel = zeros(1,numel(vtset.results{tree}.detects(1).cellNr))+vtset.results{tree}.quants(x).settings.detectchannel;
%         vtset.results{tree}.quants(x).active = zeros(1,numel(vtset.results{tree}.detects(1).cellNr));
%     end
% end