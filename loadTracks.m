% function r=loadTracks(trackfile,imgroot,exp,ext)
% loads a tracking file and assigns image files
%
% TODO:
% - better interpolation of cell divisions
function full=loadTracks(tree,imgroot,exp,ext)

% read the tracks
f = fields(tree);
clear full
for i=1:numel(f)
    full.(f{i}) = [];
end

% insert the missing ones
for i=1:numel(tree.X)
    if i<numel(tree.X) && tree.cellNr(i+1) == tree.cellNr(i)
        % iterate over one cell
        for t=tree.timepoint(i):tree.timepoint(i+1)-1
            % copy values
            ind = numel(full.X)+1;
            for j=1:numel(f)
                full.(f{j})(ind,1) = tree.(f{j})(i);
            end
            % set timepoints
            full.timepoint(ind,1) = t;
        end
    end
end


startfound=0;
endfound=0;
startind=[];
endind=[];
% set absolute time
for i=2:numel(full.X)
    
    if full.cellNr(i)==full.cellNr(i-1) && full.absoluteTime(i) == full.absoluteTime(i-1)
        if startfound
            % go on
        else
            startfound=1;
            startind=i;
            % check first timepoint
            if i==2 && full.cellNr(i)==full.cellNr(i-1)
                startind=1;
            end
                
        end
    else 
        if startfound
            endfound=1;
            endind=i;
%         else
            % go on
        end
    end
    if startfound && endfound
        %create linspace
        full.absoluteTime(startind:endind)=linspace(full.absoluteTime(startind), full.absoluteTime(endind),numel(startind:endind));
        startfound=0;
        endfound=0;
        startind=[];
        endind=[];      
    end
end
    


% collect files
full.filename = cell(numel(full.X),1);

%%% new idea: get all files and enter them in struct instead of checking if
%%% there exists a file for every tp 
%%%
% get all directories of existing positions
% for pos=unique(full.positionIndex)'
%     files=getFilesExt((sprintf('%s/%s_p%03d/', imgroot, exp, pos)),ext);
%     
%     for tpindex=find(full.positionIndex==pos)'
%         % found file?
%         if ismember(sprintf('%s_p%03d_t%05d_%s', exp, pos, full.timepoint(tpindex), ext),files)
%             full.filename{tpindex}=sprintf('%s/%s_p%03d/%s_p%03d_t%05d_%s', imgroot, exp, pos, exp, pos, full.timepoint(tpindex), ext);
%         end
%         
%     end
% end
    
%%%%%%% GET IT FASTER !!!!1111einself %%%%%

% iterate over each entryful
% 
for i=1:numel(full.X)
    pos = full.positionIndex(i);
    timepoint = full.timepoint(i);
    % generate filename
    fname = sprintf('%s/%s_p%03d/%s_p%03d_t%05d_%s', imgroot, exp, pos, exp, pos, timepoint, ext);
    % check if it exists
    if exist(fname,'file') ~= 0
        % store info
        fname=strrep(fname,imgroot,'');
        full.filename{i} = fname;
    else
        full.filename{i} = '';
    end
    
end

% OLD CODE:
% %% load tracking file (ttt_simple)
% 
% fpath = '/home/jan/scratch/adam/checkpics';
% fexp = '090415AF3';
% fext = 'w1.jpg';
% fout = '/home/jan/scratch/adam/checkpics/verify';
% 
% 
% file = '/home/jan/scratch/adam/trees/090415AF3_p008-010AF.ttt_simple';
% raw = ezread(file,';');
% f = fields(raw);
% clear full
% for i=1:numel(f)
%     full.(f{i}) = [];
% end
% %full = {};
% %full.cellNr=[]; full.timepoint = []; full.positionIndex=[];
% %full.X=[]; full.Y=[]; full.backgroundX=[]; full.backgroundY=[];
% % insert the missing ones
% 
% for i=1:numel(tree.X)
%     if i<numel(tree.X) && tree.cellNr(i+1) == tree.cellNr(i)
%         for t=tree.timepoint(i):tree.timepoint(i+1)-1
%             % copy values
%             ind = numel(full.X)+1;
%             for j=1:numel(f)
%                 full.(f{j})(ind,1) = tree.(f{j})(i);
%             end
%             % set timepoints
%             full.timepoint(ind,1) = t;
%         end
%     end
% end
% 
% 
% %%
% fullnew={};
% cell2 = find(full.cellNr==2);
% for i=1:numel(f)
%     fullnew.(f{i}) = full.(f{i}) (cell2);
% end
% full=fullnew;
% 
% %% collect files
% % iterate over each entryful
% full.filename = cell(numel(full.X),1);
% for i=1:numel(full.X)
%     pos = full.positionIndex(i);
%     timepoint = full.timepoint(i);
%     % generate filename
%     fname = sprintf('%s/%s_p%03d/%s_p%03d_t%05d_%s', fpath, fexp, pos, fexp, pos, timepoint, fext);
%   %  fprintf('%s\n', fname);
%     % check if it exists
%     if exist(fname,'file') ~= 0
%         % store info
%         full.filename{i} = fname;        
%     else
%         full.filename{i} = '';
%     end
%     fprintf('%d\n', i);
% end
