global siq 

ext='*w01.png';

mypath =  '/storage/cmbstore/TTT/130103MEZ9/';
% mypath='F:\121102AF5'
cd (mypath);
d = dir(mypath);
for dx = 3:numel(d)
    if ~d(dx).isdir
        continue
    end
    
    cd (d(dx).name)
    
    siq.pathname = [pwd '/'];
    d2 = dir(ext);
    if isempty(d2)
        continue
    end
    
    siq.filename ={d2.name};

%
% opti.batch=0;
% quant different channel?
% list all common filenames?
% a = questdlg('Do you want to quantify also different channels?','Quantify more than one channel?');

% extraChan = {};
% if strcmp(a(1),'Y')
%     extraChan = inputdlg('Enter common filename extension (i.e. w2.tif, ch02.tif, etc.)','Common file names');
% else
    extraChan = {};
% end
% % extraChan = {'ch01.tif','ch03.tif'};

% ask for segmentation params
% if ~isfield(siq,'mseroptions')
    siq.mseroptions.maxVariation = 1;
    siq.mseroptions.minSize = 15;
    siq.mseroptions.maxSize = 300;
    siq.mseroptions.delta = 1;
    siq.mseroptions.invert=0;
% end

mseroptions = siq.mseroptions;

% if ~numel(mseroptions)
%     opti.batch = 0;
%     return
% end
% siq.mseroptions = mseroptions;


%%%%  collect backgrounds
% backgrounds = questdlg('Do you need backgrounds?');
backgrounds = 'Yes';

thisIsAMovie =1;



% if strcmp(backgrounds(1),'Y')
%     % first see if there is gain->assume every background should be there
%     wl = regexp(siq.filename{siq.fid}, '(_w)(\d{1,2})', 'match');
%     if ~isempty(wl)
% %         wl = str2double(wl{1}(3:end));
%         gainsuggest = getOptiSuggest(siq.pathname,['gain_' wl{1}(2:end) '.png']);
%         if exist(gainsuggest,'file')
%             thisIsAMovie =1;
%         else
%             thisIsAMovie =0;
%         end
%     end
%     q=questdlg('Do you want to check them manually?');
%     
%     if strcmp(q(1) ,'C')
%         return
%     elseif strcmp(q(1),'N')
%         opti.batch = 1;
%     else
%         opti.batch = 0;
%     end
%     % go through detect images, take stored images if there
%     for fid=1:numel(siq.filename)
% %         if thisIsAMovie
% %             break
% %         end
%         siq.fid=fid;
%         loadsiqimage(handles,0);
%         opti.suggest = getOptiSuggest(siq.pathname,siq.filename{siq.fid});%[siq.pathname siq.filename{siq.fid}(1:end-4) '_background.png'];
%         % run optimzer store the background and set it
%         siq.bg=optimizeBackground(siq.img,[],siq.min,siq.max);
%         if numel(siq.bg )== 1
%             siq.bg=0;
%         end
%         % go through all other quant images
%         for fx = 1:numel(extraChan)
%             newfilename = [siq.filename{siq.fid}(1:end-numel(extraChan{fx})) extraChan{fx}];
%             opti.suggest = getOptiSuggest(siq.pathname,newfilename);%[siq.pathname newfilename(1:end-4) '_background.png'];
%             tempimg=loadimage(strcat(siq.pathname,'/',newfilename),0);
%             tempmin=max(0,min(tempimg(:)));
%             tempmax=min(1,topmedian(tempimg(:),50));
%             optimizeBackground(tempimg,[],tempmin,tempmax);
%         end
%     
%     end
% end
siq.fid=1;





% wh = waitbar(0,'Batch processing....');

% go for it
for fid = 1:numel(siq.filename)
    % load img and background
    siq.fid=fid;
%     waitbar(fid/numel(siq.filename),wh,'Loading image...');
siq.img=loadimage(strcat(siq.pathname,'/',siq.filename{siq.fid}),0);
  
    % load background
%     optisuggest=getOptiSuggest(siq.pathname, siq.filename{siq.fid});
%     if strcmp(backgrounds(1),'Y') && exist(optisuggest,'file');
%         siq.bg=loadimage(optisuggest,0);
%     else
%         siq.bg=0;
%     end
    if thisIsAMovie
        wl = regexp(siq.filename{siq.fid}, '(_w)(\d{1,2})', 'match');
        [img, bg]  = loadimage([siq.pathname siq.filename{siq.fid}],1,[wl{1}(2:end) '.png']);
        siq.normSubtracted = siq.img -bg;
        siq.normDivided = img;
        siq.bg = bg;
        siq.min = min(siq.normDivided(:));
        siq.max= topmedian(siq.normDivided(:),50);
%     else
%         [siq.normSubtracted,siq.normDivided,siq.min,siq.max]=normalizesiqimg(siq.img,siq.bg);
    end
    % segment it
%     waitbar(fid/numel(siq.filename),wh,'Quantifying image...');
    [mserBW,mserProps] = mserSegmentation(siq.normDivided,mseroptions);
    siq.autodetect = mserBW;
    siq.tracking_coords = mserProps;
    
    % quant it
siq.int = zeros(1,size(siq.tracking_coords,1));
siq.Center = cell(1,size(siq.tracking_coords,1));
siq.normint = zeros(1,size(siq.tracking_coords,1));
siq.size = zeros(1,size(siq.tracking_coords,1));
siq.bgint = zeros(1,size(siq.tracking_coords,1));
siq.s2bg = zeros(1,size(siq.tracking_coords,1));
siq.perimeter = zeros(1,size(siq.tracking_coords,1));
siq.eccentricity = zeros(1,size(siq.tracking_coords,1));
siq.majorAxis = zeros(1,size(siq.tracking_coords,1));
siq.minorAxis = zeros(1,size(siq.tracking_coords,1));
siq.heterogeneity = zeros(1,size(siq.tracking_coords,1));

normSubtracted = siq.normSubtracted;
normDivided=siq.normDivided;
% use auto detection but ONLY of region!!
props=regionprops( bwconncomp(siq.autodetect,4),'PixelIdxList','centroid','perimeter','MinorAxisLength','MajorAxisLength','Eccentricity');
% validIDs = sub2ind(size(siq.autodetect),round(siq.tracking_coords(:,2)),round(siq.tracking_coords(:,1)));
counter = 0;
% iterate over coords
for c = 1:numel(props)
    if ~ismember(props(c).Centroid,siq.tracking_coords,  'rows')
        continue
    end
    counter = counter+1;
    % quantify
    siq.int(counter) = sum(normSubtracted(props(c).PixelIdxList));
    siq.normint(counter) = sum(normDivided(props(c).PixelIdxList));
    siq.size(counter) = numel(props(c).PixelIdxList);
    siq.perimeter(counter) =props(c).Perimeter;
    siq.eccentricity(counter) = props(c).Eccentricity;
    siq.majorAxis(counter) = props(c).MajorAxisLength;
    siq.minorAxis(counter) = props(c).MinorAxisLength;
    siq.heterogeneity(counter) = std(normSubtracted(props(c).PixelIdxList));
    % s2b
    if numel(bg)==1
        siq.bgint(counter)=NaN;
        siq.s2bg(counter)=NaN;
    else
        %             subimg = extractSubImage(siq.bg, x, y, trackset.win);
        siq.bgint(counter) = sum(bg(props(c).PixelIdxList));
        %             subimg = extractSubImage(siq.img, x, y, trackset.win);
        siq.s2bg(counter) =  sum(img(props(c).PixelIdxList)) / siq.bgint(counter);
    end
end    
    % store csv / fcs
%     waitbar(fid/numel(siq.filename),wh,'Saving ...');
FileName = [siq.filename{fid}(1:end-4) '.csv'];
% if ~force
%     [FileName,PathName] = uiputfile('*.csv','Enter filename to export csv-file',[PathName FileName]);
%     %prevent overwriting
%     if FileName == 0
%         return;
%     end
% end
res.x= siq.tracking_coords(:,1);
res.y= siq.tracking_coords(:,2);
res.intensity = siq.int;
res.normalizedIntensity = siq.normint;
res.size = siq.size;
res.backgroundIntensity = siq.bgint;
res.signalToBackground = siq.s2bg;
res.MeanBackgroundSignal = repmat(mean(bg(:)),1,numel(res.intensity));
res.perimenter= siq.perimeter;
res.eccentricity= siq.eccentricity;
res.majorAxis= siq.majorAxis;
res.minorAxis= siq.minorAxis;
res.heterogeneity= siq.heterogeneity;
res.backgroundIntensity = zeros(1,size(siq.tracking_coords,1));
res.signalToBackground = zeros(1,size(siq.tracking_coords,1));
res.MeanBackgroundSignal = repmat(mean(bg(:)),1,numel(res.intensity));
pause(0.1)

if isempty(res.x)
%     set(siq.statushandle,'String','Nothing to save. Zero objects.')
    continue
end

try
ezwrite([siq.pathname FileName],res,'\t')
if ispc
    % excel write
    towrite = cell(numel(res.size)+1,numel(fieldnames(res)));
    ccounter=0;
    for f = fieldnames(res)'
        ccounter=ccounter+1;
        towrite{1,ccounter}=cell2mat(f);
        towrite(2:end,ccounter) = num2cell(res.(cell2mat(f)));
    end
    try 
    xlswrite([siq.pathname FileName(1:end-3) 'xls'],towrite)
    catch e
        'xls not possible'
    end
end
catch e
    rethrow(e)
end
% set(siq.statushandle,'String','Done.')    
    % quant other image(s)
%     waitbar(fid/numel(siq.filename),wh,'Quantifying additional image(s)...');
%     for fx = 1:numel(extraChan)
%         newfilename = [siq.filename{siq.fid}(1:end-numel(extraChan{fx})) extraChan{fx}];
%         
%         img=loadimage(strcat(siq.pathname,'/',newfilename),0);
%         
%         % load bg and normalize
%         optisuggest=getOptiSuggest(siq.pathname, newfilename);
%         if strcmp(backgrounds(1),'Y') && exist(optisuggest,'file');
%             bg=loadimage(optisuggest,0);
%         else
%             bg=0;
%         end
%         
%         [normSubtracted,normDivided] =  normalizesiqimg(img,bg);
%         
%         % quant
%         siqautoquant(bg,img,normDivided,normSubtracted)
% 
%         
%         % store csv / fcs
%         exportstuff(siq.pathname,newfilename,bg,1)
%         
%     end
    
    

end

% delete(wh);
% opti.batch=0;
    
    cd('../')
end