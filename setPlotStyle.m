function varargout = setPlotStyle(varargin)
% SETPLOTSTYLE M-file for setPlotStyle.fig
%      SETPLOTSTYLE, by itself, creates a new SETPLOTSTYLE or raises the existing
%      singleton*.
%
%      H = SETPLOTSTYLE returns the handle to a new SETPLOTSTYLE or the handle to
%      the existing singleton*.
%
%      SETPLOTSTYLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETPLOTSTYLE.M with the given input arguments.
%
%      SETPLOTSTYLE('Property','Value',...) creates a new SETPLOTSTYLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setPlotStyle_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setPlotStyle_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setPlotStyle

% Last Modified by GUIDE v2.5 25-Nov-2011 16:49:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setPlotStyle_OpeningFcn, ...
                   'gui_OutputFcn',  @setPlotStyle_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before setPlotStyle is made visible.
function setPlotStyle_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setPlotStyle (see VARARGIN)
global colors
% Choose default command line output for setPlotStyle
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

if numel(varargin)==1
    colors = varargin{1};
    %%%%% set all params
    setparams(colors,handles);
else 
    colors.cell={};
    colors.color={};
    colors.linestyle={};
    colors.linewidth={};
    colors.markerstyle={};
end
% UIWAIT makes setPlotStyle wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- read params and set into listbox and plot styles
function setparams(colors,handles)

return
%%%%%% update listbox
% listString=cell(numel(colors.cell),1);
% for x = 1:numel(colors.cell)
%     if ischar(colors.cell{x})
%         listString{x}= sprintf('Cells: %s Color: %s, Style: %s, Width: %d, MarkerStyle %s', colors.cell{x}, colors.color{x}, colors.linestyle{x},colors.linewidth{x}, colors.markerstyle{x});
%     else
%         listString{x}= sprintf('Cells: %s Color: %s, Style: %s, Width: %d, MarkerStyle %s', sprintf('%d,',colors.cell{x}), colors.color{x}, colors.linestyle{x},colors.linewidth{x}, colors.markerstyle{x});
%     end
% end
% set(handles.listboxStyles,'String',listString)

%%% update plot styles
set(handles.checkboxGrid,'Value',colors.grid)


%%% map legend position
set(handles.popupmenuLegend,'Value',find(strcmp(colors.legend,get(handles.popupmenuLegend,'String'))))

%%% BGCOLOR

%%% labels/title
set(handles.xAxisLabel, 'String',colors.xlabel)
set(handles.yAxisLabel, 'String',colors.ylabel)
set(handles.title, 'String',colors.title)

% x/y axis ranges
set(handles.xAxisScale, 'String',num2str(colors.xaxisrange))
set(handles.yAxisScale, 'String',num2str(colors.yaxisrange))

% tree style
set(handles.checkboxDynamicLineWidth,'Value',colors.dynamicLineWidth);
set(handles.editTreeLineWidth,'String',num2str(colors.treelinewidth));

% --- Outputs from this function are returned to the command line.
function varargout = setPlotStyle_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global confirmed colors

if ~confirmed
% colors.cell = [];
% colors.color= [];
end


% Get default command line output from handles structure
varargout{1} = colors;



function cellsBox_Callback(hObject, eventdata, handles)
% hObject    handle to cellsBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cellsBox as text
%        str2double(get(hObject,'String')) returns contents of cellsBox as a double


% --- Executes during object creation, after setting all properties.
function cellsBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellsBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderLineWidth_Callback(hObject, eventdata, handles)
% hObject    handle to sliderLineWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

width=round(get(handles.sliderLineWidth,'Value'));

set(handles.sliderLineWidth,'Value',width);

set(handles.BoxLineWidth,'String',num2str(width));




% --- Executes during object creation, after setting all properties.
function sliderLineWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderLineWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function BoxLineWidth_Callback(hObject, eventdata, handles)
% hObject    handle to BoxLineWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of BoxLineWidth as text
%        str2double(get(hObject,'String')) returns contents of BoxLineWidth as a double


% --- Executes during object creation, after setting all properties.
function BoxLineWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to BoxLineWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listboxStyles.
function listboxStyles_Callback(hObject, eventdata, handles)
% hObject    handle to listboxStyles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listboxStyles contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxStyles


% --- Executes during object creation, after setting all properties.
function listboxStyles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxStyles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonOK.
function ButtonOK_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset confirmed colors


%%%% read plot styl params
legend=get(handles.popupmenuLegend,'String');
colors.legend = legend{get(handles.popupmenuLegend,'Value')};
bgcolor=get(handles.popupmenuBGColor,'String');
colors.bgcolor = bgcolor{get(handles.popupmenuBGColor,'Value')};
colors.grid = get(handles.checkboxGrid,'Value');

scale = (get(handles.xAxisScale,'String'));
colors.xaxisrange = eval(['[' scale ']']);
colors.yaxisrange = eval(['[' (get(handles.yAxisScale,'String')) ']']);
colors.xlabel = get(handles.xAxisLabel,'String');
colors.ylabel = get(handles.yAxisLabel,'String');
colors.title = get(handles.title,'String');

colors.dynamicLineWidth = get(handles.checkboxDynamicLineWidth,'Value');
colors.treelinewidth = str2double(get(handles.editTreeLineWidth,'String'));

vtset.colors = colors;

confirmed=1;

delete(handles.figure1)

% --- Executes on button press in ButtonCancel.
function ButtonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global confirmed

confirmed  = 0;
delete(handles.figure1)

% --- Executes on button press in ButtonDeleteStyle.
function ButtonDeleteStyle_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDeleteStyle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global colors

delID = get(handles.listboxStyles,'Value');

list = get(handles.listboxStyles,'String');
if numel(list)>0
    list(delID)=[];

    set(handles.listboxStyles,'String',list);
    colors.cell(delID)=[];
    colors.color(delID)=[];
    colors.linestyle(delID)=[];
    colors.linewidth(delID)=[];

end


% --- Executes on button press in ButtonSet.
function ButtonSet_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global colors

% read all params and write into matlab struct & listbox

% read params

%%%%% cells
cells = get(handles.cellsBox,'String');
if strcmpi(cells,'all')
    colors.cell{end+1} = 'all';
else
    cells=strrep(cells, '-',':');
    cells=strrep(cells, ',',' ');
    cells=['[' cells ']' ];
    cells=eval(cells);
    colors.cell{end+1} = cells;
end

%%%%% lineWidth
colors.linewidth{end+1} = str2double(get(handles.BoxLineWidth,'String'));


%%%%% linestyle

if get(handles.radiobuttonSolid,'Value')
    ls='-';
elseif get(handles.radiobuttonDashed,'Value')
    ls='--';
elseif get(handles.radiobuttonDotted,'Value')
    ls='.';
elseif get(handles.radiobuttonDashDot,'Value')
    ls='-.';
else
    errordlg('No Line Style set !!??');
end
colors.linestyle{end+1} = ls;


%%%%% color
if get(handles.radiobuttonRed,'Value')
    cl='red';
elseif get(handles.radiobuttonBlue,'Value')
    cl='blue';
elseif get(handles.radiobuttonGreen,'Value')
    cl='green';
elseif get(handles.radiobuttonYellow,'Value')
    cl='yelloq';
elseif get(handles.radiobuttonBlack,'Value')
    cl='black';
elseif get(handles.radiobuttonCyan,'Value')
    cl='cyan';
elseif get(handles.radiobuttonMagenta,'Value')
    cl='magenta';
else
    errordlg('No Color Style set !!??');
end

colors.color{end+1} = cl;

%%%%% markerstyle
if get(handles.radiobuttonPlus,'Value')
    cl='+';
elseif get(handles.radiobuttonX,'Value')
    cl='x';
elseif get(handles.radiobuttonO,'Value')
    cl='o';
elseif get(handles.radiobuttonStar,'Value')
    cl='*';
elseif get(handles.radiobuttonDot,'Value')
    cl='.';
elseif get(handles.radiobuttonDiamond,'Value')
    cl='diamond';
elseif get(handles.radiobuttonOff,'Value')
    cl='off';
else
    errordlg('No Marker Style set !!??');
end

colors.markerstyle{end+1} = cl;


%%%%%% update listbox
if ischar(colors.cell{end})
    listString= sprintf('Cells: %s Color: %s, Style: %s, Width: %d, MarkerStyle %s', colors.cell{end}, colors.color{end}, colors.linestyle{end},colors.linewidth{end}, colors.markerstyle{end});
else
    listString= sprintf('Cells: %s Color: %s, Style: %s, Width: %d, MarkerStyle %s', sprintf('%d,',colors.cell{end}), colors.color{end}, colors.linestyle{end},colors.linewidth{end}, colors.markerstyle{end});
end


x=get(handles.listboxStyles,'String');
ls={};
if numel(x)==1
    ls(end+1) = x;
elseif numel(x)>1
    ls = x;
end
ls{end+1} = listString;

set(handles.listboxStyles,'String',ls)


% --- Executes on button press in ButtonReset.
function ButtonReset_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonReset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

vtset=rmfield(vtset,'colors');
vtset.updateCallback()
delete(handles.figure1);

% --- Executes on button press in ButtonRedraw.
function ButtonRedraw_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonRedraw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset colors
%%%% read plot styl params
legend=get(handles.popupmenuLegend,'String');
colors.legend = legend{get(handles.popupmenuLegend,'Value')};
bgcolor=get(handles.popupmenuBGColor,'String');
colors.bgcolor = bgcolor{get(handles.popupmenuBGColor,'Value')};
colors.grid = get(handles.checkboxGrid,'Value');

scale = (get(handles.xAxisScale,'String'));
colors.xaxisrange = eval(['[' scale ']']);
colors.yaxisrange = eval(['[' (get(handles.yAxisScale,'String')) ']']);
colors.xlabel = get(handles.xAxisLabel,'String');
colors.ylabel = get(handles.yAxisLabel,'String');
colors.title = get(handles.title,'String');

colors.dynamicLineWidth = get(handles.checkboxDynamicLineWidth,'Value');
colors.treelinewidth = str2double(get(handles.editTreeLineWidth,'String'));


vtset.colors = colors;

vtset.updateCallback()


% --- Executes on button press in checkboxGrid.
function checkboxGrid_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxGrid (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxGrid


% --- Executes on selection change in popupmenuBGColor.
function popupmenuBGColor_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuBGColor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenuBGColor contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuBGColor


% --- Executes during object creation, after setting all properties.
function popupmenuBGColor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuBGColor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuLegend.
function popupmenuLegend_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuLegend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenuLegend contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuLegend


% --- Executes during object creation, after setting all properties.
function popupmenuLegend_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuLegend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobuttonPlus.
function radiobuttonPlus_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonPlus


% --- Executes on button press in radiobuttonX.
function radiobuttonX_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonX


% --- Executes on button press in radiobuttonO.
function radiobuttonO_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonO (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonO


% --- Executes on button press in radiobuttonStar.
function radiobuttonStar_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonStar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonStar


% --- Executes on button press in radiobuttonDot.
function radiobuttonDot_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonDot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonDot


% --- Executes on button press in radiobuttonDiamond.
function radiobuttonDiamond_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonDiamond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonDiamond



function xAxisScale_Callback(hObject, eventdata, handles)
% hObject    handle to xAxisScale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xAxisScale as text
%        str2double(get(hObject,'String')) returns contents of xAxisScale as a double


% --- Executes during object creation, after setting all properties.
function xAxisScale_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xAxisScale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function yAxisScale_Callback(hObject, eventdata, handles)
% hObject    handle to yAxisScale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of yAxisScale as text
%        str2double(get(hObject,'String')) returns contents of yAxisScale as a double


% --- Executes during object creation, after setting all properties.
function yAxisScale_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yAxisScale (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function xAxisLabel_Callback(hObject, eventdata, handles)
% hObject    handle to xAxisLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xAxisLabel as text
%        str2double(get(hObject,'String')) returns contents of xAxisLabel as a double


% --- Executes during object creation, after setting all properties.
function xAxisLabel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xAxisLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function yAxisLabel_Callback(hObject, eventdata, handles)
% hObject    handle to yAxisLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of yAxisLabel as text
%        str2double(get(hObject,'String')) returns contents of yAxisLabel as a double


% --- Executes during object creation, after setting all properties.
function yAxisLabel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yAxisLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function title_Callback(hObject, eventdata, handles)
% hObject    handle to title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of title as text
%        str2double(get(hObject,'String')) returns contents of title as a double


% --- Executes during object creation, after setting all properties.
function title_CreateFcn(hObject, eventdata, handles)
% hObject    handle to title (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkboxDynamicLineWidth.
function checkboxDynamicLineWidth_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxDynamicLineWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxDynamicLineWidth



function editTreeLineWidth_Callback(hObject, eventdata, handles)
% hObject    handle to editTreeLineWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTreeLineWidth as text
%        str2double(get(hObject,'String')) returns contents of editTreeLineWidth as a double


% --- Executes during object creation, after setting all properties.
function editTreeLineWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTreeLineWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
