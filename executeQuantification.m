% gets settings and already parsed treelist
% several steps:
% will create results stucture
% read binary ttt file
% find all corresponding images
% start the detection of every channel
% quantify all channels
% give back to TrackingGUI
function executeQuantification(settings,treelist)
global vtset
wh = waitbar(0,'Initializing ...','CreateCancelBtn',...
    'setappdata(gcbf,''canceling'',1)');
    setappdata(wh,'canceling',0)
try
    
    
    if getappdata(wh,'canceling')
        % cancel code
        delelte(wh)
        return
    end
    
    
    % init for every selected tree
    for tree = 1:numel(unique(settings.selectedtrees))
        vtset.results{tree} = initializeTreeData(settings,treelist,settings.selectedtrees(tree));
%         vtset.results{tree}.settings.filesettings = settings.filesettings;
%%% moved into init tree data
    end
    
%     getappdata(wh,'canceling');
    % read ttt file & gather images 
    for tree =1:numel(vtset.results)
        if strcmp(vtset.results{tree}.settings.treefile(end-2:end),'ttt')
            % ttt
            waitbar(0,wh,'Reading ttt file...')
            xmlfilename = dir([vtset.movieFolder '*xml']);
            if isempty(xmlfilename)
                xmlfilename=[];
            else
                xmlfilename = xmlfilename(end).name;
            end
            res = tttParser([vtset.results{tree}.settings.treeroot '/' vtset.results{tree}.settings.treefile],[vtset.results{tree}.settings.imgroot xmlfilename]);
            if isempty(res)
                continue
            end
            % gather images
            waitbar(0,wh,'Scanning movie disk...')
            filereader(res,tree)
            % set timestamps
            dx = dir([vtset.results{tree}.settings.treeroot '/' vtset.results{tree}.settings.treefile]);
            vtset.results{tree}.settings.lastTTTchange = dx.datenum;
        else
            % csv
            %use filesettings
            waitbar(0,wh,'Reading csv file...')
            
            res = ezread([vtset.results{tree}.settings.treeroot '/' vtset.results{tree}.settings.treefile],';');
            
            %%% TODO check if needed fields are in the csv.
            % needed res fields: cellNr,timepoint,X,Y
            % optional: positionIndex,stopReason,absoluteTime
            % delimiter is ';'
            
            if isempty(res)
                continue
            end
            % gather images
            waitbar(0,wh,'Scanning movie disk...')
%             vtset.results{tree}.settings.imgArgument='tp';
%             vtset.results{tree}.settings.imgString='img_Brightfield_%04d';
%             vtset.results{tree}.settings.positionString='2-Pos_028_%03d'; % 2-Pos_028_001
            
            filereader_csv(res,tree)
            % set timestamps
            dx = dir([vtset.results{tree}.settings.treeroot '/' vtset.results{tree}.settings.treefile]);
            vtset.results{tree}.settings.lastTTTchange = dx.datenum;
        end
    end
%     getappdata(wh,'canceling')
%     if getappdata(wh,'canceling')
%         % cancel code
%         delelte(wh)
%         return
%     end
    
    % if mode is set to only first, middle, last 3 timepoints restrict
    % cells:
    if settings.onlyTimepoints
        setactive
    end

    %%% detect!
    waitbar(0,wh,'Starting detection process...')
    for tree = 1:numel(vtset.results)
        for i = 1:numel(vtset.results{tree}.detects)
            fprintf('Detecting channel %d of %d in tree %s\n',i,numel(vtset.results{tree}.detects),vtset.results{tree}.settings.treefile);
            waitbar(i/numel(vtset.results{tree}.detects),wh,sprintf('Detecting channel %d of %d',i,numel(vtset.results{tree}.detects)))
            detectCells(i,tree,vtset.results{tree}.settings.restrictcells,'complete')
        end
    end
%     getappdata(wh,'canceling')
    %%% quantify
    waitbar(0,wh,'Quantifying all channels...')
    for tree = 1:numel(vtset.results)
        for i = 1:numel(vtset.results{tree}.quants)
            fprintf('Quantifying channel %d of %d in tree %s\n',i,numel(vtset.results{tree}.detects),vtset.results{tree}.settings.treefile);
            waitbar(i/numel(vtset.results{tree}.quants),wh,sprintf('Quantifying channel %d of %d',i,numel(vtset.results{tree}.quants)))
            quantifyCells(i,tree,vtset.results{tree}.settings.restrictcells,'complete')
        end
    end
    
    %%% finishing
    waitbar(1,wh,'Finishing...')
    
    delete(wh)
catch e
    errordlg(sprintf('Please report error:\n%s',e.message))
    delete(wh)
    rethrow(e)
end


% function results = initializeResults(settings,treelist,tree)
% %%
% % only god and i know what this code is doing ...
% 
% % init the detects
% results.detects = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'X',{},'Y',{},'filename',{},'cellmask',{},'size',{},'trackset',{},'active',{});
% 
% % find the unique combination of detect/quant channel and thresh method
% detects=cell(1,numel({settings.channelsettings.detect}));
% quants=cell(1,numel({settings.channelsettings.quant}));
% for i=1:numel({settings.channelsettings.detect})
%     detects{1,i} = [ settings.channelsettings(i).detect '---' settings.channelsettings(i).settings.threshmethod];
%     detects{2,i} = num2str(i);
%     quants{i} = [settings.channelsettings(i).quant '...' settings.channelsettings(i).detect '---' settings.channelsettings(i).settings.threshmethod];
% end
% 
% unidetects = unique({detects{1,:}});
% 
% % set the channels
% for j = 1:numel(unidetects)
%     splitted = strsplit('---',unidetects{j});
%     results.detects(j).settings.wl = splitted{1};
%     % find correct threshmethod
%     results.detects(j).settings.trackset = settings.channelsettings(strcmp(unidetects{j},{detects{1,:}})).settings;
%     results.detects(j).settings.normalize = settings.corrDetect;
% end
% %%
% % init the quants
% results.quants = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'int',{},'active',{},'detectchannel',{});
% quants = unique(quants);
% for j = 1:numel(quants)
%     splitted = strsplit('...',quants{j});
%     results.quants(j).settings.wl = splitted{1};
%     % find the correct detection channel
%     results.quants(j).settings.detectchannel = find(strcmp(unidetects,splitted{2}));
%     results.quants(j).settings.normalize = settings.corrQuant;
% end
% 
% %%
% % finally the nonFluor
% results.nonFluor = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'active',{},'stopReason',{});
% results.nonFluor(1).settings.wl = settings.extNonFluor;
% 
% % and the settings
% results.settings.treeroot=treelist.root{(tree)};
% results.settings.treefile= treelist.treefile{(tree)};
% results.settings.imgroot=settings.imgroot;
% results.settings.exp=settings.exp;
% results.settings.channelsettings = settings.channelsettings;
% results.settings.anno=settings.anno;
% results.settings.anno2=settings.anno2;
% results.settings.restrictcells=settings.restrictcells;
% results.settings.corrDetect=settings.corrDetect;
% results.settings.corrQuant=settings.corrQuant;
% results.settings.moviestart=settings.moviestart;% timestamp of first image in pos001
% results.description='';


