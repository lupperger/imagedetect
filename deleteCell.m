%% delete all cells higher than a timepoint and belonging to on cell and all its daughters
global vtset
t = vtset.treenum;

tpfrom=1233;
cellfrom=57;

for r = [cellfrom getchildren(cellfrom,max(vtset.results{t}.nonFluor.cellNr))]
    for q = 1:numel(vtset.results{t}.quants)
        ids = vtset.results{t}.quants(q).cellNr == r & vtset.results{t}.quants(q).timepoint > tpfrom;
        
        for f = fieldnames(vtset.results{t}.quants(q))'
            if strcmp(f ,'settings')
            else
                f=cell2mat(f);
                vtset.results{t}.quants(q).(f)(ids)=[];
            end
        end
        
    end
    
    for q = 1:numel(vtset.results{t}.detects)
        ids = vtset.results{t}.detects(q).cellNr == r & vtset.results{t}.detects(q).timepoint > tpfrom;
        for f = fieldnames(vtset.results{t}.detects(q))'
            if strcmp(f ,'settings')
            else
                f=cell2mat(f);
                vtset.results{t}.detects(q).(f)(ids)=[];
            end
        end
        
    end
    for q = 1:numel(vtset.results{t}.nonFluor)
        ids = vtset.results{t}.nonFluor(q).cellNr == r & vtset.results{t}.nonFluor(q).timepoint > tpfrom;
        for f = fieldnames(vtset.results{t}.nonFluor(q))'
            if strcmp(f ,'settings') || ~numel(vtset.results{t}.nonFluor(q).(cell2mat(f)))
            else
                f=cell2mat(f);
                vtset.results{t}.nonFluor(q).(f)(ids)=[];
            end
        end
        
    end
    
end