function status = copyBackgrounds(expfolder)

destination = '/home/tttcomputer/250GB/gibtsnich';

positions = dir(expfolder);
positions = {positions([positions.isdir]).name};


if numel(positions)>2
    for i = 3:numel(positions)
        if isdir([expfolder positions{i} '/background'])
            copyfile([expfolder positions{i} '/background/*'], [destination]);
        else
            msgbox(sprintf('no background found in %s',[expfolder positions{i} '/background'))
        end

    end
else
    msgbox(sprintf('No positions found in given folder %s',expfolder));
    status = 0;
end