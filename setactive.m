function setactive
%% set only first middle and last three tps to active
global vtset




for tree = 1:numel(vtset.results)
    cells=vtset.results{tree}.settings.restrictcells;
    if strcmp(cells,'all')
        cells = unique(vtset.results{tree}.nonFluor.cellNr);
    end
    for channel = 1:numel(vtset.results{tree}.detects);
        for c = torow(cells)
            allids = find(vtset.results{tree}.detects(channel).cellNr == c);
            timepoints = vtset.results{tree}.detects(channel).timepoint(vtset.results{tree}.detects(channel).cellNr == c);
            [~, ids]=sort(timepoints);
            allids=allids(ids);
            if numel(allids)>9
                vtset.results{tree}.detects(channel).active(allids)=0;
                vtset.results{tree}.detects(channel).active(allids(1:3))=1;
                vtset.results{tree}.detects(channel).active(allids(end-2:end))=1;
                vtset.results{tree}.detects(channel).active(allids(round(end/2)-1:round(end/2)+1))=1;
            end
        end
    end
    
    
    for channel = 1:numel(vtset.results{tree}.quants);
        for c = torow(cells)
            allids = find(vtset.results{tree}.quants(channel).cellNr == c);
            timepoints = vtset.results{tree}.quants(channel).timepoint(vtset.results{tree}.quants(channel).cellNr == c);
            [~, ids]=sort(timepoints);
            allids=allids(ids);
            if numel(allids)>9
                vtset.results{tree}.quants(channel).active(allids)=0;
                vtset.results{tree}.quants(channel).active(allids(1:3))=1;
                vtset.results{tree}.quants(channel).active(allids(end-2:end))=1;
                vtset.results{tree}.quants(channel).active(allids(round(end/2)-1:round(end/2)+1))=1;
            end
        end
    end
end

function dummy
%% set complete cell active again:

tree = 3;
cells  = [24];

% set all quants active and remember them
quantids={};

for channel = 1:numel(vtset.results{tree}.quants);
    
    allids = ismember(vtset.results{tree}.quants(channel).cellNr,cells) & vtset.results{tree}.quants(channel).active==0;
%     notactive = vtset.results{tree}.quants(channel).active(allids)==0;
    quantids{channel}=allids;
    vtset.results{tree}.quants(channel).active(allids)=1;
end

% detect them
for channel = 1:numel(vtset.results{tree}.detects);
    allids = ismember(vtset.results{tree}.detects(channel).cellNr,cells) & vtset.results{tree}.detects(channel).active == 0;
%     notactive = vtset.results{tree}.detects(channel).active(allids)==0;
    vtset.results{tree}.detects(channel).active(allids)=1;
    % detect them
    detectCells(channel,tree,cells,vtset.results{tree}.detects(channel).timepoint(allids))
end

% quantify
for channel = 1:numel(vtset.results{tree}.quants);
    quantifyCells(channel,tree,cells,vtset.results{tree}.quants(channel).timepoint(quantids{channel}))
end


