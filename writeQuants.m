function writeQuants(quants, basepath, delimiter)

if nargin<3
    delimiter=';';
end

for i=1:numel(quants)
    % generate output filename
    [pathstr, name, ext, versn] = fileparts(quants{i}.treefile);
    filename = [basepath '/' name '.out'];
    % construct structure to write out
    out.int = quants{i}.int;
    out.mean = quants{i}.mean;
    out.size = quants{i}.size;
    out.cellNr = quants{i}.cellNr;
    out.timepoint = quants{i}.timepoint;
    out.X = quants{i}.X;
    out.Y = quants{i}.Y;
    % write it out
    ezwrite(filename, out, delimiter);
end


