% calculates the Threshold of given Image with given method (defined in
% trackset.threshmethod or given as string)
% possible methods:
% [Mode/Mean/Median/KSD/Otsu] [Global/Sub]

function Threshold=determineThreshold(Image, method)




method=strsplit_qtfy(' ',method);
method=method(1);

switch lower(cell2mat(method))
    case 'mode'
        Threshold=mode(Image(:));
    case 'mean'
        Threshold=mean(Image(:));
    case 'median'
        Threshold=median(Image(:));
    case 'ksd'
        [f,x]=ksdensity(Image(:));
        Threshold=x(find(f==max(f)));
    case 'otsu'
        Threshold=graythresh(Image);
    case 'ellipse'
        %%% HACK AROUND as well -.-
        Threshold=graythresh(Image);
    otherwise
        error('unknown threshold method');
end