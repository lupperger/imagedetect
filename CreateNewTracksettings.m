function varargout = CreateNewTracksettings(varargin)
% CREATENEWTRACKSETTINGS M-file for CreateNewTracksettings.fig
%      CREATENEWTRACKSETTINGS, by itself, creates a new CREATENEWTRACKSETTINGS or raises the existing
%      singleton*.
%
%      H = CREATENEWTRACKSETTINGS returns the handle to a new CREATENEWTRACKSETTINGS or the handle to
%      the existing singleton*.
%
%      CREATENEWTRACKSETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATENEWTRACKSETTINGS.M with the given input arguments.
%
%      CREATENEWTRACKSETTINGS('Property','Value',...) creates a new CREATENEWTRACKSETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CreateNewTracksettings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CreateNewTracksettings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CreateNewTracksettings

% Last Modified by GUIDE v2.5 12-Mar-2014 15:43:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CreateNewTracksettings_OpeningFcn, ...
                   'gui_OutputFcn',  @CreateNewTracksettings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CreateNewTracksettings is made visible.
function CreateNewTracksettings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CreateNewTracksettings (see VARARGIN)

% Choose default command line output for CreateNewTracksettings
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



%%% TODO: slider!!! ???welche?



    %initialize track settings
    maxSize = getMaxImageSize();
    set(handles.windowSizeSlider_otsu, 'Max', maxSize);
    set(handles.windowSize_otsu, 'String', num2str(50));
    set(handles.windowSize_otsu, 'enable', 'on')
    set(handles.thresholdCorrection_otsu, 'String', num2str(1.0));
    set(handles.smoothingFilter_otsu, 'String', num2str(4));
    set(handles.maximaSupression_otsu, 'String', num2str(4));
%     set(handles.fixThreshold_otsu, 'String', num2str(0));
%     set(handles.minimumThreshold_otsu, 'String', num2str(0));
    set(handles.distinguishClumpedList_otsu, 'Value', 2);
    set(handles.DividingLinesList_otsu, 'Value', 2);
    set(handles.useNearest, 'Value', 1);
%     set(handles.switchChannel, 'Value', 0);  
    set(handles.radiusSlider,'min',1,'max',50,'Value',15);
    set(handles.radiusDisplay, 'String', 15);
    set(handles.text5_otsu,'String','Threshold')
    set(handles.thresholdCorrectionSlider_otsu,'Max',2)
    set(handles.thresholdCorrectionSlider_otsu,'Min',0)
    set(handles.thresholdCorrectionSlider_otsu,'SliderStep',[0.1 0.2]/2)
    set(handles.thresholdCorrectionSlider_otsu,'Value',str2double(get(handles.thresholdCorrection_otsu,'String')))
    set(handles.text6_otsu,'String','Smoothing Filter')
    set(handles.smoothingFilterSlider_otsu,'Max',8)
    set(handles.smoothingFilterSlider_otsu,'Min',0)
    set(handles.smoothingFilterSlider_otsu,'SliderStep',[1 2]/8)
    set(handles.smoothingFilterSlider_otsu,'Value',str2double(get(handles.smoothingFilter_otsu,'String')))
    set(handles.text7_otsu,'String','Maxima suppression')
    set(handles.maximaSupressionSlider_otsu,'Max',20)
    set(handles.maximaSupressionSlider_otsu,'SliderStep',[1 2]/20)
    set(handles.maximaSupressionSlider_otsu,'Min',0)
    set(handles.maximaSupressionSlider_otsu,'Value',str2double(get(handles.maximaSupression_otsu,'String')))

    set(handles.sliderMaximalArea,'Max',str2double(get(handles.windowSize_otsu, 'String'))^2)
    set(handles.sliderMaximalArea,'Min',0)
    set(handles.sliderMaximalArea,'SliderStep',[10 50]/str2double(get(handles.windowSize_otsu, 'String'))^2)
    set(handles.sliderMaximalArea,'Value',500)
    set(handles.editMaximalArea,'String','500');
    set(handles.editMaximalArea, 'enable', 'on')
    set(handles.sliderMinimalArea,'Max',str2double(get(handles.windowSize_otsu,'String'))^2)
    set(handles.sliderMinimalArea,'Min',0)
    set(handles.sliderMinimalArea,'SliderStep',[10 50]/str2double(get(handles.windowSize_otsu,'String'))^2)
    set(handles.sliderMinimalArea,'Value',30)
    set(handles.editMinimalArea,'String','30');
    set(handles.editMinimalArea, 'enable', 'on')
if numel(varargin)>0 &&   islogical(varargin{1}) 
    % show only external segmentation
    set(handles.detectionMethod,'String','External Segmentation');
    set(handles.detectionMethod,'Value',1);
elseif numel(varargin)>0
    trackset=varargin{1};
    set(handles.windowSize_otsu, 'String', num2str(trackset.win));
    set(handles.windowSizeSlider_otsu, 'Value', (trackset.win));
    set(handles.thresholdCorrection_otsu, 'String', num2str(trackset.threshcorr));
    set(handles.thresholdCorrectionSlider_otsu, 'Value', trackset.threshcorr);
    set(handles.smoothingFilter_otsu, 'String', num2str(trackset.smooth));
    set(handles.maximaSupression_otsu, 'String', num2str(trackset.max));
    set(handles.distinguishClumpedList_otsu,'Value',find(strcmp( get(handles.distinguishClumpedList_otsu,'String'), trackset.clumped)));
    set(handles.DividingLinesList_otsu,'Value',find(strcmp( get(handles.DividingLinesList_otsu,'String'), trackset.dividing)));
    set(handles.detectionMethod,'Value',find(strcmp( get(handles.detectionMethod,'String'), trackset.threshmethod)));
    set(handles.sliderMinimalArea, 'Value', (trackset.MinArea));
    set(handles.sliderMaximalArea, 'Value', (trackset.MaxArea));
    set(handles.editMinimalArea, 'String', num2str(trackset.MinArea));
    set(handles.editMaximalArea, 'String', num2str(trackset.MaxArea));
%     set(handles.fixThreshold_otsu, 'String', num2str(trackset.threshfix));

%     set(handles.minimumThreshold_otsu, 'String', num2str(trackset.threshmin));
    set(handles.useNearest, 'Value', trackset.usenearest);
%     if isfield(trackset, 'detectionChannel')
%         set(handles.switchChannel, 'Value', trackset.detectionChannel);
%     else
%         set(handles.switchChannel, 'Value', 0);
%     end
%     set(handles.radiusSlider,'min',1,'max',50,'Value',25);

detectionMethod_Callback([], [], handles)
end


% UIWAIT makes CreateNewTracksettings wait for user response (see UIRESUME)
uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = CreateNewTracksettings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
global confirmed trackset

if confirmed

%     trackset.threshfix=get(handles.treeRoot,'String');
%     trackset.threshmin=get(handles.treeRoot,'String');
%     trackset.threshmethod=get(handles.treeRoot,'String');


    varargout{1}=trackset;
else
    varargout{1}=[]; 
end




% --- Executes on button press in ok_button.
function ok_button_Callback(hObject, eventdata, handles)
% hObject    handle to ok_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global confirmed trackset

confirmed=true;


trackset.win=str2double(get(handles.windowSize_otsu,'String'));
trackset.threshcorr=min(2,str2double(get(handles.thresholdCorrection_otsu,'String')));
trackset.smooth=min(str2double(get(handles.smoothingFilter_otsu,'String')),8);
trackset.max=min(str2double(get(handles.maximaSupression_otsu,'String')),20);
trackset.clumped = get(handles.distinguishClumpedList_otsu,'String');
trackset.clumped = trackset.clumped{get(handles.distinguishClumpedList_otsu,'Value')};
trackset.dividing =get(handles.DividingLinesList_otsu,'String');
trackset.dividing =trackset.dividing{get(handles.DividingLinesList_otsu,'Value')};
trackset.usenearest=get(handles.useNearest,'Value');
% trackset.invert=get(handles.invertImage,'Value');
trackset.threshmethod=get(handles.detectionMethod,'String');
if iscell(trackset.threshmethod)
    trackset.threshmethod =trackset.threshmethod{get(handles.detectionMethod,'Value')};
end
% trackset.detectionChannel=get(handles.switchChannel,'Value');
% mode = get(handles.detectionMethod,'String');
% mode = mode{get(handles.detectionMethod,'Value')};
trackset.ellipseradius = get(handles.radiusSlider,'Value');
trackset.Delta = round(str2double(get(handles.thresholdCorrection_otsu,'String')));
trackset.MinArea = str2double(get(handles.editMinimalArea,'String'));
trackset.MaxArea = str2double(get(handles.editMaximalArea,'String'));
trackset.freehand=[];
trackset.ellipse=[];

close(handles.figure1);



% --- Executes on button press in cancel_button.
function cancel_button_Callback(hObject, eventdata, handles)
% hObject    handle to cancel_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global confirmed

confirmed=false;
close(handles.figure1);

% --- Executes on selection change in detectionMethod.
function detectionMethod_Callback(hObject, eventdata, handles)
% hObject    handle to detectionMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns detectionMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from detectionMethod

mode = get(handles.detectionMethod,'String');
mode =mode{get(handles.detectionMethod,'Value')};

if strcmp(mode,'Ellipse')
    % change the whole thing to select radius
    for f = fields(handles)'
        if numel(strfind(cell2mat(f),'radius'))
            set(handles.(cell2mat(f)),'Visible','on')
            
        elseif numel(strfind(cell2mat(f),'_otsu'))
            set(handles.(cell2mat(f)),'Visible','off')
        end
    end
    
    % use nearest??
else
    % change everyhting back
    for f = fields(handles)'
        if numel(strfind(cell2mat(f),'radius'))
            set(handles.(cell2mat(f)),'Visible','off')
        elseif numel(strfind(cell2mat(f),'_otsu'))
            set(handles.(cell2mat(f)),'Visible','on')
        end
    end
end

if strcmp(mode,'MSER')
    set(handles.text5_otsu,'String','Delta')
    set(handles.thresholdCorrectionSlider_otsu,'Max',255)
    set(handles.thresholdCorrectionSlider_otsu,'Min',0)
    set(handles.thresholdCorrectionSlider_otsu,'SliderStep',[1 10]/255)
    set(handles.thresholdCorrectionSlider_otsu,'Value',40)
    set(handles.thresholdCorrection_otsu,'String','40')
%     set(handles.text6_otsu,'String','Minimal Area')
%     set(handles.smoothingFilterSlider_otsu,'Max',str2double(get(handles.windowSize_otsu,'String'))^2)
%     set(handles.smoothingFilterSlider_otsu,'Min',0)
%     set(handles.smoothingFilterSlider_otsu,'SliderStep',[10 50]/str2double(get(handles.windowSize_otsu,'String'))^2)
%     set(handles.smoothingFilterSlider_otsu,'Value',round(str2double(get(handles.smoothingFilter_otsu,'String'))))
%     set(handles.text7_otsu,'String','Maximal Area')
%     set(handles.maximaSupressionSlider_otsu,'Max',str2double(get(handles.windowSize_otsu,'String'))^2)
%     set(handles.maximaSupressionSlider_otsu,'SliderStep',[10 50]/str2double(get(handles.windowSize_otsu,'String'))^2)
%     set(handles.maximaSupressionSlider_otsu,'Min',0)
%     set(handles.maximaSupressionSlider_otsu,'Value',round(str2double(get(handles.maximaSupression_otsu,'String'))))
else
    set(handles.text5_otsu,'String','Threshold')
    set(handles.thresholdCorrectionSlider_otsu,'Max',2)
    set(handles.thresholdCorrectionSlider_otsu,'Min',0)
    set(handles.thresholdCorrectionSlider_otsu,'SliderStep',[0.1 0.2]/2)
    set(handles.thresholdCorrectionSlider_otsu,'Value',1)
    set(handles.thresholdCorrection_otsu,'String','1')
%     set(handles.text6_otsu,'String','Smoothing Filter')
%     set(handles.smoothingFilterSlider_otsu,'Max',8)
%     set(handles.smoothingFilterSlider_otsu,'Min',0)
%     set(handles.smoothingFilterSlider_otsu,'SliderStep',[1 2]/8)
%     set(handles.smoothingFilterSlider_otsu,'Value',min(str2double(get(handles.smoothingFilter_otsu,'String')),8))
%     set(handles.text7_otsu,'String','Maxima suppression')
%     set(handles.maximaSupressionSlider_otsu,'Max',8)
%     set(handles.maximaSupressionSlider_otsu,'SliderStep',[1 2]/8)
%     set(handles.maximaSupressionSlider_otsu,'Min',0)
%     set(handles.maximaSupressionSlider_otsu,'Value',min(str2double(get(handles.maximaSupression_otsu,'String')),8))
end

% --- Executes during object creation, after setting all properties.
function detectionMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to detectionMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in useNearest.
function useNearest_Callback(hObject, eventdata, handles)
% hObject    handle to useNearest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of useNearest


% --- Executes on selection change in distinguishClumpedList_otsu.
function distinguishClumpedList_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to distinguishClumpedList_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns distinguishClumpedList_otsu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from distinguishClumpedList_otsu


% --- Executes during object creation, after setting all properties.
function distinguishClumpedList_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to distinguishClumpedList_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in DividingLinesList_otsu.
function DividingLinesList_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to DividingLinesList_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns DividingLinesList_otsu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from DividingLinesList_otsu


% --- Executes during object creation, after setting all properties.
function DividingLinesList_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DividingLinesList_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function windowSize_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to windowSize_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of windowSize_otsu as text
%        str2double(get(hObject,'String')) returns contents of windowSize_otsu as a double
set(handles.windowSizeSlider_otsu, 'Value', str2double(get(hObject,'String')))
if(str2double(get(hObject,'String')) > get(handles.windowSizeSlider_otsu, 'Max'))
    set(handles.windowSizeSlider_otsu, 'Value', get(handles.windowSizeSlider_otsu, 'Max'))
    set(handles.windowSize_otsu, 'String', num2str(get(handles.windowSizeSlider_otsu, 'Max')))
end

if(get(handles.sliderMaximalArea,'Value') > round(get(handles.windowSizeSlider_otsu,'Value'))^2)
    set(handles.sliderMaximalArea,'Value',round(get(handles.windowSizeSlider_otsu,'Value'))^2)
    set(handles.editMaximalArea, 'String', num2str(round(get(handles.sliderMaximalArea,'Value'))));
end
set(handles.sliderMaximalArea,'Max',round(get(handles.windowSizeSlider_otsu,'Value'))^2)

% --- Executes during object creation, after setting all properties.
function windowSize_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to windowSize_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function windowSizeSlider_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to windowSizeSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

set(handles.windowSize_otsu, 'String', num2str(round(get(hObject,'Value'))));
if(get(handles.sliderMaximalArea,'Value') > round(get(hObject,'Value'))^2)
    set(handles.sliderMaximalArea,'Value',round(get(hObject,'Value'))^2)
    set(handles.editMaximalArea, 'String', num2str(round(get(handles.sliderMaximalArea,'Value'))));
end
set(handles.sliderMaximalArea,'Max',round(get(hObject,'Value'))^2)

% --- Executes during object creation, after setting all properties.
function windowSizeSlider_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to windowSizeSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function thresholdCorrection_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to thresholdCorrection_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of thresholdCorrection_otsu as text
%        str2double(get(hObject,'String')) returns contents of thresholdCorrection_otsu as a double


% --- Executes during object creation, after setting all properties.
function thresholdCorrection_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thresholdCorrection_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function thresholdCorrectionSlider_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to thresholdCorrectionSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

if get(handles.detectionMethod,'Value') == 3
    %mser mode only integers
    set(handles.thresholdCorrection_otsu, 'String', num2str(round(get(hObject,'Value'))));
else
    % everything else two digits
    set(handles.thresholdCorrection_otsu, 'String', num2str(round(100*get(hObject,'Value'))/100));
end

% --- Executes during object creation, after setting all properties.
function thresholdCorrectionSlider_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to thresholdCorrectionSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function smoothingFilter_otsu_Callback(~, eventdata, handles)
% hObject    handle to smoothingFilter_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of smoothingFilter_otsu as text
%        str2double(get(hObject,'String')) returns contents of smoothingFilter_otsu as a double


% --- Executes during object creation, after setting all properties.
function smoothingFilter_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to smoothingFilter_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function smoothingFilterSlider_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to smoothingFilterSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.smoothingFilter_otsu, 'String', num2str(round(get(hObject,'Value'))));

% --- Executes during object creation, after setting all properties.
function smoothingFilterSlider_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to smoothingFilterSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function maximaSupression_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to maximaSupression_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maximaSupression_otsu as text
%        str2double(get(hObject,'String')) returns contents of maximaSupression_otsu as a double


% --- Executes during object creation, after setting all properties.
function maximaSupression_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maximaSupression_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function maximaSupressionSlider_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to maximaSupressionSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.maximaSupression_otsu, 'String', num2str(round(get(hObject,'Value'))));

% --- Executes during object creation, after setting all properties.
function maximaSupressionSlider_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maximaSupressionSlider_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in invertImage.
function invertImage_Callback(hObject, eventdata, handles)
% hObject    handle to invertImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of invertImage



function fixThreshold_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to fixThreshold_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of fixThreshold_otsu as text
%        str2double(get(hObject,'String')) returns contents of fixThreshold_otsu as a double


% --- Executes during object creation, after setting all properties.
function fixThreshold_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fixThreshold_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minimumThreshold_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to minimumThreshold_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minimumThreshold_otsu as text
%        str2double(get(hObject,'String')) returns contents of minimumThreshold_otsu as a double


% --- Executes during object creation, after setting all properties.
function minimumThreshold_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minimumThreshold_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in switchChannel.
function switchChannel_Callback(hObject, eventdata, handles)
% hObject    handle to switchChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of switchChannel


% --- Executes on slider movement.
function radiusSlider_Callback(hObject, eventdata, handles)
% hObject    handle to radiusSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiusDisplay, 'String', num2str(round(get(hObject,'Value'))));

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function radiusSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radiusSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function radiusDisplay_Callback(hObject, eventdata, handles)
% hObject    handle to radiusDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of radiusDisplay as text
%        str2double(get(hObject,'String')) returns contents of radiusDisplay as a double


% --- Executes during object creation, after setting all properties.
function radiusDisplay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radiusDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderMaximalArea_Callback(hObject, eventdata, handles)
% hObject    handle to sliderMaximalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editMaximalArea, 'String', num2str(round(get(hObject,'Value'))));


% --- Executes during object creation, after setting all properties.
function sliderMaximalArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderMaximalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderMinimalArea_Callback(hObject, eventdata, handles)
% hObject    handle to sliderMinimalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editMinimalArea, 'String', num2str(round(get(hObject,'Value'))));


% --- Executes during object creation, after setting all properties.
function sliderMinimalArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderMinimalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editMinimalArea_Callback(hObject, eventdata, handles)
% hObject    handle to editMinimalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMinimalArea as text
%        str2double(get(hObject,'String')) returns contents of editMinimalArea as a double

set(handles.sliderMinimalArea, 'Value', str2double(get(hObject,'String')))
if(str2double(get(hObject,'String')) > get(handles.sliderMinimalArea, 'Max'))
    set(handles.sliderMinimalArea, 'Value', get(handles.sliderMinimalArea, 'Max'))
    set(handles.editMinimalArea, 'String',  num2str(get(handles.sliderMinimalArea, 'Max')))
end


% --- Executes during object creation, after setting all properties.
function editMinimalArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMinimalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMaximalArea_Callback(hObject, eventdata, handles)
% hObject    handle to editMaximalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMaximalArea as text
%        str2double(get(hObject,'String')) returns contents of editMaximalArea as a double
set(handles.sliderMaximalArea, 'Value', str2double(get(hObject,'String')))
if(str2double(get(hObject,'String')) > get(handles.sliderMaximalArea, 'Max'))
    set(handles.sliderMaximalArea, 'Value', get(handles.sliderMaximalArea, 'Max'))
    set(handles.editMaximalArea, 'String',  num2str(get(handles.sliderMaximalArea, 'Max')))
end

% --- Executes during object creation, after setting all properties.
function editMaximalArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMaximalArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function maxSize = getMaxImageSize()
    global vtset
    [~,file,~] = getAnImageFile(vtset.movieFolder, vtset.experiment);
    info = imfinfo(file);
    if(info.Width > info.Height)
        maxSize = info.Width;
    else
        maxSize = info.Height;
    end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to windowSize_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of windowSize_otsu as text
%        str2double(get(hObject,'String')) returns contents of windowSize_otsu as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to windowSize_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
