

figure;
hold on
counter= 0;
for c = unique(vtset.results.quants{1}.cellNr)'
    counter = counter+1;
    subplot(5,10,counter)
    plot(vtset.results.quants{1}.int(vtset.results.quants{1}.cellNr == c ))
    hold on
    plot(vtsetrecalc.results.quants{1}.int(vtsetrecalc.results.quants{1}.cellNr == c),'color','r')
end

%% compute distance

dist = [];

for c = unique(vtset.results.quants{1}.cellNr)'
    counter = counter+1;
    
    alldata1 = vtset.results.quants{1}.int(vtset.results.quants{1}.cellNr == c );
    alldata2 = vtsetrecalc.results.quants{1}.int(vtsetrecalc.results.quants{1}.cellNr == c);
    
    alldata1(alldata1==0) = [];
    alldata2(alldata2==0) = [];
    
    if alldata1
    alldata1 = alldata1 / mean(alldata1);
    alldata2 = alldata2 / mean(alldata2);
    
    dist(end+1) = mean(abs(alldata1-alldata2) ./ alldata2);
    end
end

%% plot vtset and vtsetrecalc of cyclo
figure
counter=0;
for t = 1:numel(vtset.results.quants)
    counter=counter+1;
    subplot(4,2,counter)
    plot(vtset.results.quants{t}.int(vtset.results.quants{t}.cellNr==1),'r')
    hold on
    plot(vtsetrecalc.results.quants{t}.int(vtsetrecalc.results.quants{t}.cellNr==1),'k')
end
    