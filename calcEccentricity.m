function res=calcEccentricity(results)


%iterate over each cell

for tree=1:numel(results.quants)
    if isfield(results.quants{tree}, 'cellNr')
        for cell=1:numel(results.quants{tree}.cellNr)
            if ismember(1,results.quants{tree}.cellmask{cell})
                t=regionprops(double(results.quants{tree}.cellmask{cell}), 'Eccentricity');

                results.quants{tree}.eccentricity(cell) = t.Eccentricity;
            else
                results.quants{tree}.eccentricity(cell) = -eps;
            end

        end
    end
end

res=results;