function res = trytoreadbinaryttt_imp()
    % read file
    % tttfile = '/nfs/cmbdata/schwarzfischer/100301PH4_p009-001MB.ttt';
    % tttfile = '/home/tttcomputer/TTTexport/TTTfiles/2010/100922PH3/100922PH3_p009/100922PH3_p009-013PH.ttt';
    % tttfile= '/home/tttcomputer/TTTexport/TTTfiles/2010/100811PH4/100811PH4_p043/100811PH4_p043-002PHRep.ttt';
    tttfile= '/home/ibis/anke.nissen/amt/ttt_samples/100301PH4_p009-001MB.ttt';
    % tttfile ='/home/tttcomputer/TTTexport/TTTfiles/2010/100226DL3/100226DL3_p036/100226DL3_p040-005DL.ttt';
    % tttfile='/home/tttcomputer/TTTexport/TTTfiles/2010/100205AF1/100205AF1_p068/100205AF1_p068-002AF.ttt';
    tic
    fid = fopen(tttfile);

    if fid == -1
        fprintf('File not found %s\n',tttfile);
        return;
    end

    % read xml file and get values for coord mapping
    xmlfile = fileread('/home/ibis/anke.nissen/amt/ttt_samples/TATexp.xml');
    facs = xml_reader(xmlfile);

    % calculate micrometer per pixel factor
    pixelfactor = pixel_mapping(facs);


    % - 4 bytes, int: version number
    version = fread(fid,1,'uint32');
    fprintf('Reading TTT file version %d...\n',version);

    if version<15
        errordlg('TTT file version < 15 --- not supported!\n');
        return
    end

    % - 4 bytes, int: last timepoint of the experiment
    lastTP = fread(fid,1,'uint32');

    % - 4 bytes, int: the number of tracks in the tree
    numberOfTracks = fread(fid,1,'uint32');

    % - 2 bytes, short int: maximum wavelength (-> mwl)
    if version >= 18
        maximumWL = fread(fid,1,'uint16');
    else 
        maximumWL = 5;
    end

    % - 2 bytes, short int: tree finished (0 = false, 1 = true)
    if version >=19
        treeFinished = fread(fid,1,'uint16');
    end

    % beware of the swap!
    if numberOfTracks>1000
        errordlg('more than 1000 tracks!??!')
        fclose(fid);
        return;
    end
    tracks = cell(numberOfTracks,1);
    for track = 1:numberOfTracks
        % - for each track of the tree:
        %   - 4 bytes, int: track number
        out = fread(fid,2,'uint32');
        tracks{track}.trackNo = out(1);
        %   - 4 bytes, int: starting timepoint
        tracks{track}.startTP = out(2);
        %   - 4 bytes, int: stopping timepoint (can be -1, indicating "not set")
        tracks{track}.stopTP = fread(fid,1,'int32');
        %   - 2 bytes, short int: stop reason (see enum TrackStopReason for values)
        tracks{track}.stopReason = fread(fid,1,'uint16');
        %   - 4 bytes, int: number of trackpoints that were set for this track (how many marks did the user set)
        tracks{track}.NumberOfTrackpoints = fread(fid,1,'uint32');

        % beware of the swap!
        if tracks{track}.NumberOfTrackpoints>5000
            errordlg('more than 5000 trackpoints!??!')
            fclose(fid);
            return;
        end

        trackpoints = cell(tracks{track}.NumberOfTrackpoints,1);
        for trackpoint = 1:tracks{track}.NumberOfTrackpoints
            %   - for each trackpoint of the current track:
            %     - 2 bytes, short int: timepoint of this trackpoint
            trackpoints{trackpoint}.timepoint = fread(fid,1,'uint16');
            %     - 4 bytes, float: X coordinate
            out = fread(fid,5,'float');
            trackpoints{trackpoint}.X = out(1);
            %     - 4 bytes, float: Y abscisse
            trackpoints{trackpoint}.Y =  out(2);
            %     - 4 bytes, float: Z coordinate (not used currently)
            trackpoints{trackpoint}.Z = out(3);
            %     - 4 bytes, float: X background coordinate (only if a background track was set)
            trackpoints{trackpoint}.backgroundX =  out(4);
            %     - 4 bytes, float: Y background coordinate (only if a background track was set)
            trackpoints{trackpoint}.backgroundY =  out(5);
            %     - 1 byte, char:  tissue type (see trackpoint.h/cellproperties.h)
            out = fread(fid,3,'uint8');
            trackpoints{trackpoint}.tissueType = out(1);
            %     - 1 byte, char:  general type (see trackpoint.h/cellproperties.h)
            trackpoints{trackpoint}.generalType = out(2);
            %     - 1 byte, char:  lineage (see trackpoint.h)
            trackpoints{trackpoint}.lineage =out(3);
            %     - 2 bytes, short int: nonadherent (boolean: 0 = false, >0 = true)
            out = fread(fid,2,'uint16');
            trackpoints{trackpoint}.nonadherent = out(1);
            %     - 2 bytes, short int: freefloating (boolean)
            trackpoints{trackpoint}.freefloating = out(2);
            %     - 4 bytes, float:  cell radius (default = 25)
            if version <= 15
                trackpoints{trackpoint}.cellRadius = fread(fid,1,'uint8');
            else
                trackpoints{trackpoint}.cellRadius = fread(fid,1,'single');
            end
            %     - 2 bytes, short int: Endomitosis (boolean)
            trackpoints{trackpoint}.endomitosis = fread(fid,1,'uint16');
            %     - mwl*2 bytes, each short int: wavelength (1-mwl) on (boolean)
            trackpoints{trackpoint}.wavelengths = ones(0,maximumWL);
            if version <= 17
    %             for wl = 1:maximumWL
                    trackpoints{trackpoint}.wavelengths(1:maximumWL) = fread(fid,maximumWL,'uint16');
    %             end
            else
    %             for wl = 1:maximumWL
                    trackpoints{trackpoint}.wavelengths(1:maximumWL) = fread(fid,maximumWL,'uint8');
    %             end
            end
            %     - 4 bytes, long: additional attribute, for various purposes 
            %%%% not shure but seems that since <v19 8byte??
            %%%% system dependend!?!?!
            if version <=18
                trackpoints{trackpoint}.additionalAttribute = fread(fid,1,'uint64');
            else
                trackpoints{trackpoint}.additionalAttribute = fread(fid,1,'uint32');
            end
            %     - 2 bytes, short int: positionn in which the trackpoint actually was set
            if version >= 17
                trackpoints{trackpoint}.position = fread(fid,1,'uint16');
            else
                trackpoints{trackpoint}.position = 0;
            end

            % get x & y offsets for the position
            offset_x = str2num(facs.offsetx{trackpoints{trackpoint}.position}.PosInfoDimension{1}.ATTRIBUTE.posX);
            offset_y = str2num(facs.offsety{trackpoints{trackpoint}.position}.PosInfoDimension{1}.ATTRIBUTE.posY);

            % calculate coords with pixelfactor and offsets
            trackpoints{trackpoint}.X = round((trackpoints{trackpoint}.X - offset_x) / pixelfactor);
            trackpoints{trackpoint}.Y = round((trackpoints{trackpoint}.Y - offset_y) / pixelfactor);
            
            res.cellNr(track) = 
            
        end

        tracks{track}.trackpoints=trackpoints;
    end
    
     






    posi = ftell(fid);
    fseek(fid,0,'eof');
    if posi == ftell(fid)
        fprintf('done with reading %s\n',tttfile);
    else
        fprintf('!!! file %s not fully read\n',tttfile);
    end
    fclose(fid);
    toc
end