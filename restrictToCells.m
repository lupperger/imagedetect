% s = a tree struct by loadTracks
% c = all cells which should be used in the detection
function r=restrictToCells(s,c)

f=fields(s);
for i=1:numel(f)
    r.(f{i}) = s.(f{i})(c);
end