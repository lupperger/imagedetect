function visualizeTree(cells,channel)
global vtset vzt
vzt.channel2= [];
% if numel(channel)>1
%     vzt.channel2= channel(2);
% end
vzt.channel= channel(1);

% data = vtset.results{vtset.treenum}.quants(channel);

vzt.max = quantile(vtset.results{vtset.treenum}.quants(vzt.channel).int,.95);
vzt.min = 0;% min(vtset.results{vtset.treenum}.quants(vzt.channel).int);

vzt.realtimescale=0;
vzt.realintensity=0;
vzt.compartment=0;
vzt.comp1 = 1;
vzt.comp2 = 5;
vzt.topmedian = 3;
vzt.bins=10;
vzt.gridcolor= 'white';
vzt.numbercolor='\color{white}';
% vzt.cells = cells;
% GET this from vtset.showonlycells
vzt.sortmode = 'sort upwards';
vzt.sortby = 1;
vzt.showcellnumbers= 1;
vzt.intmodify ='int';
vzt.bgcolor = 'white';
vzt.textBG = 'None';

if ~isfield(vtset,'htree22') || numel(vtset.htree2)==0
    vtset.htree2=figure;
    oldpos = get(vtset.htree2,'position');
    set(vtset.htree2,'position',[oldpos(1:2) 980 445]);

else
    figure(vtset.htree2)
end

set(gca,'color',[1 1 1])
vzt.map = hot;

vzt.redraw = @redraw;
redraw
initGUI


function redraw
global vzt vtset
title(gca(vtset.htree2),'redrawing')
drawnow
tic
vzt.hanno=[];
vzt.ax = get(vtset.htree2,'CurrentAxes');
cla(vzt.ax)
hold(vzt.ax,'on');
% cells = vzt.cells;
cells =intersect(vtset.showonlycells,vtset.results{vtset.treenum}.nonFluor.cellNr); 

actualgen = 1;
% draw mother
if ~vzt.realtimescale
    w = 1;
    x=0;
else
    time = vtset.results{vtset.treenum}.quants(vzt.channel).absoluteTime(vtset.results{vtset.treenum}.quants(vzt.channel).cellNr == 1)/3600;
    x=min(time);
    w = max(1,max(time)-x);
    
end
if ismember(1,cells)
    drawcell(x,-1,w,2,1)
end

vzt.cellsplotted = 1;
vzt.xend=x+w;
vzt.yend = -1;

% iterate through generations
% draw from top to bottom, sorted depending on the mother cell & position
while actualgen<=floor(log2(max(cells)))
    %iterate over cells in this generation
    %     ids = floor(log2((cells))) == actualgen;
    y = 1;
    %from top to bottom
    for position = 2^actualgen:(2^(actualgen+1)-1)
        % check which one is the mother cells
        mothercell=vzt.cellsplotted(floor((position)/2));
        
        % check which cell is higher if in even position
        cell = whichonetoplot(mothercell,position);
        vzt.cellsplotted(end+1) = cell;
        
        % get parameters
        % fixed generation timescale
        y = y - 2/(2^actualgen);
        
        if ismember(cell,cells)
            % get time scale
            if ~vzt.realtimescale
                w = 1;
                x = vzt.xend(floor((position)/2));
            else
                x = vzt.xend(floor((position)/2));
                time = vtset.results{vtset.treenum}.quants(vzt.channel).absoluteTime(vtset.results{vtset.treenum}.quants(vzt.channel).cellNr == cell)/3600;
%                 w = max(1,max(time)-min(time));
                %%%% need one more timepoint between last tp of mother to
                %%%% first of actual
                w = max(1,max(time)-x);
            end
            % should remeber x(end) of mother cell
            try
            vzt.xend(end+1) = x+w;
            catch e
                'muh'
            end
            vzt.yend(end+1) = y;

            % draw it
%             tic
            drawcell(x,y,w,2/2^actualgen,cell)
%             toc
        else
            %happens if cell is hidden (vtset.showonlycells)
            vzt.xend(end+1) = 1;
            vzt.yend(end+1) = y;
        end
        
        
    end
    % update
    actualgen = actualgen +1;
end
hold(vzt.ax,'off');
set(vzt.ax,'YTick',[])
% set(gca,'xlim',[-0.5 max(get(gca,'xlim'))])
% grid on
colorbar('peer',vzt.ax);

%set BG Color of the axis
if strcmp(vzt.bgcolor, 'white')
    set(vzt.ax, 'color', 'white');
elseif strcmp(vzt.bgcolor, 'black')
    set(vzt.ax, 'color', 'black');
else
end
 

% for two color heattree: create custom colormap
if numel(vzt.channel2)
    mymap = zeros(128,3);
    mymap(1:32,1) = linspace(0,1,32);
    mymap(33:64,3) = linspace(0,1,32);
    mymap(33:64,1) = 1;
    mymap(65:96,3) = 1;
    mymap(65:96,1) = linspace(1,0,32);
    mymap(97:end,3) = linspace(1,0,32);
    colormap(mymap);
    colorbar('ytick',[1 32 64 96 128]./128*vzt.max, 'YTickLabel',{'nothing','full gata','full both','full pu','nothing'},'peer',vzt.ax)
else
    colormap(vzt.ax,vzt.map)
end
set(vzt.ax, 'clim', [vzt.min vzt.max]);
title(vzt.ax,strrep(vtset.results{vtset.treenum}.settings.treefile,'_','\_'))

if vzt.realtimescale
    xlabel(vzt.ax,'[h]')
else
    xlabel(vzt.ax,'Generation')
end

% bring annotation change on top
idstotop=find(ismember(get(vzt.ax,'children'), vzt.hanno));
childs = get(vzt.ax,'children');
for id = 1:numel(idstotop)
    temp = childs(id);
    childs(id) = childs(idstotop(id));
    childs(idstotop(id)) = temp;
end
set(vzt.ax,'children',childs);

toc

function initGUI
global vtset vzt
vzt.hgui=[];
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 50 100 20],'style','checkbox','String','Show cell numbers','Value',1,'callback',@checkboxShowCellNumbers);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 175 100 20],'style','checkbox','String','Real time scale','callback',@checkboxRealTimeScale);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 150 100 20],'style','popupmenu','String',{'<Color map>','Jet','Hot','Gray','Spring','Summer','Autumn','Winter','HSV','Cool','Bone','Copper','Pink'},'callback',@popupColormap);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 325 100 20],'style','popupmenu','String',{'Real intensity','Gradient','Compartment'},'callback',@popupIntensityMode);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 275 100 20],'style','popupmenu','String',{'Sort upwards','Sort downwards','No sorting'},'callback',@popupSortMode);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 1 100 20],'style','pushbutton','String','Redraw','callback',@buttonredraw);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 25 100 20],'style','pushbutton','String','Screenshot','callback',@buttonCopyToClipboard);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 225 100 20],'style','text','String','Take median of:');
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[100 225 20 20],'style','edit','String','3','callback',@editTopMedian);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 350 100 20],'style','text','String','Compartment 1');
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[100 350 20 20],'style','edit','String','3','callback',@editcompartment1);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 375 100 20],'style','text','String','Compartment 2');
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[100 375 20 20],'style','edit','String','9','callback',@editCompartment2);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 125 100 20],'style','popupmenu','String',{'<GridColor>','White','Black','none'},'callback',@popupGridColor);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 250 100 20],'style','popupmenu','String',{'<Sort by>','First timepoints','Last timepoints'},'callback',@popupSortBy);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 200 100 20],'style','popupmenu','String',{'<Intensity mode>','Absolute','Intensity by area','Net production rate','Concentration','Cell area','Cumulative','Cumulative - ignoring deacreasing intensities','Linear regression','Doubling hypothesis'},'callback',@popupIntensityModify);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 100 100 20],'style','popupmenu','String',{'<BG Color>','White','Black'},'callback',@popupBGColor);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 75 100 20],'style','popupmenu','String',{'<Cell Text BG>','None','Black','White'},'callback',@popupCellTextBG);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 400 100 20],'style','text','String','Minimum Value');
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[100 400 20 20],'style','edit','String','0','callback',@editMinimun);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[1 425 100 20],'style','text','String','Maximum Value');
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[100 425 20 20],'style','edit','String',num2str(round(vzt.max*10)/10),'callback',@editMaximum);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[130 1 60 20],'style','pushbutton','String','Prev tree','Callback',@prevTree);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[200 1 60 20],'style','pushbutton','String','Next tree','callback',@nextTree);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[270 1 60 20],'style','pushbutton','String','X axis limits','callback',['global vzt; '...
                'oldx = get(vzt.ax,''xlim''); '...
                't=inputdlg(''Enter new x axis limits:'',''X axis'',1,{num2str(oldx)});'...
                'try set(vzt.ax,''xlim'',eval([''['' t{1} '']'']));catch e;end;'...
                '']);
vzt.hgui(end+1)=uicontrol(vtset.htree2,'position',[340 1 60 20],'style','pushbutton','String','Export all','callback',@ExportAll);

function channels=getChannelList
global vtset
channels={};
for i = 1:numel(vtset.results{vtset.treenum}.quants)
    channels{i} = ['Q ' vtset.results{vtset.treenum}.quants(i).settings.wl ' D ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(i).settings.detectchannel).settings.wl ' ' vtset.results{vtset.treenum}.detects(vtset.results{vtset.treenum}.quants(i).settings.detectchannel).settings.trackset.threshmethod];
end

function nextTree(muh,maeh)
global vtset  vzt
actualchannel = getChannelList;
actualchannel = actualchannel{vzt.channel};
vtset.treenum = min(numel(vtset.results),vtset.treenum+1);
newchannel = getChannelList;
found = 0;
for i = 1:numel(newchannel)
    if strcmp(actualchannel,newchannel{i})
        found = i;
    end
end
if found == 0
    [found,ok] = listdlg('Liststring',newchannel','PromptString','Please select a channel','listSize',[300 200]);
    if ok ==0
        vtset.treenum = max(1,vtset.treenum-1);
        return
    end
end
vzt.channel = found;

vtset.functions.initTreeList();
vtset.functions.initFigure();
vtset.updateCallback();
vtset.redrawtree();
redraw();

function prevTree(muh,maeh)
global vtset vzt
actualchannel = getChannelList;
actualchannel = actualchannel{vzt.channel};
vtset.treenum = max(1,vtset.treenum-1);
newchannel = getChannelList;
found = 0;
for i = 1:numel(newchannel)
    if strcmp(actualchannel,newchannel{i})
        found = i;
    end
end
if found == 0
    [found,ok] = listdlg('Liststring',newchannel','PromptString','Please select a channel','listSize',[300 200]);
    if ok ==0
        vtset.treenum = min(numel(vtset.results),vtset.treenum+1);
        return
    end
end
vzt.channel = found;
vtset.channels = newchannel;
vtset.functions.initTreeList();
vtset.functions.initFigure();
vtset.updateCallback();
vtset.redrawtree();
redraw();


function checkboxShowCellNumbers(hObject,eventdata)
global vzt
vzt.showcellnumbers =  get(hObject,'Value');
redraw

function editcompartment1(hObject,eventdata)
global vzt
vzt.comp1 =  str2double(get(hObject,'String'));
redraw

function editCompartment2(hObject,eventdata)
global vzt
vzt.comp2 =  str2double(get(hObject,'String'));
redraw

function editMinimun(hObject,eventdata)
global vzt
vzt.min=  str2double(get(hObject,'String'));
redraw

function editMaximum(hObject,eventdata)
global vzt
vzt.max =  str2double(get(hObject,'String'));
redraw

function buttonCopyToClipboard(hObject,eventdata)
global vzt vtset
% hide buttons
set(vzt.hgui,'visible','off')

% rearrange subplot
oldpos = get(vzt.ax,'position');
set(vzt.ax,'position',[0.01 0.1 0.92 0.85]);

% take screenshot
if ispc
    print(vtset.htree2, '-dmeta','-zbuffer');
else 
    msgbox('Only supported using windows')
end
% rearrange subplot
set(vzt.ax,'position',oldpos);

% show buttons
set(vzt.hgui,'visible','on')


function editTopMedian(hObject,eventdata)
global vzt
vzt.topmedian =  str2double(get(hObject,'String'));
redraw


function buttonredraw(hObject,eventdata)
redraw

function popupSortMode(hObject,eventdata)
global vzt
vzt.sortmode =  get(hObject,'String');
vzt.sortmode =  lower(vzt.sortmode{get(hObject,'Value')});
redraw

function checkboxRealTimeScale(hObject,eventdata)
global vzt
set(vzt.ax,'xlimMode','auto')
vzt.realtimescale =  get(hObject,'Value');
redraw

function popupColormap(hObject,eventdata)
global vzt
if get(hObject,'Value') == 1
    return
end
vzt.map =  get(hObject,'String');
vzt.map =  lower(vzt.map{get(hObject,'Value')});
redraw
    
function popupIntensityMode(hObject,eventdata)
global vzt
mode= get(hObject,'Value');

% {'Real intensity','Gradient','Compartment'}
if mode == 1
    vzt.realintensity = 1;
    vzt.compartment = 0 ;
elseif mode == 2
    vzt.realintensity = 0;
    vzt.compartment = 0 ;
elseif mode == 3
    vzt.realintensity = 0;
    vzt.compartment = 1;
end
redraw

function popupIntensityModify(hObject,eventdata)
global vzt
mode= get(hObject,'Value');

% {'<Intensity mode>','Absolute','Intensity by area','Net production rate','Concentration','Cell area','Cumulative','Cumulative - ignoring deacreasing intensities','Linear regression','Doubling hypothesis'}

if mode == 1
    return    
elseif mode == 2
    vzt.intmodify ='int';
elseif mode == 3
    vzt.intmodify ='mean';
elseif mode == 4
    vzt.intmodify ='prodrate';
elseif mode == 5
    vzt.intmodify ='concentration';
elseif mode == 6
    vzt.intmodify ='size';
elseif mode == 7
    vzt.intmodify ='cumulative';
elseif mode == 8
    vzt.intmodify ='cumulativeNN';
elseif mode == 9
    vzt.intmodify ='linearregression';
elseif mode == 10
    vzt.intmodify ='doublethes';
end
redraw

function popupSortBy(hObject,eventdata)
global vzt
mode= get(hObject,'Value');

% '<Sort by>','First timepoints','Last timepoints'
if mode == 1
    return    
elseif mode == 2
    vzt.sortby =1;
elseif mode == 3
    vzt.sortby = 0;
end
redraw

function popupGridColor(hObject,eventdata)
global vzt
mode= get(hObject,'Value');

% '<GridColor>','White','Black','none'
if mode == 1
    return
elseif mode == 2
    vzt.gridcolor = 'white';
elseif mode == 3
    vzt.gridcolor = 'black';
elseif mode == 4
    vzt.gridcolor = 'none';
end
redraw

function popupBGColor(hObject, eventdata)
global vzt;
mode = get(hObject, 'Value');
if mode == 1
    vzt.bgcolor = 'white';
elseif mode == 2
    vzt.bgcolor = 'white';
elseif mode == 3
    vzt.bgcolor = 'black';
end
redraw

function popupCellTextBG(hObject, eventdata)
global vzt;
mode = get(hObject, 'Value');
if mode == 1
    vzt.textBG = 'None';
elseif mode == 2
    vzt.textBG = 'None';
elseif mode == 3
     vzt.textBG = 'Black';   
elseif mode == 4
     vzt.textBG = 'White';  
end
redraw;

function drawcell(x,y,w,h,cell)
global vzt vtset
% get color check mode
[int, tps] = calculateTimeCourse(cell, vzt.channel, vzt.intmodify, false);

if vzt.realintensity
    time = sort(vtset.results{vtset.treenum}.quants(vzt.channel).absoluteTime(vtset.results{vtset.treenum}.quants(vzt.channel).cellNr == cell)/3600);
    cl=min(64,max(1,ceil(eps+(int-vzt.min)/(vzt.max-vzt.min)*64)));
%     cl2 = 0;
%     if numel(vzt.channel2)
%         int2 = vtset.results{vtset.treenum}.quants(vzt.channel2).int(vtset.results{vtset.treenum}.quants(vzt.channel2).cellNr == cell);
%         time2 = sort(vtset.results{vtset.treenum}.quants(vzt.channel2).absoluteTime(vtset.results{vtset.treenum}.quants(vzt.channel2).cellNr == cell)/3600);
%         loine=linspace(1,2^(2/3),numel(int2));
%         int2=torow(int2)./(loine.^(3/2));
%         cl2=min(64,max(1,ceil(eps+(int2-vzt.min)/(vzt.max-vzt.min)*64)));
%         
%     end
    if numel(time)>1
        cl = interp1(time,cl,linspace(min(time),max(time),vzt.bins));
%         cl2 = interp1(time2,cl2,linspace(min(time2),max(time2),vzt.bins));
    else
        cl = repmat(cl,1,vzt.bins);
%         cl2 = repmat(cl2,1,vzt.bins);
    end
    % get colormap
    mymap=colormap(vzt.map);
    
    % ad two color-colormap (black to green, black to red)
%     myclr = [1 0 0];
%     myclr2 = [0 1 0];
    
    % draw gradient
    for i = 1:vzt.bins
%         daclr(3) = round(cl(i))/64;
%         daclr(1) = round(cl2(i))/64;
%         daclr(2) = 0;
%         rectangle('position',[x+i*w/vzt.bins-w/vzt.bins y w/vzt.bins h],'facecolor',daclr,'LineStyle','none','parent',vzt.ax);
        rectangle('position',[x+i*w/vzt.bins-w/vzt.bins y w/vzt.bins h],'facecolor',mymap(round(cl(i)),:),'LineStyle','none');
    end
else
    first = medianfirst(int,vzt.topmedian);
    last = medianlast(int,vzt.topmedian);
    % compartment mode
    if vzt.compartment
        % check in which compartment is first
        dasum = sum(sign([vzt.comp1 vzt.comp2] - first));
        if dasum ==2
            first=vzt.min;
        elseif dasum == 0
            first = vzt.min+(vzt.max-vzt.min)/2;
        else
            first = vzt.max;
        end
        % and last int
        dasum = sum(sign([vzt.comp1 vzt.comp2] - last));
        if dasum ==2
            last=vzt.min;
        elseif dasum == 0
            last = vzt.min+(vzt.max-vzt.min)/2;
        else
            last = vzt.max;
        end
    end
    % then interp betwenn
    cl_first=first;%min(64,max(1,ceil(eps+(first-vzt.min)/(vzt.max-vzt.min)*64)));
    cl_last=last;%min(64,max(1,ceil(eps+(last-vzt.min)/(vzt.max-vzt.min)*64)));
%     cl = interp1([1 vzt.bins],[cl_first cl_last],1:vzt.bins);
    % hill or linear
    
    % patch with linear gradient
    p = patch([x x+w x+w x],[y y y+h y+h],'red','parent',vzt.ax);
    set(p,'FaceColor','interp','FaceVertexCData',[cl_first cl_last cl_last cl_first]','EdgeColor','none','LineWidth',1,'CDataMapping','scaled')
end

% grid around cell
[yo t]= annotationchange(vtset.results,vtset.treenum,1,cell,vtset.results{1}.settings.anno);
[yo2 t]= annotationchange(vtset.results,vtset.treenum,1,cell,vtset.results{1}.settings.anno2);
if yo
    vzt.hanno(end+1)=rectangle('position',[x y w h],'facecolor','none','EdgeColor','white','linewidth',3,'LineStyle','--','parent',vzt.ax);
%     line([t/3600 t/3600+w],[y y+h])
elseif yo2
    vzt.hanno(end+1)=rectangle('position',[x y w h],'facecolor','none','EdgeColor','black','linewidth',3,'LineStyle','--','parent',vzt.ax);
else
    rectangle('position',[x y w h],'facecolor','none','EdgeColor',vzt.gridcolor,'parent',vzt.ax);
end


% cell number
if vzt.showcellnumbers  
    if strcmp(vzt.textBG, 'White')
        text(x+0.4, y+h/2,['\color{Black}' sprintf('%d',cell)], 'BackgroundColor',vzt.textBG,'parent',vzt.ax);
    else
        text(x+0.4, y+h/2,[vzt.numbercolor sprintf('%d',cell)], 'BackgroundColor',vzt.textBG,'parent',vzt.ax);
    end
end

function cell=whichonetoplot(mother,position)
global vtset vzt
% get intensity

if strcmp(vzt.sortmode,'no sorting')
%     position
    if mod(position,2)
        cell = mother*2;
    else
        cell = mother*2+1;
    end
    return
end

%%% TODO check if prodrate, whatever
int1 = vtset.results{vtset.treenum}.quants(vzt.channel).int(vtset.results{vtset.treenum}.quants(vzt.channel).cellNr == mother*2);
int2 = vtset.results{vtset.treenum}.quants(vzt.channel).int(vtset.results{vtset.treenum}.quants(vzt.channel).cellNr == mother*2+1);

% get relevant timepoint (end or start)
% if only one cell exist, put it on top
if ~numel(int1)
    int1=-Inf;
end
if ~numel(int2)
    int2=-Inf;
end
    
if ~vzt.sortby
    int1 = medianlast(int1,vzt.topmedian);
    int2 = medianlast(int2,vzt.topmedian);
else
    int1 = medianfirst(int1,vzt.topmedian);
    int2 = medianfirst(int2,vzt.topmedian);
end
%%% TODO check if int1==int2 !!!!
if int1 == int2
    int2=int2+eps;
end
if mod(position,2)
    if strcmp(vzt.sortmode,'sort upwards')
        [m,cell] = min([int1 int2]);
        if int1 == int2
            cell = 1;
        end
    else
        [m,cell] = max([int1 int2]);
        if int1 == int2
            cell = 1;
        end
    end
    cell = mother*2+cell-1;
else
    if strcmp(vzt.sortmode,'sort upwards')
        [m,cell]=max([int1 int2]);
        if int1 == int2
            cell = 2;
        end
    else
        [m,cell]=min([int1 int2]);
        if int1 == int2
            cell = 2;
        end
    end
    cell = mother*2+cell-1;
end

function ExportAll(m,n)
global vtset vzt
PathName = uigetdir('','Select folder to export jpg-files');
if PathName==0
    return;
end
wh = waitbar(0,'Exporting jpg...');
temp = vtset.treenum;
tempcells = vtset.showonlycells;
otherhandles = guidata(vtset.hObject);
% xscale = get(vtset.haxes(1),'Xlim');
% yscale = get(vtset.haxes(1),'ylim');
for treenum=1:numel(vtset.results)
    vtset.treenum = treenum;
    if strcmpi(get(otherhandles.showOnlyCells,'String'),'all')
        vtset.showonlycells = unique(vtset.results{vtset.treenum}.quants(vzt.channel).cellNr);
    end
    redraw();
    % hide buttons
    set(vzt.hgui,'visible','off')
    
    % rearrange subplot
    oldpos = get(vzt.ax,'position');
    set(vzt.ax,'position',[0.01 0.1 0.92 0.85]);
    
    
    waitbar(treenum/numel(vtset.results),wh)
    filename = strsplit_qtfy('.',  vtset.results{vtset.treenum}.settings.treefile);
    FileName = filename{1};%[vtset.results{vtset.treenum}.settings.treefile(1:end-4) '.jpg'];
    %         for m = 1:numel(vtset.haxes)
    %             set(vtset.haxes(m),'Xlim',xscale)
    %             set(vtset.haxes(m),'ylim',yscale)
    %         end
    print(vtset.htree2,[PathName '/' FileName],'-djpeg')
    
    
    % rearrange subplot
    set(vzt.ax,'position',oldpos);
    
    % show buttons
    set(vzt.hgui,'visible','on')
end
vtset.treenum=temp;
vtset.showonlycells = tempcells;

redraw();
close(wh)


function dummy
%%

cells=unique(vtset.results{vtset.treenum}.quants(vzt.channel).cellNr);
visualizeTree(cells)



