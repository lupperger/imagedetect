function varargout = addDetectionChannel(varargin)
% ADDDETECTIONCHANNEL MATLAB code for addDetectionChannel.fig
%      ADDDETECTIONCHANNEL, by itself, creates a new ADDDETECTIONCHANNEL or raises the existing
%      singleton*.
%
%      H = ADDDETECTIONCHANNEL returns the handle to a new ADDDETECTIONCHANNEL or the handle to
%      the existing singleton*.
%
%      ADDDETECTIONCHANNEL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADDDETECTIONCHANNEL.M with the given input arguments.
%
%      ADDDETECTIONCHANNEL('Property','Value',...) creates a new ADDDETECTIONCHANNEL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before addDetectionChannel_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to addDetectionChannel_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help addDetectionChannel

% Last Modified by GUIDE v2.5 09-Apr-2014 10:26:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @addDetectionChannel_OpeningFcn, ...
                   'gui_OutputFcn',  @addDetectionChannel_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before addDetectionChannel is made visible.
function addDetectionChannel_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to addDetectionChannel (see VARARGIN)
global vtset
% Choose default command line output for addDetectionChannel
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);




% fill tree list
treelist={};
for t = 1:numel(vtset.results)
    treelist{end+1} = vtset.results{t}.settings.treefile;
end
set(handles.listboxTreeList,'String',treelist)

% fill wavelengths
xmlfilename = dir([vtset.movieFolder '*xml']);
%%%% COPIED FROM CreateNewTrackset.m
if numel(xmlfilename)>0 %exist([vtset.movieFolder 'TATexp.xml'],'file')
    xmlfile = fileread(strcat(vtset.movieFolder, xmlfilename(end).name));
    wllist ={};
    
    
    xmlstr =  xml_parseany(xmlfile);
    
    list = xmlstr.WavelengthData{1}.WavelengthInformation;
    for k = 1:size(list,2)
        wllist{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name) '.' char(list{k}.WLInfo{1}.ATTRIBUTE.ImageType)];
    end
    
    %%% check if an image exists with this wavelength
    firstdir = dir(vtset.movieFolder);
    x=[firstdir.isdir];
    k = find(x(3:end),1,'first');
    firstdir = firstdir(k+2).name;
    
    if numel(firstdir(strfind(firstdir,'_p')+2:end)) == 4
        fourdigitpos=1;
    else
        fourdigitpos = 0;
    end
    
    if fourdigitpos
        tocheck=[vtset.movieFolder '/' firstdir '/*' wllist{1}];
    else
        tocheck=[vtset.movieFolder '/' firstdir '/*' wllist{1}];
    end
    d= dir(tocheck);
    if numel(d)==0
        for k = 1:numel(wllist)
            wllist{k}(2) = [];
        end
    end
end

set(handles.popupmenuWavelength,'string',wllist)


% UIWAIT makes addDetectionChannel wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = addDetectionChannel_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;


% --- Executes on selection change in listboxTreeList.
function listboxTreeList_Callback(hObject, eventdata, handles)
% hObject    handle to listboxTreeList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listboxTreeList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxTreeList


% --- Executes during object creation, after setting all properties.
function listboxTreeList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxTreeList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuWavelength.
function popupmenuWavelength_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuWavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuWavelength contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuWavelength


% --- Executes during object creation, after setting all properties.
function popupmenuWavelength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuWavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkboxNormalize.
function checkboxNormalize_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxNormalize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxNormalize


% --- Executes on button press in pushbuttonOK.
function pushbuttonOK_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset multiFolder

% init all
wllist = get(handles.popupmenuWavelength,'String');
wl = wllist{get(handles.popupmenuWavelength,'Value')};
if get(handles.SegmentationCheck,'Value') &&  ~isempty(strfind(wl, '*')) 
    trackset = CreateNewTracksettings(true);
else
    trackset = CreateNewTracksettings;
end
if isempty(trackset)
    return
end
corr=get(handles.checkboxNormalize,'value');

% iterate over selected trees
wh =  waitbar(0);
error=[];
counter=0;
for tree = get(handles.listboxTreeList,'Value')
    vtset.results{tree}.detects(end+1).settings.wl = wl;
    counter=counter+1;
    try
        waitbar(0,wh,sprintf('Adding channel to tree %d of %d',counter,numel(get(handles.listboxTreeList,'Value'))))
        % find correct threshmethod
        vtset.results{tree}.detects(end).settings.trackset = trackset;
        vtset.results{tree}.detects(end).settings.normalize = corr;
        if get(handles.SegmentationCheck,'value')
            vtset.results{tree}.detects(end).settings.externalSegmentation.multiFolder = multiFolder;
            detect= wl;
            list = get(handles.popupmenuWavelength,'String');
            for i = 1:numel(list)
                wll = strsplit_qtfy('.', list{i});
                wll = wll{1};
                if(~(isempty(strfind(detect, wll))))
                    externalSegmentation.extension = strrep(detect,wll,'');
                    vtset.results{tree}.detects(end).settings.externalSegmentation.extension = externalSegmentation.extension(1:end-1); % remove star
                    detect = list{i};
                    break
                end
            end
            vtset.results{tree}.detects(end).settings.wl = detect;
        end
        
        % gather images
        dchan = numel(vtset.results{tree}.detects);
        
        xmlfilename = dir([vtset.movieFolder '/*xml']);
        xmlfilename = xmlfilename(end).name;
        [~,pos]=fileparts(vtset.results{tree}.settings.treeroot);
        res = tttParser([vtset.tttfilesFolder '/' pos '/' vtset.results{tree}.settings.treefile],[vtset.results{tree}.settings.imgroot xmlfilename]);
        % gather images
        gatherimages(res,tree,'D',dchan)
        
        %run detection
        detectCells(dchan,tree,'all','complete');
        
        % set to unsaved
        vtset.unsaved(tree) = 1;
    catch
        error(end+1) = tree;
        vtset.results{tree}.detects(end)=[];
    end
end
delete(wh)

vtset.functions.lstTrees_Callback();

if isempty(error)
    delete(handles.figure1);
else
    treelist = get(handles.listboxTreeList,'String');
    treelist = treelist(error);
    treestring =treelist{1};
    for t = 2:numel(treelist)
        treestring = [treestring  '\n' treelist{t}];
    end
    
    errordlg(sprintf('There have been errors with the following tree(s)\n%s\n',sprintf(treestring)))
end
    

% --- Executes on button press in pushbuttonCancel.
function pushbuttonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1)


% --- Executes on button press in SegmentationCheck.
function SegmentationCheck_Callback(hObject, eventdata, handles)
% hObject    handle to SegmentationCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of SegmentationCheck
global multiFolder
multiFolder = false;
% check whether box is marked
if (get(handles.SegmentationCheck,'Value'))
    [list] = useSegmentation(true,  get(handles.popupmenuWavelength,'String'));
    
else
    [list] = useSegmentation(false, get(handles.popupmenuWavelength,'String'));
end
set(handles.popupmenuWavelength,'String',list);
    % make  visible 
    set(handles.popupmenuWavelength, 'Value', 1);
