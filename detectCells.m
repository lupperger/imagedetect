% AMT function to detect cells in the detect chennel
%%%% needs channel number index and number of cells/timepoints
%
%%%% writes all cellmasks and sizes, maybe some intelligent autocorrect stuff (adjust trackset),
%%%% this would need quantifications ... ?
function detectCells(channelIndex,tree,cells,range)

global vtset imgcache
imgcache.verbose = 0;
% restrict cells/range
if strcmp(cells,'all')
    cells = unique(vtset.results{tree}.detects(channelIndex).cellNr);
end
if strcmp(range,'complete')
    range = unique(vtset.results{tree}.detects(channelIndex).timepoint);
end

if strcmpi((cells),'only new cells')
    indizes = range;
else
    indizes = find(ismember(vtset.results{tree}.detects(channelIndex).cellNr,cells) & ismember(vtset.results{tree}.detects(channelIndex).timepoint,range));
end

%%% TODO optimize that one timepoint/image is only used once 4 every cell
%%% on it

for i = indizes
    % update waitbar
    waitbar(find(indizes==i)/numel(indizes))
    
    
    % check if any quant image exists
    found = 0;
    for ch = 1:numel(vtset.results{tree}.quants)
        found = found | any(vtset.results{tree}.quants(ch).timepoint == vtset.results{tree}.detects(channelIndex).timepoint(i) & vtset.results{tree}.quants(ch).cellNr == vtset.results{tree}.detects(channelIndex).cellNr(i));
    end
    
    % skip inactive cells
    if found && vtset.results{tree}.detects(channelIndex).active(i)
        
        trackset=vtset.results{tree}.detects(channelIndex).settings.trackset;
        % check for missing min max for old versions
        if ~isfield(trackset,'MaxArea')
            trackset.MaxArea = 500;
        end
        if ~isfield(trackset,'MinArea')
            trackset.MinArea = 15;
        end
        
        % x and y
        %%% TODO check all -1!!
        x = round(vtset.results{tree}.detects(channelIndex).X(i));
        y = round(vtset.results{tree}.detects(channelIndex).Y(i));
        
        
        % load image & normalize
        try
            %%% TODO: make normalize image more generic
            if strcmp(trackset.threshmethod,'MSER')
                image = loadimage([vtset.results{tree}.settings.imgroot vtset.results{tree}.detects(channelIndex).filename{i}],3, vtset.results{tree}.detects(channelIndex).settings.wl);
            elseif strcmp(trackset.threshmethod,'External Segmentation')
                name = vtset.results{tree}.detects(channelIndex).filename{i};
                name = strsplit_qtfy('.', name);
                name  = [ name{1} vtset.results{tree}.detects(channelIndex).settings.externalSegmentation.extension];
                if vtset.results{tree}.detects(channelIndex).settings.externalSegmentation.multiFolder
                    [a b c ]=fileparts(name);
                    name = [a '/segmentation/' b c];
                else
                    name = ['segmentation/' name];
                end
                %                 name = [values{1} vtset.results{tree}.detects(channelIndex).settings.wl(1:length(vtset.results{tree}.detects(channelIndex).settings.wl))];
                image = loadimage([vtset.results{tree}.settings.imgroot name],vtset.results{tree}.detects(channelIndex).settings.normalize, vtset.results{tree}.detects(channelIndex).settings.wl);
            else
                image = loadimage([vtset.results{tree}.settings.imgroot vtset.results{tree}.detects(channelIndex).filename{i}],vtset.results{tree}.detects(channelIndex).settings.normalize, vtset.results{tree}.detects(channelIndex).settings.wl);
            end
            %%%% ATTENTION!!!
            %         only for testing, segmentation on two channels at same time
            %         image = image+loadimage([vtset.results{tree}.settings.imgroot vtset.results{tree}.detects(channelIndex).filename{i}(1:end-5) '2.png'],vtset.results{tree}.detects(2).settings.normalize, 'w2.png');
            
            
            % check if we are near the boarder of an image, then skip
            if x<0+sqrt(trackset.MaxArea) || y<0+sqrt(trackset.MaxArea) || x>size(image,2)-sqrt(trackset.MaxArea) || y>size(image,1)-sqrt(trackset.MaxArea)
                warnstring=sprintf('Negative or cell near the boarder! \nSkipping cell %d timepoint %d, X = %d Y = %d\n',vtset.results{tree}.detects(channelIndex).cellNr(i),vtset.results{tree}.detects(channelIndex).timepoint(i),x,y);
                fprintf(warnstring);
                %             vtset.results{tree}.detects(channelIndex).active(i) = 0;
                %             continue;
            end
            
            %%%% SEGMENTATION
            
            [Center ,~,trackset] = cellSegmentation(image,x,y,trackset);
            
            % optimize segmentation
            if sum(Center(:))<trackset.MinArea || sum(Center(:))>trackset.MaxArea && strcmp(trackset.threshmethod(1),'O')
                [Center, trackset]=optimizeSegmentation(Center,image,x,y,trackset);
            end
        catch e
            fprintf('Skipping cell segmentation timepoint %d cell %d, tree %s;\n',vtset.results{tree}.detects(channelIndex).timepoint(i),vtset.results{tree}.detects(channelIndex).cellNr(i),vtset.results{tree}.settings.treefile);
            fprintf('Error: %s\n',e.message);
            Center = 0;
            trackset=vtset.results{tree}.detects(channelIndex).settings.trackset;
            
        end
    else
        fprintf('Skipping cell segmentation timepoint %d cell %d; no relevant quantification found.\n',vtset.results{tree}.detects(channelIndex).timepoint(i),vtset.results{tree}.detects(channelIndex).cellNr(i));
        Center = 0;
        trackset=vtset.results{tree}.detects(channelIndex).settings.trackset;
    end
    
    % finally store size & cellmask & perimeter
    vtset.results{tree}.detects(channelIndex).cellmask{i} = Center;
    vtset.results{tree}.detects(channelIndex).size(i) = sum(Center(:));
    vtset.results{tree}.detects(channelIndex).inspected(i) = 0;
    vtset.results{tree}.detects(channelIndex).perimeter(i) = sum(sum(bwperim(Center)));
    
    if numel(vtset.results{tree}.detects(channelIndex).trackset) == 0
        vtset.results{tree}.detects(channelIndex).trackset(i) = trackset;
        continue
    end
    
    if isStructEqual(vtset.results{tree}.detects(channelIndex).trackset , trackset)
        vtset.results{tree}.detects(channelIndex).trackset(i) = trackset;
    else
        for f = fieldnames(trackset)'
            vtset.results{tree}.detects(channelIndex).trackset(i).(cell2mat(f)) = trackset.(cell2mat(f));
        end
    end
    
end
imgcache.verbose=1;
