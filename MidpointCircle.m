% Draw a circle in a matrix using the integer midpoint circle algorithm
% Does not miss or repeat pixels
% Created by : Peter Bone
% Created : 19th March 2007
function i = MidpointCircle(i, radius, xc, yc, value)

xc = int16(xc);
yc = int16(yc);

x = int16(0);
y = int16(radius);
d = int16(1 - radius);

i(xc, min(size(i,2),yc+y)) = value;
i(xc, max(1,yc-y)) = value;
i(min(size(i,1),xc+y), yc) = value;
i(max(1,xc-y), yc) = value;

while ( x < y - 1 )
    x = x + 1;
    if ( d < 0 ) 
        d = d + x + x + 1;
    else 
        y = y - 1;
        a = x - y + 1;
        d = d + a + a;
    end
    i( min(size(i,1),x+xc),  min(size(i,2),y+yc)) = value;
    i( min(size(i,1),y+xc),  min(size(i,2),x+yc)) = value; %%
    i( min(size(i,1),y+xc), max(1,-x+yc)) = value;
    i( min(size(i,1),x+xc), max(1,-y+yc)) = value;
    i(max(1,-x+xc), max(1,-y+yc)) = value;
    i(max(1,-y+xc), max(1,-x+yc)) = value;
    i(max(1,-y+xc),  min(size(i,2),x+yc)) = value;
    i(max(1,-x+xc),  min(size(i,2),y+yc)) = value;
end
