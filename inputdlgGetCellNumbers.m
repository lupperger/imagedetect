function cells = inputdlgGetCellNumbers(varargin)
if numel(varargin)>0
    cells = varargin{1};
else
    cells = inputdlg('Enter cell numbers','Cell Numbers');
    if numel(cells) == 0
        cells = [];
        return
    end
    cells=cells{1};
end


if strcmpi(cells,'all')
    cells = unique([vtset.results{vtset.treenum}.quants(1).cellNr]);
else
    f = strfind(cells,'>g');
    if f
        gen = str2double(cells(f+2))+1;
        cells(f:f+2) =[];
        cells = [cells ' ' num2str(2^gen:max(vtset.results{vtset.treenum}.nonFluor.cellNr))];
    end
    cells=strrep(cells, '-',':');
    cells=strrep(cells, ',',' ');
    cells=['[' cells ']' ];
    cells=eval(cells);
%     cells=cells(ismember(cells, unique([vtset.results{vtset.treenum}.quants(1).cellNr])));
end