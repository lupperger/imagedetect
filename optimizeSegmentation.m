function [Center trackset]=optimizeSegmentation(Center,image,x,y,trackset)
fprintf('Optimizing segmentation... \n')
if sum(Center(:))>trackset.MaxArea
    for i=1:10
        trackset.threshcorr=max(0,trackset.threshcorr+0.1);
        Center = cellSegmentation(image,x,y,trackset);
        if sum(Center(:))<trackset.MaxArea
            return
        end
    end
elseif sum(Center(:))<trackset.MinArea
    for i=1:10
        trackset.threshcorr=min(2,trackset.threshcorr-0.1);
        Center = cellSegmentation(image,x,y,trackset);
        if sum(Center(:))>trackset.MinArea
            return
        end
    end
end