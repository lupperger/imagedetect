function varargout = TreeList(varargin)
% TREELIST M-file for TreeList.fig
%      TREELIST, by itself, creates a new TREELIST or raises the existing
%      singleton*.
%
%      H = TREELIST returns the handle to a new TREELIST or the handle to
%      the existing singleton*.
%
%      TREELIST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TREELIST.M with the given input arguments.
%
%      TREELIST('Property','Value',...) creates a new TREELIST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TreeList_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TreeList_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TreeList

% Last Modified by GUIDE v2.5 03-Jan-2013 09:25:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TreeList_OpeningFcn, ...
                   'gui_OutputFcn',  @TreeList_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TreeList is made visible.
function TreeList_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TreeList (see VARARGIN)

global treelist selectedtrees restrictcells

restrictcells='All';

if numel(varargin)>1
    selectedtrees=varargin{2};
else
    selectedtrees=[];
end
% Choose default command line output for TreeList
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

set(handles.individualRestrictedCells,'KeyPressFcn',@myKeyFunction);

%%%%% gets ttt directory searches for all ttt files and lists them

tttroot=cell2mat(varargin{1});
whtemp=waitbar(0,'Scanning TTT files...');
drawnow;
filestrct = getfiles_Nonrec_all(tttroot,{'ttt';'csv';'amt_tree'});
%filestrct = getfiles_Nonrec(tttroot,'ttt');
%filestrct2 = getfiles_Nonrec(tttroot,'csv');
%filestrct.folders= [filestrct.folders filestrct2.folders];
%filestrct.files = [filestrct.files filestrct2.files];
amtCells = ~cellfun(@isempty,strfind(filestrct.files,'amt_tree'));
amtfiles.folders = filestrct.folders(amtCells);
amtfiles.files = filestrct.files(amtCells);
filestrct.folders= filestrct.folders(~amtCells);
filestrct.files = filestrct.files(~amtCells);
delete(whtemp);
% tttfolder=dir(tttroot);
treelist.root=filestrct.folders;
treelist.treefile=filestrct.files;

% check for already existing quantifications
for f = 1:numel(filestrct.files)
    tttfile = strsplit_qtfy('.',filestrct.files{f});
    idx=strfind(amtfiles.files, tttfile{1});
    if ([idx{:,:}])
        filestrct.files{f} = [filestrct.files{f} '   | (found QTFy file)'];
    end
end

set(handles.treelist,'String',filestrct.files);
set(handles.treelist,'Value',selectedtrees);



% UIWAIT makes TreeList wait for user response (see UIRESUME)
uiwait(handles.figure1);

% set radio button to INDIVIDUAL if number entered
function myKeyFunction(src, evnt)
global restrictcells
handles = guidata(src);
% k= evnt.Key; %k is the key that is pressed
 
% if strcmp(k,'return') %if enter was pressed
    pause(0.01) %allows time to update
    set(handles.individual,'Value',1);
    restrictcells='Individual';

% end


% --- Outputs from this function are returned to the command line.
function varargout = TreeList_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
global selectedtrees restrictcells confirmed treelist

if confirmed
    varargout{1} = selectedtrees;
    varargout{2} = restrictcells;
    varargout{3} = treelist;
else
    varargout{1}=[];
    varargout{2}=[];
    varargout{3}=[];    
end


% --- Executes on selection change in treelist.
function treelist_Callback(hObject, eventdata, handles)
% hObject    handle to treelist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns treelist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from treelist
% global selectedtrees
% 
% treenum = get(handles.treelist,'Value');
% 
% if ismember(treenum,selectedtrees)
%     selectedtrees(selectedtrees==treenum)=[];
% else
%     selectedtrees=[selectedtrees treenum];
% end
% update(handles)


% --- Executes during object creation, after setting all properties.
function treelist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to treelist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in okButton.
function okButton_Callback(hObject, eventdata, handles)
% hObject    handle to okButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global confirmed restrictcells selectedtrees

selectedtrees = get(handles.treelist,'Value');

if strcmp(restrictcells(1),'I')
    restrictcells=get(handles.individualRestrictedCells,'String');
end

confirmed=1;
delete(handles.figure1)


% --- Executes on button press in cencelButton.
function cencelButton_Callback(hObject, eventdata, handles)
% hObject    handle to cencelButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global confirmed

confirmed=0;

delete(handles.figure1);



% --- Executes on button press in selectNone.
function selectNone_Callback(hObject, eventdata, handles)
% hObject    handle to selectNone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global selectedtrees
selectedtrees=[];
update(handles);

% --- Executes on button press in selectall.
function selectall_Callback(hObject, eventdata, handles)
% hObject    handle to selectall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global selectedtrees

treelist=get(handles.treelist, 'String');

selectedtrees= 1:numel(treelist);
update(handles);


function update(handles)

global selectedtrees

set(handles.treelist,'Value',selectedtrees);


% --- Executes on button press in all.
function all_Callback(hObject, eventdata, handles)
% hObject    handle to all (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of all
global restrictcells
restrictcells='All';



% --- Executes on button press in onlyNewCells.
function onlyNewCells_Callback(hObject, eventdata, handles)
% hObject    handle to onlyNewCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of onlyNewCells
global restrictcells
restrictcells='Only new Cells';

% --- Executes on button press in individual.
function individual_Callback(hObject, eventdata, handles)
% hObject    handle to individual (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of individual
global restrictcells
restrictcells='Individual';


function individualRestrictedCells_Callback(hObject, eventdata, handles)
% hObject    handle to individualRestrictedCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of individualRestrictedCells as text
%        str2double(get(hObject,'String')) returns contents of individualRestrictedCells as a double



% --- Executes during object creation, after setting all properties.
function individualRestrictedCells_CreateFcn(hObject, eventdata, handles)
% hObject    handle to individualRestrictedCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectallnew.
function selectallnew_Callback(hObject, eventdata, handles)
% hObject    handle to selectallnew (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
list = get(handles.treelist,'String');
found=zeros(1,numel(list));
for x=1:numel(list)
    if numel(strfind(list{x},'found'))
        found(x)=1;
    end
end
set(handles.treelist,'value',find(~found));

