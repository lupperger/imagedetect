%% go through a tree, with all cell numbers from moritz and mark them as inspected, also ask for correct channel
global vtset
%%%% tree 120125PH5 13-001
cells = [1 2 3 4 6 8 9 12			 16				17	18	19	24		25	 32			33	35	36	37	48	49	50	51 64		65	67	71	75	76	96	99	101	102 128	129	130 131	135	143	151	152	195	199	203	205 256			270 271	287	303	304	384	398	407	411 512				575	607	608	768	798	815	823 1024				1150	1215	1216 1217	1536  1537	1598	1631	1647 2048						2434 2435			3263	3295  	999];
%%%% tree 120125 - 09 007
cells= [1																								2												3												4										5		6				7								8				9						10		12	13			14				15				16			17	18			19			21		24	26			28		29		30		31		32		33	34	36		37	38		39	42	43	49	52		53	56	57	58	59	60	61	62	63	64	65	67	68	72		75	76		79	84	87	99	105		107	112	115	116	119	120	123	124	126	127 128	131	135	136	144		151	152		159	168	175	199	211		215	224	231	233	239	240	247	248	253	255 256	263	271	272	288	289	303	304		319	336	351		422	423	431	448	463	466	479	480	495	496	507	510/511 512		543	544	576	579	606/607	608	609	639	672	702		844	847	863	897	927	932	959	960	991	992	1014	1020/1023 		1087	1088		1159		1216	1218	1279	1344	140		1689		1327	1795	1854/1855	1864	1919	1920	1983	1984	2028	 		2175					2432	2436	2559	2688	2808		3379			3591				3840		3968	4056	 											5616												8112	  ];
%%%% tree 120125 025001
cells=[1			 											 2							3								 4			5				6				7				 8		9	10		11		12		13		14		15		 16	17	18/19	20	21	22	23	24	25	26	27	28	29	30	31	 32	34/35		40	43	44	47	48	51	52	55	56	59	60	62	63 64			80	87		95	96	103	104	111	112	119	120	124	127  128			160	175		191	192	207	208	223	224	239	240	248	 			320	351		383	384/385	415	416	446/447	448	479	480	496	 			640	703		767	770			892	896	959	960	992	 			1280	1407		1535	1540			1785	1792	1919	1920	1984	 			2560	2815			3080				3584/3585	3939	3840	3968	  											7949			];
%%%% tree 120208 020 001
cells=[1	 												2							3						4				5			6				7		 8		9		10	11		12		13		14	15	 16	17	18	19	20	22	23	24	25	26	27	28	30	31 32	35	36	39	40	44	47	48	51	52	55	56	60	63 64	71	72	79	80	88	95	96	103	104	111	112	120	127 	143	144	159	160	176	191	192	207	208	223	224	240	255 	287		316	320	352	383	384	415	416	447	448	480	511 				640	704	767	768	831	832	895		960	1023 				1280		1535	1536	1663	1664			1920	2047 							3071	3327				3840	4095 												7680	 ];
%%%% tree 120208 030 001
cells=[1																										 2																										3 4							5										6									7 8			9				10				11						12		14							15 16		17	18		19		20		21		22				23		24	49	28		30					31 32	33	34	36	37	38	39	40	41	42	43	44	45			46	47	48	98	56		60	61		63		63 64	67	68	72	75	76	79	80	86	84	87 88	90		91	92	95	96	196	112	113	120	122		126	127	127 128	135		144			159	160	167	168	175	176	180	181	183	184	191	192	392	224	227	240	244	245	252	255	255 256	271		288				320	335	336	351	352	360	363	367	368	383	384	785	448	455	480	488	491	504		 	543						640	671		703	704	720	727	735	736	767	768	1570	986		960		983	1008		 	1087						1280	1343		1407	1408		1457	1471	1472	1535			1792				1967	2016		 	2175									2815	2816					3701								4032		 										5631];																 
channels = [1 2 3 4]; 
figure('position',[654   185   560   420]);
for ixx = ixx:numel(cells)
    c = cells(ixx);
    % show all timecourses, let them click 
    for quantChannel = channels
    alltps=vtset.results{vtset.treenum}.quants(quantChannel).(vtset.timemode)(vtset.results{vtset.treenum}.quants(quantChannel).cellNr==c & vtset.results{vtset.treenum}.quants(quantChannel).active==1)/vtset.timemodifier;
    alldata=vtset.results{vtset.treenum}.quants(quantChannel).int(vtset.results{vtset.treenum}.quants(quantChannel).cellNr==c & vtset.results{vtset.treenum}.quants(quantChannel).active==1);
    [alltps, order] = sort(alltps);
    alldata = alldata(order);
    subplot(numel(channels),1,find(quantChannel ==channels))
    plot(alltps,alldata)
    title(sprintf('cell %d',c))
    end
    if isempty(alltps)
        continue
    end
    a = questdlg(sprintf('Which channel'),'channel?','1','2','cancel','1');
    if strcmp(a,'cancel')
        return
    end
    if numel(a)
        vtset.results{vtset.treenum}.quants(str2double(a)).inspected(vtset.results{vtset.treenum}.quants(str2double(a)).cellNr==c)=1;
        vtset.results{vtset.treenum}.quants(str2double(a)+2).inspected(vtset.results{vtset.treenum}.quants(str2double(a)+2).cellNr==c)=1;
        vtset.results{vtset.treenum}.detects(str2double(a)).inspected(vtset.results{vtset.treenum}.detects(str2double(a)).cellNr==c)=1;
    end
    
end