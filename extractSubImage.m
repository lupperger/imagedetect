function [subimg xcenter ycenter]=extractSubImage(image, x, y, win)

yfrom=y-floor(win/2);
yto=y+floor(win/2);
xfrom=x-floor(win/2);
xto=x+floor(win/2);

xcenter = ceil(win/2);
ycenter = ceil(win/2);

% X
if xfrom<1
  xcenter=x;
  xfrom = 1;
elseif xto > size(image,2)
    xto = size(image,2);
end

% Y
if yfrom<1
  ycenter=y;
  yfrom = 1;
elseif yto > size(image,1)
    yto = size(image,1);
end

% extract the subimages
subimg = image(round(yfrom:yto), round(xfrom:xto));