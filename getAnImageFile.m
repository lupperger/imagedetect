%% function searches exp directory for image files to return information about its structure

function [imName file ext] = getAnImageFile(directory , exp)
% values = strsplit_qtfy('/',directory);
% exp = values{numel(values)};
x= dir(directory);
isub = [x(:).isdir];
if(numel(x(isub)) > 0)
nameFolds = {x(isub).name};
 y = strfind(nameFolds, exp);
Index = find(not(cellfun('isempty', y)));
folder = [directory nameFolds{Index(1)}];
else
    folder = directory;
end
x = dir(folder);
names = {x.name};
for i = 1: numel(names)
    try
        in = imfinfo([folder '/' names{i}]);
        file = [folder '/' names{i}];
        imName = names{i};
        break;
    catch
        continue;
    end
end
 y = strsplit_qtfy('.',imName);
ext = y{numel(y)};
end

