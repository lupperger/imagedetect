% simple filter for outliers with to high steepness

function [ovec counter]=deltafiltering(vec,thresh)
counter=0;
% deleteid=[];
deriv= diff(vec);
ovec=vec;

for i = 2:numel(deriv)-1
    if deriv(i)-deriv(i-1)>thresh
        counter=counter+1;
        ovec(i) = mean([vec(i-1) vec(i+1)]);

    end
end

% vec(deleteid)=[];