function features = getTilingFeatures(sub,idx)
if isempty(sub)
    features = 7 + 6 + 15;% + 27;
elseif exist('idx','var')
    features=nan(1,7+6+15);
    for id = idx
        switch id
            case 1
                features(id) = median(sub(:));
            case 2
                features(id) = mean(sub(:));
            case 3
                features(id) =  std(sub(:));
            case 4
                features(id) = skewness(sub(:));
            case 5
                features(id) =  kurtosis(sub(:));
            case 6
                features(id) =  var(sub(:))/mean(sub(:));
            case 7
                features(id) =  max(sub(:))-min(sub(:));
            case 12
                temp=computeTamuraQTFySelected(sub,2);
                features(id) =  temp(5);
            case 13
                temp=computeTamuraQTFySelected(sub,3);
                features(id) =  temp(6);
        end
    end
    if any(ismember(idx,[8,9,10,11]))
        temp = computeTamuraQTFySelected(sub,1);
        features(8:11) =  temp(1:4);
    end
    if any(ismember(idx,[ 14    15    16    17    18    19    20    21    22    23    24    25    26    27    28]))
        features(14:28) =  computeTextureQTFy(sub,true(size(sub)));
    end
else
    features = [median(sub(:)) mean(sub(:)) std(sub(:)) skewness(sub(:)) kurtosis(sub(:)) var(sub(:))/mean(sub(:)) max(sub(:))-min(sub(:))];
    features = [features computeTamuraQTFy(sub)]; % Tamura
    features = [features computeTextureQTFy(sub,true(size(sub)))]; % Textures
    %     features = [features computeHoG2QTFy(sub,[3,3])']; % HoGs
end