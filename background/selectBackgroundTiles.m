function selectBackgroundTiles(img,win)
global sbt
sbt.cursor =[];
if nargin>1
    sbt.win = win;
else
    sbt.win = 50;
end
sbt.coords=[];
sbt.classes=[];
sbt.allrect=[];
if isfield(sbt,'h') && ishandle (sbt.h)
    figure(sbt.h)
else
    sbt.h=figure;
    
    set(sbt.h,'KeyPressFcn',@myfmatkey)
    set(sbt.h,'WindowButtonMotionFcn',@mywbmf)
    set(sbt.h,'WindowButtonDownFcn',@mywbdf)
    
end

imagesc(img,'parent',gca(sbt.h))
hold on
colormap(gray(256));
axis off
axis equal
sbt.rect= rectangle('position',[1 1 sbt.win sbt.win],'Edgecolor',[1 0 0]);
updateTitle();

function mywbmf(src,evnt)
global sbt

act= get(gca(sbt.h),'CurrentPoint');
sbt.cursor=act(1,1:2);
xlim=get(gca(sbt.h),'xlim');
ylim=get(gca(sbt.h),'ylim');

if xlim(1)<sbt.cursor(1) && sbt.cursor(1)<xlim(2) && ylim(1)<sbt.cursor(2) && sbt.cursor(2)<ylim(2)
%     'drin' inside 
    set(sbt.rect,'position',[sbt.cursor sbt.win sbt.win])
    
    
    
        
end

function mywbdf(src,evnt)
global sbt
sbt.coords(end+1,:) = sbt.cursor;


if strcmp('normal',(get(gcf,'SelectionType')))
    % right mouse for cells
    sbt.allrect(end+1)=rectangle('position',[sbt.cursor sbt.win sbt.win],'Edgecolor',[1 1 0]);
    sbt.classes(end+1) = 0;
else
    % left for background
    sbt.allrect(end+1)=rectangle('position',[sbt.cursor sbt.win sbt.win],'Edgecolor',[0 0 1]);
    sbt.classes(end+1) = 1;
end
updateTitle();


function updateTitle
global sbt
title(gca(sbt.h),sprintf('isBackground (blue = right mouse button) %d, noBackground (yellow = left mouse button) %d',sum(sbt.classes == 1),sum(sbt.classes == 0)))

function myfmatkey(src,evnt)
global sbt

% handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

if strcmp(k,'backspace') 
    if numel(sbt.allrect)
        delete(sbt.allrect(end));
        sbt.allrect(end)=[];
        sbt.classes(end)=[];
        sbt.coords(end,:)=[];
    end

end
updateTitle();