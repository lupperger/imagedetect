function [ZI counter imagefeatures] = backgroundestimation_modified( varargin)
%BACKGROUNDESTIMATION_MODIFIED Summary of this function goes here
%   Detailed explanation goes here


image=varargin{1};

% get params
if numel(varargin)>1
    settings=varargin{2};
    
    binsize=settings.winsize; %20-40
    kurto=settings.kurtosis; %3
    skew = settings.skewness; %0.3
    varmean = settings.dispersion; %2.5e-04
else
    binsize = 20;
    kurto = 3;
    skew = 0.3;
    varmean = 2.5e-04;
end

if numel(varargin)>2
    handles=varargin{3};
end

if numel(varargin)>3
    features_exist=varargin{4};
end

if numel(varargin)>4
    features=varargin{5};
end


%init
show=0;
imgsize=size(image);
imgwidth=size(image,2);
imghight=size(image,1);

wh = waitbar(0,'Getting image features...');
try
% varmean =0.0009; %% nan gfp
% varmean = 0.0003; %% venus
if show
    figure;
end
scounter=0;
minpoints = 70000;
points=100000;
while points>minpoints
    counter=0;
    illu= [];
for i = 1:binsize/2:imgsize(1)-binsize
waitbar(i/imgsize(1),wh)
    for j=1:binsize/2:imgsize(2)-binsize
        
        sub = image(i:i+binsize, j:j+binsize);
        
  
        if ~features_exist
            features.kurtosis{i}{j} = kurtosis(sub(:));
            features.skewness{i}{j} = skewness(sub(:));
            features.var{i}{j} = var(sub(:))/mean(sub(:));
            features.meansub{i}{j} = mean(sub(:));
        end
        if features.kurtosis{i}{j} < kurto && features.skewness{i}{j} <skew && features.var{i}{j} < varmean %&& ...%mean(sub(:))*1.3 > max(sub(:))  &&...
                %max(sub(:))-min(sub(:)) <0.1% && mean(sub(:))*1.3 > max(sub(:)) 
            counter=counter+1;
            % found BG

%             x(end+1) = i+binsize/2;
%             y(end+1) = j+binsize/2;
%             z(xcounter,ycounter)= mean(sub(:));
            illu(end+1,:)=[i+binsize/2,j+binsize/2, features.meansub{i}{j}];
%             illu2(i+binsize/2,j+binsize/2)=mean(sub(:));
%         else 
%             fprintf('y %d,x %d\n',i,j);
        end
    end
end
points = counter;
fprintf('Found %d interpoints, optimizing...\n',points);
varmean = varmean*0.8;
end
% toBase(illu2)
fprintf('using %d interpolation points\n',counter)

% update background GUI
if numel(varargin)>2
    drawnow;
end
waitbar(1,wh,'Interpolating...')
%%%% check if x and y are interchanged ... -.-
%%%% should be fine now! 29.04.11
y=illu(:,1);
x=illu(:,2);
z=illu(:,3);


%interpolate
[XI YI] = meshgrid(1:imgwidth,1:imghight);
F=TriScatteredInterp(x,y,z,'natural');
ZI=F(XI,YI);

%extrapolate
for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
    ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
end

for i=1:imgwidth
    ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
end

% repair strange extrapolations
ZI(ZI<min(image(:)))=min(image(:));
ZI(ZI>max(image(:)))=max(image(:));
close(wh);
imagefeatures = features;

catch e
    delete(wh)
    rethrow(e)
end