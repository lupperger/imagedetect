function [featuremat classes] = exploreBackgroundParams(varargin)

image = varargin{1};
imgwidth=size(image,2);
imghight=size(image,1);


if numel(varargin)>1
    binsize = varargin{2};
else
    binsize = 30;
end

if numel(varargin)>2
    auto = varargin{3};
else
    auto=0;
end

counter = 0;
featuremat= [];
classes=[];
if auto
    h=figure('position',[ 285   572   560   420]);
end
wh = waitbar(0,'Detecting image features');
for x=1:4000
    waitbar(x/4000,wh);
    r = rand(1,2);
    works=0;
    try
        sub = image(round(r(1)*imghight):round(r(1)*imghight)+binsize, round(r(2)*imgwidth):round(r(2)*imgwidth)+binsize);
        if auto
            % show subimg
            subplot(1,2,1)
            imagesc(sub)
            axis off
            axis equal
            colormap(gray)
            % show stats
            subplot(1,2,2)
            plot(1:10,zeros(1,10))
            axis off
            axis equal
            text(1,3,sprintf('sk:%2.2f',skewness(sub(:))))
            text(1,0,sprintf('ku:%2.2f',kurtosis(sub(:))))
            text(0,-3,sprintf('d:%2.5f',var(sub(:))/mean(sub(:))))
        end
        works=1;
    catch e

    end
    
    if works
        
        if auto
            button = questdlg(sprintf('is this background?\nso far %d bg %d cells',sum(classes),sum(~classes)),'background?','Yes','No','Cancel','Yes');
        else
            button='Y';
        end
        if strcmp(button(1),'Y')
            counter=counter+1;
            featuremat(counter,:)=[skewness(sub(:)) kurtosis(sub(:)) var(sub(:))/mean(sub(:))];
            classes(counter)=1;
        elseif strcmp(button(1),'C')
            delete(h)
            return;
        else
            counter=counter+1;
            featuremat(counter,:)=[skewness(sub(:)) kurtosis(sub(:)) var(sub(:))/mean(sub(:))];
            classes(counter)=0;
        end
    end
    
end
delete(wh)

function dummy
%%
global opti
[featuremat classes]=exploreBackgroundParams(img,50,1);

%% plot the classes
cl = {'Red','blue','green'};
figure;
hold on
for x=1:numel(classes)
    plot3(featuremat(x,1),featuremat(x,2),featuremat(x,3),'o','Color',cl{classes(x)+1},'linewidth',10)
end
xlabel('skewness')
ylabel('kurtosis')
zlabel('dispersion')
legend({'background','cell'})
plot3(centroids(1,1),centroids(1,2),centroids(1,3),'x','color','black','linewidth',20)
plot3(centroids(2,1),centroids(2,2),centroids(2,3),'x','color','black','linewidth',20)
% plot3(centroids(3,1),centroids(3,2),centroids(3,3),'x','color','black','linewidth',10)
%% plot tree

t = classregtree(featuremat,classes,'names',{'skewness','kurtosis','dispersion'})

view(t)

%% try some unsupervised

[classes centroids] = kmeans(featuremat,2) ;
classes = classes-1;
centroids
%% settings
if centroids(1,1)<centroids(2,1)
    settings.kurtosis = centroids(1,2);
    settings.skewness = centroids(1,1);
    settings.dispersion = centroids(1,3);
else
    settings.kurtosis = centroids(2,2);
    settings.skewness = centroids(2,1);
    settings.dispersion = centroids(2,3);
end

%% plot densitiy matrix

figure
scattercloud(featuremat(:,2),featuremat(:,3),30,1,'k.',jet(256),1)


%% try dbscan

[classes,type]=dbscan(featuremat,100,[]);
unique(classes)
classes(classes==-1)=0;
%%
cl = {'green','blue','red'};
figure;
hold on
for x=unique(classes)
    plot3(featuremat(classes ==x,1),featuremat(classes == x,2),featuremat(classes ==x,5),'.','Color',cl{unique(classes(classes==x))+1},'linewidth',10)
end
xlabel('Skewness')
ylabel('Kurtosis')
zlabel('Variance')

