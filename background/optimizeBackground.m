function varargout = optimizeBackground(varargin)
% OPTIMIZEBACKGROUND MATLAB code for optimizeBackground.fig
%      OPTIMIZEBACKGROUND, by itself, creates a new OPTIMIZEBACKGROUND or raises the existing
%      singleton*.
%
%      H = OPTIMIZEBACKGROUND returns the handle to a new OPTIMIZEBACKGROUND or the handle to
%      the existing singleton*.
%
%      OPTIMIZEBACKGROUND('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OPTIMIZEBACKGROUND.M with the given input arguments.
%
%      OPTIMIZEBACKGROUND('Property','Value',...) creates a new OPTIMIZEBACKGROUND or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before optimizeBackground_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to optimizeBackground_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help optimizeBackground

% Last Modified by GUIDE v2.5 09-Jun-2011 17:58:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @optimizeBackground_OpeningFcn, ...
                   'gui_OutputFcn',  @optimizeBackground_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before optimizeBackground is made visible.
function optimizeBackground_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to optimizeBackground (see VARARGIN)
global opti imgcache siq

if isfield(opti,'batch') && opti.batch
    set(hObject,'Visible','off')
end

if ~isfield(imgcache,'size')
    imageCache('init',100);
end

% Choose default command line output for optimizeBackground
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
opti.figurehandle = handles.figure1;
opti.settings.contrastMin = 0;
opti.settings.contrastMax = 1;
opti.colorbar=0;
opti.redraw=@redraw;
opti.forcesave = 0;

if ~isfield(opti.settings,'winsize')
    opti.settings.winsize = 34;
end

if numel(varargin)>2
    opti.settings.contrastMin = varargin{3};
    opti.settings.contrastMax = varargin{4};
end

if numel(varargin)>0
    opti.img = varargin{1};
    axes(handles.axesOriginal)
    imagesc(opti.img,[opti.settings.contrastMin opti.settings.contrastMax]);
%     opti.settings
    axis off 
    axis equal
    colorbar
    set(handles.textStatus,'String',sprintf('Image %s loaded',siq.filename{siq.fid}))
    
%     axes(handles.axesBackground)
%     cla
    
    if numel(varargin)>1 && numel(varargin{2})
        opti.ZI = varargin{2};
        if isfield(opti.settings,'kurtosis')
            setparams(handles)
        end
        redraw()
       
    else
        if exist(opti.suggest,'file')
            opti.ZI = loadimage(opti.suggest,0);
            if isfield(opti.settings,'kurtosis')
                setparams(handles)
            end
            redraw();
        else
            if ~isfield(opti.settings,'kurtosis')
                ButtonAuto_Callback([],[],handles);
            else
                setparams(handles)
                
                set(handles.textStatus,'String',sprintf('Detecting parameters ... Done.'));
                drawnow;
                ButtonRecalc_Callback([],[],handles)
            end
        end
    end
    
    
    
%     axes(handles.axesNormalized)
    
end


% UIWAIT makes optimizeBackground wait for user response (see UIRESUME)
if isfield(opti,'batch') && opti.batch
    ButtonOK_Callback([],[],handles)
else
    uiwait(handles.figure1);
end


% --- Outputs from this function are returned to the command line.
function varargout = optimizeBackground_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti
% Get default command line output from handles structure
varargout{1} = opti.ZI;
varargout{2} = opti.settings.contrastMin;
varargout{3} = opti.settings.contrastMax;



function editWinSize_Callback(hObject, eventdata, handles)
% hObject    handle to editWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editWinSize as text
%        str2double(get(hObject,'String')) returns contents of editWinSize as a double


% --- Executes during object creation, after setting all properties.
function editWinSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editKurtosis_Callback(hObject, eventdata, handles)
% hObject    handle to editKurtosis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editKurtosis as text
%        str2double(get(hObject,'String')) returns contents of editKurtosis as a double


% --- Executes during object creation, after setting all properties.
function editKurtosis_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editKurtosis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editSkewness_Callback(hObject, eventdata, handles)
% hObject    handle to editSkewness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSkewness as text
%        str2double(get(hObject,'String')) returns contents of editSkewness as a double


% --- Executes during object creation, after setting all properties.
function editSkewness_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSkewness (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDispersion_Callback(hObject, eventdata, handles)
% hObject    handle to editDispersion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDispersion as text
%        str2double(get(hObject,'String')) returns contents of editDispersion as a double


% --- Executes during object creation, after setting all properties.
function editDispersion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDispersion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonOK.
function ButtonOK_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti
% if ~exist(opti.suggest,'file')
    opti.forcesave=1;
    ButtonSaveBackground_Callback([],[],handles)
% end
delete(handles.figure1)

% --- Executes on button press in ButtonCancel.
function ButtonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti
opti.ZI = 1;
delete(handles.figure1)

% --- Executes on button press in ButtonRecalc.
function ButtonRecalc_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonRecalc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti

readparams(handles)

set(handles.textStatus,'String','Calculating Background...')
drawnow

tic
try
[opti.ZI counter] = backgroundestimation(opti.img,opti.settings,handles);
catch e
    opti.ZI = zeros(size(opti.img));
    warndlg('The settings you have made do not deliver a useful background image. Hint: increase dispersion?')
    return
end
% opti.ZI  = backgr(opti.img*255)/255;
% counter=0;

% set(handles.textStatus,'String',sprintf('Calculating Background... using %d interpolation points ...',counter))
% drawnow;

% [XI YI] = meshgrid(1:imgwidth,1:imghight);
% F=TriScatteredInterp(x,y,z,'natural');
% ZI=F(XI,YI);
% for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
%     ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
% end
% 
% for i=1:imgwidth
%     ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
% end
% 
% ZI(ZI<min(img(:)))=min(img(:));
% ZI(ZI>max(img(:)))=max(img(:));


elapsed = toc;

set(handles.textStatus,'String',sprintf('Done in %0.2f seconds.',elapsed))

redraw()



function redraw
global opti

handles=guidata(opti.figurehandle);

ZI = opti.ZI;
img = opti.img;

axes(handles.axesOriginal);
muh1=subplot(handles.axesOriginal);
imagesc(img,[opti.settings.contrastMin opti.settings.contrastMax])
axis off
axis equal
if opti.colorbar
    colorbar
end
% datacursormode on

if numel(opti.ZI)
    axes(handles.axesBackground);
    muh2=subplot(handles.axesBackground);
    imagesc(ZI,[opti.settings.contrastMin opti.settings.contrastMax])
    axis off
    axis equal
%     colorbar
    colormap(jet(256))
    
    
    axes(handles.axesNormalized);
    muh3 = subplot(handles.axesNormalized);
    imagesc(img-ZI,[opti.settings.contrastMin opti.settings.contrastMax]*mean(opti.img(:)));
    axis off
    axis equal
    linkaxes([muh1 muh2 muh3],'xy')

end

% --- Executes on button press in ButtonSelect.
function ButtonSelect_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti

if isfield(opti,'pathname')
    [filename pathname] = uigetfile([opti.pathname '*.*']);
else
    [filename pathname] = uigetfile('*.*');
end

if filename ~= 0
    opti.pathname = pathname;
    img = loadimage([pathname filename],0);
    
    opti.img = img;
    
    subplot(handles.axesOriginal)
    imagesc(img);
    axis off 
    axis equal
    colorbar
    set(handles.textStatus,'String',sprintf('Image %s loaded.',filename))
    
    subplot(handles.axesBackground)
    cla
    
    subplot(handles.axesNormalized)
    cla
    
end



function editContrastMin_Callback(hObject, eventdata, handles)
% hObject    handle to editContrastMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editContrastMin as text
%        str2double(get(hObject,'String')) returns contents of editContrastMin as a double


% --- Executes during object creation, after setting all properties.
function editContrastMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editContrastMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editContrastMax_Callback(hObject, eventdata, handles)
% hObject    handle to editContrastMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editContrastMax as text
%        str2double(get(hObject,'String')) returns contents of editContrastMax as a double


% --- Executes during object creation, after setting all properties.
function editContrastMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editContrastMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function readparams(handles)
global opti

opti.settings.kurtosis= str2double(get(handles.editKurtosis,'String'));
opti.settings.skewness= str2double(get(handles.editSkewness,'String'));
opti.settings.winsize= str2double(get(handles.editWinSize,'String'));
opti.settings.dispersion= str2double(get(handles.editDispersion,'String'));


function setparams(handles)
global opti

set(handles.editKurtosis,'String', num2str( opti.settings.kurtosis));
set(handles.editSkewness,'String',num2str(opti.settings.skewness));
set(handles.editWinSize,'String',num2str(opti.settings.winsize));
set(handles.editDispersion,'String' ,num2str( opti.settings.dispersion));

% --- Executes on button press in ButtonRedraw.
function ButtonRedraw_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonRedraw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
readparams(handles)
redraw()


% --- Executes on button press in ButtonSaveBackground.
function ButtonSaveBackground_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSaveBackground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global opti siq

% is this a movie?
if isempty(opti.suggest)
    thisIsAMovie = 0;
%     opti.suggest = [
else
    pos = regexp(opti.suggest, '(_p)(\d{3,4})', 'match');
    if numel(pos)>1
        thisIsAMovie =1;
    else
        thisIsAMovie =0;
    end
end
if thisIsAMovie 
    %%%% commented, because this has been done in QuantifySingleImage
    %%%%% TODO : check if this works now properly
%     pos=str2double(pos{1}(3:end));
%     tp = regexp(file, '(_t)(\d{4,5})', 'match');
%     tp = str2double(tp{1}(3:end));
%     wl = regexp(file, '(_w)(\d{1,2})', 'match');
%     wl = str2double(wl{1}(3:end));
%     [path file] = fileparts(opti.suggest);
%     file = strrep(file,'_background','');
%     exp = regexp([path file], '\d{6}[A-Z]{2,}\d{1,2}', 'match');
%     exp=exp{1};
%     opti.suggest = [path '/../background/' sprintf('%s_p%04d/',exp,pos) '/' file '.png'];
end

if ~opti.forcesave
    if isfield(opti,'suggest')
        [file path]=uiputfile(opti.suggest,'Store background image');
    else
        [file path]=uiputfile('*.png','Store background image');
    end
    if file == 0
        return;
    end
else
    path='';
    file = opti.suggest;
end

if isfield(opti,'ZI')
    fprintf('Saving BG %s...',[path file]);
    imwrite(im2uint16(opti.ZI),[path file],'bitdepth',16)
    
    set(handles.textStatus,'String',sprintf('Image saved: %s',file));
    fprintf('done\n');
end

% --- Executes on button press in ButtonSaveNormalized.
function ButtonSaveNormalized_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSaveNormalized (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti

[file path]=uiputfile('*.png','Store normalized image');

if file == 0 
    return;
end

if isfield(opti,'ZI')
    
    imwrite(uint16(255*(opti.img-opti.ZI)),[path file],'bitdepth',16)
    
    set(handles.textStatus,'String',sprintf('Image saved: %s',file));
end

% --- Executes on button press in ButtonSaveSettings.
function ButtonSaveSettings_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSaveSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global opti

[path file]=uiputfile('*.mat','Store settings file');

if file == 0
    return;
end

if isfield(opti,'settings')
    settings = opti.settings;
    save([path file],'settings');
    set(handles.textStatus,'String',sprintf('Settings saved: %d',file));
end


% --- Executes on button press in ButtonAuto.
function ButtonAuto_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonAuto (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti
set(handles.textStatus,'String',sprintf('Detecting parameters ...'));
drawnow;

featuremat=exploreBackgroundParams(opti.img,str2double(get(handles.editWinSize,'String')),0);

[classes, centroids] = kmeans(featuremat,2) ;

opti.settings.kurtosis = min(centroids(:,2));
opti.settings.skewness = min(centroids(:,1));
opti.settings.dispersion = min(.9*centroids(:,3));

setparams(handles)

% centroids

% set smaller parameters:
% if centroids(1,1)<centroids(2,1)
%     set(handles.editKurtosis,'String',num2str(centroids(1,2)))
%     set(handles.editSkewness,'String',num2str(centroids(1,1)))
%     set(handles.editDispersion,'String',num2str(0.9*centroids(1,3)))
% else
%     set(handles.editKurtosis,'String',num2str(centroids(2,2)))
%     set(handles.editSkewness,'String',num2str(centroids(2,1)))
%     set(handles.editDispersion,'String',num2str(0.9*centroids(2,3)))
% end

set(handles.textStatus,'String',sprintf('Detecting parameters ... Done.'));
drawnow;
ButtonRecalc_Callback([],[],handles)


% --- Executes on button press in ButtonContrast.
function ButtonContrast_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonContrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global adjC opti

adjC.close=0;

adjustContrast(opti.settings.contrastMin, opti.settings.contrastMax,'opti','opti.redraw()','opti.settings.contrastMin','opti.settings.contrastMax',opti.img)
    


% --------------------------------------------------------------------
function uitoggletool1_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%%% ZOOM IN
zoom on


% --------------------------------------------------------------------
function uitoggletool2_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom out


% --------------------------------------------------------------------
function uitoggletool3_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
datacursormode on


% --------------------------------------------------------------------
function uitoggletool4_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uitoggletool4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opti
subplot(handles.axesOriginal)
if opti.colorbar
    colorbar off
    opti.colorbar = 0;
else
    colorbar
    opti.colorbar=1;
end