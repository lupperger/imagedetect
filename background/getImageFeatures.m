function [featuremat, illu] = getImageFeatures(image,binsize)
warning('Function getImageFeatures outdated, please use getImageFeaturesSelected in the futute.')
[featuremat, illu] = getImageFeaturesSelected(image,binsize,1:getTilingFeatures([]));
return

imgsize=size(image);
% imgwidth=size(image,2);
% imghight=size(image,1);

featuremat = [];
illu=[];
counter = 0;
wh  = waitbar(0,'Getting image features...');
for i = 1:binsize:imgsize(1)-binsize
    waitbar(i/imgsize(1),wh);
    for j=1:binsize:imgsize(2)-binsize
        counter = counter+1;
        sub = image(i:i+binsize, j:j+binsize);
        try
        featuremat(counter,:) = getTilingFeatures(sub);
%         sub = image(i+5:i+binsize-5, j+5:j+binsize-5);
        illu(end+1,:)=[i,j, mean(sub(:))];
        catch me
            fprintf('ERROR: Failed to calc sub features\n')
        end
    end
end



delete(wh)