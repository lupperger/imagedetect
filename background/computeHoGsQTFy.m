function props = computeHoGs(I_c_masked,props)
% hogs = computeHoG(double(I_c_masked),[2 10 9 0 0.2]);
hogs = computeHoG2(double(I_c_masked),[3,3]);
numel(hogs)
for h=1:numel(hogs)
    props.(['HoG_' num2str(h)]) = hogs(h);
end
end