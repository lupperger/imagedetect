function [featuremat, classes,illu] = applyTilingSVM(svm,image,binsize)

imgsize=size(image);
% imgwidth=size(image,2);
% imghight=size(image,1);

featuremat = [];
illu=[];
counter = 0;
wh  = waitbar(0,'Getting features...');
for i = 1:binsize/2:imgsize(1)-binsize
    waitbar(i/imgsize(1),wh);
    for j=1:binsize/2:imgsize(2)-binsize
        counter = counter+1;
        sub = image(i:i+binsize, j:j+binsize);
        featuremat(counter,:) = getTilingFeatures(sub);
        illu(end+1,:)=[i+binsize/2,j+binsize/2, mean(sub(:))];

    end
end

waitbar(1,wh,'Applying SVM');

classes = svmclassify(svm,featuremat);

delete(wh)