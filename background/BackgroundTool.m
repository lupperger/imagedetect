function varargout = BackgroundTool(varargin)
% BACKGROUNDTOOL MATLAB code for BackgroundTool.fig
%      BACKGROUNDTOOL, by itself, creates a new BACKGROUNDTOOL or raises the existing
%      singleton*.
%
%      H = BACKGROUNDTOOL returns the handle to a new BACKGROUNDTOOL or the handle to
%      the existing singleton*.
%
%      BACKGROUNDTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BACKGROUNDTOOL.M with the given input arguments.
%
%      BACKGROUNDTOOL('Property','Value',...) creates a new BACKGROUNDTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BackgroundTool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BackgroundTool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BackgroundTool

% Last Modified by GUIDE v2.5 06-Aug-2014 09:14:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BackgroundTool_OpeningFcn, ...
                   'gui_OutputFcn',  @BackgroundTool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BackgroundTool is made visible.
function BackgroundTool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BackgroundTool (see VARARGIN)
global bt
% Choose default command line output for BackgroundTool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% init all structs
bt.wh=NaN;
bt.wl='';
bt.pos=0;
bt.win = 30;
bt.train.coords=[]; % will be stored as 1 based index from upper left corner
% bt.train.allrect=[];
bt.train.classes=[];
bt.train.img = {};
bt.train.iscalced = [];
bt.train.features = [];
bt.train.pos = [];
bt.train.traingchanged=0;
bt.train.win = bt.win;
bt.train.usefeatures = 1:getTilingFeatures([]); 

bt.test.coords=[];
bt.test.classes=[]; % 1=bg, 0=cells
bt.test.img = {};
bt.test.iscalced = [];
bt.test.features = [];
bt.test.pos = [];
bt.test.activeRect=0;

bt.result.img={};
bt.result.coords={};
bt.result.mean={};
bt.result.classes={};
bt.result.features = {};
bt.result.pos={};
bt.result.ZI={};

bt.activeAxes=0;
bt.pressed=0;
bt.activeRect = 0;
bt.mode=1;
bt.getrandom =0;
bt.showresult =1;

bt.directoryonly = 0; % do not use TTT structure

set(handles.figure1,'name','BackgroundTool: Select / Train');

imageCache('init',50);

% select experiment folder

path=uigetdir('C:\..\','Select Experiment directory');
if path == 0
    return
end
bt.exppath =path;
exp = regexp(path, '\d{6}[A-Z]{2,}\d{1,2}', 'match');
bt.exp=exp{1};
if exist(path,'dir') && ~isempty(dir([path '/*.xml']))
    % std ttt format
    parseXML(path,handles);
else
    bt.directoryonly = 1;
    % load all files in this directory
    parseDirectory(path,handles);
    
    % check for wavelength
    
    
    % ask for filter?
end

% init all window button functions

set(handles.figure1,'KeyPressFcn',@mykeyf)
set(handles.popupmenuImage,'KeyPressFcn',@mykeyf)
set(handles.figure1,'WindowButtonMotionFcn',@mywbmf)
set(handles.figure1,'WindowButtonDownFcn',@mywbdf)

function mywbmf(src,evnt)
global bt
handles = guidata(src);

act= get(handles.axesRawImage,'CurrentPoint');
bt.cursor=act(1,1:2);
% bt.cursor(2) = size(bt.img,1)-bt.cursor(2);
xlim=[1 size(bt.img,2)];
ylim=[1 size(bt.img,1)];
% set(handles.textStatus,'String',sprintf('%3.0f,%3.0f',act(1,1),act(1,2)))
if xlim(1)<bt.cursor(1) && bt.cursor(1)<xlim(2) && ylim(1)<bt.cursor(2) && bt.cursor(2)<ylim(2)
%     'drin' inside 
    if bt.mode == 1
        set(bt.rect,'visible','on','position',[max(1,min(bt.cursor(1),xlim(2))-bt.win) max(1,min(bt.cursor(2),ylim(2))-bt.win) bt.win bt.win])
%     else
%         set(bt.rect,'visible','off')
    end
    bt.activeAxes = 1;
else
    bt.activeAxes = 0;
end

function mywbdf(src,evnt)
global bt
bt.pressed=0;
handles = guidata(src);

if bt.activeAxes==1 && bt.mode == 1
    bt.train.traingchanged=1;
    xlim=[1 size(bt.img,2)];
    ylim=[1 size(bt.img,1)];
    bt.train.coords(end+1,:) = [max(1,min(bt.cursor(1),xlim(2))-bt.win) max(1,min(bt.cursor(2),ylim(2))-bt.win)];

    if strcmp('normal',(get(gcf,'SelectionType')))
        % right mouse for cells
        rectangle('position',[bt.train.coords(end,:) bt.win bt.win],'Edgecolor',[1 1 0],'parent',handles.axesRawImage);
        bt.train.classes(end+1) = 0;
        bt.train.img{end+1} = bt.imglist{bt.imgid};
        bt.train.pos(end+1) = bt.pos;
        bt.train.iscalced(end+1) = 0;
        bt.train.features(end+1,:) = nan(1,getTilingFeatures([]));
        bt.activeRect = numel(bt.train.classes);
        updateCloseUp(handles);
    else
        % left for background
        rectangle('position',[bt.train.coords(end,:) bt.win bt.win],'Edgecolor',[0 0 1],'parent',handles.axesRawImage);
        bt.train.classes(end+1) = 1;
        bt.train.img{end+1} = bt.imglist{bt.imgid};
        bt.train.iscalced(end+1) = 0;
        bt.train.pos(end+1) = bt.pos;
        bt.train.features(end+1,:) = nan(1,getTilingFeatures([]));
        bt.activeRect = numel(bt.train.classes);
        updateCloseUp(handles);
    end
    updateText(handles)
else
    % check if contrast was clicked
    act= get(handles.axesContrast,'CurrentPoint');
    act=act(1,1:2);
    xlim=get(handles.axesContrast,'xlim');
    ylim=get(handles.axesContrast,'ylim');
    
    if xlim(1)<act(1) && act(1)<xlim(2) && ylim(1)<act(2) && act(2)<ylim(2)
        % drin
        if strcmp('normal',(get(gcf,'SelectionType')))
            if act(1)<bt.contrast(get(handles.popupmenuWavelength,'Value'),2)
                bt.contrast(get(handles.popupmenuWavelength,'Value'),1) = act(1);
                updateContrast(get(handles.popupmenuWavelength,'Value'),handles)
                % update
%                 redraw(handles)
            end
        else
            if act(2)>bt.contrast(get(handles.popupmenuWavelength,'Value'),1)
                bt.contrast(get(handles.popupmenuWavelength,'Value'),2) = act(1);
                updateContrast(get(handles.popupmenuWavelength,'Value'),handles)
                % update
%                 redraw(handles)
            end
        end

    end
end



function wbucb(src,evnt)
global bt
% handles = guidata(src);
bt.pressed=0;

function mykeyf(src,evnt)
global bt

handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

if strcmp(k,'backspace')
    ids = strcmp(bt.train.img,bt.imglist{bt.imgid});
    if any(ids)
        lastid = find(ids,1,'last');
        bt.train.coords(lastid,:) = [];
        bt.train.classes(lastid) = [];
        bt.train.img(lastid) = [];
        bt.train.iscalced(lastid) = [];
        bt.train.features(lastid,:) = [];
        bt.train.pos(lastid) = [];
        bt.activeRect = 0;
        redraw(handles);

    end
    
end

function updateText(handles)
global bt
set(handles.textStatus,'String',sprintf('Background tiles(blue = right mouse button) %d\nCell tiles (yellow = left mouse button) %d',sum(bt.train.classes == 1),sum(bt.train.classes == 0)))



function updateCloseUp(handles)
global bt
clr = [1 1 0;0 0 1];

if bt.mode == 1 && bt.activeRect~=0
    % sub = extractsubImage(bt.img,bt.train.coords(bt.activeRect,1),bt.train.coords(bt.activeRect,2),100);
    x=round(bt.train.coords(bt.activeRect,1));
    y=round(bt.train.coords(bt.activeRect,2));
    clr = clr (1+bt.train.classes(bt.activeRect),:);
    
elseif bt.mode==2 && bt.test.activeRect ~=0
    x=round(bt.test.coords(bt.test.activeRect,1));
    y=round(bt.test.coords(bt.test.activeRect,2));
    clr = clr (1+bt.test.classes(bt.test.activeRect),:);
else 
    x=1;
    y=1;
    return
end
sub=bt.img;
sub=sub(max(1,y-25):min(y+75,size(bt.img,1)),max(1,x-25):min(x+75,size(bt.img,2)));
imagesc(sub,'parent',handles.axesCloseUp,bt.contrast(get(handles.popupmenuWavelength,'Value'),:));
axis(handles.axesCloseUp,'off')
axis(handles.axesCloseUp,'equal')
rectangle('position',[25+(sign(x-25)==-1)*(x-25) 25+(sign(y-25)==-1)*(y-25) bt.win bt.win],'parent',handles.axesCloseUp,'edgecolor',clr)

function  parseXML(path,handles)
global bt
% read xml

d = dir([path '/*.xml']);
if isempty(d)
    errordlg('XML not found')
    return
end
xmlfilename = d(end).name;


xmlfile = fileread([path '/' xmlfilename]);
xmlstr =  xml_parseany(xmlfile); 


% check for four digit pos and 2 digit WL
firstdir = dir(path);
x=[firstdir.isdir];
k = find(x(3:end),1,'first');
firstdir = firstdir(k+2).name;

if numel(firstdir(strfind(firstdir,'_p')+2:end)) == 4
    bt.fourdigitpos=1;
else
    bt.fourdigitpos = 0;
end


%%% collect WLs
wllist={};
list = xmlstr.WavelengthData{1}.WavelengthInformation;
for k = 1:size(list,2)
    wllist{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name) '.' char(list{k}.WLInfo{1}.ATTRIBUTE.ImageType)];
end

% check two digit WL
if bt.fourdigitpos
    tocheck=[path '/' firstdir '/*' wllist{1}];
else
    tocheck=[path '/' firstdir '/*' wllist{1}];
end
d= dir(tocheck);
if numel(d)==0
    bt.twodigitWL = 0;
else
    bt.twodigitWL = 1;
end
if numel(strfind(wllist{1},'0'))==1
    bt.twodigitWL = 0;
end

% set wl dropdown
set(handles.popupmenuWavelength,'String',wllist)
% select first
set(handles.popupmenuWavelength,'Value',1)
% init all contrast
for k = 1:numel(wllist)
    bt.contrast(k,:) =[0 1];
end

popupmenuWavelength_Callback([],[],handles);



%%% collect positions
poslist={};
for k = 1:numel(xmlstr.PositionData{1}.PositionInformation)
    poslist{end+1} = [xmlstr.PositionData{1}.PositionInformation{k}.PosInfoDimension{1}.ATTRIBUTE.index ' [' xmlstr.PositionData{1}.PositionInformation{k}.PosInfoDimension{1}.ATTRIBUTE.comments ']'];
end
% set position drop down
set(handles.popupmenuPosition,'String',poslist)
set(handles.popupmenuPosition,'Value',1)
popupmenuPosition_Callback([],[],handles);

function  parseDirectory(path,handles)
global bt

bt.fourdigitpos=0;
bt.twodigitWL = 0;



% set wl dropdown
set(handles.popupmenuWavelength,'Enable','off')
set(handles.popupmenuWavelength,'String','noWLs')
% select first
set(handles.popupmenuWavelength,'Value',1)
% init all contrast
bt.contrast(1,:) =[0 1];


% popupmenuWavelength_Callback([],[],handles);
bt.wl = 1;
bt.wlext = '';
updateContrast(get(handles.popupmenuWavelength,'Value'),handles)

%%% collect positions

% set position drop down
set(handles.popupmenuPosition,'String','NoPos')
set(handles.popupmenuPosition,'Enable','off')
set(handles.popupmenuPosition,'Value',1)
popupmenuPosition_Callback([],[],handles);


function updateImageDropDown(handles)
% read log file of pos and update dropdown menu, the first entry is
% selected
global bt
% some inits?
set(handles.textStatus,'String','Updating Filestructure...')
% read log file of actual position
if bt.fourdigitpos
    logfile = sprintf('%s/%s_p%04d/%s_p%04d.log',bt.exppath,bt.exp,bt.pos,bt.exp,bt.pos);
else
    logfile = sprintf('%s/%s_p%03d/%s_p%03d.log',bt.exppath,bt.exp,bt.pos,bt.exp,bt.pos);
end

if ~exist(logfile,'file')
    errordlg(sprintf('Logfile %s not found. Cannot work.',logfile))
    set(handles.popupmenuImage,'string',{'Log file not found found'});
    set(handles.popupmenuImage,'Value',1);
    return
end

log = positionLogFileReader(logfile);
wl = bt.wl;
wlext = bt.wlext;
% filter for actual wavelength
imagelist=cell(1,sum(log.wavelength == wl));
counter=0;
for id = find(log.wavelength == wl)
    counter=counter+1;
    if log.zindex(id) == -1
        z='';
    else
        z=sprintf('_z%03d',log.zindex(id));
    end
    if bt.twodigitWL
        wlx=sprintf('_w%02d',wl);
    else
        wlx=sprintf('_w%01d',wl);
    end
    if bt.fourdigitpos
        imagelist{counter} = sprintf('%s_p%04d_t%05d%s%s.%s',bt.exp,bt.pos,log.timepoint(id),z,wlx,wlext);
    else
        imagelist{counter} = sprintf('%s_p%03d_t%05d%s%s.%s',bt.exp,bt.pos,log.timepoint(id),z,wlx,wlext);
    end
end
set(handles.textStatus,'String',sprintf('Found %d files.',numel(imagelist)));    
% update dropdown
set(handles.popupmenuImage,'string',imagelist);
set(handles.popupmenuImage,'Value',1);

% popupmenuImage_Callback([],[],handles);

function updateImageDropDownDirectory(handles)
% read log file of pos and update dropdown menu, the first entry is
% selected
global bt
% some inits?
set(handles.textStatus,'String','Updating Filestructure...')

d = dir([bt.exppath '/*.png']);

imagelist={d.name};

set(handles.textStatus,'String',sprintf('Found %d files.',numel(imagelist)));    
% update dropdown
set(handles.popupmenuImage,'string',imagelist);
set(handles.popupmenuImage,'Value',1);



function redraw(handles)
global bt
% cla(handles.axesRawImage);


imagesc(bt.img,'parent',handles.axesRawImage,bt.contrast(get(handles.popupmenuWavelength,'Value'),:))

clr = [1 1 0;0 0 1];

% draw all relevant rectangles
if bt.mode == 1
    % draw all already clicked rect
    set(handles.textStatus,'String',sprintf('Please select tiles:\nBackground = right mouse click\nCells = left mouse click'))
    imghasrect = strcmp(bt.imglist{bt.imgid},bt.train.img);
    if any(imghasrect)
        for k = find(imghasrect)
            rectangle('position',[bt.train.coords(k,1) bt.train.coords(k,2) bt.win bt.win],'Edgecolor',clr(bt.train.classes(k)+1,:),'parent',handles.axesRawImage);
        end
    end
elseif bt.mode == 2
    if ~strcmp(bt.imglist{bt.imgid},bt.test.img{1})
        pushbuttonModeAL_Callback([],[],handles)
    end
        
    
    set(handles.textStatus,'String',sprintf('Does this tile contain background only?\nBackground tiles %d\nCell tiles %d',sum(bt.train.classes==1),sum(bt.train.classes==0)))
    lw=[1 3];
    % draw rect of AL batch
    for k = bt.test.batch
        rectangle('position',[bt.test.coords(k,1) bt.test.coords(k,2) bt.win bt.win],'Edgecolor',clr(bt.test.classes(k)+1,:),'parent',handles.axesRawImage,'linewidth',lw(1+1*(k==bt.test.activeRect)));
    end
elseif bt.mode == 3
%     imghasresult = strcmp(bt.result.img,bt.imglist{bt.imgid});

    if ~ismember(bt.imglist{bt.imgid},bt.result.img)
        prepareImageFeatures();
        
    end
    id = strcmp(bt.result.img,bt.imglist{bt.imgid});
    if isempty(bt.result.classes{id})
        calcResult(handles);
    end
     
    if get(handles.radiobuttonShowInterpolation,'Value')
        if isempty(bt.result.ZI{id})
            interpolate(handles)
        end
            
        imagesc(bt.result.ZI{id},'parent',handles.axesRawImage,bt.contrast(get(handles.popupmenuWavelength,'Value'),:))

    end
    
    if get(handles.checkboxShowTiles,'Value')
        for k = find(bt.result.classes{id}==get(handles.radiobuttonShowBackground,'Value'))'
            rectangle('position',[bt.result.coords{id}(k,1) bt.result.coords{id}(k,2) bt.win bt.win],'Edgecolor',clr(get(handles.radiobuttonShowBackground,'Value')+1,:),'parent',handles.axesRawImage);
        end
    end

    set(handles.textStatus,'String',sprintf('Showing result.\nClassifier used:\nBackground tiles %d\nCell tiles %d',sum(bt.train.classes==1),sum(bt.train.classes==0)));

    
end
colormap(gray(64));
axis(handles.axesRawImage,'off')
axis(handles.axesRawImage,'equal')
if bt.mode == 1
    bt.rect= rectangle('position',[1 1 bt.win bt.win],'Edgecolor',[1 0 0],'parent',handles.axesRawImage);
end

cla(handles.axesCloseUp)
axis(handles.axesCloseUp,'off')
axis(handles.axesCloseUp,'equal')
updateCloseUp(handles)

% --- Outputs from this function are returned to the command line.
function varargout = BackgroundTool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbuttonRandomImage.
function pushbuttonRandomImage_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonRandomImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
validpos = get(handles.popupmenuPosition,'String');
set(handles.popupmenuPosition,'Value',randi(size(validpos,1),1,1))
bt.getrandom =1;
popupmenuPosition_Callback([],[],handles);
validimg = get(handles.popupmenuImage,'String');
set(handles.popupmenuImage,'Value',randi(size(validimg,1),1,1))
popupmenuImage_Callback([],[],handles);




% --- Executes on button press in pushbuttonInterpolate.
function pushbuttonInterpolate_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonInterpolate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
interpolate(handles)
redraw(handles);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbuttonYesIsBackground.
function pushbuttonYesIsBackground_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonYesIsBackground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
if isempty(bt.test.batch)
    return
end
% set correct label
bt.test.classes(bt.test.activeRect) = 1;

% update test and train set

updateTestAndTrain(handles)

function updateTestAndTrain(handles)
global bt
bt.train.classes(end+1) = bt.test.classes(bt.test.activeRect);
bt.train.coords(end+1,:) = bt.test.coords(bt.test.activeRect,:);
bt.train.features(end+1,:) = bt.test.features(bt.test.activeRect,:);
bt.train.img{end+1} = bt.test.img{bt.test.activeRect};
bt.train.iscalced(end+1) = bt.test.iscalced(bt.test.activeRect);
bt.train.pos(end+1) = bt.test.pos(bt.test.activeRect);

bt.test.classes(bt.test.activeRect)=[];
bt.test.coords(bt.test.activeRect,:)=[];
bt.test.features(bt.test.activeRect,:)=[];
bt.test.img(bt.test.activeRect)=[];
bt.test.iscalced(bt.test.activeRect)=[];
bt.test.pos(bt.test.activeRect)=[];

bt.test.batch(bt.test.batch>bt.test.batch(1)) = bt.test.batch(bt.test.batch>bt.test.batch(1))-1;
bt.test.batch(1) = [];


bt.train.traingchanged=1;
% update view
if isempty(bt.test.batch)
    % update active learning
    bt.test.activeRect = 0;
    redraw(handles);
    pushbuttonModeAL_Callback([],[],handles);
else
    bt.test.activeRect=bt.test.batch(1);
    redraw(handles);
end



% --- Executes on button press in pushbuttonNoBackground.
function pushbuttonNoBackground_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonNoBackground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
if isempty(bt.test.batch)
    return
end

% set correct label
bt.test.classes(bt.test.activeRect) = 0;

% update test and train set

updateTestAndTrain(handles)


% --- Executes on button press in pushbuttonModeResult.
function pushbuttonModeResult_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonModeResult (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt

set(handles.uipanelDisplaySettings,'visible','on')
set(handles.pushbuttonYesIsBackground,'visible','off')
set(handles.pushbuttonNoBackground,'visible','off')
set(handles.pushbuttonSaveBackground,'visible','on')
set(handles.pushbuttonCalculateAll,'visible','on')


set(handles.pushbuttonModeSelect,'Value',0)
set(handles.pushbuttonModeResult,'Value',1)
set(handles.pushbuttonModeAL,'Value',0)
set(handles.figure1,'name','BackgroundTool: Results')
bt.mode = 3;
if bt.train.traingchanged
    train
end
redraw(handles);


function prepareImageFeatures()
global bt


% get feature of img
[featuremat, illu] = getImageFeaturesSelected(bt.img,bt.win,bt.train.usefeatures);

bt.result.img{end+1} = bt.imglist{bt.imgid};
bt.result.coords{end+1} = illu(:,2:-1:1);
bt.result.features{end+1} = featuremat;
bt.result.mean{end+1} = illu(:,3);
bt.result.pos{end+1} = bt.pos;
bt.result.classes{end+1} = [];
bt.result.ZI{end+1} = [];

function calcResult(handles)

set(handles.textStatus,'String','Running classification')

global bt
id = strcmp(bt.result.img,bt.imglist{bt.imgid});
% classify
[testclasses,scores] = predict(bt.train.rf, bt.result.features{id}(:,bt.train.usefeatures));
bt.result.classes{id} = strcmp(testclasses,'1');
% bt.result.classes{id} = scores(:,1)<.2;


function interpolate(handles)
global bt
set(handles.textStatus,'String','Interpolating.')
try
    set(handles.figure1,'Pointer','watch')
    drawnow
    
    id = strcmp(bt.result.img,bt.imglist{bt.imgid});
    x=bt.result.coords{id}(bt.result.classes{id}==1,1)+bt.train.win/2;
    y=bt.result.coords{id}(bt.result.classes{id}==1,2)+bt.train.win/2;
    z=bt.result.mean{id}(bt.result.classes{id}==1);
    imgwidth=size(bt.img,2);
    imghight=size(bt.img,1);
    
    %interpolate
    fprintf('Interpolating...\n')
    
    [XI,YI] = meshgrid(1:imgwidth,1:imghight);
    % F=TriScatteredInterp(x,y,z,'natural');
    F=scatteredInterpolant(x,y,z,'natural');
    ZI=F(XI,YI);
    
    
    %%%% OLD CODE
%     %interpolate
%     [XI YI] = meshgrid(1:imgwidth,1:imghight);
%     F=TriScatteredInterp(x,y,z,'natural');
%     set(handles.textStatus,'String','Interpolating..')
%     drawnow
%     ZI=F(XI,YI);
%     set(handles.textStatus,'String','Interpolating...')
%     drawnow
%     % extrapolate
%     for i=find(sum(~isnan(ZI(1:imghight,:)))>1)
%         ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
%     end
%     
%     for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
%         ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
%     end
    %%%% END  OLD CODE
    
    bt.result.ZI{id} = ZI;
    set(handles.textStatus,'String','Interpolation done.')
catch e
    set(handles.textStatus,'String','Error during interpolation.')
end
set(handles.figure1,'Pointer','arrow')



% --- Executes on selection change in popupmenuWavelength.
function popupmenuWavelength_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuWavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
wllist = get(handles.popupmenuWavelength,'String');
% split = strsplit('.',wllist{get(handles.popupmenuWavelength,'Value')});
[~,part2,part3]=fileparts(wllist{get(handles.popupmenuWavelength,'Value')});
% bt.wl = str2double(split{1}(2:end));
bt.wl = str2double(part2(2:end));
bt.wlext = part3(2:end);
if bt.pos~=0
    updateImageDropDown(handles);
    popupmenuImage_Callback([],[],handles)
end
updateContrast(get(handles.popupmenuWavelength,'Value'),handles)

function updateContrast(channel,handles)
% after WL was switched
global bt
cla(handles.axesContrast)
set(handles.axesContrast,'xlim',[0 1])
set(handles.axesContrast,'yticklabel',[])
% set(handles.axesContrast,'xticklabel',[])
line(bt.contrast(channel,:),[0 max(get(handles.axesContrast,'ylim'))],'Linewidth',3,'color','black','Parent',handles.axesContrast)
line([bt.contrast(channel,2) bt.contrast(channel,2)],[0 max(get(handles.axesContrast,'ylim'))],'Linewidth',3,'color','black','parent',handles.axesContrast)
line([bt.contrast(channel,1) bt.contrast(channel,1)],[0 max(get(handles.axesContrast,'ylim'))],'Linewidth',3,'color','black','parent',handles.axesContrast)
caxis(handles.axesRawImage,bt.contrast(channel,:));
caxis(handles.axesCloseUp,bt.contrast(channel,:));
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuWavelength contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuWavelength


% --- Executes during object creation, after setting all properties.
function popupmenuWavelength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuWavelength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuPosition.
function popupmenuPosition_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
poslist=  get(handles.popupmenuPosition,'String');

if strcmp(poslist,'NoPos')
    bt.pos = 1;
    updateImageDropDownDirectory(handles);
else
    bt.pos = str2double(poslist{get(handles.popupmenuPosition,'Value')}(1:4));
    updateImageDropDown(handles);
end
if bt.getrandom == 1
    bt.getrandom =0;
else
    popupmenuImage_Callback([],[],handles);
end
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuPosition contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuPosition


% --- Executes during object creation, after setting all properties.
function popupmenuPosition_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuImage.
function popupmenuImage_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
bt.imgid = get(handles.popupmenuImage,'Value');
bt.imglist = get(handles.popupmenuImage,'String');
% read new img
loadBTimage();

redraw(handles);
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuImage contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuImage

function loadBTimage
global bt
%%% TODO: store this in some struct for later
try
    if strcmp(bt.imglist{bt.imgid},'Log file not found found')
        bt.img=[];
    elseif bt.directoryonly 
        bt.img = loadimage([bt.exppath '/'  bt.imglist{bt.imgid}],0);
    else
        if bt.fourdigitpos
            bt.img = loadimage([bt.exppath '/' sprintf('%s_p%04d/',bt.exp,bt.pos) bt.imglist{bt.imgid}],0);
        else
            bt.img = loadimage([bt.exppath '/' sprintf('%s_p%03d/',bt.exp,bt.pos) bt.imglist{bt.imgid}],0);
        end
    end
catch e
    bt.img=[];
end

% --- Executes during object creation, after setting all properties.
function popupmenuImage_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonModeModify.
function pushbuttonModeModify_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonModeModify (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbuttonModeSelect.
function pushbuttonModeSelect_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonModeSelect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
set(handles.pushbuttonModeSelect,'Value',1)
set(handles.pushbuttonModeResult,'Value',0)
set(handles.pushbuttonModeAL,'Value',0)
set(handles.figure1,'name','BackgroundTool: Select / Train')
set(handles.uipanelDisplaySettings,'visible','off')
set(handles.pushbuttonYesIsBackground,'visible','off')
set(handles.pushbuttonNoBackground,'visible','off')
set(handles.pushbuttonSaveBackground,'visible','off')
set(handles.pushbuttonCalculateAll,'visible','off')


bt.activeRect = 0;
bt.mode = 1;
redraw(handles);

% --- Executes on button press in pushbuttonModeAL.
function pushbuttonModeAL_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonModeAL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
set(handles.uipanelDisplaySettings,'visible','off')
set(handles.pushbuttonModeSelect,'Value',0)
set(handles.pushbuttonModeResult,'Value',0)
set(handles.pushbuttonYesIsBackground,'visible','on')
set(handles.pushbuttonNoBackground,'visible','on')
set(handles.pushbuttonSaveBackground,'visible','off')
set(handles.pushbuttonCalculateAll,'visible','off')

set(handles.pushbuttonModeAL,'Value',1)
set(handles.figure1,'name','BackgroundTool: Active Learning')
bt.mode = 2;
getAllTilingFeatures(handles)

function getAllTilingFeatures(handles)
global bt

bt.wh = waitbar(0,'Getting Features...');

if bt.train.traingchanged
    train()
    
end

%%%%TODO only smaple if not already sampled?
% clear testset
bt.test.coords=[];
bt.test.classes=[];
bt.test.img = {};
bt.test.iscalced = [];
bt.test.features = [];
bt.test.pos = [];

%sample
sampleTiles();



waitbar(0,bt.wh,'Applying classifier...')
[testclasses,scores, stdev] = predict(bt.train.rf, bt.test.features(:,bt.train.usefeatures));
bt.test.classes = strcmp(testclasses,'1');


waitbar(1,bt.wh,'Preparing active learning')
alpha = 2/3;
batchsize=10;
% find index of rare class
cn = numel(bt.train.rf.ClassNames);
cc = zeros(1, cn);
for c = 1:cn
    cc(c) = sum(strcmp(bt.train.rf.Y, bt.train.rf.ClassNames{c}));
end
[~, rare] = min(cc);

% sort stdev values q
q = stdev(:, rare);
sq = sort(q, 'descend');
unlabNum = size(bt.test.features, 1);
q0 = sq(floor(alpha*unlabNum) + 1);

% update text
% set(handles.textStatus,'String',sprintf('Highest uncertainty %3.2f',sq(1)))

% calculate sampling distrbution L
L = (q - q0) / (sq(1) - q0);
L = max(L, 0);
L = L / sum(L);

% sample batch
batch = zeros(1, min(batchsize, c));
for i = 1:batchsize
    % sample one item from L
    r = rand();
    cumprob = [ 0; cumsum(L) ];
    for j = 1:numel(L)
        if cumprob(j) < r && r <= cumprob(j + 1)
            batch(i) = j;
            break;
        end
    end
    
    % set prob to zero and renormalize
    L(j) = 0;
    L = L / sum(L);
end

bt.test.batch = batch;

try
delete(bt.wh)
catch
end
bt.test.activeRect = batch(1);
redraw(handles);


function train()
global bt
closeit=0;
if ~ishandle(bt.wh)
    bt.wh = waitbar(0,'Getting Features...');
    closeit=1;
end

if any(~bt.train.iscalced)
    % iterate over images
    
    relevantimages = unique(bt.train.img(~bt.train.iscalced));
    counter=0;
    for f =1:numel(relevantimages)
        relevantTiles = find(strcmp(bt.train.img,relevantimages{f}) & ~bt.train.iscalced);
        
        for t = relevantTiles
            counter=counter+1;
            waitbar(counter/sum(~bt.train.iscalced),bt.wh)
            % load img
            if bt.directoryonly
                img = loadimage([bt.exppath '/' relevantimages{f}],0);
            elseif bt.fourdigitpos
                img = loadimage([bt.exppath '/' sprintf('%s_p%04d/',bt.exp,bt.train.pos(t)) relevantimages{f}],0);
            else
                img = loadimage([bt.exppath '/' sprintf('%s_p%03d/',bt.exp,bt.train.pos(t)) relevantimages{f}],0);
            end
            x = round(bt.train.coords(t,1));
            y = round(bt.train.coords(t,2));
            sub = img(y:y+bt.win,x:x+bt.win);
            % get features
            bt.train.features(t,:) = getTilingFeatures(sub);
            bt.train.iscalced(t) =1;
            bt.train.traingchanged=1;
        end
        
    end
    
end
fprintf('Creating new Classifier\n')
waitbar(1,bt.wh,'Training classifier...')
matlabversion = version('-release');
matlabversion = str2double(matlabversion(1:4));
if verLessThan('matlab','8.1')
    bt.train.rf = TreeBagger(1000,bt.train.features(:,bt.train.usefeatures),bt.train.classes,'priorprob','equal','weights',bt.train.classes+1,'oobpred','on');
else
    bt.train.rf = TreeBagger(1000,bt.train.features(:,bt.train.usefeatures),bt.train.classes,'Prior','uniform','weights',bt.train.classes+1,'oobpred','on'); % 'oobvarimp','on'
end
bt.train.traingchanged=0;

for k = 1:numel(bt.result.classes)
    bt.result.classes{k} = [];
    bt.result.ZI{k} = [];
end

if closeit
    delete(bt.wh)
end

function sampleTiles()
global bt

waitbar(0,bt.wh,'Sampling new tiles from image...');



imgwidth=size(bt.img,2);
imghight=size(bt.img,1);

counter = 0;
for k = 1:100
    counter=counter+1;
    waitbar(counter/100,bt.wh)
    r = rand(1,2);
    x=round(r(1)*imgwidth);
    y=round(r(2)*imghight);
    works=0;
    try
        sub = bt.img(y:y+bt.win, x:x+bt.win);
        features =getTilingFeatures(sub);
        works=1;
    catch e
        
    end
    
    if works
        
        % get feature and store
        bt.test.coords(end+1,:)=[x y];
        bt.test.classes(end+1)=nan;
        bt.test.img{end+1} = bt.imglist{bt.imgid};
        bt.test.iscalced(end+1) = 1;
        bt.test.features(end+1,:) = features;
        bt.test.pos(end+1) = bt.pos;
    end
end



function editTileSize_Callback(hObject, eventdata, handles)
% hObject    handle to editTileSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
% Hints: get(hObject,'String') returns contents of editTileSize as text
%        str2double(get(hObject,'String')) returns contents of editTileSize as a double
a=questdlg('Changing the window size will reset your classifier. Do you want do continue?');

if numel(a) && strcmp(a(1),'Y')
    % update window size
    newwin = get(hObject,'String');
    newwin = strrep(newwin,'px','');
    bt.win = str2double(newwin);
    
    % reset classifier
    bt.train.coords=[];
    bt.train.classes=[];
    bt.train.img = {};
    bt.train.iscalced = [];
    bt.train.features = [];
    bt.train.pos = [];
    bt.train.traingchanged=0;
    bt.train.win = bt.win;

    
    % go to selection mode
    pushbuttonModeSelect_Callback([],[],handles);
end

% --- Executes during object creation, after setting all properties.
function editTileSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTileSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuShowResult.
function popupmenuShowResult_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuShowResult (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuShowResult contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuShowResult
global bt
bt.showresult = get(hObject,'Value');
redraw(handles);

% --- Executes during object creation, after setting all properties.
function popupmenuShowResult_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuShowResult (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobuttonShowBackground.
function radiobuttonShowBackground_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonShowBackground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobuttonShowForeground,'Value',0)
% Hint: get(hObject,'Value') returns toggle state of radiobuttonShowBackground
redraw(handles)

% --- Executes on button press in radiobuttonShowForeground.
function radiobuttonShowForeground_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonShowForeground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobuttonShowBackground,'Value',0)
% Hint: get(hObject,'Value') returns toggle state of radiobuttonShowForeground
redraw(handles)

% --- Executes on button press in checkboxShowTiles.
function checkboxShowTiles_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShowTiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redraw(handles)
% Hint: get(hObject,'Value') returns toggle state of checkboxShowTiles


% --- Executes on button press in radiobuttonShowOriginal.
function radiobuttonShowOriginal_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonShowOriginal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobuttonShowInterpolation,'Value',0)
% Hint: get(hObject,'Value') returns toggle state of radiobuttonShowOriginal
redraw(handles)

% --- Executes on button press in radiobuttonShowInterpolation.
function radiobuttonShowInterpolation_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonShowInterpolation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobuttonShowOriginal,'Value',0)
redraw(handles)
% Hint: get(hObject,'Value') returns toggle state of radiobuttonShowInterpolation


% --- Executes on button press in pushbuttonSaveBackground.
function pushbuttonSaveBackground_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonSaveBackground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt

id = strcmp(bt.result.img,bt.imglist{bt.imgid});
if isempty(id)
    return
end
% interpolate it
if isempty(bt.result.ZI{id})
    interpolate(handles)
end
suggest=bt.imglist{bt.imgid};
if exist([bt.exppath '/background/' sprintf('%s_p%04d/',bt.exp,bt.pos)],'dir')
    
    
    [FileName,PathName] = uiputfile('*.png','Enter filename to store background',[bt.exppath '/background/' sprintf('%s_p%04d/',bt.exp,bt.pos) suggest]);
else
    %%%% TODO : andpassen auf_background
    [FileName,PathName] = uiputfile('*.png','Enter filename to store background',[bt.exppath  '/' suggest]);
end

if PathName == 0
    return
end

imwrite(im2uint16(bt.result.ZI{id}),[PathName '/' FileName],'bitdepth',8)

% --- Executes on button press in pushbuttonSaveClassifier.
function pushbuttonSaveClassifier_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonSaveClassifier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
suggest = sprintf('%s_w%02d_classifier.mat',bt.exp,bt.wl);
[FileName,PathName] = uiputfile('*.mat','Enter filename to store classifier',[bt.exppath '/' suggest]);
if PathName == 0
    return
end
train = bt.train;
save([PathName '/' FileName],'train');

% --- Executes on button press in pushbuttonLoadClassifier.
function pushbuttonLoadClassifier_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonLoadClassifier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
suggest = sprintf('%s_w%02d_classifier.mat',bt.exp,bt.wl);
[FileName,PathName] = uigetfile('*.mat','Enter filename to load classifier',[bt.exppath '/' suggest]);
if PathName == 0
    return
end
train= load([PathName '/' FileName],'-mat','train');
bt.train = train(1).train;
bt.train.traingchanged=1;
bt.win = bt.train.win;
set(handles.editTileSize,'String',[num2str(bt.win) ' px']);
redraw(handles);
set(handles.textStatus,'String','Classifier loaded.')


% --- Executes on button press in pushbuttonAutoContrast.
function pushbuttonAutoContrast_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonAutoContrast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt
data=bt.img;
ca=[quantile(data(:),.01) quantile(data(:),.99)];
bt.contrast(get(handles.popupmenuWavelength,'Value'),:)=ca;
updateContrast(get(handles.popupmenuWavelength,'Value'),handles)


% --- Executes on button press in pushbuttonCalculateAll.
function pushbuttonCalculateAll_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonCalculateAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bt

% select a folder where to store the background
bgfolder=uigetdir(bt.exppath,'Select folder where to store the background images');

if bgfolder==0
    return
end

if exist([bgfolder '/'  bt.imglist{1}],'file')
    a=questdlg(sprintf('Background files will have the same filename as their originals.\nAll files in this directory will be overwritten. Do you want to continue?'),'Overwrite?');
    if ~strcmp(a,'Yes')
        return
    end
end

oldid = bt.imgid;
% calc all images within a path
wh = waitbar(0,sprintf('Estimating background for %d images...',numel(bt.imglist)));
temp=get(wh,'Position');
set(wh,'position',[temp(1) temp(2)+temp(4) temp(3:4)]);
for id= 1:numel(bt.imglist)
    waitbar(id/numel(bt.imglist),wh,sprintf('Estimating background %d of %d images...',id,numel(bt.imglist)))
    %load image
    bt.imgid=id;
    loadBTimage();
    % get feature of img
    [featuremat, illu] = getImageFeaturesSelected(bt.img,bt.win,bt.train.usefeatures);
    coords = illu(:,2:-1:1);
    means = illu(:,3);
    % classify
    [testclasses] = predict(bt.train.rf, featuremat(:,bt.train.usefeatures));
    classes = strcmp(testclasses,'1');
    
    % init interpolate
    x=coords(classes==1,1)+bt.train.win/2; %select center coordinate
    y=coords(classes==1,2)+bt.train.win/2; %select center coordinate
    z=means(classes==1);
    imgwidth=size(bt.img,2);
    imghight=size(bt.img,1);
    
    %interpolate
    [XI,YI] = meshgrid(1:imgwidth,1:imghight);
    % F=TriScatteredInterp(x,y,z,'natural');
    F=scatteredInterpolant(x,y,z,'natural');
    ZI=F(XI,YI);

    
    
    % save BG
    imwrite(ZI, [bgfolder '/'  bt.imglist{bt.imgid}],'png','Bitdepth',8);
end

bt.imgid=oldid;
close(wh)

