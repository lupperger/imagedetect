%% get all std devs for predictions

[testclasses,scores,stddevs]= rf.predict(testmat);
testclasses = strcmp(testclasses,'1');

[~,order]=sort(stddevs(:,1),'descend');

%% sample the next batch:

alpha = 2/3;
nu = numel(testclasses);
L = zeros(1,round(numel(testclasses)*alpha));
q0 = stddevs(order(round(alpha * nu)),1);
for x = 1:nu*alpha
    L(x) = (stddevs(order(x),1)-q0) / (stddevs(order(1),1)-q0);
    
end

p_sel = L ./sum(L);
% figure;
% plot(p_sel)
nextbatch = randsample(numel(p_sel),10,true,p_sel)';

% or do inversion of cumsum of p_sel
%% show top 10 candidates in image

binsize = sbt.win;
figure;
imagesc(img)
hold on
colormap(gray)
axis off
axis equal
y=illu(:,1);
x=illu(:,2);
% nextbatch=order(1:10)';
for i = nextbatch
    if testclasses(i) ==1
        rectangle('position',[x(i)-binsize/2 y(i)-binsize/2 binsize binsize],'EdgeColor',[0 0 1])
    else
        rectangle('position',[x(i)-binsize/2 y(i)-binsize/2 binsize binsize],'EdgeColor',[1 1 0])
    end
    text(x(i),y(i),sprintf('%3.2f',stddevs(i,1)),'color',[1 0 0])
end

%% now ask for these labels

% just simple question
figure('position',[34   242   560   420]);
ALclasses=[];
counter=0;
for i = nextbatch
    sub = img( y(i)-binsize/2:y(i)-binsize/2 + binsize , x(i)-binsize/2:x(i)-binsize/2 + binsize);
    % show subimg
%     subplot(1,2,1)
    imagesc(sub)
    axis off
    axis equal
    colormap(gray)
    button = questdlg(sprintf('is this background?\n'),'background?','Yes','No','Cancel','Yes');
    
    if strcmp(button(1),'Y')
        counter=counter+1;
        ALclasses(counter)=1;
    elseif strcmp(button(1),'C')
        delete(h)
        return;
    else
        counter=counter+1;
        ALclasses(counter)=0;
    end
end

%% get new features

ALtrainingmat = getBackgroundTileParams(img,[x(1:10) y(1:10)],sbt.win);
trainingmat = [trainingmat;ALtrainingmat];
trainclasses =[trainclasses ALclasses];

