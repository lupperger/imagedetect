function [featuremat, illu] = getImageFeaturesSelected(image,binsize,idx)

imgsize=size(image);
% imgwidth=size(image,2);
% imghight=size(image,1);
tic
featuremat = zeros(round(imgsize(1)/binsize) * round(imgsize(2)/binsize),getTilingFeatures([]));
illu=zeros(round(imgsize(1)/binsize) * round(imgsize(2)/binsize),3);
counter = 0;
% wh  = waitbar(0,'Getting image features...');
for i = 1:binsize:imgsize(1)
%     waitbar(i/imgsize(1),wh);
    for j=1:binsize:imgsize(2)
        counter = counter+1;
        if i+binsize>imgsize(1)
            i = imgsize(1)-binsize;
        end
        if j+binsize>imgsize(2)
            j = imgsize(2)-binsize;
        end
        
        sub = image(i:i+binsize, j:j+binsize);
        try
        featuremat(counter,:) = getTilingFeatures(sub,idx);
%         sub = image(i+5:i+binsize-5, j+5:j+binsize-5);
        illu(counter,:)=[i,j, mean(sub(:))];
        catch me
            fprintf('ERROR: Failed to calc sub features\n')
        end
    end
end


toc
% delete(wh)