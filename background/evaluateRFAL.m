%% train a RF on some images, do AL then eval



uniFileNames = unique(trainFilename);
ids = randi(numel(uniFileNames),1,5)
mytrainIds=nan(1,numel(trainclasses));

for i = 1:numel(trainclasses)
    match = 0;
    for j = ids
        match = match || strcmp(trainFilename{i},uniFileNames{j});
    end
    
    
    mytrain(i) = match;
end

sum(mytrain)

%% train RF
fboot = .6;
rf = TreeBagger(1000,trainingmat(mytrain == 1,:),trainclasses(mytrain ==1),'FBoot', fboot);

% confusionmat(trainclasses(mytrain==1),double(strcmp(rf.oobPredict,'1')))

%% predict:
[testclasses scores]= rf.predict(trainingmat(mytrain==0,:));
testclasses = strcmp(testclasses,'1');
% testclasses = scores(:,1)<.1;
theyshouldbe = trainclasses(mytrain==0)';
confusionmat(double(testclasses'),double(theyshouldbe'))

fpr  = (sum(testclasses == 1 & theyshouldbe == 0 ) ) / (sum((testclasses == 1 & theyshouldbe == 0 )) + sum((testclasses == 0 & theyshouldbe == 0 )))

%% AL:
batch = sqbf(trainingmat(mytrain==1,:), trainclasses(mytrain ==1), trainingmat(mytrain ==0,:), 10);

mytrain(batch) = 1;
sum(mytrain)

%% show distribution of scores for TP und FP

figure;
hold on
[f,xi] = ksdensity(scores(testclasses == 1 & theyshouldbe == 0,1));
plot(xi,f,'r')
[f,xi] = ksdensity(scores(testclasses == 1 & theyshouldbe == 1,1));
plot(xi,f,'b')

%% show false positives in image

mycoords=allcoords(mytrain ==0,:);
myfiles = trainFilename(mytrain == 0);
figure;
imagesc(siq.img)
hold on
colormap(gray)
axis off
axis equal
y=mycoords(:,1);
x=mycoords(:,2);
% nextbatch=order(1:10)';
for i = find(testclasses == 1 & theyshouldbe == 0 )'
    if strcmp(myfiles{i},siq.filename{1})
        if testclasses(i) ==1
            rectangle('position',[x(i)-binsize/2 y(i)-binsize/2 binsize binsize],'EdgeColor',[0 0 1])
        else
            rectangle('position',[x(i)-binsize/2 y(i)-binsize/2 binsize binsize],'EdgeColor',[1 1 0])
        end
        %     text(x(i),y(i),sprintf('%3.2f',stddevs(i,1)),'color',[1 0 0])
    end
end