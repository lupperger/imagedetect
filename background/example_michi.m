% features
table = randn(20, 10); 

% classlabels of rows in table, 0 = no class label
labels = zeros(20, 1);
labels(1) = 1;
labels(8) = 1;
labels(10) = 2;

% index of labeled, unlabeled rows
lab = (find(labels > 0));
unl = (find(labels == 0));

X = table(lab, :);
Y = labels(lab, :);
Z = table(unl, :);

batch = sqbf(X, Y, Z, 10);