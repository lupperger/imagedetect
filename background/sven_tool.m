function varargout = sven_tool(varargin)
% SVEN_TOOL MATLAB code for sven_tool.fig
%      SVEN_TOOL, by itself, creates a new SVEN_TOOL or raises the existing
%      singleton*.
%
%      H = SVEN_TOOL returns the handle to a new SVEN_TOOL or the handle to
%      the existing singleton*.
%
%      SVEN_TOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SVEN_TOOL.M with the given input arguments.
%
%      SVEN_TOOL('Property','Value',...) creates a new SVEN_TOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sven_tool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sven_tool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sven_tool

% Last Modified by GUIDE v2.5 26-Feb-2014 10:54:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sven_tool_OpeningFcn, ...
                   'gui_OutputFcn',  @sven_tool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sven_tool is made visible.
function sven_tool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sven_tool (see VARARGIN)

% Choose default command line output for sven_tool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
global data
set(hObject,'KeyPressFcn',@myKeyFunction);
set(handles.listbox1,'KeyPressFcn',@myKeyFunction);
data.figurehandle = handles.figure1;
data.functions.show_bg = @pushbutton1_Callback;
data.functions.redraw = @redraw;
data.handles = handles


data.axes2isfilled = 0;
data.settings.contrastMin = 0;
data.settings.contrastMax = 1;
data.colorbar = 0;
data.imagefeatures = 0;
data.updatefeatures = 1;

imageCache('init',50)

%sliderMin = 0
%sliderMax = 50
%sliderStep = [1, 1] / (sliderMax - sliderMin)
%set(handles.slider1, 'SliderStep', sliderStep);


% UIWAIT makes sven_tool wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = sven_tool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
global data
data.updatefeatures = 1;

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double



% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double



% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data

readparams(data.handles)

tic
try
%[data.ZI counter] = backgroundestimation(data.img, data.settings);
if data.updatefeatures
    [data.ZI counter imagefeatures] = backgroundestimation_modified(data.img, data.settings, handles, 0, data.imagefeatures);
    data.imagefeatures = imagefeatures;   
else
   [data.ZI counter imagefeatures] = backgroundestimation_modified(data.img, data.settings, handles, 1, data.imagefeatures); 
end
data.updatefeatures = 0;

catch e
    try
        data.ZI = zeros(size(data.img));
    catch e
        warndlg('No Image selected')
        return
    end
    data.updatefeatures = 0;
    warndlg('The settings you have made do not deliver a useful background image. Hint: increase dispersion?')
    return
end

elapsed = toc;


redraw()

function readparams(handles)
global data

data.settings.kurtosis= str2double(get(handles.edit2,'String'));
data.settings.skewness= str2double(get(handles.edit3,'String'));
data.settings.winsize= str2double(get(handles.edit1,'String'));
data.settings.dispersion= str2double(get(handles.edit4,'String'));




function redraw
global data

handles=guidata(data.figurehandle);

ZI = data.ZI;
img = data.img;

axes(handles.axes1);
muh1=subplot(handles.axes1);

%imagesc(img);
imagesc(img,[data.settings.contrastMin data.settings.contrastMax])
axis off
axis equal
if data.colorbar
    colorbar
end
% datacursormode on

if numel(data.ZI)
    axes(handles.axes2);
    muh2=subplot(handles.axes2);
    imagesc(ZI,[data.settings.contrastMin data.settings.contrastMax])
    %imagesc(ZI);
    data.axes2isfilled = 1
    axis off
    axis equal
%     colorbar
    colormap(jet(256))
    
    
   
    linkaxes([muh1 muh2],'xy')

end




% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, ~, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data
data.wlength = get(handles.edit5,'String')


if isfield(data,'pathname')
    path_s = regexp(data.pathname,'\','split');
    size_path = size(path_s);
    path = '';
    for i=1:1:size_path(2)-1
        path = [path  path_s{i} '\'];
    end
    data.pathname = uigetdir([path]);
else
    data.pathname = uigetdir('*.*');
end
 
wh = waitbar(0,'Load image list');
list = dir(data.pathname)
w_length = 'w09'
x=1
list_text = ''
for i=1:1:size(list)
    if ((~isempty(findstr(list(i).name, data.wlength))) & (isempty(findstr(list(i).name, '.xml'))) )          
            list_text{x} = list(i).name  
            x = x+1
    end   
    size_list = size(list);
    waitbar(i/size_list(1),wh);
end
set(handles.listbox1,'String',  list_text)
close(wh);


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
global data
data.updatefeatures = 1;
lb_str = get(handles.listbox1, 'string');
selected = get(handles.listbox1, 'value');
data.img = loadimage([data.pathname '\' lb_str{selected}],0);
subplot(handles.axes1)
imagesc(data.img, [data.settings.contrastMin data.settings.contrastMax]);
axis off
axis equal

if(~ (data.axes2isfilled))
    subplot(handles.axes2)
    data.ZI = zeros(size(data.img));
    imagesc(data.ZI, [data.settings.contrastMin data.settings.contrastMax]);
    axis off
    axis equal
end

    





% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
global data
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function myKeyFunction(src, evnt)
global data;

handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

if strcmp(k,'return') %if enter was pressed
   disp('return');
elseif strcmp(k,'numpad0')   
      data.functions.show_bg();
elseif strcmp(k,'upArrow')   
      disp('up')
elseif strcmp(k,'downArrow')   
      disp('down')
else
    disp('key not found')
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global data
val = get(handles.slider1, 'value')
set(handles.edit1, 'string', val)
data.updatefeatures = 1;




% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end




% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global data
val = get(handles.slider2, 'value')
set(handles.edit2, 'string', val)


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global data
val = get(handles.slider3, 'value')
set(handles.edit3, 'string', val)


% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global data
val = get(handles.slider4, 'value')
set(handles.edit4, 'string', val)


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data adjC

adjC.close=0;
try
    adjustContrast(data.settings.contrastMin, data.settings.contrastMax,'data','data.functions.redraw()','data.settings.contrastMin','data.settings.contrastMax',data.img)
catch e
    warndlg('No Image to adjust')
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data
try
readparams(data.handles)
lb_str = get(handles.listbox1, 'string');
selected = get(handles.listbox1, 'value');
size_lb = size(lb_str);
size_lb = size_lb(1);

    path_s = regexp(data.pathname,'\','split');
    size_path = size(path_s);
    bgpath = '';
    for i=1:1:size_path(2)-1
        bgpath = [bgpath  path_s{i} '\'];
    end
    
    bgpath = [bgpath 'background']
    if(length(dir(bgpath)) == 0)
        mkdir(bgpath);
    end
    bgpath = [bgpath '\' path_s{size_path(2)} '\'];
    if(length(dir(bgpath)) == 0)
        mkdir(bgpath);
    end

    
    
 
wh = waitbar(0,'Overall calculation progress');    
for i = 1:size_lb
    if (exist ([bgpath lb_str{i}], 'file') & i==1)
        choice = questdlg('Background already exist. Do you want to overwrite it?', 'Overwrite alarm', 'Yes','No', 'sure');
        % Handle response
        switch choice
        case 'Yes'
           disp('yes');
        case 'No'
            close(wh);
            break;
        case 'sure'   
            disp('sure')
        end
    end
    
    waitbar(i/size_lb,wh);
    img = loadimage([data.pathname '\' lb_str{i}],0);
    [bg_img counter] = backgroundestimation(img, data.settings);
    
    imwrite(img, [bgpath lb_str{i}])
    %dsf
end
close(wh);
catch e
    close(wh);
    warndlg('No Images loaded')
end
