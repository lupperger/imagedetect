%%init
global siq sbt
labels = [{'Median','Mean','Std','Skewness','Kurtosis','CoV','MaxMinusMin',...
    'Tamcoarseness','TamCoarseness histogram1','TamCoarseness histogram2','TamCoarseness histogram3','TamDirectionality','TamContrast',...
    'AngularSecondMoment','Contrast','Correlation','Variance','InverseDifferenceMoment','SumAverage','SumVariance','SumEntropy','Entropy','DifferenceVariance','DifferenceEntropy','InfoMeas','InfoMeas2','GaborX','GaborY',...
   } strsplit(',',sprintf('HoG %d,',1:27))];
labels(end)=[]
    
img = siq.img;
% classes == 1 = is background 

%% do it with GUI:
selectBackgroundTiles(img,30)
%% get features
trainingmat = getBackgroundTileParams(img,sbt.coords,sbt.win);
labels = {'Median','Mean','Std','Skewness','Kurtosis','CoV','MaxByMin'};
trainclasses = sbt.classes;
trainFilename = repmat({siq.filename{1}},1,numel(sbt.classes));
allcoords = sbt.coords;
size(trainingmat)

%% extend trainmat

trainingmat = [trainingmat; getBackgroundTileParams(img,sbt.coords,sbt.win)];
trainclasses = [trainclasses sbt.classes];

trainFilename = [trainFilename repmat({siq.filename{1}},1,numel(sbt.classes))];
allcoords = [allcoords;sbt.coords];
size(trainingmat)

%% plot the classes
cl = {'Red','blue','green'};
figure;
hold on
for x=1:numel(trainclasses)
    plot3(trainingmat(x,5),trainingmat(x,2),trainingmat(x,3),'o','Color',cl{trainclasses(x)+1},'linewidth',10)
end
legend('noBack','isBack')

%% plot tree

t = classregtree(trainingmat,trainclasses)

view(t)

%% correlation matrix
figure
plotMatrixCorr(trainingmat,corr(trainingmat),labels)

%% get all image features

[testmat, illu] = getImageFeatures(siq.img,sbt.win);


%% try an svm
SVMStruct = svmtrain(trainingmat,trainclasses,'kernel_function','rbf','boxconstraint',1,'rbf_sigma',1)

% newgroups = svmclassify(SVMStruct,featuremat)

%% and apply SVM

[testmat, testclasses,illu] = applyTilingSVM(SVMStruct,img,sbt.win);

%% train random forest

rf = TreeBagger(1000,trainingmat,trainclasses,'OOBPred','on')

confusionmat(trainclasses',double(strcmp(rf.oobPredict,'1')))

%% apply RF
[testclasses, testscores]=rf.predict(testmat);
testclasses = strcmp(testclasses,'1');
fprintf('%d backgrounds',sum(testclasses==1))
% testclasses = testscores(:,1)<.3;
fprintf(' after thresh %d backgrounds\n',sum(testclasses==1))
%% try a pca
featuremat = testmat;
classes = testclasses;
[pc, score, latent] = princomp(zscore(featuremat));

cumsum(latent)./sum(latent)

figure;
hold on
plot(score(classes == 1,1),score(classes == 1,2),'.')
plot(score(classes == 0,1),score(classes == 0,2),'.r')
legend('isBack','noBack')

%% show the grid

y=illu(testclasses==1,1);
x=illu(testclasses==1,2);
z=illu(testclasses==1,3);
imgwidth=size(img,2);
imghight=size(img,1);
figure
plot3(x,y,z,'xk')
grid on

%% show the rectangles in img
binsize = sbt.win;
figure;
imagesc(img)
hold on
colormap(gray)
axis off
axis equal
y=illu(:,1);
x=illu(:,2);
for i = find(testclasses == 1)'
    p=rectangle('position',[x(i)-binsize/2 y(i)-binsize/2 binsize binsize],'EdgeColor',[0 0 1]);
%     set(p,'FaceAlpha',0.5);
    
end

figure;
imagesc(img)
hold on
colormap(gray)
axis off
axis equal
y=illu(:,1);
x=illu(:,2);
for i = find(testclasses == 0)'
    rectangle('position',[x(i)-binsize/2 y(i)-binsize/2 binsize binsize],'EdgeColor',[1 1 0])
    
end
%% interpolate it

y=illu(testclasses==1,1);
x=illu(testclasses==1,2);
z=illu(testclasses==1,3);
imgwidth=size(img,2);
imghight=size(img,1);

%interpolate
[XI YI] = meshgrid(1:imgwidth,1:imghight);
F=TriScatteredInterp(x,y,z,'natural');
ZI=F(XI,YI);
figure;
imagesc(ZI)
%% try some unsupervised

[classes centroids] = kmeans(featuremat,2) ;
classes = classes-1;
centroids
%% settings
if centroids(1,1)<centroids(2,1)
    settings.kurtosis = centroids(1,2);
    settings.skewness = centroids(1,1);
    settings.dispersion = centroids(1,3);
else
    settings.kurtosis = centroids(2,2);
    settings.skewness = centroids(2,1);
    settings.dispersion = centroids(2,3);
end

%% plot densitiy matrix

figure
scattercloud(featuremat(:,2),featuremat(:,3),30,1,'k.',jet(256),1)


%% try dbscan

[classes,type]=dbscan(featuremat,100,[]);
unique(classes)
classes(classes==-1)=0;
%%
cl = {'green','blue','red'};
figure;
hold on
for x=unique(classes)
    plot3(featuremat(classes ==x,1),featuremat(classes == x,2),featuremat(classes ==x,5),'.','Color',cl{unique(classes(classes==x))+1},'linewidth',10)
end
xlabel('Skewness')
ylabel('Kurtosis')
zlabel('Variance')

