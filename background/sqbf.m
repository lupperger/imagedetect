function [ batch ] = sqbf(X, Y, Z, batchsize)
% sqbf: Stochastic Query-By-Forest (Bosirov 2011)
%
% USAGE:
% ======
% [batch] = sqbf(X, Y, Z, batchsize)
%
% X:            Feature table of _learned_ instances
%               rows = instances, column = features
% Y:            Classlabels of _learned_ instances,
%               row vector for each instance
% Z:            Feature table of _unlearned_ instances
%               rows = instances, column = features
% batchsize:    size of desired batch
%
% Output Arguments:
% =================
% batch:        Index vector of instances from X to learn
%
% Author: Manuel Kroiss

% parameters
trees = 700;
fboot = 0.6;
n = size(X, 1) * fboot;
% eigene Umsetzung der Mindestgr��e f�r Bl�tter (im Paper splitten sie den
% Baum nur bisie nur bis zur Tiefe 6) kannst du auch auskommentieren
minlf = max(1, round(n*exp(-lambertw(n*log(2)))));
alpha = 2/3;

% build random forest and predict
op = statset('UseParallel', 'never');
tb = TreeBagger(trees, X, Y, 'FBoot', fboot, 'MinLeaf', minlf, 'priorprob', 'equal', 'Options', op);
[~, ~, stdev] = predict(tb, Z);

% find index of rare class
cn = numel(tb.ClassNames);
cc = zeros(1, cn);
for c = 1:cn
    cc(c) = sum(strcmp(tb.Y, tb.ClassNames{c}));
end
[~, rare] = min(cc);

% sort stdev values q
q = stdev(:, rare);
sq = sort(q, 'descend');
unlabNum = size(Z, 1);
q0 = sq(floor(alpha*unlabNum) + 1);

% calculate sampling distrbution L
L = (q - q0) / (sq(1) - q0);
L = max(L, 0);
L = L / sum(L);

% sample batch
batch = zeros(1, min(batchsize, c));
for i = 1:batchsize
    % sample one item from L
    r = rand();
    cumprob = [ 0; cumsum(L) ];
    for j = 1:numel(L)
        if cumprob(j) < r && r <= cumprob(j + 1)
            batch(i) = j;
            break;
        end
    end
    
    % set prob to zero and renormalize
    L(j) = 0;
    L = L / sum(L);
end

end
