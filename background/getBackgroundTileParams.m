function featuremat = getBackgroundTileParams(image, coords,binsize)

% imgwidth=size(image,2);
% imghight=size(image,1);


featuremat = zeros(size(coords,1),getTilingFeatures([]));


for i = 1:size(coords,1)
    r = coords(i,:);
    sub = image(round(r(2)):round(r(2))+binsize, round(r(1)):round(r(1))+binsize);

    featuremat(i,:) = getTilingFeatures(sub);
    
end