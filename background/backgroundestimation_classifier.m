function bg = backgroundestimation_classifier(img,train)

tic
% get feature of img
fprintf('Getting image features...\n')
if isfield(train,'usefeatures')
    [featuremat, illu] = getImageFeaturesSelected(img,train.win,train.usefeatures);
else
[featuremat, illu] = getImageFeatures(img,train.win);
end
% init
coords = illu(:,2:-1:1)+train.win/2;
means = illu(:,3);


% classify
fprintf('Running classifier...\n')
if isfield(train,'usefeatures')
    [testclasses] = train.rf.predict(featuremat(:,train.usefeatures));
else
    [testclasses] = train.rf.predict(featuremat);
end
classes = strcmp(testclasses,'1');

% init interpolate
x=coords(classes==1,1);
y=coords(classes==1,2);
z=means(classes==1);


imgwidth=size(img,2);
imghight=size(img,1);

%interpolate
fprintf('Interpolating...\n')

[XI YI] = meshgrid(1:imgwidth,1:imghight);
% F=TriScatteredInterp(x,y,z,'natural');
F=scatteredInterpolant(x,y,z,'natural');
ZI=F(XI,YI);

% extrapolate 
% for i=find(sum(~isnan(ZI(1:imghight,:)))>1)
%     ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
% end
% 
% for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
%     ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
% end

bg = ZI;
fprintf('Done in %3.2f sec.\n',toc)
