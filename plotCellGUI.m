function varargout = plotCellGUI(varargin)
% PLOTCELLGUI MATLAB code for plotCellGUI.fig
%      PLOTCELLGUI, by itself, creates a new PLOTCELLGUI or raises the existing
%      singleton*.
%
%      H = PLOTCELLGUI returns the handle to a new PLOTCELLGUI or the handle to
%      the existing singleton*.
%
%      PLOTCELLGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTCELLGUI.M with the given input arguments.
%
%      PLOTCELLGUI('Property','Value',...) creates a new PLOTCELLGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before plotCellGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to plotCellGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help plotCellGUI

% Last Modified by GUIDE v2.5 26-Jun-2012 15:55:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @plotCellGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @plotCellGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

selection.cell = [1];
selection.res = [1];
selection.channel = [1];
% End initialization code - DO NOT EDIT


% --- Executes just before plotCellGUI is made visible.
function plotCellGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to plotCellGUI (see VARARGIN)

% Choose default command line output for plotCellGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% UIWAIT makes plotCellGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = plotCellGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global selection;
plotCell(selection.cell, selection.res, selection.channel, handles);

function plotCell(usedc, usedres, channel, handles)
global vtset;

%figure
%axes(handles.axes1);
cla;
hold(handles.axes1, 'all');


usedcells = usedc;
usedresults = usedres;


for cellnr = usedcells
    for i = usedresults
        if  numel(vtset.results) > 0 &&  numel(vtset.results{1,i}.quants) > 0
            set(handles.axes1, 'ColorOrder', vtset.colors.color{i,1});
            cellvector = find(vtset.results{1,i}.quants(1,channel).cellNr == cellnr);
            time = vtset.results{1,i}.quants(1,channel).timepoint(cellvector);
            [time,order]=sort(time);
            int = vtset.results{1,i}.quants(1,channel).int(cellvector);
            int = int(order);
            plot(handles.axes1, time, int);
        else
            return
        end
    end
end

hold off;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
global selection;
selection.res = get(hObject,'Value');
plotCell(selection.cell, selection.res, selection.channel, handles);


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
global vtset;
ex = exist ('vtset', 'var');
switch ex
    case 0
    case 1
        set(hObject, 'String', 1:max(vtset.results), 'Min', 0, 'Max', numel(vtset.results));
    otherwise       
end



% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2
global selection;
selection.cell = get(hObject,'Value');
plotCell(selection.cell, selection.res, selection.channel, handles);

% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
global vtset;
ex = exist ('vtset', 'var');
switch ex
    case 0
    case 1
        set(hObject, 'String', 1:max(vtset.results{1,1}.quants(1,1).cellNr), 'Min', 0, 'Max', numel(vtset.results{1,1}.quants(1,1).cellNr));
    otherwise       
end



% --- Executes on selection change in popupmenuChannel.
function popupmenuChannel_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuChannel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuChannel
global selection;
selection.channel = get(hObject, 'Value');
plotCell(selection.cell, selection.res, selection.channel, handles);

% --- Executes during object creation, after setting all properties.
function popupmenuChannel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
global vtset;
ex = exist ('vtset', 'var');
switch ex
    case 0
    case 1
        set(hObject, 'String', 1:numel(vtset.results{1,1}.quants));
    otherwise       
end
