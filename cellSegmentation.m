%%%%%%%% CELL DETECTION / SEGMENTATION %%%%%%%%%%%
% needs: image, x, y and the tracksettings
% gives: detected center as binary image, all detected, the extracted
% subimg

function [Center, subimg,trackset]=cellSegmentation(image, x, y, trackset,bg,imgraw)

% extract subimage
[subimg centerX centerY] = extractSubImage(image, x, y, trackset.win);

if ~numel(subimg)
    fprintf('looks like there is a pixel mapping error\n')
    Center = false(trackset.win,trackset.win);
    return
end

%%% TODO make imadjust more robust!
subimgtemp = subimg; %for MSER
subimg = imadjust(subimg);
% determine threshold & do the segmentation
if strcmp('Ellipse',trackset.threshmethod)
    % first get center, then create ellipse
    ThreshImg=imadjust(image);
    Threshold = determineThreshold(ThreshImg, trackset.threshmethod)*trackset.threshcorr;
    
    Center = myseg(subimg, Threshold, trackset,centerX,centerY);
    stats = regionprops(Center,'Centroid');
    if numel(stats)==0
        stats(1).Centroid=[trackset.win/2 trackset.win/2];
    end
    centerX = stats.Centroid(1);
    centerY = stats.Centroid(2);
    
    % create circle
    Center = zeros(size(Center));
    if ~isfield(trackset,'ellipseradius')
        trackset.ellipseradius=7;
    end
    Center = filledCircleMatrix(Center,round(centerX),round(centerY),trackset.ellipseradius,1);
%     Center = filledCircleMatrix(Center,trackset.ellipseradius,centerY,centerX,1);
%     [x y]=scircle2(centerY,centerX,centerY-trackset.ellipseradius,centerX-trackset.ellipseradius);
%     for j = 1:numel(x)
%         Center(min(trackset.win,max(1,round(x(j)))),min(trackset.win,max(1,round(y(j)))))=1;
%     end
    Center= im2bw(Center);
elseif strcmp('MSER',trackset.threshmethod)
    % check settings struct
    
    % segment active contour
%     Center = myseg_drlse(image, 0, trackset,x,y,bg,imgraw);
%     Center  = extractSubImage(Center, 50,50, trackset.win);
    
    % segment mser
    Center = myseg_mser_brightfield(subimgtemp, trackset,centerX,centerY);
    
%     Center = extractSubImage(Center, x,y, trackset.win);
elseif strcmp('Freehand',trackset.threshmethod)
    % do nothing
    Center=0;
elseif strcmp('External Segmentation',trackset.threshmethod)
    % process external segmentation if necessary
    Center = myseg_external(subimg,trackset,centerX,centerY);
    
else
    % do the normal detection
    method=lower(strsplit_qtfy(' ',trackset.threshmethod));
    if(strcmp(method(2),'global'))
        ThreshImg=imadjust(image);
        clear image method
    elseif(strcmp(method(2), 'local'))
        ThreshImg=subimg;
    end
    
    
    Threshold = determineThreshold(ThreshImg, trackset.threshmethod)*trackset.threshcorr;
    
    Center = myseg(subimg, Threshold, trackset,centerX,centerY);

end

Center=imfill(Center,'holes');