%% vtset with all the GUI settings remains


%% results should look like this:


%%% detects %%%
% idee: detects = detection channel (durchnummeriert, genaue infos was es ist steht in detects(i).settings) 
% pro detects steht tp, posi, ... tracksettings (initially set / later modified) + binary (cellmask, will be calculated) und ein
% active flag
results.detects = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'X',{},'Y',{},'filename',{},'cellmask',{},'size',{},'trackset',{},'active',{});

%%% quants %%%
% wie detect, in settings steht, aus welchem channel die cellmask drüber
% gelegt werden soll, 
results.quants = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'int',{},'active',{});
    % settings: normalize flag, detect channel, 
     


%%%% nonfluor %%%
% not sure what todo when detecting / quantifiing on brightfield
results.nonFluor = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'active',{});


%%% restinfo? %%% 
% addition WLs, stop reason etc


%%%% general settings %%%
% tree description, image and tree root
% which quant uses which detect
% own treefile
% separate for each tree!
results.settings.imgroot=[];
results.settings.description=[];
results.settings.treename=[];



%% some funny tests
% init some test structure
results.quants(1).cellNr=[1 1 1 1 1 1 1 1];
results.quants(1).timepoint=1:8;
results.quants(1).absoluteTime=1:8;
results.quants(1).positionIndex=[1 1 1 1 1 1 1 1];
results.quants(1).filename=cell(1,8);
results.quants(1).int=rand(1,8)*10;
results.quants(1).active=boolean(ones(8,1));
results.quants(1).settings.detectchannel=1;
results.quants(1).settings.wl=1;


results.detects(1).cellNr=[1 1 1 1 1 1 1 1];
results.detects(1).timepoint=1:8;
results.detects(1).absoluteTime=1:8;
results.detects(1).positionIndex=[1 1 1 1 1 1 1 1];
results.detects(1).filename=cell(1,8);
results.detects(1).active=boolean(ones(8,1));
results.detects(1).settings.tracksettings=[];
results.detects(1).wl=1;


% quantify

% iterate over quantification channels
for chan = 1:numel(results.quants)
    % iterate over all entries in results.quants(channel)
    for x = 1:numel(results.quants(chan).timepoint)        
        % init
        timepoint = results.quants(chan).timepoint(x);
        cellNr=results.quants(chan).cellNr(x);
        detectchan=results.quants(chan).settings.detectchannel;
        % get detection
        try
        cellmask = results.detects(detectchan).cellmask(results.detects(detectchan).timepoint==timepoint & results.detects(detectchan).cellNr==cellNr);
        % throw some errors when same cell & timepoint not there or not yet
        % detected
        catch abrotz
            rethrow(abrotz)
        end
        
        %read image
        img = imread(results.quants(chan).filename{x});
        %normalize it
        results.quants(chan).int(x) = sum(img(overlay));
        % done jippie!!
    end
end

