function verifyTree(recalc)

global vtset

cell = vtset.selected.cell;
timepoint = vtset.selected.timepoint;
curtreeDetect = vtset.results.detects{vtset.treenum};
curtreeQuant = vtset.results.quants{vtset.treenum};

if numel(cell)<1
    msgbox('No cell selected - this should not happen anymore');
    return;
end
% load detection image
cellid = find(curtreeDetect.cellNr==cell & curtreeDetect.timepoint==timepoint);

if numel(cellid)<1
    msgbox(sprintf('No cell ID found for cell %d time %f',cell,timepoint),'Error','error');
    return;
end

imgfile=curtreeDetect.filename{cellid};
imageDetect=imageCache([vtset.results.settings.imgroot imgfile])/255;

% apply illumination correction?
if vtset.results.settings.corrDetect
    corrImage = vtset.results.settings.corrImage;

    if vtset.results.settings.corrWithPreCalculatet && exist([vtset.results.settings.imgroot imgfile(1:16) 'background/' imgfile(17:end-3) 'png'],'file')
        corrImage = double(imread([vtset.results.settings.imgroot imgfile(1:16) 'background/' imgfile(17:end-3) 'png']))/255/255;

    else
        fprintf('Pre calculated background not found\n');
    end

    %%% check for gain/offset
    if exist([vtset.results.settings.imgroot imgfile(1:16) 'background/gain.png'],'file')
        gain = double(imread([vtset.results.settings.imgroot imgfile(1:16) 'background/gain.png']))/255/255;
        offset = double(imread([vtset.results.settings.imgroot imgfile(1:16) 'background/offset.png']))/255/255;
        imageDetect = normalizeImage(imageDetect,corrImage,gain,offset);
    else
        fprintf('gain / offset not found\n');
        imageDetect = normalizeImage(imageDetect,corrImage);
    end

end

% load corresponding quantification image
imgfileQuant=curtreeQuant.filename{cellid};

imageQuant=imageCache([vtset.results.settings.imgroot imgfileQuant])/255;

% apply illumination correction?
if vtset.results.settings.corrQuant
    
    corrImage = vtset.results.settings.corrImage;
    
    if vtset.results.settings.corrWithPreCalculatet && exist([vtset.results.settings.imgroot imgfileQuant(1:16) 'background/' imgfileQuant(17:end-3) 'png'],'file')
        corrImage = double(imread([vtset.results.settings.imgroot imgfileQuant(1:16) 'background/' imgfileQuant(17:end-3) 'png']))/255/255;
        
    else
        fprintf('Pre calculated background not found\n');
    end
    %%% check for gain/offset
    if exist([vtset.results.settings.imgroot imgfile(1:16) 'background/gain.png'],'file')
        gain = double(imread([vtset.results.settings.imgroot imgfile(1:16) 'background/gain.png']))/255/255;
        offset = double(imread([vtset.results.settings.imgroot imgfile(1:16) 'background/offset.png']))/255/255;
        imageQuant = normalizeImage(imageQuant,corrImage,gain,offset);
    else
        fprintf('gain / offset not found\n');
        imageQuant = normalizeImage(imageQuant,corrImage);
    end
    
        
end

% check if there is already a trackset for this cell
if isfield(curtreeQuant,'trackset') && size(curtreeQuant.trackset,2)>=cellid && numel(curtreeQuant.trackset{cellid})>0
    trackset = curtreeQuant.trackset{cellid};
else
    % generate trackset structure
    trackset = vtset.results.trackset;
    trackset.x = round(curtreeDetect.X(cellid))-1;
    trackset.y = round(curtreeDetect.Y(cellid))-1;
end

% generate info string
infoStr = sprintf('cell: %d, tp: %d, position: %d, x: %.0f, y: %.0f, value: %.3f', cell, ...
    timepoint, curtreeQuant.positionIndex(cellid),curtreeQuant.X(cellid), curtreeQuant.Y(cellid), curtreeQuant.int(cellid));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%555
%%%%%% MUSS HIER NICH quantify HIN!?!?!?!!???? %%%%%%%%%%%5


%%%%%%%%%% SEGMENTATION %%%%%%%%%%%%%%%%%%%%%%

% show it only in manual mode
if ~recalc
    %%%% WTF?!?!?!?!?!?!?! wo kommtn das her??
    [newtrackset,center]=ManualTracking(imageDetect,imageQuant,trackset,infoStr);
        
% else recalc with settings of selected cell
else
    % take settings of reference cell
    newtrackset= vtset.temp.trackset;

    newtrackset.x=trackset.x;
    newtrackset.y=trackset.y;

    
    %%%% 0 == detect, 1 == quant
    if newtrackset.detectionChannel
        img=imageQuant;
    else
        img=imageDetect;
    end

    center = cellSegmentation(img, newtrackset.x, newtrackset.y, newtrackset);
end


%%%%%%%%% QUANTIFICATION %%%%%%%%%%%%%%%%

if numel(newtrackset)>0

    % extract subimage
    x = newtrackset.x;
    y = newtrackset.y;
    window = newtrackset.win;
    subimgQuant = extractSubImage(imageQuant, x, y, window);
    

    % store size
    vtset.results.quants{vtset.treenum}.size(cellid) =  sum(center(:));
    % quantify
    vtset.results.quants{vtset.treenum}.int(cellid) = sum(subimgQuant(center));

    % calc mean
    vtset.results.quants{vtset.treenum}.mean(cellid) = vtset.results.quants{vtset.treenum}.int(cellid) ...
        / vtset.results.quants{vtset.treenum}.size(cellid);
    % store settings
    vtset.results.quants{vtset.treenum}.trackset{cellid} = newtrackset;
    % cell mask
    vtset.results.quants{vtset.treenum}.cellmask{cellid} = center;
    % store coordinates
    vtset.results.quants{vtset.treenum}.X(cellid) = x;
    vtset.results.quants{vtset.treenum}.Y(cellid) = y;
    vtset.unsaved(vtset.treenum)=1;
end

