% this function does the segmentation as well as the quantification based
% on given images. had to be seperated from the main function prcoess tree
% for parallelisation purposes
function tracksQuant=quantify(cells, tracksDetect, trackset, imgQuant, imgDetect, tracksQuant)


for j=1:numel(cells)

            try
            x = round(tracksDetect.X(cells(j)))-1; % -1 because matlab is 1-based and coords are 0-based
            y = round(tracksDetect.Y(cells(j)))-1;
            
            
            if trackset.detectionChannel
                img=imgQuant;
            else
                img=imgDetect;
            end
            
            %%%% SEGMENTATION
            
            center= cellSegmentation(img, x, y, trackset);
            
            
            %%%%%%%%%%%%%%% QUANTIFICATION %%%%%%%%%%%%%%%%%%%%

            subimgQuant = extractSubImage(imgQuant, x, y, trackset.win);
            
            % calculate block median normalization
%             if blocknorm
%                 subimgQuantblock=extractSubImage(imgQuant, x, y, blocksize);
%                 subimgQuant = subimgQuant - median(subimgQuantblock(:));
%             end
            
            total=sum(subimgQuant(center));
            
            
            %%% store in strucct
            tracksQuant.int(cells(j)) = total;
            tracksQuant.size(cells(j)) = numel(find(center));
            tracksQuant.mean(cells(j)) = total / tracksQuant.size(cells(j));
            tracksQuant.cellmask{cells(j)} = center;

            tracksQuant.trackset{cells(j)} = trackset;
            tracksQuant.trackset{cells(j)}.x=x;
            tracksQuant.trackset{cells(j)}.y=y;
            
            catch
                tracksQuant.int(cells(j)) = 0;
                tracksQuant.size(cells(j)) = 0;
                tracksQuant.mean(cells(j)) = 0;
                tracksQuant.cellmask{cells(j)} = 0;

                tracksQuant.trackset{cells(j)} = trackset;
                tracksQuant.trackset{cells(j)}.x=x;
                tracksQuant.trackset{cells(j)}.y=y;
                tracksQuant.error.x=x;
                tracksQuant.error.y=y;
                tracksQuant.error.timepoint=tracksDetect.timepoint(cells(j));
                tracksQuant.error.cell=tracksDetect.cellNr(cells(j));
%                 errordlg(sprintf('Quantification Error: Timepoint %d cell %d\nX: %d Y: %d', tracksDetect.timepoint(cells(j)), tracksDetect.timepoint(cells(j)), x,y));
            end
end