
%% subimg must be set
function p=findCell(img,params)
x = params(1);
y = params(2);
r = params(3);
fprintf('x=%f, y=%f, r=%f\n', x,y,r);
p=-quantifyCircle(img(y-r:y+r,x-r:x+r))/(pi*r);


function dummy
%% try the anneal
opts = {};
opts.Verbosity = 2;
opts.Generator = @newCircle;

anneal(@(x)findCell(subimg,x), [30 30 5], opts);