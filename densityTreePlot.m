function densityTreePlot

global vtset dtp

dtp.h=[];
dtp.hgui=[];

% get a list of trees
treelist = {};
for t = 1:numel(vtset.results)
    treelist{end+1}= vtset.results{t}.settings.treefile;
end

selected=listdlg('liststring',treelist,'name','Select tree to calculate','initialvalue',vtset.treenum,'ListSize',[200 200]);


% select channel
channs = {};
for c = 1:numel(vtset.results{1}.settings.channelsettings)
    channs{end+1} = vtset.results{1}.settings.channelsettings(c).quant;
end

selectedChan=listdlg('liststring',channs,'name','Select Wavelength to calculate','initialvalue',1,'ListSize',[200 200]);

dachannel = channs{selectedChan};

% ask for mode

a = questdlg('Absolute or cumulative view','DrawMode','Absolute','Cumulative','Absolute');

%% collect data
alltp = [];
allnormtps = [];
alldata=[];
allcells = [];
for id = selected
    
    %check for correct quantification channel
    if strcmp(vtset.results{id}.settings.channelsettings(selectedChan).quant,dachannel)
        channel = selectedChan;
    else
        found = 0;
        for c = 1:numel(vtset.results{id}.settings.channelsettings)
            if strcmp(vtset.results{id}.settings.channelsettings(c).quant,dachannel)
                channel = c;
                found = 1;
            end
        end
        if ~found
            warndlg(sprintf('Skipping Tree %s, Quantification channel %s not found',vtset.results{id}.settings.treefile,dachannel))
            continue
        end
    end
    
    for c = unique(vtset.results{id}.quants(channel).cellNr);
        dat= vtset.results{id}.quants(channel).int( vtset.results{id}.quants(channel).cellNr == c);
        tps= vtset.results{id}.quants(channel).timepoint(vtset.results{id}.quants(channel).cellNr == c);
        [tps,order] = sort(tps);
        normtps = (tps -min(tps))/(max(tps)-min(tps));
        dat=dat(order);
        rp = rootpath(c);
        if ~strcmp(a(1),'A')
            if numel(rp)>1
                for rpc = rp(2:end)
                    mother = vtset.results{id}.quants(channel).int(vtset.results{id}.quants(channel).cellNr == rpc);
                    mothertp = vtset.results{id}.quants(channel).timepoint(vtset.results{id}.quants(channel).cellNr == rpc);
                    mother = mother(max(mothertp)==mothertp);
                    
                    
                    dat = dat+mother/2;
                    
                end
            end
        end
        alltp = [alltp tps];
        allnormtps=[allnormtps normtps];
        alldata= [alldata dat];
        allcells= [allcells zeros(1,numel(dat))+c];
    end
    %     plot(vtset.results{id}.quants(2).absoluteTime/3600,vtset.results{id}.quants(2).int,'.');
end
data = ([alltp;alldata;allcells])';
data(isnan(data))=0;
data(isinf(data))=0;
data(:,4) = floor(log2(data(:,3)));
data(:,5) = allnormtps;

%% density plot, density by n-neighbourhood
dtp.h=figure('position',[  398         558        1049         540]);
hold on
binning=2;
unitp = unique(data(:,1));
for tp = 1:numel(unitp)
    
    alldata=data(data(:,1) >= unitp(max(1,tp-binning)) & data(:,1)<=unitp(min(numel(unitp),tp+binning)),:);
    alldatatoplot=data(data(:,1) == unitp(tp),:);
    %     if numel(alldata)>10
    [f,xi] = ksdensity(alldata(:,2));
    f= f./max(f);
    d = interp1(xi,f,alldatatoplot(:,2));
    scatter(alldatatoplot(:,1),alldatatoplot(:,2),4,d,'linewidth',3)
    %     end
end
xlabel('timepoint')
ylabel('intensity')

dtp.ax = gca;
dtp.hgui(end+1)=uicontrol(dtp.h,'position',[1 1 60 20],'style','pushbutton','String','X axis limits','callback',['global dtp; '...
                'oldx = get(dtp.ax,''xlim''); '...
                't=inputdlg(''Enter new x axis limits:'',''X axis'',1,{num2str(oldx)});'...
                'try set(dtp.ax,''xlim'',eval([''['' t{1} '']'']));catch e;end;'...
                '']);
dtp.hgui(end+1)=uicontrol(dtp.h,'position',[1 25 60 20],'style','pushbutton','String','Y axis limits','callback',['global dtp; '...
                'oldy = get(dtp.ax,''ylim''); '...
                't=inputdlg(''Enter new y axis limits:'',''Y axis'',1,{num2str(oldy)});'...
                'try set(dtp.ax,''ylim'',eval([''['' t{1} '']'']));catch e;end;'...
                '']);

% set(gca,'ylim',[0 8])
% set(gca,'xlim',[0 3000])