function plot2features(f1,f2)

% do the plot

plot(f1,f2)

function dummy
%%  test some sample data

figure;
hold on

binsize = 2;

l = colormap(jet(201));

for tp = 0:binsize:200
    clf
    for t=1:numel(results.quants)
        
        hold on;
        title(sprintf('Time %d min',tp))
        
        
        alldata=results.quants{t}.int(results.quants{t}.absoluteTime/3600>tp & results.quants{t}.absoluteTime/3600<tp+binsize);
        allsizes=results.quants{t}.size(results.quants{t}.absoluteTime/3600>tp & results.quants{t}.absoluteTime/3600<tp+binsize);
        if numel(alldata) && numel(allsizes)
            
            loine=(linspace(1,2^(2/3),numel(alldata)));
            alldata = alldata./((loine.^(3/2)))';
%             allsizes = allsizes./((loine.^(3/2)))';
            
            plot(alldata,allsizes,'*','Color',l(tp+1,:))
        end
    end
    set(gca,'ylim',[0 150])
    set(gca,'xlim',[0 10])
    drawnow
end

