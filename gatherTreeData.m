%% fill or extend all missing elements of ONE treedata
function treedata = gatherTreeData(treedata)
global vtset


%% init
if ispc && ~isfield(vtset,'movieFolder')
    treepath =  'X:\TTTfiles/';
    experimentspath = 'Z:\TTT\';
elseif isunix
    treepath =  '/nfs/ttt/TTTfiles/';
    experimentspath = '/storage/cmbstore/TTT/';
else
    treepath =  vtset.tttrootFolder;
    experimentspath = vtset.movierootFolder;
end

moviename = treedata.settings.exp;
imgroot= [experimentspath '/' moviename];
moviestart = getmoviestart(imgroot);

xmlname = [experimentspath moviename '/' moviename '_TATexp.xml'];
% older experiments may have other xml files
if ~exist(xmlname,'file')
    warning('General XML file missing. Trying to take first xml file in movie folder');
    xmlname = dir([imgroot '*.xml']);
    xmlname = xmlname(1).name;
end


xmlpath = [experimentspath moviename '/' xmlname];


%%%%% AB HIER WEITER MACHEN
%%%%% settings structur bauen ohne gui

%% get tracking data
res = tttParser([treepath treedata.settings.treeroot treedata.settings.treefile],xmlpath);
treedataidx = 1;
if isempty(res) || numel(unique(res.cellNr)) < r.cellthresh
    continue
end
fprintf('Processing %s (%i of %i)\n',stru.files{i},i,numel(stru.files))
% resfields = fields(res);

%% map filenames

res=gatherAbsTimeAndFileName(res,imgroot,moviename,wavelenght,moviestart);
% for posi = unique(res.positionIndex)'
%     fprintf('gathering position %d files %s...\n',posi,r.wl)
%     
%     % read position log file to get absolute timepoints otherwise use old
%     % method
%     if exist(sprintf('%s/%s_p%04d/',imgroot,r.moviename, posi),'dir')
%         newexperiment=1;
%         filename = sprintf('%s/%s_p%04d/%s_p%04d.log',imgroot,r.moviename,posi,r.moviename,posi);
%     else
%         newexperiment = 0;
%         filename = sprintf('%s/%s_p%03d/%s_p%03d.log',imgroot,r.moviename,posi,r.moviename,posi);
%         
%     end
%     
%     if exist(filename,'file')
%         % read logfile
%         log = positionLogFileReader(filename);
%         
%         idx=find(res.positionIndex == posi)';
%         
%         wl = strsplit('.',wavelength);
%         wl = str2double(wl{1}(2:end));
%         
%         for id = idx
%             % store everything
%             abstime = log.absoluteTime(log.timepoint == res.timepoint(id) & log.wavelength == wl);
%             if isempty(abstime)
%                 continue
%             else
%                 zindex= '';
%                 if newexperiment
%                     if log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl) ~= -1
%                         zindex = sprintf('z%03d_',log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl));
%                     end
%                     % example:
%                     % old 110624AF6_p0024_t00002_w1.tif
%                     % new 111103PH5_p0148_t00002_z001_w01.png
%                     
%                     filename = sprintf('%s_p%04d/%s_p%04d_t%05d_%s%s', r.moviename,posi,r.moviename,posi,res.timepoint(id),zindex,wavelength);
%                 else
%                     filename = sprintf('%s_p%03d/%s_p%03d_t%05d_%s%s',  r.moviename,posi,r.moviename,posi,res.timepoint(id),zindex,wavelength);
%                 end
%                 for f = 1:numel(resfields)
%                     treedata.nonFluor.(resfields{f})(treedataidx) = res.(resfields{f})(id);
%                 end
%                 treedata.nonFluor.absoluteTime(treedataidx) = abstime-moviestart;
%                 treedata.nonFluor.filename{treedataidx} = filename;
%                 treedataidx = treedataidx+1;
%             end
%         end
%     end
% end
% treedata(cellfun(@isempty,treedata)) = [];

fprintf('%i trees with at least %i cells saved as treedata struct!\n',numel(treedata),r.cellthresh)
end