function Center = myseg_external(subimage,trackset,centerX,centerY)

Objects = subimage;


%% select central object (and show it)
% centerX = round(size(Objects,1)/2);
% centerY = round(size(Objects,2)/2);
All = im2bw(Objects);

numL=0;
if ~trackset.usenearest
    Center = bwselect(Objects,centerX,centerY,4);
else
    L = bwlabel(All,4);
    numL = max(L(:));
    % iterate over them
    minDist=Inf;
    minCell=-1;
    for i=1:numL
        [r,c] = find(L==i);
        % distance to center
        dist=norm([centerX centerY] -[mean(c) mean(r)]);
        if dist<minDist
            minDist=dist;
            minCell=i;
        end
    end
    Center=L==minCell;
end