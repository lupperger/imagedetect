% iterate over image, detect background via skewness, get stützstellen,
% interpolate -> create synthetic illumination image

function [ZI interp]=backgroundestimation_new(varargin)

image=varargin{1};

%% hack around the backspline thing
% addpath('/nfs/cmbdata/schwarzfischer/scripts/bg_comp')
% interp=0;
% ZI=backgr(image*255,'auto',3,5,0);
% return
%%



% get params
if numel(varargin)>1
%     settings=varargin{2};
    
    binsize=varargin{2};%settings.winsize; %20-40
%     kurto=settings.kurtosis; %3
%     skew = settings.skewness; %0.3
%     varmean = settings.dispersion; %2.5e-04
else
    binsize = 40;
%     kurto = 3;
%     skew = 0.3;
%     varmean = 2.5e-04;
end

if numel(varargin)>2
    handles=varargin{3};
end

% if image is complete black / white 
if numel(unique(image)) == 1
    ZI = zeros(size(image));
    interp=0;
    return
end


%%
%init
fprintf('init...\n')

imgsize=size(image);

x = nan(floor((imgsize(1)-binsize)/binsize*2*(imgsize(2)-binsize)/binsize*2),1);
y = nan(floor((imgsize(1)-binsize)/binsize*2*(imgsize(2)-binsize)/binsize*2),1);
z = nan(floor((imgsize(1)-binsize)/binsize*2*(imgsize(2)-binsize)/binsize*2),1);
featuremat = nan(floor((imgsize(1)-binsize)/binsize*2*(imgsize(2)-binsize)/binsize*2),5);


imgwidth=size(image,2);
imghight=size(image,1);


counter=0;
fprintf('getting tiling features...\n')
for i = 1:floor(binsize/2):floor(imgsize(1)-binsize)
    
    for j=1:floor(binsize/2):floor(imgsize(2)-binsize)
        
        sub = image(i:i+binsize-1, j:j+binsize-1);
        counter=counter+1;
        y(counter) = i+binsize/2-1;
        x(counter) = j+binsize/2-1;
        z(counter)= mean(sub(:));
        featuremat(counter,:)=([std(sub(:)) skewness(sub(:)) max(sub(:))/min(sub(:)) kurtosis(sub(:)) var(sub(:))/mean(sub(:))  ]);
        
    end
end

%% repair strange features
featuremat(isinf(featuremat)) = 0;
featuremat(isnan(featuremat)) = 0;

%% cluster it
% fprintf('clustering %d points...\n',counter)
% % featuremat=zscore(featuremat);
% [classes,type]=dbscan(featuremat,size(featuremat,2)+1,.1);
%  
% 
% % classes=kmeans(featuremat,6);
% % classes = torow(classes);
% % type = ones(1,numel(classes));
% 
% unique(classes)
% 
% % if only one class
% if numel(unique(classes)) == 1
%     ZI = zeros(size(image));
%     interp=0;
%     return
% end
% 
% if numel ( type ) ~= numel ( classes)
%     classes(numel(classes):numel(type)) = -1;
% end
% %
% % classes(classes==-1)=0;
% % classes = classes+1;
% % find correct class
% 
% if numel(unique(classes))==1
%     daclass = unique(classes);
% else
%     classstd = [];
%     allnumels=[];
%     for c = unique(classes)
%         allnumels(end+1)=numel(featuremat(classes == c & type == 1,1));
%         if numel(featuremat(classes == c & type == 1,1))>200
%             classstd(end+1)= mean(featuremat(classes == c,1));% mean(featuremat(classes == 1,3))];% mean(featuremat(classes == 2,3))];
%         else
%             classstd(end+1) = inf;
%         end
%     end
%     [~,daclass] = min(classstd(2:end));
%     daclass = daclass+1;
%     daclasstemp  = unique(classes);
%     daclass = daclasstemp(daclass);
% end
% 
% if all(isinf(classstd))
%     [~,daclass] = max(allnumels(2:end));
%     daclass = daclass+1;
%     daclasstemp  = unique(classes);
%     daclass = daclasstemp(daclass);
% end
% 
% interp=sum(classes == daclass& type == 1);
% 
% fprintf('using %d interpolation points...\n',interp)

%% cluster with OPTICS

[RD,CD,order]=optics(featuremat,10);

cutoff= min(topmedian(RD,40),   40);
cuts = find(RD>cutoff);
% %%
% figure
% hold all
% for c = 2:numel(cuts)
%     plot(cuts(c-1):cuts(c),RD(cuts(c-1):cuts(c)))
% end

% find cluster with lowest std
allstd=Inf;
allnumels=0;
classes=zeros(1,numel(RD));
for c= 2:numel(cuts)
    points = order(cuts(c-1):cuts(c));
    classes(points)=c;
    allnumels(c)=numel(points);
    if numel(points)>1200
        allstd(c) = mean(featuremat(points,1));
    else 
        allstd(c)=Inf;
    end
end
[~,c]=min(allstd);

if all(isinf(allstd))
    [~,c]=max(allnumels);
end
% figure;
% plot(RD)
% hold on
% plot(cuts(c-1):cuts(c),RD(cuts(c-1):cuts(c)),'r')



interp=numel(cuts(c-1):cuts(c));

fprintf('using %d interpolation points...\n',interp)

%%
% update background GUI
if numel(varargin)>2
    set(handles.textStatus,'String',sprintf('Calculating Background... using %d interpolation points ...',interp))
    drawnow;
end



%% interpolate
% dist=[];
% for xxx = 1:20
% idx = find(type == 1);
% r = randi(numel(idx),100,1);
% type(idx(r))=-10;


[XI YI] = meshgrid(1:imgwidth,1:imghight);
%%% DBSCAN
% F=TriScatteredInterp(x(classes == daclass& type == 1),y(classes ==daclass& type == 1) ,z(classes ==daclass& type == 1),'natural');
%%% OPTICS
F=TriScatteredInterp(x(order(cuts(c-1):cuts(c))),y(order(cuts(c-1):cuts(c))) ,z(order(cuts(c-1):cuts(c))),'natural');
ZI=F(XI,YI);

%% extrapolate
for i=find(sum(~isnan(ZI(1:imghight,:)))>1)
    ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
end
%%
for i=find(sum(~isnan(ZI(:,1:imgwidth))')>1)
    ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
end


%%
% repair strange extrapolations
ZI(ZI<min(image(:)))=min(image(:));
ZI(ZI>max(image(:)))=max(image(:));
% dist(end+1) = rmsd(ZI,bg);

% end
fprintf('done\n')




function dummy
%% create a ZI with nan holes
ZI = nan(size(image));

for xy = order(cuts(c-1):cuts(c))
    ZI(y(xy),x(xy))=z(xy);
end
figure;
imagesc(ZI)

%%
figure;
hold all
% cl = lines;%{'green','blue','red'};

for xi=unique(classes)
    plot3(log(featuremat(classes ==xi,1)),log(featuremat(classes == xi,2)),log(featuremat(classes ==xi,3)),'.','linewidth',10)
end
xlabel('Skewness')
ylabel('Kurtosis')
zlabel('Variance')


%% all scatterclouds
figure;
counter= 0;
for i = 1:size(featuremat,2)
    for j = i:size(featuremat,2)
        counter = counter+1;
        
        subplot(size(featuremat,2),size(featuremat,2),counter)
        scattercloud(featuremat(:,i),featuremat(:,j),100,1,'k.',jet(256),1)
        hold on
        plot(featuremat(classes ==daclass,i),featuremat(classes ==daclass,j),'r*')
    end
end

%% zoom into scattercloud
set(0,'DefaultFigureColor',[1 1 1])
towrite = 'C:\users\michi\videos\density\';
printdat=0;
i=2;
j=3;
figure
scattercloud(log(featuremat(:,i)),log(featuremat(:,j)),100,1,'w.',jet(256),1)
hold on
% plot(log(featuremat(classes ==daclass,i),featuremat(classes ==daclass,j),'b.')
plot(log(featuremat(classes == daclass & type == 1,i)),log(featuremat(classes ==daclass & type == 1,j)),'r.')
xlabel('kurtosis')
ylabel('coefficient of variation')

xlim = get(gca,'xlim');
ylim = get(gca,'ylim');
return
i = linspace(xlim(2),xlim(2)/100,100);
j = linspace(ylim(2),ylim(2)/100,100);
for muh=1:100
   
    set(gca,'xlim',[xlim(1) i(muh)]);
    set(gca,'ylim',[ylim(1) j(muh)]);
    
    drawnow
    if printdat
        print(sprintf('%s%03d.png',towrite,muh),'-dpng')
    end
    pause(0.1)
end


%% create export strct for weka
clear struct
struct.kurtosis = log(featuremat(:,1));
struct.cov = log(featuremat(:,2));
struct.std= log(featuremat(:,3));
% struct.krutosis = featuremat(:,1);

ezwrite('test.csv',struct)