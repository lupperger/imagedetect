function children=getchildren(cellid,maxid)
if cellid*2+1<=maxid
    children = [cellid*2 cellid*2+1 getchildren(cellid*2,maxid) getchildren(cellid*2+1,maxid) ];
else
    children=[];
end