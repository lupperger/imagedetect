function BackgroundConverter

folder = uigetdir('Z:\TTT\100301PH4','Select experiment folder');

d = dir(folder);
wh = waitbar(0,'Scanning movie');

for i = 3:numel(d)
    % for each position
    if d(i).isdir
        files = dir([folder '/' d(i).name '/background/']);
        
        fprintf('Folder %s: found %d background files\n',d(i).name,numel(files))
        waitbar((i-2)/sum([d.isdir]),wh,sprintf('Moving Folder %d of %d, %d files',(i-2),sum([d.isdir]),numel(files)))
        fprintf('Creating folder %s\n',sprintf('%s/background/%s/',folder,d(i).name));
        
        try 
            mkdir(sprintf('%s/background/%s/',folder,d(i).name))
        catch e
            errordlg(sprintf('Could not create folder %s',sprintf('%s/background/%s/',folder,d(i).name)))
            rethrow(e)
        end
        source = sprintf('%s/%s/background/* ',folder,d(i).name);
        destination = sprintf('%s/background/%s/',folder,d(i).name);
        if ispc
            source = strrep(source,'/','\');
            destination = strrep(destination,'/','\');
        end
        
        fprintf('moving %d files from %s to %s\n',numel(files),source,destination)
        try
            if ispc   
                system(['move ' source destination]);
            else
                system(['mv ' source destination]);
            end
        catch e
            errordlg('Could not move files')
            rethrow(e)
        end
        
%         for f=3:numel(files)
%             waitbar(f/numel(files),wh)
%             fprintf('Moving file %s to %s\n',sprintf('%s/%s/background/%s ',folder,d(i).name,files(f).name),sprintf('%s/background/%s/',folder,d(i).name))
%             movefile(sprintf('%s/%s/background/%s ',folder,d(i).name,files(f).name),sprintf('%s/background/%s/',folder,d(i).name));
%             
%         end
        
        
    end
end

close(wh)