function varargout = QuantifySingleImage(varargin)
% QUANTIFYSINGLEIMAGE MATLAB code for QuantifySingleImage.fig
%      QUANTIFYSINGLEIMAGE, by itself, creates a new QUANTIFYSINGLEIMAGE or raises the existing
%      singleton*.
%
%      H = QUANTIFYSINGLEIMAGE returns the handle to a new QUANTIFYSINGLEIMAGE or the handle to
%      the existing singleton*.
%
%      QUANTIFYSINGLEIMAGE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QUANTIFYSINGLEIMAGE.M with the given input arguments.
%
%      QUANTIFYSINGLEIMAGE('Property','Value',...) creates a new QUANTIFYSINGLEIMAGE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before QuantifySingleImage_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to QuantifySingleImage_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help QuantifySingleImage

% Last Modified by GUIDE v2.5 05-Feb-2013 09:37:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @QuantifySingleImage_OpeningFcn, ...
                   'gui_OutputFcn',  @QuantifySingleImage_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before QuantifySingleImage is made visible.
function QuantifySingleImage_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDnumel(siq.tracking_coords)>0ATA)
% varargin   command line arguments to QuantifySingleImage (see VARARGIN)
global siq
% init
siq.img=[];
siq.int=[];
siq.Center={};
siq.normint=[];
siq.size=[];
siq.s2bg=[];
siq.bgint=[];
siq.perimeter = [];
siq.eccentricity = [];
siq.majorAxis = [];
siq.minorAxis = [];
siq.heterogeneity = [];
siq.bg=0;
siq.autodetect=[];
siq.actual=1;
siq.trackset=1;
siq.min=0;
siq.max=1;
siq.gimme=0;
siq.manual = 0;
siq.quantify=@ButtonQuantify_Callback;
siq.figurehandle = handles.figure1;
siq.statushandle = handles.textStatus;
siq.automode=0;
siq.autodetect=[];
siq.tracksetTemplate=[];
siq.includeSubfolder =0;
siq.thisIsAMovie = 0;
set(handles.textStatus,'String','Please load image...')

imageCache('init',100);
gainCache('init',20)

% catch keyboard function (has to be catched in every slider etc ...)
set(hObject,'KeyPressFcn',@myKeyFunction);
set(handles.figure1,'KeyPressFcn',@myKeyFunction);
set(handles.sliderCell,'KeyPressFcn',@myKeyFunction);
set(handles.sliderMaxima,'KeyPressFcn',@myKeyFunction);
set(handles.sliderSmooth,'KeyPressFcn',@myKeyFunction);
set(handles.sliderWin,'KeyPressFcn',@myKeyFunction);
set(handles.sliderTreshCorr,'KeyPressFcn',@myKeyFunction);
set(handles.listbox1,'KeyPressFcn',@myKeyFunction);
set(handles.listbox2,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonNext,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonBack,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonSaveNext,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonSave,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonDisacrd,'KeyPressFcn',@myKeyFunction);
axis(handles.axesQuant,'off')
axis(handles.axesDetect,'off')

% Choose default command line output for QuantifySingleImage
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes QuantifySingleImage wait for user response (see UIRESUME)
% uiwait(handles.figure1);
function myKeyFunction(src, evnt)
global siq
handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

origTrackset = siq.trackset(siq.actual);

if strcmp(k,'return') %if enter was pressed
    pause(0.01) %allows time to update
    ButtonSaveNext_Callback([],[],handles);
elseif strcmp(k,'backspace')    
    % one back
    ButtonBack_Callback([],[],handles);
    % maybe some undo function???
elseif strcmp(k,'delete')    
    % delete this timepoint
    ButtonDelete_Callback([],[],handles);   
elseif strcmp(k,'separator')
    ButtonSave_Callback([],[],handles);    
elseif strcmp(k,'home')    
    % go to first tp
    siq.actual=getNearestCell(siq.actual,-10000,handles);
    update(handles);
elseif strcmp(k,'end')        
    % go to last tp
    siq.actual=getNearestCell(siq.actual,+10000,handles);
    update(handles);
       
elseif strcmp(k,'leftarrow')
    % one timepoint back
    ButtonBack_Callback([],[],handles);
elseif strcmp(k,'rightarrow')
    % one timepoint further
    ButtonNext_Callback([],[],handles);
elseif strcmp(k,'numpad7')
    % threshcorr 0.1 down
    set(handles.sliderTreshCorr,'Value',max(get(handles.sliderTreshCorr,'Value')-0.1,0));
    set(handles.editTrashCorr,'String',num2str(get(handles.sliderTreshCorr,'Value')));
    siq.temp.trackset.threshcorr=get(handles.sliderTreshCorr,'Value');
    update(handles);
elseif strcmp(k,'numpad9')
    % threshcorr 0.1 up
    set(handles.sliderTreshCorr,'Value',min(get(handles.sliderTreshCorr,'Value')+0.1,2));
    set(handles.editTrashCorr,'String',num2str(get(handles.sliderTreshCorr,'Value')));
    siq.temp.trackset.threshcorr=get(handles.sliderTreshCorr,'Value');
    update(handles);
elseif strcmp(k,'numpad8')
    % reset threshcorr
    set(handles.sliderTreshCorr,'Value',origTrackset.threshcorr);
    set(handles.editTrashCorr,'String',num2str(get(handles.sliderTreshCorr,'Value')));
    siq.temp.trackset.threshcorr=get(handles.sliderTreshCorr,'Value');
    update(handles);
elseif strcmp(k,'numpad4')
    % smooth 1 down
    set(handles.sliderSmooth,'Value',max(get(handles.sliderSmooth,'Value')-1,0));
    set(handles.editSmooth,'String',num2str(get(handles.sliderSmooth,'Value')));
    siq.temp.trackset.smooth=get(handles.sliderSmooth,'Value');
    update(handles);
elseif strcmp(k,'numpad6')    
    % smooth 1 up
    set(handles.sliderSmooth,'Value',min(get(handles.sliderSmooth,'Value')+1,10));
    set(handles.editSmooth,'String',num2str(get(handles.sliderSmooth,'Value')));
    siq.temp.trackset.smooth=get(handles.sliderSmooth,'Value');
    update(handles);
elseif strcmp(k,'numpad5')
    % reset smooth
    set(handles.sliderSmooth,'Value',origTrackset.smooth);
    set(handles.editSmooth,'String',num2str(get(handles.sliderSmooth,'Value')));
    siq.temp.trackset.smooth=get(handles.sliderSmooth,'Value');
    update(handles);
elseif strcmp(k,'numpad1')    
    % max 1 down
    set(handles.sliderMaxima,'Value',max(get(handles.sliderMaxima,'Value')-1,0));
    set(handles.editMaxima,'String',num2str(get(handles.sliderMaxima,'Value')));
    siq.temp.trackset.max=get(handles.sliderMaxima,'Value');
    update(handles);
elseif strcmp(k,'numpad3')   
    % max 1 up
    set(handles.sliderMaxima,'Value',min(get(handles.sliderMaxima,'Value')+1,20));
    set(handles.editMaxima,'String',num2str(get(handles.sliderMaxima,'Value')));
    siq.temp.trackset.max=get(handles.sliderMaxima,'Value');
    update(handles);
elseif strcmp(k,'numpad2')
    % reset max
    set(handles.sliderMaxima,'Value',origTrackset.max);
    set(handles.editMaxima,'String',num2str(get(handles.sliderMaxima,'Value')));
    siq.temp.trackset.max=get(handles.sliderMaxima,'Value');
    update(handles);
elseif strcmp(k,'add')    
    % scrWindow 10 up
    set(handles.sliderWin,'Value',get(handles.sliderWin,'Value')-10);
    set(handles.editWinSize,'String',num2str(get(handles.sliderWin,'Value')));
    siq.temp.trackset.win=get(handles.sliderWin,'Value');
    update(handles);
elseif strcmp(k,'subtract')   
    % scrWindow 10 down
    set(handles.sliderWin,'Value',get(handles.sliderWin,'Value')+10);
    set(handles.editWinSize,'String',num2str(get(handles.sliderWin,'Value')));
    siq.temp.trackset.win=get(handles.sliderWin,'Value');
    update(handles);
elseif strcmp(k,'numpad0') 
    siq.temp.trackset = origTrackset;
    ButtonDisacrd_Callback([],[],handles);
end

% --- Outputs from this function are returned to the command line.
function varargout = QuantifySingleImage_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editTrashCorr_Callback(hObject, eventdata, handles)
% hObject    handle to editTrashCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTrashCorr as text
%        str2double(get(hObject,'String')) returns contents of editTrashCorr as a double


% --- Executes during object creation, after setting all properties.
function editTrashCorr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTrashCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderTreshCorr_Callback(hObject, eventdata, handles)
% hObject    handle to sliderTreshCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.temp.trackset.threshcorr = get(hObject,'Value');
update(handles)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderTreshCorr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderTreshCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to editSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSmooth as text
%        str2double(get(hObject,'String')) returns contents of editSmooth as a double


% --- Executes during object creation, after setting all properties.
function editSmooth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to sliderSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.temp.trackset.smooth = round(get(hObject,'Value'));
update(handles)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderSmooth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editMaxima_Callback(hObject, eventdata, handles)
% hObject    handle to editMaxima (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMaxima as text
%        str2double(get(hObject,'String')) returns contents of editMaxima as a double


% --- Executes during object creation, after setting all properties.
function editMaxima_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMaxima (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderMaxima_Callback(hObject, eventdata, handles)
% hObject    handle to sliderMaxima (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.temp.trackset.max = round(get(hObject,'Value'));
update(handles)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderMaxima_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderMaxima (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
v = get(hObject,'String');
v = v{get(hObject,'Value')};
siq.temp.trackset.clumped=v;
update(handles);
% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
v = get(hObject,'String');
v = v{get(hObject,'Value')};
siq.temp.trackset.dividing=v;
update(handles);
% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editWinSize_Callback(hObject, eventdata, handles)
% hObject    handle to editWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editWinSize as text
%        str2double(get(hObject,'String')) returns contents of editWinSize as a double


% --- Executes during object creation, after setting all properties.
function editWinSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderWin_Callback(hObject, eventdata, handles)
% hObject    handle to sliderWin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.temp.trackset.win = round(get(hObject,'Value'));
update(handles)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderWin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderWin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderCell_Callback(hObject, eventdata, handles)
% hObject    handle to sliderCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.actual=getNearestCell(get(handles.sliderCell,'Value'),0,handles);
siq.temp.trackset=siq.trackset(siq.actual);
update(handles)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderCell_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editCell_Callback(hObject, eventdata, handles)
% hObject    handle to editCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editCell as text
%        str2double(get(hObject,'String')) returns contents of editCell as a double


% --- Executes during object creation, after setting all properties.
function editCell_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function saveInt(handles)
global siq

siq.trackset(siq.actual)=siq.temp.trackset;
siq.int(siq.actual)=siq.temp.int;
siq.Center{siq.actual}=siq.temp.Center;
siq.normint(siq.actual)=siq.temp.normint;
siq.size(siq.actual)=siq.temp.size;
siq.s2bg(siq.actual)=siq.temp.s2bg;
siq.bgint(siq.actual)=siq.temp.bgint;
siq.perimeter(siq.actual) = siq.temp.perimeter;
siq.eccentricity(siq.actual) =  siq.temp.eccentricity;
siq.majorAxis(siq.actual) =  siq.temp.majorAxis;
siq.minorAxis(siq.actual) =  siq.temp.minorAxis;
siq.heterogeneity(siq.actual) =  siq.temp.heterogeneity;

% --- Executes on button press in ButtonSave.
function ButtonSave_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
saveInt(handles)

% --- Executes on button press in ButtonDisacrd.
function ButtonDisacrd_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDisacrd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.temp.trackset=siq.trackset(siq.actual);
update(handles)

% --- Executes on button press in ButtonNext.
function ButtonNext_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.actual=getNearestCell(siq.actual,1,handles);
siq.temp.trackset=siq.trackset(siq.actual);
update(handles)

% --- Executes on button press in ButtonBack.
function ButtonBack_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
siq.actual=getNearestCell(siq.actual,-1,handles);
siq.temp.trackset=siq.trackset(siq.actual);
update(handles)

% --- Executes on button press in ButtonSaveNext.
function ButtonSaveNext_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSaveNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
saveInt(handles)
siq.actual=getNearestCell(siq.actual,1,handles);
siq.temp.trackset=siq.trackset(siq.actual);
update(handles)

% --- Executes on button press in ButtonSaveBack.
function ButtonSaveBack_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSaveBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
saveInt(handles)
siq.actual=getNearestCell(siq.actual,-1,handles);
siq.temp.trackset=siq.trackset(siq.actual);
update(handles)


% --- Executes on button press in ButtonSelectCells.
function ButtonSelectCells_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSelectCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq

siq.gimme=1;
CellCoordGUI(siq.normDivided,siq.tracking_coords,siq.min,siq.max)



% --- Executes on button press in ButtonLoad.
function ButtonLoad_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global siq

% load image, merke path
if isfield(siq,'pathname')
    if siq.includeSubfolder 
        [ pathname] = uigetdir([siq.pathname '*.*']);
        filename =[];
    else
        [filename pathname] = uigetfile([siq.pathname '*.*'],'MultiSelect','on');
    end
else
    if siq.includeSubfolder
        [pathname] = uigetdir('*.*','Select an image ');
        filename =  [];
    else
        [filename pathname] = uigetfile('*.*','Select an image ','MultiSelect','on');
    end
end

% show it, reset trackings
if pathname~=0
    % get all images
    if isempty(filename)
    end
    siq.filename=filename;
    siq.pathname=pathname;
    if ~iscell(siq.filename)
        siq.filename = {siq.filename};
    end
    siq.fid = 1;
    
    if numel(siq.filename)>1
        %%%% batch processing
        a = questdlg('Batch processing?','Batch','Yes','No, go throug it manually','Yes');
        if strcmp(a(1),'Y')
            batchprocess(handles)
        else
            pipeline(handles);
        end
    else
    
        pipeline(handles);
    end
end

function batchprocess(handles)
global siq opti

opti.batch=0;
% quant different channel?
% list all common filenames?
a = questdlg('Do you want to quantify also different channels?','Quantify more than one channel?');

% extraChan = {};
if strcmp(a(1),'Y')
    extraChan = inputdlg('Enter common filename extension (i.e. w2.tif, ch02.tif, etc.)','Common file names');
else
    extraChan = {};
end
% extraChan = {'ch01.tif','ch03.tif'};

% ask for segmentation params
if ~isfield(siq,'mseroptions')
    siq.mseroptions.maxVariation = 1;
    siq.mseroptions.minSize = 15;
    siq.mseroptions.maxSize = 300;
    siq.mseroptions.delta = 1;
    siq.mseroptions.invert=0;
end

mseroptions = MserSettings(siq.mseroptions);

if ~numel(mseroptions)
    opti.batch = 0;
    return
end
siq.mseroptions = mseroptions;


%%%%  collect backgrounds
backgrounds = questdlg('Do you need backgrounds?');

thisIsAMovie =0;



if strcmp(backgrounds(1),'Y')
    % first see if there is gain->assume every background should be there
    wl = regexp(siq.filename{siq.fid}, '(_w)(\d{1,2})', 'match');
    if ~isempty(wl)
%         wl = str2double(wl{1}(3:end));
        gainsuggest = getOptiSuggest(siq.pathname,['gain_' wl{1}(2:end) '.png']);
        if exist(gainsuggest,'file')
            thisIsAMovie =1;
        else
            thisIsAMovie =0;
        end
    end
    q=questdlg('Do you want to check them manually?');
    if strcmp(q(1) ,'C')
        return
    end
    opti.batch = 1;
    % go through detect images, and calc all images
    wh = waitbar(0,'Precalculating background images...');

    for fid=1:numel(siq.filename)
        waitbar(fid/numel(siq.filename),wh)
        siq.fid=fid;
        loadsiqimage(handles,0);
        opti.suggest = getOptiSuggest(siq.pathname,siq.filename{siq.fid});%[siq.pathname siq.filename{siq.fid}(1:end-4) '_background.png'];
        % run optimzer store the background and set it
        optimizeBackground(siq.img,[],siq.min,siq.max);
        % go through all other quant images
        for fx = 1:numel(extraChan)
            newfilename = [siq.filename{siq.fid}(1:end-numel(extraChan{fx})) extraChan{fx}];
            opti.suggest = getOptiSuggest(siq.pathname,newfilename);%[siq.pathname newfilename(1:end-4) '_background.png'];
            tempimg=loadimage(strcat(siq.pathname,'/',newfilename),0);
            tempmin=max(0,min(tempimg(:)));
            tempmax=min(1,topmedian(tempimg(:),50));
            optimizeBackground(tempimg,[],tempmin,tempmax);
        end
    
    end
    close(wh);
    siq.fid=1;
    opti.batch = 0;

    if strcmp(q(1),'Y')
        % go through detect images, show stored images
        for fid=1:numel(siq.filename)
            %         if thisIsAMovie
            %             break
            %         end
            siq.fid=fid;
            loadsiqimage(handles,0);
            opti.suggest = getOptiSuggest(siq.pathname,siq.filename{siq.fid});%[siq.pathname siq.filename{siq.fid}(1:end-4) '_background.png'];
            % run optimzer store the background and set it
            siq.bg=optimizeBackground(siq.img,[],siq.min,siq.max);
            if numel(siq.bg )== 1
                siq.bg=0;
            end
            % go through all other quant images
            for fx = 1:numel(extraChan)
                newfilename = [siq.filename{siq.fid}(1:end-numel(extraChan{fx})) extraChan{fx}];
                opti.suggest = getOptiSuggest(siq.pathname,newfilename);%[siq.pathname newfilename(1:end-4) '_background.png'];
                tempimg=loadimage(strcat(siq.pathname,'/',newfilename),0);
                tempmin=max(0,min(tempimg(:)));
                tempmax=min(1,topmedian(tempimg(:),50));
                optimizeBackground(tempimg,[],tempmin,tempmax);
            end
            
        end

    end
   
end
siq.fid=1;





wh = waitbar(0,'Batch processing....');

% go for it
for fid = 1:numel(siq.filename)
    % load img and background
    siq.fid=fid;
    waitbar(fid/numel(siq.filename),wh,'Loading image...');
    loadsiqimage(handles,0);
    
    % load background
    optisuggest=getOptiSuggest(siq.pathname, siq.filename{siq.fid});
    if strcmp(backgrounds(1),'Y') && exist(optisuggest,'file');
        siq.bg=loadimage(optisuggest,0);
    else
        siq.bg=0;
    end
    if thisIsAMovie
        wl = regexp(siq.filename{siq.fid}, '(_w)(\d{1,2})', 'match');
        [img, bg]  = loadimage([siq.pathname siq.filename{siq.fid}],1,[wl{1}(2:end) '.png']);
        siq.normSubtracted = siq.img -bg;
        siq.normDivided = img;
        siq.bg = bg;
        siq.min = min(siq.normDivided(:));
        siq.max= topmedian(siq.normDivided(:),50);
    else
        [siq.normSubtracted,siq.normDivided,siq.min,siq.max]=normalizesiqimg(siq.img,siq.bg);
    end
    % segment it
    waitbar(fid/numel(siq.filename),wh,'Quantifying image...');
    [mserBW,mserProps] = mserSegmentation(siq.normDivided,mseroptions);
    siq.autodetect = mserBW;
    siq.tracking_coords = mserProps;
    
    % quant it
    siqautoquant(siq.bg,siq.img,siq.normDivided,siq.normSubtracted)
    
    % store csv / fcs
    waitbar(fid/numel(siq.filename),wh,'Saving ...');
    exportstuff(siq.pathname,siq.filename{siq.fid},siq.bg,1)
    
    % quant other image(s)
    waitbar(fid/numel(siq.filename),wh,'Quantifying additional image(s)...');
    for fx = 1:numel(extraChan)
        newfilename = [siq.filename{siq.fid}(1:end-numel(extraChan{fx})) extraChan{fx}];
        
        img=loadimage(strcat(siq.pathname,'/',newfilename),0);
        
        % load bg and normalize
        optisuggest=getOptiSuggest(siq.pathname, newfilename);
        if strcmp(backgrounds(1),'Y') && exist(optisuggest,'file');
            bg=loadimage(optisuggest,0);
        else
            bg=0;
        end
        
        [normSubtracted,normDivided] =  normalizesiqimg(img,bg);
        
        % quant
        siqautoquant(bg,img,normDivided,normSubtracted)

        
        % store csv / fcs
        exportstuff(siq.pathname,newfilename,bg,1)
        
    end
    
    

end

delete(wh);
opti.batch=0;

function pipeline(handles)
global siq;
keep = 0;
if isfield(siq,'tracking_coords') && numel(siq.tracking_coords)
    if get(handles.checkboxAutoMode,'Value')
        
        if siq.manual
            keep=1;
        else
            keep=0;
        end
    else

        q = questdlg('Do you want to keep the selected cells?');
        
        % q='N';
        if strcmp(q(1),'Y')
            keep = 1;
        end
    end
end
if ~keep
    siq.tracking_coords = [];
end

loadsiqimage(handles,keep);

ButtonLoadBackground_Callback([],[],handles)
CellCoordGUI(siq.normDivided,siq.tracking_coords,siq.min,siq.max)
        


function loadsiqimage(handles,keep)
global siq
% Einlesen der Grafik
siq.img=loadimage(strcat(siq.pathname,'/',siq.filename{siq.fid}),0);
% siq.img=siq.img*255;
%     siq.img = siq.img(50:450,400:850);
siq.normSubtracted = siq.img;
siq.normDivided = siq.img;
set(handles.textStatus,'String',sprintf('Image %s loaded. Please select background and cells.',siq.filename{siq.fid}))
siq.bg=0;
if ~keep
    siq.autodetect= [];
end
siq.gimme=1;

siq.min=max(0,min(siq.img(:)));
siq.max=min(1,topmedian(siq.img(:),50));

% if max(siq.img(:))>0.99
%     warndlg(sprintf('Image %s seems to be overexposed!',siq.filename{siq.fid}))
%     pause(1)
% end

function bgname=getOptiSuggest(path,file)
global siq
%%% see if a background file already exists somewhere
bgname=[];
pos = regexp([path file], '(_p)(\d{3,4})', 'match');
if numel(pos)>1 || (numel(pos)>0&&~isempty(strfind(file,'gain')))
    % regular TAT file, see if there is a background subfolder
    pos=str2double(pos{1}(3:end));
%     tp = regexp(file, '(_t)(\d{4,5})', 'match');
%     tp = str2double(tp{1}(3:end));
%     wl = regexp(file, '(_w)(\d{1,2})', 'match');
%     wl = str2double(wl{1}(3:end));
    exp = regexp([path file], '\d{6}[A-Z]{2,}\d{1,2}', 'match');
    exp=exp{1};
    bgname = [path '/../background/' sprintf('%s_p%04d/',exp,pos) '/' file(1:end-4) '.png'];
end

% if this file does not exist look for _background images
if ~exist(bgname,'file')
%     if exist([path file(1:end-4) '_background.png'],'file')
        bgname=[path file(1:end-4) '_background.png'];
%     end
else
    siq.thisIsAMovie = 1;
end


% --- Executes on button press in ButtonLoadBackground.
function ButtonLoadBackground_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonLoadBackground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq opti


bgname = getOptiSuggest(siq.pathname, siq.filename{siq.fid});

opti.suggest = bgname;
thisIsAMovie =0;
if get(handles.checkboxAutoMode,'Value')
    b = 'Y';
else
    b = questdlg('Do you already have a background image?','Load background image','Yes','No, calculate it!','No, I do not need one','Yes');
end
if numel(b) && strcmp(b(1),'Y')
    % first see if there is gain->apply normal correction    
    wl = regexp(siq.filename{siq.fid}, '(_w)(\d{1,2})', 'match');
    if ~isempty(wl)
%         wl = str2double(wl{1}(3:end));
        gainsuggest = getOptiSuggest(siq.pathname,['gain_' wl{1}(2:end) '.png']);
        if exist(gainsuggest,'file')
            thisIsAMovie =1;
        else
            thisIsAMovie =0;
        end
    end
    if thisIsAMovie
        %%%% use normal norm
        [img bg] = loadimage([siq.pathname siq.filename{siq.fid}],1,[wl{1}(2:end) '.png']);
        
        
        raw = loadimage([siq.pathname siq.filename{siq.fid}],0);
        
        siq.img = raw;
        siq.normDivided = img;
        siq.bg = bg;
        siq.normSubtracted = raw-bg;
        siq.min=min(siq.normDivided(:));
        siq.max=topmedian(siq.normDivided(:),50);
        set(handles.textStatus,'String',sprintf('Background %s loaded.',[siq.filename{siq.fid}]))

    else
        % if automode
        optisuggest=getOptiSuggest(siq.pathname,siq.filename{siq.fid});
        if exist(optisuggest,'file')
            % load the BG image and make snapshot norm
            siq.bg=loadimage(optisuggest,0);
            set(handles.textStatus,'String',sprintf('Background %s loaded.',optisuggest))
        else
            if ~get(handles.checkboxAutoMode,'Value')
                % select a bg by yourself
                if isfield(siq,'pathname')
                    [siq.backgroundfilename siq.pathname] = uigetfile([siq.pathname '*.*']);
                else
                    [siq.backgroundfilename siq.pathname] = uigetfile({'*.jpg';'*.tif'},'Select an image ');
                end
                
                if siq.backgroundfilename ~= 0
                    siq.bg=loadimage(strcat(siq.pathname,'/',siq.backgroundfilename),0);
                    set(handles.textStatus,'String',sprintf('Background %s loaded.',siq.backgroundfilename))
                end
            end
        end
    end
    
        %%%%% OLD 
%     splittedpath = strsplit('_p',siq.pathname);
%     splittedname = strsplit('.',siq.filename{siq.fid});
%     if exist(optisuggest,'file')%exist( [splittedpath{1}(1:end-9) 'background/' splittedpath{1}(end-9:end) '_p' splittedpath{2}],'dir')
% %         [backgroundfilename pathname] = uigetfile([splittedpath{1}(1:end-9) 'background/' splittedpath{1}(end-9:end) '_p' splittedpath{2} splitted{1} '*.*']);
%         splitted = strsplit('_w',siq.filename{siq.fid});
%         siq.img = loadimage([siq.pathname siq.filename{siq.fid}],1,['w' splitted{2}]);
% %         siq.img = loadimage([siq.pathname siq.filename{siq.fid}],1);
%         siq.bg =0;
%     elseif exist([siq.pathname '/'  splittedname{1} '_background.png'],'file')
%         siq.bg=loadimage(strcat(siq.pathname,'/',[splittedname{1} '_background.png']),0);
%         set(handles.textStatus,'String',sprintf('Background %s loaded.',[siq.pathname '/'  splittedname{1} '_background.png']))
%     else
%         if isfield(siq,'pathname')
%             [siq.backgroundfilename siq.pathname] = uigetfile([siq.pathname '*.*']);          
%         else
%             [siq.backgroundfilename siq.pathname] = uigetfile({'*.jpg';'*.tif'},'Select an image ');
%         end
%         
%         if siq.backgroundfilename ~= 0
%             siq.bg=loadimage(strcat(siq.pathname,'/',siq.backgroundfilename),0);
%             set(handles.textStatus,'String',sprintf('Background %s loaded.',siq.backgroundfilename))
%         end
%     end
    %%%%% OLD 
elseif strcmp(b(5),'c')
    % check if there is already a bg
    if numel(siq.bg)>1
        [temp mini maxi]=optimizeBackground(siq.img,siq.bg,siq.min,siq.max);
        if numel(temp)>1
            siq.bg=temp;
            siq.min = mini;
            siq.max = maxi;
        end
    else
    
        %run optimzer store the background and set it
        siq.bg=optimizeBackground(siq.img,[],siq.min,siq.max);
        if numel(siq.bg )== 1
            siq.bg=0;
        end
    end
else
    siq.bg=0;
end
if ~thisIsAMovie
    [siq.normSubtracted,siq.normDivided,siq.min,siq.max]=normalizesiqimg(siq.img,siq.bg);
end


function [normSubtracted,normDivided,mins,maxs]=  normalizesiqimg(img,bg)

normSubtracted = img-bg;
if numel(bg)>1
    normDivided = img./bg-1;
else
     normDivided = img;
end
mins=max(min(normDivided(:)),-1);
maxs=min(1,topmedian(normDivided(:),50));



% --- Executes on button press in ButtonQuantify.
function ButtonQuantify_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonQuantify (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq opti
handles=guidata(siq.figurehandle);



set(handles.textStatus,'String',sprintf('Quantifying %d cells',size(siq.tracking_coords,1)))
siq.int = zeros(1,size(siq.tracking_coords,1));
siq.Center = cell(1,size(siq.tracking_coords,1));
siq.normint = zeros(1,size(siq.tracking_coords,1));
siq.size = zeros(1,size(siq.tracking_coords,1));
siq.bgint = zeros(1,size(siq.tracking_coords,1));
siq.s2bg = zeros(1,size(siq.tracking_coords,1));
siq.perimeter =zeros(1,size(siq.tracking_coords,1));
siq.eccentricity =  zeros(1,size(siq.tracking_coords,1));
siq.majorAxis =  zeros(1,size(siq.tracking_coords,1));
siq.minorAxis =  zeros(1,size(siq.tracking_coords,1));
siq.heterogeneity =  zeros(1,size(siq.tracking_coords,1));
% set slider
set(handles.sliderCell,'Max',size(siq.tracking_coords,1))
set(handles.sliderCell,'Min',1)
set(handles.sliderCell,'SliderStep',[1 5]/size(siq.tracking_coords,1))

% check if autodetection was used
if isfield(siq,'autodetect') && numel(siq.autodetect)
    a = questdlg(sprintf(['Should I use autosegmenation or do you want to go through it manually?\n\n'...
    'Autosegmentation is using the image as shown to quantify the cells.\n'...
    'If you go through it manually it will use the positions identified by the autosegmentation and will run an Otsu segmentation for each cell\n']),'Auto detection?','Automatic','Manual','Cancel','Manual');   
    if strcmp(a(1),'C')
        return
    end
else
    a = 'M'; 
end

% a='A';
if strcmp(a(1),'A')
    % which image will be quantified?
%     a = questdlg('Should I quantify this image or do you want to quantify another image?','Auto detection?','This one','Another one','This one');
% %     a='T';
%     if strcmp(a(1),'A')
%         % select different image
%         
%         % see if same timepoint different WL exists
%         splitted=strsplit('_z',siq.filename{siq.fid});
%         
%         dsame = dir([siq.pathname '/' splitted{1} '*' siq.filename{siq.fid}(end-3:end)]);
%         channel = numel(dsame)+1;
%         if numel(dsame)
%             channel = listdlg('Promptstring','Detected following images from same timepoint:', 'Name','Show Tree',...
%             'Liststring',[{dsame.name}, 'other'],'ListSize',[300 300],'SelectionMode','single');
%             
%             if isempty(channel)
%                 return
%             end
%             
%         end
%         
%         
%         
%         % load image, merke path
%         if channel> numel(dsame)
%             if isfield(siq,'pathname')
%                 [filename pathname] = uigetfile([siq.pathname '*.*'],'Select image which will be quantified using the same gating');
%             else
%                 [filename pathname] = uigetfile('*.*','Select image which will be quantified using the same gating');
%             end
%         else
%             filename = dsame(channel).name; 
%             pathname = siq.pathname;
%         end
%         % show it, reset trackings
%         if filename == 0
%             return
%         end
%         
% %         [img bg_Ext]=loadimage(strcat(pathname,'/',filename),1,'w1');
%         img=loadimage(strcat(pathname,'/',filename),0);
% 
%         %%% get background
%         opti.suggest = [pathname filename(1:end-4) '_background.png'];
%         
%         b = questdlg('Do you already have a background image?','Load background image','Yes','No, calculate it!','No, I do not need one','Yes');
%         
%         if numel(b) && strcmp(b(1),'Y')
%             % load image, merke path
%             if isfield(siq,'pathname')
%                 [backgroundfilename pathname] = uigetfile([siq.pathname '*.*']);
%             else
%                 [backgroundfilename pathname] = uigetfile({'*.jpg';'*.tif'},'Select an image ');
%             end
%             
%             if backgroundfilename == 0
%                 return
%             end
%             bg=loadimage(strcat(pathname,'/',backgroundfilename),0);
%             
%         elseif strcmp(b(5),'c')
%             
%             
%             
%             %run optimzer store the background and set it
%             bg=optimizeBackground(img,[],min(img(:)),max(img(:)));
%             if numel(bg )== 1
%                 bg=0;
%             end
%             
%         else
%             bg=0;
%         end
%         
%         % do background subtraction
%         if numel(bg)>1
%             normSubtracted = img-bg;
%             normDivided = img./bg-1;
%         else
%             normSubtracted = img;
%             normDivided = img;
%         end
% %         bg = bg_Ext;
%     else
        normSubtracted = siq.normSubtracted;
        normDivided = siq.normDivided;
        img = siq.img;
        pathname= siq.pathname;
        filename = siq.filename{siq.fid};
        bg = siq.bg;
%     end
    
    siq.manual = 0;
    siqautoquant(bg,img,normDivided,normSubtracted)
    
    % show some stats or something?
    
    exportstuff(pathname,filename,bg,0);
    
    if numel(siq.filename)>1 && siq.fid <numel(siq.filename)
        % next image?
        q = questdlg(sprintf('Continue with next image?\n%s',siq.filename{siq.fid+1}));
        if strcmp(q(1),'Y')
            siq.fid = siq.fid+1;
            pipeline(handles);
        end
    
    end
else
    if isempty(siq.tracksetTemplate)
        trackset = CreateNewTracksettings;
    else
        if get(handles.checkboxAutoMode,'Value')
            trackset = siq.tracksetTemplate;
        else
            trackset = CreateNewTracksettings(siq.tracksetTemplate);
        end
    end
    
    if ~numel(trackset)
        return
    end
    siq.tracksetTemplate = trackset;
    siq.trackset = repmat(trackset,1,size(siq.tracking_coords,1));
    siq.manual = 1;
    wh = waitbar(0,'Quantifying...');
    % itereate over coords
    for i = 1:size(siq.tracking_coords,1)
        %     i/size(siq.tracking_coords,1)
        waitbar(i/size(siq.tracking_coords,1),wh)
        x=siq.tracking_coords(i,1);
        y=siq.tracking_coords(i,2);
        
        % detect
        Center = cellSegmentation(siq.img,x,y,trackset);
        
        % optimize segmentation
        if sum(Center(:))<trackset.MinArea || sum(Center(:))>trackset.MaxArea && ~strcmp(trackset.threshmethod,'Ellipse')
            [Center, trackset]=optimizeSegmentation(Center,siq.img,x,y,trackset);
            siq.trackset(i)=trackset;
        end
        
        %quant
        subimg = extractSubImage(siq.normSubtracted, x, y, trackset.win);
        subimgDivided = extractSubImage(siq.normDivided, x, y, trackset.win);
        
        siq.int(i) = sum(subimg(Center));
        siq.Center{i} = Center;
        siq.normint(i) = sum(subimgDivided(Center));
        siq.size(i) = sum(Center(:));
        props = regionprops(Center,'Perimeter','Eccentricity','MajorAxisLength','MinorAxisLength');
        siq.perimeter(i) =props.Perimeter;
        siq.eccentricity(i) =  props.Eccentricity;
        siq.majorAxis(i) =  props.MajorAxisLength;
        siq.minorAxis(i) =   props.MinorAxisLength;
        siq.heterogeneity(i) =  std(subimg(Center));
        % s2b
        if numel(siq.bg)==1
            siq.bgint(i)=NaN;
            siq.s2bg(i)=NaN;
        else
            subimg = extractSubImage(siq.bg, x, y, trackset.win);
            siq.bgint(i) = sum(subimg(Center));
            subimg = extractSubImage(siq.img, x, y, trackset.win);
            siq.s2bg(i) =  sum(subimg(Center)) / siq.bgint(i);
        end
    end
    
    delete(wh)
    
    %update slider
    set(handles.sliderCell,'Max',size(siq.tracking_coords,1))
    set(handles.sliderCell,'Min',1)
    
    
    % show first quant
    siq.actual = 1;
    siq.temp.trackset=siq.trackset(siq.actual);
    set(handles.textStatus,'String','Done. updating...')
    update(handles)

end

function quantifyDifferentImage
global siq opti
% check if auto or manual, just select one or more images, they will all be
% quantified

% see if same timepoint different WL exists
splitted=strsplit_qtfy('_w',siq.filename{siq.fid});

dsame = dir([siq.pathname '/' splitted{1} '*' siq.filename{siq.fid}(end-3:end)]);
channel = numel(dsame)+1;
if numel(dsame)
    channel = listdlg('Promptstring','Detected following images from same timepoint:', 'Name','Show Tree',...
        'Liststring',[{dsame.name}, 'other'],'ListSize',[300 300],'SelectionMode','multiple');
    
    if isempty(channel)
        return
    end
    
end

% remove other, if selected more than one
if numel(channel)>1 && max(channel) >numel(dsame)
    channel(channel ==max(channel))=[];
end


% load image, merke path
if max(channel)> numel(dsame)
    if isfield(siq,'pathname')
        [filename pathname] = uigetfile([siq.pathname '*.*'],'Select image which will be quantified using the same gating','MultiSelect','on');
    else
        [filename pathname] = uigetfile('*.*','Select image which will be quantified using the same gating','MultiSelect','on');
    end
else
    filename = {dsame(channel).name};
    pathname = siq.pathname;
end

% show it, reset trackings
if pathname == 0
    return
end

if ~iscell(filename)
    filename = {filename};
end
for fid = 1:numel(filename)
    %         [img bg_Ext]=loadimage(strcat(pathname,'/',filename),1,'w1');
    img=loadimage(strcat(pathname,'/',filename{fid}),0);
    
    %%% get background
    opti.suggest = getOptiSuggest(pathname,filename{fid});% [pathname filename{fid}(1:end-4) '_background.png'];
    if isfield(siq,'automode') && siq.automode
        b='Y';
    else
        b = questdlg(sprintf('Do you already have a background image for image\n%s?',filename{fid}),'Load background image','Yes','No, calculate it!','No, I do not need one','Yes');
    end
    
    if numel(b) && strcmp(b(1),'Y')
        % load image, merke path
        loaded=0;
        if isfield(siq,'pathname')
            try 
                wl = strfind(filename{fid},'_w');
                [img mybg myg]=loadimage(strcat(pathname,'/',filename{fid}),1,filename{fid}(wl+1:end));
                %                 figure;imagesc(mybg);
                %                 figure;imagesc(myg);
                if isempty(mybg)
                    if exist(opti.suggest,'file')
                        % try to load _background file
                        % FUCKING WORKAROUND
                        [pathname backgroundfilename ext] = fileparts(opti.suggest);
                        backgroundfilename = [backgroundfilename ext];
                    else
                    error('Background not found')
                    end
                else
                    loaded=1;
                    backgroundfilename=1;
                    bg=0;
                end
            catch e
                [backgroundfilename pathname] = uigetfile([siq.pathname '*.*']);
            end
        else
            [backgroundfilename pathname] = uigetfile({'*.jpg';'*.tif'},'Select an image ');
        end
        
        if backgroundfilename == 0
            continue
        end
        if ~loaded
            bg=loadimage(strcat(pathname,'/',backgroundfilename),0);
        end
        
        %%%%% TODO: get background by yourself
        %%%%% TODO lege den schei� code mit dem backgroundbutton zusammen!
    elseif strcmp(b(5),'c')
        %run optimzer store the background and set it
        bg=optimizeBackground(img,[],min(img(:)),max(img(:)));
        if numel(bg )== 1
            bg=0;
        end
        
    else
        bg=0;
    end
    

    
    % do background subtraction
    if numel(bg)>1
        normSubtracted = img-bg;
        normDivided = img./bg-1;
    else
        normSubtracted = img;
        normDivided = img;
    end
    
    % this is for the FACS vs IMAGING stuff:
%     gain = loadimage([pwd '\gain_eYFP.png'],0)*255;
%     bg = loadimage([pathname '/' filename{fid}(1:end-4) '_background.png'],0);
%     normDivided = ((img - bg) ./ gain);
% [img, bg, gain]=loadimage([pathname filename{fid}],1,filename{fid}(end-6:end));
% normDivided = img;
    
    % check auto or manual
    if siq.manual
        res.x= siq.tracking_coords(:,1);
        res.y= siq.tracking_coords(:,2);
        res.intensity = zeros(1,size(siq.tracking_coords,1));
        res.normalizedIntensity = zeros(1,size(siq.tracking_coords,1));
        res.size = zeros(1,size(siq.tracking_coords,1));
        res.perimenter= zeros(1,size(siq.tracking_coords,1));
        res.eccentricity= zeros(1,size(siq.tracking_coords,1));
        res.majorAxis= zeros(1,size(siq.tracking_coords,1));
        res.minorAxis= zeros(1,size(siq.tracking_coords,1));
        res.heterogeneity= zeros(1,size(siq.tracking_coords,1));
        res.backgroundIntensity = zeros(1,size(siq.tracking_coords,1));
        res.signalToBackground = zeros(1,size(siq.tracking_coords,1));
        res.MeanBackgroundSignal = repmat(mean(bg(:)),1,numel(res.intensity));
        
        
        wh = waitbar(0,'Quantifying...');
        % itereate over coords
        for i = 1:size(siq.tracking_coords,1)
            waitbar(i/size(siq.tracking_coords,1),wh)
            x=siq.tracking_coords(i,1);
            y=siq.tracking_coords(i,2);
            
            % detect
%             Center = cellSegmentation(siq.img,x,y,siq.trackset(i));
            Center = siq.Center{i};
            
            %quant
            subimg = extractSubImage(normSubtracted, x, y, siq.trackset(i).win);
            subimgDivided = extractSubImage(normDivided, x, y, siq.trackset(i).win);
            
            res.intensity(i) = sum(subimg(Center));
            res.normalizedIntensity(i) = sum(subimgDivided(Center));
            res.size(i) = sum(Center(:));
            props = regionprops(Center,'All');
            res.eccentricity(i) = props.Eccentricity;
            res.perimeter(i) = props.Perimeter;
            res.majorAxis(i)= props.MajorAxisLength;
            res.minorAxis(i)= props.MinorAxisLength;
            res.heterogeneity(i)= std(subimg(Center));
            
            % s2b
            if numel(bg)==1
                res.backgroundIntensity(i)=NaN;
                res.signalToBackground(i)=NaN;
            else
                subimg = extractSubImage(bg, x, y, siq.trackset(i).win);
                res.backgroundIntensity(i) = sum(subimg(Center));
                subimg = extractSubImage(img, x, y, siq.trackset(i).win);
                res.signalToBackground(i) =  sum(subimg(Center)) / res.backgroundIntensity(i);
            end
        end
        delete(wh)
        
        
        [FileName,PathName] = uiputfile('*.csv','Enter filename to export csv-file',[pathname filename{fid}(1:end-4) '.csv']);
        %prevent overwriting
        if FileName == 0
            continue;
        end
        pause(0.1)
%         toBase(res)
        
        ezwrite([PathName FileName],res,'\t')
        if ispc
            % excel write
            % excel write
            towrite = cell(numel(res.size)+2,numel(fieldnames(res)));
            towrite{1,1} = siq.filename{siq.fid};
            ccounter=0;
            for f = fieldnames(res)'
                ccounter=ccounter+1;
                towrite{2,ccounter}=cell2mat(f);
                towrite(3:end,ccounter) = num2cell(res.(cell2mat(f)));
            end
            try
            xlswrite([PathName FileName(1:end-3) 'xls'],towrite)
            catch e
                'xls not possible'
            end
        end
        
    else
        siqautoquant(bg,img,normDivided,normSubtracted)
        exportstuff(pathname,filename{fid},bg,0);
    end

end

function exportstuff(PathName,filename,bg,force)
global siq
% export stuff
FileName = [filename(1:end-4) '.csv'];
if ~force
    [FileName,PathName] = uiputfile('*.csv','Enter filename to export csv-file',[PathName FileName]);
    %prevent overwriting
    if FileName == 0
        return;
    end
end
res.x= siq.tracking_coords(:,1);
res.y= siq.tracking_coords(:,2);
res.intensity = siq.int;
res.normalizedIntensity = siq.normint;
res.size = siq.size;
res.backgroundIntensity = siq.bgint;
res.signalToBackground = siq.s2bg;
res.MeanBackgroundSignal = repmat(mean(bg(:)),1,numel(res.intensity));
res.perimenter= siq.perimeter;
res.eccentricity= siq.eccentricity;
res.majorAxis= siq.majorAxis;
res.minorAxis= siq.minorAxis;
res.heterogeneity= siq.heterogeneity;
res.MeanBackgroundSignal = repmat(mean(bg(:)),1,numel(res.intensity));
pause(0.1)

if isempty(res.x)
    set(siq.statushandle,'String','Nothing to save. Zero objects.')
    return
end

try
ezwrite([PathName FileName],res,'\t')
if ispc
    % excel write
    towrite = cell(numel(res.size)+2,numel(fieldnames(res)));
    towrite{1,1} = siq.filename{siq.fid};
    ccounter=0;
    for f = fieldnames(res)'
        ccounter=ccounter+1;
        towrite{2,ccounter}=cell2mat(f);
        towrite(3:end,ccounter) = num2cell(res.(cell2mat(f)));
    end
    try 
    xlswrite([PathName FileName(1:end-3) 'xls'],towrite)
    catch e
        'xls not possible'
    end
end
catch e
    rethrow(e)
end
set(siq.statushandle,'String','Done.')

function siqautoquant(bg,img,normDivided,normSubtracted)
global siq
%init 
siq.int = zeros(1,size(siq.tracking_coords,1));
siq.Center = cell(1,size(siq.tracking_coords,1));
siq.normint = zeros(1,size(siq.tracking_coords,1));
siq.size = zeros(1,size(siq.tracking_coords,1));
siq.bgint = zeros(1,size(siq.tracking_coords,1));
siq.s2bg = zeros(1,size(siq.tracking_coords,1));
siq.perimeter = zeros(1,size(siq.tracking_coords,1));
siq.eccentricity = zeros(1,size(siq.tracking_coords,1));
siq.majorAxis = zeros(1,size(siq.tracking_coords,1));
siq.minorAxis = zeros(1,size(siq.tracking_coords,1));
siq.heterogeneity = zeros(1,size(siq.tracking_coords,1));


% use auto detection but ONLY of region!!
props=regionprops( bwconncomp(siq.autodetect,4),'PixelIdxList','centroid','perimeter','MinorAxisLength','MajorAxisLength','Eccentricity');
% validIDs = sub2ind(size(siq.autodetect),round(siq.tracking_coords(:,2)),round(siq.tracking_coords(:,1)));
counter = 0;
% iterate over coords
for c = 1:numel(props)
    if ~ismember(props(c).Centroid,siq.tracking_coords,  'rows')
        continue
    end
    counter = counter+1;
    % quantify
    siq.int(counter) = sum(normSubtracted(props(c).PixelIdxList));
    siq.normint(counter) = sum(normDivided(props(c).PixelIdxList));
    siq.size(counter) = numel(props(c).PixelIdxList);
    siq.perimeter(counter) =props(c).Perimeter;
    siq.eccentricity(counter) = props(c).Eccentricity;
    siq.majorAxis(counter) = props(c).MajorAxisLength;
    siq.minorAxis(counter) = props(c).MinorAxisLength;
    siq.heterogeneity(counter) = std(normSubtracted(props(c).PixelIdxList));
    % s2b
    if numel(bg)==1
        siq.bgint(counter)=NaN;
        siq.s2bg(counter)=NaN;
    else
        %             subimg = extractSubImage(siq.bg, x, y, trackset.win);
        siq.bgint(counter) = sum(bg(props(c).PixelIdxList));
        %             subimg = extractSubImage(siq.img, x, y, trackset.win);
        siq.s2bg(counter) =  sum(img(props(c).PixelIdxList)) / siq.bgint(counter);
    end
end


function update(handles)
global siq cellcoord
set(handles.textStatus,'String','Updating...')

trackset=siq.temp.trackset;

% update all sliders
set(handles.sliderWin,'Value',trackset.win)
set(handles.editWinSize,'String',num2str(trackset.win))
set(handles.sliderMaxima,'Value',trackset.max)
set(handles.editMaxima,'String',num2str(trackset.max))
set(handles.sliderSmooth,'Value',trackset.smooth)
set(handles.editSmooth,'String',num2str(trackset.smooth))
set(handles.sliderTreshCorr,'Value',trackset.threshcorr)
set(handles.editTrashCorr,'String',num2str(trackset.threshcorr))
set(handles.sliderTreshCorr,'Value',trackset.threshcorr)
set(handles.editTrashCorr,'String',num2str(trackset.threshcorr))
set(handles.sliderCell,'Value',siq.actual)
set(handles.editCell,'String',num2str(siq.actual))
set(handles.listbox1,'Value',find(strcmp( get(handles.listbox1,'String'), trackset.clumped)));
set(handles.listbox2,'Value',find(strcmp( get(handles.listbox2,'String'), trackset.dividing)));


x=siq.tracking_coords(siq.actual,1);
y=siq.tracking_coords(siq.actual,2);
    

% show cell
Center = cellSegmentation(siq.img,x,y,trackset);
subimg = extractSubImage(siq.normSubtracted, x, y, trackset.win);
subimgDivided = extractSubImage(siq.normDivided, x, y, trackset.win);

%quantify
siq.temp.int = sum(subimg(Center));
siq.temp.normint = sum(subimgDivided(Center));
siq.temp.size = sum(Center(:));
siq.temp.Center = Center;
props = regionprops(Center,'Perimeter','Eccentricity','MajorAxisLength','MinorAxisLength');
siq.temp.perimeter =props.Perimeter;
siq.temp.eccentricity = props.Eccentricity;
siq.temp.majorAxis = props.MajorAxisLength;
siq.temp.minorAxis = props.MinorAxisLength;
siq.temp.heterogeneity = std(subimg(Center));

% background

if numel(siq.bg)==1
    siq.temp.bgint=NaN;
    siq.temp.s2bg=NaN;
else
    subimgbg = extractSubImage(siq.bg, x, y, trackset.win);
    siq.temp.bgint = sum(subimgbg(Center));
    subimg = extractSubImage(siq.img, x, y, trackset.win);
    siq.temp.s2bg =  sum(subimg(Center)) / siq.temp.bgint;
%     siq.temp.s2bg = siq.temp.int / siq.temp.bgint;
end

axes(handles.('axesDetect'));
subplot(handles.('axesDetect'));
imagesc(Center)
axis equal
axis off
colormap(gray);

BWoutline = bwperim(Center);
Segout = subimg;
Segout(BWoutline) = max(subimg(:))*1.2;


subplot(handles.('axesQuant'));
imagesc(Segout)
axis equal
axis off
colormap(gray);
set(handles.textStatus,'String',sprintf('Cell %d Intensity: %2.3f / Signal To Background %2.3f',siq.actual,siq.temp.int,siq.temp.s2bg))

% try to update cellcoord
try
    try
        delete(siq.arrowhandle(1))
        delete(siq.arrowhandle(2))
        delete(cellcoord.linehandle(1))
        delete(cellcoord.linehandle(2))
    catch e
        %nevermind
    end
    siq.arrowhandle(1)=plot(cellcoord.axeshandle,x,y,'o','color','red');
    [arrowx,arrowy] = dsxy2figxy(cellcoord.axeshandle, [x-30 x-5],size(siq.img,1)-[y-30 y-5]);
    siq.arrowhandle(2) = annotation(cellcoord.figurehandle,'Arrow',arrowx,arrowy,'color','red');
    cellcoord.linehandle(1) = line([1 size(cellcoord.im,2)],[y y],'color','black','linestyle','--','parent',get(cellcoord.figurehandle,'CurrentAxes'));
    cellcoord.linehandle(2) = line([x x],[1 size(cellcoord.im,1)],'color','black','linestyle','--','parent',get(cellcoord.figurehandle,'CurrentAxes'));
catch e
    %nevermind - stupid arrow -.-
end 
% set focus for keyboard shortcut
uicontrol(handles.ButtonNext);


function nearest=getNearestCell(tp,step,handles)
global siq

timepoints=1:size(siq.tracking_coords,1);

[~, c]=min(abs(timepoints-tp));

if c+step<=0
    nearest=timepoints(1);
elseif c+step>numel(timepoints)
    nearest=timepoints(end);
else
    nearest=timepoints(c+step);
end

% --- Executes on button press in ButtonExport.
function ButtonExport_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonExport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%%%% WHY NOT LINKED TO EXPORTSTUFF ?!?? %%%

global siq

if siq.manual
    saveInt(handles)
end

[FileName,PathName] = uiputfile('*.csv','Enter filename to export csv-file',[siq.pathname siq.filename{siq.fid}(1:end-4) '.csv']);
%prevent overwriting
if FileName == 0
    return;
end 
res.x= siq.tracking_coords(:,1);
res.y= siq.tracking_coords(:,2);
res.intensity = siq.int;
res.normalizedIntensity = siq.normint;
res.size = siq.size;
res.backgroundIntensity = siq.bgint;
res.signalToBackground = siq.s2bg;
res.MeanBackgroundSignal = repmat(mean(siq.bg(:)),1,numel(siq.int));
res.perimenter= siq.perimeter;
res.eccentricity= siq.eccentricity;
res.majorAxis= siq.majorAxis;
res.minorAxis= siq.minorAxis;
res.heterogeneity= siq.heterogeneity;
toBase(res)
pause(0.1);
ezwrite([PathName FileName],res,'\t')
if ispc
    % excel write
        % excel write
    towrite = cell(numel(res.size)+2,numel(fieldnames(res)));
    towrite{1,1} = siq.filename{siq.fid};
    ccounter=0;
    for f = fieldnames(res)'
        ccounter=ccounter+1;
        towrite{2,ccounter}=cell2mat(f);
        towrite(3:end,ccounter) = num2cell(res.(cell2mat(f)));
    end
    try
    xlswrite([PathName FileName(1:end-3) 'xls'],towrite)
    catch e
        'xls not possible'
    end
end

if numel(siq.filename)>1 && siq.fid <numel(siq.filename) && siq.manual
    % next image?
    q = questdlg(sprintf('Continue with next image?\n%s',siq.filename{siq.fid+1}));
    if strcmp(q(1),'Y')
        siq.fid = siq.fid+1;
        pipeline(handles);
    end
    
end

% --- Executes on button press in ButtonDelete.
function ButtonDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq cellcoord
set(handles.textStatus,'String','deleting cell')
siq.trackset(siq.actual)=[];
siq.Center(siq.actual)=[];
siq.int(siq.actual)=[];
siq.normint(siq.actual) = [];
siq.size(siq.actual)=[];
siq.s2bg(siq.actual)=[];
siq.bgint(siq.actual)=[];
siq.perimeter(siq.actual) = [];
siq.eccentricity(siq.actual) = [];
siq.majorAxis(siq.actual) = [];
siq.minorAxis (siq.actual)= [];
siq.heterogeneity(siq.actual) = [];
siq.tracking_coords(siq.actual,:)=[];
siq.actual=getNearestCell(siq.actual,0,handles);

% delete arrow
try
    delete(siq.arrowhandle(1))
    delete(siq.arrowhandle(2))
    delette(cellcoord.linehandle)
catch e
    %nevermind
end

% also delete in cellcoord and redraw it
try
    cellcoord.tracking_coords(siq.actual,:)=[];
    cellcoord.redraw();
catch e
%     rethrow(e)
    fprintf('cell coord gui seems not to be present?\n')
end
%update slider
set(handles.sliderCell,'Max',size(siq.tracking_coords,1))
set(handles.sliderCell,'Min',1)
set(handles.sliderCell,'SliderStep',[1 5]/size(siq.tracking_coords,1))
update(handles)


%%% SAVE BUTTON FROM TOOLBAR
% --------------------------------------------------------------------
function uipushtool2_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
if ~isfield(siq,'pathname')
    set(handles.textStatus,'String','Nothing to save...')
    return
end
[file path]=uiputfile('*.mat','Enter filename to save quantifications',[siq.pathname siq.filename{siq.fid}(1:end-4) '.mat']);

if file == 0 
    return
end
try
save([path file],'siq')
catch e
    errordlg('Could not save file. Please try again.')
    return
end
set(handles.textStatus,'String','Quantifications saved.')

%%% LOAD BUTTON FROM TOOLBAR
% --------------------------------------------------------------------
function uipushtool1_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq

if isfield(siq,'pathname')
    [file path ]=uigetfile([siq.pathname '*.mat'],'Select precalculated Quantifications');
else
    [file path ]=uigetfile('*.mat');
end

if file == 0 
    set(handles.textStatus,'String','Nothing to load...')
    return
end
load([path file],'siq')
siq.figurehandle = handles.figure1;
siq.quantify = @ButtonQuantify_Callback;

if numel(siq.trackset)>1
    siq.manual = 1;
else
    siq.manual =0;
end

if siq.manual && ~isfield(siq,'Center')
    wh=waitbar(0);
    siq.Center=cell(1,numel(siq.int));
    for i = 1:size(siq.tracking_coords,1)
        %     i/size(siq.tracking_coords,1)
        waitbar(i/size(siq.tracking_coords,1),wh)
        x=siq.tracking_coords(i,1);
        y=siq.tracking_coords(i,2);
        
        % detect
        Center = cellSegmentation(siq.img,x,y,siq.trackset(i));
        
       
        siq.Center{i} = Center;
    end
    delete(wh)
end

update(handles)
set(handles.textStatus,'String','Quantifications loaded.')
%update slider
set(handles.sliderCell,'Max',size(siq.tracking_coords,1))
set(handles.sliderCell,'Min',1)


% --- Executes on button press in ButtonPlotSizeVsIntensity.
function ButtonPlotSizeVsIntensity_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonPlotSizeVsIntensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
if ~isempty(siq.autodetect) && siq.manual == 0
    siqautoquant(siq.bg,siq.img,siq.normDivided,siq.normSubtracted)
end 
if numel(siq.int)>1
    figure;
    plot(siq.size,siq.int,'*')
    xlabel('Cell Size (px)')
    ylabel('Cell Intensity')
end


% --- Executes on button press in Button_Histogram.
function Button_Histogram_Callback(hObject, eventdata, handles)
% hObject    handle to Button_Histogram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
if ~isempty(siq.autodetect) && siq.manual == 0
    siqautoquant(siq.bg,siq.img,siq.normDivided,siq.normSubtracted)
end 
if numel(siq.int)>1
    bins = inputdlg('How many bins do you want?','Histogram',1,{'10'});
    if numel(bins)
    figure;
    hist(siq.int,str2double(bins{1}))
    xlabel('Intensity')
    ylabel('Frequency')
    end
end


% --- Executes on button press in Button_otherImage.
function Button_otherImage_Callback(hObject, eventdata, handles)
% hObject    handle to Button_otherImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
quantifyDifferentImage();


% --- Executes on button press in ButtonOverlay.
function ButtonOverlay_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonOverlay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq

% select image to show overlay
% see if same timepoint different WL exists
splitted=strsplit_qtfy('_w',siq.filename{siq.fid});

dsame = dir([siq.pathname '/' splitted{1} '*' siq.filename{siq.fid}(end-3:end)]);
channel = numel(dsame)+1;
if numel(dsame)
    channel = listdlg('Promptstring','Detected following images from same timepoint:', 'Name','Show Tree',...
        'Liststring',[{dsame.name}, 'other'],'ListSize',[300 300],'SelectionMode','single');
    
    if isempty(channel)
        return
    end
    
end
% load image, merke path
if channel> numel(dsame)
    if isfield(siq,'pathname')
        [filename pathname] = uigetfile([siq.pathname '*.*'],'Select image which will be quantified using the same gating');
    else
        [filename pathname] = uigetfile('*.*','Select image which will be quantified using the same gating');
    end
else
    filename = dsame(channel).name;
    pathname = siq.pathname;
end

if filename == 0
    return
end
    
% toOverlay=loadimage(strcat(pathname,'/',filename),1,'w1.png');
toOverlay=loadimage(strcat(pathname,'/',filename),0);
overmax=topmedian(toOverlay(:),100);
if siq.manual
    % show manual tracksets
    wh = waitbar(0,'Quantifying...');
    % itereate over coords
    for i = 1:size(siq.tracking_coords,1)
        waitbar(i/size(siq.tracking_coords,1),wh)
        x=siq.tracking_coords(i,1);
        y=siq.tracking_coords(i,2);
        
        % detect
%         Center = cellSegmentation(siq.img,x,y,siq.trackset(i));
        Center = siq.Center{i};
        win =siq.trackset(i).win;
        subimg = extractSubImage(toOverlay, x, y, win);
        cnter = bwperim(Center);
        subimg(cnter) = overmax*1.1;
        toOverlay(round(max(1,y-floor(win/2)):min(size(toOverlay,1),y+floor(win/2))),round(max(1,x-floor(win/2)):min(size(toOverlay,2),x+floor(win/2)))) = subimg;        
        
        
    end
    delete(wh)
else
    % show automatic trackset
    toOverlay(bwperim(siq.autodetect)) = overmax*1.1;
    
end
figure;
imagesc(toOverlay)
axis off
axis equal
colormap(gray(256));
caxis([min(toOverlay(:)),overmax])


% --- Executes on button press in Button_Boxplot.
function Button_Boxplot_Callback(hObject, eventdata, handles)
% hObject    handle to Button_Boxplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% needs folder of csvs
global siq
if isfield(siq,'pathname')
    folder =siq.pathname;
else
    folder = pwd;
end
folder = uigetdir(folder);
if folder == 0
    return
end

files =dir([folder '/**.csv']);

selected = listdlg('ListString',{files.name},'ListSize',[400 300],'name','Select all csv files for the boxplot');

if isempty(selected)
    return
end

% select field
file = ezread([folder '/' files(selected(1)).name],'\t');
allfields=fieldnames(file);
allfields{end+1} = 'intbyarea';
f = listdlg('ListString',allfields,'ListSize',[300 200],'name','Select field for the boxplot','SelectionMode','single');

f= allfields{f};

% needs field to plot
% f = 'signalToBackground';
% f = 'normalizedIntensity';
% f = 'intensity';
% f='size';
% f = 'intbyarea';
% f= 'MeanBackgroundSignal';
% f='backgroundIntensity';
data=[];
groups=[];
allmedians=[];
ywerte=[];
xwerte = [];
titles={};
for i = selected
    
    file = ezread([folder '/' files(i).name],'\t');
    file.intbyarea = file.intensity./file.size;
    data = [data torow(file.(f))];
    groups = [groups i+zeros(1,numel(file.(f)))];
    allmedians=[allmedians median(file.(f))];
    titles{end+1} = [files(i).name(1:end-4) sprintf(' | %1.3f | %d',median(file.(f)),numel(file.(f)))];
    xwerte(end+1) = str2double(files(i).name(1:2));
    ywerte(end+1) = mean(file.(f));
end


allmedians = allmedians-1;
allmedians= allmedians/max(allmedians);
% for i=1:numel(allmedians)
%     titles{i} = [titles{i} sprintf(' | %0.2f',allmedians(i))];
% end
figure('Position',[403 49 560 636]);
oldpos = get(gcf,'position');
% set(gcf,'position',[oldpos(1:2) 800 400])
boxplot(data,groups,'labels',titles,'labelorientation','inline')
ylabel(f)

% set(gca,'ylim',[0.9 1.5])


if strcmp(f,'signalToBackground')
    ylabel('Signal to Background')
    for i=1:numel(allmedians)
        text(i-0.1,max(get(gca,'ylim'))+0.02,sprintf('%3.0f%%',allmedians(i)*100))
    end

else
    ylabel(f)
end


% --- Executes on button press in checkboxAutoMode.
function checkboxAutoMode_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxAutoMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
% Hint: get(hObject,'Value') returns toggle state of checkboxAutoMode
siq.automode = get(hObject,'Value');


% --- Executes on button press in checkboxIncludeSubfolder.
function checkboxIncludeSubfolder_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxIncludeSubfolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global siq
% Hint: get(hObject,'Value') returns toggle state of checkboxIncludeSubfolder
siq.includeSubfolder = get(hObject,'Value') ;
