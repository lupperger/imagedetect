function varargout = CellMovie(varargin)
% CELLMOVIE M-file for CellMovie.fig
%      CELLMOVIE, by itself, creates a new CELLMOVIE or raises the existing
%      singleton*.
%
%      H = CELLMOVIE returns the handle to a new CELLMOVIE or the handle to
%      the existing singleton*.
%
%      CELLMOVIE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CELLMOVIE.M with the given input arguments.
%
%      CELLMOVIE('Property','Value',...) creates a new CELLMOVIE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CellMovie_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CellMovie_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CellMovie

% Last Modified by GUIDE v2.5 11-May-2011 15:31:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CellMovie_OpeningFcn, ...
                   'gui_OutputFcn',  @CellMovie_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CellMovie is made visible.
function CellMovie_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CellMovie (see VARARGIN)

% Choose default command line output for CellMovie
handles.output = hObject;
set(handles.figure1,'KeyPressFcn',@myFunction)
set(handles.sliderTime,'KeyPressFcn',@myFunction)

% Update handles structure
guidata(hObject, handles);

global vtset cmset

% set wavelength popupmenu
cmset.channelinfo=[];
wllist={['nonFluor: ' vtset.results{vtset.treenum}.nonFluor(1).settings.wl]};
cmset.channelinfo(1) = 1;
for i=1:numel(vtset.results{vtset.treenum}.detects)
    wllist{end+1} = ['Detection: ' vtset.results{vtset.treenum}.detects(i).settings.wl];
    cmset.channelinfo(end+1) = i;
end
for i=1:numel(vtset.results{vtset.treenum}.quants)
    wllist{end+1} = ['Quantification: ' vtset.results{vtset.treenum}.quants(i).settings.wl];
    cmset.channelinfo(end+1) = i;
end
% wllist=unique(wllist);
set(handles.popupmenuWavelengths,'String',wllist);
set(handles.popupmenuWavelengths,'Value',1)

% set winsize
set(handles.editWinSize,'String',60);

% position me
scnsize = get(0,'ScreenSize');
oldpos = get(handles.figure1,'Position');
set(handles.figure1,'Position', [1000 scnsize(4)-450 oldpos(3) oldpos(4)]);



function myFunction(src,evnt)
%this function takes in two inputs by default
global vtset
%src is the gui figure
%evnt is the keypress information
 
%this line brings the handles structures into the local workspace
%now we can use handles.cats in this subfunction!
handles = guidata(src);
k=( evnt.Key );

if strcmp(k,'space')
    vtset.trackCell();
end

% UIWAIT makes CellMovie wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CellMovie_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure


varargout{1} = handles.output;


% --- Executes on slider movement.
function sliderTime_Callback(hObject, eventdata, handles)
% hObject    handle to sliderTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

global subimages curtree cells maxval minval showoutlines outlines vtset linehandle


i = round(get(handles.sliderTime,'Value'));

showimg=subimages{i};

%%% show outlines?
if showoutlines
    % check if an outline exists for timepoint and cell   
    celloutline=outlines.cellmask(curtree.timepoint(cells(i))==outlines.timepoint & curtree.cellNr(cells(i))==outlines.cellNr);
    if numel(celloutline)>0
        BWoutline=bwperim(cell2mat(celloutline));
        showimg(BWoutline)=max(subimages{i}(:))*1.2;
    end
end

% show it
image((showimg-minval) / (maxval-minval) * 64);
colormap(gray);
% set(gca,'XTick',[]);
% set(gca,'YTick',[]);
axis off


% show line in timecourse
hold on;
if linehandle~=0
    try
    delete(linehandle)
    catch exception
        fprintf('stupid CellMovie Linehandle error\n')
    end
end
linehandle=line([curtree.(vtset.timemode)(cells(i))/vtset.timemodifier curtree.(vtset.timemode)(cells(i))/vtset.timemodifier], [0 max(get(get(vtset.hplot,'CurrentAxes'),'yLim'))],'parent',get(vtset.hplot,'CurrentAxes'));
hold off;
% display info

vtset.selected.cell=curtree.cellNr(cells(i));
vtset.selected.timepoint=curtree.timepoint(cells(i));

text = sprintf('cell %d, tp %d', curtree.cellNr(cells(i)), curtree.timepoint(cells(i)));
set(handles.textInfo,'String',text);

% --- Executes during object creation, after setting all properties.
function sliderTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in exportcellmovie.
function exportcellmovie_Callback(hObject, eventdata, handles)
% hObject    handle to exportcellmovie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global subimages curtree cells maxval minval

% toBase(subimages);

% mov = avifile('3dplot.avi');


% display info
text = sprintf('creating movie');
set(handles.textInfo,'String',text);


% h=figure;
% set(gca,'nextplot','replacechildren');



% show movie and save it
for i = 1:numel(subimages)
    fname=sprintf('img%0d.png', i);
%      imwrite((subimages{i}-minval) / (maxval-minval) * 64 /255, fname);
%     image((subimages{i}-minval) / (maxval-minval) * 64);
%     colormap(gray);
%     axis off;
%     pause(0.2)
%     mop=getframe(h);
%     mov = addframe(mov,mop);
end
% mov = close(mov);



%display info
text = sprintf('done');
set(handles.textInfo,'String',text);


% --- Executes on button press in checkboxShowOutlines.
function checkboxShowOutlines_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShowOutlines (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxShowOutlines
global showoutlines

if showoutlines
    showoutlines=0;
else
    showoutlines=1;
end

scrImages_Callback(0,0, handles)


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global linehandle
try

    if linehandle~=0
        delete(linehandle)
    end

    delete(hObject);
catch exception
    sprintf('stupid CellMovie error\n');
    delete(hObject);
end
    
    


% --- Executes on selection change in popupmenuWavelengths.
function popupmenuWavelengths_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuWavelengths (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuWavelengths contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuWavelengths
global vtset cmset

%%%% show image of actual cell (vtset.selected) at actual timepoint 

% set cmset as in vtset.selected
cmset.tree = vtset.treenum;
cmset.cell = vtset.selected.cell;
cmset.timepoint = vtset.selected.timepoint;
cmset.wavelength = get(handles.popupmenuWavelengths,'String');
cmset.wavelength = cmset.wavelength{get(handles.popupmenuWavelengths,'Value')};

%%% update time slider
timepoints = vtset.results{vtset.treenum}.nonFluor(1).timepoint(vtset.results{vtset.treenum}.nonFluor(1).cellNr == cmset.cell);
set(handles.sliderTime,'Min',min(timepoints))
set(handles.sliderTime,'Max',max(timepoints))
set(handles.sliderTime,'Value',cmset.timepoint)

showimage(handles);


function showimage(handles)
global cmset vtset
% load image
if strcmp(cmset.wavelength(1),'n')
    % load from non fluor
    filename = vtset.results{cmset.tree}.nonFluor(1).filename{vtset.results{cmset.tree}.nonFluor(1).cellNr == cmset.cell & vtset.results{cmset.tree}.nonFluor(1).timepoint == cmset.timepoint};
    filename = [vtset.results{cmset.tree}.settings.imgroot filename];
    img = imageCache(filename);
    x = vtset.results{cmset.tree}.nonFluor(1).X(vtset.results{cmset.tree}.nonFluor(1).cellNr == cmset.cell & vtset.results{cmset.tree}.nonFluor(1).timepoint == cmset.timepoint);
    y = vtset.results{cmset.tree}.nonFluor(1).Y(vtset.results{cmset.tree}.nonFluor(1).cellNr == cmset.cell & vtset.results{cmset.tree}.nonFluor(1).timepoint == cmset.timepoint);
elseif strcmp(cmset.wavelength(1),'D')
    % load from detection channel
    filename = vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).filename{...
        vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr == cmset.cell ...
        & vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == cmset.timepoint};
    filename = [vtset.results{cmset.tree}.settings.imgroot filename];
    img = imageCache(filename);
    x= vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).X(...
        vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr == cmset.cell ...
        & vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == cmset.timepoint);
    y= vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).Y(...
        vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr == cmset.cell ...
        & vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == cmset.timepoint);
    
else
    % load from quantification channel
    filename = vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).filename{...
        vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr == cmset.cell ...
        & vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == cmset.timepoint};
    filename = [vtset.results{cmset.tree}.settings.imgroot filename];
    img = imageCache(filename);
    x= vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).X(...
        vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr == cmset.cell ...
        & vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == cmset.timepoint);
    y= vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).Y(...
        vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr == cmset.cell ...
        & vtset.results{cmset.tree}.quants(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == cmset.timepoint);
end

% use zoom, cellID, outline as set
winsize = get(handles.sliderWinSize,'Value');
showCellIDs = get(handles.checkboxShowCellIDs,'Value');
showOutlines = get(handles.checkboxShowOutlines,'Value');

% extract subimage
subimg = extractSubImage(img,x,y,winsize);

if showOutlines && ~strcmp(cmset.wavelenght(1),'n')
    % overlay outline
    % only if not non fluor
    
    
end

axes(handles.axes1)
imagesc(subimg);
axis off
axis equal

if showCellIDs
% plot all cellIDs in subimagec
    zellen = vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr(vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == timepoint)';
    xsub = floor(trackset.x-(floor(trackset.win/2)));
    ysub = floor(trackset.y-(floor(trackset.win/2)));
    
    if numel(zellen)>1
        for zelle = zellen
            X = vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).X(vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == timepoint...
                & vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr== zelle) - xsub;
            Y = vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).Y(vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).timepoint == timepoint...
                & vtset.results{cmset.tree}.detects(cmset.channelinfo(get(handles.popupmenuWavelengths,'Value'))).cellNr== zelle) - ysub;
            text(X,Y,num2str(zelle),'color','r');
        end
    end
end



% --- Executes during object creation, after setting all properties.
function popupmenuWavelengths_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuWavelengths (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderWinSize_Callback(hObject, eventdata, handles)
% hObject    handle to sliderWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderWinSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editWinSize_Callback(hObject, eventdata, handles)
% hObject    handle to editWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editWinSize as text
%        str2double(get(hObject,'String')) returns contents of editWinSize as a double


% --- Executes during object creation, after setting all properties.
function editWinSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editWinSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkboxShowCellIDs.
function checkboxShowCellIDs_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxShowCellIDs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxShowCellIDs

