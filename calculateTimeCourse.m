function [alldata, alltps, order] = calculateTimeCourse(varargin)
global vtset;

cell=varargin{1};
quantChannel=varargin{2};
plotmode=varargin{3};
smoothit=varargin{4};
if numel(varargin)>4
    tree=varargin{5};
    timemode = varargin{6};
    timemodifier = varargin{7};
else
    tree= vtset.treenum;
    timemode = vtset.timemode;
    timemodifier = vtset.timemodifier;
end


% dont delete, but udpate x, ydata and style
alltps=vtset.results{tree}.quants(quantChannel).(timemode)(vtset.results{tree}.quants(quantChannel).cellNr==cell & vtset.results{tree}.quants(quantChannel).active==1)/timemodifier;
alldata=vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==cell & vtset.results{tree}.quants(quantChannel).active==1);
[alltps, order] = sort(alltps);
alldata = alldata(order);

if isempty(alldata)
    return
end

% calculate production rate for each cell
switch plotmode
%cases for plotCell
    case 'mean'
       alltimepoints = vtset.results{tree}.quants(quantChannel).timepoint(vtset.results{tree}.quants(quantChannel).cellNr==cell);
       size = vtset.results{tree}.detects(vtset.results{tree}.quants(quantChannel).settings.detectchannel).size(vtset.results{tree}.detects(vtset.results{tree}.quants(quantChannel).settings.detectchannel).cellNr==cell & ismember(vtset.results{tree}.detects(vtset.results{tree}.quants(quantChannel).settings.detectchannel).timepoint,alltimepoints));    
       alldata = vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==cell)./size;
       
    case 'prodrate'
%         alldata = derivative_cwt(torow(alldata),'gaus1',1,1/1000)/1000;
        alldata = [alldata(1) diff(alldata)];
    case 'concentration'
        % area of doubling cell will grow linear up to 2^(2/3)
        loine=linspace(1,2^(2/3),numel(alldata));
        alldata = alldata./(loine.^(3/2)); 
        if cell == 1
            loine=linspace(1,2^(2/3),21);
            alldata = alldata./(loine(end-numel(alldata)+1:end).^(3/2)); 
        end
        
    case 'size'
        try
            alldetectchans = vtset.results{tree}.quants(quantChannel).detectchannel(vtset.results{tree}.quants(quantChannel).cellNr==cell & vtset.results{tree}.quants(quantChannel).active==1);
            alltimepoints = vtset.results{tree}.quants(quantChannel).timepoint(vtset.results{tree}.quants(quantChannel).cellNr==cell &vtset.results{tree}.quants(quantChannel).active ==1);
            size = zeros(1,numel(alldata));
            for tp = 1:numel(alltps)
                size(tp) = vtset.results{tree}.detects(alldetectchans(tp)).size(vtset.results{tree}.detects(alldetectchans(tp)).active==1 & vtset.results{tree}.detects(alldetectchans(tp)).cellNr==cell & vtset.results{tree}.detects(alldetectchans(tp)).timepoint == alltimepoints(tp));
            end
            alldata = size;
        catch e
%             rethrow(e)
            fprintf('Structure inconsistency at cell %d channel %d\n',cell,quantChannel)
        end
        
    case 'cumulative'
        sumofmothers=0;
        trace = rootpath(cell);
         if numel(trace)>1
            for t = trace(1:end-1)
                alldata2= vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==t & vtset.results{tree}.quants(quantChannel).active==1);
                lastmother= vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==floor(t/2) & vtset.results{tree}.quants(quantChannel).active==1);
                if t == 1
                    lastmother=0;
                end
                dadiff2= diff([lastmother(end)/2 alldata2]);
                if strfind(vtset.plotmode,'cumulativeNN')
                    dadiff2(dadiff2<0)=0;
                    dadiff2(1) = diff([lastmother(end)/2 alldata2(1)]);
                end
                sumofmothers = sumofmothers+sum(dadiff2);
            end
         end
        lastmother=vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==floor(cell/2) & vtset.results{tree}.quants(quantChannel).active==1);
        if cell == 1
            lastmother=0;
        end
        dadiff=diff([lastmother(end)/2 alldata]);
        if strfind(plotmode,'cumulativeNN')
            dadiff(dadiff<0)=0;
            dadiff(1) = diff([lastmother(end)/2 alldata(1)]);
        end
        alldata = cumsum(dadiff)+sumofmothers;
    case 'cumulativeNN'
        sumofmothers=0;
        trace = rootpath(cell);
         if numel(trace)>1
            for t = trace(1:end-1)
                alldata2= vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==t & vtset.results{tree}.quants(quantChannel).active==1);
                lastmother= vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==floor(t/2) & vtset.results{tree}.quants(quantChannel).active==1);
                if t == 1
                    lastmother=0;
                end
                dadiff2= diff([lastmother(end)/2 alldata2]);
                if strfind(vtset.plotmode,'cumulativeNN')
                    dadiff2(dadiff2<0)=0;
                    dadiff2(1) = diff([lastmother(end)/2 alldata2(1)]);
                end
                sumofmothers = sumofmothers+sum(dadiff2);
            end
         end
        lastmother=vtset.results{tree}.quants(quantChannel).int(vtset.results{tree}.quants(quantChannel).cellNr==floor(cell/2) & vtset.results{tree}.quants(quantChannel).active==1);
        if cell == 1
            lastmother=0;
        end
        dadiff=diff([lastmother(end)/2 alldata]);
        if strfind(plotmode,'cumulativeNN')
            dadiff(dadiff<0)=0;
            dadiff(1) = diff([lastmother(end)/2 alldata(1)]);
        end
        alldata = cumsum(dadiff)+sumofmothers;
    case 'linearregression'
        p = polyfit(alltps, alldata, 1);
        alldata = polyval(p, alltps);
        
    case 'int'
        % nix weiter
        
    case 'prod'
        alldata = derivative_cwt(torow(alldata),'gaus1',1,1/1000)/1000;
       
%     case 'conc'
%         loine=linspace(1,2^(2/3),numel(alldata));
%         alldata=torow(alldata)./(loine.^(3/2));
   
    case 'doublethes'
        line = linspace(medianfirst(alldata,3),medianfirst(alldata,3)*2,numel(alldata));
        alldata = alldata-line;
    otherwise 
        errordlg(sprintf('Time Course mode %s unknown',plotmode))

            
           
end

if smoothit
    alldata = moving_average(alldata,5);
end

end

