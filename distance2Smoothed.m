% get local outliers
function rm=distance2Smoothed(alldata)

alldata(isinf(alldata))=[];
alldata(isnan(alldata))=[];

%%% TODO: what is this?
alldata(alldata == 0)=[];


rm =zeros(numel(alldata),1);
estimated=zeros(numel(alldata),1);
for p = 2:numel(alldata)-1
    data = alldata(find(alldata)~=p);
    %         tps = alltps(find(alldata)~=p);
    
%     sm = smooth(data);
    sm = moving_average(data,5);
    try
    estimate = (sm(p)+sm(p-1))/2;
    actual = alldata(p);
    estimated(p) = estimate;
    rm(p)=(estimate-actual)^2;
    catch e
        'muh'
    end
end