function results = initializeTreeData(settings,treelist,tree,vtsettree)
%%
% only god and i know what this code is doing ...

% init the detects
results.detects = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'X',{},'Y',{},'filename',{},'cellmask',{},'size',{},'trackset',{},'active',{},'inspected',{});

% find the unique combination of detect/quant channel and thresh method
detects=cell(1,numel({settings.channelsettings.detect}));
quants=cell(1,numel({settings.channelsettings.quant}));
for i=1:numel({settings.channelsettings.detect})
    detects{1,i} = [ settings.channelsettings(i).detect '---' settings.channelsettings(i).settings.threshmethod];
    detects{2,i} = num2str(i);
    quants{i} = [settings.channelsettings(i).quant '...' settings.channelsettings(i).detect '---' settings.channelsettings(i).settings.threshmethod];
end

unidetects = unique({detects{1,:}});

% set the channels
for j = 1:numel(unidetects)
    splitted = strsplit_qtfy('---',unidetects{j});
    results.detects(j).settings.wl = splitted{1};
    % find correct threshmethod
    results.detects(j).settings.trackset = settings.channelsettings(strcmp(unidetects{j},{detects{1,:}})).settings;
    results.detects(j).settings.normalize = settings.corrDetect;
end
%%
% init the quants
results.quants = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'int',{},'active',{},'detectchannel',{},'inspected',{});
quants = unique(quants);
for j = 1:numel(quants)
    splitted = strsplit_qtfy('...',quants{j});
    results.quants(j).settings.wl = splitted{1};
    % find the correct detection channel
    results.quants(j).settings.detectchannel = find(strcmp(unidetects,splitted{2}));
    results.quants(j).settings.normalize = settings.corrQuant;
end

%%
% finally the nonFluor
results.nonFluor = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'active',{},'stopReason',{});
results.nonFluor(1).settings.wl = settings.extNonFluor;


%%
% create morph


%%
% and the settings
results.settings.treeroot=treelist.root{(tree)};
results.settings.treefile= treelist.treefile{(tree)};
results.settings.imgroot=settings.imgroot;
results.settings.exp=settings.exp;
results.settings.channelsettings = settings.channelsettings;
results.settings.anno=settings.anno;
results.settings.anno2=settings.anno2;
results.settings.restrictcells=settings.restrictcells;
results.settings.corrDetect=settings.corrDetect;
results.settings.corrQuant=settings.corrQuant;
results.settings.moviestart=settings.moviestart;% timestamp of first image in pos001
results.description='';
