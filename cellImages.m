function cellImages

global vtset

cell = vtset.selected.cell ;
curtree = vtset.results.quants{vtset.treenum};
win = vtset.results.trackset.win;

% get cell ids
cellids = find(curtree.cellNr==cell);

cols = 10;
rows = ceil(numel(cellids)/cols);

figure;
% iterate over them
for i=1:numel(cellids)
    cellid = cellids(i);
    pos = curtree.positionIndex(cellid);
    timepoint = curtree.timepoint(cellid);
    x = curtree.X(cellid);
    y = curtree.Y(cellid);
    % load image
    imgfile = genImageFile(vtset.results.imgroot, vtset.results.exp, ...
        pos, timepoint, vtset.results.extDetect);
   	image = imageCache(imgfile);
    % get subimage
    subimg = extractSubImage(image, x, y, win);
    % show it
    subplot(rows,cols,i);
    imagesc(subimg);
    title(['tp ' num2str(timepoint)]);
end
