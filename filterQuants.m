function out=filterQuants(quants, filters)

% go through the trees
for i=1:numel(quants)
    % get out the actual tree name
    treefile = quants{i}.treefile;
    p1 = strfind(treefile, 'p');
    p2 = strfind(treefile, '.');
    treename = treefile(p1(1)+1:p2(1)-3);
    % find filters for this tree
    valid = find(strcmp(filters.tree,treename));
    
    % apply filters
    curtree = quants{i};
    copy = logical(ones(numel(quants{i}.int),1));
    for j=1:numel(valid)
        % get filter entries
        filterCell = filters.cell(valid(j));
        filterTP = filters.tp(valid(j));
        % find entry
        entry = find(curtree.cellNr==filterCell & curtree.timepoint==filterTP);
        % empty?
        if numel(entry) == 0
            fprintf('Warning: No entries for tree %s, cell %d, tp %d\n', treename, filterCell, filterTP);
        else
            copy(entry) = 0;
        end
    end
    
    % copy it
    out{i}.cellNr = quants{i}.cellNr(copy);
    out{i}.timepoint = quants{i}.timepoint(copy);
    out{i}.positionIndex = quants{i}.positionIndex(copy);
    out{i}.X = quants{i}.X (copy);
    out{i}.Y = quants{i}.Y(copy);
    out{i}.backgroundX = quants{i}.backgroundX(copy);
    out{i}.backgroundY = quants{i}.backgroundY(copy);
    out{i}.treefile = quants{i}.treefile;
    out{i}.int = quants{i}.int(copy);
    out{i}.mean = quants{i}.mean(copy);
    out{i}.size = quants{i}.size(copy);
    out{i}.cellmask = quants{i}.cellmask(copy);
%    out{i}.trackset = quants{i}.trackset(copy);
end