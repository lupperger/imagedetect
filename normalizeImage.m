%%% Normalizes an image for illumination and background intensity
% normalizeImage(origImage, illuImage, gain, offset)
function normed=normalizeImage(varargin)
origImage=varargin{1};
illuImage=varargin{2};
% normed = origImage ;
% return

if numel(varargin)>2 && numel(varargin)<5
    %%% new method
    gain =varargin{3};
    offset = varargin{4};
    normed = ((origImage - illuImage) ./ gain);% / median(illuImage(:));

elseif numel(varargin)>4
%     %%% approximate background image by using gain/offset
    gain =varargin{3};
    offset = varargin{4};
%     mymean = varargin{5};
    temp = (origImage - mean(illuImage(:)).*gain - offset);
%     temp = temp - median(temp(:));
    normed = temp ./ gain;
%     normed = normed - median(normed(:));


% for testing gain eval
% normed = origImage;
else
    %%% method without gain/offset :
    
    bg = median( origImage(:) ) ;
    normed = ((origImage ./ illuImage ) - 1 ) * bg; % background is
%     normalized by median background signal (if background varies between
%     positions) but cellular signal is not affected


%     normed = ((origImage ./ illuImage ) - 1 ) ; % normalizes cell signal
%     dependent on background signal


%     normed = origImage - illuImage; % only subtraction
end

