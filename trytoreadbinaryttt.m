%% read file
% tttfile = '/nfs/cmbdata/schwarzfischer/100301PH4_p009-001MB.ttt';
% tttfile ='/home/tttcomputer/TTTExport/TTTfiles/2010/100226DL3/100226DL3_p036/100226DL3_p040-005DL.ttt';
% tttfile='/home/tttcomputer/TTTExport/TTTfiles/2010/100205AF1/100205AF1_p068/100205AF1_p068-002AF.ttt';
tic
fid = fopen(tttfile);

% - 4 bytes, int: version number
version = fread(fid,1,'uint32');

if version<15
    errordlg('TTT file version < 15 --- not supported!');
    return
end

% - 4 bytes, int: last timepoint of the experiment
lastTP = fread(fid,1,'uint32');

% - 4 bytes, int: the number of tracks in the tree
numberOfTracks = fread(fid,1,'uint32');

% - 2 bytes, short int: maximum wavelength (-> mwl)
if version >= 18
    maximumWL = fread(fid,1,'uint16');
else 
    maximumWL = 5;
end

% - 2 bytes, short int: tree finished (0 = false, 1 = true)
if version >=19
    treeFinished = fread(fid,1,'uint16');
end

% beware of the swap!
if numberOfTracks>1000
    errordlg('more than 1000 tracks!??!')
    fclose(fid);
    return;
end
counter=0;
tracks = cell(numberOfTracks,1);
for track = 1:numberOfTracks
    % - for each track of the tree:
    %   - 4 bytes, int: track number
    tracks{track}.trackNo = fread(fid,1,'uint32');
    %   - 4 bytes, int: starting timepoint
    tracks{track}.startTP = fread(fid,1,'uint32');
    %   - 4 bytes, int: stopping timepoint (can be -1, indicating "not set")
    tracks{track}.stopTP = fread(fid,1,'int32');
    %   - 2 bytes, short int: stop reason (see enum TrackStopReason for values)
    tracks{track}.stopReason = fread(fid,1,'uint16');
    %   - 4 bytes, int: number of trackpoints that were set for this track (how many marks did the user set)
    tracks{track}.NumberOfTrackpoints = fread(fid,1,'uint32');
    
    % beware of the swap!
    if tracks{track}.NumberOfTrackpoints>5000
        errordlg('more than 5000 trackpoints!??!')
        fclose(fid);
        return;
    end

    trackpoints = cell(tracks{track}.NumberOfTrackpoints,1);
    for trackpoint = 1:tracks{track}.NumberOfTrackpoints
        counter=counter+1;
        %   - for each trackpoint of the current track:
        %     - 2 bytes, short int: timepoint of this trackpoint
        trackpoints{trackpoint}.timepoint = fread(fid,1,'uint16');
        %     - 4 bytes, float: X coordinate
        trackpoints{trackpoint}.X = fread(fid,1,'float');
        %     - 4 bytes, float: Y abscisse
        trackpoints{trackpoint}.Y = fread(fid,1,'single');
        %     - 4 bytes, float: Z coordinate (not used currently)
        trackpoints{trackpoint}.Z = fread(fid,1,'single');
        %     - 4 bytes, float: X background coordinate (only if a background track was set)
        trackpoints{trackpoint}.backgroundX = fread(fid,1,'single');
        %     - 4 bytes, float: Y background coordinate (only if a background track was set)
        trackpoints{trackpoint}.backgroundY = fread(fid,1,'single');
        %     - 1 byte, char:  tissue type (see trackpoint.h/cellproperties.h)
        trackpoints{trackpoint}.tissueType = fread(fid,1,'uint8');
        %     - 1 byte, char:  general type (see trackpoint.h/cellproperties.h)
        trackpoints{trackpoint}.generalType = fread(fid,1,'uint8');
        %     - 1 byte, char:  lineage (see trackpoint.h)
        trackpoints{trackpoint}.lineage = fread(fid,1,'uint8');
        %     - 2 bytes, short int: nonadherent (boolean: 0 = false, >0 = true)
        trackpoints{trackpoint}.nonadherent = fread(fid,1,'uint16');
        %     - 2 bytes, short int: freefloating (boolean)
        trackpoints{trackpoint}.freefloating = fread(fid,1,'uint16');
        %     - 4 bytes, float:  cell radius (default = 25)
        if version <= 15
            trackpoints{trackpoint}.cellRadius = fread(fid,1,'uint8');
        else
            trackpoints{trackpoint}.cellRadius = fread(fid,1,'single');
        end
        %     - 2 bytes, short int: Endomitosis (boolean)
        trackpoints{trackpoint}.endomitosis = fread(fid,1,'uint16');
        %     - mwl*2 bytes, each short int: wavelength (1-mwl) on (boolean)
        trackpoints{trackpoint}.wavelengths = ones(0,maximumWL);
        if version <= 17
%             for wl = 1:maximumWL
                trackpoints{trackpoint}.wavelengths(1:maximumWL) = fread(fid,maximumWL,'uint16');
%             end
        else
%             for wl = 1:maximumWL
                trackpoints{trackpoint}.wavelengths(1:maximumWL) = fread(fid,maximumWL,'uint8');
%             end
        end
        %     - 4 bytes, long: additional attribute, for various purposes
        trackpoints{trackpoint}.additionalAttribute = fread(fid,1,'uint64');
        %     - 2 bytes, short int: positionn in which the trackpoint actually was set
        if version >= 17
            trackpoints{trackpoint}.position = fread(fid,1,'uint16');
        else
            trackpoints{trackpoint}.position = 0;
        end
    end
    tracks{track}.trackpoints=trackpoints;
end

posi = ftell(fid);
fseek(fid,0,'eof');
if posi == ftell(fid)
    fprintf('done with reading %s\n',tttfile);
else
    fprintf('!!! file %s not fully read\n',tttfile);
end
fclose(fid);
toc