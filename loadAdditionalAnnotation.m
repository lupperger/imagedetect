function varargout = loadAdditionalAnnotation(varargin)
% LOADADDITIONALANNOTATION MATLAB code for loadAdditionalAnnotation.fig
%      LOADADDITIONALANNOTATION, by itself, creates a new LOADADDITIONALANNOTATION or raises the existing
%      singleton*.
%
%      H = LOADADDITIONALANNOTATION returns the handle to a new LOADADDITIONALANNOTATION or the handle to
%      the existing singleton*.
%
%      LOADADDITIONALANNOTATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOADADDITIONALANNOTATION.M with the given input arguments.
%
%      LOADADDITIONALANNOTATION('Property','Value',...) creates a new LOADADDITIONALANNOTATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before loadAdditionalAnnotation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to loadAdditionalAnnotation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help loadAdditionalAnnotation

% Last Modified by GUIDE v2.5 08-Nov-2013 18:17:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loadAdditionalAnnotation_OpeningFcn, ...
                   'gui_OutputFcn',  @loadAdditionalAnnotation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before loadAdditionalAnnotation is made visible.
function loadAdditionalAnnotation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to loadAdditionalAnnotation (see VARARGIN)

% Choose default command line output for loadAdditionalAnnotation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% UIWAIT makes loadAdditionalAnnotation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = loadAdditionalAnnotation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenuAnnotation.
function popupmenuAnnotation_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuAnnotation contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuAnnotation


% --- Executes during object creation, after setting all properties.
function popupmenuAnnotation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuAnnotationNumber.
function popupmenuAnnotationNumber_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuAnnotationNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuAnnotationNumber contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuAnnotationNumber


% --- Executes during object creation, after setting all properties.
function popupmenuAnnotationNumber_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuAnnotationNumber (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonOK.
function pushbuttonOK_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
annotation = get(handles.popupmenuAnnotation,'String');
annotation = annotation{get(handles.popupmenuAnnotation,'Value')};
try
    mapmissingannotation(vtset.treenum,annotation)
catch e
    errordlg(sprintf('Annotation %s could not be added:\n%s',annotation,e.message))
    return
end
annoNr = get(handles.popupmenuAnnotationNumber,'Value');
if annoNr == 1
    vtset.results{vtset.treenum}.settings.anno = annotation;
else
    vtset.results{vtset.treenum}.settings.anno2 = annotation;
end
vtset.redrawtree();
close(handles.figure1)

% --- Executes on button press in pushbuttonCancel.
function pushbuttonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(handles.figure1)
