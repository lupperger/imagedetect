function fmatplayer(varargin)
global fmat

if numel(varargin)<1
    if isfield(fmat,'folder') && isdir(fmat.folder)
        folder= uigetdir(fmat.folder);
    else
        folder = uigetdir;
    end
    if folder==0
        return
    end
    fmat.folder=folder;
    fmat.hgui=[];
    fmat.pos = 1;
    fmat.wl = 1;
    fmat.exp = strsplit_qtfy('\',fmat.folder);
    fmat.exp = fmat.exp{end};
    fmat.showsegmentation=0;
    fmat.showbackground=0;
    fmat.showfeature = 1;
    fmat.fileExtension = 'png';
    fmat.files = dir([fmat.folder '/' sprintf('%s_p%04d',fmat.exp,fmat.pos) '/*' sprintf('%d',fmat.wl) '.' fmat.fileExtension]);
    
elseif strcmp(varargin{1},'update')
    fmat.files = dir([fmat.folder '/' sprintf('%s_p%04d',fmat.exp,fmat.pos) '/*' sprintf('%d',fmat.wl) '.' fmat.fileExtension]);
else
    fmat.folder = varargin{1};
end

% fmat.folder=folder;


% figure
if isfield(fmat,'h') && ishandle (fmat.h)
    figure(fmat.h)
else
    fmat.h=figure;
    fmat.i=1;
    set(fmat.h,'KeyPressFcn',@myfmatkey)
    %%% GUI
    fmat.hgui(end+1)=uicontrol(fmat.h,'position',[1 1 100 20],'style','checkbox','String','Show segmentation','Value',0,'callback','global fmat;fmat.showsegmentation=~fmat.showsegmentation;fmatplayer(fmat.folder)');
    fmat.hgui(end+1)=uicontrol(fmat.h,'position',[120 1 100 20],'style','checkbox','String','Show background','Value',0,'callback','global fmat;fmat.showbackground=~fmat.showbackground;fmatplayer(fmat.folder)');
end
title('Updating...')
drawnow

% show actual image
img = loadimage([fmat.folder '/' sprintf('%s_p%04d',fmat.exp,fmat.pos) '/' fmat.files(fmat.i).name],0);
toshow=img;
% toshow(:,:,2)=img;
% toshow(:,:,3)=img;
if fmat.showsegmentation
    filename = strsplit_qtfy('_w',fmat.files(fmat.i).name);
    filename = [filename{1} '_w0.jpg'];
    outline = loadimage([fmat.folder '/segmentation/' sprintf('%s_p%04d',fmat.exp,fmat.pos) '/' filename],0);
%     toshow(bwperim(outline),1)=1;%topmedian(img(:),50);
%     toshow(bwperim(outline),2)=0;%topmedian(img(:),50);
%     toshow(bwperim(outline),3)=0;%topmedian(img(:),50);
    
    

    temp=img;
    img(bwperim(outline))=0;
    temp(bwperim(outline))=1;
    toshow(:,:,1) = temp;
    toshow(:,:,2) = img;
    toshow(:,:,3)= img;

elseif fmat.showbackground
    [~,bg] = loadimage([fmat.folder '/' sprintf('%s_p%04d',fmat.exp,fmat.pos) '/' fmat.files(fmat.i).name],1);

    toshow = bg;
end


    

imagesc(toshow);
axis equal
robustmin = medianfirst(sort(toshow(:)),400);
robustmax = topmedian(toshow(:),400);
caxis([robustmin robustmax]);
title(strrep(fmat.files(fmat.i).name,'_','\_'))

if fmat.showfeature && fmat.showsegmentation
    props = regionprops(imfill(outline==1,'holes'),'eccentricity','area','centroid');
    field = 'Eccentricity';
    field2 = 'Area';
    for c = 1:numel(props)
        text(props(c).Centroid(1)+5,props(c).Centroid(2),['\color{White}' sprintf('Ecc %3.2f\nArea %03d',props(c).(field),props(c).(field2))])
    end
    
end

function myfmatkey(src,evnt)
global fmat

% handles = guidata(src);
k= evnt.Key; %k is the key that is pressed

if strcmp(k,'f') 
    fmat.i = min(numel(fmat.files), fmat.i+1);
    fmatplayer(fmat.folder)
elseif strcmp(k,'g')
    fmat.i = min(numel(fmat.files),fmat.i+10);
    fmatplayer(fmat.folder)
elseif strcmp(k,'s')
    fmat.i = max(1,fmat.i-1);
    fmatplayer(fmat.folder)
elseif strcmp(k,'a')
    fmat.i = max(1,fmat.i-10);
    fmatplayer(fmat.folder)
elseif strcmp(k,'uparrow')
    fmat.pos = fmat.pos +1;
    fmatplayer('update')
elseif strcmp(k,'downarrow')
    fmat.pos = fmat.pos -1;
    fmatplayer('update')
elseif strcmp(k,'1')
    fmat.wl =1;
    fmatplayer('update')
elseif strcmp(k,'2')
    fmat.wl =2;
    fmatplayer('update')
elseif strcmp(k,'3')
    fmat.wl =3;
    fmatplayer('update')
elseif strcmp(k,'4')
    fmat.wl =4;
    fmatplayer('update')
elseif strcmp(k,'5')
    fmat.wl =5;
    fmatplayer('update')
elseif strcmp(k,'6')
    fmat.wl =6;
    fmatplayer('update')
elseif strcmp(k,'7')
    fmat.wl =7;
    fmatplayer('update')
elseif strcmp(k,'8')
    fmat.wl =8;
    fmatplayer('update')
elseif strcmp(k,'9')
    fmat.wl =9;
    fmatplayer('update')
    
elseif strcmp(k,'0')
    fmat.wl =0;
    fmatplayer('update')

    % elseif strcmp(k,'g')
%     fmat.i = fmat.i+1;
end

