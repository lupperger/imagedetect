function gatherimages_csv(res,tree,ch,x)
%%
global vtset

% imgroot = vtset.results{tree}.settings.imgroot;
% positionstring = vtset.results{tree}.settings.positionString;
% imgstring = vtset.results{tree}.settings.imgString;
% imgargument= vtset.results{tree}.settings.imgArgument;

if strcmp(ch(1),'D')
    channel = vtset.results{tree}.detects;
elseif strcmp(ch(1),'Q')
    channel = vtset.results{tree}.quants;
else
    channel = vtset.results{tree}.nonFluor;
end


% needed res fields: cellNr,timepoint,X,Y,
%optional: positionIndex,stopReason,absoluteTime

if ~isfield(res,'positionIndex');
    res.positionIndex = ones(numel(res.cellNr),1);
end


if ~isfield(res,'stopReason');
    res.stopReason =zeros(numel(res.cellNr),1);
end

if ~isfield(res,'absoluteTime');
    res.absoluteTime = res.timepoint;
end

for posi = unique(res.positionIndex)'
    fprintf('gathering position %d files %s...',posi,channel(x).settings.wl)
    
    
    
    wll = strsplit_qtfy('.',channel(x).settings.wl);
    wl = str2double(wll{1}(2:end));
    ext = wll{2};
    
%     posfolder = sprintf(positionstring,posi);
    posFolder = [vtset.results{tree}.settings.exp vtset.results{tree}.settings.filesettings.separator [vtset.results{tree}.settings.filesettings.matrix{1,3} [repmat('0',1,(vtset.results{tree}.settings.filesettings.matrix{1,2}-1)) num2str(posi)]]];
    idx=find(res.positionIndex == posi)';
    

    for id = idx
        % check if there is a image for this specific wavelength at given
        % timepoint
        
        

        
        % store everything
        channel(x).cellNr(end+1) = res.cellNr(id);
        tp=res.timepoint(id);
        channel(x).timepoint(end+1) = tp;
        channel(x).absoluteTime(end+1) = res.absoluteTime(id);
        channel(x).positionIndex(end+1) = posi;
        %%% TODO integrate additional/z-value 
        imgName = generateFilename(vtset.results{tree}.settings.exp, posi, tp, wl, 1, ext, vtset.results{tree}.settings.filesettings);
        %channel(x).filename{end+1} = [posfolder '/' sprintf(imgstring,eval(imgargument)) channel(x).settings.wl];
        channel(x).filename{end+1} = [posFolder '/' imgName];
        if strcmp(ch(1),'D')
            channel(x).X(end+1) = res.X(id);
            channel(x).Y(end+1) = res.Y(id);
            channel(x).active(end+1) = 1;
            channel(x).inspected(end+1) = 0;
            channel(x).perimeter(end+1) = 0;
            channel(x).size(end+1) = 0;
            channel(x).cellmask{end+1} = [];
            channel(x).trackset(end+1) = struct(channel(x).settings.trackset);
        elseif strcmp(ch(1),'Q')
            %%% TODO check if this always works with every channel
            %%% combination
            channel(x).detectchannel(end+1) = channel(x).settings.detectchannel;
            channel(x).active(end+1) = 0;
            channel(x).int(end+1) = -1;
            channel(x).inspected(end+1) = 0;
        else
            channel(x).stopReason(end+1) = res.stopReason(id);
            channel(x).X(end+1) = res.X(id);
            channel(x).Y(end+1) = res.Y(id);
        end
            
        
    end
end
fprintf('done.\n')
%     end

if strcmp(ch(1),'D')
    vtset.results{tree}.detects = channel;
elseif strcmp(ch(1),'Q')
    vtset.results{tree}.quants = channel;
else
    vtset.results{tree}.nonFluor = channel;
end

