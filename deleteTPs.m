function deleteTPs(tree,struct,q,delids)
global vtset
for field=fields(vtset.results{tree}.(struct))'
    if ~strcmp(field,'settings') && ~numel(vtset.results{tree}.(struct)(q).(cell2mat(field))) == 0
        vtset.results{tree}.(struct)(q).(cell2mat(field))(delids)=[];
    end
end