function varargout = ManualTracking(varargin)
% MANUALTRACKING M-file for ManualTracking.fig
%      MANUALTRACKING, by itself, creates a new MANUALTRACKING or raises the existing
%      singleton*.
%
%      H = MANUALTRACKING returns the handle to a new MANUALTRACKING or the handle to
%      the existing singleton*.
%
%      MANUALTRACKING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MANUALTRACKING.M with the given input arguments.
%
%      MANUALTRACKING('Property','Value',...) creates a new MANUALTRACKING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ManualTracking_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property
%      application
%      stop.  All inputs are passed to ManualTracking_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ManualTracking

% Last Modified by GUIDE v2.5 06-Apr-2010 16:18:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ManualTracking_OpeningFcn, ...
                   'gui_OutputFcn',  @ManualTracking_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ManualTracking is made visible.
function ManualTracking_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ManualTracking (see VARARGIN)

% Choose default command line output for ManualTracking
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% store stuff
global img imgQuant imgDetect trackset hfig MTfig confirmed vtset
confirmed=false;

trackset = varargin{3};

imgDetect = varargin{1};
imgQuant = varargin{2};

vtset.closeManualTracking= @closeMT;
MTfig=hObject;

if ~isfield(trackset,'detectionChannel')
    trackset.detectionChannel=0;
end

%%%% channels: 1 == quant %%%% 0 == detect
if trackset.detectionChannel
    img=imgQuant;
else
    img=imgDetect;
end

if ~isfield(trackset,'invert')
    trackset.invert=0;
end

hfig=figure('Tag','manualtracking2');
% resize figure
set(hfig,'Position', [130 400 470 500]);
% init gui
set(handles.txtWindow, 'String', num2str(trackset.win));
set(handles.scrWindow, 'Value', trackset.win/300);
set(handles.txtThreshCorr, 'String', num2str(trackset.threshcorr));
set(handles.scrThreshCorr, 'Value', trackset.threshcorr/2);
set(handles.txtSmooth, 'String', num2str(trackset.smooth));
set(handles.scrSmooth, 'Value', trackset.smooth/10);
set(handles.txtMax, 'String', num2str(trackset.max));
set(handles.scrMax, 'Value', trackset.max/10);
set(handles.lstClumped,'Value',find(strcmp( get(handles.lstClumped,'String'), trackset.clumped)));
set(handles.lstDividing,'Value',find(strcmp( get(handles.lstDividing,'String'), trackset.dividing)));
set(handles.chkNearest,'Value', trackset.usenearest);
set(handles.switchChannel,'Value', trackset.detectionChannel);

% set info string
set(handles.lblInfo,'String',varargin{4});

update('x',handles);

% position me
scnsize = get(0,'ScreenSize');
oldpos = get(handles.manualtracking,'Position');
set(handles.manualtracking,'Position', [1300 scnsize(4)-350 oldpos(3) oldpos(4)]);

% position figure
oldpos = get(hfig,'Position');
set(hfig,'Position', [1800 scnsize(4)-350 oldpos(3) oldpos(4)]);
% set caption
set(hfig,'Name', 'Detection');

% UIWAIT makes ManualTracking wait for user response (see UIRESUME)
uiwait(handles.manualtracking);


% --- Outputs from this function are returned to the command line.
function varargout = ManualTracking_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
global trackset confirmed Center All 

if confirmed
    varargout{1} = trackset;
    varargout{2} = Center;
    varargout{3} = All;
    
else
    varargout{1} = [];
    varargout{2} = [];
end


function closeMT
global confirmed MTfig
confirmed=0;
uiresume(MTfig);

close(MTfig);



% --- Executes on button press in btnUp.
function btnUp_Callback(hObject, eventdata, handles)
% hObject    handle to btnUp (see GCBO)
% eventdata  reserved900 - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
update('up',handles);

% --- Executes on button press in btnDown.
function btnDown_Callback(hObject, eventdata, handles)
% hObject    handle to btnDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
update('down',handles);

% --- Executes on button press in btnLeft.
function btnLeft_Callback(hObject, eventdata, handles)
% hObject    handle to btnLeft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
update('left',handles);

% --- Executes on button press in btnRight.
function btnRight_Callback(hObject, eventdata, handles)
% hObject    handle to btnRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
update('right',handles);


function update(action,handles,arg)

% get var
global img imgQuant imgDetect trackset hfig Center All 

%%% PERFORM PARAMETER UPDATE
if strcmp(action,'up')
    trackset.y = trackset.y-1;
elseif strcmp(action,'down')
    trackset.y = trackset.y+1;
elseif strcmp(action,'left')
    trackset.x = trackset.x-1;
elseif strcmp(action,'right')
    trackset.x = trackset.x+1;
elseif strcmp(action,'threshcorr')
    trackset.threshcorr = arg;
elseif strcmp(action,'smooth')
    trackset.smooth = arg;
elseif strcmp(action,'max')
    trackset.max = arg;
elseif strcmp(action,'clumped')
    trackset.clumped = arg;
elseif strcmp(action,'dividing')
    trackset.dividing = arg;
elseif strcmp(action,'win')
    trackset.win = arg;
elseif strcmp(action, 'nearest')
    trackset.usenearest = arg;  
elseif strcmp(action, 'invert')

    if trackset.invert==1
        trackset.invert=0;
        img=imcomplement(img);
    else
        trackset.invert=1;
        img=imcomplement(img);
    end

    % elseif strcmp(action, 'posleft')
    %     trackset.x = size(img,2)-ceil(trackset.win/2);
    %     img=double(imread('/media/disk/data/090901PH2/090901PH2_p033/090901PH2_p033_t00691_w1.tif'))/255;
    %     img = img ./ vtset.results.settings.corrImage;
    %     % substract mean of background
    %     img=img-median(img(:));
    %     img(img<0)=0;
elseif strcmp(action,'switchChannel')
    
    %%%% 0 == detect, 1 == quant
    if trackset.detectionChannel
        img=imgQuant;
    else
        img=imgDetect;
    end

end


%%%%%%%%%%%% SEGMENATION %%%%%%%%%%%%%%

% shortcuts
% y = trackset.y;
% x = trackset.x;
% window = trackset.win;
% % extract subimage
% [subimg centerX centerY] = extractSubImage(img, x, y, window);
% subimg = imadjust(subimg);
% 
% 
% % set Threshold
% % check for determine mode
% method=lower(strsplit(' ', trackset.threshmethod));
% if(strcmp(method(2),'global'))
%     ThreshImg=imadjust(img);
% elseif(strcmp(method(2), 'sub'))
%     ThreshImg=subimg;
% else
%     error('Unknown Threshold Image Mode');
% end
% 
% Threshold = determineThreshold(ThreshImg, trackset)*trackset.threshcorr;
% 
% % segmentation
% [Center,All] = myseg(subimg, Threshold, trackset,centerX,centerY);

[Center, All, subimg] = cellSegmentation(img, trackset.x, trackset.y, trackset);

% calc eccentricity
% t=regionprops(double(Center), 'Eccentricity');

% update static text
oldStr=get(handles.lblInfo,'String');
infoStr = [oldStr(1:31) sprintf('x: %.0f, y: %.0f, value: %.3f, size: %d',  ...
     trackset.x, trackset.y, sum(subimg(Center))/255,sum(Center(:)))];

set(handles.lblInfo,'String',infoStr);

%%% SHOW IN FIGURE
figure(hfig);
subplot(2,2,1);
imagesc(subimg);
colormap(gray);
title('Original');
% show all detected nuclei
subplot(2,2,2);
imagesc(All);
title('All detected');
% show center
subplot(2,2,3);
imagesc(Center);
title('Detected');
% show outline
subplot(2,2,4);
BWoutline = bwperim(Center);
Segout = subimg;
Segout(BWoutline) = max(subimg(:))*1.2;
toBase(Segout)
imagesc(Segout);
title('Outline');


function txtThreshCorr_Callback(hObject, eventdata, handles)
% hObject    handle to txtThreshCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtThreshCorr as text
%        str2double(get(hObject,'String')) returns contents of txtThreshCorr as a double


% --- Executes during object creation, after setting all properties.
function txtThreshCorr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtThreshCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrThreshCorr_Callback(hObject, eventdata, handles)
% hObject    handle to scrThreshCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
update('threshcorr',handles, get(hObject,'Value')*2);
set(handles.txtThreshCorr, 'String', num2str(get(hObject,'Value')*2));

% --- Executes during object creation, after setting all properties.
function scrThreshCorr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrThreshCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function txtSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to txtSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtSmooth as text
%        str2double(get(hObject,'String')) returns contents of txtSmooth as a double


% --- Executes during object creation, after setting all properties.
function txtSmooth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrSmooth_Callback(hObject, eventdata, handles)
% hObject    handle to scrSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
update('smooth',handles, get(hObject,'Value')*10);
set(handles.txtSmooth, 'String', num2str(get(hObject,'Value')*10));

% --- Executes during object creation, after setting all properties.
function scrSmooth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrSmooth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function txtMax_Callback(hObject, eventdata, handles)
% hObject    handle to txtMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtMax as text
%        str2double(get(hObject,'String')) returns contents of txtMax as a double


% --- Executes during object creation, after setting all properties.
function txtMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrMax_Callback(hObject, eventdata, handles)
% hObject    handle to scrMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
update('max',handles, get(hObject,'Value')*10);
set(handles.txtMax, 'String', num2str(get(hObject,'Value')*10));

% --- Executes during object creation, after setting all properties.
function scrMax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrMax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in lstClumped.
function lstClumped_Callback(hObject, eventdata, handles)
% hObject    handle to lstClumped (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns lstClumped contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lstClumped

lst = get(hObject,'String');
update('clumped',handles, lst{get(hObject,'Value')});

% --- Executes during object creation, after setting all properties.
function lstClumped_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lstClumped (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in manualtracking.
function lstDividing_Callback(hObject, eventdata, handles)
% hObject    handle to manualtracking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns manualtracking contents as cell array
%        contents{get(hObject,'Value')} returns selected item from manualtracking
lst = get(hObject,'String');
update('dividing',handles, lst{get(hObject,'Value')});

% --- Executes during object creation, after setting all properties.
function manualtracking_CreateFcn(hObject, eventdata, handles)
% hObject    handle to manualtracking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function lstDividing_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lstDividing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnOK.
function btnOK_Callback(hObject, eventdata, handles)
% hObject    handle to btnOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% callback
global confirmed

confirmed=true;
close(handles.manualtracking);



% --- Executes on button press in btnCancel.
function btnCancel_Callback(hObject, eventdata, handles)
% hObject    handle to btnCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close(handles.manualtracking);


% --- Executes when user attempts to close manualtracking.
function manualtracking_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to manualtracking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
global hfig
if ishandle(hfig)
    close(hfig);
end



function txtWindow_Callback(hObject, eventdata, handles)
% hObject    handle to txtWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtWindow as text
%        str2double(get(hObject,'String')) returns contents of txtWindow as a double


% --- Executes during object creation, after setting all properties.
function txtWindow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrWindow_Callback(hObject, eventdata, handles)
% hObject    handle to scrWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

update('win',handles, get(hObject,'Value')*300);
set(handles.txtWindow, 'String', num2str(get(hObject,'Value')*300));

% --- Executes during object creation, after setting all properties.
function scrWindow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in chkNearest.
function chkNearest_Callback(hObject, eventdata, handles)
% hObject    handle to chkNearest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkNearest
update('nearest',handles, get(hObject,'Value'));



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%% what is this?
% --- Executes on button press in posleft.
function posleft_Callback(hObject, eventdata, handles)
% hObject    handle to posleft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global confirmed 

confirmed=false;
close(handles.manualtracking);


% --- Executes on button press in inverimg.
function inverimg_Callback(hObject, eventdata, handles)
% hObject    handle to inverimg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of inverimg
update('invert',handles);


% --- Executes on button press in switchChannel.
function switchChannel_Callback(hObject, eventdata, handles)
% hObject    handle to switchChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of switchChannel

global trackset

if trackset.detectionChannel
    trackset.detectionChannel=0;
else
    trackset.detectionChannel=1;
end

update('switchChannel',handles);

%%% NEXT
% --- Executes on button press in next.
function next_Callback(hObject, eventdata, handles)
% hObject    handle to next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global vtset
% vtset.next();


%%% LAST 
% --- Executes on button press in back.
function back_Callback(hObject, eventdata, handles)
% hObject    handle to back (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global vtset
% vtset.back();

