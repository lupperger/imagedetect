%% generates a file name for a given set of parameter
function name = generateFilename(exp, pos, tp, wl, add, ext, filesettings)
    
    tab = filesettings.matrix;
    sep = filesettings.separator;
    preName = {'�','�','�','�'};
    in = {pos,tp,wl,add};
    for i=1:4 
        if(tab{i,1} ~= -1 && in{i} >= 0)
            l = numel(num2str(in{i}));
            preName{tab{i,1}} = [tab{i,3} [repmat('0',1,(tab{i,2}-l)) num2str(in{i})]];
        end
    end
    name = [exp sep strjoin(preName, sep) '.' ext];    
    name = strrep(name,[sep '�'], '');

end

