% [tracksDetect,tracksQuant,tracksNonFluor] = 
%    processTree(tree,settings,trackset,waitbarhandle)
function [tracksDetect,tracksQuant,tracksNonFluor] = ...
    processTree(tree,settings,trackset)
% shortcuts to settings

imgroot = settings.imgroot;
exp = settings.exp;
extDetect = settings.extDetect;
extQuant = settings.extQuant;
extNonFluor = settings.extNonFluor; 
corrDetect = settings.corrDetect;
corrQuant = settings.corrQuant;
corrImage = settings.corrImage;
% blocknorm = settings.blocknorm;
% blocksize = settings.blocksize;
debugOut = settings.debugOut;
% load track stuff
tracksDetect = loadTracks(tree,imgroot,exp,extDetect);
tracksQuant = loadTracks(tree,imgroot,exp,extQuant);
tracksNonFluor = loadTracks(tree,imgroot,exp,extNonFluor);

% get those cells that have an assigned image in both detection and
% quantification channel
usecells = ~strcmp(tracksDetect.filename,'') & ~strcmp(tracksQuant.filename,'');

% restrict
tracksDetect = restrictToCells(tracksDetect,usecells);
tracksQuant = restrictToCells(tracksQuant,usecells);

% init
tracksQuant.int = zeros(numel(tracksQuant.X),1);
tracksQuant.mean = zeros(numel(tracksQuant.X),1);
tracksQuant.size = zeros(numel(tracksQuant.X),1);
tracksQuant.cellmask = cell(numel(tracksQuant.X),1);

%%%% needed anymore????
% restrict again to cell which have to be calculated, set in the GUI

% if strcmp('all',settings.restrictcells)
    imgfiles = unique(tracksDetect.filename);
%     settings.restrictcells=unique(tracksDetect.cellNr);
% else
%     imgfiles = unique(tracksDetect.filename(ismember(tracksDetect.cellNr,settings.restrictcells)));
% end
tracksDetectfilename=tracksDetect.filename;
settingsimgroot=settings.imgroot;
imgDetect=cell(numel(imgfiles),1);
imgQuant=cell(numel(imgfiles),1);
tracksQuantres=cell(numel(imgfiles),1);

% iterate over all images to be processed
if numel(imgfiles)==0
    warndlg(sprintf('No images found'));
end

for i=1:numel(imgfiles)
    % who progress bar

    waitbar(i/numel(imgfiles));
    % one could be empty (why again?)
    if numel(imgfiles{i})>1
        % collect tracks for this image

        cells = find(strcmp(tracksDetectfilename, imgfiles{i}));% & ismember(tracksDetect.cellNr,settings.restrictcells));

        % load both images
        imgDetect{i} = double(imageCache([settingsimgroot imgfiles{i}]))/255;
        imgQuant{i} = double(imageCache([settingsimgroot imgfiles{i}(1:end-6) extQuant]))/255;

        % do the illumination correction
        if corrDetect
            img2file=imgfiles{i}(1:end);
            
            if settings.corrWithPreCalculatet && exist([settings.imgroot img2file(1:16) 'background/' img2file(17:end-3) 'png'],'file')
                corrImage = double(imread([settings.imgroot img2file(1:16) 'background/' img2file(17:end-3) 'png']))/255/255;

            else
                fprintf('Pre calculated background not found\n');
            end
            
            %%% check for gain/offset
            if exist([settings.imgroot img2file(1:16) 'background/gain.png'],'file')
                gain = double(imread([settings.imgroot img2file(1:16) 'background/gain.png']))/255/255;
                offset = double(imread([settings.imgroot img2file(1:16) 'background/offset.png']))/255/255;
                imgDetect{i} = normalizeImage(imgDetect{i},corrImage,gain,offset);
            else
                fprintf('gain / offset not found\n');
                imgDetect{i} = normalizeImage(imgDetect{i},corrImage);
            end
                        
            
        end
        if corrQuant
            img2file=[imgfiles{i}(1:end-6) extQuant];
            
            if settings.corrWithPreCalculatet && exist([settings.imgroot img2file(1:16) 'background/' img2file(17:end-3) 'png'],'file')
                corrImage = double(imread([settings.imgroot img2file(1:16) 'background/' img2file(17:end-3) 'png']))/255/255;
                
            else
                fprintf('Pre calculated background not found\n');
            end
            
            %%% check for gain/offset
            if exist([settings.imgroot img2file(1:16) 'background/gain.png'],'file')
                gain = double(imread([settings.imgroot img2file(1:16) 'background/gain.png']))/255/255;
                offset = double(imread([settings.imgroot img2file(1:16) 'background/offset.png']))/255/255;
                imgQuant{i} = normalizeImage(imgQuant{i},corrImage,gain,offset);
            else
                fprintf('gain / offset not found\n');
                imgQuant{i} = normalizeImage(imgQuant{i},corrImage);
            end
            imgQuant{i} = normalizeImage(imgQuant{i}, corrImage);
            
        end
        
        % do segmentation/quantifications of cells
        tracksQuantres{i}=quantify(cells, tracksDetect, trackset, imgQuant{i}, imgDetect{i}, tracksQuant);
        
        %%% clear memory
        imgQuant{i}=[];
        imgDetect{i}=[];
        
        fprintf('Done with image %s\n',imgfiles{i});
    end
end
% toBase(imgfiles)
% toBase(tracksQuant);
% toBase(tracksQuantres)
fprintf('Copying structure...');

for ii=1:numel(imgfiles)
    if isfield(tracksQuantres{ii}, 'error')
            errordlg(sprintf('Quantification Error: Timepoint %d cell %d\nX: %d Y: %d', tracksQuantres{ii}.error.timepoint,...
                 tracksQuantres{ii}.error.cell,  tracksQuantres{ii}.error.x, tracksQuantres{ii}.error.y));
    end
    cells = find(strcmp(tracksDetect.filename, imgfiles{ii}));
    for j=1:numel(cells) 
        tracksQuant.int(cells(j)) = tracksQuantres{ii}.int(cells(j));
        tracksQuant.size(cells(j)) =  tracksQuantres{ii}.size(cells(j));
        tracksQuant.mean(cells(j)) =  tracksQuantres{ii}.mean(cells(j));
        tracksQuant.cellmask(cells(j)) =  tracksQuantres{ii}.cellmask(cells(j));
        tracksQuant.trackset(cells(j)) = tracksQuantres{ii}.trackset(cells(j));
    end
end
fprintf('done.\n');


