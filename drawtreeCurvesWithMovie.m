%field1 = color
%field2 = width

function drawtreeCurves(data, field1, field2, htree, cells)

global vtset
% find min an max field1
maxuser1 = max(data.(field1));
minuser1 = min(data.(field1));


% find min an max field2
maxuser2 = max(data.(field2));
minuser2 = min(data.(field2));

% divisor to normalize width
divisor=20;




% iterate over all cells
% cells = unique(data.cellNr)';

htree=figure;
hold on;
for cell=unique(data.cellNr)'
    % get index
    index = find(data.cellNr==cell);
    

    % get path to root
    rp = rootpath(cell);
    % determine x coordinate
    x = 0;
    for i=2:numel(rp)
        horlen = 1/2^(i-1);
        if mod(rp(i),2)==0
            % even number, left branch
            x = x - horlen;
        else
            % odd number, right branch
            x = x + horlen;
        end
    end
    
    % draw the line
    time = data.(vtset.timemode)(index)/vtset.timemodifier;
   
    % draw line with widht
    alltp= data.(vtset.timemode)(index)/vtset.timemodifier;
    data1=data.(field1)(index);
    data2=data.(field2)(index);
    
    for tp= 2:numel(alltp)
        % calc color from field1
        cl=ceil(eps+(data1(tp)-minuser1)/(maxuser1-minuser1)*64);
        mymap=jet;

        %calc width from field2
        offset=eps+(data2(tp)-minuser2)/(maxuser2-minuser2);
        
%         offset=max(eps,allint(tp)/divisor);
        rectangle('Position', [(x-offset/divisor), alltp(tp)-1, offset/divisor*2, max(eps,(alltp(tp)-alltp(tp-1)))], 'FaceColor',mymap(cl,:),'LineStyle','none');
        
    end
    
    % draw line if annotation starts
    allanno=data.wavelength_3(index);
    if ismember(1,allanno) && ismember(0,allanno)
        ap=find(allanno==1);
        ap=ap(1);
        aptp=alltp(ap);
%         plot_arrow(x-0.05,aptp, x+0.05,aptp,'headheight',0.2,'headwidth',0.3);

%         fig_pos=get(gca,'position'); 
%         yp1=(fig_pos(2)+fig_pos(4))*0.98;
%         yp2=0.98;
%         xp1=fig_pos(1)+fig_pos(3)*0.5;
%         annotation('arrow',[xp1 xp1],[yp1 yp2])
        line([x-0.05 x+0.05], [aptp aptp], 'Color', 'black', 'LineWidth', 4);
    end
    % draw line if annotation2 starts
    allanno=data.wavelength_2(index);
    if ismember(1,allanno) && ismember(0,allanno)
        ap=find(allanno==1);
        ap=ap(1);
        aptp=alltp(ap);
%         plot_arrow(x-0.05,aptp, x+0.05,aptp,'headheight',0.2,'headwidth',0.3);

%         fig_pos=get(gca,'position'); 
%         yp1=(fig_pos(2)+fig_pos(4))*0.98;
%         yp2=0.98;
%         xp1=fig_pos(1)+fig_pos(3)*0.5;
%         annotation('arrow',[xp1 xp1],[yp1 yp2])
        line([x-0.05 x+0.05], [aptp aptp], 'Color', 'red', 'LineWidth', 4);
    end
    
    % and the connection of the children, if there
    c1 = cell*2;
    c2 = cell*2+1;
    if (numel(find(data.cellNr==c1))>0&&numel(find(data.cellNr==c2))>0)
        x1 = x+(1/2^floor(log2(cell)+1));
        x2 = x-(1/2^floor(log2(cell)+1));
        line([x1 x2], [max(time) max(time)]);
%         text(x2-0.05,max(time), sprintf('%d',cell*2));        
%         text(x1,max(time), sprintf('%d',cell*2+1));
    end

    infox = x;
    infoy = min(time);%;mean([min(time) max(time)]) - (max(time)-min(time))*0.2;
    % draw number
    text(infox,infoy-2, sprintf('%d',cell));
    % draw information
%     imagesc(data.(userfield)(index)', 'XData', [infox infox+0.07], 'YData', [infoy infoy+((max(time)-min(time))/1.8/60)]);
%     set(gca, 'clim', [minuser1 maxuser1]);
%        col = [ 1 0 0 ] * (data.(userfield)(index)-minuser)/(maxuser-minuser)  ;
    %    rectangle('Position', [infox, infoy, 0.07, (max(time)-min(time))/1.8/60], 'FaceColor',col);
   
    %


end
hold off;
set(gca,'YDir','reverse');
ylabel('Hours');
set(gca,'XTick',[])
colorbar;
colormap(gray(1000))
set(gca, 'clim', [0 250]);
%% iterate over movie and display cells of tp
hold on
for tp=unique(data.timepoint)'
    index = find(data.timepoint == tp)';
    
    for cellindex = index
        
        celltime  = data.(vtset.timemode)(data.cellNr == data.cellNr(cellindex))/vtset.timemodifier;
        
        yfrom = min(celltime);
        yto = max(celltime);
        % determine x coordinate
        rp = rootpath(data.cellNr(cellindex));
        x = 0;
        for i=2:numel(rp)
            horlen = 1/2^(i-1);
            if mod(rp(i),2)==0
                % even number, left branch
                x = x - horlen;
            else
                % odd number, right branch
                x = x + horlen;
            end
        end

        x1 = x+(1/2^floor(log2(data.cellNr(cellindex))+1));
        x2 = x-(1/2^floor(log2(data.cellNr(cellindex))+1));

        xrange=x1:-0.001 : x1-0.3;
        if x>=0
            xrange = xrange+abs(xrange(1)-xrange(end));
        else
            xrange = xrange-abs(xrange(1)-xrange(end));
        end

        
        %load image
        img = double(imread([vtset.results.settings.imgroot data.filename{cellindex}]))/255;
        
        img = normalizeImage(img,vtset.results.settings.corrImage);
        
        trackset = data.trackset{cellindex};
        
        subimg = extractSubImage(img, trackset.x, trackset.y, trackset.win);
        
        imagesc(subimg*60*6, 'XData',xrange, 'YData',(yfrom:0.01:yto)-5);
        pause(0.2)
        
    end
end



