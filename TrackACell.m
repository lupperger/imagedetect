function varargout = TrackACell(varargin)
% TRACKACELL M-file for TrackACell.fig
%      TRACKACELL, by itself, creates a new TRACKACELL or raises the existing
%      singleton*.
%
%      H = TRACKACELL returns the handle to a new TRACKACELL or the handle to
%      the existing singleton*.
%
%      TRACKACELL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRACKACELL.M with the given input arguments.
%
%      TRACKACELL('Property','Value',...) creates a new TRACKACELL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TrackACell_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TrackACell_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TrackACell

% Last Modified by GUIDE v2.5 11-Feb-2013 13:21:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TrackACell_OpeningFcn, ...
                   'gui_OutputFcn',  @TrackACell_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TrackACell is made visible.
function TrackACell_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TrackACell (see VARARGIN)
global vtset
% Choose default command line output for TrackACell
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


%%%% THE IDEAD IS:
% correct all detect channels, corresponding to the quantification channel,
% the user selected. timepoints etc should be collected by using quants
% channel and mapping them on detects

% gather detection channels with methods
channels=cell(1,numel(vtset.results{vtset.treenum}.detects));
for i = 1:numel(vtset.results{vtset.treenum}.detects)
    channels{i} = [vtset.results{vtset.treenum}.detects(i).settings.wl ' ' vtset.results{vtset.treenum}.detects(i).settings.trackset.threshmethod];
end

set(handles.dropDownSegmentationChannel,'String',channels);
set(handles.dropDownSegmentationChannel,'Value',vtset.results{vtset.treenum}.quants(vtset.selected.channel).detectchannel(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr == vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint == vtset.selected.timepoint));
vtset.temp.detectchannel = get(handles.dropDownSegmentationChannel,'Value'); % stores which detect chan is relevant. can be changed by drop down menu

%%%%% set all control features:

% set(handles.popupMode,'Value', find(strcmp(vtset.results{vtset.treenum}.    trackset.mode,v)));

% make images clickable
set(hObject,'WindowButtonDownFcn',@rearrangeCenter);
% catch keyboard function (has to be catched in every slider etc ...)
set(hObject,'KeyPressFcn',@myKeyFunction);
set(handles.scrTimepointx,'KeyPressFcn',@myKeyFunction);
set(handles.scrMax_otsu,'KeyPressFcn',@myKeyFunction);
set(handles.scrSmooth_otsu,'KeyPressFcn',@myKeyFunction);
set(handles.scrWindow,'KeyPressFcn',@myKeyFunction);
set(handles.scrThreshCorr_otsu,'KeyPressFcn',@myKeyFunction);
set(handles.lstClumped_otsu,'KeyPressFcn',@myKeyFunction);
set(handles.lstDividing_otsu,'KeyPressFcn',@myKeyFunction);
set(handles.dropDownSegmentationChannel,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonNext,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonBack,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonSaveNext,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonSaveBack,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonSave,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonCancel,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonInspected,'KeyPressFcn',@myKeyFunction);
set(handles.ButtonRecalcCell,'KeyPressFcn',@myKeyFunction);

% init show quant axes
uniquants=unique(vtset.allpossiblequants);
set(handles.dropDownQuantification1,'String',uniquants);
set(handles.dropDownQuantification2,'String',uniquants);
set(handles.dropDownQuantification3,'String',uniquants);
vtset.cellinspector.crosshairmodus = 0;
for q = 1:3
    
    if q>numel(uniquants)
        set(handles.(sprintf('dropDownQuantification%d',q)),'value',1,'visible','off');
        set(handles.(sprintf('textQuant%d',q)),'visible','off');
    else
        if isempty(vtset.showaxesQuants.wl{q})
            vtset.showaxesQuants.wl{q} = uniquants{q};
            set(handles.(sprintf('dropDownQuantification%d',q)),'value',q);
        end
        set(handles.(sprintf('dropDownQuantification%d',q)),'value',q,'visible','on');
        set(handles.(sprintf('textQuant%d',q)),'visible','on');
    end
end



set(hObject,'Name', sprintf('Cell Inspector: %s',vtset.results{vtset.treenum}.settings.treefile));

% set slider min max
% updateGUIslider(handles)


% set the image visibilty
detectchannel=vtset.results{vtset.treenum}.quants(vtset.selected.channel).detectchannel(vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint == vtset.selected.timepoint & vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr == vtset.selected.cell);
z = find(arrayfun(@(a,b)isequal(a.settings.detectchannel, detectchannel), vtset.results{vtset.treenum}.quants));
for qchan = 1:2%numel(z)
    set(handles.(sprintf('axesQuant%d',qchan)),'visible','on')
    cla(handles.(sprintf('axesQuant%d',qchan)))
end

for qchan = numel(z)+2:3
    cla(handles.(sprintf('axesQuant%d',qchan)))
    set(handles.(sprintf('axesQuant%d',qchan)),'visible','off')
end


%%% obsolate, included w0 
%make the GUI smaller
% oldpos = get(handles.TrackACell,'Position');
% if numel(z)>2
%     set(handles.TrackACell,'Position',[oldpos(1:2) 206 oldpos(4)])
% else
%     set(handles.TrackACell,'Position',[oldpos(1:2) 163 oldpos(4)])
% end

% set focus field
vtset.trackACellFocus = gcf;

vtset.manualswitch = 0;

vtset.redrawCellInspector = @redraw;
vtset.functions.deleteTimepoint = @deleteTimepoint;
vtset.functions.changeTimepointFlag = @changeTimepointFlag;
% initialize:

initializeParams(handles);
set(hObject,'WindowButtonMotionFcn',@wbmf)

% updateGUIslider(handles)

% UIWAIT makes TrackACell wait for user response (see UIRESUME)
% uiwait(handles.TrackACell);

function initializeTimeSlider(handles)
global vtset
set(handles.axesTimepointSlider,'units','normalized')
oldpos = get(handles.axesTimepointSlider,'position');

set(handles.axesTimepointSlider,'visible','off')
maxtp = sum( vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr == vtset.selected.cell);
alltp =vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr  == vtset.selected.cell);
insp =vtset.results{vtset.treenum}.quants(vtset.selected.channel).inspected(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr  == vtset.selected.cell);
active =vtset.results{vtset.treenum}.quants(vtset.selected.channel).active(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr  == vtset.selected.cell);
[alltp,order] = sort(alltp);
insp = insp(order);
active= active(order);

try 
    delete(vtset.haxesTimeSlider.axes);
catch e
    %already deleted?
end


vtset.haxesTimeSlider.axes=zeros(maxtp,1);
vtset.haxesTimeSlider.timepoint=alltp';
vtset.haxesTimeSlider.status=zeros(maxtp,1); % -1 inactive, 0 active but uninspected, 1 inspected.
vtset.haxesTimeSlider.selected=vtset.selected.timepoint;
for ax = 1:maxtp
    delimiter = oldpos(3) / maxtp;
    vtset.haxesTimeSlider.axes(ax)=axes('position',[oldpos(1)+delimiter*(ax-1) oldpos(2) delimiter oldpos(4)],'parent',vtset.trackACellFocus);
    if ~active(ax) 
        vtset.haxesTimeSlider.status(ax) = -1;
    else
        vtset.haxesTimeSlider.status(ax) = insp(ax);
    end
    if vtset.haxesTimeSlider.status(ax) == -1
        doth = plot(1,1,'xr','markersize',5,'parent',vtset.haxesTimeSlider.axes(ax));
    elseif vtset.haxesTimeSlider.status(ax)
        doth = plot(1,1,'.k','markersize',20,'parent',vtset.haxesTimeSlider.axes(ax));
    else
        doth = plot(1,1,'ok','markersize',5,'parent',vtset.haxesTimeSlider.axes(ax));
    end
    
    % set mouse callback
    set(vtset.haxesTimeSlider.axes(ax),'ButtonDownFcn',{@myTimeSliderCallback,alltp(ax)})
    set(doth,'ButtonDownFcn',{@myTimeSliderCallback,alltp(ax)})
    
    if alltp(ax) == vtset.haxesTimeSlider.selected
        set(vtset.haxesTimeSlider.axes(ax),'color',[.7 .7 .7])
    else
        set(vtset.haxesTimeSlider.axes(ax),'color',[.941 .941 .941])
    end
    
    % set context menu
    hcmenu = uicontextmenu('parent',vtset.trackACellFocus);
    cb = ['global vtset;    vtset.functions.changeTimepointFlag(''inspected'',' num2str(~(vtset.haxesTimeSlider.status(ax)>0 )) ')'];
    status = {'off','on'};
    item1 = uimenu(hcmenu, 'Label', 'Inspected', 'Checked',status{(vtset.haxesTimeSlider.status(ax)>0 ) +1}, 'Callback', cb);
    if (vtset.haxesTimeSlider.status(ax)~=-1 )
    
        cb ='global vtset;    vtset.functions.deleteTimepoint(1);';
    else
        cb = ['global vtset;    vtset.functions.changeTimepointFlag(''active'',1)'];
    end
    item2 = uimenu(hcmenu, 'Label', 'Active','Checked',status{(vtset.haxesTimeSlider.status(ax)~=-1 ) +1}, 'Callback', cb);
    set(vtset.haxesTimeSlider.axes(ax),'uicontextmenu',hcmenu)
    
    
    
    % axis(h,'off')
    set(vtset.haxesTimeSlider.axes(ax),'yticklabel',[])
    set(vtset.haxesTimeSlider.axes(ax),'xticklabel',[])
    set(vtset.haxesTimeSlider.axes(ax),'xtick',[]);
    set(vtset.haxesTimeSlider.axes(ax),'ytick',[]);
end
% updateTimeSliderCallback(vtset.haxesTimeSlider.selected,vtset.haxesTimeSlider.status(vtset.haxesTimeSlider.timepoint == vtset.haxesTimeSlider.selected))

function myTimeSliderCallback(src,evnt,newtimepoint)
    global vtset
    vtset.selected.timepoint=newtimepoint;
    vtset.dcselected.timepoint=newtimepoint;
    handles = guidata(src);

initializeParams(handles);
        
    
function updateTimeSliderCallback(timepoint,status)
global vtset
ax = vtset.haxesTimeSlider.timepoint == timepoint;
alltp = vtset.haxesTimeSlider.timepoint;
cla(vtset.haxesTimeSlider.axes(ax))
vtset.haxesTimeSlider.status(ax) = status;

% draw the new dot
if vtset.haxesTimeSlider.status(ax) == -1
    doth = plot(1,1,'xr','markersize',5,'parent',vtset.haxesTimeSlider.axes(ax));
elseif vtset.haxesTimeSlider.status(ax)
    doth = plot(1,1,'.k','markersize',20,'parent',vtset.haxesTimeSlider.axes(ax));
else
    doth = plot(1,1,'ok','markersize',5,'parent',vtset.haxesTimeSlider.axes(ax));
end
set(doth,'ButtonDownFcn',{@myTimeSliderCallback,alltp(ax)})

% set background
set(vtset.haxesTimeSlider.axes,'color',[.941 .941 .941])
if alltp(ax) == vtset.haxesTimeSlider.selected
    set(vtset.haxesTimeSlider.axes(ax),'color',[.7 .7 .7])
end


% clean up axes
set(vtset.haxesTimeSlider.axes(ax),'yticklabel',[])
set(vtset.haxesTimeSlider.axes(ax),'xticklabel',[])
set(vtset.haxesTimeSlider.axes(ax),'xtick',[]);
set(vtset.haxesTimeSlider.axes(ax),'ytick',[]);

function myKeyFunction(src, evnt)
global vtset
handles = guidata(src);
k= evnt.Key; %k is the key that is pressed


% timepoint & cell for reset function
cell = vtset.selected.cell;
timepoint = vtset.selected.timepoint;
channel = vtset.selected.channel; % refers to the selected quantification channel
detectchannel = vtset.results{vtset.treenum}.quants(channel).detectchannel(vtset.results{vtset.treenum}.quants(channel).cellNr == cell & vtset.results{vtset.treenum}.quants(channel).timepoint == timepoint);
curtreeDetect = vtset.results{vtset.treenum}.detects(detectchannel);
cellid = curtreeDetect.cellNr==cell & curtreeDetect.timepoint==timepoint;
origTrackset=curtreeDetect.trackset(cellid);
% for version<0.9.9
if iscell(origTrackset)
    origTrackset=cell2mat(origTrackset);
end

if strcmp(k,'return') %if enter was pressed
    pause(0.01) %allows time to update
    ButtonSaveNext_Callback([],[],handles);
elseif strcmp(k,'c')
   val = get(handles.dropDownSegmentationChannel,'String');
   if get(handles.dropDownSegmentationChannel,'Value') == numel(val)   
     vtset.temp.detectchannel = 1;
   else
       vtset.temp.detectchannel = get(handles.dropDownSegmentationChannel,'Value') + 1;
   end
   % uicontrol(handles.ButtonNext);
    vtset.manualswitch = 1;
    initializeParams(handles);
    update(handles);
elseif strcmp(k,'f10')
    vtset.saveTrees(vtset.treenum,1);
elseif strcmp(k,'backspace')    
    % one back
    ButtonBack_Callback([],[],handles);
    % maybe some undo function???
   
elseif strcmp(k,'separator')
    ButtonSave_Callback([],[],handles);    
elseif strcmp(k,'home')    
    % go to first tp
    vtset.selected.timepoint=getNearestTimepoint(vtset.selected.timepoint,-10000,handles);
    initializeParams(handles);
elseif strcmp(k,'end')        
    % go to last tp
    vtset.selected.timepoint=getNearestTimepoint(vtset.selected.timepoint,+10000,handles);
    initializeParams(handles);
elseif strcmp(k,'delete')    
    % delete this timepoint
    deleteTimepoint(0,handles);
    
    
elseif strcmp(k,'uparrow')
    % one cell up
    nextCell=getNearestCell(vtset.selected.cell,1);
    if vtset.selected.cell~=nextCell
        vtset.selected.cell=nextCell;
        vtset.selected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1));
        initializeParams(handles);
    end
    
elseif strcmp(k,'downarrow')
    % one cell down
    nextCell=getNearestCell(vtset.selected.cell,-1);
    if vtset.selected.cell~=nextCell
        vtset.selected.cell=nextCell;
        vtset.selected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1));
        initializeParams(handles);
    end    
    
elseif strcmp(k,'pageup')
    % one generation up
    cells=unique(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr);
    if ismember(vtset.selected.cell*2,cells)
        vtset.selected.cell=vtset.selected.cell*2;
        vtset.selected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==vtset.selected.cell& vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1));
        initializeParams(handles);
    elseif ismember(vtset.selected.cell*2+1,cells)
        vtset.selected.cell=vtset.selected.cell*2+1;
        vtset.selected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==vtset.selected.cell& vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1));
        initializeParams(handles);
    end        
    
elseif strcmp(k,'pagedown')
    % one generation down
    cells=unique(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr);
    if ismember(floor(vtset.selected.cell/2),cells)
        vtset.selected.cell=floor(vtset.selected.cell/2);
        vtset.selected.timepoint = min(vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==vtset.selected.cell& vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1));
        initializeParams(handles);
    end      
    
    
elseif strcmp(k,'leftarrow')
    % one timepoint back
    ButtonBack_Callback([],[],handles);
elseif strcmp(k,'rightarrow')
    % one timepoint further
    ButtonNext_Callback([],[],handles);
elseif strcmp(k,'numpad7')
    % threshcorr 0.1 down
    set(handles.scrThreshCorr_otsu,'Value',max(get(handles.scrThreshCorr_otsu,'Value')-0.1,0));
    set(handles.txtThreshCorr_otsu,'String',num2str(get(handles.scrThreshCorr_otsu,'Value')));
    vtset.temp.trackset.threshcorr=get(handles.scrThreshCorr_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad9')
    % threshcorr 0.1 up
    set(handles.scrThreshCorr_otsu,'Value',min(get(handles.scrThreshCorr_otsu,'Value')+0.1,2));
    set(handles.txtThreshCorr_otsu,'String',num2str(get(handles.scrThreshCorr_otsu,'Value')));
    vtset.temp.trackset.threshcorr=get(handles.scrThreshCorr_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad8')
    % reset threshcorr
    set(handles.scrThreshCorr_otsu,'Value',origTrackset.threshcorr);
    set(handles.txtThreshCorr_otsu,'String',num2str(get(handles.scrThreshCorr_otsu,'Value')));
    vtset.temp.trackset.threshcorr=get(handles.scrThreshCorr_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad4')
    % smooth 1 down
    set(handles.scrSmooth_otsu,'Value',max(get(handles.scrSmooth_otsu,'Value')-1,0));
    set(handles.txtSmooth_otsu,'String',num2str(get(handles.scrSmooth_otsu,'Value')));
    vtset.temp.trackset.smooth=get(handles.scrSmooth_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad6')    
    % smooth 1 up
    set(handles.scrSmooth_otsu,'Value',min(get(handles.scrSmooth_otsu,'Value')+1,10));
    set(handles.txtSmooth_otsu,'String',num2str(get(handles.scrSmooth_otsu,'Value')));
    vtset.temp.trackset.smooth=get(handles.scrSmooth_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad5')
    % reset smooth
    set(handles.scrSmooth_otsu,'Value',origTrackset.smooth);
    set(handles.txtSmooth_otsu,'String',num2str(get(handles.scrSmooth_otsu,'Value')));
    vtset.temp.trackset.smooth=get(handles.scrSmooth_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad1')    
    % max 1 down
    set(handles.scrMax_otsu,'Value',max(get(handles.scrMax_otsu,'Value')-1,0));
    set(handles.txtMax_otsu,'String',num2str(get(handles.scrMax_otsu,'Value')));
    vtset.temp.trackset.max=get(handles.scrMax_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad3')   
    % max 1 up
    set(handles.scrMax_otsu,'Value',min(get(handles.scrMax_otsu,'Value')+1,20));
    set(handles.txtMax_otsu,'String',num2str(get(handles.scrMax_otsu,'Value')));
    vtset.temp.trackset.max=get(handles.scrMax_otsu,'Value');
    update(handles);
elseif strcmp(k,'numpad2')
    % reset max
    set(handles.scrMax_otsu,'Value',origTrackset.max);
    set(handles.txtMax_otsu,'String',num2str(get(handles.scrMax_otsu,'Value')));
    vtset.temp.trackset.max=get(handles.scrMax_otsu,'Value');
    update(handles);
elseif strcmp(k,'add')    
    % scrWindow 10 up
    set(handles.scrWindow,'Value',get(handles.scrWindow,'Value')-10);
    set(handles.txtWindow,'String',num2str(get(handles.scrWindow,'Value')));
    vtset.temp.trackset.win=get(handles.scrWindow,'Value');
    update(handles);
elseif strcmp(k,'subtract')   
    % scrWindow 10 down
    set(handles.scrWindow,'Value',get(handles.scrWindow,'Value')+10);
    set(handles.txtWindow,'String',num2str(get(handles.scrWindow,'Value')));
    vtset.temp.trackset.win=get(handles.scrWindow,'Value');
    update(handles);
elseif strcmp(k,'numpad0') 
    ButtonCancel_Callback([],[],handles);
end




% --- Outputs from this function are returned to the command line.
function varargout = TrackACell_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtThreshCorr_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to txtThreshCorr_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtThreshCorr_otsu as text
%        str2double(get(hObject,'String')) returns contents of txtThreshCorr_otsu as a double


% --- Executes during object creation, after setting all properties.
function txtThreshCorr_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtThreshCorr_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrThreshCorr_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to scrThreshCorr_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
v = get(handles.scrThreshCorr_otsu,'Value');
set(handles.txtThreshCorr_otsu,'String',num2str(v));
if strcmp(vtset.temp.trackset.threshmethod,'MSER')
    vtset.temp.trackset.Delta=v;
else
    vtset.temp.trackset.threshcorr=v;
end
update(handles);
uicontrol(handles.ButtonNext);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function scrThreshCorr_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrThreshCorr_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function txtSmooth_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to txtSmooth_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtSmooth_otsu as text
%        str2double(get(hObject,'String')) returns contents of txtSmooth_otsu as a double


% --- Executes during object creation, after setting all properties.
function txtSmooth_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtSmooth_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrSmooth_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to scrSmooth_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
v = round(get(handles.scrSmooth_otsu,'Value'));
set(handles.txtSmooth_otsu,'String',num2str(v));
if strcmp(vtset.temp.trackset.threshmethod,'MSER')
    vtset.temp.trackset.MinArea=v;
else
    vtset.temp.trackset.smooth=v;
end
uicontrol(handles.ButtonNext);
update(handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function scrSmooth_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrSmooth_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function txtMax_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to txtMax_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtMax_otsu as text
%        str2double(get(hObject,'String')) returns contents of txtMax_otsu as a double


% --- Executes during object creation, after setting all properties.
function txtMax_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtMax_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrMax_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to scrMax_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

v = round(get(handles.scrMax_otsu,'Value'));
set(handles.txtMax_otsu,'String',num2str(v));
if strcmp(vtset.temp.trackset.threshmethod,'MSER')
    vtset.temp.trackset.MaxArea=v;
else
    vtset.temp.trackset.max=v;
end
uicontrol(handles.ButtonNext);
update(handles);


% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function scrMax_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrMax_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in lstClumped_otsu.
function lstClumped_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to lstClumped_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
v = get(handles.lstClumped_otsu,'String');
v = v{get(handles.lstClumped_otsu,'Value')};
vtset.temp.trackset.clumped=v;
uicontrol(handles.ButtonNext);
update(handles);
% Hints: contents = get(hObject,'String') returns lstClumped_otsu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lstClumped_otsu


% --- Executes during object creation, after setting all properties.
function lstClumped_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lstClumped_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in lstDividing_otsu.
function lstDividing_otsu_Callback(hObject, eventdata, handles)
% hObject    handle to lstDividing_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
v = get(handles.lstDividing_otsu,'String');
v = v{get(handles.lstDividing_otsu,'Value')};
vtset.temp.trackset.dividing=v;
uicontrol(handles.ButtonNext);
update(handles);
% Hints: contents = get(hObject,'String') returns lstDividing_otsu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from lstDividing_otsu


% --- Executes during object creation, after setting all properties.
function lstDividing_otsu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lstDividing_otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonSave.
function ButtonSave_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
saveTrackset(handles);
initializeTimeSlider(handles);

    
% --- Executes on button press in ButtonCancel.
function ButtonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
vtset.manualswitch = 0;
initializeParams(handles);


function txtWindow_Callback(hObject, eventdata, handles)
% hObject    handle to txtWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtWindow as text
%        str2double(get(hObject,'String')) returns contents of txtWindow as a double


% --- Executes during object creation, after setting all properties.
function txtWindow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrWindow_Callback(hObject, eventdata, handles)
% hObject    handle to scrWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
v = round(get(handles.scrWindow,'Value'));
set(handles.txtWindow,'String',num2str(v));
vtset.temp.trackset.win=v;
uicontrol(handles.ButtonNext);
update(handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function scrWindow_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrWindow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in chkNearest.
function chkNearest_Callback(hObject, eventdata, handles)
% hObject    handle to chkNearest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
v = get(handles.chkNearest,'Value');
vtset.temp.trackset.usenearest=v;
update(handles);
% Hint: get(hObject,'Value') returns toggle state of chkNearest


% --- Executes on button press in invertImage.
function invertImage_Callback(hObject, eventdata, handles)
% hObject    handle to invertImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of invertImage


% --- Executes on button press in ButtonNext.
function ButtonNext_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

nexttp=getNearestTimepoint(vtset.selected.timepoint,1,handles);
if vtset.selected.timepoint~=nexttp
    vtset.selected.timepoint=nexttp;
    initializeParams(handles);
end


% --- Executes on button press in ButtonBack.
function ButtonBack_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
nexttp=getNearestTimepoint(vtset.selected.timepoint,-1,handles);
if vtset.selected.timepoint~=nexttp
    vtset.selected.timepoint=nexttp;
    initializeParams(handles);
end


% --- Executes on selection change in dropDownSegmentationChannel.
function dropDownSegmentationChannel_Callback(hObject, eventdata, handles)
% hObject    handle to dropDownSegmentationChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
vtset.temp.detectchannel=get(handles.dropDownSegmentationChannel,'Value');
uicontrol(handles.ButtonNext);
vtset.manualswitch = 1;
initializeParams(handles);
update(handles);
% Hints: contents = get(hObject,'String') returns dropDownSegmentationChannel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dropDownSegmentationChannel


% --- Executes during object creation, after setting all properties.
function dropDownSegmentationChannel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dropDownSegmentationChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in WL1timepoint.
function WL1timepoint_Callback(hObject, eventdata, handles)
% hObject    handle to WL1timepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns WL1timepoint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from WL1timepoint


% --- Executes during object creation, after setting all properties.
function WL1timepoint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WL1timepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in WL2.
function WL2_Callback(hObject, eventdata, handles)
% hObject    handle to WL2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uicontrol(handles.ButtonNext);
update(handles);
% Hints: contents = get(hObject,'String') returns WL2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from WL2


% --- Executes during object creation, after setting all properties.
function WL2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WL2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in WL2timepoint.
function WL2timepoint_Callback(hObject, eventdata, handles)
% hObject    handle to WL2timepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns WL2timepoint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from WL2timepoint


% --- Executes during object creation, after setting all properties.
function WL2timepoint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WL2timepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in WL3.
function WL3_Callback(hObject, eventdata, handles)
% hObject    handle to WL3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uicontrol(handles.ButtonNext);
update(handles);
% Hints: contents = get(hObject,'String') returns WL3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from WL3


% --- Executes during object creation, after setting all properties.
function WL3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WL3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in WL3timepoint.
function WL3timepoint_Callback(hObject, eventdata, handles)
% hObject    handle to WL3timepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns WL3timepoint contents as cell array
%        contents{get(hObject,'Value')} returns selected item from WL3timepoint


% --- Executes during object creation, after setting all properties.
function WL3timepoint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to WL3timepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function scrTimepointx_Callback(hObject, eventdata, handles)
% hObject    handle to Timepointtext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
v = round(get(handles.scrTimepointx,'Value'));
v2 = str2double(get(handles.txtTimepoint,'String'));
%%%% get nearest timepoint
nearest=getNearestTimepoint(v,0,handles);
uicontrol(handles.ButtonNext);
%%% only if different number
if nearest~=v2
    set(handles.txtTimepoint,'String',num2str(nearest))
    vtset.selected.timepoint=nearest;
    initializeParams(handles);
end
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function Timepointtext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Timepointtext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function txtTimepoint_Callback(hObject, eventdata, handles)
% hObject    handle to txtTimepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtTimepoint as text
%        str2double(get(hObject,'String')) returns contents of txtTimepoint as a double


% --- Executes during object creation, after setting all properties.
function txtTimepoint_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtTimepoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% gets cell&timepoint, sets all params and calls update
function initializeParams(handles)
global vtset position abstime imageQuant imageDetect imageNonFluor qchannames
if nargin<1
    handles = guidata(vtset.trackACellFocus);
end
figure(handles.TrackACell)

    %windowpositioning
    vtset.windowposition = get(gcf,'position');
    if(isfield(vtset,'windowposition'))        
        set(gcf,'position',vtset.windowposition);
        vtset.windowposition = get(gcf,'position');
    end
    
set(handles.lblInfo,'String','initializing...');
drawnow
%%%%%%%%%%%%%%%%% get cell & timepoint
tic
cell = vtset.selected.cell;
timepoint = vtset.selected.timepoint;
channel = vtset.selected.channel; %selected quant channel
if numel(cell)<1
    msgbox('No cell selected - this should not happen anymore');
    return;
end
curtreeQuant = vtset.results{vtset.treenum}.quants; %% is an struct array
detectchannel =  curtreeQuant(channel).detectchannel(curtreeQuant(channel).cellNr == cell & curtreeQuant(channel).timepoint == timepoint);
quantDetectChannel=[];
cellidQuant=[];

% first gather all quantification channels which are using the current
% detection

% get the former detect chan
formerDetectchannel = detectchannel;

for qchan=1:numel(curtreeQuant)
    temp = curtreeQuant(qchan).detectchannel(curtreeQuant(qchan).cellNr==cell & curtreeQuant(qchan).timepoint==timepoint);
    if temp == formerDetectchannel
        quantDetectChannel(end+1) = qchan;
        cellidQuant(end+1) = find(curtreeQuant(qchan).cellNr==cell & curtreeQuant(qchan).timepoint==timepoint);
    end
end

% then get all other quantification channels, if detection has been
% switched.
switchchan = 0;
selectedDetectchannel = vtset.temp.detectchannel; % this variable could only be changed by the dropdown menu
if detectchannel ~= selectedDetectchannel && vtset.manualswitch
    % if this was switched, all other quants which belong to this detect
    % should be udpated as well
    fprintf('switching to channel %d\n',selectedDetectchannel)
    detectchannel = selectedDetectchannel;
    switchchan = 1;
    for qchan=1:numel(curtreeQuant)
        temp = curtreeQuant(qchan).detectchannel(curtreeQuant(qchan).cellNr==cell & curtreeQuant(qchan).timepoint==timepoint);
        if temp == vtset.temp.detectchannel
            quantDetectChannel(end+1) = qchan;
            cellidQuant(end+1) = find(curtreeQuant(qchan).cellNr==cell & curtreeQuant(qchan).timepoint==timepoint);
        end
    end
end

if numel(quantDetectChannel) ~= numel(unique(quantDetectChannel))
    errordlg('Error: Really strange bug with switching channels. Please report this bug!')
end
%%%%%% OLD
%{
% change corresponding to selected segmentation channel from dropdown
switchchan = 0;
selectedchannel = vtset.temp.detectchannel;
if detectchannel ~= selectedchannel && vtset.manualswitch
    fprintf('switching to channel %d\t',selectedchannel)
    detectchannel = selectedchannel;
    switchchan = 1;
end
    %}
curtreeDetect = vtset.results{vtset.treenum}.detects(detectchannel); % only the detect channel
cellidDetect = curtreeDetect.cellNr==cell & curtreeDetect.timepoint==timepoint;


%%%%%% OLD
%{
% get all quant channel with this detect channel
% quant channel numbers are stored in quantDetectChannel
% corresponding IDs are stored in cellidQuant
for qchan=1:numel(curtreeQuant)
    temp = curtreeQuant(qchan).detectchannel(curtreeQuant(qchan).cellNr==cell & curtreeQuant(qchan).timepoint==timepoint);
    if temp == vtset.temp.detectchannel
        quantDetectChannel(end+1) = qchan;
        cellidQuant(end+1) = find(curtreeQuant(qchan).cellNr==cell & curtreeQuant(qchan).timepoint==timepoint);
    end
end
% always add current channel temporary ?
%%% WTF
if switchchan
    quantDetectChannel(end+1) = channel;
    cellidQuant(end+1) = find(curtreeQuant(channel).cellNr==cell & curtreeQuant(channel).timepoint==timepoint);
end
 %}



if ~numel(cellidQuant)
    fprintf('Currently, no quantification channel found for timepoint %d, cell %d on detection channel %d\n',timepoint, cell, channel)
end

% maxtimepoint=max(curtreeQuant(channel).timepoint(curtreeQuant(channel).cellNr==cell));
% mintimepoint=min(curtreeQuant(channel).timepoint(curtreeQuant(channel).cellNr==cell));



%%%%%%% set temporary trackset/cellmask/stuff
trackset=curtreeDetect.trackset(cellidDetect);
% for version<0.9.9
if iscell(trackset)
    trackset=cell2mat(trackset);
end
vtset.temp.cellidDetect=cellidDetect;
% vtset.temp.detectchannel = detectchannel; %%%% STILL RELEVANT?
vtset.temp.trackset=trackset;
vtset.temp.cellMask=curtreeDetect.cellmask{cellidDetect};
vtset.temp.X=curtreeDetect.X(cellidDetect);
vtset.temp.Y=curtreeDetect.Y(cellidDetect);
vtset.temp.size=curtreeDetect.size(cellidDetect);
vtset.temp.perimeter=curtreeDetect.perimeter(cellidDetect);
vtset.temp.cellNr = cell;
vtset.temp.position = curtreeDetect.positionIndex(cellidDetect);
vtset.temp.abstime=curtreeDetect.absoluteTime(cellidDetect);
vtset.temp.timepoint=curtreeDetect.timepoint(cellidDetect);
vtset.temp.int=[];
for qchan = 1:numel(quantDetectChannel)
    vtset.temp.int(qchan)=curtreeQuant(quantDetectChannel(qchan)).int(cellidQuant(qchan));
end
vtset.temp.cellidQuant = cellidQuant;
vtset.temp.quantDetectChannel = quantDetectChannel;



% generate info string
% infoStr = sprintf('cell: %d, tp: %d, position: %d, x: %.0f, y: %.0f, value: %.3f', cell, ...
%     timepoint, curtreeQuant.positionIndex(cellid),curtreeQuant.X(cellid), curtreeQuant.Y(cellid), curtreeQuant.int(cellid));

% set all params
set(handles.txtWindow, 'String', num2str(trackset.win));
set(handles.scrWindow, 'Value', trackset.win);
set(handles.txtThreshCorr_otsu, 'String', num2str(trackset.threshcorr));
set(handles.scrThreshCorr_otsu, 'Value', trackset.threshcorr);
set(handles.txtSmooth_otsu, 'String', num2str(trackset.smooth));
set(handles.scrSmooth_otsu, 'Value', trackset.smooth);
set(handles.txtMax_otsu, 'String', num2str(trackset.max));
set(handles.scrMax_otsu, 'Value', trackset.max);
set(handles.lstClumped_otsu,'Value',find(strcmp( get(handles.lstClumped_otsu,'String'), trackset.clumped)));
set(handles.lstDividing_otsu,'Value',find(strcmp( get(handles.lstDividing_otsu,'String'), trackset.dividing)));
% set(handles.scrTimepointx,'min',mintimepoint,'max',maxtimepoint,'Value',timepoint,'SliderStep',[1 1]./(maxtimepoint-mintimepoint));
set(handles.txtTimepoint, 'String', num2str(timepoint));
v = get(handles.popupMode,'String');
if isempty(find(strcmp(trackset.threshmethod,v), 1)) && strcmp(trackset.threshmethod,'External Segmentation')
    v{end+1} = 'External Segmentation';
    set(handles.popupMode,'String',v);
end
set(handles.popupMode,'Value', find(strcmp(trackset.threshmethod,v)));
set(handles.dropDownSegmentationChannel,'Value',vtset.temp.detectchannel);


updateGUIslider(handles)
initializeTimeSlider(handles);
% if isfield(trackset,'mode')
%     v = get(handles.popupMode,'String')
%     set(handles.popupMode,'Value', find(strcmp(trackset.mode,v)));
% else
%     set(handles.popupMode,'Value', 1);
% end



%%%% update line in timecourse
if isfield(vtset,'linehandle')
    try
        for lh = 1:numel(vtset.linehandle)
            delete(vtset.linehandle(lh));
        end
        vtset=rmfield(vtset,'linehandle');
    catch exception
        % never mind
    end
end

for lh = 1:numel(quantDetectChannel)
    vtset.linehandle(lh)=line(...
        [curtreeQuant(quantDetectChannel(lh)).(vtset.timemode)(cellidQuant((lh)))/vtset.timemodifier ...
        curtreeQuant(quantDetectChannel(lh)).(vtset.timemode)(cellidQuant((lh)))/vtset.timemodifier],...
        [0 max(get(vtset.haxes(vtset.showquantification == channel),'yLim'))],...
        'parent',vtset.haxes(vtset.showquantification == channel),'Color',[1 .5 0]);
end

set(handles.lblInfo,'String','updating...');
drawnow


%%% update datacursor
vtset.datacursor.Mgr.removeAllDataCursors();
% vtset.datacursor.hDatatip = createDatatip(vtset.datacursor.Mgr, vtset.hcurves(vtset.hcurvescells == cell & vtset.hcurveschannel == vtset.dcselected.channel));
% visi={'off','on'};
% set(vtset.datacursor.Mgr.CurrentDataCursor.textBoxHandle,'Visible',visi{vtset.datacursor.showtip+1})
% if isempty(vtset.datacursor.Mgr.DataCursor)
    hcurve=vtset.hcurves(vtset.hcurvescells == cell & vtset.hcurveschannel == vtset.dcselected.channel);
    createDatatip(vtset.datacursor.Mgr, hcurve(1));
% end
datatime = curtreeQuant(vtset.dcselected.channel).(vtset.timemode)(curtreeQuant(channel).cellNr==cell & curtreeQuant(channel).timepoint==timepoint);
pos =  [datatime/vtset.timemodifier vtset.temp.int(vtset.temp.quantDetectChannel == vtset.dcselected.channel)];
dataindex = curtreeQuant(channel).timepoint(curtreeQuant(channel).cellNr==cell);
dataindex=find(timepoint == sort(dataindex));
set(get(vtset.datacursor.Mgr.CurrentDataCursor,'DataCursor'),'TargetPoint',pos,'DataIndex',dataindex,'Target',hcurve(1))
set(vtset.datacursor.Mgr.CurrentDataCursor,'position',pos)
updateDataCursors(vtset.datacursor.Mgr)
% set(vtset.datacursor.Mgr ,'DisplayStyle','window')   ;
%%%%%%%%%%%%% get the images

%%%%%%%%%%%%%%%% get the detect image
imgdetectfile=curtreeDetect.filename{cellidDetect};
%%% TODO: make normalize image more generic
if strcmp(trackset.threshmethod,'MSER')
    imageDetect = loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],3,curtreeDetect.settings.wl);
else
    imageDetect = loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],curtreeDetect.settings.normalize,curtreeDetect.settings.wl);
end
% imageDetect=loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],curtreeDetect.settings.normalize,curtreeDetect.settings.wl);
% imageDetect=imageDetect+loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile(1:end-5) '1.tif'],curtreeDetect.settings.normalize,'w1.tif');

%%%% show all background & gain
if 0
    
    bild=loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],0,curtreeDetect.settings.wl);
    [~,bg,gain] =loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],curtreeDetect.settings.normalize,curtreeDetect.settings.wl);
    figure;
    imagesc(bild)
    adjustContrastIntelligent
    tobase(bild)
    title(sprintf('original mean %3.4f timepoint %d',mean(bild(:)),vtset.temp.timepoint))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
    figure;
    imagesc(bg)
    title(sprintf('bg mean %3.4f',mean(bg(:))))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
    figure;
    imagesc(gain)
    title(sprintf('gain mean %3.4f',mean(gain(:))))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
    figure;
    imagesc(imageDetect)
        adjustContrastIntelligent

    title(sprintf('normalized mean %3.4f timepoint %d',mean(imageDetect(:)),vtset.temp.timepoint))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
end
%%%%%%%%%%%%%%%% load corresponding quantification channels
imageQuant={};
qchannames={};
for qchan = 1:numel(quantDetectChannel)
    imgQuantFile=curtreeQuant(quantDetectChannel(qchan)).filename{cellidQuant(qchan)};
    qchannames{qchan} = curtreeQuant(quantDetectChannel(qchan)).settings.wl;
    imageQuant{qchan}=loadimage([vtset.results{vtset.treenum}.settings.imgroot imgQuantFile],curtreeQuant(quantDetectChannel(qchan)).settings.normalize,curtreeQuant(quantDetectChannel(qchan)).settings.wl);
end

%%%% show all quant+bg+gain
if 0
    
    bild=loadimage([vtset.results{vtset.treenum}.settings.imgroot imgQuantFile],0,curtreeQuant(end).settings.wl);
    [~,bg,gain] =loadimage([vtset.results{vtset.treenum}.settings.imgroot imgQuantFile],curtreeQuant(end).settings.normalize,curtreeQuant(end).settings.wl);
    figure;
    imagesc(bild)
    tobase(bild)
    title(sprintf('original mean %3.4f timepoint %d',mean(bild(:)),vtset.temp.timepoint))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
    figure;
    imagesc(bg)
    title(sprintf('bg mean %3.4f',mean(bg(:))))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
    figure;
    imagesc(gain)
    
    title(sprintf('gain mean %3.4f',mean(gain(:))))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
    figure;
    imagesc(imageDetect)
    caxis([medianfirst(sort(imageDetect(:)),300),topmedian(imageDetect(:),300)])
    title(sprintf('normalized mean %3.4f timepoint %d',mean(imageDetect(:)),vtset.temp.timepoint))
    hold on 
    plot(vtset.temp.X,vtset.temp.Y,'o')
end

%%%%%%%%%%%%%%%% load corresponding nonFluor
try
    imgnonFluorfile = vtset.results{vtset.treenum}.nonFluor.filename{vtset.results{vtset.treenum}.nonFluor.timepoint == vtset.temp.timepoint & vtset.results{vtset.treenum}.nonFluor.cellNr == vtset.temp.cellNr};
    imageNonFluor = loadimage([vtset.results{vtset.treenum}.settings.imgroot imgnonFluorfile],0);
catch e
    warning('Non Fluor image not found!')
    imageNonFluor = zeros(size(imageQuant{1}));
end
    
% if channel was switched segment and quantify again
if switchchan
    update(handles)
end

% if someone clicked an timepoint without a cellmask, do segmentation first
if vtset.temp.cellMask == 0
    update(handles)
end


redraw(handles);
toc
drawnow

%%%%%%% cache next picture
nexttp=getNearestTimepoint(vtset.selected.timepoint,1,handles);
imgdetectfile=curtreeDetect.filename{curtreeDetect.cellNr==cell & curtreeDetect.timepoint==nexttp};
loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],curtreeDetect.settings.normalize,curtreeDetect.settings.wl);

for qchan = 1:numel(quantDetectChannel)
    try
    imgQuantFile=curtreeQuant(quantDetectChannel(qchan)).filename{curtreeQuant(quantDetectChannel(qchan)).cellNr==cell & curtreeQuant(quantDetectChannel(qchan)).timepoint==nexttp};
    loadimage([vtset.results{vtset.treenum}.settings.imgroot imgQuantFile],curtreeQuant(quantDetectChannel(qchan)).settings.normalize,curtreeQuant(quantDetectChannel(qchan)).settings.wl);
    catch e
        fprintf('Next timepoint not found for Detection channel %d in quantification channel %d\n',vtset.temp.detectchannel,quantDetectChannel(qchan))
    end
end
try
    imgnonFluorfile = vtset.results{vtset.treenum}.nonFluor.filename{vtset.results{vtset.treenum}.nonFluor.timepoint == nexttp & vtset.results{vtset.treenum}.nonFluor.cellNr == vtset.temp.cellNr};
    loadimage([vtset.results{vtset.treenum}.settings.imgroot imgnonFluorfile],0);
catch e
    warning('Non Fluor image not found!')
end





% do segmentation & quantification
function update(handles)
tic
global vtset imageQuant imageDetect freehand imageNonFluor
set(handles.lblInfo,'String','updating...');
drawnow

%%%%%%%%%%%%%% segmentation & show images

%%%%% WORKAROUND snakes
% imgdetectfile=vtset.results{vtset.treenum}.detects(vtset.selected.channel).filename{vtset.results{vtset.treenum}.detects(vtset.selected.channel).cellNr==vtset.selected.cell & vtset.results{vtset.treenum}.detects(vtset.selected.channel).timepoint==vtset.selected.timepoint};
% [~,bg] = loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],1,'w2');
% imgraw = loadimage([vtset.results{vtset.treenum}.settings.imgroot imgdetectfile],0,'w2');


trackset = vtset.temp.trackset;


% load external segmentation if neede
if strcmp(trackset.threshmethod,'External Segmentation')

    name = vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).filename{vtset.temp.cellidDetect};
    name = strsplit_qtfy('.', name);
    name  = [ name{1} vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).settings.externalSegmentation.extension];
    if vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).settings.externalSegmentation.multiFolder
        [a b c ]=fileparts(name);
        name = [a '/segmentation/' b c];
    else
        name = ['segmentation/' name];
    end
    %                 name = [values{1} vtset.results{tree}.detects(channelIndex).settings.wl(1:length(vtset.results{tree}.detects(channelIndex).settings.wl))];
    image = loadimage([vtset.results{vtset.treenum}.settings.imgroot name],vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).settings.normalize, vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).settings.wl);
else
    image = imageDetect;
end


% segmentation
% [Center, subimg] = cellSegmentation(imageDetect, vtset.temp.X, vtset.temp.Y, vtset.temp.trackset,bg,imgraw);
[Center, subimg] = cellSegmentation(image, vtset.temp.X, vtset.temp.Y, vtset.temp.trackset);

%%% make ellipse or freehand segmentation
if strcmp(trackset.threshmethod,'Ellipse')
    % do the ellipse thing
    if isfield(freehand,'delete')
        freehand.delete;
    end
    % plot temporary for getting correct size of Center
%     subplot(handles.('axesDetectSegmented'));
    imagesc(subimg,'parent',handles.axesDetectSegmented);
    
    if isfield(trackset,'ellipse') && numel(trackset.ellipse)
        eposition = trackset.ellipse;
        % rearrange if window size is altered
        eposition(1) = eposition(1)- (size(vtset.temp.cellMask,1)-size(Center,1))/2;
        eposition(2) = eposition(2)- (size(vtset.temp.cellMask,2)-size(Center,2))/2;
        freehand = imellipse(handles.('axesDetectSegmented'),eposition);
        
    else
        % show outline image without outline
%         subplot(handles.('axesDetectSegmented'));
        imagesc(subimg,'parent',handles.axesDetectSegmented);
        axis(handles.axesDetectSegmented,'off')
        axis(handles.axesDetectSegmented,'equal')
        title(handles.axesDetectSegmented,'Draw here');
        if isfield(trackset,'drawnew')
            freehand = imellipse(handles.('axesDetectSegmented'));
            trackset=rmfield(trackset,'drawnew');
        else
            if ~isfield(trackset,'ellipseradius') || ~numel(trackset.ellipseradius)
                trackset.ellipseradius = 10;
            end
            
            stats = regionprops(Center,'Centroid');
            %%%% update to editable radius!
            eposition = [(stats.Centroid-trackset.ellipseradius) 2*trackset.ellipseradius 2*trackset.ellipseradius];
            freehand = imellipse(handles.('axesDetectSegmented'),eposition);
        end
    end

    
    Center = freehand.createMask;
    trackset.ellipse = freehand.getPosition;
%     freehand.addNewPositionCallback(@ButtonUpdateCircle_Callback);
    set(gcf,'WindowButtonUpFcn',@updateCircle);

elseif strcmp(trackset.threshmethod,'Freehand')
    % draw freehand, but check if already drawn
    if isfield(trackset,'freehand') && numel(trackset.freehand)
        Center = trackset.freehand;
    else
        
%         if isfield(freehand,'delete')
%            freehand.delete; 
%         end

        % show outline image without outline
%         subplot(handles.('axesDetectSegmented'));
        imagesc(subimg,'parent',handles.axesDetectSegmented);
        axis(handles.axesDetectSegmented,'off')
        axis(handles.axesDetectSegmented,'equal')
        title(handles.axesDetectSegmented,'Draw here');
        
        freehand = imfreehand(handles.('axesDetectSegmented'));
        trackset.freehand = freehand.createMask;
        
%         set(gcf,'WindowButtonUpFcn',@updateFreehand);
        Center = freehand.createMask;
       

    end
end

mysize= sum(Center(:));
periemter = sum(sum(bwperim(Center)));

%%%% quantification
int=[];
for qchan = 1:numel(vtset.temp.cellidQuant)
    subimgQuant = extractSubImage(imageQuant{qchan}, vtset.temp.X, vtset.temp.Y, trackset.win);
    dasum=subimgQuant(Center);
%     dasum(dasum<0)=0;
    int(qchan)= sum(dasum);
end

% store temp stats
vtset.temp.int=int;
vtset.temp.size=mysize;
vtset.temp.perimeter=periemter;
vtset.temp.cellMask=Center;
vtset.temp.mean=mysize;
vtset.temp.trackset = trackset;

redraw(handles)
toc


% plots all images
function redraw(handles)
global vtset imageQuant imageDetect freehand imageNonFluor qchannames
if nargin<1
    handles = guidata(vtset.trackACellFocus); 
end
cellMask = vtset.temp.cellMask;

subimgDetect = extractSubImage(imageDetect, vtset.temp.X, vtset.temp.Y, vtset.temp.trackset.win);
subimgQuant={};
for chan= 1:numel(imageQuant)
    subimgQuant{chan} = extractSubImage(imageQuant{chan}, vtset.temp.X, vtset.temp.Y, vtset.temp.trackset.win);
end
subimgNonFluor = extractSubImage(imageNonFluor, vtset.temp.X, vtset.temp.Y, vtset.temp.trackset.win);


% show them
% show original deteciton channel
% subplot(handles.('axesDetectOrig'));
id = strcmp(vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).settings.wl,vtset.contrast.wl);
if ischar(vtset.contrast.contrast{id})
    a=imagesc(subimgDetect,'parent',handles.axesDetectOrig);
%     cmin = min(subimgDetect(:));
%     cmax = max(subimgDetect(:));
else
    a=imagesc(subimgDetect,'parent',handles.axesDetectOrig,vtset.contrast.contrast{id});
%     cmin = vtset.contrast.contrast{id}(1);
%     cmax = vtset.contrast.contrast{id}(2);
end
axis(handles.axesDetectOrig,'off')
axis(handles.axesDetectOrig,'equal')
colormap(gray);
title(handles.axesDetectOrig,'Original');
% and contrast menu not needed here


% show detected nuclei
% subplot(handles.('axesDetectSegmented'));
% imagesc(cellMask);
% axis off
% axis equal

% show outline
% subplot(handles.('axesDetectSegmented'));
BWoutline = bwperim(cellMask);
Segout = subimgDetect;
Segout(BWoutline) = max(subimgDetect(:))*1.2;
if ischar(vtset.contrast.contrast{id})
    a=imagesc(Segout,'parent',handles.axesDetectSegmented);
    cmin = min(Segout(:));
    cmax = max(Segout(:));
else
    a=imagesc(Segout,'parent',handles.axesDetectSegmented,vtset.contrast.contrast{id});
    cmin = vtset.contrast.contrast{id}(1);
    cmax = vtset.contrast.contrast{id}(2);
end
axis(handles.axesDetectSegmented,'off')
axis(handles.axesDetectSegmented,'equal')
title(handles.axesDetectSegmented,'Outline');
% contrast menu
hcmenu = uicontextmenu('parent',vtset.trackACellFocus);
cb = ['global vtset;vtset.contrast.contrast{' num2str(find(id)) '} = [' num2str(cmin) ' ' num2str(cmax) ']; adjustContrast(' num2str(cmin) ', ' num2str(cmax) ',''vtset'',''vtset.redrawCellInspector()'',''vtset.contrast.contrast{' num2str(find(id)) '}(1)'',''vtset.contrast.contrast{' num2str(find(id)) '}(2)'')'];
item1 = uimenu(hcmenu, 'Label', 'Adjust Contrast', 'Callback', cb);
set(a,'uicontextmenu',hcmenu)



% show nonFluor
if strcmp(get(handles.('showOutlineNonFluor'),'checked'), 'on')
    subimgNonFluor(BWoutline) = max(subimgNonFluor(:))*1.2;
end
id = strcmp(vtset.results{vtset.treenum}.nonFluor.settings.wl,vtset.contrast.wl);
if all(~id) || ischar(vtset.contrast.contrast{id})
    a=imagesc(subimgNonFluor,'parent',handles.axesNonFluor);
    cmin = min(subimgNonFluor(:));
    cmax = max(subimgNonFluor(:));
else
    a=imagesc(subimgNonFluor,'parent',handles.axesNonFluor,vtset.contrast.contrast{id});
    cmin = vtset.contrast.contrast{id}(1);
    cmax = vtset.contrast.contrast{id}(2);
end

axis(handles.axesNonFluor,'off')
axis(handles.axesNonFluor,'equal')
colormap(gray);
% contrast menu
hcmenu = uicontextmenu('parent',vtset.trackACellFocus);
cb = ['global vtset;vtset.contrast.contrast{' num2str(find(id)) '} = [' num2str(cmin) ' ' num2str(cmax) ']; adjustContrast(' num2str(cmin) ', ' num2str(cmax) ',''vtset'',''vtset.redrawCellInspector()'',''vtset.contrast.contrast{' num2str(find(id)) '}(1)'',''vtset.contrast.contrast{' num2str(find(id)) '}(2)'')'];
item1 = uimenu(hcmenu, 'Label', 'Adjust Contrast', 'Callback', cb);
set(a,'uicontextmenu',hcmenu)

% title(handles.axesNonFluor,'Brightfield');

%{
% show cell numbers in brightfield
if strcmp(get(handles.showCellNumbers, 'checked'), 'on')
    allx = vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).X(vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).timepoint == vtset.temp.timepoint & vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).positionIndex == vtset.temp.position);
    ally = vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).Y(vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).timepoint == vtset.temp.timepoint & vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).positionIndex == vtset.temp.position);
    allcellNr = vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).cellNr(vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).timepoint == vtset.temp.timepoint & vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).positionIndex == vtset.temp.position);

    ids = allx-vtset.temp.X+vtset.temp.trackset.win/2<vtset.temp.trackset.win & allx-vtset.temp.X+vtset.temp.trackset.win/2>0 & ally-vtset.temp.Y+vtset.temp.trackset.win/2<vtset.temp.trackset.win & ally-vtset.temp.Y+vtset.temp.trackset.win/2 > 0;
    for id=find(ids)
        text(allx(id)-vtset.temp.X+vtset.temp.trackset.win/2,ally(id)-vtset.temp.Y+vtset.temp.trackset.win/2,sprintf('%d',allcellNr(id)),'color','red','parent',handles.axesNonFluor)
    end
end
%}

% show cell numbers in brightfield
trees = [];
if strcmp(get(handles.showCellNumbers, 'checked'), 'on')
    trees=1:numel(vtset.results);
end
if strcmp(get(handles.showSingleCell, 'checked'), 'on')
    trees=union(trees,vtset.treenum);
end
% trees
% if strcmp(get(handles.showCellNumbers, 'checked'), 'on')
    for i = trees
        % get coordinates of every tree, use always channel 1
        allx = vtset.results{i}.detects(1).X(vtset.results{i}.detects(1).timepoint == vtset.temp.timepoint & vtset.results{i}.detects(1).positionIndex == vtset.temp.position);
        ally = vtset.results{i}.detects(1).Y(vtset.results{i}.detects(1).timepoint == vtset.temp.timepoint & vtset.results{i}.detects(1).positionIndex == vtset.temp.position);
        allcellNr = vtset.results{i}.detects(1).cellNr(vtset.results{i}.detects(1).timepoint == vtset.temp.timepoint & vtset.results{i}.detects(1).positionIndex == vtset.temp.position);

        ids = allx-vtset.temp.X+vtset.temp.trackset.win/2<vtset.temp.trackset.win & allx-vtset.temp.X+vtset.temp.trackset.win/2>0 & ally-vtset.temp.Y+vtset.temp.trackset.win/2<vtset.temp.trackset.win & ally-vtset.temp.Y+vtset.temp.trackset.win/2 > 0;
        for id=find(ids)
            try
            c_str=regexp(regexp(vtset.results{i}.settings.treefile,'_','split'), '-', 'split');  
            position = num2str(str2double(regexprep(c_str{1,2}{1,1}, '[a-z]+|[A-Z]', '')));
            colony = num2str(str2double(regexprep(c_str{1,2}{1,2}, '([a-z]+|[A-Z]+|\.)', '')));
            if i == vtset.treenum
                cell_id_name = ['Cell '  sprintf('%d',allcellNr(id))];
            else
                cell_id_name = ['Pos' position ',Col'  colony  ',C'  sprintf('%d',allcellNr(id))];
            end
            text(allx(id)-vtset.temp.X+vtset.temp.trackset.win/2,ally(id)-vtset.temp.Y+vtset.temp.trackset.win/2, cell_id_name,'color','red','parent',handles.axesNonFluor)
            catch e
                warning('Error creating cell number text for BF image')
            end
        end
    end  
% end

% prepare quant text
intstr=cell(1,numel(vtset.temp.int));
for qc = 1:numel(vtset.temp.int)
    intstr{qc} =  sprintf('Int: %.3f ',vtset.temp.int(qc));
end

%show quantifications with outline
uniquants = unique(qchannames);

% init dropdowns
for i = 1:min(3,numel(uniquants))
    set(handles.(sprintf('dropDownQuantification%d',i)),'visible','on')
    set(handles.(sprintf('axesQuant%d',i)),'visible','on')
    set(handles.(sprintf('textQuant%d',i)),'visible','on');
end
for i=numel(uniquants)+1:3
    set(handles.(sprintf('axesQuant%d',i)),'visible','off')
    set(handles.(sprintf('dropDownQuantification%d',i)),'visible','off')
    set(handles.(sprintf('textQuant%d',i)),'visible','off');
end

for unichan = 1:min(3,numel(uniquants))
    chanName = get(handles.(sprintf('dropDownQuantification%d',unichan)),'string');
    chanName = chanName{get(handles.(sprintf('dropDownQuantification%d',unichan)),'Value')};
    chan = unichan;
    qchan = find(strcmp(chanName,qchannames));
    if numel(qchan)>1
        qchan = qchan(1);
    end
    
     if vtset.showaxesQuants.active(chan) && numel(qchan)
        set(handles.(sprintf('axesQuant%d',chan)),'visible','on')
%         subplot(handles.(sprintf('axesQuant%d',chan)));
        tempimg=subimgQuant{qchan};
        if vtset.showaxesQuants.showoutline(chan)
            tempimg(BWoutline) = max(tempimg(:))*1.2;
        end
        
        id = strcmp(qchannames{qchan},vtset.contrast.wl);
        if ischar(vtset.contrast.contrast{id})
            a=imagesc(tempimg,'parent',handles.(sprintf('axesQuant%d',chan)));
            cmin = min(tempimg(:));
            cmax = max(tempimg(:));
        else
            a=imagesc(tempimg,'parent',handles.(sprintf('axesQuant%d',chan)),vtset.contrast.contrast{id});
            cmin = vtset.contrast.contrast{id}(1);
            cmax = vtset.contrast.contrast{id}(2);
        end
        set(handles.(sprintf('textQuant%d',chan)),'String',intstr{qchan})
        axis(handles.(sprintf('axesQuant%d',chan)),'off')
        axis(handles.(sprintf('axesQuant%d',chan)),'equal')
        colormap(gray);
%         title(handles.(sprintf('axesQuant%d',chan)),['Quantification: ' qchannames{chan}]);
        
        % contrast menu
        hcmenu = uicontextmenu('parent',vtset.trackACellFocus);
        cb = ['global vtset;vtset.contrast.contrast{' num2str(find(id)) '} = [' num2str(cmin) ' ' num2str(cmax) ']; adjustContrast(' num2str(cmin) ', ' num2str(cmax) ',''vtset'',''vtset.redrawCellInspector()'',''vtset.contrast.contrast{' num2str(find(id)) '}(1)'',''vtset.contrast.contrast{' num2str(find(id)) '}(2)'')'];
        item1 = uimenu(hcmenu, 'Label', 'Adjust Contrast', 'Callback', cb);
        set(a,'uicontextmenu',hcmenu)

    else
        cla(handles.(sprintf('axesQuant%d',chan)))
        set(handles.(sprintf('axesQuant%d',chan)),'visible','off')
    end
end


% draw clickable elipse
if strcmp(vtset.temp.trackset.threshmethod,'Ellipse')
    if ~isfield(vtset.temp.trackset,'ellipse') || ~numel(vtset.temp.trackset.ellipse)
        update(handles)
    end
    freehand = imellipse(handles.('axesDetectSegmented'),vtset.temp.trackset.ellipse);
end


%recalc time to hh:mm
abstime=vtset.temp.abstime/3600;
hours=floor(abstime);
minutes=mod(abstime,1);
minutes=minutes*60;

if ~numel(vtset.temp.int)
    vtset.temp.int=Inf;
end

% update static text


insp = vtset.results{vtset.treenum}.quants(vtset.selected.channel).inspected(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr == vtset.selected.cell);

infoStr = sprintf('Cell: %d\nPosition: %d, x: %.0f, y: %.0f,  Size: %d px',  ...
     vtset.temp.cellNr, vtset.temp.position, vtset.temp.X, vtset.temp.Y,vtset.temp.size);
 
 set(handles.txtTime,'String',sprintf('%2.0fh %2.0fm',hours,minutes))

set(handles.lblInfo,'String',infoStr);

vtset.haxesTimeSlider.selected = vtset.temp.timepoint;
updateTimeSliderCallback(vtset.temp.timepoint,vtset.haxesTimeSlider.status(vtset.haxesTimeSlider.timepoint == vtset.temp.timepoint))

drawnow





% --- Executes on button press in ButtonSaveNext.
function ButtonSaveNext_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSaveNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
saveTrackset(handles);
nexttp=getNearestTimepoint(vtset.selected.timepoint,1,handles);
if vtset.selected.timepoint~=nexttp
    vtset.selected.timepoint=nexttp;
    initializeParams(handles);
else
    redraw(handles);
    initializeTimeSlider(handles);
end



% --- Executes on button press in ButtonSaveBack.
function ButtonSaveBack_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSaveBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
saveTrackset(handles);
nexttp=getNearestTimepoint(vtset.selected.timepoint,-1,handles);
if vtset.selected.timepoint~=nexttp
    vtset.selected.timepoint=nexttp;
    initializeParams(handles);
end


% function channel=mapChannel(channel)
% global vtset
% 
% if findstr(channel,vtset.results.settings.extQuant)
%     channel = 'quants';
% elseif findstr(channel,vtset.results.settings.extDetect)
%     channel = 'detects';
% elseif findstr(channel,vtset.results.settings.extNonFluor)
%     channel = 'nonFluor';
% else
%     channel = [];
% end
    


% --- Executes during object creation, after setting all properties.
function scrTimepointx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scrTimepointx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function nearest=getNearestTimepoint(tp,step,handles)
global vtset
cell=vtset.selected.cell;

if get(handles.outliermodus,'Value')
    detectOutliers(handles);
    if numel(vtset.outliers)>0
        timepoints = vtset.outliers;
    else
        timepoints=vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1);
        step=-10000;
    end
else
    timepoints=vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1);
end
timepoints = sort(timepoints);

[~, c]=min(abs(timepoints-tp));

if c+step<=0
    nearest=min(timepoints);
elseif c+step>numel(timepoints)
    nearest=max(timepoints);
else
    nearest=timepoints(c+step);
end

  function nearest=getNearestCell(cell,step)
global vtset

cells=unique(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr);
[~, c]=min(abs(cells-cell));

if c+step<=0
    nearest=cells(1);
elseif c+step>numel(cells)
    nearest=cells(end);
else
    nearest=cells(c+step);
end  



function saveTrackset(handles)
global vtset
% tobe stored: trackset, int, mean,size,cellmask,[ecc]
if isfield(vtset.temp.trackset,'mode')
    vtset.temp.trackset = rmfield(vtset.temp.trackset,'mode');
    'aiaiaia'
end
% if isstructEqual( vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).trackset(vtset.temp.cellidDetect) , vtset.temp.trackset)
%     vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).trackset(vtset.temp.cellidDetect) = vtset.temp.trackset;
% else
    
    for f = fieldnames(vtset.temp.trackset)'
        vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).trackset(vtset.temp.cellidDetect).(cell2mat(f)) = vtset.temp.trackset.(cell2mat(f));
    end
    
% end
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).X(vtset.temp.cellidDetect) = vtset.temp.X;
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).Y(vtset.temp.cellidDetect) = vtset.temp.Y;
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).size(vtset.temp.cellidDetect) = vtset.temp.size;
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).perimeter(vtset.temp.cellidDetect) = vtset.temp.perimeter;
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).cellmask{vtset.temp.cellidDetect} = vtset.temp.cellMask;
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).inspected(vtset.temp.cellidDetect) = 1;
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).active(vtset.temp.cellidDetect) = 1;

% store quantification

for qchan = 1:numel(vtset.temp.quantDetectChannel)
    vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).int(vtset.temp.cellidQuant(qchan)) = vtset.temp.int(qchan);
    vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).active(vtset.temp.cellidQuant(qchan)) = 1;
    vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).inspected(vtset.temp.cellidQuant(qchan)) = 1;
%     vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).mean(vtset.temp.cellidQuant(qchan)) = vtset.temp.int(qchan)/vtset.temp.mean;
    vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).detectchannel(vtset.temp.cellidQuant(qchan)) = vtset.temp.detectchannel;
end
% remove datacursor to prevent crash
vtset.datacursor.Mgr.removeAllDataCursors();


%%%%% tell TrackingGUI that something changed
vtset.unsaved(vtset.treenum)=1;
% vtset.updateCallback();
for qchan = 1:numel(vtset.temp.quantDetectChannel)
    vtset.plotCell(vtset.selected.cell,vtset.temp.quantDetectChannel(qchan),vtset.haxes(vtset.temp.quantDetectChannel(qchan)))
end

% update datacursor
vtset.datacursor.hDatatip = createDatatip(vtset.datacursor.Mgr, vtset.hcurves(vtset.hcurvescells == vtset.temp.cellNr & vtset.hcurveschannel == vtset.dcselected.channel));
pos =  [vtset.temp.abstime/vtset.timemodifier vtset.temp.int(vtset.temp.quantDetectChannel == vtset.dcselected.channel)];
set(get(vtset.datacursor.Mgr.CurrentDataCursor,'DataCursor'),'TargetPoint',pos,'DataIndex',pos)
set(vtset.datacursor.Mgr.CurrentDataCursor,'position',pos)
updateDataCursors(vtset.datacursor.Mgr)

% get focus back
drawnow
set(0,'CurrentFigure', handles.TrackACell) 
figure(handles.TrackACell);


function changeTimepointFlag(field,flag)
global vtset
% update fields
vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).(field)(vtset.temp.cellidDetect) = flag;
for qchan = 1:numel(vtset.temp.quantDetectChannel)
    vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).(field)(vtset.temp.cellidQuant(qchan)) = flag;
end

%%%%% tell TrackingGUI that something changed
vtset.unsaved(vtset.treenum)=1;
% vtset.updateCallback();
for qchan = 1:numel(vtset.temp.quantDetectChannel)
    vtset.plotCell(vtset.selected.cell,vtset.temp.quantDetectChannel(qchan),vtset.haxes(vtset.temp.quantDetectChannel(qchan)))
end

initializeParams();

% get mouse position
function rearrangeCenter(src,evnt)
global vtset
handles = guidata(src);


% check if in freehand or ellipse mode
% if isfield(vtset.temp.trackset,'mode') && strcmp(vtset.temp.trackset.mode,'Ellipse')
%     return;
% end

% check if in range of an image
cp = get(handles.axesDetectOrig,'CurrentPoint');
cp=cp(1,1:2);


win=vtset.temp.trackset.win;

if sum(cp<win & cp>0)==2
    % update x and y
    vtset.temp.X=vtset.temp.X+cp(1)-win/2;
    vtset.temp.Y=vtset.temp.Y+cp(2)-win/2;
    update(handles);
end


function wbmf(src,evnt)
% check if in range of an image
global vtset
handles = guidata(src);

cp = get(handles.axesDetectOrig,'CurrentPoint');
cp=cp(1,1:2);


win=vtset.temp.trackset.win;

if sum(cp<win & cp>0)==2
    set(vtset.trackACellFocus , 'Pointer', 'crosshair');
    vtset.cellinspector.crosshairmodus = 1;
elseif vtset.cellinspector.crosshairmodus == 1
    set(vtset.trackACellFocus , 'Pointer', 'arrow');
    vtset.cellinspector.crosshairmodus = 0;
end

function deleteTimepoint(force,handles)
global vtset

if nargin<2
    handles = guidata(vtset.trackACellFocus); 
end

try
    curtree=vtset.treenum;
    cell=vtset.selected.cell;
    tp=vtset.selected.timepoint;


    %%% TODO extend to delete whole ranges
    if force 
        button = 'Y';
    else
        button = questdlg(sprintf('Do you want to delete Cell %d Timepoint %d',cell,tp),'Delete Tree');
    end
    if numel(button)>0
        if button(1) == 'Y'
            
            %%% trackpoint will be  deleted in actual quant channel
            % detect will be deleted also
            % non fluor will remain the same
            for chan=1:numel(vtset.temp.quantDetectChannel)
                deleteid = vtset.temp.cellidQuant(chan);
                %%%% change, just make it not active
                %%%% TODO : set size to 0 and int to -1 ??
                vtset.results{curtree}.quants(vtset.temp.quantDetectChannel(chan)).active(deleteid) =0;
%                 for field=fields(vtset.results{curtree}.quants)'
%                     if ~strcmp(field,'settings')
%                         vtset.results{curtree}.quants(vtset.temp.quantDetectChannel(chan)).(cell2mat(field))(deleteid)=[];
%                     end
%                 end
            end
            
            %%%% change, just make it not active
            %%%% TODO : set size to 0 and int to -1 ??
            vtset.results{curtree}.detects(vtset.temp.detectchannel).active(vtset.temp.cellidDetect) =0;
%             for field=fields(vtset.results{curtree}.detects(vtset.temp.detectchannel))'
%                 if ~strcmp(field,'settings') 
%                     vtset.results{curtree}.detects(vtset.temp.detectchannel).(cell2mat(field))(vtset.temp.cellidDetect)=[];
%                 end
%             end
        end
    end

    vtset.unsaved(vtset.treenum)=1;
    
catch e
    msgbox(sprintf('Could not delete cell %d, timepoint %d', cell, tp), 'Error','error');
    rethrow(e)
end
vtset.selected.timepoint=getNearestTimepoint(vtset.selected.timepoint+1,0,handles);

updateTimeSliderCallback(tp,-1)

% remove datacursor
vtset.datacursor.Mgr.removeAllDataCursors();

% redraw cells
for qchan = 1:numel(vtset.temp.quantDetectChannel)
    vtset.plotCell(vtset.selected.cell,vtset.temp.quantDetectChannel(qchan),vtset.haxes(vtset.temp.quantDetectChannel(qchan)))
end

initializeParams(handles);

% --- Executes on button press in ButtonDelete.
function ButtonDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

deleteTimepoint(0,handles);
uicontrol(handles.ButtonNext);


function detectOutliers(handles)
global vtset

thresh = str2double(get(handles.outlierThresh,'String'));
data= tocolumn(vtset.results{vtset.treenum}.quants(vtset.selected.channel).int(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1));
timepoints= tocolumn(vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr==vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).active==1));

% outlier  = deviate thresh * std from smoothed data
thresh=thresh*std(smooth(data)-data);
vtset.outliers= timepoints(smooth(data)-data>thresh);


    
        



function outlierThresh_Callback(hObject, eventdata, handles)
% hObject    handle to outlierThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uicontrol(handles.ButtonNext);
% Hints: get(hObject,'String') returns contents of outlierThresh as text
%        str2double(get(hObject,'String')) returns contents of outlierThresh as a double


% --- Executes during object creation, after setting all properties.
function outlierThresh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to outlierThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in outliermodus.
function outliermodus_Callback(hObject, eventdata, handles)
% hObject    handle to outliermodus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of outliermodus
if  get(hObject,'Value')
    detectOutliers(handles)
end
uicontrol(handles.ButtonNext);


% --- Executes on selection change in popupMode.
function popupMode_Callback(hObject, eventdata, handles)
% hObject    handle to popupMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% check for meanigful params
global vtset
vtset.temp.trackset.threshmethod=get(handles.popupMode,'String');
vtset.temp.trackset.threshmethod=vtset.temp.trackset.threshmethod{get(handles.popupMode,'Value')};

mode =vtset.temp.trackset.threshmethod;
if strcmp(mode,'MSER')
    if ~isfield(vtset.temp.trackset,'MaxArea') || ~numel(vtset.temp.trackset.MaxArea)
        vtset.temp.trackset.MaxArea = 300;
    end
    if ~isfield(vtset.temp.trackset,'MinArea')|| ~numel(vtset.temp.trackset.MinArea)
        vtset.temp.trackset.MinArea = 50;
    end
    if ~isfield(vtset.temp.trackset,'Delta')|| ~numel(vtset.temp.trackset.Delta)
        vtset.temp.trackset.Delta = 2;
    end
end

updateGUIslider(handles)
uicontrol(handles.ButtonNext);
% saveTrackset(handles);
update(handles);
% Hints: contents = cellstr(get(hObject,'String')) returns popupMode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupMode

function updateGUIslider(handles)
global vtset
vtset.temp.trackset.threshmethod=get(handles.popupMode,'String');
vtset.temp.trackset.threshmethod=vtset.temp.trackset.threshmethod{get(handles.popupMode,'Value')};

mode =vtset.temp.trackset.threshmethod;

if ~strcmp(mode,'Otsu global') && ~strcmp(mode,'Otsu local') && ~strcmp(mode,'MSER')
    % change the whole thing to select radius
    for f = fields(handles)'
        if numel(strfind(cell2mat(f),'freehand'))
            set(handles.(cell2mat(f)),'Visible','on')
%             set(handles.(cell2mat(f)),'Enable','off')            
        elseif numel(strfind(cell2mat(f),'_otsu'))
            set(handles.(cell2mat(f)),'Visible','off')
%             set(handles.(cell2mat(f)),'Enable','off')
        end
    end
    
    % use nearest??
else
    % change everyhting back
    for f = fields(handles)'
        if numel(strfind(cell2mat(f),'freehand'))
            set(handles.(cell2mat(f)),'Visible','off')
%             set(handles.(cell2mat(f)),'Enable','off')
        elseif numel(strfind(cell2mat(f),'_otsu'))
            set(handles.(cell2mat(f)),'Visible','on')
%             set(handles.(cell2mat(f)),'Enalbe','on')
        end
    end
end

% change for MSER
if strcmp(mode,'MSER')
    set(handles.text1_otsu,'String','Delta')
    set(handles.scrThreshCorr_otsu,'Max',20)
    set(handles.scrThreshCorr_otsu,'Min',0)
    set(handles.scrThreshCorr_otsu,'SliderStep',[1 1]/20)
    if isfield(vtset.temp.trackset,'Delta') && numel(vtset.temp.trackset.Delta)
        % change default values
        if vtset.temp.trackset.Delta == 1 && vtset.temp.trackset.MinArea == 4 && vtset.temp.trackset.MaxArea == 4
            vtset.temp.trackset.Delta = 2;
            vtset.temp.trackset.MinArea = 50;
            vtset.temp.trackset.MaxArea = 250;
        end
        set(handles.scrThreshCorr_otsu,'Value',vtset.temp.trackset.Delta)
        set(handles.txtThreshCorr_otsu,'String',vtset.temp.trackset.Delta)
    else
        set(handles.scrThreshCorr_otsu,'Value',round(str2double(get(handles.txtThreshCorr_otsu,'String'))))
        set(handles.txtThreshCorr_otsu,'String',round(str2double(get(handles.txtThreshCorr_otsu,'String'))))
    end
    set(handles.text2_otsu,'String','Minimal Area')
    set(handles.scrSmooth_otsu,'Max',str2double(get(handles.txtWindow,'String'))^2)
    set(handles.scrSmooth_otsu,'Min',0)
    set(handles.scrSmooth_otsu,'SliderStep',[10 50]/str2double(get(handles.txtWindow,'String'))^2)
    if isfield(vtset.temp.trackset,'MinArea')&& numel(vtset.temp.trackset.MinArea)
        set(handles.scrSmooth_otsu,'Value',vtset.temp.trackset.MinArea)
        set(handles.txtSmooth_otsu,'String',vtset.temp.trackset.MinArea)
    else
        set(handles.scrSmooth_otsu,'Value',round(str2double(get(handles.txtSmooth_otsu,'String'))))
        set(handles.txtSmooth_otsu,'String',round(str2double(get(handles.txtSmooth_otsu,'String'))))
    end
    set(handles.text3_otsu,'String','Maximal Area')
    set(handles.scrMax_otsu,'Max',str2double(get(handles.txtWindow,'String'))^2)
    set(handles.scrMax_otsu,'SliderStep',[10 50]/str2double(get(handles.txtWindow,'String'))^2)
    set(handles.scrMax_otsu,'Min',0)
    if isfield(vtset.temp.trackset,'MaxArea')&& numel(vtset.temp.trackset.MaxArea)
        set(handles.scrMax_otsu,'Value',vtset.temp.trackset.MaxArea)
        set(handles.txtMax_otsu,'String',vtset.temp.trackset.MaxArea)
    else
        set(handles.scrMax_otsu,'Value',round(str2double(get(handles.txtMax_otsu,'String'))))
        set(handles.txtMax_otsu,'String',round(str2double(get(handles.txtMax_otsu,'String'))))
    end
else
    set(handles.text1_otsu,'String','Threshold')
    set(handles.scrThreshCorr_otsu,'Max',2)
    set(handles.scrThreshCorr_otsu,'Min',0)
    set(handles.scrThreshCorr_otsu,'SliderStep',[0.02 0.2]/2)
    set(handles.scrThreshCorr_otsu,'Value',vtset.temp.trackset.threshcorr)
    set(handles.txtThreshCorr_otsu,'String',vtset.temp.trackset.threshcorr)
    set(handles.text2_otsu,'String','Smoothing Filter')
    set(handles.scrSmooth_otsu,'Max',8)
    set(handles.scrSmooth_otsu,'Min',0)
    set(handles.scrSmooth_otsu,'SliderStep',[1 2]/8)
    set(handles.scrSmooth_otsu,'Value',vtset.temp.trackset.smooth)
    set(handles.txtSmooth_otsu,'String',vtset.temp.trackset.smooth)
    set(handles.text3_otsu,'String','Maxima suppression')
    set(handles.scrMax_otsu,'Max',20)
    set(handles.scrMax_otsu,'SliderStep',[1 2]/20)
    set(handles.scrMax_otsu,'Min',0)
    set(handles.scrMax_otsu,'Value',vtset.temp.trackset.max)
    set(handles.txtMax_otsu,'String',vtset.temp.trackset.max)
end
    



% --- Executes during object creation, after setting all properties.
function popupMode_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonUpdateCircle_freehand.
function ButtonUpdateCircle_freehand_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonUpdateCircle_freehand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset freehand
    


trackset =vtset.temp.trackset;
if strcmp(vtset.temp.trackset.threshmethod,'Ellipse')
    freehand.delete;
    trackset = rmfield(trackset,'ellipse');
    trackset.drawnew=1;
    set(gcf,'WindowButtonUpFcn','');
else
    trackset = rmfield(trackset,'freehand');
end
vtset.temp.trackset = trackset;
    
handles=guidata(vtset.trackACellFocus);
set(get(handles.axesDetectSegmented,'title'),'String','Draw here')
% vtset.temp.trackset.ellipse = freehand.getPosition;
update(handles)

function updateCircle(x,e)
global vtset freehand

if ~strcmp(vtset.temp.trackset.threshmethod,'Ellipse')
    return
end

handles=guidata(gcbo);

cp = get(handles.axesDetectSegmented,'CurrentPoint');
cp=cp(1,1:2);


win=vtset.temp.trackset.win;

if sum(cp<win & cp>0)==2
    % update x and y
%     vtset.temp.trackset.x=vtset.temp.trackset.x+cp(1)-win/2;
%     vtset.temp.trackset.y=vtset.temp.trackset.y+cp(2)-win/2;
    vtset.temp.trackset.ellipse = freehand.getPosition;
    update(handles);
end


function updateFreehand(x,e)
global vtset freehand
handles=guidata(gcbo);

cp = get(handles.axesDetectSegmented,'CurrentPoint');
cp=cp(1,1:2);


win=vtset.temp.trackset.win;

if sum(cp<win & cp>0)==2
    % update x and y
%     vtset.temp.trackset.x=vtset.temp.trackset.x+cp(1)-win/2;
%     vtset.temp.trackset.y=vtset.temp.trackset.y+cp(2)-win/2;
    vtset.temp.trackset.freehand= freehand.createMask;
    update(handles);
end

        


% --- Executes on slider movement.
function SliderEllipse_freehand_Callback(hObject, eventdata, handles)
% hObject    handle to SliderEllipse_freehand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

%%%%% WTF?
vtset.temp.trackset.ellipse-get(hObject,'Value')

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function SliderEllipse_freehand_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SliderEllipse_freehand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in CheckboxShowOutline1.
function CheckboxShowOutline1_Callback(hObject, eventdata, handles)
% hObject    handle to CheckboxShowOutline1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redraw(handles)

% Hint: get(hObject,'Value') returns toggle state of CheckboxShowOutline1


% --- Executes on button press in CheckboxShowOutline2.
function CheckboxShowOutline2_Callback(hObject, eventdata, handles)
% hObject    handle to CheckboxShowOutline2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckboxShowOutline2


% --- Executes on button press in CheckboxShowOutline3.
function CheckboxShowOutline3_Callback(hObject, eventdata, handles)
% hObject    handle to CheckboxShowOutline3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckboxShowOutline3


% --- Executes on button press in CheckboxShowImage1.
function CheckboxShowImage1_Callback(hObject, eventdata, handles)
% hObject    handle to CheckboxShowImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redraw(handles)
% Hint: get(hObject,'Value') returns toggle state of CheckboxShowImage1


% --- Executes on button press in CheckboxShowImage2.
function CheckboxShowImage2_Callback(hObject, eventdata, handles)
% hObject    handle to CheckboxShowImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redraw(handles)
% Hint: get(hObject,'Value') returns toggle state of CheckboxShowImage2


% --- Executes on button press in CheckboxShowImage3.
function CheckboxShowImage3_Callback(hObject, eventdata, handles)
% hObject    handle to CheckboxShowImage3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of CheckboxShowImage3
dropDownSegmentationChannel


% --- Executes on button press in ButtonInspected.
function ButtonInspected_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonInspected (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).inspected(vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).cellNr ==  vtset.selected.cell & vtset.results{vtset.treenum}.detects(vtset.temp.detectchannel).active ==  1) = 1;

for qchan = 1:numel(vtset.temp.quantDetectChannel)
    vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).inspected(vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).cellNr == vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.temp.quantDetectChannel(qchan)).active == 1) = 1;
end
vtset.unsaved(vtset.treenum) = 1;
initializeTimeSlider(handles);
redraw(handles);


% --------------------------------------------------------------------
function Image1_Callback(hObject, eventdata, handles)
% hObject    handle to Image1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function showImage1_Callback(hObject, eventdata, handles)
% hObject    handle to showImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
end
redraw(handles);







% --------------------------------------------------------------------
function showOutline1_Callback(hObject, eventdata, handles)
% hObject    handle to showOutline1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
end
redraw(handles);


% --- Executes during object creation, after setting all properties.
function CheckboxShowImage1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CheckboxShowImage1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function axesQuant1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axesQuant1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axesQuant1


% --------------------------------------------------------------------
function Image2_Callback(hObject, eventdata, handles)
% hObject    handle to Image2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Image3_Callback(hObject, eventdata, handles)
% hObject    handle to Image3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function showImage3_Callback(hObject, eventdata, handles)
% hObject    handle to showImage3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
end
redraw(handles);

% --------------------------------------------------------------------
function showOutline3_Callback(hObject, eventdata, handles)
% hObject    handle to showOutline3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
end
redraw(handles);

% --------------------------------------------------------------------
function showImage2_Callback(hObject, eventdata, handles)
% hObject    handle to showImage2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
end
redraw(handles);

% --------------------------------------------------------------------
function showOutline2_Callback(hObject, eventdata, handles)
% hObject    handle to showOutline2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
end
redraw(handles);


% --------------------------------------------------------------------
function CellNumbers_Callback(hObject, eventdata, handles)
% hObject    handle to CellNumbers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --------------------------------------------------------------------
function showCellNumbers_Callback(hObject, eventdata, handles)
% hObject    handle to showCellNumbers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global menu_handles;
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
%     set(handles.showSingleCell, 'Checked', 'off');
end
redraw(handles);


% --------------------------------------------------------------------
function showSingleCell_Callback(hObject, eventdata, handles)
% hObject    handle to showSingleCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global menu_handles;
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
%     set(handles.showCellNumbers, 'Checked', 'off');
end
redraw(handles);


% --- Executes during object creation, after setting all properties.
function showCellNumbers_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showCellNumbers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% global menu_handles;
% menu_handles.showAllHandle = hObject;


% --- Executes during object creation, after setting all properties.
function showSingleCell_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showSingleCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% global menu_handles;
% menu_handles.showSingleHandle = hObject;


% --------------------------------------------------------------------
function showOutlineNonFluor_Callback(hObject, eventdata, handles)
% hObject    handle to showOutlineNonFluor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
o = get(hObject);
if strcmp(o.Checked, 'on')  
    set(hObject, 'Checked', 'off');
else    
    set(hObject, 'Checked', 'on');
%     set(handles.showCellNumbers, 'Checked', 'off');
end
redraw(handles);



function txtTime_Callback(hObject, eventdata, handles)
% hObject    handle to txtTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtTime as text
%        str2double(get(hObject,'String')) returns contents of txtTime as a double


% --- Executes during object creation, after setting all properties.
function txtTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in dropDownQuantification1.
function dropDownQuantification1_Callback(hObject, eventdata, handles)
% hObject    handle to dropDownQuantification1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redraw(handles);
% Hints: contents = cellstr(get(hObject,'String')) returns dropDownQuantification1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dropDownQuantification1


% --- Executes during object creation, after setting all properties.
function dropDownQuantification1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dropDownQuantification1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in dropDownQuantification2.
function dropDownQuantification2_Callback(hObject, eventdata, handles)
% hObject    handle to dropDownQuantification2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redraw(handles);

% Hints: contents = cellstr(get(hObject,'String')) returns dropDownQuantification2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dropDownQuantification2


% --- Executes during object creation, after setting all properties.
function dropDownQuantification2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dropDownQuantification2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in dropDownQuantification3.
function dropDownQuantification3_Callback(hObject, eventdata, handles)
% hObject    handle to dropDownQuantification3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
redraw(handles);

% Hints: contents = cellstr(get(hObject,'String')) returns dropDownQuantification3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from dropDownQuantification3


% --- Executes during object creation, after setting all properties.
function dropDownQuantification3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dropDownQuantification3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonRecalcCell.
function ButtonRecalcCell_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonRecalcCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
curtree = vtset.results{vtset.treenum}.quants(vtset.selected.channel);
tps= curtree.timepoint(curtree.cellNr==vtset.selected.cell);


res=questdlg(sprintf('Recalculate all timepoints of cell %d with the actual segmentation settings? This will reset your inspected flags.',vtset.selected.cell), 'Recalc', 'Yes', 'No', 'Yes');

% recalc with new trackset of selected cell
if strcmp(res,'Yes')
    vtset.datacursor.Mgr.removeAllDataCursors();

    hwait=waitbar(0,sprintf('recalculating %d timepoints:',numel(tps)));
    
    % get actual detection channel
    channelIndex = vtset.results{vtset.treenum}.quants(vtset.selected.channel).detectchannel(vtset.results{vtset.treenum}.quants(vtset.selected.channel).cellNr == vtset.selected.cell & vtset.results{vtset.treenum}.quants(vtset.selected.channel).timepoint == vtset.selected.timepoint);
    % new detection
    % set actual trackset temporary
    vtset.results{vtset.treenum}.detects(channelIndex).settings.trackset = vtset.temp.trackset;
    detectCells(channelIndex,vtset.treenum,vtset.selected.cell,tps)
    
    % quantify relevant quantification
    waitbar(0,hwait,'Quantifying ...')
    for q = 1:numel(vtset.results{vtset.treenum}.quants)
        if ismember(channelIndex,vtset.results{vtset.treenum}.quants(q).detectchannel(vtset.results{vtset.treenum}.quants(q).cellNr == vtset.selected.cell))
            quantifyCells(q,vtset.treenum,vtset.selected.cell,tps)
        end
    end
    
    %             waitbar(i/numel(tps));
    %         end
    % end
    close(hwait)
    vtset.unsaved(vtset.treenum)=1;
    vtset.updateCallback();
    initializeParams(handles);
end
