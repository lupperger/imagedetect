function intensityPosPlot(img, vert)

if nargin<2
    vert=false;
end

if ~vert
    plot(1:size(img,2),img(:,:),'o');
else
    plot(1:size(img,1),img(:,:),'o')
end