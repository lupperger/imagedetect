function [bw3,props, bw] = mserSegmentation(I,options)
%[bw,props] = morhphoQuant(I,minSize,maxSize,delta)
% Input:
%   I:       grayscale input image, should be already background corrected!
%   options:
%   options.minSize: minimum size of objects to be segmented by mser
%   options.maxSize: maximum size of objects to be segmented by mser
%   options.delta:   threshold for region stability. Small: less stability, High:
%            high stability
%
% Output:
%   bw:    black/white image. white pixels represent regions, black pixels are
%          background
%   props: structure with measured information for each region

%% Mser Segmentation and postprocessing
   tic;
%    I_w = medfilt2(I,[3 3]);
   I_w = wiener2(I,[3 3]);
%    I_norm = imadjust(I_w);
robustmin = medianfirst(sort(I_w(:)),100);
robustmax = topmedian(I_w(:),100);
   I_norm = (I_w-robustmin)./(robustmax-robustmin);
%     I_norm = adapthisteq(I_w);
%     I_norm = wiener2(I_norm,[3 3]);
   I_uint8 = im2uint8(I_norm);
%    if options.invert
%        I_uint8 = imcomplement(I_uint8);
%    end
   
   minSize = options.minSize;
   maxSize = options.maxSize;
   Delta= options.delta;
   maxVariation = options.maxVariation;
   
   msers = linearMser(I_uint8,Delta,minSize,maxSize,maxVariation,false);
   bw = zeros(size(I));
   for i=1:numel(msers)
     bw(msers{i}) = bw(msers{i})+1;
   end
   
   bw2 = imfill(bw,'holes');
   bw3 = logical(imclearborder(bw2));
   
   
%    bw3(1:470,200:end)=false;
   
%    props = regionprops(I_uint8,bw3,'all');
   toc;
   
   stats=regionprops( bwconncomp(bw3,4), 'Centroid');
   coords = [stats(:).Centroid];
   
   props = [coords(1:2:end);coords(2:2:end)]';
    
end



   
    