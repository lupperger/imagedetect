% needs image, records all mouse clicks
% returns coordinates [x,y] if the any key is pressed

function track_real=getcellcoordinates_anke(img)

cp=[ 0 0 0 ];
track=[];
track_real=[];
speed=0.01;

pressed=0;
set(gcf,'windowbuttonmotionfcn',@wbmcb);
set(gcf,'WindowButtonUpFcn',@wbucb);
set(gcf,'WindowButtonDownFcn',@wbdcb);
set(gcf,'KeyPressFcn',@kp);



% show first picture
ret=0;
index=1;
%imagesc(img,[0.1 0.2]);
%axis equal
axis on
%axis off
% colormap(gray)
fprintf('clicks:');

while true   

    % wait if not pressed
    while ~pressed
        % close if any other key is pressed
        if ret
            fprintf('\n');
        %    close(f)
            return;
        end
        pause(speed);
    end

    pos = get(gca,'Position');
    if (cpf(1,2) > pos(1,2) && cpf(1,1) < pos(1,3))
    %%% jippie innerhalb
        % store coordinates
        track(index,:)=cpf(1,1:2);
        track_real(index,:)=cpa(1,1:2);
        x = cpa(1,1);
        y = cpa(1,2);
        hold on
        plot(x,y,'rx');
        fprintf('.');
    end
%     fprintf('.');
    

    index=index+1;
    while pressed
        pause(speed);
    end
  
end


    % get mouse position
    function wbmcb(src,evnt)
        cpf = get(gcf,'CurrentPoint');
        cpa = get(gca,'CurrentPoint');
    end

    % mouse button down
    function wbdcb(src,evnt)
        pressed=1;
    end

    % mouse button up
    function wbucb(src,evnt)
        pressed=0;
    end
    % any keyboard key
    function kp(src,evnt)
        ret=1;
    end


end