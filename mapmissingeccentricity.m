function results=mapmissingeccentricity(results)
% global vtset


wh = waitbar(0,'Calculating missing Eccentricity');


for q=1:numel(results.detects)
    results.detects(q).eccentricity=[];
    for c = 1:numel(results.detects(q).cellNr)
        waitbar(c/numel(results.detects(q).cellNr),wh)
        ecc = regionprops(results.detects(q).cellmask{c},'eccentricity');
        results.detects(q).eccentricity(c) = ecc.Eccentricity;
    end
end

close(wh)