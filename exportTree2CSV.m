function exportTree2CSV(treenum,FileName,PathName,force)
global vtset

try

%%%% NEW:
% create a subfile for each quantification wavelength
% header should always be the same:
% cellNr,timepoint,intensity,area,perimeter,intbyarea,concentration,producationrate,x,y,pos,absoluteTime,stopReason,active,inspected


exportstruct = cell(1,numel(vtset.results{treenum}.quants));

for q = 1:numel(vtset.results{treenum}.quants)
    %init
    exportstruct{q}.cellNr = vtset.results{treenum}.quants(q).cellNr;
    exportstruct{q}.timepoint = vtset.results{treenum}.quants(q).timepoint;
    exportstruct{q}.intensity =vtset.results{treenum}.quants(q).int;
    exportstruct{q}.area = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.perimeter = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.intbyarea = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.concentration = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.productionrate = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.x = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.y = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.pos = vtset.results{treenum}.quants(q).positionIndex;
    exportstruct{q}.absoluteTime = vtset.results{treenum}.quants(q).absoluteTime;
    exportstruct{q}.stopReason = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.active = vtset.results{treenum}.quants(q).active;
    exportstruct{q}.inspected = vtset.results{treenum}.quants(q).inspected;
    exportstruct{q}.doublinghypothesis = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.linregression = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.cumulative = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    exportstruct{q}.cumulativeNN = zeros(1,numel(vtset.results{treenum}.quants(q).cellNr))-1;
    
    % iterate over index and fill out x,y,area, stopreason
    delidd=[];
    for i = 1:numel(vtset.results{treenum}.quants(q).cellNr)
        tp = vtset.results{treenum}.quants(q).timepoint(i);
        c = vtset.results{treenum}.quants(q).cellNr(i);
        dchan=vtset.results{treenum}.quants(q).detectchannel(i);
        idd = vtset.results{treenum}.detects(dchan).timepoint == tp & vtset.results{treenum}.detects(dchan).cellNr ==c;
        idn = vtset.results{treenum}.nonFluor.timepoint == tp & vtset.results{treenum}.nonFluor.cellNr ==c;
        if sum(idd)==0
            delidd(end+1)=i;
            continue
        end
        exportstruct{q}.x(i) = vtset.results{treenum}.detects(dchan).X(idd);
        exportstruct{q}.y(i) = vtset.results{treenum}.detects(dchan).Y(idd);
        exportstruct{q}.area(i) = vtset.results{treenum}.detects(dchan).size(idd);
        exportstruct{q}.perimeter(i) = vtset.results{treenum}.detects(dchan).perimeter(idd);
        exportstruct{q}.stopReason(i) = vtset.results{treenum}.nonFluor.stopReason(idn);
    end
    
    if numel(delidd)
        % delete elements which do not have any detects struct
        for f = fieldnames(exportstruct{q})'
            exportstruct{q}.(cell2mat(f))(delidd)=[];
        end
    end
    
    if ~isempty(exportstruct{q}.area)
        % calc remaining intbyarea,
        exportstruct{q}.intbyarea = exportstruct{q}.intensity./exportstruct{q}.area;
        % and prod, conc, cumsum, doublethes, linreg
        for c = unique(exportstruct{q}.cellNr)
            id = exportstruct{q}.cellNr ==c;
            int = exportstruct{q}.intensity(id);
            alltps = exportstruct{q}.timepoint(id);
            
            prod = diff(int);
            prod(end+1) = 0;
            loine=(linspace(1,2^(2/3),numel(int)));
            conc= (torow(int)./((loine.^(3/2))));
            
            exportstruct{q}.productionrate(id) = prod;
            exportstruct{q}.concentration(id) = conc;
            
            % doublethes
            line = linspace(medianfirst(int,3),medianfirst(int,3)*2,numel(int));
            alldata = int-line;
            exportstruct{q}.doublinghypothesis(id) = alldata;
            
            % linreg
            p = polyfit(alltps, int, 1);
            alldata = polyval(p, alltps);
            exportstruct{q}.linregression(id) = alldata;
            
            % cumsum
            sumofmothers=0;
            sumofmothersNN=0;
            trace = rootpath(c);
            if numel(trace)>1
                for t = trace(1:end-1)
                    if ~ismember(t,exportstruct{q}.cellNr)
                        continue
                    end
                    alldata2= exportstruct{q}.intensity(exportstruct{q}.cellNr==t & exportstruct{q}.active==1);
                    if isempty(alldata2)
                        alldata2=0;
                    end
                    lastmother= exportstruct{q}.intensity(exportstruct{q}.cellNr==floor(t/2) & exportstruct{q}.active==1);
                    if t == 1 || isempty(lastmother)
                        lastmother=0;
                    end
                    dadiff2= diff([lastmother(end)/2 alldata2]);
                    dafiff2NN = dadiff2;
                    dafiff2NN(dafiff2NN<0)=0;
                    dafiff2NN(1) = diff([lastmother(end)/2 alldata2(1)]);
                    
                    sumofmothers = sumofmothers+sum(dadiff2);
                    sumofmothersNN = sumofmothersNN+sum(dafiff2NN);
                end
            end
            lastmother=exportstruct{q}.intensity(exportstruct{q}.cellNr==floor(c/2) & exportstruct{q}.active==1);
            if c == 1
                lastmother=0;
            end
            if isempty(lastmother)
                % happens if cells are not active/ tracked)
                alldata = NaN;
                alldataNN = NaN;
            else
                dadiff=diff([lastmother(end)/2 int]);
                dadiffNN = dadiff;
                dadiffNN(dadiffNN<0)=0;
                dadiffNN(1) = diff([lastmother(end)/2 int(1)]);
                alldata = cumsum(dadiff)+sumofmothers;
                alldataNN = cumsum(dadiffNN)+sumofmothersNN;
            end
            
            exportstruct{q}.cumulative(id) = alldata;
            exportstruct{q}.cumulativeNN(id) = alldataNN;
        end
    end
end
    
%%%%%%%% OLD STUFF %%%%%%%%%%%%%%%%%%%%%%
%{
clear exportstruct
results =vtset.results;
i = vtset.treenum;
% find missing measurements in different channels, then set them to NaN
x=[torow(results{i}.quants(1).timepoint) ; torow(results{i}.quants(1).cellNr)]';
for chan = 2:numel(results{i}.quants)
    x = union(x,[torow(results{i}.quants(chan).timepoint) ; torow(results{i}.quants(chan).cellNr)]','rows');
end
for dchan = 1:numel(results{i}.detects)
    x = union(x,[torow(results{i}.detects(dchan).timepoint) ; torow(results{i}.detects(dchan).cellNr)]','rows');
end

reihenfolge={};
for chan = 1:numel(results{i}.quants)
    % find missing value and add them in quants
    y= [torow(results{i}.quants(chan).timepoint) ; torow(results{i}.quants(chan).cellNr)]';    
    missing = setdiff(x,y,'rows');
    
    for mis = 1: size(missing,1)
        results{i}.quants(chan).timepoint(end+1) =  missing(mis,1);
        results{i}.quants(chan).int(end+1) =  -1;
        results{i}.quants(chan).cellNr(end+1) =  missing(mis,2);
%         results{i}.quants(chan).positionIndex(end+1) =  missing(mis,1);
    end
    y= [torow(results{i}.quants(chan).timepoint) ; torow(results{i}.quants(chan).cellNr)]';
    
    % get the order of this channel for the export
    reihenfolge{chan}= [];
    for n = 1:size(x,1)
        %
        reihenfolge{chan}(end+1) = find(x(n,1) == y(:,1) & x(n,2) == y(:,2));
    end
    
end
% fill a new struct with all sizes, X, Y, perimeter
detectexport.size=[];
detectexport.perimeter=[];
detectexport.X=[];
detectexport.Y=[];
detectexport.absoluteTime=[];
detectexport.positionIndex=[];
detectexport.inspected=zeros(1,size(x,1));

reihenfolgedetect={};
for dchan= 1:numel(results{i}.detects)
    y= [torow(results{i}.detects(dchan).timepoint) ; torow(results{i}.detects(dchan).cellNr)]';
    
    % fill the struct, but leave inspected as they are; otherwise overwrite
    
    
    
%     reihenfolgedetect{dchan}= [];
    for n = find(detectexport.inspected==0)
        if any(x(n,1) == y(:,1) & x(n,2) == y(:,2))
        detectexport.size(n) = results{i}.detects(dchan).size(x(n,1) == y(:,1) & x(n,2) == y(:,2));
        detectexport.perimeter(n) = results{i}.detects(dchan).perimeter(x(n,1) == y(:,1) & x(n,2) == y(:,2));
        detectexport.X(n) = results{i}.detects(dchan).X(x(n,1) == y(:,1) & x(n,2) == y(:,2));
        detectexport.Y(n) = results{i}.detects(dchan).Y(x(n,1) == y(:,1) & x(n,2) == y(:,2));
        detectexport.absoluteTime(n) = results{i}.detects(dchan).absoluteTime(x(n,1) == y(:,1) & x(n,2) == y(:,2));
        detectexport.positionIndex(n) = results{i}.detects(dchan).positionIndex(x(n,1) == y(:,1) & x(n,2) == y(:,2));
        detectexport.inspected(n) = results{i}.detects(dchan).inspected(x(n,1) == y(:,1) & x(n,2) == y(:,2));
        end
        
        %
%         reihenfolgedetect{dchan}(end+1) = find(x(n,1) == y(:,1) & x(n,2) == y(:,2));
    end
end
%cellNr;timepoint;int1;int2;int3;int4;area;intbyarea1;intbyarea2;intbyarea3;intbyarea4;...
%concentration1;concentration2;concentration3;concentration4;...
%productionrate1;productionrate2;productionrate3;productionrate4;...
%x;y;pos;absoluteTime;stopReason;wavelength_1;wavelength_2;wavelength_3;wavelength_4;wavelength_5;wavelength_6;wavelength_7;wavelength_8;wavelength_9


chan = 1;

exportstruct.cellNr=results{i}.quants(chan).cellNr(reihenfolge{1});
exportstruct.timepoint=results{i}.quants(chan).timepoint(reihenfolge{1});
exportstruct.int1=results{i}.quants(chan).int(reihenfolge{1});
if numel(results{i}.quants)>1
    exportstruct.int2=results{i}.quants(2).int(reihenfolge{2});
else
    exportstruct.int2=zeros(numel(results{i}.quants(chan).int),1)-1;
end
if numel(results{i}.quants)>2
    exportstruct.int3=results{i}.quants(3).int(reihenfolge{3});
else
    exportstruct.int3=zeros(numel(results{i}.quants(chan).int),1)-1;
end
if numel(results{i}.quants)>3
    exportstruct.int4=results{i}.quants(4).int(reihenfolge{4});
else
    exportstruct.int4=zeros(1,numel(results{i}.quants(chan).int))-1;
end


exportstruct.area=detectexport.size;
exportstruct.perimeter=detectexport.perimeter;

exportstruct.intbyarea1=torow(exportstruct.int1)./torow(exportstruct.area);
exportstruct.intbyarea1(exportstruct.int1==-1) = -1;
if numel(results{i}.quants)>1
    
    exportstruct.intbyarea2=exportstruct.int2./exportstruct.area;
    exportstruct.intbyarea2(exportstruct.int2==-1) = -1;
else
    exportstruct.intbyarea2=zeros(numel(results{i}.quants(chan).int),1)-1;
end
if numel(results{i}.quants)>2
    exportstruct.intbyarea3=exportstruct.int3./exportstruct.area;
    exportstruct.intbyarea3(exportstruct.int3==-1) = -1;
else
    exportstruct.intbyarea3=zeros(numel(results{i}.quants(chan).int),1)-1;
end
if numel(results{i}.quants)>3
    exportstruct.intbyarea4=exportstruct.int4./exportstruct.area;
    exportstruct.intbyarea4(exportstruct.int4==-1) = -1;
else
    exportstruct.intbyarea4=zeros(numel(results{i}.quants(chan).int),1)-1;
end

for chan = 1:numel(results{i}.quants)
    
    for c = torow(unique(exportstruct.cellNr))
        alldata=exportstruct.(sprintf('int%d',chan))(exportstruct.cellNr == c);
        ids = (exportstruct.cellNr == c);
%         prod =  derivative_cwt(torow(alldata),'gaus1',10,1/1000)/1000;
        prod = diff(alldata);
        prod(end+1) = 0;
        loine=(linspace(1,2^(2/3),numel(alldata)));
        conc= (torow(alldata)./((loine.^(3/2))));
        exportstruct.(sprintf('concentration%d',chan))(ids)=conc;
        exportstruct.(sprintf('producationrate%d',chan))(ids)=prod;
        exportstruct.(sprintf('producationrate%d',chan))(exportstruct.(sprintf('int%d',chan))==-1) = -1;
        exportstruct.(sprintf('concentration%d',chan))(exportstruct.(sprintf('int%d',chan))==-1) = -1;
    end
    
end

if ~isfield(exportstruct,'concentration2')
    exportstruct.concentration2=zeros(numel(results{i}.quants(chan).int),1)-1;
    exportstruct.producationrate2=zeros(numel(results{i}.quants(chan).int),1)-1;
end

if ~isfield(exportstruct,'concentration3')
    exportstruct.concentration3=zeros(numel(results{i}.quants(chan).int),1)-1;
    exportstruct.producationrate3=zeros(numel(results{i}.quants(chan).int),1)-1;
end

if ~isfield(exportstruct,'concentration4')
    exportstruct.concentration4=zeros(numel(results{i}.quants(chan).int),1)-1;
    exportstruct.producationrate4=zeros(numel(results{i}.quants(chan).int),1)-1;
end
chan = 1;
exportstruct.x=detectexport.X;
exportstruct.y=detectexport.Y;
exportstruct.pos=detectexport.positionIndex;
exportstruct.absoluteTime=detectexport.absoluteTime;

if ~isfield(vtset.results{i}.nonFluor,'stopReason') || ~numel(vtset.results{i}.nonFluor.stopReason)
    mapmissingstopreason(i)
end

y= [torow(results{i}.nonFluor(1).timepoint) ; torow(results{i}.nonFluor(1).cellNr)]';
    
reihenfolgenonFluor= [];
for n = 1:size(x,1)
    %
    reihenfolgenonFluor(end+1) = find(x(n,1) == y(:,1) & x(n,2) == y(:,2));
end

exportstruct.stopReason=vtset.results{i}.nonFluor.stopReason(reihenfolgenonFluor);

%     exportstruct.wavelength_1=results{i}.quants(chan).wavelength_1;
%     exportstruct.wavelength_2=results{i}.quants(chan).wavelength_2;
%     exportstruct.wavelength_3=results{i}.quants(chan).wavelength_3;
%     exportstruct.wavelength_4=results{i}.quants(chan).wavelength_4;
%     exportstruct.wavelength_5=results{i}.quants(chan).wavelength_5;
%     exportstruct.wavelength_6=results{i}.quants(chan).wavelength_6;
%     exportstruct.wavelength_7=results{i}.quants(chan).wavelength_7;
%     exportstruct.wavelength_8=results{i}.quants(chan).wavelength_8;
%     exportstruct.wavelength_9=results{i}.quants(chan).wavelength_9;

%}
catch e 
    msgbox(sprintf('Export of tree \n%s\nfailed. Structure is not consistent.',vtset.results{treenum}.settings.treefile),'Export','error')
%     rethrow(e)
    return
end

% init channels names
channels=cell(1,numel(vtset.results{treenum}.quants));
for i = 1:numel(vtset.results{treenum}.quants)
    wl1=vtset.results{treenum}.quants(i).settings.wl;
    wl1=strsplit_qtfy('.',wl1);
    wl1=wl1{1};
    wl2=vtset.results{treenum}.detects(vtset.results{treenum}.quants(i).settings.detectchannel).settings.wl;
    wl2=strsplit_qtfy('.',wl2);
    wl2=wl2{1};
    channels{i} = ['Q' wl1 'D' wl2 vtset.results{treenum}.detects(vtset.results{treenum}.quants(i).settings.detectchannel).settings.trackset.threshmethod];
end

doexcel=1;

for q = 1:numel(exportstruct)
    if isempty(exportstruct{q}.active)
        continue
    end
    try
        FileNameT = strsplit_qtfy('.',FileName);
        FileNameT = [FileNameT{1} '_' strrep(channels{q},' ','') '.' FileNameT{2}];
        fprintf('Exporting file to %s\n',[PathName FileNameT]);
        ezwrite([PathName FileNameT],exportstruct{q},',')
        fprintf('Exporting file to %s\n',[PathName FileNameT(1:end-11) 'xls']);
        newexp = cell(numel(exportstruct{q}.active)+1,numel(fieldnames( exportstruct{q})));
        % header
        ccounter=0;
        for f = fieldnames(exportstruct{q})'
            ccounter=ccounter+1;
            newexp{1,ccounter}=cell2mat(f);
            newexp(2:end,ccounter) = num2cell(exportstruct{q}.(cell2mat(f)));
        end
        if ispc && doexcel
            try
                xlswrite([strrep(strrep(PathName,'/','\'),'\\','\') FileNameT(1:end-11) 'xls'],newexp);
                
            catch e
                if doexcel
                    
                    errordlg('Excel export not possible. Please check if Excel is installed properly.')
                end
                doexcel = 0;
            end
        end
    catch e
        msgbox(sprintf('Export of tree \n%s\ninto file\n%s\nfailed.',vtset.results{vtset.treenum}.settings.treefile,[PathName FileName]),'Export','error')
        rethrow(e)
    end
    
end
if ~force
    msgbox(sprintf('Export of tree \n%s\ninto file(s)\n%s\nsuccessfull',vtset.results{vtset.treenum}.settings.treefile,[PathName FileName]),'Export','none')
end