%% new export for carsten and ask for channel
global vtset
for i=1:numel(vtset.results)
    clear exportstruct
    counter=0;
    channel=0;
    for chan = 1:numel(vtset.results{i}.quants)
        if ~isempty(strfind(vtset.results{i}.quants(chan).settings.wl,'1.')) && ~isempty(strfind(vtset.results{i}.detects(vtset.results{i}.quants(chan).settings.detectchannel).settings.wl,'2.'))
            channel=chan;
            break
        end
    end
      
    if channel == 0
        fprintf('ERROR: Tree %s No channel D w2 Q w1 found\n',vtset.results{i}.settings.treefile)
        continue
    end
    
%     if isfield(vtset.results{i}.settings,'activechannel')
%         a = (vtset.results{i}.settings.activechannel);
%         channel = a;v
%         fprintf('using channel %d for tree %s\n',a,vtset.results{i}.settings.treefile);
%     else
%         a = questdlg(sprintf('Which channel for tree %s',vtset.results{i}.settings.treefile),'channel?','1','2','3','1');
%         if isempty(a)
%             continue
%         end
%         % channel=chan(i);
%         %     a='4';
%         % channel=vtset.results{i}.settings.activechannel;
%         channel = str2double(a);
%         vtset.results{i}.settings.activechannel = a;
%     end
    %         if numel(unique(vtset.results{i}.quants.cellNr))>1
    for j=1:numel(vtset.results{i}.quants(channel).cellNr)
        muh=find(vtset.results{i}.detects(vtset.results{i}.quants(channel).detectchannel(j)).cellNr == vtset.results{i}.quants(channel).cellNr(j) & vtset.results{i}.detects(vtset.results{i}.quants(channel).detectchannel(j)).timepoint == vtset.results{i}.quants(channel).timepoint(j) );
        if ~isempty(muh)
        counter=counter+1;
        %         exportstruct.tree{counter}=vtset.results.treefiles{i};
        exportstruct.cellNr(counter)=vtset.results{i}.quants(channel).cellNr(j);
        exportstruct.timepoint(counter)=vtset.results{i}.quants(channel).timepoint(j);
        exportstruct.active(counter)=vtset.results{i}.quants(channel).active(j);
        exportstruct.inspected(counter)=vtset.results{i}.quants(channel).inspected(j);
        exportstruct.int(counter)=vtset.results{i}.quants(channel).int(j);
        dchan = vtset.results{i}.quants(channel).detectchannel(j);
        exportstruct.size(counter)=vtset.results{i}.detects(dchan).size(vtset.results{i}.detects(dchan).cellNr == exportstruct.cellNr(counter) & vtset.results{i}.detects(dchan).timepoint == exportstruct.timepoint(counter) );
        exportstruct.x(counter)=vtset.results{i}.detects(dchan).X(j);
        exportstruct.y(counter)=vtset.results{i}.detects(dchan).Y(j);
        exportstruct.pos(counter)=vtset.results{i}.quants(channel).positionIndex(j);
        exportstruct.absoluteTime(counter)=vtset.results{i}.quants(channel).absoluteTime(j);
        idfl =find(vtset.results{i}.nonFluor.cellNr == vtset.results{i}.quants(channel).cellNr(j) & vtset.results{i}.nonFluor.timepoint == vtset.results{i}.quants(channel).timepoint(j));
        exportstruct.stopReason(counter)=vtset.results{i}.nonFluor.stopReason(idfl(1));
        %         exportstruct.fcg(counter)=vtset.results.quants{i}.wavelength_3(j);
        %         exportstruct.cd150(counter)=vtset.results.quants{i}.wavelength_2(j);
        
        end
    end
    exportstruct.size(exportstruct.active == 0 ) = 0;
    exportstruct.int(exportstruct.active == 0 ) = -1;
    ezwrite([vtset.results{i}.settings.treefile '.csv'],exportstruct,',')
    %         end
    
end