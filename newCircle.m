function newparas=newCircle(paras)

r = ceil(3.*rand);
newparas = paras;
if r==1
    newparas(1) = newparas(1) + ceil(3.*rand) - 2;
elseif r==2
    newparas(2) = newparas(2) + ceil(3.*rand) - 2;
else
    newparas(3) = newparas(3) + ceil(3.*rand) - 2;
    if newparas(3) == 0
        newparas(3) = 1;
    end
end