function setInspected(cells,channels,insp,timepoints)
global vtset
tree = vtset.treenum;

%%%% TODO erweitern auf timepoints

% set all quants inspected 
dchannels=[];
for channel =channels
    
    allids = ismember(vtset.results{tree}.quants(channel).cellNr,cells) & vtset.results{tree}.quants(channel).active==1;
    %%%% TODO extend: only set those to inspected which detects belong to
    %%%% this actual channel
    vtset.results{tree}.quants(channel).inspected(allids)=insp;
    dchannels(end+1) = vtset.results{tree}.quants(channel).settings.detectchannel;
end

% and detects


for channel =unique(dchannels)
    allids = ismember(vtset.results{tree}.detects(channel).cellNr,cells) & vtset.results{tree}.detects(channel).active == 1;
    vtset.results{tree}.detects(channel).inspected(allids)=insp;
  
end

