function vtset=recalcInt(vtset)
% vtset.results.settings.imgroot = '/nfs/cmbdata/schwarzfischer/100205AF1/';
vtset.error.tp=[];
vtset.error.cell=[];
vtset.error.tree=[];
for t = 1:numel(vtset.results.quants)

    for c = unique(vtset.results.quants{t}.cellNr)'
        fprintf('%d,',c');
        alltps=vtset.results.quants{t}.timepoint(vtset.results.quants{t}.cellNr == c)';
        for tp = alltps
            fprintf('tree %d cell %d tp %d ',t, c, tp);

            cellid = find(vtset.results.quants{t}.cellNr==c & vtset.results.quants{t}.timepoint == tp);
            % load quant img

            imgfileQuant=vtset.results.quants{t}.filename{cellid};
            %         imageQuant=double(imread(['/local/backup/schwarzfischer/data/1003
            %         01PH4' imgfileQuant]))/255;
            try
                imageQuant=double(imread([vtset.results.settings.imgroot imgfileQuant]))/255;
                corrImage = double(imread([vtset.results.settings.imgroot imgfileQuant(1:16) 'background/' imgfileQuant(17:end-3) 'png']))/255/255;
                %%% for tanigushi normalization
%                 fluorImage = double(imread([vtset.results.settings.imgroot imgfileQuant(1:16) '../100301PH4_p040/100301PH4_p040_t' sprintf('%05d',tp)  '_w4.tif']))/255;
%                 fluorImage = fluorImage./mean(fluorImage(:));
%                 imageQuant = ( imageQuant - corrImage ) ./(fluorImage - corrImage);
                imageQuant = normalizeImage(imageQuant, corrImage);

                trackset = vtset.results.quants{t}.trackset{cellid};

                center = vtset.results.quants{t}.cellmask{cellid};

                subimgQuant = extractSubImage(imageQuant, trackset.x, trackset.y, trackset.win);
                fprintf('old %f new %f\n',vtset.results.quants{t}.int(cellid), sum(subimgQuant(center)))
                % store size
                vtset.results.quants{t}.size(cellid) =  sum(center(:));
                % quantify
                vtset.results.quants{t}.int(cellid) = sum(subimgQuant(center));

                % calc mean
                vtset.results.quants{t}.mean(cellid) = vtset.results.quants{t}.int(cellid) ...
                    / vtset.results.quants{t}.size(cellid);
            catch e
% rethrow(e)
                'No background!'
                daend = numel(vtset.error.tp);
                vtset.error.tp(daend+1) = tp;
                vtset.error.cell(daend+1) = c;
                vtset.error.tree(daend+1) = t;
            end
        end
    end
    fprintf('\n');
end


function dummy
%% recalc and compare dfferent background estimation approaches
counter=0;
bg_new=[];
bg_block=[];
for t = 1:numel(vtset.results.quants)

    for c = 1:35%unique(vtset.results.quants{t}.cellNr)'
        fprintf('%d,',c');
        for tp = vtset.results.quants{t}.timepoint(vtset.results.quants{t}.cellNr == c)'
            fprintf('tree %d cell %d tp %d ',t, c, tp);

            cellid = find(vtset.results.quants{t}.cellNr==c & vtset.results.quants{t}.timepoint == tp);
            % load quant img
            img=vtset.results.quants{t}.filename{cellid};
            img=double(imread([vtset.results.settings.imgroot img]))/255;
            trackset = vtset.results.quants{t}.trackset{cellid};

            % use background estimation

            imgwidth=size(img,2);
            imghight=size(img,1);

            [y x z] = backgroundestimation(img,70);
            [XI YI] = meshgrid(1:imgwidth,1:imghight);
            ZI=griddata(x',y',z',XI,YI,'cubic', {'Qt','Qbb','Qc' ,'Qs'});
            for i=find(sum(~isnan(ZI(:,1:imgwidth))')>0)
                ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
            end

            for i=1:imgwidth
                ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
            end
            counter=counter+1;

            bg_new(counter)  = ZI(trackset.y,trackset.x);

            % use block background estimation

            subimgQuantblock=extractSubImage(img, trackset.x, trackset.y, 600);
            bg_block(counter) = median(subimgQuantblock(:));

            fprintf('\n');
        end

    end
end



