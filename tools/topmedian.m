function r=topmedian(imgvector,n)

mysort=sort(imgvector, 'descend');

r=nanmedian(mysort(1:n));
