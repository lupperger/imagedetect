%% map segmentation
basepath = vtset.results{vtset.treenum}.settings.imgroot;

channel = vtset.dcselected.channel;

unicells = unique(vtset.results{vtset.treenum}.detects(channel).cellNr);
for c = unicells
    load(sprintf('%s/treemeasurements/tracksets/%.4i.mat',basepath,vtset.treenum));
    load(sprintf('%s/treemeasurements/tree_%.4i/track_%.4i_cell_%i',basepath,vtset.treenum,vtset.treenum,c));
    
    X_segcell = segCell.dataPoints(:,strcmp(segCell.featNames,'X'));
    Y_segcell = segCell.dataPoints(:,strcmp(segCell.featNames,'Y'));

    filenames_vtset = vtset.results{vtset.treenum}.detects(channel).filename';
    tps_vtset = vtset.results{vtset.treenum}.detects(channel).timepoint(vtset.results{vtset.treenum}.detects(channel).cellNr == c);
    xqtfy= vtset.results{vtset.treenum}.detects(channel).X';
    yqtfy = vtset.results{vtset.treenum}.detects(channel).Y';
    
    tps_segcell = segCell.timePoints;

    
    for t = tps_vtset
        idx = vtset.results{vtset.treenum}.detects(channel).cellNr == c & vtset.results{vtset.treenum}.detects(channel).timepoint == t ;

        segcell_idx = find(tps_segcell == t);
        
        filename_vtset = filenames_vtset{idx};

        x_segcell = X_segcell(segcell_idx);
        y_segcell = Y_segcell(segcell_idx);
        
        if isnan(x_segcell) || isnan(y_segcell)
            x_segcell = celltrack.X(t == celltrack.timepoint);
            y_segcell = celltrack.Y(t == celltrack.timepoint);
            warning('missing cell segmentation!')
        end
        
        segm = loadimage([basepath '/segmentation/' filename_vtset],0);
        props = regionprops(logical(segm),'PixelIdxList','Centroid');
        centroids = round([props.Centroid]);
        centroids = [centroids(1:2:end-1)',centroids(2:2:end)'];
        [~,foundidx] = min(pdist2([x_segcell,y_segcell],centroids));
        bw_single = zeros(size(segm));
        bw_single(props(foundidx).PixelIdxList) = 1;
        bw_single = logical(bw_single);
           
        % get nearest object
        trackset = vtset.results{vtset.treenum}.detects(channel).trackset(idx);
        
        out = extractSubImage(bw_single,x_segcell,y_segcell,trackset.win);
        
        vtset.results{vtset.treenum}.detects(channel).cellmask{idx} = out==1;
        vtset.results{vtset.treenum}.detects(channel).X(idx) = x_segcell;
        vtset.results{vtset.treenum}.detects(channel).Y(idx) = y_segcell;
        
%         figure(2)
%         imagesc(out)
%         drawnow;
%         
%          figure(2);
%          imagesc(segm)
%          hold on
%          plot(x_segcell,y_segcell,'g+')
%          plot(x_celltrack,y_celltrack,'r+')
%          hold off;  
%         drawnow
    end
end

%% re quantify with new cellmasks
qchan=1;
quantifyCells(qchan,vtset.treenum,'all','complete');
qchan=4;
quantifyCells(qchan,vtset.treenum,'all','complete');