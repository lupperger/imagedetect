/**
 * Copyright (C) 2011 Oliver Hilsenbeck
 *
 * Use only with permission
 */

#include "mser.h"
#include "mser_internal.h"


namespace mser {

	MSERDetector::MSERDetector( unsigned int imageSizeX, unsigned int imageSizeY )
	{
		//// Init variables
		//fillRegionsVisited = 0;
		//fillRegionsVisitedSize = 0;

		// Create detector
		m_detector = new MSERDetectorInternal(imageSizeX, imageSizeY);
		if(!m_detector)
			throw MSERException(MSERException::ALLOC_FAILED);
	}

	MSERDetector::~MSERDetector()
	{
		delete m_detector;
		//mser_free(fillRegionsVisited);
	}

	void MSERDetector::setParameters(unsigned int delta, unsigned int minSize, unsigned int maxSize, float maxVariation)
	{
		// Redirect to setters
		setDelta(delta);
		setMaxSize(maxSize);
		setMinSize(minSize);
		setMaxVariation(maxVariation);
	}

	void MSERDetector::setDelta( unsigned int delta )
	{
		// Basic parameter checks
		if(delta < 1)
			throw MSERException(MSERException::INVALID_PARAMETERS);

		m_detector->m_delta = delta;
	}

	void MSERDetector::setMaxSize( unsigned int maxSize )
	{
		m_detector->m_maxSize = maxSize;
	}

	void MSERDetector::setMinSize( unsigned int minSize )
	{
		m_detector->m_minSize = minSize;
	}

	void MSERDetector::setMaxVariation( float maxVariation )
	{
		// Basic parameter checks
		if(maxVariation < 0)
			throw MSERException(MSERException::INVALID_PARAMETERS);

		m_detector->m_maxVariation = maxVariation;
	}

	unsigned int MSERDetector::getDelta() const
	{
		return m_detector->m_delta;
	}

	unsigned int MSERDetector::getMaxSize() const
	{
		return m_detector->m_maxSize;
	}

	unsigned int MSERDetector::getMinSize() const
	{
		return m_detector->m_minSize;
	}

	float MSERDetector::getMaxVariation() const
	{
		return m_detector->m_maxVariation;
	}

	void MSERDetector::processImage(const unsigned char* imageData, unsigned int imageSizeX, unsigned int imageSizeY, bool searchDarkOnBright )
	{
		m_detector->processImage(imageData, imageSizeX, imageSizeY, searchDarkOnBright);
	}

	void MSERDetector::getMserSeeds(seeds_t& seeds) const
	{
		m_detector->getMserSeeds(seeds);
	}

	void MSERDetector::fillRegion( unsigned int seed, std::deque<unsigned int, AlignedAllocator<unsigned int> >& pixels, bool darkOnBright )
	{
		m_detector->fillRegion(seed, pixels, darkOnBright);
	}

}