/**
 * Copyright (C) 2011 Oliver Hilsenbeck
 *
 * Use only with permission
 */

#ifndef mser_defs_h__
#define mser_defs_h__

// STL
//#include <malloc.h>
#include <stdlib.h>

//////////////////////////////////////////////////////////////////////////
// Compiler specific global definitions
//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
// Microsoft Visual C++

// Intrinsics
#include <intrin.h>

// Fundamental data types requiring special care
typedef __int64 mserInt64;
typedef unsigned __int64 mserUInt64;

#else 
//#ifdef __GNUC__
//// GNU GCC
//#warning Note: Aligned memory is not yet supported for GCC.
//
//#include <inttypes.h>
//
//// Fundamental data types requiring special care
//typedef int64_t mserInt64;
//typedef uint64_t mserUInt64;
//
//#else
// Unknown compiler
//#warning Unknown compiler - memory alignment and bitscan optimizations disabled.	<- warning disabled, scares users too much

// Fundamental data types requiring special care
typedef long long mserInt64;
typedef unsigned long long mserUInt64;

//#endif
#endif

namespace mser {

	//////////////////////////////////////////////////////////////////////////
	// Compile time settings
	//////////////////////////////////////////////////////////////////////////

	// If allocated memory should be aligned according to cache lines to avoid false sharing. Increases speed when using 
	// multi-threading but also slightly increases memory consumption.
	const bool MSER_ALIGN_MEMORY = true;

	// Cache line size, does nothing if MSER_ALIGN_MEMORY is false.
	const unsigned int MSER_CACHE_LINE_SIZE = 64;

	//////////////////////////////////////////////////////////////////////////
	// Namespace global definitions
	//////////////////////////////////////////////////////////////////////////

	// Number of gray levels, must be dividable by 64
	static const unsigned int NUM_GRAY_LEVELS = 256;

	//////////////////////////////////////////////////////////////////////////
	// Compiler specific namespace global definitions
	//////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER


	/**
	 * Check if code is compiled for 64 bit architecture
	 * @return true if 64 bit architecture
	 */
	inline bool is64BitArchitecture()
	{
#ifdef _WIN64
		return true;
#else
		return false;
#endif
	}

	/**
	 * Memory allocation function. If MSER_ALIGN_MEMORY is true, memory will be aligned.
	 * @param size number of bytes to allocate.
	 * @param alignment desired memory alignment, has no effect if MSER_ALIGN_MEMORY is false.
	 * @return pointer to allocated memory block
	 */
	inline void* mser_malloc(size_t size, size_t alignment)
	{
		if(mser::MSER_ALIGN_MEMORY)
			return _aligned_malloc(size, alignment);
		else
			return malloc(size);
	}

	/**
	 * Free memory that was allocated using mser_malloc()
	 * @param p pointer to memory block to be freed
	 */
	inline void mser_free(void* p)
	{
		if(mser::MSER_ALIGN_MEMORY)
			_aligned_free(p);
		else
			free(p);
	}

	/**
	 * BitScanForward on a NUM_GRAY_LEVELS bit word: Scan for set bit from least significant bit to most significant bit
	 * @param bitMask pointer to a NUM_GRAY_LEVELS/64-element array of 64 bit words with least significant word at index 0
	 * @return index of first found bit that is set or -1 if no bit is set
	 */
	inline int mser_bitScanReverse256(mserUInt64* bitMask)
	{
		unsigned long tmp;

#ifdef _WIN64
		// x64, use _BitScanForward64
		for(int i = 0; i < NUM_GRAY_LEVELS / 64; ++i) {
			if(_BitScanForward64(&tmp, bitMask[i]))
				return tmp + i * 64;
		}

		// Nothing found
		return -1;
#else
		// x86, use _BitScanForward
		unsigned long* pTmp = (unsigned long*)bitMask;
		for(int i = 0; i < NUM_GRAY_LEVELS / 32; ++i) {
			if(_BitScanForward(&tmp, pTmp[i]))
				return tmp + i * 32;
		}

		// Nothing found
		return -1;
#endif
	}



#else
//#ifdef __GNUC__
//
//	/**
//	 * Check if code is compiled for 64 bit architecture
//	 * @return true if 64 bit architecture
//	 */
//	inline bool is64BitArchitecture()
//	{
//		if(sizeof(int*) == 8)
//			return true;
//		else
//			return false;
//	}
//
//	/**
//	 * Memory allocation function. If MSER_ALIGN_MEMORY is true, memory will be aligned.
//	 * @param size number of bytes to allocate.
//	 * @param alignment (ignored).
//	 * @return pointer to allocated memory block
//	 */
//	inline void* mser_malloc(size_t size, size_t alignment)
//	{
//		return malloc(size);
//	}
//
//	/**
//	 * Free memory that was allocated using mser_malloc()
//	 * @param p pointer to memory block to be freed
//	 */
//	inline void mser_free(void* p)
//	{
//		free(p);
//	}
//
//	/**
//	 * BitScanForward on a NUM_GRAY_LEVELS bit word: Scan for set bit from least significant bit to most significant bit
//	 * @param bitMask pointer to a NUM_GRAY_LEVELS/64-element array of 64 bit words with least significant word at index 0
//	 * @return index of first found bit that is set or -1 if no bit is set
//	 */
//	inline int mser_bitScanReverse256(mserUInt64* bitMask)
//	{
//		unsigned int tmp;
//		// Check if 64 bit system
//		if(is64BitArchitecture()) {
//			// x64
//			for(int i = 0; i < NUM_GRAY_LEVELS / 64; ++i) {
//				tmp = __builtin_ffs(bitMask[i]);
//				if(tmp)
//					return tmp + i * 64;
//			}
//
//			// Nothing found
//			return -1;
//		}
//		else {
//			// x86
//			unsigned long* pTmp = (unsigned long*)bitMask;
//			for(int i = 0; i < NUM_GRAY_LEVELS / 32; ++i) {
//				tmp = __builtin_ffs(pTmp[i]);
//				if(tmp)
//					return tmp + i * 32;
//			}
//
//			// Nothing found
//			return -1;
//		}
//	}
//#else

	/**
	 * Check if code is compiled for 64 bit architecture
	 * @return true if 64 bit architecture
	 */
	inline bool is64BitArchitecture()
	{
		if(sizeof(int*) == 8)
			return true;
		else
			return false;
	}

	/**
	 * Memory allocation function. If MSER_ALIGN_MEMORY is true, memory will be aligned.
	 * @param size number of bytes to allocate.
	 * @param alignment (ignored).
	 * @return pointer to allocated memory block
	 */
	inline void* mser_malloc(size_t size, size_t alignment)
	{
		return malloc(size);
	}

	/**
	 * Free memory that was allocated using mser_malloc()
	 * @param p pointer to memory block to be freed
	 */
	inline void mser_free(void* p)
	{
		free(p);
	}

	/**
	 * BitScanForward on a NUM_GRAY_LEVELS bit word: Scan for set bit from least significant bit to most significant bit
	 * @param bitMask pointer to a NUM_GRAY_LEVELS/64-element array of 64 bit words with least significant word at index 0
	 * @return index of first found bit that is set or -1 if no bit is set
	 */
	inline int mser_bitScanReverse256(mserUInt64* bitMask)
	{
		// Check if 64 bit system
		if(is64BitArchitecture()) {
			mserUInt64 mask = -1;
			for(int i = 0; i < NUM_GRAY_LEVELS / 64; ++i) {
				if(bitMask[i] & mask) {
					for(int j = 0; j < 64; ++j) {
						if(bitMask[i] & ((mserUInt64)1 << j))
							return j + i * 64;
					}
				}
			}
		}
		else {
			// We could also use the previous loop, but in 32bit systems, this is much faster actually.
			unsigned int mask = -1;
			unsigned int *tmp = (unsigned int*)bitMask;
			for(int i = 0; i < NUM_GRAY_LEVELS / 32; ++i) {
				if(tmp[i] & mask) {
					for(int j = 0; j < 32; ++j) {
						if(tmp[i] & ((mserUInt64)1 << j))
							return j + i * 32;
					}
				}
			}
		}

		// Nothing found
		return -1;
	}

//#endif
#endif

	/**
	 * Set specified bit in specified bitmask
	 * @param bitmask pointer to bitmask (not checked if valid)
	 * @param bitNumber number of bit to set starting with least significant bit of bitmask[0], 
	 *		  must be in [0, 8*size of bitmask - 1] (not checked)
	 */
	inline void setBit(unsigned char* bitmask, unsigned int bitNumber)
	{
		bitmask[bitNumber / 8] |= (unsigned char)1 << (bitNumber % 8);
	}

	/**
	 * Check if bit is set
	 * @param bitmask pointer to bitmask (not checked if valid)
	 * @param bitNumber number of bit to test starting with least significant bit of bitmask[0] (not checked if valid)
	 * @return 0 if bit is not set
	 */
	inline int testBit(unsigned char* bitmask, unsigned int bitNumber)
	{
		return bitmask[bitNumber / 8] & (unsigned char)1 << (bitNumber % 8);
	}

}
#endif // mser_defs_h__