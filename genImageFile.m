% fname=genImageFile(imgroot,exp,pos,timepoint,ext)
function fname=genImageFile(imgroot,exp,pos,timepoint,ext)
fname = sprintf('%s/%s_p%03d/%s_p%03d_t%05d_%s', imgroot, exp, pos, exp, pos, timepoint, ext);