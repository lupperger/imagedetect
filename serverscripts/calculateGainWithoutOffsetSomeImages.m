function [gain offset]=calculateGainWithoutOffsetSomeImages(path,wavelength,lower,upper)
wavelength
warning off
%%% init
fprintf('init...')
cd(path)
d = dir(['*' wavelength '.png']);
fprintf('found %d images for command %s in path %s\n',numel(d),['*' wavelength '.png'],path);

%%
means = zeros(numel(d),1);
img = double(imread(d(1).name))/255/255;

riesenmat=zeros(size(img,1),size(img,2),numel(d));
counter = 0;

%% load images
imageCache('init',1);
fprintf('loading images...\n')

xx = 1:numel(d);
% xx = [1:numel(d)];
for x=xx(xx<numel(d))
    fprintf('%d,',x)
    if ~(numel(d(x).name)>18) || str2double(d(x).name(18:22)) < str2double(lower)
        'skip'
        continue
    end
    if str2double(d(x).name(18:22))> str2double(upper)
        'skip'
        continue
    end
    counter = counter+1;
    d(x)
    img = loadimage(d(x).name,0);
    means(counter) = median (img(:));
    riesenmat(:,:,counter) = img;
    if x > 150
        disp('BEWARE of the SWAP')
        break;
    end
end
%clean up
riesenmat(:,:,counter+1:end)=[];
fprintf('...done.\n')
%% fit the pixels
fprintf('fitting ...\n')
gain = zeros(size(riesenmat,1),size(riesenmat,2));
offset = zeros(size(riesenmat,1),size(riesenmat,2));

%%
for i = 1:size(riesenmat,1)
    
    fprintf('line %d of %d \n',i,size(riesenmat,1));
    
    for j = 1:size(riesenmat,2)
        data = squeeze(riesenmat(i,j,:));
        p=[-1 -1];
        try
            p(2) = robustfit(means(data~=0 & data~=1),data(data~=0& data~=1),'fair',0.5,'off');
            p(1) = 0;
        catch e
            fprintf('some strange values at %d, %d\n',i,j);
        end
        if p(1)<0
            p(2)=means(data~=0& data~=1)\data(data~=0& data~=1);
            p(1)=0;
        end
        
        gain(i,j) = p(2);
        offset(i,j) = p(1);
    end
end


