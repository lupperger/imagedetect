% calculates the background of every image in imgroot
% imgroot must contain position folder

function interpointscount=calculateBackgroundClassifierComplete(imgroot,fullpath,wavelength,extension,dogain)
imgroot
wavelength
extension
fullpath
dogain

% get pos folder
pos = dir([imgroot '/*' fullpath(end-5:end)]);
fprintf('position %s\n',pos(1).name)

%%% check if out folder exists
if ~isdir([imgroot '/background/'])
    mkdir([imgroot '/background/']);
end

if ~isdir([imgroot '/background/' pos(1).name])
    mkdir([imgroot '/background/' pos(1).name]);
end

% check for classifier

allc= dir([imgroot '/*classifier.mat']);

if  isempty(allc)
    error('did not find any classifiers.\n');
end
cx=0;
for c = 1:numel(allc)
    if strfind(allc(c).name,wavelength)
        cx = c;
    end
end


if cx == 0 
    error('did not find classifier for wavelength %s.\n',wavelength);
end    
fprintf('loading classifier %s...\n',[imgroot '/' allc(cx).name])
train = load([imgroot '/' allc(cx).name],'train');
train = train.train;
try 
    TreeBagger();
catch e
    % stupid including treebagger into mcc
end
imageCache('init',1);
%%% get files
d  = dir ([fullpath '/*' wavelength '*' extension]);
winsize=30;
interpointscount=[];
% figure;
fprintf('Found %d images\n',numel(d))
for f = 1:numel(d)
    
    tic
%     fprintf('Reading file %s (%d of %d)... ',d(f).name,f,numel(d));
    %%% load
    img = loadimage([fullpath '/' d(f).name],0);

    %%% normalize
    try
%         fprintf('Autodetecting parameters...')
%         featuremat=exploreBackgroundParams(img,settings.winsize,0);
        
%         [classes, centroids] = kmeans(featuremat,2) ;
        
        % set smaller parameters:
%         if centroids(1,1)<centroids(2,1)
%             settings.kurtosis = centroids(1,2);
%             settings.skewness = centroids(1,1);
%             settings.dispersion = centroids(1,3);
%         else
%             settings.kurtosis = centroids(2,2);
%             settings.skewness = centroids(2,1);
%             settings.dispersion = centroids(2,3);
%         end


        fprintf('creating background... ')
        
        
        ZI = backgroundestimation_classifier(img,train);
    
        interpointscount(f)=1;


        %%% save
        fprintf('saving... ')
        imwrite(ZI, [imgroot '/background/' pos(1).name '/' d(f).name(1:end-4) '.png'],'png','Bitdepth',16);

        t=toc;
        fprintf('done in %f seconds.\n',t);
    catch e
        interpointscount(f) =-1;
        t=toc;
        fprintf('failed in %f seconds.\n',t);
        rethrow(e)
    end
end
if any(interpointscount<0)
    error(['There were problems with %d images.\nFollowing IDs:\n' sprintf('%d\t',find(interpointscount<0))],sum(interpointscount<0));
end

if str2double(dogain)
    gainpath = [imgroot '/background/' pos(1).name '/' ]
%     path
    [gain offset]=calculateGainOffset(gainpath,wavelength);
    
    
    fprintf('saving images...')
    imwrite(uint16(255*gain),[gainpath 'gain_' wavelength '.png'],'bitdepth',16)
    imwrite((offset),[gainpath 'offset_' wavelength '.png'],'bitdepth',16)
    fprintf('done. Have a nice day!\n');
end
exit

