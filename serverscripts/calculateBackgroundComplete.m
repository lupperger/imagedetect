% calculates the background of every image in imgroot
% imgroot contains position folder
% example: calculateBackgroundComplete('F:\130412AF6\','F:\130412AF6\130412AF6_p0154\','w01','png',1)
function interpointscount=calculateBackgroundComplete(imgroot,fullpath,wavelength,extension,dogain)
imgroot
wavelength
extension
fullpath
dogain

% get pos folder
[~,pos] = fileparts(fullpath);
fprintf('position %s\n',pos)

%%% check if out folder exists
if ~isdir([imgroot '/background/'])
    mkdir([imgroot '/background/']);
end

if ~isdir([imgroot '/background/' pos])
    mkdir([imgroot '/background/' pos]);
end


imageCache('init',1);
%%% get files
d  = dir ([fullpath '/*' wavelength '*' extension]);
winsize=50;
interpointscount=[];
% figure;
fprintf('Found %d images\n',numel(d))
for f = 1:numel(d)
    if usejava('desktop')
        waitbar(f/numel(d))
    end
    tic
%     fprintf('Reading file %s (%d of %d)... ',d(f).name,f,numel(d));
    %%% load
    img = loadimage([fullpath '/' d(f).name],0);

    %%% normalize
    try
%         fprintf('Autodetecting parameters...')
%         featuremat=exploreBackgroundParams(img,settings.winsize,0);
        
%         [classes, centroids] = kmeans(featuremat,2) ;
        
        % set smaller parameters:
%         if centroids(1,1)<centroids(2,1)
%             settings.kurtosis = centroids(1,2);
%             settings.skewness = centroids(1,1);
%             settings.dispersion = centroids(1,3);
%         else
%             settings.kurtosis = centroids(2,2);
%             settings.skewness = centroids(2,1);
%             settings.dispersion = centroids(2,3);
%         end


        fprintf('creating background... ')
        
        
        [ZI counter extrap] = backgroundestimation_server(img,winsize);
    
        interpointscount(f)=counter;


        %%% save
        fprintf('saving... ')
        imwrite(ZI, [imgroot '/background/' pos '/' d(f).name(1:end-4) '.png'],'png','Bitdepth',16);
%         imwrite(extrap, [imgroot '/background/' pos '/' d(f).name(1:end-4) '_extrap.png'],'png','Bitdepth',1);

        t=toc;
        fprintf('done in %f seconds.\n',t);
    catch e
        interpointscount(f) =-1;
        t=toc;
        fprintf('failed in %f seconds.\n',t);
%         rethrow(e)
    end
end
if any(interpointscount<0)
    error(['There were problems with %d images.\nFollowing IDs:\n' sprintf('%d\t',find(interpointscount<0))],sum(interpointscount<0));
end

if str2double(dogain)
    gainpath = [imgroot '/background/' pos '/' ]
%     path
    [gain offset]=calculateGainOffset(gainpath,wavelength);
    
    
    fprintf('saving images...')
    imwrite(uint16(255*gain),[gainpath 'gain_' wavelength '.png'],'bitdepth',16)
    imwrite((offset),[gainpath 'offset_' wavelength '.png'],'bitdepth',16)
    fprintf('done. Have a nice day!\n');
end
% exit

