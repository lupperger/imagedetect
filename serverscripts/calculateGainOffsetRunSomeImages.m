function calculateGainOffsetRunSomeImages(path,wavelength,lower,upper)
%% path will be given by qsub script

path
[gain offset]=calculateGainOffsetSomeImages(path,wavelength,lower,upper);


fprintf('saving images...')
imwrite(uint16(255*gain),[path 'gain_' wavelength '.png'],'bitdepth',16)
imwrite((offset),[path 'offset_' wavelength '.png'],'bitdepth',16)
fprintf('done. Have a nice day!\n');
exit