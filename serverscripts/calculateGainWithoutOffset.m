function [gain offset]=calculateGainWithoutOffset(path,wavelength)
wavelength
warning off
%%% init
fprintf('init...')
cd(path)
d = dir(['*' wavelength '.png']);
fprintf('found %d images for command %s in path %s\n',numel(d),['*' wavelength '.png'],path);
if numel(d)<30
    error('to few images')
end
%%
means = zeros(37,1);
img = double(imread(d(1).name))/255/255;

riesenmat=zeros(size(img,1),size(img,2),37);
counter = 0;

%% load images
imageCache('init',1);

fprintf('loading images...\n')
xx = [1:29 30:10:numel(d)];

% xx = [1:numel(d)];
for x=xx(xx<numel(d))
    fprintf('%d,',x)
    counter = counter+1;
    d(x)
    img = loadimage(d(x).name,0);
    means(counter) = median (img(:));
    riesenmat(:,:,counter) = img;
    if x > 150
        disp('BEWARE of the SWAP')
        break;
    end
end
fprintf('...done.\n')
%% fit the pixels
fprintf('fitting ...\n')
gain = zeros(size(riesenmat,1),size(riesenmat,2));
offset = zeros(size(riesenmat,1),size(riesenmat,2));


for i = 1:size(riesenmat,1)
    
    fprintf('line %d of %d \n',i,size(riesenmat,1));
    
    for j = 1:size(riesenmat,2)
        data = squeeze(riesenmat(i,j,:));
        p=[-1 -1];
        try
            p = robustfit(means(data~=0 & data~=1),data(data~=0 & data~=1),'fair',0.5,'off');
        catch e
            fprintf('some strange values at %d, %d\n',i,j);
        end
        if p(1)<0
            p(2)=means(data~=0 & data~=1)\data(data~=0 & data~=1);
            p(1)=0;
        end
        
        gain(i,j) = p(2);
        offset(i,j) = p(1);
    end
end


