%% auto check fileName for fragments (position, timepoint, wavelength...) and create filesettings

function filesettings = generateFilesettings(imageName)
 %% fragmentImage;
    %get extension
    x = strsplit_qtfy('.',imageName);
    ext = x(end);
    ext= ext{1};
    imageName = imageName(1:end-(length(ext)+1));
    %find separator    
    if(length(strfind(imageName,'_')) > 1)
        sep = '_';
    elseif(length(strfind(imageName,'-')) > 1)
        sep = '-';
    elseif(length(strfind(imageName,'.')) > 1)
        sep='.';
    end
    tab = {-1 -1 '-1'; -1 -1 '-1'; -1 -1 '-1'; -1 -1 '-1'};
    %split fragments
    frags = strsplit_qtfy( sep, imageName);
    for i = 2: numel(frags)
         id = regexprep(frags{i},'[^a-zA-Z]','');         
        len = length(frags{i})-length(id);
        if(ismember('p',id) || ismember('s',id))
            tab{1,1} = i-1;
            tab{1,2} = len;
            tab{1,3} = id;
        elseif(ismember('t',id))
            tab{2,1} = i-1;
            tab{2,2} = len;
            tab{2,3} = id;
        elseif(ismember('w',id) || ismember('c',id))
            tab{3,1} = i-1;
            tab{3,2} = len;
            tab{3,3} = id;
        else
            tab{4,1} = i-1;
            tab{4,2} = len;
            tab{4,3} = id;
        end
    end
    filesettings.matrix = tab;
    filesettings.separator = sep;
end

