% v=quantifyCircle(A), quantifies a circle area in the quadratic array A
function v=quantifyCircle(A)
if size(A,1) ~= size(A,2) || mod(size(A,1),2) == 0
    error('Both array dimensions must be equal and odd');
end

v=0;
n = size(A,1);
m = ceil(n/2);
% iterate over all coordinates
for x=1:n
    for y=1:n
        if ( ((x-m)/m)^2+((y-m)/m)^2 < 1 )
            v=v+double(A(x,y));
        end
    end
end

