% searches all subfolders for all files with all given extensions

function res = getfiles_Nonrec_all(varargin)

folder = varargin{1};
exts = cellstr(varargin{2});

res.files={};
res.folders={};
for i = 1:numel(exts)
    ext= (exts(i));
    ext = char(ext);
res=addfiles(res,folder,ext);

allpaths = genpath(folder);
if ispc
    allpaths=strsplit_qtfy(';',allpaths);
else
    allpaths=strsplit_qtfy(':',allpaths);
end
for p = 2:numel(allpaths)-1
    res = addfiles(res,allpaths{p},ext);

end

end

function res=addfiles(res,folder,ext)


files = dir([folder '/*.' ext]);

if numel(files)>0
    res.files = [res.files files.name];
    res.folders = [res.folders repmat({folder},1,numel(files))];
end
