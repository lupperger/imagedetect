function readXMLfile(handles)
global vtset
%%% read XML file to get wavelength information

xmlfilename = dir([vtset.movieFolder '*xml']);
xmlfile = fileread(strcat(vtset.movieFolder, xmlfilename.name));

try
    if numel(xmlfilename)>0 %exist([vtset.movieFolder 'TATexp.xml'],'file')
        wllist ={};
        wllist2 ={};
        
        xmlstr =  xml_parseany(xmlfile); 
        
        list = xmlstr.WavelengthData{1}.WavelengthInformation;
        for k = 1:size(list,2)
            wllist{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name) '.' char(list{k}.WLInfo{1}.ATTRIBUTE.ImageType)];
            wllist2{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name)];
        end



        % set all wavelength selectors to droplist
        set(handles.extDetection,'Style','popupmenu');
        set(handles.extDetection,'String',wllist);
        set(handles.extQuantification,'Style','popupmenu');
        set(handles.extQuantification,'String',wllist);
        set(handles.extNonFluor,'Style','popupmenu');
        set(handles.extNonFluor,'String',wllist);
        set(handles.annotation1,'Style','popupmenu');
        set(handles.annotation1,'String',wllist2);
        set(handles.annotation2,'Style','popupmenu');
        set(handles.annotation2,'String',wllist2);


    else
        exp=get(handles.experiment,'String');
        warndlg(sprintf('No XML file found for experiment %s',exp));
    end
catch
    % reset all wavelength selectors to edit
    set(handles.extDetection,'Style','edit');
    set(handles.extDetection,'String','');
    set(handles.extQuantification,'Style','edit');
    set(handles.extQuantification,'String','');
    set(handles.extNonFluor,'Style','edit');
    set(handles.extNonFluor,'String','');
    warndlg('XML read error, please set wavelengths manually','XML error')
end