function cost = ACoptimization(params,img,cellmask,x,y,trackset)




trackset.Delta = params(1);
trackset.MinArea = params(2);
trackset.MaxArea = params(3);

[subimg centerX centerY] = extractSubImage(img, x, y, trackset.win);


[Center]=myseg_drlse(subimg, [], [], centerX, centerY,[],[]);


% nothing segmented
if ~any(Center)
    cost=Inf;
    return
end

% has to overlap
if ~ismember(2,Center+cellmask)
    cost = Inf;
    return
end

% calc distance
cost = segmentationdistance(cellmask,Center);