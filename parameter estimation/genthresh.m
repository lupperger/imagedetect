function vec = genthresh(vec,str)

i = randi(numel(vec));

vec(i) = vec(i) + round(randn * vec(i) *100)/100 *str;