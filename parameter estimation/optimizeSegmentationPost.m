%% find outliers
global vtset opp;

tree=vtset.treenum;
c = 112;
channel = 1;
alldata = vtset.results{tree}.quants(channel).int(vtset.results{tree}.quants(channel).cellNr ==c);
alltps  = vtset.results{tree}.quants(channel).timepoint(vtset.results{tree}.quants(channel).cellNr ==c);
ids=find(vtset.results{tree}.quants(channel).cellNr ==c);

[alltps, order] = sort(alltps);
alldata = alldata(order);
ids = ids(order);

figure;
plot(alltps,alldata,'*-')
hold on
plot(alltps,smooth(alldata),'-r')

% plot(alltp(1:end-1),diff(alldata),'-k')

% leave one out cross validation
rm =zeros(numel(alldata),1);
estimated=zeros(numel(alldata),1);
for p = 2:numel(alldata)-1
    data = alldata(find(alldata)~=p);
    tps = alltps(find(alldata)~=p);
    
    sm = smooth(data);
    
    estimate = (sm(p)+sm(p-1))/2;
    actual = alldata(p);
    estimated(p) = estimate;
    rm(p)=(estimate-actual)^2;
end
plot(alltps,rm,'g')


%% now take the x highest values and optimize segmentation
upto = 3;
norm = 1;
tree=1;
plotdat=1;
chan = 'w3.png';

opts.StopTemp=0.1;
opts.Verbosity = 2;
opts.StopVal = 1e-2;
opts.InitTemp = 1;
str = 0.2;
opts.Generator = @(v)genthresh(v,str);
opts.CoolSched = @(T) (.1*T);

constraints.minArea=50;
constraints.maxArea=250;
% constraints.minArea=50;

[~,order] = sort(rm,'descend');

% vtset.results{tree}.quants(channel).oldint = vtset.results{tree}.quants(channel).int

for i = 1:upto
    
    id = ids(order(i));
%     id = 470;
    tp =vtset.results{tree}.quants(channel).timepoint(id);
    c  = vtset.results{tree}.quants(channel).cellNr(id);
    Center = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellmask{vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp};
    trackset = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).trackset(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
    x = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).X(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
    y = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).Y(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
    filename = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).filename{vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp};
    img  = loadimage([vtset.results{tree}.settings.imgroot '/' filename],norm,chan);
    
%     trackset.win =50;
    
    quantfilename = vtset.results{tree}.quants(channel).filename{id};
    quantimage = loadimage([vtset.results{tree}.settings.imgroot '/' quantfilename],norm,chan);
    quantsubimage = extractSubImage(quantimage, x, y, trackset.win);
    
    
    
    estimate = estimated(order(i));
    actual = alldata(order(i));
    h=figure;
    oldpos= get(h,'position');
    set(h,'position',[oldpos(1:2) 1200 400]);
    colormap(gray)
    subplot(1,3,1)
    [Center,subimg]=cellSegmentation(img, x, y, trackset,[],[]);
    out = bwperim(Center);
    sub = quantsubimage;
    sub(out) = max(sub(:))*1.2;
    imagesc(sub);
    axis off
    axis equal
    
    trackset.threshcorr=1;
    subplot(1,3,2)
    axis off
    axis equal
% Center = zeros(51,51);
% Center(5:end-5,5:end-5)=1;
    % now optimize with contraints: max, min area, but maximize intensity
    zuOptimieren = trackset.threshcorr;
    
    %[bestparam bestcost] = anneal(@(zuOptimieren)optimizeSingleSegmentation(zuOptimieren,img,x,y,trackset,quantsubimage,constraints,estimate,plotdat), zuOptimieren,opts);
        

     [bestparam bestcost, eflag] = fminsearch(@(zuOptimieren)optimizeSingleSegmentation(zuOptimieren,img,x,y,trackset,quantsubimage,constraints,estimate), zuOptimieren);

    
    
%     seg = region_seg(subimg, double(Center), 1000,1,true);
    % show final segmentation
    subplot(1,3,3)
    trackset.threshcorr = bestparam;
    [seg,subimg]=cellSegmentation(img, x, y, trackset,[],[]);
    out = bwperim(seg);
    sub = quantsubimage;
    sub(out) = max(sub(:))*1.2;
    imagesc(sub);
    axis off
    axis equal
    drawnow
    
    vtset.results{tree}.quants(channel).int(id) = sum(sum(quantsubimage(seg)));
    
end
