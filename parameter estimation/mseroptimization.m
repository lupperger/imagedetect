function cost = mseroptimization(params,img,cellmask,x,y,trackset)

if (params(1)<=1)
    cost = Inf;
    return
end

if any(params<=0)
    cost = Inf;
    return
end

if params(2)>params(3)
   cost = Inf;
   return
end

if params(3) > trackset.win^2
    cost = Inf;
    return
end

trackset.Delta = params(1);
trackset.MinArea = params(2);
trackset.MaxArea = params(3);

[subimg centerX centerY] = extractSubImage(img, x, y, trackset.win);

Center=myseg_mser(subimg, 0, trackset, centerX, centerY);

% nothing segmented
if ~any(Center)
    cost=Inf;
    return
end

% has to overlap
if ~ismember(2,Center+cellmask)
    cost = Inf;
    return
end

% calc distance
cost = segmentationdistance(cellmask,Center);