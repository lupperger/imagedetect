%% create a manually curated testset

% 111115af6 -p201-01 channel 3

testset = vtset.results{1}.detects(3);
imgroot= vtset.results{1}.settings.imgroot;
imageCache('init',50);
%% run segmentation and execute cost function

opts.StopTemp=1e-3;
opts.Verbosity = 2;
opts.StopVal = 1e-5;
opts.InitTemp = 10;
str = 0.1;
opts.Generator = @(v)genfun(v,str);

ids = find(testset.active);
bestparams = zeros(numel(ids),3);
counter=0;
for i = ids(3100)
    counter = counter+1;
    tic
    img = loadimage([imgroot '/' testset.filename{i}],0);
    x = testset.X(i);
    y = testset.Y(i);
    
    cellmask = testset.cellmask{i};
    
    trackset = testset.trackset(i);
    
    % optimization...
    %...

    initialparams(1) = trackset.Delta;
    initialparams(2) = trackset.MinArea;
    initialparams(3) = trackset.MaxArea;
    
    % initial segmentation
    [subimg centerX centerY] = extractSubImage(img, x, y, trackset.win);
    initalCenter=myseg_mser(subimg, 0, trackset, centerX, centerY);

    
    [bestparam bestcost] = anneal(@(zuOptimieren)mseroptimization(zuOptimieren, img,cellmask,x,y,trackset), initialparams, opts)
    
%     initialparams = initialparams *rand*2;
%     [bestparam bestcost] = fminunc(@(zuOptimieren)mseroptimization(zuOptimieren, img,cellmask,x,y,trackset), initialparams, opts);

    % set best params
    bestparams(counter,:) = bestparam;
    trackset.Delta = bestparam(1);
    trackset.MinArea = bestparam(2);
    trackset.MaxArea = bestparam(3);
    
    % show final segmentation: 
    Center=myseg_mser(subimg, 0, trackset, centerX, centerY);
    
    subplot(2,2,1)
    imagesc(subimg)
    title('original')
    
    subplot(2,2,2)
    imagesc(cellmask)
    title('manual')
    
    subplot(2,2,4)
    imagesc(Center)
    title('optimized');
    
    subplot(2,2,3)
    imagesc(initalCenter)
    title('inital');
%     
%     
%     % calc distance
%     cost = segmentationdistance(cellmask,Center);
    toc
    return
    
    
end