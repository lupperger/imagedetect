function [ output_args ] = optimizeSegmentationPost_new( input_args )
%OPTIMIZESEGMENTATIONPOST_NEW Summary of this function goes here
%   Detailed explanation goes here

disp('optimize');
%% find outliers
global vtset opp;




%% now take the x highest values and optimize segmentation
upto = 3;
norm = 1;
tree=1;
plotdat=1;
channel = 1;
range = 1;
chan = ['w01.png'];

opts.StopTemp=0.1;
opts.Verbosity = 2;
opts.StopVal = 1e-2;
opts.InitTemp = 1;
str = 0.2;
opts.Generator = @(v)genthresh(v,str);
opts.CoolSched = @(T) (.1*T);

constraints.minArea=50;
constraints.maxArea=250;
% constraints.minArea=50;

%[~,order] = sort(rm,'descend');

% vtset.results{tree}.quants(channel).oldint = vtset.results{tree}.quants(channel).int
outlier_s = size(opp.outliers);
for j=1:1:outlier_s(1)
    
    for i = 1:upto
        %{
        id = ids(order(i));
    %     id = 470;
        tp =vtset.results{tree}.quants(channel).timepoint(id);
        c  = vtset.results{tree}.quants(channel).cellNr(id);
        Center = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellmask{vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp};
        trackset = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).trackset(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
        x = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).X(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
        y = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).Y(vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp);
        filename = vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).filename{vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).cellNr == c & vtset.results{tree}.detects(vtset.results{tree}.quants(channel).detectchannel(id)).timepoint ==tp};
        img  = loadimage([vtset.results{tree}.settings.imgroot '/' filename],norm,chan);

    %     trackset.win =50;
        %}
        
        tree = opp.outliers(j,3);
        tp = opp.outliers(j,2);
        cell = opp.outliers(j,1);
        
        
        (vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp)
        
        filename =  [cell2mat(vtset.results{tree}.quants(channel).filename(vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp))];
        img = loadimage([vtset.results{tree}.settings.imgroot filename],norm, vtset.results{tree}.quants(channel).settings.wl);
        
        Center = vtset.results{tree}.detects(channel).cellmask((vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp));
        trackset = vtset.results{tree}.detects(channel).trackset((vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp));
        x = vtset.results{tree}.detects(channel).X((vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp));
        y = vtset.results{tree}.detects(channel).Y((vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp));
        
        
        quantfilename = [cell2mat(vtset.results{tree}.quants(channel).filename(vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp))];
        quantimage = loadimage([vtset.results{tree}.settings.imgroot  quantfilename],norm,vtset.results{tree}.quants(channel).settings.wl);
        quantsubimage = extractSubImage(quantimage, x, y, trackset.win);
       
        pkt =  find((vtset.results{tree}.detects(channel).cellNr == cell & vtset.results{tree}.detects(channel).timepoint == tp));
        estimate = 0;
        if( vtset.results{tree}.detects(channel).cellNr(pkt+range) == cell && vtset.results{tree}.detects(channel).cellNr(pkt-range) == cell)
            for r=-range:1:range
                estimate = estimate + vtset.results{tree}.quants(channel).int(pkt+r);
            end
            estimate = estimate / ((range*2)+1);
        end
        %{
        estimate = estimated(order(i));
        actual = alldata(order(i));
        h=figure;
        oldpos= get(h,'position');
        set(h,'position',[oldpos(1:2) 1200 400]);
        colormap(gray)
        subplot(1,3,1)
        %}
        
        [Center,subimg]=cellSegmentation(img, x, y, trackset,[],[]);
        out = bwperim(Center);
        sub = quantsubimage;
        sub(out) = max(sub(:))*1.2;
        imagesc(sub);
        axis off
        axis equal

        trackset.threshcorr=1;
        subplot(1,3,2)
        axis off
        axis equal
    % Center = zeros(51,51);
    % Center(5:end-5,5:end-5)=1;
        % now optimize with contraints: max, min area, but maximize intensity
        zuOptimieren = trackset.threshcorr;

        %[bestparam bestcost] = anneal(@(zuOptimieren)optimizeSingleSegmentation(zuOptimieren,img,x,y,trackset,quantsubimage,constraints,estimate,plotdat), zuOptimieren,opts);


         [bestparam bestcost, eflag] = fminsearch(@(zuOptimieren)optimizeSingleSegmentation(zuOptimieren,img,x,y,trackset,quantsubimage,constraints,estimate, 0), zuOptimieren);



    %     seg = region_seg(subimg, double(Center), 1000,1,true);
        % show final segmentation
        subplot(1,3,3)
        trackset.threshcorr = bestparam;
        %vtset.results{tree}.detects(channel).trackset(channel).threshcorr = bestparam;
        vtset.results{tree}.detects(channel).settings.trackset.threshcorr = bestparam;
        
        [cent subimg, new_trackset]=cellSegmentation(img, x, y, trackset,[],[]);
        
        detectCells(channel, tree, cell, tp);
        quantifyCells(channel, tree, cell, tp);
        
        %{
        mysize= sum(Center(:));
        periemter = sum(sum(bwperim(Center)));
        int=[];
 
        subimgQuant = extractSubImage(subimg, x, y, trackset.win);
        dasum=subimgQuant(Center);
        int(chaneln)= sum(dasum);
        
        vtset.temp.int=int;
        vtset.temp.size=mysize;
        vtset.temp.perimeter=periemter;
        vtset.temp.cellMask=Center;
        vtset.temp.mean=mysize;
        vtset.temp.trackset = trackset;
        %}
        disp('dooooone');
        %{
        out = bwperim(seg);
        sub = quantsubimage;
        sub(out) = max(sub(:))*1.2;
        imagesc(sub);
        axis off
        axis equal
        drawnow

        vtset.results{tree}.quants(channel).int(id) = sum(sum(quantsubimage(seg)));
        %}
    end
end
    
    
end

