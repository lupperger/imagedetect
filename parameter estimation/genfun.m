function vec = genfun(vec,str)

i = randi(numel(vec));

vec(i) = vec(i) + randn * vec(i) *str;