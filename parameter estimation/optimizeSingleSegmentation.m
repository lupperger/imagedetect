function cost = optimizeSingleSegmentation(threshcorr,image,x,y,trackset,quantsubimage,constraints,estimate,plotdat)
% threshcorr
trackset.threshcorr =threshcorr;
if threshcorr<=0
    cost=99999999;
    return
end

if threshcorr>=2
    cost=99999999;
    return
end


[Center]=cellSegmentation(image, x, y, trackset,[],[]);
out = bwperim(Center);
sub = quantsubimage;
sub(out) = max(sub(:))*1.2;

if sum(Center(:))< constraints.minArea
    cost=99999999;
    return
end


if sum(Center(:))> constraints.maxArea
    cost=99999999;
    return
end
actual = sum(sum(quantsubimage(Center)));
cost = (actual - estimate)^2;

if plotdat
imagesc(sub);
title(sprintf('cost %2.2f thresh %2.2f',cost,threshcorr))
axis off
    axis equal
drawnow
% pause(0.01)
end
