% take two images (binary) and two feature sets
% return distance of both images

function cost = segmentationdistance(image1,image2,feature1,feature2)


cost = rmsd(image1,image2);