function mapmissingannotation(t,anno)
global vtset


wh = waitbar(0,'Mapping additional annotation');
try
    xmlfilename = dir([vtset.movierootFolder '/*xml']);
    xmlfilename = xmlfilename(end).name;
catch e
    errordlg(sprintf('Xml %s not found',[vtset.movierootFolder '/*xml']))
    %     close(wh)
    %     rethrow(e)
    %     return
end
if strcmp(vtset.results{t}.settings.treeroot(end),'/')
    vtset.results{t}.settings.treeroot(end)=[];
end
[~,pos]=fileparts(vtset.results{t}.settings.treeroot);
res = tttParser([vtset.tttrootFolder ['/20' vtset.results{t}.settings.exp(1:2) '/']  vtset.results{t}.settings.exp '/' pos '/' vtset.results{t}.settings.treefile],[vtset.movierootFolder '/' xmlfilename]);

if isempty(res)
    return
end

vtset.results{t}.nonFluor.(anno)=zeros(numel(vtset.results{t}.nonFluor.cellNr),1);
for c = unique(vtset.results{t}.nonFluor.cellNr)
    waitbar(c/numel(unique(vtset.results{t}.nonFluor.cellNr)),wh)
    an = res.(anno)(res.cellNr == c);
    tp = res.timepoint(res.cellNr == c);
    [~,order] = sort(tp);
    an = an(order);
    fltps = vtset.results{t}.nonFluor.timepoint(vtset.results{t}.nonFluor.cellNr == c);
    [~,order]=sort(fltps);
    [~,order ]= sort(order);
    an = an(order);
    %     if numel(an)~=1
    %         fprintf('Numel of cell %d = %d vs struct: %d\n',c ,numel(an),numel(fltps))
    %         vtset.results{t}.nonFluor.(anno)(vtset.results{t}.nonFluor.cellNr == c) = 0;
    %         continue;
    %     end
    vtset.results{t}.nonFluor.(anno)(vtset.results{t}.nonFluor.cellNr == c) = an;
end



% for q=1:numel(vtset.results{t}.nonFluor)
%     vtset.results{t}.nonFluor(q).(anno)=zeros(numel(vtset.results{t}.nonFluor(q).cellNr),1);
%     for c = 1:numel(vtset.results{t}.nonFluor(q).cellNr)
%         waitbar(c/numel(vtset.results{t}.nonFluor(q).cellNr),wh)
%         an = res.(anno)(res.cellNr == vtset.results{t}.nonFluor(q).cellNr(c) & res.timepoint == vtset.results{t}.nonFluor(q).timepoint(c));
%         if numel(an)~=1
%             fprintf('Numel of tp %d, cell %d = %d\n',vtset.results{t}.nonFluor(q).timepoint(c),vtset.results{t}.nonFluor(q).cellNr(c) ,numel(an))
%             vtset.results{t}.nonFluor(q).(anno)(c) = 0;
%             continue;
%         end
%         vtset.results{t}.nonFluor(q).(anno)(c) = an;
%     end
% end

close(wh)

