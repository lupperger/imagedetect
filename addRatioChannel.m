%% add a channel which shows ratio between two 

% copy all stuff
vtset.results{1}.quants(5).timepoint = vtset.results{1}.quants(1).timepoint;
vtset.results{1}.quants(5).cellNr = vtset.results{1}.quants(1).cellNr;
vtset.results{1}.quants(5).absoluteTime = vtset.results{1}.quants(1).absoluteTime;
vtset.results{1}.quants(5).active = vtset.results{1}.quants(1).active;
vtset.results{1}.quants(5).int = (vtset.results{1}.quants(1).int-vtset.results{1}.quants(3).int)./(vtset.results{1}.quants(3).int+vtset.results{1}.quants(1).int);

vtset.results{1}.quants(5).settings.wl = 'wx.xxx';
vtset.results{1}.quants(5).settings.detectchannel = 2;
vtset.results{1}.quants(5).settings.normalize = 0;

%%
global vtset
% set(vtset.haxes(1),'ylim',[0 1])
% set(vtset.haxes(3),'ylim',[0 2])
% set(vtset.haxes(2),'ylim',[0 2])
% set(vtset.haxes(4),'ylim',[0 2])
% set(vtset.haxes(5),'ylim',[-1.5 1.5])

title(vtset.haxes(1),'PU.1')
title(vtset.haxes(4),'GATA-1')
% title(vtset.haxes(5),'PU.1 / GATA-1 ratio')


% set(vtset.haxes(1),'yticklabel','')
% set(vtset.haxes(3),'yticklabel','')
% set(vtset.haxes(5),'yticklabel','')

%% 

set(vtset.hplot,'position',[360   216   693   449]);
for chan = 2:numel(vtset.haxes)
    oldpos = get(vtset.haxes(chan),'position');
    coursepos = get(vtset.haxes(1),'position');
    set(vtset.haxes(chan),'position',[coursepos(1) oldpos(2) coursepos(3) oldpos(4)])
end
% oldpos = get(vtset.haxes(5),'position');
% coursepos = get(vtset.haxes(1),'position');
% set(vtset.haxes(5),'position',[coursepos(1) oldpos(2) coursepos(3) oldpos(4)])

% oldpos = get(vtset.haxes(6),'position');
% coursepos = get(vtset.haxes(1),'position');
% set(vtset.haxes(6),'position',[coursepos(1) oldpos(2) coursepos(3) oldpos(4)])