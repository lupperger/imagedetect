
% Not really documented yet
%
% [Center,All]=myseg(OrigImage,trackset, centerX, centerY)
% 
% trackset requires fields:
% 


function [Center,All, numL]=myseg(OrigImage, Threshold, trackset, centerX, centerY)


Threshold=min(Threshold,1);
Threshold=max(Threshold,0);

% figure;
% % example for diss: 120208_p0028-001, tp4150, c1407
% imagesc(OrigImage)
% hold on;
% colormap(gray)
% caxis([0 1])
% axis equal
% axis off
% myT=1;
% contour(OrigImage>=Threshold*myT,'r')
%%
% figure;hist(OrigImage(:),20)
% hold on
% line([Threshold Threshold].*1, get(gca,'ylim'),'color',[1 0 0])
% line([Threshold Threshold].*1.6, get(gca,'ylim'),'color',[0 0 0])
% line([Threshold Threshold].*.6, get(gca,'ylim'),'color',[0 0 0])
%% smoothing filter
sigma = 1;
FiltLength = 2*sigma;                                              % Determine filter size, min 3 pixels, max 61
[x,y] = meshgrid(-FiltLength:FiltLength,-FiltLength:FiltLength);   % Filter kernel grid
f = exp(-(x.^2+y.^2)/(2*sigma^2));f = f/sum(f(:));                 % Gaussian filter kernel
%                BlurredImage = conv2(OrigImage,f,'same');                             % Blur original image
%%% This adjustment prevents the outer borders of the image from being
%%% darker (due to padding with zeros), which causes some objects on the
%%% edge of the image to not  be identified all the way to the edge of the
%%% image and therefore not be thrown out properly.
BlurredImage = conv2(OrigImage,f,'same') ./ conv2(ones(size(OrigImage)),f,'same');


% imagesc(BlurredImage)

%% apply threshold
%Objects = BlurredImage > Threshold;
%% apply Otsu

% if(trackset.threshfix > 0)
%     Threshold = trackset.threshfix;
% else
%     Threshold = max(trackset.threshmin, graythresh(BlurredImage));
% end
% 
% Threshold=Threshold*trackset.threshcorr;

Objects = im2bw(BlurredImage,Threshold);
% imagesc(Objects)
% drawnow

%% calc mode of picture




%% maxima suppression
% BlurredImage = CPsmooth(OrigImage,'Gaussian Filter',trackset.smooth,0); 
BlurredImage=(imfilter(OrigImage,fspecial('disk',trackset.smooth),'replicate'));
MaximaMask = getnhood(strel('disk', trackset.max));

%% object distinction
if strcmp(trackset.clumped,'Intensity')
    ResizedBlurredImage = BlurredImage;

    %%% Initialize MaximaImage
    MaximaImage = ResizedBlurredImage;
    %%% Save only local maxima
    MaximaImage(ResizedBlurredImage < ...
        ordfilt2(ResizedBlurredImage,sum(MaximaMask(:)),MaximaMask)) = 0;

    MaximaImage = MaximaImage > Threshold;
    %%% Shrink to points (needed because of the resizing)
    MaximaImage = bwmorph(MaximaImage,'shrink',inf);
elseif strcmp(trackset.clumped,'Shape')
    %%% Calculate distance transform
    DistanceTransformedImage = bwdist(~Objects);
    %%% Add some noise to get distinct maxima
    %%% First set seed to 0, so that it is reproducible
    rand('seed',0);
    DistanceTransformedImage = DistanceTransformedImage + ...
        0.001*rand(size(DistanceTransformedImage));
    ResizedDistanceTransformedImage = DistanceTransformedImage;
    %%% Initialize MaximaImage
    MaximaImage = ones(size(ResizedDistanceTransformedImage));
    %%% Set all pixels that are not local maxima to zero
    MaximaImage(ResizedDistanceTransformedImage < ...
        ordfilt2(ResizedDistanceTransformedImage,sum(MaximaMask(:)),MaximaMask)) = 0;
    %%% We are only interested in maxima within thresholded objects
    MaximaImage(~Objects) = 0;
    %%% Shrink to points (needed because of the resizing)
    MaximaImage = bwmorph(MaximaImage,'shrink',inf);
end

%% watershedding
%%% Overlay the maxima on either the original image or a distance
%%% transformed image. The watershed is currently done on
%%% non-smoothed versions of these image. We may want to try to do
%%% the watershed in the slightly smoothed image.
if strcmp(trackset.dividing,'Intensity')
    %%% Overlays the objects markers (maxima) on the inverted original image so
    %%% there are black dots on top of each dark object on a white background.
    Overlaid = imimposemin(1 - OrigImage,MaximaImage);
elseif strcmp(trackset.dividing,'Distance')
    %%% Overlays the object markers (maxima) on the inverted DistanceTransformedImage so
    %%% there are black dots on top of each dark object on a white background.
    %%% We may have to calculate the distance transform if not already done:
    if ~exist('DistanceTransformedImage','var')
        DistanceTransformedImage = bwdist(~Objects);
    end
    Overlaid = imimposemin(-DistanceTransformedImage,MaximaImage);
    % figure, imagesc(Overlaid), title('overlaid');
    % figure, imagesc(-DistanceTransformedImage), title('-DistanceTransformedImage');
end

%%% Calculate the watershed transform and cut objects along the boundaries
WatershedBoundaries = watershed(Overlaid) > 0;
Objects = Objects.*WatershedBoundaries;
%%% Label the objects
[Objects num] = bwlabel(Objects);

%%% filter by size
if ~isfield(trackset,'sizefilter')
    trackset.sizefilter.min= 5;
    trackset.sizefilter.max= Inf;
end
   
stats=regionprops(Objects, 'Area');
ids = find([stats.Area]<trackset.sizefilter.min | [stats.Area]>trackset.sizefilter.max);
Objects(ismember(Objects,ids)) = 0;


%%% Remove objects with no marker in them (this happens occasionally)
%%% This is a very fast way to get pixel indexes for the objects
% tmp = regionprops(Objects,'PixelIdxList');
% for k = 1:length(tmp)
%     %%% If there is no maxima in these pixels, exclude object
%     if sum(MaximaImage(tmp(k).PixelIdxList)) == 0
%         Objects(index) = 0;
%     end
% end

%%%% Label the objects
%Objects = bwlabel(Objects);

%% select central object (and show it)
% centerX = round(size(Objects,1)/2);
% centerY = round(size(Objects,2)/2);
All = im2bw(Objects);

numL=0;
if ~trackset.usenearest
    Center = bwselect(Objects,centerX,centerY);
else
    L = bwlabel(All);
    numL = max(L(:));
    % iterate over them
    minDist=Inf;
    minCell=-1;
    for i=1:numL
        [r,c] = find(L==i);
        % distance to center
        dist=norm([centerX centerY] -[mean(c) mean(r)]);
        if dist<minDist
            minDist=dist;
            minCell=i;
        end
    end
    Center=L==minCell;
end
