%map intensity from one quant channel to another

tree=1;
channelto = 1;
channelfrom = 2;
for c = 1:31
    
%     vtset.results{tree}.quants(channelto).int(vtset.results{tree}.quants(channelto).cellNr == c) =vtset.results{tree}.quants(channelfrom).int(vtset.results{tree}.quants(channelfrom).cellNr == c)
    
    alltp = torow(vtset.results{tree}.quants(channelto).timepoint(vtset.results{tree}.quants(channelto).cellNr == c));
    
    for tp = alltp
        intfrom = vtset.results{tree}.quants(channelfrom).int(vtset.results{tree}.quants(channelfrom).cellNr == c & vtset.results{tree}.quants(channelfrom).timepoint == tp);
        if isempty(intfrom)
            % delete timepoint in channelto
            delid = find(vtset.results{tree}.quants(channelto).cellNr == c & vtset.results{tree}.quants(channelto).timepoint == tp);
            
            for field=fields(vtset.results{tree}.quants)'
                if ~strcmp(field,'settings')
                    vtset.results{tree}.quants(channelto).(cell2mat(field))(delid)=[];
                end
            end
            
        else
            vtset.results{tree}.quants(channelto).int(vtset.results{tree}.quants(channelto).cellNr == c & vtset.results{tree}.quants(channelto).timepoint == tp) =intfrom;
        end
        
    end
end

%% create new detects struct
% results = vtset.results;
wl = 'w06.png';
trackset = CreateNewTracksettings;
corr=1;
tree=vtset.treenum;

vtset.results{tree}.detects(end+1).settings.wl = wl;
% find correct threshmethod
vtset.results{tree}.detects(end).settings.trackset = trackset;
vtset.results{tree}.detects(end).settings.normalize = corr;

%% create quants

tree=vtset.treenum;
wl = 'w01.png';
channel = numel(vtset.results{tree}.detects);
corr = 1;


vtset.results{tree}.quants(end+1).settings.wl = wl;
% find the correct detection channel
vtset.results{tree}.quants(end).settings.detectchannel = channel;
vtset.results{tree}.quants(end).settings.normalize = corr;

%% read ttt file & gather images 
dchan = numel(vtset.results{tree}.detects);
qchan=numel(vtset.results{tree}.quants);
    xmlfilename = dir([vtset.movieFolder '/*xml']);
    xmlfilename = xmlfilename(end).name;
    res = tttParser([vtset.tttfilesFolder '/' vtset.results{tree}.settings.treefile(1:end-10) '/' vtset.results{tree}.settings.treefile],[vtset.results{tree}.settings.imgroot xmlfilename]);
    % gather images
    gatherimages(res,tree,'D',dchan)
    gatherimages(res,tree,'Q',qchan)

%%  run detection
channel = numel(vtset.results{tree}.detects);
tree=vtset.treenum;
detectCells(channel,tree,vtset.results{tree}.settings.restrictcells,'complete')
% detectCells(channel,tree,[1 62 1015 768],'complete')

%% copy detectchannel from already qtfied quant channel to new quant channel

chfrom = 3;
chto = 4;
for id = 1:numel(vtset.results{tree}.quants(chfrom).cellNr)
    c  = vtset.results{tree}.quants(chfrom).cellNr(id);
    tp  = vtset.results{tree}.quants(chfrom).timepoint(id);
    detectchan  = vtset.results{tree}.quants(chfrom).detectchannel(id);
    vtset.results{tree}.quants(chto).detectchannel(vtset.results{tree}.quants(chto).cellNr == c  & vtset.results{tree}.quants(chto).timepoint == tp) = detectchan;
end
%% quantify
for channel = qchan;
tree=vtset.treenum;
quantifyCells(channel,tree,vtset.results{tree}.settings.restrictcells,'complete')
end
%% all together for adding endpoint stuff




trackset = CreateNewTracksettings;


for tree=1:numel(vtset.results)
    
    if max(vtset.results{tree}.nonFluor.timepoint) <55
        continue
    end
    tree
    % detects
    wl = 'w05.png';
    corr=1;
    vtset.results{tree}.detects(end+1).settings.wl = wl;
    % find correct threshmethod
    vtset.results{tree}.detects(end).settings.trackset = trackset;
    vtset.results{tree}.detects(end).settings.normalize = corr;
    
    
    % quants
    for wl = {'w05.png','w04.png','w03.png','w02.png'}
        wl = cell2mat(wl);
        channel = numel(vtset.results{tree}.detects);
        corr = 1;
        
        
        vtset.results{tree}.quants(end+1).settings.wl = wl;
        % find the correct detection channel
        vtset.results{tree}.quants(end).settings.detectchannel = channel;
        vtset.results{tree}.quants(end).settings.normalize = corr;
    end
    
    %     gather images
    dchan = numel(vtset.results{tree}.detects);
    
    xmlfilename = dir([vtset.movieFolder '/*xml']);
    xmlfilename = xmlfilename(end).name;
    res = tttParser([vtset.tttfilesFolder '/' vtset.results{tree}.settings.treefile(1:end-10) '/' vtset.results{tree}.settings.treefile],[vtset.results{tree}.settings.imgroot xmlfilename]);
    % gather images
    gatherimages(res,tree,'D',dchan)
    for qchan=2:numel(vtset.results{tree}.quants)
        gatherimages(res,tree,'Q',qchan)
    end
    
    % detect
    channel = dchan;
    detectCells(channel,tree,vtset.results{tree}.settings.restrictcells,'complete')
    
    % quantify
    for channel = 2:numel(vtset.results{tree}.quants)
        
        quantifyCells(channel,tree,vtset.results{tree}.settings.restrictcells,'complete')
    end
end


%% all together for adding nucmem quant




% trackset = CreateNewTracksettings;


for tree=39:numel(vtset.results)
    
    
    tree
    
    
    wl = 'w2.tif';
    channel = numel(vtset.results{tree}.detects);
    corr = 1;
    
    if vtset.results{tree}.settings.moviestart == 0
        vtset.results{tree}.settings.moviestart = getmoviestart(vtset.movieFolder);
    end
    
    vtset.results{tree}.quants(end+1).settings.wl = wl;
    % find the correct detection channel
    vtset.results{tree}.quants(end).settings.detectchannel = channel;
    vtset.results{tree}.quants(end).settings.normalize = corr;
    
    qchan = numel(vtset.results{tree}.quants);
    
    
    xmlfilename = dir([vtset.movieFolder '/*xml']);
    xmlfilename = xmlfilename(end).name;
    res = tttParser([vtset.tttfilesFolder '/' vtset.results{tree}.settings.treefile(1:end-10) '/' vtset.results{tree}.settings.treefile],[vtset.results{tree}.settings.imgroot xmlfilename]);
    % gather images
    gatherimages(res,tree,'Q',qchan)
    
    % copy detectchannel from already qtfied quant channel to new quant channel
    
    chfrom = qchan-1;
    chto = qchan;
    for id = 1:numel(vtset.results{tree}.quants(chfrom).cellNr)
        c  = vtset.results{tree}.quants(chfrom).cellNr(id);
        tp  = vtset.results{tree}.quants(chfrom).timepoint(id);
        detectchan  = vtset.results{tree}.quants(chfrom).detectchannel(id);
        vtset.results{tree}.quants(chto).detectchannel(vtset.results{tree}.quants(chto).cellNr == c  & vtset.results{tree}.quants(chto).timepoint == tp) = detectchan;
    end
    
    
    % quantify
    
    quantifyCells(qchan,tree,vtset.results{tree}.settings.restrictcells,'complete')
    
end

%% clean up wrong channel

for t= 1:numel(vtset.results)
    vtset.results{t}.detects(2:end) = [];
    vtset.results{t}.quants(2:end) = [];
end
    