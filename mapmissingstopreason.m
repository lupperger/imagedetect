function results=mapmissingstopreason(results)
global vtset


xmlfilename = dir([vtset.movierootFolder '/*xml']);
xmlfilename = xmlfilename(end).name;
pos = regexp(results.settings.treefile, '(_p)(\d{3,4})', 'match');
if isempty(pos)
    warndlg('Could not find ttt file')
    return
end
wh = waitbar(0,'Mapping additional annotation');

alldash = strfind(results.settings.treefile,'_');
exp = results.settings.treefile(1:alldash(1)-1);
pos=(pos{1}(3:end));
tttfile = [vtset.tttrootFolder '/20' exp(1:2) '/' exp '/' exp '_p' pos '/' results.settings.treefile];
res = tttParser(tttfile,[vtset.movierootFolder '/' xmlfilename]);
if isempty(res)
    fprintf('Empty ttt file, or file not found. Cannot map additional information.\n')
    close(wh)
    return
end
results.nonFluor.stopReason=zeros(1,numel(results.nonFluor.cellNr));
results.nonFluor.X=zeros(1,numel(results.nonFluor.cellNr));
results.nonFluor.Y=zeros(1,numel(results.nonFluor.cellNr));
for c = 1:numel(results.nonFluor.cellNr)
    waitbar(c/numel(results.nonFluor.cellNr),wh)
    
    an = res.cellNr == results.nonFluor.cellNr(c) & res.timepoint == results.nonFluor.timepoint(c);
    if ~any(an)
        results.nonFluor.stopReason(c) = -1;
        results.nonFluor.X(c) = -1;
        results.nonFluor.Y(c) = -1;
    else
        
        results.nonFluor.stopReason(c) = res.stopReason(res.cellNr == results.nonFluor.cellNr(c) & res.timepoint == results.nonFluor.timepoint(c));
        results.nonFluor.X(c) = res.X(res.cellNr == results.nonFluor.cellNr(c) & res.timepoint == results.nonFluor.timepoint(c));
        results.nonFluor.Y(c) = res.Y(res.cellNr == results.nonFluor.cellNr(c) & res.timepoint == results.nonFluor.timepoint(c));
    end
end
close(wh)


