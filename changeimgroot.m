function res = changeimgroot(results, newroot)

%change all filenames
for tree= 1:numel(results.quants)
    for cell=1:numel(results.quants{tree}.cellNr)

        oldroot=results.quants{tree}.filename{cell};
        % find main root
        ileft = findstr([results.settings.exp '_'], oldroot);
        
        oldroot(1:ileft(1)-2)=[];
        
        modroot=[newroot oldroot];
        
        modroot=strrep(modroot,'//','/');
        results.quants{tree}.filename{cell}=modroot;
    end
end

%change all filenames
for tree= 1:numel(results.nonFluor)
    for cell=1:numel(results.nonFluor{tree}.cellNr)

        oldroot=results.nonFluor{tree}.filename{cell};
        % find main root
        ileft = findstr([results.settings.exp '_'], oldroot);
        
        oldroot(1:ileft(1)-2)=[];
        
        modroot=[newroot oldroot];
        
        modroot=strrep(modroot,'//','/');
        
        results.nonFluor{tree}.filename{cell}=modroot;
    end
end

%change all filenames
for tree= 1:numel(results.detects)
    for cell=1:numel(results.detects{tree}.cellNr)

        oldroot=results.detects{tree}.filename{cell};
        % find main root
        ileft = findstr([results.settings.exp '_'], oldroot);
        
        oldroot(1:ileft(1)-2)=[];
        
        modroot=[newroot oldroot];
        
        modroot=strrep(modroot,'//','/');
        
        results.detects{tree}.filename{cell}=modroot;
    end
end


% change settings

results.settings.imgroot=newroot;

res=results;