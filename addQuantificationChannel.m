function varargout = addQuantificationChannel(varargin)
% ADDQUANTIFICATIONCHANNEL MATLAB code for addQuantificationChannel.fig
%      ADDQUANTIFICATIONCHANNEL, by itself, creates a new ADDQUANTIFICATIONCHANNEL or raises the existing
%      singleton*.
%
%      H = ADDQUANTIFICATIONCHANNEL returns the handle to a new ADDQUANTIFICATIONCHANNEL or the handle to
%      the existing singleton*.
%
%      ADDQUANTIFICATIONCHANNEL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ADDQUANTIFICATIONCHANNEL.M with the given input arguments.
%
%      ADDQUANTIFICATIONCHANNEL('Property','Value',...) creates a new ADDQUANTIFICATIONCHANNEL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before addQuantificationChannel_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to addQuantificationChannel_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help addQuantificationChannel

% Last Modified by GUIDE v2.5 11-Apr-2014 12:40:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @addQuantificationChannel_OpeningFcn, ...
                   'gui_OutputFcn',  @addQuantificationChannel_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before addQuantificationChannel is made visible.
function addQuantificationChannel_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to addQuantificationChannel (see VARARGIN)

% Choose default command line output for addQuantificationChannel
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global vtset


% fill tree list
treelist={};
for t = 1:numel(vtset.results)
    treelist{end+1} = vtset.results{t}.settings.treefile;
end
set(handles.listbox1,'String',treelist)

% get active detect chans
dlist={};
for d= 1:numel(vtset.results{vtset.treenum}.detects)
    dlist{end+1} = [vtset.results{vtset.treenum}.detects(d).settings.wl ' | ' vtset.results{vtset.treenum}.detects(d).settings.trackset.threshmethod];
end

% get possible wavelengths
xmlfilename = dir([vtset.movieFolder '*xml']);
wllist ={};
%%%% COPIED FROM CreateNewTrackset.m
if numel(xmlfilename)>0 %exist([vtset.movieFolder 'TATexp.xml'],'file')
    xmlfile = fileread(strcat(vtset.movieFolder, xmlfilename(end).name));
    
    
    
    xmlstr =  xml_parseany(xmlfile);
    
    list = xmlstr.WavelengthData{1}.WavelengthInformation;
    for k = 1:size(list,2)
        wllist{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name) '.' char(list{k}.WLInfo{1}.ATTRIBUTE.ImageType)];
    end
    
    %%% check if an image exists with this wavelength
    firstdir = dir(vtset.movieFolder);
    x=[firstdir.isdir];
    k = find(x(3:end),1,'first');
    firstdir = firstdir(k+2).name;
    
    if numel(firstdir(strfind(firstdir,'_p')+2:end)) == 4
        fourdigitpos=1;
    else
        fourdigitpos = 0;
    end
    
    if fourdigitpos
        tocheck=[vtset.movieFolder '/' firstdir '/*' wllist{1}];
    else
        tocheck=[vtset.movieFolder '/' firstdir '/*' wllist{1}];
    end
    d= dir(tocheck);
    if numel(d)==0
        for k = 1:numel(wllist)
            wllist{k}(2) = [];
        end
    end
else
    warndlg('XMLfile not found.')
end
% ask for detect chan and quant WL

set(handles.popupmenuQuant,'String',wllist)
set(handles.popupmenuDetect,'String',dlist)

% UIWAIT makes addQuantificationChannel wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = addQuantificationChannel_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;


% --- Executes on selection change in popupmenuDetect.
function popupmenuDetect_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuDetect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuDetect contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuDetect


% --- Executes during object creation, after setting all properties.
function popupmenuDetect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuDetect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenuQuant.
function popupmenuQuant_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenuQuant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenuQuant contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenuQuant


% --- Executes during object creation, after setting all properties.
function popupmenuQuant_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenuQuant (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbuttonOK.
function pushbuttonOK_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset
% read ttt file & gather images 
dchan = get(handles.popupmenuDetect,'value');
dchanstring = get(handles.popupmenuDetect,'string');
if iscell(dchanstring)
    dchanstring = dchanstring{dchan};
end
normalize = 1;

% create quants
wh = waitbar(0,'Adding new channel');
try
    % get selected trees
    selected = get(handles.listbox1,'value');
    
    for tree=selected;
        waitbar(0,wh,sprintf('Adding channel to tree %d of %d',find(tree == selected),numel(selected)));
        
        wl = get(handles.popupmenuQuant,'string');
        wl = wl{get(handles.popupmenuQuant,'value')};
        
        % check if segmentation channel exists
%         dlist={};
        found= 0 ;
        for d= 1:numel(vtset.results{tree}.detects)
            found = strcmp(dchanstring,[vtset.results{tree}.detects(d).settings.wl ' | ' vtset.results{tree}.detects(d).settings.trackset.threshmethod]);
            if found
                dchan = d;
                break
            end
        end
        
        if ~found
            warndlg(sprintf('Detection channel %s not found for tree %s',wl,vtset.results{tree}.settings.treefile))
            continue
        end
                
        qchan = numel(vtset.results{tree}.quants)+1;
        
        
        vtset.results{tree}.quants(end+1).settings.wl = wl;
        % find the correct detection channel
        vtset.results{tree}.quants(end).settings.detectchannel = dchan;
        vtset.results{tree}.quants(end).settings.normalize = normalize;
        
        xmlfilename = dir([vtset.movieFolder '/*xml']);
        xmlfilename = xmlfilename(end).name;
        [~,pos]=fileparts(vtset.results{tree}.settings.treeroot);
        res = tttParser([vtset.tttfilesFolder '/' pos '/' vtset.results{tree}.settings.treefile],[vtset.results{tree}.settings.imgroot xmlfilename]);
        % gather images
        
        gatherimages(res,tree,'Q',qchan)
        
        % quantify
        quantifyCells(qchan,tree,vtset.results{tree}.settings.restrictcells,'complete')
        
        % set to unsaved
        vtset.unsaved(tree) = 1;
    end
vtset.functions.lstTrees_Callback();

delete(wh)
delete(handles.figure1)
catch e
    delete(wh)
    errordlg('Could not add new quantification channel. Please do not save the file and close QTFy. Please report error including errorlog.')
end


% --- Executes on button press in pushbuttonCancel.
function pushbuttonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(handles.figure1)


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
