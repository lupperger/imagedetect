%% img


%% detect centroides


%% grab detects from SnapQuant
global cellcoord

coords=cellcoord.tracking_coords;
% select coords in ROI
if isfield(cellcoord,'impoly') && cellcoord.impoly.isvalid
    xiyi = cellcoord.impoly.getPosition;
    in = inpolygon(coords(:,1),coords(:,2),xiyi(:,1),xiyi(:,2));
    coords= coords(in,:);
end


%% create cell boundaries for shedding



mseroptions = siq.mseroptions;

[mserBW,mserProps,mserHierachy] = mserSegmentation(medfilt2(siq.normDivided),mseroptions);
figure;imagesc(mserBW)
hold on
plot(coords(:,1),coords(:,2),'ow')
%% shedden
I_max = zeros(size(siq.normDivided));
for c =1:numel(coords(:,1))
    
    I_max(round(coords(c,2)),round(coords(c,1))) = 1;
end
figure;imagesc(I_max)
%%
% function [L,bw4] = philMarkerWatershedding(I,bw,bw2)
   
%    I_f = medfilt2(I_c,[3 3]);
%    I_max = imextendedmax(I,25);
%    I_rm_min = bwareaopen(I_max,15);
%    I_max(I_rm_min) = 0;
%    I_inv = imcomplement(mserHierachy);
%    I_inv = imcomplement(siq.normDivided);
    Overlaid = imimposemin(1 - mserHierachy,I_max);

%    I_mod = imimposemin(I_inv, ~mserBW | I_max);
   figure;imagesc(Overlaid)
   
   
   %%
%    I_mod = bw;
%    I_mod(I_max) = 0;

WatershedBoundaries = watershed(Overlaid) > 0;
figure;imagesc(WatershedBoundaries)
Objects = mserBW.*WatershedBoundaries;

%    L = watershed(I_mod);
   figure;imagesc(Objects)
   
   %% filter watershed image, keep only objects with object marker
L = bwlabel(Objects);
for i = 1:max(L(:))
    ids = find(L == i);
    if ~any(I_max(ids))
        L(L==i)= 0;
    end
end
  figure;
  imagesc(L)
   
%% show results
L(L<1) = 0;
L(L>=1) = 1;
out = bwperim(L);
Iout = siq.normDivided;
Iout(out) = max(Iout(:));

figure
imagesc(Iout)
   
   
% end
