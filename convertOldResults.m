% THIS CODE REQUIRES THE VARIABLE 'results'

% copy stuff
conv.quants = results.quants;
conv.detects = results.detects;
conv.nonFluor = results.nonFluor;
conv.trackset = results.trackset;
% settings struct
conv.settings.imgroot = results.imgroot;
conv.settings.exp = results.exp;
conv.settings.extDetect = results.extDetect;
conv.settings.extQuant = results.extQuant;
conv.settings.extNonFluor = results.extNonFluor;
conv.settings.corrImage = results.corrImage;
conv.settings.corrDetect = results.corrDetect;
conv.settings.corrQuant = results.corrQuant;
% get treefiles
conv.treefiles = {};
for i=1:numel(results.quants)
    conv.treefiles{i} = results.quants{i}.treefile;
end