%% settings adam 090415AF3
clear settings
settings.treeroot = '/home/michi/hemato_ext/data/090415AF3/trees';
settings.imgroot = '/home/michi/hemato_ext/data/090415AF3';
settings.exp = '090415AF3';
settings.extDetect = 'w1.jpg';
settings.extQuant = 'w2.tif';
settings.extNonFluor = 'w0.jpg';
settings.debugOut='';
settings.corrImage = [];
settings.corrDetect = 0;
settings.corrQuant= 0;
settings.anno = 'wavelength_3';


clear trackset
trackset.win=41;
trackset.threshcorr=1;
trackset.smooth=4;
trackset.max=4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest=0;
trackset.threshfix=0;
trackset.threshmin=0;
trackset.threshmethod='otsu global';
trackset.tp2min=2;

%% adam 090415AF3, filtering
clear filters
%filters = struct('tree', '', 'cell', '', 'tp', '');
filters.tree = {};
filters.cell = [];
filters.tp = [];
filters.tree{end+1} = '008-001';  filters.cell(end+1) = 5;   filters.tp(end+1) = 517;  
filters.tree{end+1} = '009-001';  filters.cell(end+1) = 3;   filters.tp(end+1) = 181;  
filters.tree{end+1} = '009-001';  filters.cell(end+1) = 4;   filters.tp(end+1) = 337;  
filters.tree{end+1} = '009-001';  filters.cell(end+1) = 9;   filters.tp(end+1) = 469;  
filters.tree{end+1} = '009-001';  filters.cell(end+1) = 9;   filters.tp(end+1) = 517;  
filters.tree{end+1} = '008-003';  filters.cell(end+1) = 2;   filters.tp(end+1) = 229;  
filters.tree{end+1} = '008-003';  filters.cell(end+1) = 5;   filters.tp(end+1) = 373;  
filters.tree{end+1} = '008-003';  filters.cell(end+1) = 6;   filters.tp(end+1) = 457;  
filters.tree{end+1} = '008-003';  filters.cell(end+1) = 9;   filters.tp(end+1) = 541;  
filters.tree{end+1} = '008-003';  filters.cell(end+1) = 10;   filters.tp(end+1) = 397;  
filters.tree{end+1} = '008-006';  filters.cell(end+1) = 3;   filters.tp(end+1) = 181;  

out=filterQuants(results.quants, filters);

%% settings phil 090528PH4
clear settings
settings.treeroot = '/home/michi/data/retracked/090528PH4';
settings.imgroot = '~/hemato_ext/data/090528PH4';
settings.exp = '090528PH4';
settings.extDetect = 'w1.tif';
settings.extQuant = 'w1.tif';
settings.extNonFluor = 'w0.jpg';
settings.debugOut='';%/home/jan/scratch/philouttest';
corrImage =double(imread('~/macsrvbackup/data/normalize_with5.png'));
settings.corrImage = corrImage / max(corrImage(:));
settings.corrDetect = 0;
settings.corrQuant = 0;
settings.anno = 'wavelength_3';

clear trackset
trackset.win=51;
trackset.threshcorr=1.3;
trackset.smooth=4;
trackset.max=4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest=1;
trackset.threshfix=0;
trackset.threshmin=0;
trackset.threshmethod='otsu global';
trackset.tp2min=2;


%% settings phil 090901PH2
clear settings
settings.treeroot = '/home/michi/data/retracked/090901PH2';
settings.imgroot = '~/macsrvbackup/data//090901PH2';
settings.exp = '090901PH2';
settings.extDetect = 'w1.tif';
settings.extQuant = 'w1.tif';
settings.extNonFluor = 'w0.jpg';
settings.debugOut='';%/home/jan/scratch/philouttest';
corrImage = double(imread('~/hemato_ext/data/normalize/090901.png'));
settings.corrImage = corrImage / max(corrImage(:));
settings.corrDetect = 1;
settings.corrQuant = 1;
settings.anno = 'wavelength_3';

clear trackset
trackset.win=51;
trackset.threshcorr=1.3;
trackset.smooth=4;
trackset.max=4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest=1;
trackset.threshfix=0;
trackset.threshmin=0;
trackset.threshmethod='otsu global';
trackset.tp2min=2;

%% settings phil 090907PH2
clear settings
settings.treeroot = '/local/home/hoppe/Quant/090907PH2';
settings.imgroot = '/local/backup/schwarzfischer/data/090907PH2';
settings.exp = '090907PH2';
settings.extDetect = 'w1.tif';
settings.extQuant = 'w1.tif';
settings.extNonFluor = 'w0.jpg';
settings.debugOut='';%/home/jan/scratch/philouttest';
corrImage = double(imread('~/hemato/data/normalize/090907.png'));
settings.corrImage = corrImage / max(corrImage(:));
settings.corrDetect = 1;
settings.corrQuant = 1;
settings.anno = 'wavelength_3';

clear trackset
trackset.win=51;
trackset.threshcorr=1.3;
trackset.smooth=4;
trackset.max=4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest=1;
trackset.threshfix=0;
trackset.threshmin=0;
trackset.threshmethod='otsu global';
trackset.tp2min=2;

%% settings phil 090901PH2 corrected
clear settings
settings.treeroot = '/media/disk/data/090901PH2_trees_corrected';
settings.imgroot = '/media/disk/data/090901PH2';
settings.exp = '090901PH2';
settings.extDetect = 'w1.tif';
settings.extQuant = 'w1.tif';
settings.extNonFluor = 'w0.jpg';
settings.debugOut='';%/home/jan/scratch/philouttest';
corrImage = double(imread('~/hemato_ext/data/normalize/090901.png'));
settings.corrImage = corrImage / max(corrImage(:));
settings.corrDetect = 1;
settings.corrQuant = 1;
settings.anno = 'wavelength_3';
settings.tp2min=2;

clear trackset
trackset.win=51;
trackset.threshcorr=1.3;
trackset.smooth=4;
trackset.max=4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest=1;
trackset.threshfix=0;
trackset.threshmin=0;
trackset.threshmethod='otsu global';



%% settings flo/carsten mopped
clear settings
settings.treeroot = '/local/backup/schwarzfischer/data/060817_trees/060817_p001-p004';
% settings.treeroot = '/media/disk/data/060817_trees/060817_p024-p026';
settings.imgroot = '/local/backup/schwarzfischer/data/060817';
% settings.imgroot = '/media/disk/data/060817';
settings.exp = '060817';
settings.extDetect = 'w0.jpg';
settings.extQuant = 'w0.jpg';
settings.extNonFluor = 'w0.jpg';
settings.debugOut='';%/home/jan/scratch/philouttest';
% corrImage = double(imread('~/hemato/data/normalize/090901.png'));
settings.corrImage = 1;%corrImage / max(corrImage(:));
settings.corrDetect = 0;
settings.corrQuant = 0;
settings.anno = '';
settings.tp2min=2;

clear trackset
trackset.win=60;
trackset.threshcorr=1;
trackset.smooth=4;
trackset.max=4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest=1;
trackset.threshfix=0;
trackset.threshmin=0;
trackset.threshmethod='otsu global';


%% settings phil shorter time interval

% 3min = w1 p001
% 6min = w2 p010
%12min = w3 p019
%21min = w4 p028

clear settings
% /home/michi/data/hoppe/100104PH4_p001 alle 3 min
settings.treeroot = '/home/michi/data/hoppe/trees/21 min Intervall';
settings.imgroot = '/home/michi/data/hoppe/100104PH4_p028 alle 21 min';
settings.exp = '100104PH4';
settings.extDetect = 'w4.tif';
settings.extQuant = 'w4.tif';
settings.extNonFluor = 'w0.jpg';
settings.debugOut='';%/home/jan/scratch/philouttest';
corrImage = double(imread('/home/michi/data/hoppe/100104PH4_p046 Fluoresceinloesung/100104PH4_p046_t00465_w1.tif'));
settings.corrImage = corrImage / max(corrImage(:));
settings.corrDetect = 1;
settings.corrQuant = 1;
settings.anno = 'wavelength_3';
settings.tp2min=2;
settings.version=1507;


clear trackset
trackset.win=51;
trackset.threshcorr=0.7;
trackset.smooth=4;
trackset.max=4;
trackset.clumped = 'Shape';
trackset.dividing = 'Distance';
trackset.usenearest=1;
trackset.threshfix=0;
trackset.threshmin=0;
trackset.threshmethod='otsu global';

