% AMT function to detect cells in the detect chennel
%%%% needs channel number index and number of cells/timepoints
%
%%%% writes all cellmasks and sizes, maybe some intelligent autocorrect stuff (adjust trackset),
%%%% this would need quantifications ... ?
function detectCellsparfor(channelIndex,tree,cells,range)

global vtset

% restrict cells/range
if strcmp(cells,'all')
    cells = unique(vtset.results{tree}.detects(channelIndex).cellNr);
end
if strcmp(range,'complete')
    range = unique(vtset.results{tree}.detects(channelIndex).timepoint);
end

if strcmp(cells,'Only New Cells')
    indizes = range;
else
    indizes = find(ismember(vtset.results{tree}.detects(channelIndex).cellNr,cells) & ismember(vtset.results{tree}.detects(channelIndex).timepoint,range));
end

%%% TODO optimize that one timepoint/image is only used once 4 every cell
%%% on it

allactive =vtset.results{tree}.detects(channelIndex).active;
quants = vtset.results{tree}.quants;
detects = vtset.results{tree}.detects;
allX = vtset.results{tree}.detects(channelIndex).X;
allY=vtset.results{tree}.detects(channelIndex).Y;
imgroot=vtset.results{tree}.settings.imgroot;
allcellmask = cell(numel(indizes),1);
alltrackset =cell(numel(indizes),1);
allsize=zeros(numel(indizes),1);
allperimeter =zeros(numel(indizes),1);
alltimepointsD =detects(channelIndex).timepoint;
allCellNr = detects(channelIndex).cellNr;
allfilenames = detects(channelIndex).filename;
settings= detects(channelIndex).settings;
normalize = settings.normalize;
wl = settings.wl(1:2);
tracksetini= settings.trackset;

tic
parfor ixx = 1:numel(indizes)
%     i = indizes(ixx);
    % update waitbar
%     waitbar(find(indizes==i)/numel(indizes))
    
    
    % check if any quant image exists
    found = 0;
    for ch = 1:numel(quants)
        found = found | any(quants(ch).timepoint == alltimepointsD(ixx) & quants(ch).cellNr == allCellNr(ixx));
    end
        
    % skip inactive cells
    if found && allactive(ixx)
        
        % x and y
        %%% TODO check all -1!!
        x = round(allX(ixx));
        y = round(allY(ixx));
        
        if x<0 || y<0
            warnstring=sprintf('Negative or out of bound index! \nSkipping cell %d timepoint %d \nX = %d Y = %d\n',allCellNr(ixx),alltimepointsD(ixx),x,y);
            fprintf(warnstring);
            warndlg(warnstring,'Index out of bound');
            continue;
        end
        
        % load image & normalize
        image = loadimage([imgroot allfilenames{ixx}], normalize, wl);
        
        
        %%%% SEGMENTATION
        trackset=tracksetini;
        
        Center = cellSegmentation(image,x,y,trackset);
        
        % optimize segmentation
        if sum(Center(:))<trackset.MinArea || sum(Center(:))>trackset.MaxArea && ~strcmp(trackset.threshmethod,'Ellipse')
            [Center, trackset]=optimizeSegmentation(Center,image,x,y,trackset);
        end
    else
        fprintf('Skipping cell segmentation timepoint %d cell %d; no relevant quantification found.\n',alltimepointsD(ixx),allCellNr(ixx));
        Center = 0;
        trackset=tracksetini;
    end
    
    % finally store size & cellmask & perimeter
    allcellmask{ixx} = Center;
    allsize(ixx) = sum(Center(:));
    allperimeter(ixx) = sum(sum(bwperim(Center)));
    alltrackset{ixx} = trackset;
    
%     if numel(alltrackset) == 0
%         alltrackset(ixx) = trackset;
%         continue
%     end
    
%     if isStructEqual(alltrackset(ixx) , trackset)
%         alltrackset(ixx) = trackset;
%     else
%         for f = fieldnames(trackset)'
%             alltrackset(ixx).(cell2mat(f)) = trackset.(cell2mat(f));
%         end
%     end

end
toc

% 4 worker : 71.7s
% 6 worker : 54.6s
% 8 worker : 46.4s

%%%  and backmap calced params
for ixx = 1:numel(indizes)
    i = indizes(ixx);
    vtset.results{tree}.detects(channelIndex).trackset(i) = alltrackset{ixx};
    vtset.results{tree}.detects(channelIndex).cellmask{i} = allcellmask{ixx};
    vtset.results{tree}.detects(channelIndex).size(i) = allsize(ixx);
    vtset.results{tree}.detects(channelIndex).perimeter(i) = allperimeter(ixx);
    
end