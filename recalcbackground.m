%% enter position and timepoints and wl

wl='w2.png';
pos = 11;
timepoint = 1953:1983;
imgroot= vtset.results{vtset.treenum}.settings.imgroot;
exp = vtset.results{vtset.treenum}.settings.exp;
for tp = timepoint
    
    % if exist filename
    % Z:\TTT//120125PH5/background/120125PH5_p0012/120125PH5_p0012_t02238_z001_w1.png
    filename = sprintf('%s/%s_p%04d/%s_p%04d_t%05d_z001_%s',imgroot,exp,pos,exp,pos,tp,wl);
    if exist(filename,'file')
        %do bg
        img = loadimage(filename,0);
        bg = backgroundestimation_new(img);
        
        % store
        imagesc(bg)
        drawnow
        filenamesave = sprintf('%s/background/%s_p%04d/%s_p%04d_t%05d_z001_%s',imgroot,exp,pos,exp,pos,tp,wl);
        fprintf('saving... %s\n',filenamesave)
        imwrite(bg, filenamesave,'png','Bitdepth',16);

    end
end