function results = mapmissingannotationLocal(results,anno)
global vtset 


wh = waitbar(0,'Mapping additional annotation');

results.nonFluor.(anno)=zeros(numel(results.nonFluor.cellNr),1);

xmlfilename = dir([vtset.movierootFolder '/*xml']);
if isempty(xmlfilename)
    delete(wh)
    warndlg('Could not find xml file')
    return
end
xmlfilename = xmlfilename(end).name;
pos = regexp(results.settings.treefile, '(_p)(\d{3,4})', 'match');
if isempty(pos)
    delete(wh)
    warndlg('Could not find ttt file')
    return
end

alldash = strfind(results.settings.treefile,'_');
exp = results.settings.treefile(1:alldash(1)-1);
pos=(pos{1}(3:end));
tttfile = [vtset.tttrootFolder '/20' exp(1:2) '/' exp '/' exp '_p' pos '/' results.settings.treefile];
res = tttParser(tttfile,[vtset.movierootFolder '/' xmlfilename]);
if isempty(res)
fprintf('Empty ttt file, or file not found. Cannot map additional information.\n')
    close(wh)
    return
end

for c = unique(results.nonFluor.cellNr)
    waitbar(c/numel(unique(results.nonFluor.cellNr)),wh)
    an = res.(anno)(res.cellNr == c);
    tp = res.timepoint(res.cellNr == c);
    [~,order] = sort(tp);
    an = an(order);
    fltps = results.nonFluor.timepoint(results.nonFluor.cellNr == c);
    [~,order]=sort(fltps);
    [~,order ]= sort(order);
    an = an(order);
%     if numel(an)~=1
%         fprintf('Numel of cell %d = %d vs struct: %d\n',c ,numel(an),numel(fltps))
%         vtset.results{t}.nonFluor.(anno)(vtset.results{t}.nonFluor.cellNr == c) = 0;
%         continue;
%     end
    results.nonFluor.(anno)(results.nonFluor.cellNr == c) = an;
end

close(wh)

