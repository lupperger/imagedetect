function [I,bw,L,overlay,bw2]=segmentImage_mser(varargin)

I = varargin{1};
% Get image size
imSize = size(I);

% MSER parameters
if numel(varargin)>1
    settings=varargin{2};
    Delta=settings.Delta;
    MinDiversity=settings.MinDiversity;
    MaxVariation=settings.MaxVariation;
    MaxArea=settings.MaxArea;    % 500 px (so we do not miss cells sticking together)
    MinArea=settings.MinArea;     % 15 px -> unter 15 viel dreck (aber bereits FN in PH_dirt)
%     Diamond = settings.Diamond;
    centerX = varargin{3};
    centerY = varargin{4};
else
    Delta=1;
    MinDiversity=0.001;
    MaxVariation=1;
    MaxArea=800;    % 500 px (so we do not miss cells sticking together)
    MinArea=20;     % 15 px -> unter 15 viel dreck (aber bereits FN in PH_dirt)
    % -> ab 30 FN in DL -> min cell
%     Diamond = 2;
    centerX = size(I,2)/2;
    centerY = size(I,1)/2;
end
% size has to be set at beginning
% Delta=delta;


% MSER
% regionSeeds = vl_mser(I,'MinDiversity',MinDiversity,'MaxVariation',MaxVariation,'Delta',Delta,'MaxArea',MaxArea, 'MinArea', MinArea,'darkonbright',1);

% Linear MSER
% regionSeeds = linearMser(I,'MinDiversity',MinDiversity,'MaxVariation',MaxVariation,'Delta',Delta,'MaxArea',MaxArea, 'MinArea', MinArea,'darkonbright',1);
regionSeeds = linearMser(I, Delta, MinArea, MaxArea,MaxVariation,false);

% Create bw image
bw = zeros(imSize);

% Iterate over all regions
for regionSeed=regionSeeds'
%     if ismember((sub2ind(size(bw),y,x)),regionSeed{1})
        bw(regionSeed{1}) = bw(regionSeed{1})+1;
%     end
end

L = bw;
% bw=imfill(bw,'holes');
% imagesc(bw)


% bw= imclose(bw,strel('diamond',Diamond));
% [L,bw2,overlay] = applyWatershedding(I,bw);
% [L,bw2] = applyMarkerWatershedding(I,bw);
% overlay =imoverlay(double(I),bwperim(bw2));
% overlay = 0;
% imagesc(bw2)


% numL = max(bw(:));
% % iterate over them
% minDist=Inf;
% minCell=-1;
% nearestX=centerX;
% nearestY=centerY;
% for i=1:numL
%     [r,c] = find(bw==i);
%     % distance to center
%     dist=norm([centerX centerY] -[mean(c) mean(r)]);
%     if dist<minDist
%         minDist=dist;
%         minCell=i;
%         nearestX=c;
%         nearestY=r;
%     end
% end
[allex alley]=find(bw~=0);
[~, id]=min((allex'-centerX).^2 + (alley'-centerY).^2);

nearestX = allex(id);
nearestY = alley(id);

bw = false(imSize);
for regionSeed=regionSeeds'
    if ismember(sub2ind(imSize,nearestX,nearestY),regionSeed{1})
        bw(regionSeed{1}) = 1;
    end
end



function dummy
%%