function vec=tocolumn(vec)
if ~iscolumn(vec)
    vec=vec';
end