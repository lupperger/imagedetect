function varargout = MserSettings(varargin)
% MSERSETTINGS MATLAB code for MserSettings.fig
%      MSERSETTINGS, by itself, creates a new MSERSETTINGS or raises the existing
%      singleton*.
%
%      H = MSERSETTINGS returns the handle to a new MSERSETTINGS or the handle to
%      the existing singleton*.
%
%      MSERSETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MSERSETTINGS.M with the given input arguments.
%
%      MSERSETTINGS('Property','Value',...) creates a new MSERSETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MserSettings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MserSettings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MserSettings

% Last Modified by GUIDE v2.5 02-Dec-2011 12:42:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MserSettings_OpeningFcn, ...
                   'gui_OutputFcn',  @MserSettings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MserSettings is made visible.
function MserSettings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MserSettings (see VARARGIN)

% Choose default command line output for MserSettings
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%%% init slider:

if numel(varargin)
    options = varargin{1};
else
    options.maxVariation = 1;
    options.minSize = 15;
    options.maxSize = 300;
    options.delta = 1;
    options.invert = 0;
end

set(handles.sliderMaxArea,'Max',1500)
set(handles.sliderMaxArea,'Min',0)
set(handles.sliderMaxArea,'SliderStep',[10 50]/1500)
set(handles.sliderMaxArea,'Value',options.maxSize)
set(handles.editMaxArea,'String',num2str(options.maxSize));

set(handles.sliderMinArea,'Max',800)
set(handles.sliderMinArea,'Min',0)
set(handles.sliderMinArea,'SliderStep',[10 50]/800)
set(handles.sliderMinArea,'Value',options.minSize)
set(handles.editMinArea,'String',num2str(options.minSize));

set(handles.sliderDelta,'Max',100)
set(handles.sliderDelta,'Min',0)
set(handles.sliderDelta,'SliderStep',[2 5]/100)
set(handles.sliderDelta,'Value',options.delta)
set(handles.editDelta,'String',num2str(options.delta));

set(handles.sliderMaxVar,'Max',10)
set(handles.sliderMaxVar,'Min',0)
set(handles.sliderMaxVar,'SliderStep',[1 2]/10)
set(handles.sliderMaxVar,'Value',options.maxVariation)
set(handles.editMaxVar,'String',num2str(options.maxVariation));


set(handles.checkboxInvert,'value',options.invert)
% UIWAIT makes MserSettings wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MserSettings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global msersettings
% Get default command line output from handles structure
% varargout{1} = handles.output;



varargout{1}= msersettings.options;

% --- Executes on slider movement.
function sliderDelta_Callback(hObject, eventdata, handles)
% hObject    handle to sliderDelta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.editDelta,'String',num2str(round(get(hObject,'Value'))))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderDelta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderDelta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderMinArea_Callback(hObject, eventdata, handles)
% hObject    handle to sliderMinArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.editMinArea,'String',num2str(round(get(hObject,'Value'))))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderMinArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderMinArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderMaxArea_Callback(hObject, eventdata, handles)
% hObject    handle to sliderMaxArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.editMaxArea,'String',num2str(round(get(hObject,'Value'))))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function sliderMaxArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderMaxArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function sliderMaxVar_Callback(hObject, eventdata, handles)
% hObject    handle to sliderMaxVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.editMaxVar,'String',num2str(round(get(hObject,'Value'))))

% --- Executes during object creation, after setting all properties.
function sliderMaxVar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderMaxVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editMaxVar_Callback(hObject, eventdata, handles)
% hObject    handle to editMaxVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMaxVar as text
%        str2double(get(hObject,'String')) returns contents of editMaxVar as a double


% --- Executes during object creation, after setting all properties.
function editMaxVar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMaxVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMaxArea_Callback(hObject, eventdata, handles)
% hObject    handle to editMaxArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMaxArea as text
%        str2double(get(hObject,'String')) returns contents of editMaxArea as a double


% --- Executes during object creation, after setting all properties.
function editMaxArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMaxArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editMinArea_Callback(hObject, eventdata, handles)
% hObject    handle to editMinArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editMinArea as text
%        str2double(get(hObject,'String')) returns contents of editMinArea as a double


% --- Executes during object creation, after setting all properties.
function editMinArea_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editMinArea (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDelta_Callback(hObject, eventdata, handles)
% hObject    handle to editDelta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDelta as text
%        str2double(get(hObject,'String')) returns contents of editDelta as a double


% --- Executes during object creation, after setting all properties.
function editDelta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDelta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonOK.
function ButtonOK_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global msersettings
options.delta = str2double(get(handles.editDelta,'String'));
options.maxVariation = str2double(get(handles.editMaxVar,'String'));
options.minSize = str2double(get(handles.editMinArea,'String'));
options.maxSize = str2double(get(handles.editMaxArea,'String'));
options.invert = get(handles.checkboxInvert,'Value');

msersettings.options = options;

close(handles.figure1);


% --- Executes on button press in ButtonCancel.
function ButtonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global msersettings
msersettings.options=[];

close(handles.figure1);


% --- Executes on button press in checkboxInvert.
function checkboxInvert_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxInvert (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxInvert
