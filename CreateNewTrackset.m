function varargout = CreateNewTrackset(varargin)
% CREATENEWTRACKSET M-file for CreateNewTrackset.fig
%      CREATENEWTRACKSET, by itself, creates a new CREATENEWTRACKSET or raises the existing
%      singleton*.
%
%      H = CREATENEWTRACKSET returns the handle to a new CREATENEWTRACKSET or the handle to
%      the existing singleton*.
%
%      CREATENEWTRACKSET('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATENEWTRACKSET.M with the given input arguments.
%
%      CREATENEWTRACKSET('Property','Value',...) creates a new CREATENEWTRACKSET or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CreateNewTrackset_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CreateNewTrackset_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CreateNewTrackset

% Last Modified by GUIDE v2.5 27-Feb-2014 14:15:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CreateNewTrackset_OpeningFcn, ...
                   'gui_OutputFcn',  @CreateNewTrackset_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CreateNewTrackset is made visible.
function CreateNewTrackset_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CreateNewTrackset (see VARARGIN)

% Choose default command line output for CreateNewTrackset
global vtset cntsettings filesettings
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

if numel(varargin)>0
    settings=varargin{1};
    setvariables(settings, handles);
else
    
    % init
    cntsettings = struct('settings',{},'detect',{},'quant',{},'externalSegmentation',{});
    
%     d= dir(vtset.movierootFolder);
    d = uigetdir(vtset.movieFolder,'Select Experiment folder');

    
    if d==0
%         msgbox(sprintf('There is no experiment in %s. Disk properly mounted?',vtset.movierootFolder),'Folder not found')


    else
        vtset.movieFolder = [d '/'];
        vtset.movierootFolder = [d '/'];
        %         explist={d([d.isdir]).name};
        [~, exp]=fileparts(d);
        if isempty(exp)
            msgbox('Could not validate experiment folder.')
            return
        end
%         [exp ok]=listdlg('PromptString','Select experiment:',...
%             'SelectionMode','single',...
%             'ListSize',[200 200],...
%             'ListString',explist(1:end));


        if numel(exp)>0
            set(handles.experiment,'String',exp)
            vtset.experiment = exp;
            evnt.Key='return';
            getFolders(handles.figure1,evnt)
        end
    end
end
[imFile, ~, ext] = getAnImageFile(vtset.movieFolder,vtset.experiment);
filesettings = generateFilesettings(imFile);
name = generateFilename(get(handles.experiment,'String'), 1, 1, 1, 1, ext, filesettings);
set(handles.imageStructure, 'String', name);
set(handles.experiment,'KeyPressFcn',@getFolders); 




% UIWAIT makes CreateNewTrackset wait for user response (see UIRESUME)
uiwait(handles.figure1);



function getFolders(src,evnt)
%keyPressFcn automatically takes in two inputs
%src is the object that was active when the keypress occurred
%evnt stores the data for the key pressed
 
global treelist selectedtrees vtset

%brings in the handles structure in to the function
handles = guidata(src);
 
k= evnt.Key; %k is the key that is pressed
 
if strcmp(k,'return') %if enter was pressed
    pause(0.01) %allows time to update
    drawnow
    % search for img folder
    
    exp=get(handles.experiment,'String');
    vtset.AMTfilesFolder = [vtset.tttrootFolder '20' exp(1:2) '/' exp '/'];
    vtset.tttfilesFolder = [vtset.tttrootFolder '20' exp(1:2) '/' exp '/'];
%     vtset.movieFolder = [vtset.movierootFolder ];
    
    imgfolder=vtset.movieFolder;
    
    if exist(imgfolder, 'dir')
        set(handles.imageRoot, 'String', imgfolder);
    else 
        msgbox(sprintf('Image folder %s not found', imgfolder),'Folder not found','warn');
        return;
    end
    
    % get all experiment subfolders and list ttt_simple files
    
    [newselectedtrees restrictcells treelist]=TreeList({vtset.tttfilesFolder});
    if numel(newselectedtrees)>0
        set(handles.treequantified,'String',numel(newselectedtrees));
        set(handles.treecount,'String',numel(treelist.treefile));
        set(handles.restrictcells,'String',restrictcells);

        selectedtrees=newselectedtrees;
    end
    set(handles.treeRoot, 'String', vtset.tttfilesFolder);

    readXMLfile(handles);


end


function readXMLfile(handles)
global vtset
%%% read XML file to get wavelength information

xmlfilename = dir([vtset.movieFolder '*xml']);



try
    if numel(xmlfilename)>0 %exist([vtset.movieFolder 'TATexp.xml'],'file')
        xmlfile = fileread(strcat(vtset.movieFolder, xmlfilename(end).name));
        wllist ={};
        wllist2 ={};
        
        xmlstr =  xml_parseany(xmlfile); 
        
        list = xmlstr.WavelengthData{1}.WavelengthInformation;
        for k = 1:size(list,2)
            wllist{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name) '.' char(list{k}.WLInfo{1}.ATTRIBUTE.ImageType)];
            wllist2{end+1} = ['w' char(list{k}.WLInfo{1}.ATTRIBUTE.Name)];
        end
        
        %%% check if an image exists with this wavelength
        firstdir = dir(vtset.movieFolder);
        x=[firstdir.isdir];
        k = find(x(3:end),1,'first');
        firstdir = firstdir(k+2).name;
        
        if numel(firstdir(strfind(firstdir,'_p')+2:end)) == 4
            fourdigitpos=1;
        else
            fourdigitpos = 0;
        end
        
        if fourdigitpos
            tocheck=[vtset.movieFolder '/' firstdir '/*' wllist{1}];
        else
            tocheck=[vtset.movieFolder '/' firstdir '/*' wllist{1}];
        end
        d= dir(tocheck);
        if numel(d)==0
            for k = 1:numel(wllist)
                wllist{k}(2) = [];
                wllist2{k}(2) = [];
            end
        end


        % set all wavelength selectors to droplist
        set(handles.extDetection,'Style','popupmenu');
        set(handles.extDetection,'String',wllist);
        set(handles.extQuantification,'Style','popupmenu');
        set(handles.extQuantification,'String',wllist);
        set(handles.extNonFluor,'Style','popupmenu');
        set(handles.extNonFluor,'String',wllist);
        set(handles.annotation1,'Style','popupmenu');
        set(handles.annotation1,'String',wllist2);
        set(handles.annotation2,'Style','popupmenu');
        set(handles.annotation2,'String',wllist2);


    else
        exp=get(handles.experiment,'String');
        warndlg(sprintf('No XML file found for experiment %s',exp));
    end
catch
    % reset all wavelength selectors to edit
    set(handles.extDetection,'Style','edit');
    set(handles.extDetection,'String','');
    set(handles.extQuantification,'Style','edit');
    set(handles.extQuantification,'String','');
    set(handles.extNonFluor,'Style','edit');
    set(handles.extNonFluor,'String','');
    warndlg('XML read error, please set wavelengths manually','XML error')
end

function setvariables(settings, handles)
global vtset selectedtrees

vtset.temp.settings = settings;

%%% change most of the things to notchangeable
set(handles.experiment, 'String', num2str(settings.exp));
set(handles.experiment, 'enable', 'off');
% set(handles.extDetection, 'String', num2str(settings.extDetect));
set(handles.extDetection, 'enable', 'off');
% set(handles.extQuantification, 'String', num2str(settings.extQuant));
set(handles.extQuantification, 'enable', 'off');
% set(handles.extNonFluor, 'String', num2str(settings.extNonFluor));
set(handles.extNonFluor, 'enable', 'off');
set(handles.listbox1, 'enable', 'off');
set(handles.ButtonAdd, 'enable', 'off');
set(handles.ButtonDelete, 'enable', 'off');
set(handles.restrictcells, 'String', 'all');
set(handles.restrictcells, 'enable', 'off');
set(handles.editlist, 'enable', 'off');

%%% the things which can be changed
set(handles.treeRoot, 'String', num2str(settings.treeroot));
set(handles.imageRoot, 'String', num2str(settings.imgroot));
set(handles.corrDetection, 'Value', (settings.corrDetect));
set(handles.corrQuantification, 'Value', (settings.corrQuant));


set(handles.annotation1, 'String', num2str(settings.anno));
if isfield(settings,'anno2')
    set(handles.annotation2, 'String', num2str(settings.anno2));
else
    set(handles.annotation2, 'String', '');
end


% selectedtrees=settings.selectedtrees;
% treefiles = getFilesExt(settings.treeroot, '.ttt_simple');
% set(handles.treequantified,'String',numel(selectedtrees));
% set(handles.treecount,'String',numel(treefiles));
set(handles.treecount, 'enable', 'off');
set(handles.treequantified, 'enable', 'off');



function settings=getvariables(handles)

global selectedtrees cntsettings vtset filesettings

settings.treeroot = get(handles.treeRoot,'String');
settings.imgroot = get(handles.imageRoot,'String');
settings.exp = get(handles.experiment,'String');
% get all detect/quant channels
% non Fluor
settings.extNonFluor = get(handles.extNonFluor,'String');
if iscell(settings.extNonFluor)
    settings.extNonFluor = settings.extNonFluor{get(handles.extNonFluor,'Value')};
end



% parsing of detect/quant list not neccessary, struct will be crated in
% ButtonAdd



settings.channelsettings = cntsettings;

settings.onlyTimepoints = get(handles.checkboxOnlyTimepoints,'Value');

settings.corrDetect = get(handles.corrDetection,'Value');
settings.corrQuant= get(handles.corrQuantification,'Value');
settings.filesettings = filesettings;
settings.anno = get(handles.annotation1,'String');
if iscell(settings.anno)
    settings.anno = settings.anno{get(handles.annotation1,'Value')};
end
settings.anno2 = get(handles.annotation2,'String');
if iscell(settings.anno2)
    settings.anno2 = settings.anno2{get(handles.annotation2,'Value')};
end

% parse the restrict cells
settings.restrictcells=get(handles.restrictcells,'String');
settings.restrictcells=lower(settings.restrictcells);
if ~numel(strfind(settings.restrictcells,'all'))>0 && ~numel(strfind(settings.restrictcells,'only'))
    settings.restrictcells=strrep(settings.restrictcells, '-',':');
    settings.restrictcells=strrep(settings.restrictcells, ',',' ');
    settings.restrictcells=['[' settings.restrictcells ']' ];
    settings.restrictcells=eval(settings.restrictcells);
end

if isfield(vtset,'temp') && isfield(vtset.temp,'settings') && isfield(vtset.temp.settings,'treefile')
    settings.treefile = vtset.temp.settings.treefile;
    vtset.temp = rmfield(vtset.temp,'settings');
end



settings.selectedtrees=selectedtrees;
%%% done


% --- Outputs from this function are returned to the command line.
function varargout = CreateNewTrackset_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
global settings confirmed treelist

if confirmed
    varargout{1} = settings;
    varargout{2} = treelist;

else
    varargout{1} = [];
    varargout{2} = [];

end




function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function treeRoot_Callback(hObject, eventdata, handles)
% hObject    handle to treeRoot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of treeRoot as text
%        str2double(get(hObject,'String')) returns contents of treeRoot as a double


% --- Executes during object creation, after setting all properties.
function treeRoot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to treeRoot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function experiment_Callback(hObject, eventdata, handles)
% hObject    handle to experiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of experiment as text
%        str2double(get(hObject,'String')) returns contents of experiment as a double


% --- Executes during object creation, after setting all properties.
function experiment_CreateFcn(hObject, eventdata, handles)
% hObject    handle to experiment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function extDetection_Callback(hObject, eventdata, handles)
% hObject    handle to extDetection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of extDetection as text
%        str2double(get(hObject,'String')) returns contents of extDetection as a double


% --- Executes during object creation, after setting all properties.
function extDetection_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extDetection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function extQuantification_Callback(hObject, eventdata, handles)
% hObject    handle to extQuantification (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of extQuantification as text
%        str2double(get(hObject,'String')) returns contents of extQuantification as a double


% --- Executes during object creation, after setting all properties.
function extQuantification_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extQuantification (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function extNonFluor_Callback(hObject, eventdata, handles)
% hObject    handle to extNonFluor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of extNonFluor as text
%        str2double(get(hObject,'String')) returns contents of extNonFluor as a double


% --- Executes during object creation, after setting all properties.
function extNonFluor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extNonFluor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in corrDetection.
function corrDetection_Callback(hObject, eventdata, handles)
% hObject    handle to corrDetection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of corrDetection


% --- Executes on button press in corrQuantification.
function corrQuantification_Callback(hObject, eventdata, handles)
% hObject    handle to corrQuantification (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of corrQuantification



function annotation1_Callback(hObject, eventdata, handles)
% hObject    handle to annotation1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of annotation1 as text
%        str2double(get(hObject,'String')) returns contents of annotation1 as a double


% --- Executes during object creation, after setting all properties.
function annotation1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to annotation1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function imageRoot_Callback(hObject, eventdata, handles)
% hObject    handle to imageRoot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imageRoot as text
%        str2double(get(hObject,'String')) returns contents of imageRoot as a double


% --- Executes during object creation, after setting all properties.
function imageRoot_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageRoot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function normalizationImage_Callback(hObject, eventdata, handles)
% hObject    handle to normalizationImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of normalizationImage as text
%        str2double(get(hObject,'String')) returns contents of normalizationImage as a double


% --- Executes during object creation, after setting all properties.
function normalizationImage_CreateFcn(hObject, eventdata, ~)
% hObject    handle to normalizationImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectTreeRoot.
function selectTreeRoot_Callback(hObject, eventdata, handles)
% hObject    handle to selectTreeRoot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global vtset;

if exist(get(handles.treeRoot,'String'),'dir')
    pathname=uigetdir(get(handles.treeRoot,'String'));
elseif exist(vtset.tttfilesFolder,'dir')
    pathname=uigetdir(vtset.tttfilesFolder);
else
    pathname=uigetdir;
end

if ~isequal(pathname,0) 
%     treefiles = getFilesExt(pathname, '.ttt_simple');
    % create structure
%     treelist.treefile=treefiles;
%     for tree=1:numel(treefiles)
%         treelist.root{tree}=pathname;
%     end
    pathname = [pathname '/'];
%     [newselectedtrees restrictcells treelist]=TreeList({pathname});
%     if numel(newselectedtrees)>0
%         set(handles.treequantified,'String',numel(newselectedtrees));
%         set(handles.treecount,'String',numel(treelist.treefile));
%         set(handles.restrictcells,'String',restrictcells);
% 
%         selectedtrees=newselectedtrees;
% 
%     end
    set(handles.treeRoot, 'String', vtset.tttfilesFolder);
    vtset.tttfilesFolder = pathname;
    vtset.AMTfilesFolder = pathname;
%     [newselectedtrees restrictcells]=TreeList({pathname});
%     if numel(newselectedtrees)>0
%         set(handles.treeRoot, 'String', pathname)
%         set(handles.treequantified,'String',numel(selectedtrees));
%         set(handles.treecount,'String',numel(treefiles));
%         set(handles.restrictcells,'String',restrictcells);
%         selectedtrees=newselectedtrees;
%     end
end


    % get all experiment subfolders and list ttt_simple files
%     tttroot=['/home/tttcomputer/ISF/Daten/ISF-H/TTTexport/TTTfiles/*' exp '*'];
%     tttfolder=dir(tttroot);
%     treelist.root={};
%     treelist.treefile={};
%     if numel(tttfolder)>0 %exist([tttroot(1:end-(2+numel(exp))) tttfolder(1).name], 'dir')
% 
%         set(handles.treeRoot, 'String', tttroot(1:end-(2+numel(exp))));
%         for x=1:numel(tttfolder)
%             if tttfolder(x).isdir
%                 %cut tttroot = exp+ 2x * 
%                 tttfiles=dir([tttroot(1:end-(2+numel(exp))) tttfolder(x).name '/*.ttt_simple']);
%                 for f=1:numel(tttfiles)
%                     treelist.root=[treelist.root [tttroot(1:end-(2+numel(exp))) tttfolder(x).name]];
%                 end
%                 treelist.treefile=[treelist.treefile {tttfiles.name}];
%             end
%         end
%         
%         if numel(treelist.root)>0
%             [newselectedtrees restrictcells]=TreeList(treelist);
%             if numel(newselectedtrees)>0
%                 set(handles.treequantified,'String',numel(newselectedtrees));
%                 set(handles.treecount,'String',numel(treelist.treefile));
%                 set(handles.restrictcells,'String',restrictcells);
%                 
%                 selectedtrees=newselectedtrees;
%             end
% 
%         else
%             msgbox(sprintf('No TTT file found in %s',tttroot),'TTT File not found','warn');
%         end
%         
%     else
%         msgbox(sprintf('TTT tree folder %s not found', tttroot),'Folder not found','warn');
%     end
    



% --- Executes on button press in selectImageRoot.
function selectImageRoot_Callback(hObject, eventdata, handles)
% hObject    handle to selectImageRoot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global vtset

if exist(get(handles.imageRoot,'String'),'dir')
    pathname=uigetdir(get(handles.imageRoot,'String'));
elseif exist(vtset.movieFolder,'dir')
    pathname=uigetdir(vtset.movieFolder);
else
    pathname=uigetdir;
end

if ~isequal(pathname,0)
    pathname = [pathname '/'];
    set(handles.imageRoot, 'String', pathname)
    vtset.movieFolder = pathname;
    readXMLfile(handles);
end

% --- Executes on button press in selectNormlaizationImage.
% function selectNormlaizationImage_Callback(hObject, eventdata, handles)
% hObject    handle to selectNormlaizationImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% global vtset
% 
% if exist(get(handles.imageRoot,'String'),'dir')
%     [filename,pathname]=uigetfile([get(handles.imageRoot,'String') '*.tif;*.png;*.jpg'], 'Select the normalization image file');
% elseif exist(vtset.movieFolder,'dir')
%     [filename,pathname]=uigetfile([vtset.movieFolder '*.tif;*.png;*.jpg'], 'Select the normalization image file');
% else
%     [filename,pathname]=uigetfile('*.tif;*.png;*.jpg', 'Select the normalization image file');
% end
% 
% 
% if ~isequal(filename,0)
%     set(handles.normalizationImage, 'String', [pathname filename])
% end


% --- Executes on button press in ok_button.
function ok_button_Callback(hObject, eventdata, handles)
% hObject    handle to ok_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global settings confirmed

settings=getvariables(handles);

if isdir(settings.treeroot) && isdir(settings.imgroot)
    confirmed=1;

    close(handles.figure1);
else
    msgbox('Invalid tree or image directory','Invalid directory','warn');
end


% --- Executes on button press in cancel_button.
function cancel_button_Callback(hObject, eventdata, handles)
% hObject    handle to cancel_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global confirmed


confirmed=0;
close(handles.figure1);



% --- Executes on button press in showNormalizationImage.
% function showNormalizationImage_Callback(hObject, eventdata, handles)
% % hObject    handle to showNormalizationImage (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% corrImagePath = get(handles.normalizationImage,'String');
% if exist(corrImagePath,'file')
%     figure;
%     corrImage = double(imread(corrImagePath))/255;
%     tobase(corrImage)
%     topm= median(topmedian(corrImage(:),50));
%     corrImage = corrImage / topm;
%     imagesc(corrImage);
% 
% else
%     msgbox(sprintf('File not found: %s',corrImagePath),'File not found','warn');
% end



function restrictcells_Callback(hObject, eventdata, handles)
% hObject    handle to restrictcells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of restrictcells as text
%        str2double(get(hObject,'String')) returns contents of restrictcells as a double


% --- Executes during object creation, after setting all properties.
function restrictcells_CreateFcn(hObject, eventdata, handles)
% hObject    handle to restrictcells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%% SAVE
% --------------------------------------------------------------------
function uipushtool3_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global treelist

[filename,pathname]=uiputfile('*.mat', 'Specify a tree file');
if ~isequal(filename,0)
    disp('saving')
    file =[pathname filename];
    settings=getvariables(handles);
    save(file,'settings','treelist')
    disp('done')
end





%%%% OPEN
% --------------------------------------------------------------------
function uipushtool2_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global treelist

[filename,pathname]=uigetfile('*.mat', 'Select a settings file');


if ~isequal(filename,0)
    disp('loading')
    file =[pathname '/' filename];
    load(file)
    setvariables(settings,handles)
    
    disp('done')
end



function treecount_Callback(hObject, eventdata, handles)
% hObject    handle to treecount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of treecount as text
%        str2double(get(hObject,'String')) returns contents of treecount as a double


% --- Executes during object creation, after setting all properties.
function treecount_CreateFcn(hObject, eventdata, handles)
% hObject    handle to treecount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function treequantified_Callback(hObject, eventdata, handles)
% hObject    handle to treequantified (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of treequantified as text
%        str2double(get(hObject,'String')) returns contents of treequantified as a double


% --- Executes during object creation, after setting all properties.
function treequantified_CreateFcn(hObject, eventdata, handles)
% hObject    handle to treequantified (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in editlist.
function editlist_Callback(hObject, eventdata, handles)
% hObject    handle to editlist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global selectedtrees treelist vtset

pathname=get(handles.treeRoot,'String');

% treefiles = getFilesExt(pathname, '.ttt_simple');
tttroot=vtset.tttfilesFolder;

[newselectedtrees restrictcells treelist]=TreeList({tttroot},selectedtrees);
if numel(newselectedtrees)>0
    set(handles.treeRoot, 'String', pathname)
    set(handles.treequantified,'String',numel(selectedtrees));
    set(handles.treecount,'String',numel(treelist.treefile));
    set(handles.restrictcells,'String',restrictcells);

    selectedtrees=newselectedtrees;

end



function annotation2_Callback(hObject, eventdata, handles)
% hObject    handle to annotation2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of annotation2 as text
%        str2double(get(hObject,'String')) returns contents of annotation2 as a double


% --- Executes during object creation, after setting all properties.
function annotation2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to annotation2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in blockCorrection.
% function blockCorrection_Callback(hObject, eventdata, handles)
% hObject    handle to blockCorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of blockCorrection



% function blockCorrectionSize_Callback(hObject, eventdata, handles)
% hObject    handle to blockCorrectionSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of blockCorrectionSize as text
%        str2double(get(hObject,'String')) returns contents of blockCorrectionSize as a double


% --- Executes during object creation, after setting all properties.
% function blockCorrectionSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to blockCorrectionSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
% if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor','white');
% end


% --- Executes on button press in useprecalculatedBackground.
% function useprecalculatedBackground_Callback(hObject, eventdata, handles)
% hObject    handle to useprecalculatedBackground (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of useprecalculatedBackground


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonAdd.
function ButtonAdd_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonAdd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global cntsettings multiFolder

wllist = get(handles.extDetection,'String');
wl = wllist{get(handles.extDetection,'Value')};
  
if get(handles.SegmentationCheck,'Value')&&  ~isempty(strfind(wl, '*'))
    detectsettings = CreateNewTracksettings(true);
else
detectsettings = CreateNewTracksettings;
end
if numel(detectsettings)==0
    return;
end

liste = get(handles.listbox1,'String');

detect = get(handles.extDetection,'String');
if iscell(detect)
    detect = detect{get(handles.extDetection,'Value')};
end
quant = get(handles.extQuantification,'String');
if iscell(quant)
    quant = quant{get(handles.extQuantification,'Value')};
end
externalSegmentation = [];
if get(handles.SegmentationCheck,'Value')
    list = get(handles.extDetection,'String');
    for i = 1:numel(list)
        wl = strsplit_qtfy('.', list{i});
        wl = wl{1};
        if(~(isempty(strfind(detect, wl))))
            externalSegmentation.extension = strrep(detect,wl,'');
            externalSegmentation.extension = externalSegmentation.extension(1:end-1); % remove star 
           detect = list{i}; 
           break
        end
    end
end
if ~iscell(liste)
    if numel(liste)>0
        liste{1}=liste;
    else
        liste={};
    end
end
liste{end+1} = sprintf('detect:%s -> quantify:%s: %s',detect,quant,detectsettings.threshmethod);
set(handles.listbox1,'String',liste);
cntsettings(end+1).settings = detectsettings;
cntsettings(end).detect = detect;
cntsettings(end).quant = quant;
cntsettings(end).externalSegmentation = externalSegmentation;
cntsettings(end).externalSegmentation.multiFolder = multiFolder;


% --- Executes on button press in ButtonDelete.
function ButtonDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global cntsettings
liste = get(handles.listbox1,'String');

if numel(liste)==0
    return;
end

if ~iscell(liste)
    liste{1}=liste;
end

liste(get(handles.listbox1,'Value'))=[];
cntsettings(get(handles.listbox1,'Value'))=[];
set(handles.listbox1,'Value',1)
set(handles.listbox1,'String',liste)

    


% --- Executes on button press in checkboxOnlyTimepoints.
function checkboxOnlyTimepoints_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxOnlyTimepoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxOnlyTimepoints


% --- Executes when uipanel4 is resized.
function uipanel4_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to uipanel4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function imageStructure_Callback(hObject, eventdata, handles)
% hObject    handle to imageStructure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of imageStructure as text
%        str2double(get(hObject,'String')) returns contents of imageStructure as a double


% --- Executes during object creation, after setting all properties.
function imageStructure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageStructure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in editImageStructure.
function editImageStructure_Callback(hObject, eventdata, handles)
% hObject    handle to editImageStructure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filesettings vtset
s.name = get(handles.imageStructure, 'String');
settings = figureSetup(s);

if numel(settings)==0
    return;
end
    % set new settings and update display
    filesettings = settings;
    [imFile ext] = getAnImageFile(vtset.movieFolder,get(handles.experiment,'String'));
    name = generateFilename(get(handles.experiment,'String'), 1, 1, 1, 1, ext, filesettings);   
    set(handles.imageStructure,'String',name); 


% --- Executes on button press in SegmentationCheck.
function SegmentationCheck_Callback(hObject, eventdata, handles)
% hObject    handle to SegmentationCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of SegmentationCheck
global multiFolder
multiFolder = false;
% check whether box is marked
if (get(handles.SegmentationCheck,'Value'))
    [list] = useSegmentation(true, get(handles.extDetection,'String'));
    
else
    [list] = useSegmentation(false, get(handles.extDetection,'String'));
end
set(handles.extDetection,'String',list);
    % make extDetection visible 
    set(handles.extDetection, 'Value', 1);