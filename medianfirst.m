% returns the median of num first elements
% if num > numel(vec) returns the median of numel(vec) first elements

function ret=medianfirst(vec, num)

if numel(vec)<num
    ret=medianfirst(vec,numel(vec));
else
    ret=nanmedian(vec(1:num));
end