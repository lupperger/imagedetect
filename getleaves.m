function leaves =  getleaves(ids)
leaves = [];
for i=1:numel(ids)
    if ismember(ids(i)*2,ids) || ismember(ids(i)*2+1,ids)
        continue
    else
        leaves = [leaves ids(i)];
    end
end
end