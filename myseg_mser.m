function [Center,All, numL]=myseg_mser(OrigImage, Threshold, trackset, centerX, centerY)

trackset.MinDiversity=0.00001;
trackset.MaxVariation=3;

% trackset.MinArea=trackset.MinArea/trackset.win^2;
% trackset.MaxArea=trackset.MaxArea/trackset.win^2;


%% clahe

OrigImage = adapthisteq(OrigImage);

%% smoothing filter
sigma = 1;
FiltLength = 2*sigma;                                              % Determine filter size, min 3 pixels, max 61
[x,y] = meshgrid(-FiltLength:FiltLength,-FiltLength:FiltLength);   % Filter kernel grid
f = exp(-(x.^2+y.^2)/(2*sigma^2));f = f/sum(f(:));                 % Gaussian filter kernel
%                BlurredImage = conv2(OrigImage,f,'same');                             % Blur original image
%%% This adjustment prevents the outer borders of the image from being
%%% darker (due to padding with zeros), which causes some objects on the
%%% edge of the image to not  be identified all the way to the edge of the
%%% image and therefore not be thrown out properly.
% BlurredImage= OrigImage;
BlurredImage = conv2(OrigImage,f,'same') ./ conv2(ones(size(OrigImage)),f,'same');
BlurredImage=BlurredImage/max(BlurredImage(:));

%%
% BlurredImage = CPsmooth(OrigImage,'Gaussian Filter',trackset.smooth,0); 

%% apply MSER

[I,Center,All]=segmentImage_mser(im2uint8(BlurredImage),trackset,centerX,centerY);


%% select cell

% numL=0;
% if ~trackset.usenearest
%     Center = bwselect(All,centerX,centerY);
% else
% %     L = bwlabel(All);
%     numL = max(L(:));
%     % iterate over them
%     minDist=Inf;
%     minCell=-1;
%     for i=1:numL
%         [r,c] = find(L==i);
%         % distance to center
%         dist=norm([centerX centerY] -[mean(c) mean(r)]);
%         if dist<minDist
%             minDist=dist;
%             minCell=i;
%         end
%     end
%     Center=L==minCell;
% end
