function res=gatherAbsTimeAndFileName(res,imgroot,moviename,wavelength,moviestart)

% INIT
res.absoluteTime=zeros(numel(res.cellNr),1);
res.filename=cell(numel(res.cellNr),1);


for posi = unique(res.positionIndex)'
    fprintf('gathering position %d files %s...\n',posi,wavelength)
    
    % read position log file to get absolute timepoints otherwise use old
    % method
    if exist(sprintf('%s/%s_p%04d/',imgroot,moviename, posi),'dir')
        newexperiment=1;
        filename = sprintf('%s/%s_p%04d/%s_p%04d.log',imgroot,moviename,posi,moviename,posi);
    else
        newexperiment = 0;
        filename = sprintf('%s/%s_p%03d/%s_p%03d.log',imgroot,moviename,posi,moviename,posi);
    end
    
    if exist(filename,'file')
        % read logfile
        log = positionLogFileReader(filename);
        
        idx=find(res.positionIndex == posi)';
        
        wl = strsplit('.',wavelength);
        wl = str2double(wl{1}(2:end));
        
        for id = idx
            % store everything
            abstime = log.absoluteTime(log.timepoint == res.timepoint(id) & log.wavelength == wl);
            if isempty(abstime)
                continue
            else
                zindex= '';
                if newexperiment
                    if log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl) ~= -1
                        zindex = sprintf('z%03d_',log.zindex(log.timepoint == res.timepoint(id) & log.wavelength == wl));
                    end
                    % example:
                    % old 110624AF6_p0024_t00002_w1.tif
                    % new 111103PH5_p0148_t00002_z001_w01.png
                    
                    filename = sprintf('%s_p%04d/%s_p%04d_t%05d_%s%s', moviename,posi,moviename,posi,res.timepoint(id),zindex,wavelength);
                else
                    filename = sprintf('%s_p%03d/%s_p%03d_t%05d_%s%s',  moviename,posi,moviename,posi,res.timepoint(id),zindex,wavelength);
                end

                res.absoluteTime(id) = abstime-moviestart;
                res.filename{id} = filename;
            end
        end
    end
end