function treedata = initializeTreeData(settings,treelist,tree)
%%

% init the detects
treedata.detects = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'X',{},'Y',{},'filename',{},'cellmask',{},'size',{},'trackset',{},'active',{},'inspected',{},'perimeter',{});

% find the unique combination of detect/quant channel and thresh method
detects=cell(1,numel({settings.channelsettings.detect}));
quants=cell(1,numel({settings.channelsettings.quant}));
for i=1:numel({settings.channelsettings.detect})
    detects{1,i} = [ settings.channelsettings(i).detect '---' settings.channelsettings(i).settings.threshmethod];
    detects{2,i} = num2str(i);
    quants{i} = [settings.channelsettings(i).quant '...' settings.channelsettings(i).detect '---' settings.channelsettings(i).settings.threshmethod];
end

unidetects = unique({detects{1,:}});

% set the channels
for j = 1:numel(unidetects)
    splitted = strsplit_qtfy('---',unidetects{j});
    treedata.detects(j).settings.wl = splitted{1};
    % find correct threshmethod
    treedata.detects(j).settings.trackset = settings.channelsettings(strcmp(unidetects{j},{detects{1,:}})).settings;
    treedata.detects(j).settings.normalize = settings.corrDetect;
    treedata.detects(j).settings.externalSegmentation = settings.channelsettings.externalSegmentation;
end
%%
% init the quants
treedata.quants = struct('settings',{},'cellNr',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'int',{},'active',{},'detectchannel',{},'inspected',{});
quants = unique(quants);
for j = 1:numel(quants)
    splitted = strsplit_qtfy('...',quants{j});
    treedata.quants(j).settings.wl = splitted{1};
    % find the correct detection channel
    treedata.quants(j).settings.detectchannel = find(strcmp(unidetects,splitted{2}));
    treedata.quants(j).settings.normalize = settings.corrQuant;
end

%%
% finally the nonFluor
treedata.nonFluor = struct('settings',{},'cellNr',{},'X',{},'Y',{},'absoluteX',{},'absoluteY',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'active',{},'stopReason',{},'freefloating',{},'nonadherent',{},'additionalAttribute',{},'tissueType',{},'generalType',{},'lineage',{});
treedata.nonFluor(1).settings.wl = settings.extNonFluor;


%%
% create morph
treedata.morph = struct('settings',{},'cellNr',{},'X',{},'Y',{},'timepoint',{},'absoluteTime',{},'positionIndex',{},'filename',{},'features',{});


%%
% and the settings
treedata.settings.treeroot=treelist.root{(tree)};
treedata.settings.treefile= treelist.treefile{(tree)};
treedata.settings.imgroot=settings.imgroot;
treedata.settings.exp=settings.exp;
treedata.settings.channelsettings = settings.channelsettings;
treedata.settings.anno=settings.anno;
treedata.settings.anno2=settings.anno2;
treedata.settings.restrictcells=settings.restrictcells;
treedata.settings.corrDetect=settings.corrDetect;
treedata.settings.corrQuant=settings.corrQuant;
treedata.settings.moviestart=settings.moviestart;% timestamp of first image in pos001
treedata.description='';
treedata.settings.filesettings = settings.filesettings;

