function vec=torow(vec)
if ~isrow(vec)
    vec=vec';
end