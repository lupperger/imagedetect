function varargout = optimizeSegmentationGUI(varargin)
% OPTIMIZESEGMENTATIONGUI MATLAB code for optimizeSegmentationGUI.fig
%      OPTIMIZESEGMENTATIONGUI, by itself, creates a new OPTIMIZESEGMENTATIONGUI or raises the existing
%      singleton*.
%
%      H = OPTIMIZESEGMENTATIONGUI returns the handle to a new OPTIMIZESEGMENTATIONGUI or the handle to
%      the existing singleton*.
%
%      OPTIMIZESEGMENTATIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OPTIMIZESEGMENTATIONGUI.M with the given input arguments.
%
%      OPTIMIZESEGMENTATIONGUI('Property','Value',...) creates a new OPTIMIZESEGMENTATIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before optimizeSegmentationGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to optimizeSegmentationGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help optimizeSegmentationGUI

% Last Modified by GUIDE v2.5 03-Apr-2014 14:58:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @optimizeSegmentationGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @optimizeSegmentationGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before optimizeSegmentationGUI is made visible.
function optimizeSegmentationGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to optimizeSegmentationGUI (see VARARGIN)
global opp vtset
% Choose default command line output for optimizeSegmentationGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

opp.figure1 = handles.figure1;

% load tree list
tlist = {};
for t = 1:numel(vtset.results)
    tlist{end+1} = vtset.results{t}.settings.treefile;
end
set(handles.listTrees,'String',tlist)
set(handles.listTrees,'Value',vtset.treenum)

% load selected cells
set(handles.editCells,'String',num2str(vtset.cells));

% load channel list from actual tree
set(handles.listChannel,'String',vtset.channels)
if isfield(vtset,'dcselected')
    set(handles.listChannel,'Value',vtset.dcselected.channel);
else
    set(handles.listChannel,'Value',1);
end

% init threshold
if ~isfield(opp,'thresh')
    opp.thresh = 1;
end
 set(handles.sliderThresh,'Value',opp.thresh);
 set(handles.editThresh,'String',num2str(opp.thresh));
    
    
% UIWAIT makes optimizeSegmentationGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = optimizeSegmentationGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listTrees.
function listTrees_Callback(hObject, eventdata, handles)
% hObject    handle to listTrees (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listTrees contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listTrees
global vtset;

val = get(hObject, 'Value');
set(vtset.lstTree, 'Value', val(1));
feval(vtset.functions.lstTrees_Callback);
vtset.tree_numbers = val;







% --- Executes during object creation, after setting all properties.
function listTrees_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listTrees (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listChannel.
function listChannel_Callback(hObject, eventdata, handles)
% hObject    handle to listChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listChannel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listChannel


% --- Executes during object creation, after setting all properties.
function listChannel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editCells_Callback(hObject, eventdata, handles)
% hObject    handle to editCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.radiobuttonAll,'Value',0)
set(handles.radiobuttonCustom,'Value',1)
% Hints: get(hObject,'String') returns contents of editCells as text
%        str2double(get(hObject,'String')) returns contents of editCells as a double


% --- Executes during object creation, after setting all properties.
function editCells_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function sliderThresh_Callback(hObject, eventdata, handles)
% hObject    handle to sliderThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.editThresh,'String',num2cell(get(handles.sliderThresh,'Value')))
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global opp vtset
% check if deviations have been calced for this channel

channel = get(handles.listChannel,'Value');

if isfield(opp,'outlier') && isfield(opp.outlier,'allrm') && numel(opp.outlier.allrm) >= channel && numel(opp.outlier.allrm{channel})
    
else
    opp.outlier.allrm{channel}={};

    calculateDeviations(channel)
end

allrm = opp.outlier.allrm{channel};


if numel(vtset.hplot)>0
    
%     axes(vtset.haxes(chan));
    
    try
%         for i=1:numel(vtset.outlierhandles)
            delete(opp.outlierhandles);
%         end
    catch
        % already deleted
    end
    opp.outlierhandles=[];
    opp.outliers=[];
    
    % gather cells
    try
        if ~isfield(vtset.dcselected,'channel')
            msgbox('You have to select a channel first')
            return
        end
    catch err
        msgbox('You have to select a channel first')
    end
        
    chan = get(handles.listChannel,'Value');
 
    if isfield(vtset, 'tree_numbers')
        trees = vtset.tree_numbers;
    else
        trees = vtset.treenum;
    end
    for i=1:1:length(trees)
        current_tree = trees(i);
    
        if get(handles.radiobuttonAll,'value')
            cells = unique(vtset.results{current_tree}.nonFluor.cellNr);
        else
            if length(get(handles.editCells,'String')) > 1
                cells = strread(get(handles.editCells,'String'))
                %cells = cell2mat(get(handles.editCells,'String'));
            else
                cells = get(handles.editCells,'String');
            end
            try
                if strcmpi(cells,'all')
                    cells = unique(vtset.results{current_tree}.quants(chan).cellNr);
                else
                    cells=strrep(cells, '-',':');
                    cells=strrep(cells, ',',' ');
                   % cells=['[' cells ']' ];
                    %cells=eval(cells);
                    cells=cells(ismember(cells, unique(vtset.results{current_tree}.quants(chan).cellNr)));
                end
            catch e
                errordlg('Did not detect any valid cell number')
                return;
            end
        end
        hold(vtset.haxes(chan),'on');

        for c = torow(cells)
            data= tocolumn(vtset.results{current_tree}.quants(chan).int(vtset.results{current_tree}.quants(chan).cellNr==c & vtset.results{current_tree}.quants(chan).active==1));
            tps= tocolumn(vtset.results{current_tree}.quants(chan).timepoint(vtset.results{current_tree}.quants(chan).cellNr==c & vtset.results{current_tree}.quants(chan).active==1));
            abst= vtset.results{current_tree}.quants(chan).absoluteTime(vtset.results{current_tree}.quants(chan).cellNr==c & vtset.results{current_tree}.quants(chan).active==1)/vtset.timemodifier;
            % outlier  = deviate thresh * std from smoothed data
    %         thresh=get(handles.slider_outlier,'Value')*std(smooth(data)-data);
    %         outliers= (abs(smooth(data)-data))>thresh;

            % outlier in respect to whole distribution
            rm=distance2Smoothed(data);
            outliers = rm>median(allrm(allrm<100))+get(handles.sliderThresh,'Value')*std(allrm(allrm<100));
            if sum(outliers)>0
                if i == 1
                    opp.outlierhandles(end+1)=plot(abst(outliers),data(outliers),'o','linewidth',5','parent',vtset.haxes(chan),'Color','black');
                end
                opp.outliers(end+1:end+sum(outliers),:,:,:) = [zeros(sum(outliers),1)+c, tps(outliers), zeros(sum(outliers),1)+current_tree];

            end

        end
        hold(vtset.haxes(chan),'off');
    end
end

% --- Executes during object creation, after setting all properties.
function sliderThresh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editThresh_Callback(hObject, eventdata, handles)
% hObject    handle to editThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.sliderThresh,'Value',str2double(get(handles.editThresh,'String')))
sliderThresh_Callback([],[],handles);
% Hints: get(hObject,'String') returns contents of editThresh as text
%        str2double(get(hObject,'String')) returns contents of editThresh as a double


% --- Executes during object creation, after setting all properties.
function editThresh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editThresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonAll.
function ButtonAll_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.listTrees,'value',1:numel(get(handles.listTrees,'String')))

% --- Executes on button press in ButtonNone.
function ButtonNone_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonNone (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.listTrees,'value',[])


% --- Executes on button press in ButtonOptimize.
function ButtonOptimize_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonOptimize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
optimizeSegmentationPost_new;


% --- Executes on button press in ButtonDelete.
function ButtonDelete_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global opp vtset

if numel(opp.outliers)>0;
    force=0;
    erfolgid=[];
    for i = 1:size(opp.outliers,1)
        cell = opp.outliers(i,1);
        tp = opp.outliers(i,2);
        chan = get(handles.listChannel,'Value');
        if ~force
            button = questdlg(sprintf('Do you want to mark Cell %d Timepoint %d as outlier',cell,tp),'Delete Tree','Yes','Mark All','Cancel','Cancel');
        end
        if numel(button)==0
            return
        end
        if strcmp(button(1),'C')
            return
        elseif strcmp(button(1),'M')
            force=1;
            button(1) = 'Y';
        end
        
        
        if strcmp(button(1),'Y')
            
            %%% trackpoint will be  marked in actual quant channel
            % non fluor and detect can be used for other channels
            
            deleteid = find(vtset.results{vtset.treenum}.quants(chan).cellNr==cell & vtset.results{vtset.treenum}.quants(chan).timepoint==tp);
            if numel(deleteid)==1
                vtset.results{vtset.treenum}.quants(chan).active(deleteid)= 0;
                erfolgid(end+1) = i;
            else
                msgbox(sprintf('Could not delete cell %d, timepoint %d in tree %s', cell, tp, vtset.results.treefiles{curtree}),'Error','error');
            end
            
        end
        
       
    end
    opp.outliers(erfolgid,:)=[];
    try
        delete(opp.outlierhandles);
    catch e
        % nevermind
    end
    opp.outlierhandles=[];


    vtset.unsaved(vtset.treenum)=1;
    vtset.updateCallback();
%     TrackACell;
end

% --- Executes on button press in radiobuttonAll.
function radiobuttonAll_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonAll
set(handles.radiobuttonCustom,'Value',0)
set(hObject,'Value',1)

% --- Executes on button press in radiobuttonCustom.
function radiobuttonCustom_Callback(hObject, eventdata, handles)
% hObject    handle to radiobuttonCustom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobuttonCustom
set(handles.radiobuttonAll,'Value',0)
set(hObject,'Value',1)


% get a feeling for how large deviations are from estimation and store in
% vtset
function calculateDeviations(channel)
global opp vtset
wh = waitbar(0,sprintf('calculating outliers for channel %d',channel));
allrm=[];
%%%% TODO: check channel if same wl
for tree = 1:numel(vtset.results)
    waitbar(tree/numel(vtset.results),wh);
    for c = unique(vtset.results{tree}.nonFluor.cellNr)
        alldata = vtset.results{tree}.quants(channel).int(vtset.results{tree}.quants(channel).cellNr ==c);
        alltps  = vtset.results{tree}.quants(channel).timepoint(vtset.results{tree}.quants(channel).cellNr ==c);
        
        [alltps, order] = sort(alltps);
        alldata = alldata(order);
        
        
        % leave one out cross validation
        rm=distance2Smoothed(alldata);
        allrm=[allrm torow(rm)];
    end
end
opp.outlier.allrm{channel}=allrm;
delete(wh);
