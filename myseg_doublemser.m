function [L props]=myseg_doublemser(varargin)

image = varargin{1};

mseroptions = varargin{2};

if numel(varargin)>2
    mseroptionsMini = varargin{3};
else
    mseroptionsMini.delta = 1;
    mseroptionsMini.maxVariation = 1;
    mseroptionsMini.minSize = 0;
    mseroptionsMini.maxSize = 50;
    mseroptionsMini.invert = 0;
end



% do the mini MSER to detect cell markers
[mserBW,mserProps] = mserSegmentation(image,mseroptionsMini);
coords = mserProps;

% do the big MSER to detect clumped shapes

[mserBW,mserProps,mserHierachy] = mserSegmentation(image,mseroptions);
% figure;imagesc(mserBW)
% hold on
% plot(coords(:,1),coords(:,2),'ow')

% shedden
I_max = zeros(size(image));
for c =1:numel(coords(:,1))
    
    I_max(round(coords(c,2)),round(coords(c,1))) = 1;
end


% make overlay
Overlaid = imimposemin(1 - mserHierachy,I_max);

% shed it
WatershedBoundaries = watershed(Overlaid) > 0;
Objects = mserBW.*WatershedBoundaries;


% filter watershed image, keep only objects with object marker
L = bwlabel(Objects);
for i = 1:max(L(:))
    ids = (L == i);
    if ~any(I_max(ids))
        L(L==i)= 0;
    end
end

stats=regionprops( L, 'Centroid');
coords = [stats(:).Centroid];

props = [coords(1:2:end);coords(2:2:end)]';