function ret=imageCache(file,size)
global imgcache

if strcmp(file,'init')
    % initialize
    imgcache=[];
    imgcache.size = size;
    imgcache.files = {};
    imgcache.img={};
    imgcache.next=1;
    imgcache.verbose=1;
else
    % search for it
    pos=find(strcmp(imgcache.files,file));
    if numel(pos)==0
        % new one, load it
        if exist(file,'file')
            fprintf('Loading image %s ',file)
            info = imfinfo(file);
            fprintf('with BitDepth %d\n',info.BitDepth)
            
            img=double(imread(file));

            if info.BitDepth == 8
                img = img./(2^8-1);
            elseif info.BitDepth == 16
                img = img./(2^16-1);
            elseif info.BitDepth == 14
                img = img ./ (2^14-1);
            else
                fprintf('Strange BitDepth detected %d for image %s\n',info.BitDepth,file)
%                 img = img ./ (2^info.BitDepth-1);
                img = img ./ 255;
            end
            
            % to rgb
            if ndims(img) >2
                fprintf('Converting image %s to gray scale.\n',file)
                img = rgb2gray(img);
            end
            imgcache.img{imgcache.next}=img;
        else
            if imgcache.verbose
                msgbox(sprintf('File not found %s',file),'File not found','error');
            end
            error('File not found %s',file);
        end
        ret=imgcache.img{imgcache.next};
        imgcache.files{imgcache.next}=file;
%         fprintf('Cache - from disk: %s\n', file);
        % increase pointer
        imgcache.next=imgcache.next+1;
        if imgcache.next>imgcache.size
            imgcache.next=1;
        end
    else
        % existing one
%         fprintf('Cache - from mem:  %s\n', file);
        ret=imgcache.img{pos};
    end
end
