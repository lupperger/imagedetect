function split_amtexport(filename)

r = ezread(filename);


for c = unique(r.cellNr)'
    x=[];
    f = fieldnames(r)';
    for u = 2:numel(f)
        x.(f{u}) = r.(f{u})(r.cellNr == c);
    end
    
    % save
    
    ezwrite([filename(1:end-4) sprintf('_c%03d.csv',c)],x); 
end