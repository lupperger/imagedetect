function filereader(res,tree)

global vtset

%% first gather all detects
for m = 1:numel(vtset.results{tree}.detects)
    gatherimages(res,tree,'D',m)
end

%%% TODO: Optimize

% wllist_detects = {};
% for n= 1:numel(vtset.results{tree}.detects)
%     wllist_detects{end+1} = vtset.results{tree}.detects(n).settings.wl;
% end

%% check if same wl as in detects --> copy, else gather quants
for m= 1:numel(vtset.results{tree}.quants)
%     if ismember(vtset.results{tree}.quants(m).settings.wl,unique(wllist_detects))
%         pos = find(ismember(wllist_detects,vtset.results{tree}.quants(m).settings.wl)==1);
%         vtset.results{tree}.quants(m).cellNr = vtset.results{tree}.detects(pos(1)).cellNr;
%         vtset.results{tree}.quants(m).timepoint = vtset.results{tree}.detects(pos(1)).timepoint;
%         vtset.results{tree}.quants(m).absoluteTime = vtset.results{tree}.detects(pos(1)).absoluteTime;
%         vtset.results{tree}.quants(m).positionIndex = vtset.results{tree}.detects(pos(1)).positionIndex;
%         vtset.results{tree}.quants(m).filename = vtset.results{tree}.detects(pos(1)).filename;
%         vtset.results{tree}.quants(m).detectchannel = zeros(1,numel(vtset.results{tree}.detects(pos(1)).cellNr))+vtset.results{tree}.quants(m).settings.detectchannel;
%         vtset.results{tree}.quants(m).int = zeros(1,numel(vtset.results{tree}.detects(pos(1)).cellNr))-1;
%         vtset.results{tree}.quants(m).active = zeros(1,numel(vtset.results{tree}.detects(pos(1)).cellNr));
%     else
        gatherimages(res,tree,'Q',m)
%     end
end

% wllist_quants = {};
% for n= 1:numel(vtset.results{tree}.quants)
%     wllist_quants{end+1} = vtset.results{tree}.quants(n).settings.wl;
% end

%% same with nonFluor --> check if same wl as in detects or quants
for m= 1:numel(vtset.results{tree}.nonFluor)
%     if ismember(vtset.results{tree}.nonFluor(m).settings.wl,unique(wllist_detects))
%         
%         pos = find(ismember(wllist_detects,vtset.results{tree}.nonFluor(m).settings.wl)==1);
%         vtset.results{tree}.nonFluor(m).cellNr = vtset.results{tree}.detects(pos(1)).cellNr;
%         vtset.results{tree}.nonFluor(m).timepoint = vtset.results{tree}.detects(pos(1)).timepoint;
%         vtset.results{tree}.nonFluor(m).absoluteTime = vtset.results{tree}.detects(pos(1)).absoluteTime;
%         vtset.results{tree}.nonFluor(m).positionIndex = vtset.results{tree}.detects(pos(1)).positionIndex;
%         vtset.results{tree}.nonFluor(m).filename = vtset.results{tree}.detects(pos(1)).filename;
%         
%     elseif ismember(vtset.results{tree}.nonFluor(m).settings.wl,unique(wllist_quants))
%         
%         pos = find(ismember(wllist_quant,vtset.results{tree}.nonFluor(m).settings.wl)==1);
%         vtset.results{tree}.nonFluor(m).cellNr = vtset.results{tree}.quants(pos(1)).cellNr;
%         vtset.results{tree}.nonFluor(m).timepoint = vtset.results{tree}.quants(pos(1)).timepoint;
%         vtset.results{tree}.nonFluor(m).absoluteTime = vtset.results{tree}.quants(pos(1)).absoluteTime;
%         vtset.results{tree}.nonFluor(m).positionIndex = vtset.results{tree}.quants(pos(1)).positionIndex;
%         vtset.results{tree}.nonFluor(m).filename = vtset.results{tree}.quants(pos(1)).filename; 
%     else
        gatherimages(res,tree,'n',m)
%     end
end



% function gatherimages(res,tree,ch,x)
% %%
% global vtset
% 
% imgroot = vtset.results{tree}.settings.imgroot;
% 
% if strcmp(ch(1),'D')
%     channel = vtset.results{tree}.detects;
% elseif strcmp(ch(1),'Q')
%     channel = vtset.results{tree}.quants;
% else 
%     channel = vtset.results{tree}.nonFluor;
% end
% 
% for posi = unique(res.positionIndex)'
%     fprintf('gathering position %d...',posi)
% 
%     %gather all detects
% %     for x = 1:numel(channel)
%         ext = channel(x).settings.wl;
%         foldername = sprintf('%s%s_p%03d/*%s',  imgroot, vtset.results{tree}.settings.exp,posi,ext);
%         newexperiment=0;
%         d = dir(foldername);
%         if numel(d)==0
%             foldername = sprintf('%s%s_p%04d/*%s',  imgroot, vtset.results{tree}.settings.exp,posi,ext);
%             d = dir(foldername);
%             newexperiment=1;
%         end
%         
%         fprintf('found %d images...',numel(d))
%         % iterate over them and feed results struct, until timepoint is
%         % reached
%         for file = 1:numel(d)
%             % check timepoint
%             links = strfind(d(file).name,'_t');
%             rechts = strfind(d(file).name,'_w');
%             r2= strfind(d(file).name,'_z');
%             if numel(r2)
%                 rechts=r2;
%             end
%             tp = str2double(d(file).name(links+2:rechts-1));
%             if tp>max(res.timepoint)
%                 break;
%             end
%             
%             % read xml and store absolute time
%             xmlfilename=[foldername(1:end-7) d(file).name '_meta.xml'];
% %             sprintf('%s\n',xmlfilename);
%             xmlfile = fileread(xmlfilename);
%             % very naive
%             try
%                 
%                 % tat id is 1025 for acquisition time
%                 coords=strfind(xmlfile,'>1025<');
%                 % get the xml id
%                 if numel(coords)>1
%                     fprintf('\nWarning: Found more than one TAT identifier for acquisition time. Tying to use first one...\n')
%                     coords = coords(1);
%                 end
%                 id = xmlfile(coords-3:coords);
%                 id = strrep(id,'I','');
%                 
%                 % get xml value
%                 coords=strfind(xmlfile,['V' id]);
%                 abstime = datenum(xmlfile(coords(1)+4:coords(2)-3),'yyyy-mm-dd HH:MM:SS')* 60 * 60 * 24;
%             catch e
% 
%                 fprintf('\nError: XML image file error: absolute time not found in %s\n',xmlfilename)
%                 abstime = vtset.results{tree}.settings.moviestart;
%                     
%                 
%             end
% 
%             %get x,y,filename and cellnr for pos&tp&abstime
%             idx=find(res.timepoint==tp & res.positionIndex == posi)';
%             for id = idx
%                 % store everything
%                 channel(x).cellNr(end+1) = res.cellNr(id);
%                 channel(x).timepoint(end+1) = tp;
%                 channel(x).absoluteTime(end+1) = abstime-vtset.results{tree}.settings.moviestart;
%                 channel(x).positionIndex(end+1) = posi;
%                 if strcmp(ch(1),'D')
%                     channel(x).X(end+1) = res.X(id);
%                     channel(x).Y(end+1) = res.Y(id);
%                     channel(x).active(end+1) = 1;
%                 elseif strcmp(ch(1),'Q')
%                     %%% TODO check if this always works with every channel
%                     %%% combination
%                     channel(x).detectchannel(end+1) = channel(x).settings.detectchannel;
%                     channel(x).active(end+1) = 0;
%                     channel(x).int(end+1) = -1;
%                 else
%                     channel(x).stopReason(end+1) = res.stopReason(id);
%                 end
%                 if newexperiment
%                     channel(x).filename{end+1} = sprintf('%s_p%04d/%s',  vtset.results{tree}.settings.exp,posi,d(file).name);
%                 else
%                     channel(x).filename{end+1} = sprintf('%s_p%03d/%s',  vtset.results{tree}.settings.exp,posi,d(file).name);
%                 end
%                 fprintf('.')
%             end
%         end
%         fprintf('done.\n')
% %     end
%     
%     if strcmp(ch(1),'D')
%         vtset.results{tree}.detects = channel;
%     elseif strcmp(ch(1),'Q')
%         vtset.results{tree}.quants = channel;
%     else 
%         vtset.results{tree}.nonFluor = channel;
%     end
%     
% end



