% AMT function to quantify the channels
%%%% needs channel number index and number of cells/timepoints
%
%%%% take all cellmasks of given detect chennel loads and normalizes the
%%%% quantification images and quantifies.
%%%% a trackpoint gets active when quantification is successful


function quantifyCells(channelIndex,tree,cells,range)

global vtset imgcache
imgcache.verbose=0;
% restrict cells/range
if strcmp(cells,'all')
    cells = unique(vtset.results{tree}.quants(channelIndex).cellNr);
end
if strcmp(range,'complete')
    range = unique(vtset.results{tree}.quants(channelIndex).timepoint);
end

if strcmpi(cells,'only new cells')
    indizes = range;
elseif strcmpi(cells,'indizes')
    indizes = range;
else
    indizes = find(ismember(vtset.results{tree}.quants(channelIndex).cellNr,cells) & ismember(vtset.results{tree}.quants(channelIndex).timepoint,range));
end

e=0; %errorlog

%%% TODO optimize that one timepoint/image is only used once 4 every cell
%%% on it

%%% TODO catch if same timepoint is missing

for i = torow(indizes)
    % update waitbar
    waitbar(find(indizes==i)/numel(indizes))
    
    % map cellmask of timepoint and position
    detectChannel = vtset.results{tree}.quants(channelIndex).detectchannel(i);

    timepoint = vtset.results{tree}.quants(channelIndex).timepoint(i);
    cellNr = vtset.results{tree}.quants(channelIndex).cellNr(i);
    detectID = (vtset.results{tree}.detects(detectChannel).timepoint ==timepoint & vtset.results{tree}.detects(detectChannel).cellNr ==cellNr);
    try
    if sum(detectID)>1
        error('Error tree %s cell %d timepoint %d.\nYour timepoint occurs more than once as segmentation',vtset.results{tree}.settings.treefile,cellNr,timepoint)
    end
        
    % only if detect is active
    if ~vtset.results{tree}.detects(detectChannel).active(detectID)
        continue
    end
    
    if ~any(detectID)
        fprintf('seems there is no detection anymore? Skipping cell %d timepoint %d\n',cellNr,timepoint)
        continue
    end
    
    % load image & normalize
    image = loadimage([vtset.results{tree}.settings.imgroot vtset.results{tree}.quants(channelIndex).filename{i}],vtset.results{tree}.quants(channelIndex).settings.normalize, vtset.results{tree}.quants(channelIndex).settings.wl);
    
    %%% Quantification        
    fprintf('detectchan %d , id %d, cell %d, timepoint %d\n',(detectChannel),find(detectID),cellNr,timepoint);
    win=vtset.results{tree}.detects(detectChannel).trackset(detectID).win;
    
    % extract subimage
    % x and y
    x = round(vtset.results{tree}.detects(detectChannel).X(detectID));
    y = round(vtset.results{tree}.detects(detectChannel).Y(detectID));
%     trackset = vtset.results{tree}.detects(detectChannel).trackset(detectID);
    subimg = extractSubImage(image, x, y, win);
    
    % quantify
    cellmask = vtset.results{tree}.detects(detectChannel).cellmask{detectID};
    if cellmask == 0
        fprintf('Segmenting non initialized cells\n')
        detectCells(detectChannel,tree,cellNr,timepoint);
        cellmask = vtset.results{tree}.detects(detectChannel).cellmask{detectID};
    end
    dasum= subimg(cellmask);
%     dasum(dasum<0)=0;
    vtset.results{tree}.quants(channelIndex).int(i) = sum(dasum);
    vtset.results{tree}.quants(channelIndex).active(i) = 1;
    vtset.results{tree}.quants(channelIndex).inspected(i) = vtset.results{tree}.detects(detectChannel).inspected(detectID);
    catch exx
        e=e+1;
        % somethin really went wrong, report:
        fprintf('ERROR: Cell %d at timepoint %d in channel %d, tree %s could not be quantified\n',cellNr,timepoint, channelIndex, vtset.results{tree}.settings.treefile)
        fprintf('Error: %s\n',exx.message)
        vtset.results{tree}.quants(channelIndex).int(i) = -1;
        vtset.results{tree}.quants(channelIndex).active(i) = 0;
        if sum(detectID)>1 % if segmentation is twice in detects
            vtset.results{tree}.quants(channelIndex).inspected(i)=0;
        else
            vtset.results{tree}.quants(channelIndex).inspected(i) =  vtset.results{tree}.detects(detectChannel).inspected(detectID);
        end
    end
    % check if on the edge of image
%     if x+sqrt(trackset.MinArea)>size(image,2) || y+sqrt(trackset.MinArea)>size(image,1) || x-sqrt(trackset.MinArea)<1 || y-sqrt(trackset.MinArea)<1
%         vtset.results{tree}.quants(channelIndex).active(i) = 0;
%         fprintf('Ignoring cell %d at timepoint %d. Almost on the edge of image.\n',cellNr,timepoint)
%     else 
%         vtset.results{tree}.quants(channelIndex).active(i) = 1;
%     end
    
end
imgcache.verbose=1;
if e
    warndlg(sprintf('There have been %d errors during quantification fot tree %s, see log file for further details.',e,vtset.results{tree}.settings.treefile))
end