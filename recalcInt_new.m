function vtset=recalcInt_new(vtset)
vtset.error.tp=[];
vtset.error.cell=[];
vtset.error.tree=[];
normalizeit=1;
for t = 1:numel(vtset.results)
    
    %%%% HACK AROUND
%     vtset.results{t}.settings.imgroot = 'F:\120303PH5/';
    
    for qchan = 1:numel(vtset.results{t}.quants)
        for c = unique(vtset.results{t}.quants(qchan).cellNr)
            fprintf('%d,',c');
            alltps=vtset.results{t}.quants(qchan).timepoint(vtset.results{t}.quants(qchan).cellNr == c);
            for tp = alltps
                fprintf('tree %d cell %d tp %d chan %d ',t, c, tp,qchan);
                
                cellid = find(vtset.results{t}.quants(qchan).cellNr==c & vtset.results{t}.quants(qchan).timepoint == tp);
                % load quant img
                
                imgfileQuant=vtset.results{t}.quants(qchan).filename{cellid};
                
                try
                    imageQuant=loadimage([vtset.results{t}.settings.imgroot imgfileQuant],0,vtset.results{t}.quants(qchan).settings.wl);
                    %                 imageQuant=double(imread([vtset.results.settings.imgroot imgfileQuant]))/255;
                    %                 corrImage = double(imread([vtset.results.settings.imgroot imgfileQuant(1:16) 'background/' imgfileQuant(17:end-3) 'png']))/255/255;
                    %                 gain = double(imread([vtset.results.settings.imgroot imgfileQuant(1:16) 'background/gain.png']))/255/255;
                    %                 offset = double(imread([vtset.results.settings.imgroot imgfileQuant(1:16) 'background/offset.png']))/255/255;
                    %
                    %
                    %
                    %                 imageQuant = normalizeImage(imageQuant, corrImage,gain,offset);
                    dchan=vtset.results{t}.quants(qchan).detectchannel(cellid);
                    trackset = vtset.results{t}.detects(dchan).trackset(vtset.results{t}.detects(dchan).cellNr == c & vtset.results{t}.detects(dchan).timepoint ==tp);
                    x = vtset.results{t}.detects(dchan).X(vtset.results{t}.detects(dchan).cellNr == c & vtset.results{t}.detects(dchan).timepoint ==tp);
                    y = vtset.results{t}.detects(dchan).Y(vtset.results{t}.detects(dchan).cellNr == c & vtset.results{t}.detects(dchan).timepoint ==tp);
                    center = vtset.results{t}.detects(dchan).cellmask{vtset.results{t}.detects(dchan).cellNr == c & vtset.results{t}.detects(dchan).timepoint ==tp};
                    
                    subimgQuant = extractSubImage(imageQuant, x, y, trackset.win);
                    fprintf('old %f new %f\n',vtset.results{t}.quants(qchan).int(cellid), sum(subimgQuant(center)))
                    % store size
%                     vtset.results{t}.quants.size(cellid) =  sum(center(:));
                    % quantify
                    vtset.results{t}.quants(qchan).int(cellid) = sum(subimgQuant(center));
                    
                    % calc mean
%                     vtset.results{t}.quants.mean(cellid) = vtset.results{t}.quants.int(cellid) ...
%                         / vtset.results{t}.quants.size(cellid);
                catch e
%                                     rethrow(e)
                    'No background!'
                    daend = numel(vtset.error.tp);
                    vtset.error.tp(daend+1) = tp;
                    vtset.error.cell(daend+1) = c;
                    vtset.error.tree(daend+1) = t;
                    vtset.results{t}.quants.int(cellid) = -100;
                    %                 rethrow(e)
                end
            end
        end
    end
    fprintf('\n');
end


function dummy
%% recalc and compare dfferent background estimation approaches
counter=0;
bg_new=[];
bg_block=[];
for t = 1:numel(vtset.results.quants)
    
    for c = 1:35%unique(vtset.results.quants{t}.cellNr)'
        fprintf('%d,',c');
        for tp = vtset.results.quants{t}.timepoint(vtset.results.quants{t}.cellNr == c)'
            fprintf('tree %d cell %d tp %d ',t, c, tp);
            
            cellid = find(vtset.results.quants{t}.cellNr==c & vtset.results.quants{t}.timepoint == tp);
            % load quant img
            img=vtset.results.quants{t}.filename{cellid};
            img=double(imread([vtset.results.settings.imgroot img]))/255;
            trackset = vtset.results.quants{t}.trackset{cellid};
            
            % use background estimation
            
            imgwidth=size(img,2);
            imghight=size(img,1);
            
            [y x z] = backgroundestimation(img,70);
            [XI YI] = meshgrid(1:imgwidth,1:imghight);
            ZI=griddata(x',y',z',XI,YI,'cubic', {'Qt','Qbb','Qc' ,'Qs'});
            for i=find(sum(~isnan(ZI(:,1:imgwidth))')>0)
                ZI(i,:)=interp1(find(~isnan(ZI(i,:))),ZI(i,~isnan(ZI(i,:))), 1:imgwidth,'linear','extrap');
            end
            
            for i=1:imgwidth
                ZI(:,i)=interp1(find(~isnan(ZI(:,i))),ZI(~isnan(ZI(:,i)),i), 1:imghight,'linear','extrap');
            end
            counter=counter+1;
            
            bg_new(counter)  = ZI(trackset.y,trackset.x);
            
            % use block background estimation
            
            subimgQuantblock=extractSubImage(img, trackset.x, trackset.y, 600);
            bg_block(counter) = median(subimgQuantblock(:));
            
            fprintf('\n');
        end
        
    end
end



