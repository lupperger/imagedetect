% detect background pixel on raw image in the near of the cell

function background= findBackground(image, bincells, center)

% iterate over image, try to fit background square into image without
% hitting the binary cells

imgsize=size(bincells);
centersize=size(center);


% haaaaaaaaaaart
squaresize=10;

found=0;
for i = 1:(imgsize(1)-centersize(1))
    for j=1:(imgsize(2)-centersize(2))
        fittingsquare = zeros(imgsize(1), imgsize(2));
        
        %overlay center
        fittingsquare(i:i+centersize(1)-1,j:j+centersize(2)-1)=center;
        
        fittingsquare = fittingsquare + bincells;
        
        if ismember(2,fittingsquare(:))
            % collision with cell
        else
            found=1;
            break;
        end
    end
    
    if found
        break;
    end
end

if found
    % create binary image of background pixel
    fittingsquare = zeros(imgsize(1), imgsize(2));
    fittingsquare(i:i+centersize(1)-1,j:j+centersize(2)-1)=center;
    fittingsquare=im2bw(fittingsquare);
%     toBase(image);
%     toBase(fittingsquare);
    
    background=fittingsquare;
    % calc median of background
    
%     background = sum(image(fittingsquare));
%     background = median(background(:));
else
    % return median of whole picture?!?!?!?!?!?
    background = -Inf;%median(image(:));
    fprintf('no background found!!!\n');
end
