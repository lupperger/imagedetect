function results=mytracking(trackset, settings, treelist)
%% do it

% this code requires:
% 1. a 'settings' structure (must esp. contain 'treeroot')
% 2. a 'trackset' structure
%
% it processes all trees in 'treeroot'
%
% output:
% 'results' structure

% iterate over the files
% treefiles = getFilesExt(settings.treeroot, '.ttt_simple'); 

global vtset

if isfield(vtset,'results')
    results=vtset.results;
else
    results.quants = {};
    results.nonFluor = {};
    results.detects = {};
    results.description= cell(numel(treelist.treefile),1);
end
results.settings = settings;
results.trackset = trackset;
results.treefiles = treelist.treefile;
results.treelist = treelist;


tic;
h = waitbar(0,'Calculating tree...');

for trackNo=1:numel(treelist.treefile)
    %%%%%% init %%%%
    d.int=[];
    d.cellNr=[];
    q.int=[];
    q.cellNr=[];
    n.int=[];
    n.cellNr=[];

    
    %%%%% CALCULATION %%%%%%%%
    if ismember(trackNo,settings.selectedtrees)
        waitbar(0,h,sprintf('Calculating tree %d of %d',trackNo, numel(treelist.treefile)));

        % load from file
        try
            filename=[treelist.root{trackNo} '/' treelist.treefile{trackNo}];
            fid=fopen(filename);
            firstline=fgetl(fid);
            fprintf('### Processing tree %d: %s, %s\n', trackNo, treelist.treefile{trackNo},firstline);
%         if numel(strfind(firstline,'2.4.2'))<1
%             msgbox(sprintf('Trying to read an unknown TTT file --- unknown version %s',firstline(13:end)),'Unknow TTT file','warn')
%             curtree=ezread(filename,';');
%         else
            fclose(fid);
        
            curtree=ezreadskip1line(filename,';');
            curtree.absoluteTime=datenum(curtree.absoluteTime,'yyyy/mm/dd HH:MM:SS')* 60 * 60 * 24;
            curtree.absoluteTime=curtree.absoluteTime - min(curtree.absoluteTime);
        catch  e
            msgbox(sprintf('Failed to load ttt file %s', treelist.treefile{trackNo}),'Load error','error');
%             rethrow(e)
%             close(h)
            continue;
        end
        % recalc absolute time
       

        %%%%%% process tree
        % restrict if only new tracked cells should be quantified
        
        if strcmpi(settings.restrictcells(1),'o')
            
            %%% check already quantified cells if they have been retracked
            %%% try to quantify only higher timepoints
            
            for c=unique(results.quants{trackNo}.cellNr)'
                delids = (curtree.cellNr==c & curtree.timepoint <= max(results.quants{trackNo}.timepoint(results.quants{trackNo}.cellNr==c)));
                for f=fields(curtree)'
                    curtree.(str2mat(f))(delids)=[];
                end
            end
                
            

        elseif strcmpi(settings.restrictcells(1),'a')
            % do all, delete int field in order to overwrite all
            % quantifications
            if numel(results.quants)>=trackNo && isfield(results.quants{trackNo}, 'int')
                results.quants{trackNo} = rmfield(results.quants{trackNo},'int');
            end
        else
            % restrict ttt file to only selected cells
            
            delids = ~ismember(curtree.cellNr,settings.restrictcells);
            for f=fields(curtree)'
                curtree.(str2mat(f))(delids)=[];
            end
            % delete possible old quantifications
            if isfield(results,'quants') && numel(results.quants)>=trackNo && isfield(results.quants{trackNo}, 'cellNr')
                delids = ismember(results.quants{trackNo}.cellNr,settings.restrictcells);
                for f=fields(results.quants{trackNo})'
                    results.quants{trackNo}.(str2mat(f))(delids)=[];
                end
                for f=fields(results.detects{trackNo})'
                    results.detects{trackNo}.(str2mat(f))(delids)=[];
                end
                for f=fields(results.nonFluor{trackNo})'
                    results.nonFluor{trackNo}.(str2mat(f))(delids)=[];
                end
            end
        end

        % calc

        [d,q,n] = processTree(curtree,settings,trackset);
        if isfield(d,'error')
            msgbox(sprintf('Index out of range: X %d, Y %d \nImage: %s\nTree: %s',d.x+1,d.y+1,d.imgfile, treelist.treefile{trackNo}),'Error: out of range','error');
            d=rmfield(d,'x');
            d=rmfield(d,'y');
            d=rmfield(d,'imgfile');
            d=rmfield(d,'error');
        end
        %%%%%% store results

        % merge with old results or create new
        if isfield(results,'quants') &&  numel(results.quants)>= trackNo && isfield(results.quants{trackNo},'int')
            results.quants{trackNo} = mergeStructs(results.quants{trackNo},   q);
            results.detects{trackNo} = mergeStructs(results.detects{trackNo},d);
            results.nonFluor{trackNo} = mergeStructs(results.nonFluor{trackNo},n);
        else
            results.quants{trackNo} = q;
            results.detects{trackNo} = d;
            results.nonFluor{trackNo} = n;
            results.description{trackNo}=[];
        end

    end


end

close(h)
% store in results structure
results=calcEccentricity(results);
toc;