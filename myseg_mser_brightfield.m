function bw = myseg_mser_brightfield(I, trackset, centerX, centerY)

%Center = bw


I = I-min(min(I));
I = I./max(max(I));
I_org = I;
I = im2uint8(I);
%%

delta = trackset.Delta;
minsize = trackset.MinArea;
maxsize = trackset.MaxArea;
msers = linearMser(I,delta,minsize,maxsize,1,true);
bw = false(size(I));
for i=1:numel(msers)
    bw(msers{i}) = true;
end
bw_org = bw;



% bw = imclose(bw,strel('disk',1));
% bw = bwmorph(bw,'bridge');
% bw = logical(imfill(bw,'holes'));

% if ~isempty(trackset.clumped)
%     [L,bw2,bw_woMerge] = watershedCellProfiler(I,bw,'distance','hough','30,1000',1,1,1);
% end

% bw = imopen(bw,strel('disk',1));

% stats = regionprops(bw,I_org,'Eccentricity','Area','PixelIdxList','PixelValues');
% eccfilter = 0.99;
% minsize = 50;
% tofilt=([stats.Eccentricity]>eccfilter | [stats.Area]<minsize); %| ([stats.Eccentricity]<0.5 & [stats.Area]>maxSize);
% bw2 = bw;
% for id = find(tofilt)
%      bw([stats(id).PixelIdxList]) =0;
% end

%% select central object (and show it)
% centerX = round(size(Objects,1)/2);
% centerY = round(size(Objects,2)/2);
All = im2bw(bw_org);

numL=0;
if ~trackset.usenearest
    Center = bwselect(Objects,centerX,centerY);
else
    L = bwlabel(All);
    numL = max(L(:));
    % iterate over them
    minDist=Inf;
    minCell=-1;
    for i=1:numL
        [r,c] = find(L==i);
        % distance to center
        dist=norm([centerX centerY] -[mean(c) mean(r)]);
        if dist<minDist
            minDist=dist;
            minCell=i;
        end
    end
    bw=L==minCell;
end
