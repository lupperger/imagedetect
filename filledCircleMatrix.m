function img=filledCircleMatrix(img,sx,sy,r,c)


%   img(sx,sy,=c;

% int r = 50; // radius
% int ox = 100, oy = 100; // origin
sx=round(sx);
sy=round(sy);
r=round(r);

[w h] = size(img);
for x=-r:r
    height=floor(sqrt(r*r-x*x));
    for y=-height:height
        if sx+x>=1 && sy+y >=1 && sx+x<=w && sy+y<=h
            img(sx+x,sy+y,:)=c;
        end
    end
end
